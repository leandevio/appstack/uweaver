package org.uweaver.merchant.applet.dashboard;

import org.uweaver.core.json.JSONParser;
import org.uweaver.schedule.Recurrence;
import org.uweaver.schedule.Schedule;
import org.uweaver.schedule.Activity;
import org.uweaver.schedule.Schedules;
import org.uweaver.zk.applet.AbstractGadget;
import org.uweaver.zk.calendar.Calendar;
import org.uweaver.zk.calendar.CalendarEvent;
import org.uweaver.zk.util.ComponentBuilder;
import org.uweaver.zk.util.UIMLResourceBundle;
import org.uweaver.zk.widget.Trigger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class CalendarGadgets {
    private static UIMLResourceBundle resources = new UIMLResourceBundle(CalendarGadgets.class);
    private static ComponentBuilder builder = new ComponentBuilder();
    private static JSONParser parser = new JSONParser();

    public static class PersonalCalendar extends AbstractGadget {

        private Calendar calendar;
        public PersonalCalendar() {
            super();
            String zuml = resources.get("personal");
            Component component = builder.build(zuml);
            append(component);


            calendar = query("calendars", Calendar.class);
            Trigger trigger = query(".toolbar", Trigger.class);
            trigger.addEventListener("onMonth", event -> this.setMold("month"));
            trigger.addEventListener("onWeek", event -> this.setMold("week"));
            trigger.addEventListener("onDay", event -> this.setMold("day"));
            trigger.addEventListener("onPrevious", event -> calendar.previous());
            trigger.addEventListener("onNext", event -> calendar.next());
            trigger.addEventListener("onToday", event -> calendar.setCurrentDate(new Date()));

            calendar.setData(data());

            calendar.addEventListener("onEventCreate", this::onEventCreate);
            calendar.addEventListener("onEventEdit", this::onEventEdit);
            calendar.addEventListener("onEventUpdate", this::onEventUpdate);
        }

        private void onEventCreate(Event event) {
            CalendarEvent calendarEvent = (CalendarEvent) event;
            System.out.println(calendarEvent.getStartTime());
        }

        private void onEventEdit(Event event) {
            CalendarEvent calendarEvent = (CalendarEvent) event;
            System.out.println(calendarEvent.getActivity().getTitle());
        }

        private void onEventUpdate(Event event) {
            CalendarEvent calendarEvent = (CalendarEvent) event;
            System.out.println(calendarEvent.getStartTime());
        }

        private Schedules data() {
            Schedules schedules = new Schedules();

            Schedule personal = generateSchedule("Personal");
            Schedule company = generateSchedule("Company");

            schedules.add(personal);
            schedules.add(company);

            return schedules;
        }

        private Schedule generateSchedule(String name) {
            String[] TITLES = {"Appointment: Mike", "Meeting: ITSM", "Meeting: TAF", "Meeting: BraveLog"};
            String[] DESCS = {"Architecture discussion", "Project Planning", "Requirement Review", "User Training"};
            String[] COLORS = {"DodgerBlue", "Orange", "DarkCyan", "Salmon"};
            Schedule schedule = new Schedule();
            Instant startDate = calendar.getStartDate().toInstant();
            Instant endDate = calendar.getEndDate().toInstant();
            Random rand = new Random();

            for(Instant current = startDate; current.isBefore(endDate); current = current.plus(rand.nextInt(14) + 1, ChronoUnit.DAYS)) {
                Activity activity = new Activity();
                activity.setId(UUID.randomUUID().toString());
                Instant startTime = current.plus(rand.nextInt(24), ChronoUnit.HOURS);
                activity.setStartDate(Date.from(startTime));
                activity.setEndDate(Date.from(startTime.plus(rand.nextInt(72)+2, ChronoUnit.HOURS)));
                int pick = rand.nextInt(TITLES.length);
                activity.setTitle(TITLES[pick]);
                activity.setContent(DESCS[pick]);
                activity.setColor(COLORS[pick]);
                schedule.add(activity);
            }

            Activity occurentActivity = new Activity();
            occurentActivity.setId(UUID.randomUUID().toString());
            Instant startTime = startDate.plus(rand.nextInt(24), ChronoUnit.HOURS);
            occurentActivity.setStartDate(Date.from(startTime));
            occurentActivity.setEndDate(Date.from(startTime.plus(rand.nextInt(8)+2, ChronoUnit.HOURS)));
            int pick = rand.nextInt(TITLES.length);
            occurentActivity.setTitle("Recurrent Event: ITSD");
            occurentActivity.setContent("Project Review");
            occurentActivity.setColor(COLORS[pick]);

            Recurrence recurrence = new Recurrence();
            recurrence.setFrequency(Recurrence.Frequency.WEEKLY);
            occurentActivity.setRecurrence(recurrence);

            schedule.add(occurentActivity);

            return schedule;
        }

        @Override
        public void setMold(String mold) {
            if(mold.equalsIgnoreCase("month")) {
                calendar.setMold("month");
            } else if(mold.equalsIgnoreCase("day")) {
                calendar.setMold("default");
                calendar.setDays(1);
            } else {
                calendar.setMold("default");
                calendar.setDays(7);
            }
        }

        @Override
        public String title() {
            return "My Calendar";
        }
    }
}
