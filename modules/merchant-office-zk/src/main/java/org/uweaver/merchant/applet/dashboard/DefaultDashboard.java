package org.uweaver.merchant.applet.dashboard;

import org.uweaver.security.Pass;
import org.uweaver.user.Preference;
import org.uweaver.user.UserManager;
import org.uweaver.zk.applet.Gadget;
import org.uweaver.zk.applet.MDIApplet;
import org.uweaver.zk.util.ComponentBuilder;
import org.uweaver.zk.util.UIMLResourceBundle;
import org.uweaver.zk.widget.Menubar;
import org.uweaver.zk.widget.Panel;

public class DefaultDashboard extends MDIApplet {
    private UIMLResourceBundle resources = new UIMLResourceBundle(DefaultDashboard.class);
    private ComponentBuilder builder = new ComponentBuilder();
    private UserManager userManager = UserManager.getDefaultInstance();

    public DefaultDashboard() {
        String zuml = resources.get("menubar");
        Menubar menubar = builder.build(zuml, Menubar.class);
        append(menubar);

        add(new SalesGadgets.AreaSalesGadget());
        add(new SalesGadgets.DailySalesGadget());
        add(new SalesGadgets.DailySalesBarGadget());
        add(new SalesGadgets.DailySalesColumnStackingGadget());
        add(new SalesGadgets.AreaSalesPieGadget());
        add(new CalendarGadgets.PersonalCalendar());

        menubar.addEventListener("onSave", event -> this.save());
    }

    private void save() {
        Pass pass = this.pass();
        Preference preference = userManager.getPreference(pass.username());
        if(preference==null) preference = new Preference();
        preference.setKey(pass.username());
        for(Gadget gadget : gadgets()) {
            Panel panel = this.panel(gadget);
            panel.getTop();
        }
    }
}
