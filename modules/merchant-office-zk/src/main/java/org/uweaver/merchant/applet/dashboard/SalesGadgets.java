package org.uweaver.merchant.applet.dashboard;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Collection;
import org.uweaver.zk.chart.*;
import org.uweaver.search.Predicate;
import org.uweaver.zk.applet.AbstractGadget;
import org.uweaver.zk.util.ComponentBuilder;
import org.uweaver.zk.util.UIMLResourceBundle;
import org.zkoss.zk.ui.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class SalesGadgets {
    private static UIMLResourceBundle resources = new UIMLResourceBundle(SalesGadgets.class);
    private static ComponentBuilder builder = new ComponentBuilder();
    private static JSONParser parser = new JSONParser();
    public static class AreaSalesGadget extends AbstractGadget {

        public AreaSalesGadget() {
            super();
            String zuml = resources.get("area");
            Component component = builder.build(zuml);
            append(component);

            CartesianChart chart = query("charts", CartesianChart.class);
            chart.setTitle("Area Sales Statistics");
            chart.xAxis().setScale(new OrdinalScale());
            chart.xAxis().setTitle("Area");
            chart.yAxis().setScale(new LinearScale());
            chart.yAxis().setTitle("Revenue");
            chart.setData(data());
            chart.addLineSeries("2017", "area", "amount", new Predicate("year", 2017));
            chart.addLineSeries("2018", "area", "amount", new Predicate("year", 2018));
        }

        private Collection data() {
            Collection data = new Collection();

            try(InputStream input = this.getClass().getResourceAsStream("/data/AreaSales.json")) {
                data = parser.readValue(input, Collection.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return data;
        }

        @Override
        public String title() {
            return "Area Sales Statistics";
        }
    }

    public static class AreaSalesPieGadget extends AbstractGadget {

        public AreaSalesPieGadget() {
            super();
            String zuml = resources.get("area");
            Component component = builder.build(zuml);
            append(component);

            PolarChart chart = query("charts", PolarChart.class);
            chart.setTitle("Area Sales Statistics");
            chart.setData(data());
            chart.addPieSeries("2017", "amount", "area", new Predicate("year", 2017));
            chart.setDonut(0.5);
        }

        private Collection data() {
            Collection data = new Collection();

            try(InputStream input = this.getClass().getResourceAsStream("/data/AreaSales.json")) {
                data = parser.readValue(input, Collection.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return data;
        }

        @Override
        public String title() {
            return "Area Sales Statistics";
        }
    }

    public static class DailySalesGadget extends AbstractGadget {

        public DailySalesGadget() {
            super();
            String zuml = resources.get("daily");
            Component component = builder.build(zuml);
            append(component);

            CartesianChart chart = query("charts", CartesianChart.class);
            chart.setTitle("Daily Sales Statistics");
            chart.xAxis().setScale(new TimeScale());
            chart.xAxis().setTitle("Date");
            chart.yAxis().setScale(new LinearScale());
            chart.yAxis().setTitle("Revenue");
            chart.setData(data());
            chart.addLineSeries("Taiwan", "revenueDate", "amount", new Predicate("area", "Taiwan"));
            chart.addColumnSeries("Korea", "revenueDate", "amount", new Predicate("area", "Korea"));
            chart.addColumnSeries("Singapore", "revenueDate", "amount", new Predicate("area", "Singapore"));
        }

        private Collection data() {
            Collection data = new Collection();

            try(InputStream input = this.getClass().getResourceAsStream("/data/DailySales.json")) {
                data = parser.readValue(input, Collection.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return data;
        }


        @Override
        public String title() {
            return "Daily Sales Statistics";
        }
    }

    public static class DailySalesBarGadget extends AbstractGadget {

        public DailySalesBarGadget() {
            super();
            String zuml = resources.get("daily");
            Component component = builder.build(zuml);
            append(component);

            CartesianChart chart = query("charts", CartesianChart.class);
            chart.setTitle("Daily Sales Statistics");
            chart.xAxis().setScale(new TimeScale());
            chart.xAxis().setTitle("Date");
            chart.yAxis().setScale(new LinearScale());
            chart.yAxis().setTitle("Revenue");
            chart.setData(data());
            chart.addBarSeries("Taiwan", "revenueDate", "amount", new Predicate("area", "Taiwan"));
            chart.addBarSeries("Korea", "revenueDate", "amount", new Predicate("area", "Korea"));
            chart.addBarSeries("Singapore", "revenueDate", "amount", new Predicate("area", "Singapore"));
        }

        private Collection data() {
            Collection data = new Collection();

            try(InputStream input = this.getClass().getResourceAsStream("/data/DailySales.json")) {
                data = parser.readValue(input, Collection.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return data;
        }


        @Override
        public String title() {
            return "Daily Sales Statistics";
        }
    }


    public static class DailySalesColumnStackingGadget extends AbstractGadget {

        public DailySalesColumnStackingGadget() {
            super();
            String zuml = resources.get("daily");
            Component component = builder.build(zuml);
            append(component);

            CartesianChart chart = query("charts", CartesianChart.class);
            chart.setTitle("Daily Sales Statistics");
            chart.xAxis().setScale(new TimeScale());
            chart.xAxis().setTitle("Date");
            chart.yAxis().setScale(new LinearScale());
            chart.yAxis().setTitle("Revenue");
            chart.setData(data());
            chart.addColumnSeries("Taiwan", "revenueDate", "amount", new Predicate("area", "Taiwan"));
            chart.addColumnSeries("Korea", "revenueDate", "amount", new Predicate("area", "Korea"));
            chart.addColumnSeries("Singapore", "revenueDate", "amount", new Predicate("area", "Singapore"));
            chart.setStacking(Chart.Stacking.NORMAL);
        }

        private Collection data() {
            Collection data = new Collection();

            try(InputStream input = this.getClass().getResourceAsStream("/data/DailySales.json")) {
                data = parser.readValue(input, Collection.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return data;
        }


        @Override
        public String title() {
            return "Daily Sales Statistics";
        }
    }
}
