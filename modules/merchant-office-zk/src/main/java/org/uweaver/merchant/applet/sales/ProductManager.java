/**
 * This document is a part of the source code and attachments artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.applet.sales;

import org.uweaver.search.PageRequest;
import org.uweaver.search.Predicate;
import org.uweaver.core.util.Collection;
import org.uweaver.entity.Document;
import org.uweaver.merchant.entity.Product;
import org.uweaver.merchant.widget.VersionHistory;
import org.uweaver.merchant.service.PartnerService;
import org.uweaver.merchant.service.ProductService;
import org.uweaver.service.SequenceService;
import org.uweaver.zk.applet.AbstractGadget;
import org.uweaver.zk.applet.SmartApplet;
import org.uweaver.core.util.Model;
import org.uweaver.zk.util.ComponentBuilder;
import org.uweaver.zk.util.UIMLResourceBundle;
import org.uweaver.zk.widget.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProductManager obj = new ProductManager();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProductManager extends SmartApplet {
    private UIMLResourceBundle resources = new UIMLResourceBundle(ProductManager.class);
    private ComponentBuilder builder = new ComponentBuilder();
    private Finder finder = new Finder();

    public ProductManager() {
        finder.addEventListener("onFocus", event -> finder.render());
        finder.addEventListener("onCreate", event -> create());
        finder.addEventListener("onEdit", event -> edit(((String) event.getData())));
        add(finder);
    }

    private void create() {
        add(new Editor(), true);
    }

    private void edit(String uuid) {
        add(new Editor(uuid), true);
    }

    private class Finder extends AbstractGadget {
        private static final String TITLE = "Product Finder";
        private Form criteria;
        private Grid resultlist;
        private Collection collection;

        @WireVariable
        ProductService productService;

        public Finder() {
            super();
            String zuml = resources.get("finder");
            Component component = builder.build(zuml);
            append(component);

            criteria = query("#criteria", Form.class);
            Trigger criteriaTrigger = query("#criteria .trigger", Trigger.class);
            criteriaTrigger.addEventListener("onSearch", event -> search());
            criteriaTrigger.addEventListener("onReset", event -> reset());

            resultlist = query("#resultlist grid", Grid.class);
            Trigger resultlistTrigger = query("#resultlist .trigger", Trigger.class);
            resultlistTrigger.addEventListener("onCreate", event -> create());
            resultlistTrigger.addEventListener("onEdit", event -> edit());
            resultlistTrigger.addEventListener("onDelete", event -> delete());

            wire();

            collection = new Collection(productService);
            resultlist.setData(collection);
            resultlist.setHeight("400px");
            resultlist.column("platforms").hide();
            resultlist.column("release").hide();
        }

        public Finder render() {
            super.render();
             collection.fetch();
            return this;
        }

        public void create() {
            this.trigger("onCreate", null);
        }

        public void edit() {
            if(!this.resultlist.hasSelection()) {
                Popup.info("Please select an item to edit.");
                return;
            }
            this.trigger("onEdit", this.resultlist.singleSelection().getAsString("uuid"));
        }

        public void delete() {
            Model data = this.resultlist.singleSelection();
            productService.delete(data.getAsString("uuid"));
            this.render();
        }

        public void search() {
            List<Predicate> predicates = criteria.predicate();
            collection.fetch(predicates, new PageRequest(0, 4));
        }

        private void reset() {
            criteria.clear();
            collection.reset();
        }

        @Override
        public String title() {
            return TITLE;
        }
    }

    private class Editor extends AbstractGadget {
        private static final String TITLE = "Product Editor";
        private TabPanel tabPanel;
        private Forms forms = new Forms();
        private Trigger trigger;
        private String title = TITLE;
        private Product product = new Product();
        private Grid artifacts;
        private Model data = new Model();

        @WireVariable
        ProductService productService;

        @WireVariable
        PartnerService partnerService;

        @WireVariable
        SequenceService sequenceService;

        public Editor() {
            String zuml = resources.get("editor");
            Component component = builder.build(zuml);
            append(component);
            tabPanel = query("tabbox", TabPanel.class);

            zuml = ProductManager.this.resources.get("summary");
            Form summary = builder.build(zuml, Form.class);
            tabPanel.add(summary, "Summary");

            zuml = ProductManager.this.resources.get("design");
            Form design = builder.build(zuml, Form.class);
            tabPanel.add(design, "Design", false, false);

            artifacts = (Grid) design.input("artifacts");
            zuml = ProductManager.this.resources.get("artifactEditor");
            Form artifactEditor = builder.build(zuml, Form.class);
            Pane artifactEditorPane = query("#artifactEditorPane", Pane.class);
            artifactEditorPane.append(artifactEditor);
            artifacts.setEditor(artifactEditor);

            Trigger designTrigger = design.query(".trigger", Trigger.class);
            designTrigger.addEventListener("onAdd", event -> artifacts.add(event.getData()));
            designTrigger.addEventListener("onEdit", event -> editSelectedArtifact());
            designTrigger.addEventListener("onRemove", event -> artifacts.remove());

            VersionHistory history = new VersionHistory();
            tabPanel.add(history, "History", false, false);

            trigger = query("#commandbar", Trigger.class);
            trigger.addEventListener("onClose", event -> close());
            trigger.addEventListener("onSave", event -> save());
            trigger.addEventListener("onUndo", event -> undo());
            trigger.addEventListener("onDelete", event -> delete());
            trigger.addEventListener("onSequence", event -> {
                Popup.info(sequenceService.nextNumber("order"));
            });
            trigger.addEventListener("onFlush", event -> {
                sequenceService.flush();
            });

            wire();

            Combobox genres = (Combobox) summary.input("genres");
            genres.setData(genres());
            Selectbox manufacturer = (Selectbox) summary.input("manufacturer");
            manufacturer.setData(partnerService.findAll());
            Selectbox distributor = (Selectbox) summary.input("distributor");
            distributor.setData(partnerService.findAll());

            forms.add(summary); forms.add(design);

            forms.setData(data);
            forms.setState("create");
            trigger.setState("create");

            render();
        }

        public Editor(String uuid) {
            this();
            product = productService.findOne(uuid);
            product.getArtifacts().size();
            title = product.getName();
            setData(product);
        }

        private void editSelectedArtifact() {
            if(!this.artifacts.hasSelection()) {
                Popup.info("Please select an item to edit.");
                return;
            }
            String zuml = ProductManager.this.resources.get("artifactEditor");
            Form form = builder.build(zuml, Form.class);
            form.setData(this.artifacts.singleSelection());
            Popup.prompt(form, data -> this.artifacts.render());
        }

        private List<String> genres() {
            List list = new ArrayList();
            list.add("First-person shooter");
            list.add("Sports");
            list.add("Strategy");
            list.add("War");
            list.add("Action role-playing");
            list.add("Turn-based strategy");
            list.add("4X");
            list.add("Action-adventure");
            list.add("Real-time tactics");
            list.add("Construction and management simulation");
            return list;
        }

        public Editor render() {
            forms.render();
            return this;
        }

        @Override
        public String title() {
            return this.title;
        }

        private void save() {
            if(!forms.validate()) {
                Popup.warn(forms.errorMessage(), "Validation error");
                return;
            }

            Model model = new Model(data);
            Collection collection = ((Collection) model.get("artifacts"));
            List<Document> artifacts = (collection==null) ? null : collection.asBeans(Document.class);
            model.set("artifacts", artifacts);
            model.copyTo(this.product);
            try {
                this.product = productService.save(this.product);
                Popup.info("The item saved successfully.");
                setData(product);
            } catch(Exception e) {
                forms.alert(e);
            }
        }

        private void setData(Product product) {
            this.product = product;
            Model model = new Model(product);
            model.set("artifacts", new Collection(product.getArtifacts()));
            data.reset(model);
            forms.setState("edit");
            trigger.setState("edit");
        }

        private void delete() {
            productService.delete(product);
            close();
        }

        private void undo() {
            setData(product);
        }

    }
}
