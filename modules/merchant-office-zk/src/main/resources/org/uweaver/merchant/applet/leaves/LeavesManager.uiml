<?xml version = "1.0" encoding="utf-8"?>
<resources>
    <template name="dashboard">
        <div id="resultlist">
            <div sclass="trigger">
                <hbox width="100%" pack="end">
                    <button sclass="uw-button-default" label="My Leaves" iconSclass="z-icon-plus">
                        <custom-attributes action="create"/>
                    </button>
                    <button sclass="uw-button-attention" label="Edit" iconSclass="z-icon-pencil">
                        <custom-attributes action="edit"/>
                    </button>
                </hbox>
            </div>
            <separator/>
            <grid>
                <auxhead>
                    <auxheader label="Result List" colspan="5" rowspan="1"/>
                </auxhead>
                <columns menupopup="auto" sizable="true">
                    <column label="Name" width="200px" sort="auto">
                        <custom-attributes dataIndex="name"/>
                    </column>
                    <column label="Platforms" width="100px" sort="auto">
                        <custom-attributes dataIndex="platforms" code="platform"/>
                    </column>
                    <column label="Price" width="100px" sort="auto">
                        <custom-attributes dataIndex="price"/>
                    </column>
                    <column label="Release" width="120px" sort="auto">
                        <custom-attributes dataIndex="release"/>
                    </column>
                    <column label="Description" sort="auto">
                        <custom-attributes dataIndex="description"/>
                    </column>
                </columns>
            </grid>

        </div>
    </template>
    <template name="requestLeave">
        <div sclass="form">
            <grid sclass="uw-grid-layout">
                <columns>
                    <column width="10%"/>
                    <column/>
                    <column width="10%"/>
                    <column/>
                    <column width="10%"/>
                    <column/>
                </columns>
                <rows>
                    <row>
                        <label sclass="uw-label-input" value="ID">
                            <custom-attributes for="id"/>
                        </label>
                        <textbox name="id" hflex="1">
                            <custom-attributes for="id" readonlyWhen="edit" required="true"/>
                        </textbox>
                        <label sclass="uw-label-input" value="Availability">
                            <custom-attributes for="availability"/>
                        </label>
                        <checkbox name="availability" />
                    </row>
                    <row spans="1,3">
                        <label sclass="uw-label-input" value="Name">
                            <custom-attributes for="name" required="true"/>
                        </label>
                        <textbox name="name" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Price">
                            <custom-attributes for="price"/>
                        </label>
                        <decimalbox name="price" hflex="1"/>
                        <label sclass="uw-label-input" value="Release">
                            <custom-attributes for="release"/>
                        </label>
                        <datebox name="release" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Developer">
                            <custom-attributes for="manufacturer"/>
                        </label>
                        <combobox name="manufacturer" hflex="1"/>
                        <label sclass="uw-label-input" value="Publisher">
                            <custom-attributes for="distributor"/>
                        </label>
                        <combobox name="distributor" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Platforms">
                            <custom-attributes for="platforms"/>
                        </label>
                        <chosenbox name="platforms" hflex="1">
                            <custom-attributes code="platform"/>
                        </chosenbox>
                        <label sclass="uw-label-input" value="Modes">
                            <custom-attributes for="modes"/>
                        </label>
                        <hlayout>
                            <checkbox name="modes" label="Single-player" value="single-player" />
                            <checkbox name="modes" label="Multiplayer" value="multiplayer" />
                        </hlayout>
                        <label sclass="uw-label-input" value="Genres">
                            <custom-attributes for="genres"/>
                        </label>
                        <chosenbox name="genres" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Picture">
                            <custom-attributes for="picture"/>
                        </label>
                        <textbox name="picture" hflex="1">
                            <custom-attributes indicator="true"/>
                        </textbox>
                        <button sclass="uw-button-default" iconSclass="z-icon-plus" upload="true,maxsize=3000">
                            <custom-attributes name="picture"/>
                        </button>
                    </row>
                    <row spans="1,5">
                        <label sclass="uw-label-input" value="Description">
                            <custom-attributes for="description"/>
                        </label>
                        <textbox name="description" multiline="true" hflex="1" height="200px"/>
                    </row>
                </rows>
            </grid>
        </div>
        <div id="commandbar">
            <hbox width="100%" pack="end">
                <button sclass="uw-button-default" label="Sequence" iconSclass="z-icon-close">
                    <custom-attributes action="sequence"/>
                </button>
                <button sclass="uw-button-default" label="Flush" iconSclass="z-icon-close">
                    <custom-attributes action="flush"/>
                </button>
                <button sclass="uw-button-default" label="Close" iconSclass="z-icon-close">
                    <custom-attributes action="close"/>
                </button>
                <button sclass="uw-button-attention" label="Save" iconSclass="z-icon-save">
                    <custom-attributes action="save" confirmation="Save changes?: Do you want to save changes made to this item?"/>
                </button>
                <button sclass="uw-button-caution" label="Undo" iconSclass="z-icon-undo">
                    <custom-attributes action="undo" warning="Discard changes?: Are you sure you want to discard all changes to this item?"/>
                </button>
                <button sclass="uw-button-warning" label="Delete" iconSclass="z-icon-trash">
                    <custom-attributes action="delete" disableWhen="create" warning="Delete the item?: Are you sure you want to delete this item?"/>
                </button>
            </hbox>
        </div>
    </template>
    <template name="approveLeave">
        <div sclass="form">
            <grid sclass="uw-grid-layout">
                <columns>
                    <column width="10%"/>
                    <column/>
                    <column width="10%"/>
                    <column/>
                    <column width="10%"/>
                    <column/>
                </columns>
                <rows>
                    <row>
                        <label sclass="uw-label-input" value="ID">
                            <custom-attributes for="id"/>
                        </label>
                        <textbox name="id" hflex="1">
                            <custom-attributes for="id" readonlyWhen="edit" required="true"/>
                        </textbox>
                        <label sclass="uw-label-input" value="Availability">
                            <custom-attributes for="availability"/>
                        </label>
                        <checkbox name="availability" />
                    </row>
                    <row spans="1,3">
                        <label sclass="uw-label-input" value="Name">
                            <custom-attributes for="name" required="true"/>
                        </label>
                        <textbox name="name" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Price">
                            <custom-attributes for="price"/>
                        </label>
                        <decimalbox name="price" hflex="1"/>
                        <label sclass="uw-label-input" value="Release">
                            <custom-attributes for="release"/>
                        </label>
                        <datebox name="release" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Developer">
                            <custom-attributes for="manufacturer"/>
                        </label>
                        <combobox name="manufacturer" hflex="1"/>
                        <label sclass="uw-label-input" value="Publisher">
                            <custom-attributes for="distributor"/>
                        </label>
                        <combobox name="distributor" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Platforms">
                            <custom-attributes for="platforms"/>
                        </label>
                        <chosenbox name="platforms" hflex="1">
                            <custom-attributes code="platform"/>
                        </chosenbox>
                        <label sclass="uw-label-input" value="Modes">
                            <custom-attributes for="modes"/>
                        </label>
                        <hlayout>
                            <checkbox name="modes" label="Single-player" value="single-player" />
                            <checkbox name="modes" label="Multiplayer" value="multiplayer" />
                        </hlayout>
                        <label sclass="uw-label-input" value="Genres">
                            <custom-attributes for="genres"/>
                        </label>
                        <chosenbox name="genres" hflex="1"/>
                    </row>
                    <row>
                        <label sclass="uw-label-input" value="Picture">
                            <custom-attributes for="picture"/>
                        </label>
                        <textbox name="picture" hflex="1">
                            <custom-attributes indicator="true"/>
                        </textbox>
                        <button sclass="uw-button-default" iconSclass="z-icon-plus" upload="true,maxsize=3000">
                            <custom-attributes name="picture"/>
                        </button>
                    </row>
                    <row spans="1,5">
                        <label sclass="uw-label-input" value="Description">
                            <custom-attributes for="description"/>
                        </label>
                        <textbox name="description" multiline="true" hflex="1" height="200px"/>
                    </row>
                </rows>
            </grid>
        </div>
    </template>
    <template name="design">
        <div sclass="form">
            <grid sclass="uw-grid-layout">
                <columns>
                    <column/>
                    <column/>
                </columns>
                <rows>
                    <row>
                        <hbox>
                            <label sclass="uw-label-input" value="Engine">
                                <custom-attributes for="engine"/>
                            </label>
                            <textbox name="engine" width="200px"/>
                        </hbox>
                    </row>
                    <row>
                        <cell>
                            <grid>
                                <auxhead>
                                    <auxheader label="Artifacts" colspan="4">
                                        <hbox sclass="trigger" width="100%" pack="end">
                                            <button sclass="uw-button-default" iconSclass="z-icon-plus" upload="true,maxsize=3000">
                                                <custom-attributes action="add"/>
                                            </button>
                                            <button sclass="uw-button-caution" iconSclass="z-icon-minus">
                                                <custom-attributes action="remove"/>
                                            </button>
                                            <button sclass="uw-button-default" iconSclass="z-icon-pencil">
                                                <custom-attributes action="edit"/>
                                            </button>
                                        </hbox>
                                    </auxheader>
                                </auxhead>
                                <custom-attributes name="artifacts" input="true" mode="single" />
                                <columns menupopup="auto" sizable="true">
                                    <column label="Name" sort="auto">
                                        <custom-attributes dataIndex="name"/>
                                    </column>
                                    <column label="Type" width="60px" sort="auto">
                                        <custom-attributes dataIndex="type"/>
                                    </column>
                                    <column label="Size" width="60px" sort="auto">
                                        <custom-attributes dataIndex="size"/>
                                    </column>
                                    <column label="Date Created" width="120px" sort="auto">
                                        <custom-attributes dataIndex="createdOn"/>
                                    </column>
                                </columns>
                            </grid>
                        </cell>
                        <div id="artifactEditorPane">
                        </div>
                    </row>
                </rows>
            </grid>
        </div>
    </template>
    <template name="artifactEditor">
        <div sclass="form">
            <custom-attributes readonly="true"/>
            <groupbox>
                <grid sclass="uw-grid-layout">
                    <columns>
                        <column width="20%"/>
                        <column/>
                        <column width="20%"/>
                        <column/>
                    </columns>
                    <rows>
                        <row spans="1,3">
                            <label sclass="uw-label-input" value="Name"/>
                            <textbox name="name" hflex="1"/>
                        </row>
                        <row spans="1,3">
                            <label sclass="uw-label-input" value="Title"/>
                            <textbox name="title" hflex="1"/>
                        </row>
                        <row>
                            <label sclass="uw-label-input" value="Type"/>
                            <textbox name="type" hflex="1"/>
                        </row>
                        <row>
                            <label sclass="uw-label-input" value="Media Type"/>
                            <textbox name="mediaType" hflex="1"/>
                            <label sclass="uw-label-input" value="Size"/>
                            <intbox name="size" hflex="1"/>
                        </row>
                        <row>
                            <label sclass="uw-label-input" value="Created By"/>
                            <textbox name="createdBy" hflex="1"/>
                            <label sclass="uw-label-input" value="Date Created"/>
                            <datebox name="createdOn" hflex="1"/>
                        </row>
                        <row spans="1,3">
                            <label sclass="uw-label-input" value="Description"/>
                            <textbox name="description" multiline="true" hflex="1" height="100px"/>
                        </row>
                    </rows>
                </grid>
            </groupbox>
        </div>
    </template>
</resources>


