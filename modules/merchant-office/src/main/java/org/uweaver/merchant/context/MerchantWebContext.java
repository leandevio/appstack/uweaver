package org.uweaver.merchant.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.uweaver.core.app.annotation.AppContext;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.scheduling.AutoWiringSpringBeanJobFactory;
import org.uweaver.scheduling.Scheduler;
import org.uweaver.scheduling.SchedulerException;

import java.io.IOException;

@AppContext
@Configuration
public class MerchantWebContext implements ApplicationContextAware {
    private static Logger LOGGER = LogManager.getLogger(MerchantWebContext.class);
    private static ApplicationContext applicationContext = null;

    MerchantWebContext() {
        LOGGER.debug("Establishing the Merchant Office Web context ....");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public Scheduler scheduler() throws SchedulerException, IOException {
        Scheduler scheduler = new Scheduler();
        scheduler.setJobFactory(springBeanJobFactory());
        scheduler.start();
        return scheduler;
    }
}
