/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.controller.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Model;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.data.Sorter;
import org.uweaver.merchant.entity.Partner;
import org.uweaver.merchant.service.PartnerService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ManufacturerController obj = new ManufacturerController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController
@RequestMapping("reference/manufacturer")
public class ManufacturerController {

    private JSONParser parser = new JSONParser();

    @Autowired
    PartnerService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String search(@RequestParam(value="filters", required=false) String filters,
                                       @RequestParam(value="sorters", required=false) String sorters,
                                       @RequestParam(value="limit", required=false) Integer limit,
                                       @RequestParam(value="offset", required=false) Long offset,
                                       @RequestParam(value="conjunction", required = false) String conjunction,
                                       HttpServletResponse response) {
        Condition condition =new Condition(parser.readValue(filters,Filter[].class),conjunction);
        List<Partner> beans = service.search(condition,parser.readValue(sorters,Sorter[].class),limit,offset);

        Collection collection = new Collection();
        for(Partner bean : beans) {
            Model model = new Model();
            model.put("code", bean.getId());
            model.put("text", bean.getName());
            collection.add(model);
        }

        long count = service.count(condition);
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(collection);
    }
}
