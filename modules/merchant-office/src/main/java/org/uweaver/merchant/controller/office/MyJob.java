package org.uweaver.merchant.controller.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.uweaver.merchant.service.ProductService;
import org.uweaver.scheduling.Job;
import org.uweaver.scheduling.JobExecutionContext;
import org.uweaver.scheduling.JobExecutionException;

@Component
public class MyJob implements Job {

    private int counter = 0;

    @Autowired
    ProductService productService;

    @Override
    public void execute(JobExecutionContext executionContext) throws JobExecutionException {
        counter++;
        String key = executionContext.schedule().name();
        String msg = String.format("%s: A Simple Job was executed. %s", key, counter);
        System.out.println(msg);
    }
}
