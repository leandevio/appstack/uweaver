/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.controller.office;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Item;
import org.uweaver.core.util.Model;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.merchant.context.MerchantWebContext;
import org.uweaver.merchant.entity.Product;
import org.uweaver.merchant.service.PartnerService;
import org.uweaver.merchant.service.ProductService;
import org.uweaver.scheduling.CronScheduleBuilder;
import org.uweaver.scheduling.Schedule;
import org.uweaver.scheduling.Scheduler;
import org.uweaver.scheduling.SimpleScheduleBuilder;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;
import java.util.TimeZone;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProductController obj = new ProductController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@RestController
@RequestMapping("products")
public class ProductController {

    private static Logger LOGGER = LogManager.getLogger(ProductController.class);

    private JSONParser parser = new JSONParser();

    @Autowired
    ProductService productService;

    @Autowired
    PartnerService partnerService;

    @Autowired
    Scheduler scheduler;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String search(@RequestParam(value="filters", required=false) String filters,
                                       @RequestParam(value="sorters", required=false) String sorters,
                                       @RequestParam(value="limit", required=false) Integer limit,
                                       @RequestParam(value="offset", required=false) Long offset,
                                       @RequestParam(value="conjunction", required = false) String conjunction,
                                       HttpServletResponse response) throws SchedulerException, ParseException {
        Condition condition = new Condition(parser.readValue(filters,Filter[].class),conjunction);
//        Sorter sorter = new Sorter(parser.readValue(sorters,Sort[].class));
        List<Product> products = productService.search(condition, parser.readValue(sorters, org.uweaver.data.Sorter[].class), limit, offset);

        long count = productService.count(condition);

        LOGGER.debug(String.format("# of products: %d", count));

        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(products);
    }



    @RequestMapping(value="/{uuid}", method = RequestMethod.GET,produces="application/json;charset=UTF-8")
    public @ResponseBody String findOne(@PathVariable String uuid){
        return writeProductAsString(productService.findOne(uuid));
    }

    @RequestMapping(value="/{uuid}",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody String save(@PathVariable String uuid, @RequestParam(value="payload") String payload,
                                     HttpServletResponse response) {
        Product product = productService.findOne(uuid);

        Model model = parser.readValue(payload, Model.class);
        model.set("manufacturer", partnerService.findOne(model.getAsString("manufacturer")));

        Item item = new Item(product);
        item.populate(model);

        product = productService.save(product);

        return writeProductAsString(product);
    }

    @RequestMapping(value="/",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody String create(@RequestParam(value="payload") String payload,
                                     HttpServletResponse response) {
        Product product = new Product();

        Model model = parser.readValue(payload, Model.class);
        model.set("manufacturer", partnerService.findOne(model.getAsString("manufacturer")));

        Item item = new Item(product);
        item.populate(model);

        product = productService.save(product);

        return writeProductAsString(product);
    }


    private String writeProductAsString(Product product) {
        Model model = new Model(product);
        model.set("manufacturer", product.getManufacturer().getId());
        if(product.getDistributor()!=null) model.set("distributor", product.getDistributor().getId());
        return parser.writeValueAsString(model);
    }
}
