/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.controller.sales;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Collection;
import org.uweaver.merchant.service.sales.StatisticsService;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * OrderController obj = new OrderController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController
@RequestMapping("sales/statistics")
public class StatisticsController {
    private JSONParser parser = new JSONParser();

    @Autowired
    StatisticsService service;

    @RequestMapping(value="/salesByDate", method = RequestMethod.GET,produces="application/json;charset=UTF-8")
    public @ResponseBody String salesByDate(){
        Collection data = service.salesByDate();
        return parser.writeValueAsString(data);
    }


    @RequestMapping(value="/salesByProduct", method = RequestMethod.GET,produces="application/json;charset=UTF-8")
    public @ResponseBody String salesByProduct(){
        Collection data = service.salesByProduct();
        return parser.writeValueAsString(data);
    }



}
