package org.uweaver.merchant.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.event.server.ReadyEvent;
import org.uweaver.core.event.server.StopEvent;
import org.uweaver.core.observer.annotation.Subscribe;
import org.uweaver.merchant.controller.office.MyJob;
import org.uweaver.scheduling.CronScheduleBuilder;
import org.uweaver.scheduling.Schedule;
import org.uweaver.scheduling.Scheduler;
import org.uweaver.scheduling.SchedulerException;
import org.uweaver.web.context.WebApplicationContext;

import java.text.ParseException;
import java.util.TimeZone;

@AppListener
public class WebAppListener {
    private static Logger LOGGER = LoggerFactory.getLogger(WebAppListener.class);

    @Subscribe
    public void onReady(ReadyEvent event) throws ParseException, SchedulerException {

//        testScheduling();
        LOGGER.debug("Application is ready.");

    }

    @Subscribe
    public void onStop(StopEvent event) {
        LOGGER.debug("Application is going to stop.");
    }

    private void testScheduling() throws SchedulerException, ParseException {
        Scheduler scheduler = WebApplicationContext.getBean(Scheduler.class);
        Schedule schedule = CronScheduleBuilder.newSchedule("0/10 * * * * ?").forJob(MyJob.class)
                .withIdentity("mySchedule", "mygroup")
                .inTimeZone(TimeZone.getDefault())
                .usingData("greeting", "Hello")
                .build();

//        Schedule schedule = SimpleScheduleBuilder.newSchedule().forJob(MyJob.class)
//                .withIdentity("mySchedule", "group1")
//                .withIntervalInSeconds(10)
//                .repeatForever()
//                .startNow()
//                .build();
        scheduler.arrange(schedule);
    }
}
