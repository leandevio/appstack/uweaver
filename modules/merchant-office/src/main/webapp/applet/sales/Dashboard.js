/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/applet/Applet', 'uweaver/applet/Gadget',
        'uweaver/chart/CartesianChart', 'uweaver/chart/Histogram', 'uweaver/widget/Trigger', 'uweaver/widget/Pane',
        'uweaver/data/Collection', 'data/sales/Statistics',
        'uweaver/util/UIMLResourceBundle', 'text!./Dashboard.html'],
    function(_, Base, Gadget, CartesianChart, Histogram, Trigger, Pane, Collection, Statistics, UIMLResourceBundle, uiml) {
        var resources = new UIMLResourceBundle(uiml);
        var TITLE = "Sales Dashboard";

        function initialize(config) {
            Base.prototype.initialize.apply(this, arguments);

            this.$el.html(resources.get('main'));

            this.title(TITLE);

            this.queryAll("div.panel", Pane);

            this._statisticsByDate = new Statistics();
            buildSalesAmountByDateChart(this.$('[name=salesAmountByDateChart]'), this._statisticsByDate);
            buildSalesVolumeByDateChart(this.$('[name=salesVolumeByDateChart]'), this._statisticsByDate);

            this._statisticsByProduct = new Statistics();
            buildSalesAmountByProductChart(this.$('[name=salesAmountByProductChart]'), this._statisticsByProduct);
            buildSalesVolumeByProductChart(this.$('[name=salesVolumeByProductChart]'), this._statisticsByProduct);

            this.render();
        }

        function buildSalesAmountByDateChart(el, data) {
            var chart = new CartesianChart({
                el: el,
                data: data,
                //define the x and y-axis configuration.
                axes: [{
                    type: CartesianChart.AXIS.NUMERICAL,
                    position: CartesianChart.POSITION.LEFT,
                    unit: 'TWD'
                }, {
                    type: CartesianChart.AXIS.TIME,
                    position: CartesianChart.POSITION.BOTTOM,
                    title: 'Date'
                }],
                title: "Sales Statistics"
            });

            chart.addSeries({
                type: CartesianChart.SERIES.LINE,
                xField: 'orderDate',
                yField: 'amount',
                title: 'Sales Amount'
            });

            chart.addSeries({
                type: CartesianChart.SERIES.LINE,
                xField: 'orderDate',
                yField: 'cost',
                title: 'Cost'
            });
        }

        function buildSalesVolumeByDateChart(el, data) {
            var chart = new CartesianChart({
                el: el,
                data: data,
                //define the x and y-axis configuration.
                axes: [{
                    type: CartesianChart.AXIS.NUMERICAL,
                    position: CartesianChart.POSITION.LEFT
                }, {
                    type: CartesianChart.AXIS.TIME,
                    position: CartesianChart.POSITION.BOTTOM,
                    title: 'Date'
                }],
                title: "Sales Volume"
            });

            chart.addSeries({
                type: CartesianChart.SERIES.LINE,
                xField: 'orderDate',
                yField: 'volume',
                title: 'Sales Volume'
            });
        }

    function buildSalesAmountByProductChart(el, data) {
        var chart = new CartesianChart({
            el: el,
            data: data,
            //define the x and y-axis configuration.
            axes: [{
                type: CartesianChart.AXIS.NUMERICAL,
                position: CartesianChart.POSITION.LEFT,
                unit: 'TWD'
            }, {
                type: CartesianChart.AXIS.CATEGORICAL,
                position: CartesianChart.POSITION.BOTTOM,
                title: 'Product'
            }],
            title: "Sales Statistics"
        });


        chart.addSeries({
            type: CartesianChart.SERIES.BAR,
            xField: 'product',
            yField: 'cost',
            title: 'Cost'
        });

        chart.addSeries({
            type: CartesianChart.SERIES.BAR,
            xField: 'product',
            yField: 'amount',
            title: 'Sales Amount'
        });

        chart.addSeries({
            type: CartesianChart.SERIES.BAR,
            xField: 'product',
            yField: 'last',
            title: 'Previous'
        });

        chart.addSeries({
            type: CartesianChart.SERIES.BAR,
            xField: 'product',
            yField: 'forecast',
            title: 'Forecast'
        });
    }


    function buildSalesVolumeByProductChart(el, data) {
        var chart = new Histogram({
            el: el,
            data: data,
            //define the x and y-axis configuration.
            axes: [{
                type: CartesianChart.AXIS.NUMERICAL,
                position: CartesianChart.POSITION.LEFT
            }, {
                type: CartesianChart.AXIS.CATEGORICAL,
                position: CartesianChart.POSITION.BOTTOM,
                title: 'Product'
            }],
            title: "Sales Volume"
        });

        chart.addSeries({
            type: CartesianChart.SERIES.BAR,
            xField: 'product',
            yField: 'volume',
            title: 'Sales Volume'
        });
    }


    function render() {
        this._statisticsByDate.salesByDate();
        this._statisticsByProduct.salesByProduct();
    }

    var props = {
        _statisticsByDate: undefined,
        _statisticsByProduct: undefined
    };


    var Dashboard = declare(Base, {
        initialize: initialize,
        render: render
    }, props);

    return Dashboard;
});