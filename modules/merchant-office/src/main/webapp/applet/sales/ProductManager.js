/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/applet/TDIApplet', 'uweaver/applet/Gadget',
        'uweaver/widget/Trigger', 'uweaver/widget/Grid', 'uweaver/widget/Pagination', 'uweaver/widget/Form', 'uweaver/widget/Popup', 'uweaver/widget/ProgressBar',
        'data/Products', 'data/Product',
        'uweaver/util/UIMLResourceBundle', 'text!./ProductManager.html'],
    function(_, Base, Gadget, Trigger, Grid, Pagination, Form, Popup, ProgressBar, Products, Product, UIMLResourceBundle, uiml) {

    var resources = new UIMLResourceBundle(uiml);
    var TITLE = "Product Management";

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._finder = new Finder();
        var menu = new Trigger({el: resources.get('menu')});
        this._preferencesPopup = new Popup({modal: true}).render();
        this._preferences = new Preferences();
        this._preferencesPopup.content(this._preferences);

        this.listenTo(this._finder, 'edit', function(event) {
            edit(event.data());
        });

        this.listenTo(menu, 'browse', browse);
        this.listenTo(menu, 'create', create);
        this.listenTo(menu, 'preferences', preferences);
        this.listenTo(menu, 'determinate', determinate);
        this.listenTo(menu, 'indeterminate', indeterminate);
        this.listenTo(menu, 'dashboard', dashboard);

        this.title(TITLE);
        this.menu(menu);
    }

    function browse() {
        this.add(this._finder, {closable: true});
    }

    function determinate() {
        this.progress(function(value) {
            return value + Math.random() / 10;
        });
    }

    function indeterminate() {
        var progress = this.progress();
        _.delay(function() {
            progress.complete();
        }, 10*1000);
    }

    function dashboard() {
        this.navigate("applet/sales/Dashboard").then(function(applet) {
            alert("Navigate to Dashboard.");
        });
    }

    function create() {
        this.add(new Editor(), {closable: true});
    }

    function edit(id) {
        var editor = new Editor(id).render();
        this.add(editor, {closable: true});
    }

    // test combobox
    function preferences() {
        this._preferencesPopup.show();
    }

    var props = {
        _finder: undefined
    };

    var ProductManager = declare(Base, {
        initialize: initialize
    }, props);

    var Finder = declare(Gadget, {
        initialize: function(config) {
            Base.prototype.initialize.apply(this, arguments);

            this.$el.html(resources.get('finder'));

            this._products = new Products();

            this._grid = this.query('table[name=grid]', Grid);
            this._grid.data(this._products);
            this._grid.render();
            this._pagination = this.query('ul[name=pagination]', Pagination);
            this._pagination.data(this._products);

            this.title('Finder');
            this._criteria = this.query('form[name=criteria]', Form).render();
            this._trigger = this.query('form[name=criteria]', Trigger).render();
            this.listenTo(this._trigger, 'search', this.search);
            this._progressBar = this.query('div.progress', ProgressBar).render();
            this.render();
        },
        search: function() {
            this.indeterminate();
            this._products.fetch({
                limit: 5
            });
            // this._progressBar.indetermine();
        },
        determinate: function() {
            this.progress(function(value) {
                return value + Math.random() / 10;
            });
        },
        indeterminate: function() {
            var progress = this.progress();
            _.delay(function() {
                console.log("complete");
                progress.complete();
            }, 60*1000);
        },
        select: function(item) {
            this.trigger('select', item.id);
        }
    });

    var Editor = declare(Gadget, {
        initialize: function(id) {
            Base.prototype.initialize.apply(this, arguments);

            this._product = new Product();

            if(id) {
                this._product.id = id;
                this._product.fetch();
            }

            this.$el.html(resources.get('editor'));
            this._form = this.query('form', Form).render();
            this._form.data(this._product);

            this._trigger = this.query('form', Trigger).render();
            this.listenTo(this._trigger, 'save', function() {
                this.save();
            }, this);
            this.listenTo(this._trigger, 'reset', function() {
                this.reset();
            }, this);
            this.listenTo(this._product, 'change:name', function() {
                this.title(this._product.get('name'));
            }, this)
        },
        save: function() {
            var product = this._product;
            if(!this._form.validate()) {
                Popup.warn(message, "Validation error");
                return;
            }
            product.save().then(function() {
                Popup.info("The item was saved successfully.");
            }, function() {
                Popup.alert("Failed to save the item.");
            });
        },
        reset: function() {
            this._product.fetch();
        }
    });

    var Preferences = declare(Gadget, {
        initialize: function() {
            Base.prototype.initialize.apply(this, arguments);

            this.$el.html(resources.get('preferences'));

            var products = new Products();

            this._form = this.query('form', Form).render();
            var favorites = this._form.input('favorites');
            favorites.data(products);

            this._trigger = this.query('form', Trigger).render();
            this.listenTo(this._trigger, 'clear', function() {
                this.clear();
            }, this);
        },
        clear: function() {
            var data = this._form.data();
            data.set("favorites", null);
        }
    });

    return ProductManager;

});