/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.entity;

import org.hibernate.annotations.GenericGenerator;
import org.uweaver.entity.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Product obj = new Product();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Entity
public class Product {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uuid;
    /** 編號 */
    private String id;
    /** 名稱 */
    private String name;
    /** 價格 */
    private BigDecimal price;
    /** 發行日期 */
    private Date release;
    /** 可得性 */
    private Boolean availability = false;
    /** 說明 */
    private String description;
    /** 生產商 */
    private Partner manufacturer;
    /** 發行商 */
    private Partner distributor;
    /** 支援平台 */
    private List<String> platforms = new ArrayList<>();
    /** 模式 */
    private List<String> modes = new ArrayList<>();
    /** 類型 */
    private List<String> genres = new ArrayList<>();
    /** 引擎 */
    private String engine;
    /** 文件 */
    private List<Document> artifacts = new ArrayList<>();
    /** 圖片 */
    private Document picture;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter: {@link #id}.
     * @return {@link #id}.
     */
    @Column(length = 100, nullable = false, unique = true)
    @NotNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter: {@link #name}.
     * @return {@link #name}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    /**
     * Getter: {@link #price}.
     * @return {@link #price}.
     */
    @Column
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Getter: {@link #availability}.
     * @return {@link #availability}.
     */
    @Column(nullable = false)
    @NotNull
    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    /**
     * Getter: {@link #release}.
     * @return {@link #release}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "`release`")
    public Date getRelease() {
        return release;
    }

    public void setRelease(Date release) {
        this.release = release;
    }

    /**
     * Getter: {@link #description}.
     * @return {@link #description}.
     */
    @Column(length = 2000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter: {@link #manufacturer}.
     * @return {@link #manufacturer}.
     */
    @ManyToOne
    public Partner getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Partner manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * Getter: {@link #distributor}.
     * @return {@link #distributor}.
     */
    @ManyToOne
    public Partner getDistributor() {
        return distributor;
    }

    public void setDistributor(Partner distributor) {
        this.distributor = distributor;
    }

    /**
     * Getter: {@link #platforms}.
     * @return {@link #platforms}.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name="Product_Platforms",
            joinColumns=@JoinColumn(name="Owner")
    )
    @Column(name="Platform")
    public List<String> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }

    /**
     * Getter: {@link #modes}.
     * @return {@link #modes}.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name="Product_Modes",
            joinColumns=@JoinColumn(name="Owner")
    )
    @Column(name="Modes")
    public List<String> getModes() {
        return modes;
    }

    public void setModes(List<String> modes) {
        this.modes = modes;
    }

    /**
     * Getter: {@link #genres}.
     * @return {@link #genres}.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name="Product_Genres",
            joinColumns=@JoinColumn(name="Owner")
    )
    @Column(name="Genres")
    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    /**
     * Getter: {@link #engine}.
     * @return {@link #engine}.
     */
    @Column(length = 100)
    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * Getter: {@link #artifacts}.
     * @return {@link #artifacts}.
     */
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    public List<Document> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(List<Document> artifacts) {
        this.artifacts = artifacts;
    }

    @OneToOne(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    public Document getPicture() {
        return this.picture;
    }

    public void setPicture(Document picture) {
        this.picture = picture;
    }
}
