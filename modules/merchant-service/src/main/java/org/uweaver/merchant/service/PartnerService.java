/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.data.Condition;
import org.uweaver.data.DataInstaller;
import org.uweaver.data.Sorter;
import org.uweaver.merchant.entity.Partner;
import org.uweaver.merchant.repository.PartnerRepository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * GameService obj = new GameService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Service
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PartnerService {
    private DataInstaller dataInstaller = new DataInstaller();

    @Autowired
    PartnerRepository repository;

    @PostConstruct
    public void init(){
        if(repository.count()==0) {
            List<Partner> beans = Arrays.asList(dataInstaller.read(Partner[].class));
            repository.save(beans);
        }
    }

    public List<Partner> findAll() {
        return repository.findAll();
    }

    public Partner findOne(String id) {
        Partner bean = repository.findOne(id);
        if(bean==null) bean = repository.findById(id);
        return bean;
    }

    public Partner save(Partner product) {
        return repository.save(product);
    }

    public long count(Condition condition) {
        return repository.count(condition);
    }

    public List<Partner> search(Condition condition, Sorter[] sorters, Integer limit, Long offset) {
        List<Partner> beans = (List<Partner>) repository.search(condition,sorters,limit,offset);
        return beans;
    }
}
