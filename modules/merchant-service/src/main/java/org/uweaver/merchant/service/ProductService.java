/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.merchant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.core.exception.ConstraintConflictException;
import org.uweaver.core.exception.Violation;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.search.Searchable;
import org.uweaver.core.util.Model;
import org.uweaver.data.Condition;
import org.uweaver.data.DataInstaller;
import org.uweaver.merchant.entity.Partner;
import org.uweaver.merchant.entity.Product;
import org.uweaver.merchant.repository.ProductRepository;
import org.uweaver.search.Sorter;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProductService obj = new ProductService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Service
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ProductService implements Searchable<Product> {
    private DataInstaller dataInstaller = new DataInstaller();

    @Autowired
    ProductRepository productRepository;

    @Autowired
    PartnerService partnerService;

    @PostConstruct
    public void init(){
        if(productRepository.count()==0) {
            List<Product> beans = new ArrayList<>();
            List<Model> data = Arrays.asList(dataInstaller.readAs(Product.class, Model[].class));
            for(Model model : data) {
                Product bean = model.asBean(Product.class);
                Partner developer = partnerService.findOne((String) model.get("developer"));
                Partner publisher = partnerService.findOne((String) model.get("publisher"));
                bean.setManufacturer(developer);
                bean.setDistributor(publisher);
                beans.add(bean);
            }
            productRepository.save(beans);
        }
    }

    public List<Product> findAll() {
        return  productRepository.findAll();
    }

    public Product findOne(String id) {
        Product product = productRepository.findOne(id);
        product.getArtifacts().size();
        return product;
    }

    public Product save(Product product) {
//
//        String[] two = {"name", "price"};
//        Violation notEmpty = new Violation(two, "Not empty.");
//        String[] one = {"name"};
//        Violation notUnique = new Violation(one, "Not unique.");
//        ConstraintConflictException ex = new ConstraintConflictException(notUnique, notEmpty);
//
//        if(true) {
//            throw ex;
//        }

        Product saved;
        try {
           saved = productRepository.saveAndFlush(product);
        } catch(Exception e) {
            List<Violation> violations = new ArrayList<>();
            violations.add(new Violation("Not unique."));
            violations.add(new Violation("Not empty."));

            throw new ConstraintConflictException(violations);
        }
        return saved;
    }

    public void delete(Product product) {
        productRepository.delete(product);
    }

    public long count(Condition condition) {
        return productRepository.count(condition);
    }

    public List<Product> search(Condition condition, Sorter sorter, Pageable pageable) {
        List<Product> products = (List<Product>) productRepository.search(condition, sorter, pageable);
        return products;
    }

    public void delete(String uuid) {
        productRepository.delete(uuid);
    }

    @Override
    public List<Product> search(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        List<Product> products = (List<Product>) productRepository.search(predicates, sorter, pageable);
        return products;
    }

    public List<Product> search(Condition condition, Sorter sorter, Integer limit, Long offset) {
        List<Product> products = (List<Product>) productRepository.search(condition, sorter,limit, offset);
        return products;
    }

    public List<Product> search(Condition condition, org.uweaver.data.Sorter[] sorters, Integer limit, Long offset) {
        List<Product> products = (List<Product>) productRepository.search(condition, sorters,limit, offset);
        return products;
    }

    @Override
    public long count(List<Predicate> predicates) {
        return productRepository.count(predicates);
    }


}
