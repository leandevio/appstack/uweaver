/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Model;
import org.uweaver.entity.Code;
import org.uweaver.service.CodeService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * CodeController obj = new CodeController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController
@RequestMapping("code")
public class CodeController {
    private JSONParser parser = new JSONParser();

    @Autowired
    private CodeService service;

    /**
     * Find all codes by type
     *
     * @param type
     * @param response
     * @return
     */
    @RequestMapping(value="/{type}" ,method= RequestMethod.GET,produces =  "application/json;charset=UTF-8")
    public @ResponseBody String findAllByType(@PathVariable String type,
                                       HttpServletResponse response) {

        List<Code> codes = service.findAllByType(type);
        long count = codes.size();
        response.setIntHeader("Item-Count", (int) count);
        return parser.writeValueAsString(codes);
    }

    /**
     * Get a code
     *
     * @param type
     * @param code
     * @return
     */
    @RequestMapping(value = "/{type}/{code}" ,method=RequestMethod.GET, produces = "application/json;charset=UTF-8" )
    public @ResponseBody String get(@PathVariable String type,
                                     @PathVariable String code,
                                    HttpServletResponse response) {
        Code bean = service.findOneByTypeAndCode(type, code);
        if(bean==null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return parser.writeValueAsString(bean);
    }

    @RequestMapping(value = "/{type}/{code}/{what}" ,method=RequestMethod.GET, produces = "application/json;charset=UTF-8" )
    public @ResponseBody String get(@PathVariable String type,
                                    @PathVariable String code,@PathVariable String what) {
        Code bean = service.findOneByTypeAndCode(type, code);
        return parser.writeValueAsString(bean);
    }


    /**
     * Create or update a code
     *
     * @param type
     * @param code
     * @param payload
     * @return
     */
    @RequestMapping(value = "/{type}/{code}" ,method=RequestMethod.POST, produces = "application/json;charset=UTF-8" )
    public @ResponseBody String save(@PathVariable String type,
                                       @PathVariable String code,
                                       @RequestParam("payload") String payload) {
        Model model = parser.readValue(payload, Model.class);
        Code bean = service.findOneByTypeAndCode(type, code);
        if(bean==null) {
            bean = new Code();
            bean.setType(type);
            bean.setCode(code);
        }
        model.copyFrom(bean, type, code);
        bean = service.save(bean);

        return parser.writeValueAsString(bean);
    }

    /**
     * Delete a code
     *
     * @param type
     * @param code
     * @param response
     */
    @RequestMapping(value="/{type}/{code}" ,method=RequestMethod.DELETE,produces = "application/json;charset=UTF-8" )
    public void erase(@PathVariable String type,
                      @PathVariable String code,
                      HttpServletResponse response ) {
        service.delete(type,code);
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return;
    }
}
