package org.uweaver.web.context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.uweaver.core.app.annotation.AppContext;
import org.uweaver.core.util.Environment;
import org.uweaver.scheduling.AutoWiringSpringBeanJobFactory;
import org.uweaver.scheduling.Scheduler;
import org.uweaver.scheduling.SchedulerException;
import org.uweaver.web.helper.UrlPathResolver;

import java.io.IOException;


/**
 * Configuration of web application beans.
 *
 * Created by jasonlin on 5/1/14.
 */
@AppContext
@Configuration
public class WebApplicationContext implements ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebApplicationContext.class);

    private static ApplicationContext applicationContext = null;
    private static final Environment environment = Environment.getDefaultInstance();

    public WebApplicationContext() {
        LOGGER.debug("Establishing the web application context ......");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> type) {
        return applicationContext.getBean(type);
    }


    /**
     * According to MultipartResolver's javadoc, the bean name has to be "multipartResolver" to make the MultipartResolver to work.
     * @return
     */
    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver bean = new CommonsMultipartResolver();

        bean.setDefaultEncoding("UTF-8");

        int maxUploadSize = environment.fileMaxUploadSize();

        bean.setMaxUploadSize(maxUploadSize*1024);

        return bean;
    }

    /**
     * Configure RequestMapping.
     *
     * 當 Spring MVC 被配置使用 mvc:annotation-driven 時, RequestMappingHandlerMapping 會被登錄到 MVC 的 Context.
     * 這個元件會被用來處理 @RequestMapping 配置的 url routing.
     *
     * @return
     */

    @Bean
    public RequestMappingHandlerMapping handlerMapping() {
        RequestMappingHandlerMapping handlerMapping = new RequestMappingHandlerMapping();
        handlerMapping.setUseSuffixPatternMatch(false);
        handlerMapping.setUseTrailingSlashMatch(true);
//        handlerMapping.setUrlDecode(false);
        handlerMapping.setUrlPathHelper(new UrlPathResolver());
        return handlerMapping;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public Scheduler scheduler() throws SchedulerException, IOException {
        Scheduler scheduler = new Scheduler();
        scheduler.setJobFactory(springBeanJobFactory());
        scheduler.start();
        return scheduler;
    }

}
