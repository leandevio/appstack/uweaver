/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.web.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.uweaver.core.log.Logger;
import org.uweaver.core.log.LoggerManager;
import org.uweaver.web.handler.HandlerExceptionResolverComposite;
import org.uweaver.web.util.DateConverter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * The WebConfig should be loaded after the application context(WebContext).
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * WebConfig obj = new WebConfig();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Configuration
public class WebConfiguration extends WebMvcConfigurationSupport {
    private static final Logger LOGGER = LoggerManager.getLogger(WebConfiguration.class);

    public WebConfiguration() {
        LOGGER.debug("Configuring the MVC support ......");
    }

    @Bean
    public HandlerExceptionResolver handlerExceptionResolver() {
        HandlerExceptionResolverComposite bean = new HandlerExceptionResolverComposite();
        List<HandlerExceptionResolver> exceptionResolvers = new ArrayList<>();
        addDefaultHandlerExceptionResolvers(exceptionResolvers);
        bean.setExceptionResolvers(exceptionResolvers);
        return bean;
    }

    /**
     * resource mapping for html resources
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    }


    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new DateConverter());
    }


}
