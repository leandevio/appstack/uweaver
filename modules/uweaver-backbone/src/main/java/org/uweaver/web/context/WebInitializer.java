package org.uweaver.web.context;

/**
 *
 * 取代 web.xml, 配置與登錄 Servlet.
 *
 * The initializer(WebInitializer) load the application context(WebApplicationContext) and then the configuration(WebConfiguration).
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.uweaver.core.app.App;
import org.uweaver.core.app.annotation.AppContext;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.log.Logger;
import org.uweaver.core.log.LoggerManager;
import org.uweaver.core.reflection.Reflections;
import org.uweaver.core.util.Environment;
import org.uweaver.data.DataInstaller;
import org.uweaver.jpa.context.JpaContext;
import org.uweaver.servlet.dispatcher.ResourceDispatcher;
import org.uweaver.servlet.dispatcher.RestletDispatcher;
import org.uweaver.servlet.dispatcher.WebResourceDispatcher;
import org.uweaver.servlet.filter.*;
import org.uweaver.servlet.restlet.*;

import javax.servlet.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class WebInitializer implements WebApplicationInitializer {
    private static final Logger LOGGER = LoggerManager.getLogger(WebInitializer.class);
    Environment environment = Environment.getDefaultInstance();
    private static final Map<String, Class<? extends Restlet>> SYSRESTLETS = new HashMap<>();
    private static final Map<String, Class<? extends Restlet>> USERRESTLETS = new HashMap<>();

    static {
        SYSRESTLETS.put("environment", EnvironmentRestlet.class);
        SYSRESTLETS.put("i18n", I18nRestlet.class);
        USERRESTLETS.put("site", SiteRestlet.class);
        USERRESTLETS.put("applets", AppletRestlet.class);
        USERRESTLETS.put("user", UserRestlet.class);
    }

    public WebInitializer() {
        DataInstaller dataInstaller = new DataInstaller();
        dataInstaller.invalidate();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        LOGGER.info("Initializing the uweaver-backbone application ......");
        WebApplicationContext rootContext = createRootContext(servletContext);

        configureWeb(servletContext, rootContext);

        App app = App.getDefaultInstance();
        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(AppListener.class).list();

        try {
            for(Class type : types) {
                app.subscribe(type.newInstance());
            }
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
        LOGGER.info("The uweaver-backbone application has been initialized.");
        app.start();
    }



    /**
     * 配置 Application Context.
     * It's WebApplicationContext implementation that looks for Spring configuration in classes annotated with @Configuration annotation.
     *
     * 把 ContextLoaderListener 登錄到 ServletContext.
     *
     * @param servletContext
     * @throws ServletException
     */
    private WebApplicationContext createRootContext(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        List<String> basePackages = new ArrayList<>();
        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> contexts = reflections.createTypeQuery().annotatedWith(AppContext.class).list();
        Class appConfig = environment.appConfig();

        contexts.add(JpaContext.class);
        if(appConfig!=null) {
            basePackages.addAll(parseRepositoryPackages(appConfig));
            basePackages.addAll(parseServicePackages(appConfig));
            JpaContext.addPackagesToScan(parseEntityPackages(appConfig));
            contexts.add(appConfig);
        }

        basePackages.add("org.uweaver.repository");
        if(environment.property("repository.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("repository.package").split(",")));
        }

        basePackages.add("org.uweaver.service");
        if(environment.property("service.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("service.package").split(",")));
        }

        basePackages.add("org.uweaver.web.controller");
        basePackages.add("org.uweaver.rest");
        if(environment.property("controller.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("controller.package").split(",")));
        }

        if(basePackages.size()>0) {
            rootContext.scan(basePackages.toArray(new String[basePackages.size()]));
        }

        rootContext.register(contexts.toArray(new Class[contexts.size()]));

        rootContext.refresh();

        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.setInitParameter("defaultHtmlEscape", "true");

        return rootContext;
    }

    private List<String> parseEntityPackages(Class<?> config) {
        List<String> packages = new ArrayList<>();
        try {
            Method method = config.getMethod("entityPackages");
            packages.addAll((List<String>) method.invoke(null));
        } catch (NoSuchMethodException e) {
            ;
        } catch (IllegalAccessException e) {
            ;
        } catch (InvocationTargetException e) {
            ;
        }

        Import annotation = config.getAnnotation(Import.class);
        if(annotation!=null) {
            Class<?>[] dependencies = annotation.value();
            for(Class<?> dependency : dependencies) {
                packages.addAll(parseEntityPackages(dependency));
            }
        }

        return packages;
    }

    private List<String> parseRepositoryPackages(Class<?> config) {
        List<String> packages = new ArrayList<>();
        try {
            Method method = config.getMethod("repositoryPackages");
            packages.addAll((List<String>) method.invoke(null));
        } catch (NoSuchMethodException e) {
            ;
        } catch (IllegalAccessException e) {
            ;
        } catch (InvocationTargetException e) {
            ;
        }

        Import annotation = config.getAnnotation(Import.class);
        if(annotation!=null) {
            Class<?>[] dependencies = annotation.value();
            for(Class<?> dependency : dependencies) {
                packages.addAll(parseRepositoryPackages(dependency));
            }
        }

        return packages;
    }

    private List<String> parseServicePackages(Class<?> config) {
        List<String> packages = new ArrayList<>();
        try {
            Method method = config.getMethod("servicePackages");
            packages.addAll((List<String>) method.invoke(null));
        } catch (NoSuchMethodException e) {
            ;
        } catch (IllegalAccessException e) {
            ;
        } catch (InvocationTargetException e) {
            ;
        }

        Import annotation = config.getAnnotation(Import.class);
        if(annotation!=null) {
            Class<?>[] dependencies = annotation.value();
            for(Class<?> dependency : dependencies) {
                packages.addAll(parseServicePackages(dependency));
            }
        }

        return packages;
    }

    /**
     * 配置 Servlet Context 與登錄
     *
     * @param servletContext
     * @param rootContext
     */
    private void configureWeb(ServletContext servletContext, WebApplicationContext rootContext) {
        AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();

        webContext.register(WebConfiguration.class);
        webContext.setParent(rootContext);

        ServletRegistration.Dynamic appServlet = servletContext.addServlet(
                "webapp", new DispatcherServlet(webContext));
        appServlet.setLoadOnStartup(2);
        Set<String> mappingConflicts = appServlet.addMapping("/");

        FilterRegistration.Dynamic openEntityManagerInViewFilter = servletContext.addFilter("OpenEntityManagerInViewFilter", new OpenEntityManagerInViewFilter());
        openEntityManagerInViewFilter.addMappingForUrlPatterns(null, true, "/*");

        if (!mappingConflicts.isEmpty()) {
            for (String s : mappingConflicts) {
                LOGGER.error("Mapping conflict: " + s);
            }
            throw new IllegalStateException(
                    "'webapp' cannot be mapped to '/'");
        }

        try {
            if(environment.propertyAsBoolean("audit.enabled", false)) addAuditFitler(servletContext);
            addServerController(servletContext);
            addAuthenticationFilter(servletContext);
            addAuthorizationFilter(servletContext);
            addHttpTuner(servletContext);
            addWebResourceDispatcher(servletContext);
            addResourceDispatcher(servletContext);
            addRestletDispatcher(servletContext);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }

    }

    private void addAuditFitler(ServletContext servletContext) {
        AuditFilter auditFilter = new AuditFilter();
        this.addFilter(servletContext, "AuditFilter", auditFilter, "/*");
    }

    private void addServerController(ServletContext servletContext) {
        ServerController serverController = new ServerController();
        serverController.setEnvironment(environment);
        this.addFilter(servletContext, "ServerController", serverController, "/*");
    }

    private void addAuthenticationFilter(ServletContext servletContext) throws Exception {
        String className = environment.property("filter.authentication", "org.uweaver.servlet.filter.AuthenticationFilter");
        Class clazz = Class.forName(className);
        Filter authenticationFilter = (Filter) clazz.newInstance();
        this.addFilter(servletContext, "AuthenticationFilter", authenticationFilter, "/*");
    }

    private void addAuthorizationFilter(ServletContext servletContext) {
        if(!environment.propertyAsBoolean("authorization.enable", true)) return;
        AuthorizationFilter authorizationFilter = new AuthorizationFilter();
        this.addFilter(servletContext, "AuthorizationFilter", authorizationFilter, "/*");
    }

    private void addHttpTuner(ServletContext servletContext) {
        HttpTuner httpTuner = new HttpTuner();
        this.addFilter(servletContext, "HttpTuner", httpTuner, "/*");
    }

    private void addResourceDispatcher(ServletContext servletContext) {
        String pathSpec = "/*";

        ResourceDispatcher dispatcher = new ResourceDispatcher();
//        dispatcher.addResourceHandler("/uweaver/**/*.{js,html,css,json,properties,woff,woff2,ttf,png}", "/");
//        dispatcher.addResourceHandler("/nls/**/*.{js,html,css,json,properties,woff,woff2,ttf,png}", "/");
        dispatcher.addResourceHandler("/**/*.{js,html,css,json,properties,woff,woff2,ttf,png}", "/");
        this.addFilter(servletContext, "ResourceDispatcher", dispatcher, pathSpec);
    }

    private void addWebResourceDispatcher(ServletContext servletContext) {
        String pathSpec = "/*";

        WebResourceDispatcher dispatcher = new WebResourceDispatcher();
        dispatcher.addResourceHandler("/**/*.{js,html,css,json,properties,woff,woff2,ttf,png}", "/");
        this.addFilter(servletContext, "WebResourceDispatcher", dispatcher, pathSpec);
    }

    private void addRestletDispatcher(ServletContext servletContext) {
        String pathSpec = "/*";
        List<String> disabled = Arrays.asList(environment.property("restlet.disable", "").split(","));

        RestletDispatcher dispatcher = new RestletDispatcher();
        String contextPath = environment.property("restlet.contextPath");
        if(contextPath!=null) {
            pathSpec = "/" + contextPath + pathSpec;
        }
        this.addFilter(servletContext, "RestletDispatcher", dispatcher, pathSpec);

        dispatcher.addRestlet(new EnvironmentRestlet(environment), "/environment");
        dispatcher.addRestlet(new I18nRestlet(), "/i18n");
        dispatcher.addRestlet(new SessionRestlet(), "/session");

        for(String uri : USERRESTLETS.keySet()) {
            if(disabled.contains(uri)) continue;
            Restlet restlet = createRestlet(USERRESTLETS.get(uri));
            String restletPath = "/" + uri;
            dispatcher.addRestlet(restlet, restletPath);
        }
    }

    private Restlet createRestlet(Class<? extends Restlet> restletClass) {
        Restlet restlet;
        try {
            restlet = restletClass.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }

        return restlet;
    }

    private void addFilter(ServletContext servletContext, String name, Filter filter, String pathSpec) {
        FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(name, filter);
        filterRegistration.addMappingForUrlPatterns(null, true, pathSpec);
    }

}

