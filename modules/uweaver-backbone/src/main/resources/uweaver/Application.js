/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/Events', 'uweaver/environment', 'uweaver/Engine', 'uweaver/Deferred', 'uweaver/Logger'], function(Events, environment, Engine, Deferred, Logger) {
    var Base = Events;
    var LOGGER = new Logger("Application");

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._engine = Engine.getDefaultInstance();
    }

    function start() {
        var deferred = new Deferred();
        var context = this;

        var module = environment.get("wm").uri;

        require([module], function(WM) {
            var wm = new WM();
            wm.render();
            context._wm = wm;

            LOGGER.debug("Application started.");
            deferred.resolve(wm);
        });
        return deferred.promise();
    }

    function wm() {
        return this._wm;
    }

    function engine() {
        return this._engine;
    }

    var props = {
        _wm: undefined,
        _engine: undefined
    };

    var Application = declare(Base, {
        initialize: initialize,
        start: start,
        wm: wm,
        engine: engine
    }, props);

    var application = new Application();

    function getDefaultInstance() {
        return application;
    }

    return {
        getDefaultInstance: getDefaultInstance
    };
});