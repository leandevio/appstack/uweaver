/**
 * Created by jasonlin on 7/20/14.
 */
define(['underscore', 'jquery', 'uweaver/lang'], function (_, $, lang) {

    var Deferred = lang.declare(null, {

        initialize: initialize,

        resolve: resolve,
        reject: reject,
        notify: notify,

        promise: promise
    });

    function initialize() {
        this._deferred = $.Deferred();
        this._promise = new Promise(this._deferred.promise());
    }

    function resolve() {
        return this._deferred.resolve.apply(this, arguments);
    }

    function reject() {
        return this._deferred.reject.apply(this, arguments);
    }

    function notify() {
        return this._deferred.notify.apply(this, arguments);
    }

    function promise() {
        return this._promise;
    }

    function bind(callbacks, context) {
        if(!context) return callbacks;
        if(!callbacks) return callbacks;

        var fn = [];
        if(_.isArray(callbacks)) {
            _.each(callbacks, function(callback) {
                fn.push(_.bind(callback, context));
            });
        } else {
            fn.push(_.bind(callbacks, context));
        }
        return fn;
    }

    var Promise = lang.declare(null, {

        initialize: function (promise) {
            this._promise = promise;
        },

        then: function (doneCallback, failCallback, progressCallback, context) {
            var index = _.findLastIndex(arguments, function(fn) {
                return _.isFunction(fn);
            });
            context = (index>=0) ? arguments[index+1] : undefined;

            _.isFunction(doneCallback) || (doneCallback = undefined);
            _.isFunction(failCallback) || (failCallback = undefined);
            _.isFunction(progressCallback) || (progressCallback = undefined);

            this.done(doneCallback, context);
            this.fail(failCallback, context);
            this.progress(progressCallback, context);
            return this;
        },

        always: function (callback, context) {
            this._promise.always(bind(callback, context));
            return this;
        },

        done: function (callback, context) {
            this._promise.done(bind(callback, context));
            return this;
        },

        fail: function (callback, context) {
            this._promise.fail(bind(callback, context));
            return this;
        },

        progress: function (callback, context) {
            this._promise.progress(bind(callback, context));
            return this;
        },

        state: function () {
            return this._promise.state();
        },

        isResolved: function () {
            return this._promise.isResolved();
        },

        isRejected: function () {
            return this._promise.isRejected();
        }

    });

    return Deferred;
});