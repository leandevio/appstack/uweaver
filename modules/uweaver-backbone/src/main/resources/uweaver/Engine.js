/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/string', 'uweaver/Events',  'uweaver/Deferred',
    'uweaver/Logger'], function(_, string, Events, Deferred, Logger) {
    var Base = Events;
    var LOGGER = new Logger("Engine");

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._processes = {};
    }

    function open(applet) {
        var deferred = new Deferred();
        var module = _.isString(applet) ? applet : applet.get('uri');
        var context = this;

        require([module], function(Applet) {
            var isNew = false;
            var process = _.find(context._processes, function(process){
                if(process instanceof Applet) {
                    return process;
                }
            });

            if(!process) {
                try {
                    process = new Applet();
                } catch(err) {
                    LOGGER.error(err);
                    return;
                }

                process.render();

                context.listenTo(process, 'destroy', _.bind(onAppletDestroy, context));

                context._processes[process.pid()] = process;
                isNew = true;
            }

            deferred.resolve(process);

            isNew && context.trigger('start', {
                data: process,
                context: context,
                source: context
            });

            context.trigger('open', {
                data: process,
                context: context,
                source: context
            });

        });

        return deferred.promise();
    }

    function close(pid) {
        _.isString(pid) || (pid = pid.pid());
        var process = this.process(pid);
        remove.call(this, pid);

        this.trigger('close', {
            data: process,
            context: this,
            source: this
        });

        process.destroy();
    }

    function remove(pid) {
        delete this._processes[pid];
    }

    function processes() {
        return this._processes;
    }

    function process(pid) {
        return this._processes[pid];
    }

    function onAppletDestroy(event) {
        var pid = event.context.pid();
        remove.call(this, pid);
    }

    var props = {
        _processes: undefined
    };

    var Engine = declare(Base, {
        initialize: initialize,
        open: open,
        close: close,
        processes: processes,
        process: process
    }, props);

    var engine = new Engine();

    function getDefaultInstance() {
        return engine;
    }

    return {
        getDefaultInstance: getDefaultInstance
    };

});