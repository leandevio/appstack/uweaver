/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore',  'uweaver/lang', 'uweaver/constant',
    'uweaver/exception/UnsupportedTypeException'],
    function(_, lang, constant,
             UnsupportedTypeException) {

    var declare = lang.declare;

    /**
     * A class representing an dom element.
     *
     *
     * @constructor XElement
     * @author  Jason Lin
     * @since   1.0
     * @param {Element|String} element - the dom element or a tag name to create the dom element.
     */
    function initialize(element, options) {
        options || (options = {});

        if(_.isString(element)) {
            this._el = createElement(element, options.ns);
        } else if(element instanceof Element) {
            this._el = element;
        } else {
            throw new UnsupportedTypeException({
                type: element ? typeof element : element,
                supportedTypes: ['String', 'Element']
            });
        }
    }

    /**
     * Get or set the value of a style property.
     *
     * @memberof XElement#
     * @param {String} name - The css property name.
     * @param {Object} [value] - A value to set for the property.
     * @returns {Object} css style.
     */
    function style(name, value) {
        if(!_.isObject(name) && value===undefined) return this._el.style[name];

        var settings;
        var element = this._el;

        if(_.isObject(name)) {
            settings = name;
        } else {
            settings = {};
            settings[name] = value;
        }
        _.each(settings, function(value, name) {
            element.style[name] = value;
        });
        return this;
    }

    function getComputedStyle(name) {
        var style = window.getComputedStyle(this._el, null);

        if(arguments.length===0) return style;

        return style.getPropertyValue(name);
    }

    /**
     * Get or set the value of an attribute.
     *
     * @memberof XElement#
     * @param {String} name - The name of the attribute.
     * @param {Object} [value] - A value to set for the attribute.
     * @returns {*} the value of the attribute.
     */
    function attr(name, value) {
        if(!_.isObject(name) && value===undefined) return this._el.getAttribute(name);

        var settings;
        var element = this._el;

        if(_.isObject(name)) {
            settings = name;
        } else {
            settings = {};
            settings[name] = value;
        }
        _.each(settings, function(value, name) {
            if(value===null) {
                element.removeAttribute(name);
            } else {
                element.setAttribute(name, value);
            }
        });
        return this;
    }

    function hasAttr(name) {
        var element = this._el;
        return element.hasAttribute(name);
    }

    /**
     * Get or set the value of a property.
     *
     * @memberof XElement#
     * @param {String} name - The name of the property.
     * @param {Object} [value] - A value to set for the property.
     * @returns {*} The value of the property.
     */
    function prop(name, value) {
        if(!_.isObject(name) && value===undefined) return this._el[name];

        var settings;
        var element = this._el;

        if(_.isObject(name)) {
            settings = name;
        } else {
            settings = {};
            settings[name] = value;
        }
        _.each(settings, function(value, name) {
            element[name] = value;
        });
        return this;
    }

    /**
     * Add the specified class(es) to the element.
     *
     * @memberof XElement#
     * @param {String} classNames - One or more space-separated classes to be added.
     * @returns {XElement} this
     */
    function addClass(classNames) {
        var names = classNames.split(' ');
        // to support SVG in IE
        if(this._el.classList) {
            this._el.classList.add(names);
        } else {
            this._el.className+= (' ' + names);
        }

        return this;
    }

    /**
     * Remove the specified class(es) from the element.
     *
     * @memberof XElement#
     * @param {String} classNames - One or more space-separated classes to be removed.
     * @returns {XElement} this
     */
    function removeClass(classNames) {
        var names = classNames.split(' ');
        // to support SVG in IE
        if(this._el.classList) {
            this._el.classList.remove(names);
        } else {
            _.each(names, function(name){
                this._el.className = this._el.className.replace(' ' + name + ' ', "")
            }, this);
        }

        return this;
    }

    /**
     * Determine whether the element are assigned the given class.
     *
     * @memberof XElement#
     * @param {String} className - The class name to search for.
     * @returns {Boolean}
     */
    function hasClass(className) {
        var result;
        // to support SVG in IE
        if(this._el.classList) {
            result = this._el.classList.contains(className);
        } else {
            var pattern = new RegExp(' ' + className + ' ');
            result = pattern.test(this._el.className);
        }
        return result;
    }

    /**
     * Display the element.
     *
     * @memberof XElement#
     * @param {Object} [options] - A map of additional options to pass to the method.
     */
    function show(options) {
        options || (options = {});

        this.style({
            'visibility': '',
            'display': ''
        });
        return this;
    }

    /**
     * Hide the element.
     *
     * @memberof XElement#
     * @param {Object} [options] - A map of additional options to pass to the method.
     */
    function hide(options) {
        options || (options = {});

        this.style('visibility', 'hidden');

        options.collapse && this.style('display', 'none');

        return this;
    }

    /**
     * Display or hide the element.
     *
     * @memberof XElement#
     * @param {Object} [options] - A map of additional options to pass to the method.
     */
    function toggle(options) {
        this.isVisible() ? this.hide(options) : this.show(options);
        return this;
    }

    function isVisible() {
        return !!(this._el.offsetWidth || this._el.offsetHeight || this._el.getClientRects().length);
    }

    /**
     * A helper method to find the first dom element matched the selector.
     *
     * @memberof XElement#
     * @param {String} selector - css selector.
     * @returns {XElement}
     */
    function query(selector) {
        selector = selector.replace(/^#[0-9]{1}/, "#\\3" + selector.charAt(1) + " ");
        var element = this._el.querySelector(selector);
        return element ? new XElement(element) : null;
    }

    /**
     * A helper method to find the dom elements matched the selector.
     *
     * @memberof XElement#
     * @param {String} selector - css selector.
     * @returns {XElement[]}
     */
    function queryAll(selector) {
        selector = selector.replace(/^#[0-9]{1}/, "#\\3" + selector.charAt(1) + " ");
        var xelements = [];

        _.each(this._el.querySelectorAll(selector), function(element){
            xelements.push(new XElement(element));
        });

        return xelements;
    }

    /**
     * The element's CSS height in pixels.
     *
     * @param value
     * @returns {*}
     */
    function height(value) {
        if(arguments.length===0) {
            var height;
            if(this._el instanceof SVGElement) {
                if(this._el.height instanceof SVGAnimatedLength && this._el.height.animVal.unitType===SVGLength.SVG_LENGTHTYPE_PX) {
                    height = this._el.height.animVal.valueInSpecifiedUnits;
                }
            } else {
                height = this._el.offsetHeight;
            }
            return _.isNumber(height) ? height : this._el.getBoundingClientRect().height;
        }

        var height = _.isNumber(value) ? value + 'px' : value;

        this.style('height', height);
        this.attr('height', height);

        return this;
    }

    /**
     * The inner height of an element in pixels.
     *
     * the innerHeight can be calculated the element's CSS height without padding, border and the element's horizontal
     * scrollbar (if present, if rendered).
     *
     * @param value
     * @returns {*}
     */
    function innerHeight(value) {
        var style = this.getComputedStyle();
        var padding = parseNumber(style.getPropertyValue('padding-top')) + parseNumber(style.getPropertyValue('padding-bottom'));
        if(arguments.length===0) return this._el.clientHeight - padding;

        var border = parseNumber(style.getPropertyValue('border-top-width')) + parseNumber(style.getPropertyValue('border-bottom-width'));
        this.height(value + border + padding);

        return this;
    }

    function parseNumber(any) {
        var n = parseFloat(any);
        return _.isNumber(n) ? n : 0;
    }

    /**
     * The element's CSS width in pixels.
     *
     * @param value
     * @returns {*}
     */
    function width(value) {
        if(value===undefined) {
            var width;
            if(this._el instanceof SVGElement) {
                if(this._el.width instanceof SVGAnimatedLength && this._el.width.animVal.unitType===SVGLength.SVG_LENGTHTYPE_PX) {
                    width = this._el.width.animVal.valueInSpecifiedUnits;
                }
            } else {
                width = this._el.offsetWidth;
            }
            return _.isNumber(width) ? width : this._el.getBoundingClientRect().width;
        }

        var width = _.isNumber(value) ? value + 'px' : value;

        this.style('width', width);
        this.attr('width', width);

        return this;
    }


     /**
     * The inner width of an element in pixels.
     *
     * the innerWidth can be calculated the element's CSS width without padding, border and the element's vertical
     * scrollbar (if present, if rendered).
     *
     * @param value
     * @returns {*}
     */
    function innerWidth(value) {
        var style = this.getComputedStyle();
        var padding = parseNumber(style.getPropertyValue('padding-left')) + parseNumber(style.getPropertyValue('padding-right'));

        if(arguments.length===0) return this._el.clientWidth - padding;
        var border = parseFloat(style.getPropertyValue('border-left-width')) + parseFloat(style.getPropertyValue('border-right-width'));
        this.width(value + border + padding);
        return this;
    }

    function padding(position, value) {
        var cssProp = 'padding';

        if(arguments.length>=2) {
            return this.setPadding.apply(arguments);
        } else if(arguments.length===1) {
            if(constant.POSITION.validate(position)) {
                cssProp = cssProp + '-' + position.toLowerCase();
            } else {
                return this.setPadding.apply(arguments);
            }
        }

        var style = this.getComputedStyle();

        return parseNumber(style.getPropertyValue(cssProp));
    }

    function setPadding(position, value) {
        var cssProp = 'padding';
        var cssValue = value;

        if(arguments.length>=2) {
            cssProp = cssProp + '-' + position.toLowerCase();
        } else {
            cssValue = arguments[0];
        }

        this.style(cssProp, _.isNumber(cssValue) ? cssValue + 'px' : cssValue);

        return this;
    }

    function borderWidth(position, value) {
        var cssProp = 'border-' + position + '-width';

        if(arguments.length===1) {
            var style = this.getComputedStyle();
            return parseNumber(style.getPropertyValue(cssProp));
        }

        this.style(cssProp, _.isNumber(value) ? value + 'px' : value);

        return this;
    }

    function left(value) {
        if(value===undefined) return this._el.offsetLeft;

        this.style('left', value + 'px');
        return this;
    }

    function top(value) {
        if(value===undefined) return this._el.offsetTop;

        this.style('top', value + 'px');
        return this;
    }

    function right(value) {
        this.style('right', value + 'px');
        return this;
    }

    function bottom(value) {
        this.style('bottom', value + 'px');
        return this;
    }

    function move(x, y) {
        if(this._el instanceof SVGSVGElement) {
            this.attr({x: x + 'px', y: y + 'px'});
        } else {
            this.style('transform', 'translate(' + x + 'px,' + y + 'px)');
        }
        return this;
    }

    function node() {
        return this._el;
    }

    /**
     * Get or set the HTML content (inner HTML) of an element.
     *
     * @memberof XElement#
     * @param {String} [value] - Specifies the HTML content of the element
     * @returns {String|XElement} The HTML content or this.
     */
    function html(value) {
        if(value===undefined) return this._el.innerHTML;
        this._el.innerHTML = value;
        return this;
    }

    /**
     * Get or set the textual content of the element.
     *
     * If you set the textContent property, any child nodes are removed and replaced by a single Text node containing the specified string.
     *
     * @memberof XElement#
     * @param {String} [value] - Specifies the textual content of an element.
     * @returns {String|XElement} The textual content or this.
     */
    function text(value) {
        if(value===undefined) return this._el.innerText;
        this._el.innerText = value;
        return this;
    }

    function append(any, ns) {
        var xelement;
        if(any instanceof XElement) {
            xelement = any;
        } else {
            xelement = new XElement(any, {ns: ns});
        }

        this._el.appendChild(xelement.node());

        return xelement;
    }

    function before(any, ns) {
        var parent = this._el.parentNode;
        var xelement;
        if(any instanceof XElement) {
            xelement = any;
        } else {
            xelement = new XElement(any, {ns: ns});
        }
        parent.insertBefore(xelement.node(), this._el);
        return xelement;
    }

    function remove() {
        var parent = this._el.parentNode;
        parent && parent.removeChild(this._el);
        return this;
    }

    function createElement(tagName, ns) {
        var element;
        if(ns===XElement.ns.SVG) {
            element = document.createElementNS("http://www.w3.org/2000/svg", tagName);
        } else if(ns===XElement.ns.HTML) {
            element = document.createElement(tagName);
        } else  {
            element = document.createElement(tagName);
        }
        return element;
    }

    function empty() {
        while (this._el.hasChildNodes()) {
            this._el.removeChild(this._el.firstChild);
        }
        return this;
    }

    function addEventListener(event, callback, useCapture) {
        this._el.addEventListener(event, callback, useCapture);
    }

    function el() {
        return this._el;
    }

    var props = {};

    var XElement = declare(null, {
        initialize: initialize,
        el: el,
        attr: attr,
        hasAttr: hasAttr,
        prop: prop,
        addClass: addClass,
        removeClass: removeClass,
        style: style,
        getComputedStyle: getComputedStyle,
        query: query,
        queryAll: queryAll,
        node: node,
        append: append,
        remove: remove,
        before: before,
        html: html,
        text: text,
        empty: empty,

        height: height,
        innerHeight: innerHeight,
        width: width,
        innerWidth: innerWidth,
        padding: padding,
        setPadding: setPadding,
        borderWidth: borderWidth,

        show: show,
        hide: hide,
        toggle: toggle,
        move: move,

        addEventListener: addEventListener,

        hasClass: hasClass,
        isVisible: isVisible
    }, props);

    XElement.ns = {
        SVG: "SVG",
        HTML: "HTML"
    };

    return XElement;

});