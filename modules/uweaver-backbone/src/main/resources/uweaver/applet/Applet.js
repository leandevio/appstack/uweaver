/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/widget/Widget', 'uweaver/widget/ProgressBar'], function(_, lang, Widget, ProgressBar) {

    var declare = lang.declare;
    var Base = Widget;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
    }

    function pid() {
        return this.oid();
    }

    function add(gadget) {
        this.remove(this._gadget);
        gadget.attach(this.el);
        this._gadget = gadget;
    }

    function remove(gadget) {
        if(gadget) {
            gadget.detach();
        }
    }

    function select(gadget, options) {
        this.add(gadget);
    }

    function title(text) {
        if(!text) return this._title;
        this._title = text;
    }

    function menu(trigger) {
        if(!trigger) return this._menu;
        this._menu = trigger;
    }

    var props = {
        _gadget: undefined,
        _menu: undefined,
        _title: ""
    };

    function focus() {}

    function blur() {}

    function progress(measure, options) {
        options || (options = {});
        var progressBar = new ProgressBar();

        progressBar.complete(0.5);
        this.popup(progressBar);

    }

    function popup(element) {
        var $pane = $('div');
        $pane.css({
            position: 'absolute'
        });
        $pane.append(element);

        _.delay(function() {

        }, 500)

    }

    var Applet = declare(Base, {
        initialize: initialize,
        pid: pid,
        add: add,
        remove: remove,
        select: select,
        title: title,
        menu: menu,
        focus: focus,
        blur: blur,
        popup: popup,
        progress: progress
    }, props);

    return Applet;
});