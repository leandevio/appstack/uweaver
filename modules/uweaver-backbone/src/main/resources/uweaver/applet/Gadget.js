define(['underscore', 'jquery', 'uweaver/lang', 'uweaver/widget/Widget', 'uweaver/widget/Triggers', 'uweaver/widget/ProgressBar'
    ], function(_, $, lang, Widget, Triggers, ProgressBar) {

    var declare = lang.declare;
    var Base = Widget;

    /**
     * A gadget is the component of the applet.
     *
     * ##### Usage
     *     var applet = new TDIApplet({
     *         el: $('#applet')
     *     });
     *     applet.render();
     *
     *     var gadget = new Gadget({
     *         tpl: "<h3>Preferences</h3><p>Here are your personal preferences ...</p>"
     *         title: "Preferences"
     *     }).render();
     *
     *     applet.add(gadget);
     *
     * ##### Events:
     * + 'change:menu' - [menu()]{@link Gadget#menu}
     * + 'change:title' - [menu()]{@link Gadget#title}
     * + 'change:badge' - [menu()]{@link Gadget#badge}
     *
     * @constructor Gadget
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the gadget.
     * ##### Options
     * - tpl(String): A html string to layout & construct the DOM. Optional.
     * - title(String): The title of the gadget.
     * - badge(String): The badge of the gadget. Optional.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this._cfg;

        _.defaults(cfg, defaults);
        this._anchors = {};

        this.title(cfg.title);
        this.badge(cfg.badge);
    }

    /**
     * @override
     */
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };
        var cfg = this._cfg;

        options || (options = {});
        _.defaults(options, defaults);

        this.hide();

        var anchors = this._anchors;

        var titlebar = new Triggers({
            el: this._anchors.$header
        });

        titlebar.render();

        this.listenTo(titlebar, 'trigger', _.bind(onCommandTrigger, this));


        if(this.render === render) {
            this._isRendered = true;

            this.trigger('rendered', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    /**
     * Get or set the title of the gadget.
     *
     * ##### Events:
     * - 'change:title' => triggered after title changed. event.data => the title.
     *
     * @memberof Gadget#
     * @param {String} [text] - the title.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * ##### Options:
     * + silent(Boolean) - A false value will prevent the events from being triggered. Default: false.
     * @returns {String}
     */
    function title(text, options) {
        if(!text) return this._title;

        options || (options = {});

        this._title = text;
        options.silent || this.trigger('change change:title', {
            context: this,
            source: this,
            data: this._title
        });
    }

    /**
     * Get or set the badge of the gadget.
     *
     * ##### Events:
     * - 'change:badge' => triggered after badge changed. event.data => the badge.
     *
     * @memberof Gadget#
     * @param {String} [text] - the badge.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * ##### Options:
     * + silent(Boolean) - A false value will prevent the events from being triggered. Default: false.
     * @returns {String}
     */
    function badge(text, options) {
        if(!text) return this._badge;

        options || (options = {});

        this._badge = text;
        options.silent || this.trigger('change:badge', {
            context: this,
            source: this,
            data: this._badge
        });
    }

    /**
     *
     * Get or set the menu.
     *
     * ##### Events:
     * - 'change:menu' => triggered after menu changed. event.data => the menu.
     *
     * @memberof Gadget#
     * @param {Triggers} triggers - the menu.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * ##### Options:
     * + silent(Boolean) - A false value will prevent the events from being triggered. Default: false.
     * @returns {Triggers}
     *
     */
    function menu(triggers, options) {
        if(!triggers) return this._menu;

        options || (options = {});

        this._menu = triggers;
        options.silent || this.trigger('change:menu', {
            context: this,
            source: this,
            data: this._menu
        });
    }

    function focus() {
        this.trigger('focus', null, this);
    }

    function blur() {
        this.trigger('blur', null, this);
    }

    function close() {
        this.trigger('close', null, this);
    }

    function onCommandTrigger(event) {
        var value = event.data.value;
        var source = event.source;

        switch (value.toUpperCase()) {
            case Gadget.COMMAND.CLOSE:
                this.destroy();
                break;
            case Gadget.COMMAND.HIDE:
                this.hide();
                break;
            default:
                break;
        }
    }

    function progress(progress, context) {
        context || (context=this);
        var progressBar = new ProgressBar().render();

        var clientWidth = this.$el.outerWidth();
        clientWidth || (clientWidth = window.innerWidth);
        var width = Math.min(clientWidth * 0.6, 400);

        progressBar.$el.width(width);

        var $pane = this.popup(progressBar);

        if(progress) {
            progressBar.determine(determine);
        } else {
            progressBar.indetermine();
        }

        function determine(value) {
            if(value>=1) {
                complete();
                return;
            }
            return progress.call(context, value);
        }

        function complete() {
            progressBar.complete();
            _.delay(function() {
                $pane.remove();
                progressBar.destroy();
            }, 2000);
        }

        return {
            complete: complete
        };
    }

    function popup(widget) {
        var el = (widget instanceof jQuery) ? widget.get(0) : widget.el;
        var scrollHeight = this.router().el.scrollHeight;
        var scrollWidth = this.router().el.scrollWidth;

        var maxWidth = scrollWidth * 0.8,  maxHeight = scrollHeight * 0.7;
        var $pane = $('<div/>');

        $pane.append(el);

        $pane.hide();
        $pane.css({
            position: 'fixed',
            "max-width": maxWidth+"px",
            "max-height": maxHeight+"px",
            "overflow-y": "auto",
            "box-shadow": "0 12px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
        });

        this.$el.append($pane);
        _.delay(move, 500, $pane, scrollWidth, scrollHeight);

        function move($pane) {
            var left = (scrollWidth - $pane.outerWidth()) / 2;
            var top = (scrollHeight - $pane.outerHeight()) / 2;
            $pane.css({
                left: left + "px",
                top: top + "px"
            });
            $pane.show();
        }

        return $pane;
    }

    function router() {
        return window.router;
    }

    function navigate(uri) {
        return router().navigate(uri);
    }

    function applet(applet) {
        if(applet===undefined) return this._applet;

        this._applet = applet;
    }

    var props = {
        _oid: undefined,
        _title: undefined,
        _badge: undefined,
        _menu: undefined,
        _applet: undefined
    };

    var Gadget = declare(Base, {
        initialize: initialize,
        render: render,
        title: title,
        badge: badge,
        menu: menu,
        focus: focus,
        blur: blur,
        close: close,
        progress: progress,
        popup: popup,
        router: router,
        navigate: navigate,
        applet: applet
    }, props);

    Gadget.COMMAND = {
        CLOSE: "CLOSE",
        HIDE: "HIDE"
    };

    return Gadget;
});