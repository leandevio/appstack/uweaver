/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/dom', 'uweaver/applet/Applet', 'uweaver/util/UIMLResourceBundle',
    'text!./Home.html'], function(_, dom, Applet, UIMLResourceBundle, uiml) {
    var Base = Applet;
    var TITLE = "Home";
    var resources = new UIMLResourceBundle(uiml);

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var html = resources.get('main');

        this.$el.html(html);
        this.title(TITLE);
    }


    var props = {};

    var Home = declare(Base, {
        initialize: initialize
    }, props);

    return Home;

});