/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/dom', 'uweaver/string', 'uweaver/theme', 'uweaver/i18n',
    'uweaver/applet/Applet', 'uweaver/widget/Trigger',
    'uweaver/Engine', 'uweaver/service/AppManager',
    'text!./Launcher.html'], function(dom, string, theme, i18n, Applet, Trigger, Engine, AppManager, tpl) {
    var Base = Applet;
    var engine = Engine.getDefaultInstance();
    var appManager = AppManager.getDefaultInstance();
    var resources = dom.createElement("div").html(tpl);

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._el = dom.select(this.el);
        var html = resource.call(this, 'main');
        this._el.html(html);

        this._items = new Trigger({
            el: this._el.query('[name=items]')
        });
        this.listenTo(this._items, 'trigger', onItemSelect);
        this.listenTo(appManager, 'change:applet', render);
    }

    function render(options) {
        var tpl = resource('item');
        var html = "";

        appManager.applets().each(function(model, index) {
            var icon = model.get('icon') || 'gear';
            html = html + string.format(tpl, {
                    id: model.id,
                    title: i18n.translate(model.get('title')),
                    icon: icon,
                    locked: model.get('locked') ? 'visible' : 'hidden',
                    disabled: model.get('disabled') ? 'disabled' : '',
                    background: theme.fill(index)
                });
        }, this);
        this._items.html(html);
        this._items.render();

        return this;
    }

    function onItemSelect(event) {
        var id = event.data;
        var applet = appManager.applet(id);
        engine.open(applet);
    }

    function resource(name) {
        return resources.query('[name=' + name + ']').html()
    }

    var props = {
        _items: undefined
    };

    var Launcher = declare(Base, {
        initialize: initialize,
        render: render
    }, props);

    return Launcher;
});