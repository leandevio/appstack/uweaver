/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/applet/Applet', 'uweaver/widget/Form', 'uweaver/widget/Trigger',
    'uweaver/util/UIMLResourceBundle', 'text!./Logon.html'], function(_, Applet, Form, Trigger, UIMLResourceBundle, uiml) {
    var Base = Applet;
    var TITLE = "Logon";
    var resources = new UIMLResourceBundle(uiml);

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this.hide();

        var html = resources.get('main');

        this.$el.html(html);
        this.width(300);
        this.title(TITLE);

        this._form = this.query('form', Form).render();
        this._commandbar = this.query('[name=commandbar]', Trigger).render();
        this.listenTo(this._commandbar, 'execute', onCommandbarExecute, this);
    }

    function onCommandbarExecute(event) {
        var action = event.data;

        switch (action) {
            case 'login':
                login.call(this);
                break;
        }
    }

    function login() {
        if(!this._form.validate()) return;
        this.trigger('login', {
            username: this._form.data().get('username'),
            password: this._form.data().get('password')
        }, this);
    }

    var props = {
        _form: undefined,
        _commandbar: undefined
    };

    var Logon = declare(Base, {
        initialize: initialize
    }, props);

    return Logon;

});