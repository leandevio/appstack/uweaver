/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'uweaver/lang',
    'uweaver/widget/Widget', 'uweaver/widget/Panel',
    'uweaver/widget/Popup'], function (_, $, lang, Widget, Panel, Popup) {

    var declare = lang.declare;
    var Base = Widget;

    /**
     * **MDIApplet**
     * An applet is an application for one specific activity.
     *
     * **Configs:**
     * - tpl(String): A html string to layout & construct the DOM.
     * - title(String): The title of the applet.
     *
     * @constructor MDIApplet
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the applet.
     */

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        this._title = cfg.title;
        this._panels = [];
    }

    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };
        var cfg = this._cfg;

        options || (options = {});
        _.defaults(options, defaults);

        this.hide();

        var anchors = this._anchors;

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('rendered', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function isSingleton() {
        return this._singleton;
    }

    function start() {
        this.render();

        var event = {
            data: {},
            context: this,
            source: this
        };
        this.trigger('applet:started', event);
    }

    function stop() {
        var event = {
            data: {},
            context: this,
            source: this
        };
        this.trigger('applet:stop', event);
        this.destroy();
    }

    function pid() {
        return this.oid();
    }

    function title(text) {
        if(!text) return this._title;

        this._title = text;
    }

    function menu(buttons) {
        if(!buttons) return this._menu;
        this._menu = buttons;
    }


    function add(panel, options) {
        options || (options = {});

        var defaults = {
            movable: true,
            resizable: false
        };

        _.defaults(options, defaults);

        options.movable && panel.movable({
            restrict: {
                restriction: this.$el
            }
        });

        options.resizable && panel.resizable();


        panel.attach(this.$el);

        if(!options.position) {
            options.position = {
                left: (this.width() - panel.width()) * 0.5,
                top: (this.height() - panel.height()) * 0.5
            };
        }

        panel.moveTo(options.position);

        this.listenTo(panel, 'focus', _.bind(onPanelFocus, this));

        this._panels.push(panel);
    }

    function remove(panel, options) {
        if(panel===undefined) return this;

        options || (options = {});

        var index = _.isNumber(panel) ? panel : _.findIndex(this._panels, function(n) {
            return n === panel;
        });


        panel = this._panels[index];

        this._panels = _.without(this._panels, panel);

        this.stopListening(panel);
        if(this._activePanel===panel) this._activePanel = undefined;
        panel.detach();
    }

    function clear() {
        this._activePanel = undefined;
        _.each(this._panels, function(panel) {
            this.stopListening(panel);
            panel.detach();
        }, this);
        this._panels = [];
    }

    function move(panel, position) {
        panel.css('transform', 'translate(0px, 0px');
        panel.moveTo(position);
    }

    function get(index) {
        return this._panels[index];
    }

    function bringToFront(panel) {
        if(this._activePanel===panel) return;

        this._activePanel = panel;
        _.each(_.without(this._panels, panel), function(panel) {
            panel.blur();
        });

        panel.detach();
        panel.attach(this.$el);
    }

    function onPanelFocus(event) {
        var panel = event.context;

        this.bringToFront(panel);
    }

    function panels() {
        return this._panels;
    }

    /**
     * Get the active gadget.
     *
     * @memberof MDIApplet#
     * @returns {Gadget}
     */
    function current() {
        return this._activePanel;
    }

    function size() {
        return this._panels.length;
    }

    function focus() {}

    function blur() {}

    var props = {
        _singleton: false,
        _menu: undefined,
        _title: undefined,
        _maxZIndex: 0,
        _panels: undefined,
        _activePanel: undefined
    };

    var MDIApplet = declare(Base, {
        initialize: initialize,
        render: render,
        menu: menu,
        isSingleton: isSingleton,
        start: start,
        stop: stop,
        pid: pid,
        title: title,
        add: add,
        remove: remove,
        clear: clear,
        move: move,
        get: get,
        bringToFront: bringToFront,
        current: current,
        size: size,
        panels: panels,
        focus: focus,
        blur: blur
    }, props);

    return MDIApplet;
});