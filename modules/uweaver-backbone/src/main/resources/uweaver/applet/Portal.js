/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'uweaver/lang',
    'uweaver/widget/Widget', 'uweaver/applet/Gadget', 'uweaver/data/Collection', 'uweaver/widget/Triggers',
    'uweaver/widget/Popup', 'uweaver/widget/Panel',
    'uweaver/exception/UnsupportedTypeException',
    'text!./tpl/Portal.html', 'text!./tpl/Portal/LayoutSelector.html', 'text!./tpl/Portal/ContentSelector.html',
    'uweaver/Logger'], function (_, $, lang, Widget, Gadget, Collection, Triggers, Popup, Panel, UnsupportedTypeException,
                                 tpl, tplLayoutSelector, tplGadgetSelector, Logger) {

    var declare = lang.declare;
    var Base = Widget;
    var LOGGER = new Logger("uweaver/applet/Portal");

    var LayoutSelector = declare(Popup, {
        initialize: function(config) {
            Popup.prototype.initialize.apply(this, arguments);
            var defaults = {
                tpl: tplLayoutSelector
            };
            var cfg = this._cfg;
            _.defaults(cfg, defaults);

            this._data = (cfg.data) ? cfg.data : new Collection();

            this._triggers = new Triggers({
                el: this.$('[name=toolbar]')
            }).render();

            this.listenTo(this._trigger, 'trigger', _.bind(this.onTrigger, this));
        },

        onTrigger: function(e) {
            var event = {
                data: this._listbox.selection()
            };
            this.trigger('select', {

            });
        }
    }, {
        _data: undefined,
        _triggers: undefined
    });
    var GadgetSelector = declare(Panel, {
        initialize: function(config) {
            Panel.prototype.initialize.apply(this, arguments);
            var defaults = {
                tpl: tplGadgetSelector
            };
            var cfg = this._cfg;
            _.defaults(cfg, defaults);
        }
    });

    /**
     * An applet is an application for one specific activity.
     *
     * ##### Usage
     *     var applet = new Portal({
     *         el: $('#applet')
     *     });
     *     applet.render();
     *
     *     var messages = new Gadget({
     *         tpl: "<h3>Messages</h3><p>Here are messages for you ...</p>"
     *         title: "Messages",
     *         badge: "3"
     *     }).render();
     *
     *     applet.add(messages);
     *
     * ##### Events:
     * + 'select' - [select()]{@link Portal#select}, [add()]{@link Portal#add}, [remove()]{@link Portal#remove}
     * + 'add' - [add()]{@link Portal#add}
     * + 'remove' - [remove()]{@link Portal#remove}
     * + 'transition:menu' - [menu()]{@link Portal#menu}, [title()]{@link Portal#title}
     *
     * @constructor Portal
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the applet.
     * @param {String} [config.tpl] - A html string to layout & construct the DOM.
     * @param {String} [config.title] - The title of the applet.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            tpl: tpl
        };
        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        this._title = cfg.title;

        this._gadgets = (cfg.gadgets) ? cfg.gadgets : new Collection();
        this._layouts = (cfg.layouts) ? cfg.layouts : new Collection();

        this._layoutSelector = new LayoutSelector({
            data: this._layouts
        });

        this.listenTo(this._layoutSelector, 'select', _.bind(onSelectLayout, this));

        this._gadgetSelector = new GadgetSelector({
            data: this._gadgets
        });

        this.listenTo(this._gadgetSelector, 'select', _bind(onSelectGadget, this));
    }

    /**
     * @override
     */
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };
        var cfg = this.cfg();

        options || (options = {});
        _.defaults(options, defaults);

        var anchors = this._anchors;

        var toolbar = new Triggers({
            el: this.$('[name=toolbar]'),
            selector: "i"
        }).render();

        this.listenTo(toolbar, 'trigger', onCommandTrigger);

        this._toolbar = toolbar;

        this._layoutSelector.render({hidden: true});
        this._contentSelector.render({hidden: true});

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('rendered', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function onCommandTrigger(event) {
        var value = event.data.value;

        switch (value.toUpperCase()) {
            case Portal.COMMAND.LAYOUT:
                this._layoutSelector.show();
                break;
            case Portal.COMMAND.CUSTOMIZE:
                this._gadgetSelector.hide();
                break;
            default:
                break;
        }
    }

    function layout() {

    }

    function isSingleton() {
        return this._singleton;
    }

    function start() {
        this.render();

        var event = {
            data: {},
            context: this,
            source: this
        };
        this.trigger('start', event);
    }

    function stop() {
        var event = {
            data: {},
            context: this,
            source: this
        };
        this.trigger('stop', event);
        this.destroy();
    }

    function pid() {
        return this.oid();
    }

    /**
     * Get or set the title of the applet.
     *
     * ##### Events:
     * - 'transition:title' => triggered after title changed.
     *
     * @memberof Portal#
     * @param {Object} [text] - the title.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {String}
     */
    function title(text, options) {
        if(!text) return this._title;

        this._title = text;

        options || (options = {});

        options.silent || this.trigger('transition', {
            context: this,
            source: this,
            data: undefined
        });
    }

    /**
     *
     * Get or set the menu.
     *
     * ##### Events:
     * - 'transition:menu' => triggered after menu changed. event.data => the menu.
     *
     * @memberof Portal#
     * @param {Triggers} triggers - the menu.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Triggers}
     *
     */
    function menu(triggers, options) {
        if(!triggers) return this._menu;

        options || (options = {});

        this._menu = triggers;
        options.silent || this.trigger('transition:menu', {
            context: this,
            source: this,
            data: this._menu
        });
    }

    /**
     * Add the gadget to the applet.

     * ##### Events:
     * + 'add' - triggered after added. event.data => the added gadget.
     * + 'select' - triggered after new gadget selected. event.data => the selected gadget.
     *
     * @memberof Portal#
     * @param {Gadget} gadget - The gadget to add.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Portal} this
     */
    function add(gadget, options) {
        if(!(gadget instanceof Gadget)) {
            throw new UnsupportedTypeException({
                supportedTypes: ['Gadget'],
                type: typeof gadget
            });
        }

        options || (options = {});

        var defaults = {
            selected: true,
            title: gadget.title()
        };

        _.defaults(options, defaults);

        this.listenTo(gadget, 'transition', _.bind(onGadgeTransition, this));

        return this;
    }

    /**
     * @todo update the tab's content(title & badge)
     * @param event
     */
    function onGadgeTransition(event) {
        var gadget = event.context;
    }

    /**
     * Remove the gadget from the applet.
     *
     * ##### Events:
     * + 'remove' - triggered after removed. event.data => the removed gadget.
     * + 'select' - triggered after new gadget selected. event.data => the selected gadget.
     *
     * @memberof Portal#
     * @param {Gadget} gadget - The gadget to remove.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Portal} this
     */
    function remove(gadget, options) {
        options || (options = {});
        this.stopListening(gadget);
        return this;
    }

    /**
     * Remove all gadgets from the applet.
     *
     * ##### Events:
     * + 'remove' - triggered after removed.
     *
     * @memberof Portal#
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Portal} this
     */
    function removeAll(options) {
        options || (options = {});
        _.each(this.gadgets(), function(gadget) {
            this.stopListening(gadget);
        }, this);
        return this;
    }

    /**
     * Select the gadget to show.
     *
     * ##### Events:
     * + 'select' - triggered after selected. event.data => the selected gadget.
     *
     * @memberof Portal#
     * @param {Gadget|Integer} gadget - The gadget(or index of the gadget) to show.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Portal} this
     */
    function select(gadget, options) {

        return this;
    }

    /**
     * Get the gadget.
     *
     * @memberof Portal#
     * @param {Integer} index - The index of gadget. zero-based.
     * @returns {Gadget}
     */
    function get(index) {
    }

    /**
     * Get the active gadget.
     *
     * @memberof Portal#
     * @returns {Gadget}
     */
    function current() {
    }

    /**
     * Get the # of gadgets.
     *
     * @memberof Portal#
     * @returns {Integer}
     */
    function size() {
    }

    /**
     * Get all gadgets.
     *
     * @memberof Portal#
     * @returns {Integer}
     */
    function gadgets() {
    }


    var props = {
        _singleton: false,
        _menu: undefined,
        _title: undefined,
        _maxZIndex: 0,
        _gadgets: undefined,
        _activeGadget: undefined,
        className: "container",
        _porlets: undefined,
        _layouts: undefined,
        _triggers: undefined
    };

    var Portal = declare(Base, {
        initialize: initialize,
        render: render,
        menu: menu,
        isSingleton: isSingleton,
        start: start,
        stop: stop,
        pid: pid,
        title: title,
        add: add,
        remove: remove,
        removeAll: removeAll,
        select: select,
        get: get,
        current: current,
        size: size,
        gadgets: gadgets,
        layout: layout
    }, props);

    Portal.COMMAND = {
        LAYOUT: 'LAYOUT',
        CUSTOMIZE: 'CUSTOMIZE'
    };

    return Portal;
});