/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'uweaver/lang',
    'uweaver/widget/Widget', 'uweaver/widget/TabPanel', 'uweaver/applet/Gadget', 'uweaver/widget/ProgressBar',
    'uweaver/Logger',
    'uweaver/exception/UnsupportedTypeException'], function (_, $, lang, Widget, TabPanel, Gadget, ProgressBar, Logger, UnsupportedTypeException) {

    var declare = lang.declare;
    var Base = Widget;
    var LOGGER = new Logger("uweaver/applet/TDIApplet");

    /**
     * An applet is an application for one specific activity.
     *
     * ##### Usage
     *     var applet = new TDIApplet({
     *         el: $('#applet')
     *     });
     *     applet.render();
     *
     *     var messages = new Gadget({
     *         tpl: "<h3>Messages</h3><p>Here are messages for you ...</p>"
     *         title: "Messages",
     *         badge: "3"
     *     }).render();
     *
     *     applet.add(messages);
     *
     * ##### Events:
     * + 'select' - [select()]{@link TDIApplet#select}, [add()]{@link TDIApplet#add}, [remove()]{@link TDIApplet#remove}
     * + 'selected' - [select()]{@link TDIApplet#select}, [add()]{@link TDIApplet#add}, [remove()]{@link TDIApplet#remove}
     * + 'add' - [add()]{@link TDIApplet#add}
     * + 'remove' - [remove()]{@link TDIApplet#remove}
     * + 'transition:menu' - [menu()]{@link TDIApplet#menu}, [title()]{@link TDIApplet#title}
     *
     * @constructor TDIApplet
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the applet.
     * @param {String} [config.tpl] - A html string to layout & construct the DOM.
     * @param {String} [config.title] - The title of the applet.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        this._title = cfg.title;

        var tabPanel = new TabPanel().render();

        tabPanel.attach(this.$el);
        this.listenTo(tabPanel, 'all', _.bind(onTabPanelActions, this));

        this._tabPanel = tabPanel;

        this.css('position', 'relative');
    }

    function onTabPanelActions(type, e) {
        var interested = ["add", "select", "change", "remove"];

        if(!_.contains(interested, type)) return;

        if(type==='select') {
            var gadget = e.data;
            gadget.focus();
        }

        var event = {
            context: this,
            source: e.context,
            data: e.data
        };

        this.trigger(type, event);
    }

    function isSingleton() {
        return this._singleton;
    }

    function start() {
        this.render();

        var event = {
            data: {},
            context: this,
            source: this
        };
        this.trigger('started', event);
    }

    function stop() {
        var event = {
            data: {},
            context: this,
            source: this
        };
        this.trigger('stop', event);
        this.destroy();
    }

    function pid() {
        return this.oid();
    }

    /**
     * Get or set the title of the applet.
     *
     * ##### Events:
     * - 'transition:title' => triggered after title changed.
     *
     * @memberof TDIApplet#
     * @param {Object} [text] - the title.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {String}
     */
    function title(text, options) {
        if(!text) return this._title;

        this._title = text;

        options || (options = {});

        options.silent || this.trigger('transition', {
            context: this,
            source: this,
            data: undefined
        });
    }

    /**
     *
     * Get or set the menu.
     *
     * ##### Events:
     * - 'transition:menu' => triggered after menu changed. event.data => the menu.
     *
     * @memberof TDIApplet#
     * @param {Triggers} triggers - the menu.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Triggers}
     *
     */
    function menu(triggers, options) {
        if(!triggers) return this._menu;

        options || (options = {});

        this._menu = triggers;
        options.silent || this.trigger('transition:menu', {
            context: this,
            source: this,
            data: this._menu
        });
    }

    /**
     * Add the gadget to the applet.

     * ##### Events:
     * + 'add' - triggered after added. event.data => the added gadget.
     * + 'select' - triggered after new gadget selected. event.data => the selected gadget.
     *
     * @memberof TDIApplet#
     * @param {Gadget} gadget - The gadget to add.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {String} [options.title=gadget.title()] - A text to specify the caption displayed in the tab.
     * @param {String} [options.icon] - A text to specify the icon displayed in the tab.
     * @param {String} [options.badge=gadget.badge()] - A text to specify the symbol displayed in the tab.
     * @param {Boolean} [options.closable=false] - A true value to display a close button in the tab.
     * @param {Boolean} [options.selected=true] - A false value to prevent the new tabpane from being selected.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @param {Integer} [options.index] - index at which the specified component is to be inserted.
     * @returns {TDIApplet} this
     */
    function add(gadget, options) {
        if(!(gadget instanceof Gadget)) {
            throw new UnsupportedTypeException({
                supportedTypes: ['Gadget'],
                type: typeof gadget
            });
        }

        options || (options = {});

        var defaults = {
            closable: false,
            selected: true,
            title: gadget.title(),
            badge: gadget.badge()
        };

        _.defaults(options, defaults);

        if(this._tabPanel.indexOf(gadget)!==-1) {
            options.selected && this._tabPanel.select(gadget);
            return this;
        }

        this._tabPanel.add(gadget, options);
        gadget.applet(this);

        this.listenTo(gadget, 'change', onGadgetChange);
        this.listenTo(gadget, 'close', onGadgetClose);

        return this;
    }

    /**
     * @todo update the tab's content(title & badge)
     * @param event
     */
    function onGadgetChange(event) {
        var gadget = event.context;

        this._tabPanel.title(gadget, gadget.title());
    }

    function onGadgetClose(event) {
        var gadget = event.context;
        this.remove(gadget);
    }

    /**
     * Remove the gadget from the applet.
     *
     * ##### Events:
     * + 'remove' - triggered after removed. event.data => the removed gadget.
     * + 'select' - triggered after new gadget selected. event.data => the selected gadget.
     *
     * @memberof TDIApplet#
     * @param {Gadget} gadget - The gadget to remove.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {TDIApplet} this
     */
    function remove(gadget, options) {
        options || (options = {});
        this._tabPanel.remove(gadget, options);
        this.stopListening(gadget);
        return this;
    }

    function badge(gadget, text) {
        if(text===undefined) return this._tabPanel.badge(gadget);
        this._tabPanel.badge(gadget, text);
    }

    /**
     * Remove all gadgets from the applet.
     *
     * ##### Events:
     * + 'remove' - triggered after removed.
     *
     * @memberof TDIApplet#
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {TDIApplet} this
     */
    function removeAll(options) {
        options || (options = {});
        _.each(this.gadgets(), function(gadget) {
            this.stopListening(gadget);
        }, this);
        this._tabPanel.removeAll(options);
        return this;
    }

    /**
     * Select the gadget to show.
     *
     * ##### Events:
     * + 'select' - triggered after selected. event.data => the selected gadget.
     *
     * @memberof TDIApplet#
     * @param {Gadget|Integer} gadget - The gadget(or index of the gadget) to show.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {TDIApplet} this
     */
    function select(gadget, options) {
        this._tabPanel.select(gadget, options);

        return this;
    }

    /**
     * Get the gadget.
     *
     * @memberof TDIApplet#
     * @param {Integer} index - The index of gadget. zero-based.
     * @returns {Gadget}
     */
    function get(index) {
        return this._tabPanel.get(index);
    }

    /**
     * Get the active gadget.
     *
     * @memberof TDIApplet#
     * @returns {Gadget}
     */
    function current() {
        return this._tabPanel.current();
    }

    /**
     * Get the # of gadgets.
     *
     * @memberof TDIApplet#
     * @returns {Integer}
     */
    function size() {
        return this._tabPanel.size();
    }

    function indexOf(gadget) {
        return this._tabPanel.indexOf(gadget);
    }

    function contains(gadget) {
        return this._tabPanel.contains(gadget);
    }

    /**
     * Get all gadgets.
     *
     * @memberof TDIApplet#
     * @returns {Integer}
     */
    function gadgets() {
        return this._tabPanel.components();
    }

    function focus() {
        this.trigger('focus', null, this);
    }

    function blur() {
        this.trigger('blur', null, this);
    }

    function close() {
        this.trigger('close', null, this);
    }

    /**
     *
     * @param gadget
     *
     * @todo trigger a event when a gadget bring to front (focus).
     */
    function bringToFront(gadget) {
        if(this._activeGadget) this.listenTo(this._activeGadget, 'focus', _.bind(onGadgetFocus, this));
        this.stopListening(gadget, 'focus');
        this._activeGadget = gadget;
        if(gadget.css('z-index')==this._maxZIndex) return;
        gadget.css('z-index', ++this._maxZIndex);
    }

    function onGadgetFocus(event) {
        var gadget = event.context;

        this.bringToFront(gadget);
    }

    function progress(progress, context) {
        context || (context=this);
        var progressBar = new ProgressBar().render();

        var clientWidth = this.$el.outerWidth();
        clientWidth || (clientWidth = window.innerWidth);
        var width = Math.min(clientWidth * 0.6, 400);

        progressBar.$el.width(width);

        var $pane = this.popup(progressBar);

        if(progress) {
            progressBar.determine(determine);
        } else {
            progressBar.indetermine();
        }

        function determine(value) {
            if(value>=1) {
                complete();
                return;
            }
            return progress.call(context, value);
        }

        function complete() {
            progressBar.complete();
            _.delay(function() {
                $pane.remove();
                progressBar.destroy();
            }, 2000);
        }

        return {
            complete: complete
        };
    }

    function popup(widget) {
        var el = (widget instanceof jQuery) ? widget.get(0) : widget.el;
        var scrollHeight = this.router().el.scrollHeight;
        var scrollWidth = this.router().el.scrollWidth;

        var maxWidth = scrollWidth * 0.8,  maxHeight = scrollHeight * 0.7;
        var $pane = $('<div/>');

        $pane.append(el);

        $pane.hide();
        $pane.css({
            position: 'fixed',
            "max-width": maxWidth+"px",
            "max-height": maxHeight+"px",
            "overflow-y": "auto",
            "box-shadow": "0 12px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
        });

        this.$el.append($pane);
        _.delay(move, 500, $pane, scrollWidth, scrollHeight);

        function move($pane) {
            var left = (scrollWidth - $pane.outerWidth()) / 2;
            var top = (scrollHeight - $pane.outerHeight()) / 2;
            $pane.css({
                left: left + "px",
                top: top + "px"
            });
            $pane.show();
        }

        return $pane;
    }


    function router() {
        return window.router;
    }

    function navigate(uri) {
        return router().navigate(uri);
    }

    var props = {
        _singleton: false,
        _menu: undefined,
        _title: undefined,
        _tabPanel: undefined,
        _maxZIndex: 0,
        _gadgets: undefined,
        _activeGadget: undefined,
        className: "container-fluid"
    };

    var TDIApplet = declare(Base, {
        initialize: initialize,
        menu: menu,
        isSingleton: isSingleton,
        start: start,
        stop: stop,
        pid: pid,
        title: title,
        badge: badge,
        add: add,
        remove: remove,
        removeAll: removeAll,
        select: select,
        get: get,
        current: current,
        size: size,
        gadgets: gadgets,
        contains: contains,
        indexOf: indexOf,
        focus: focus,
        blur: blur,
        close: close,
        popup: popup,
        progress: progress,
        router: router,
        navigate: navigate
    }, props);

    return TDIApplet;
});