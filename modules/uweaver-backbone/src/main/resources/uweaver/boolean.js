/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * The boolean module provides a collection of utility methods for manipulating boolean values.
 *
 *
 * ##### Usage
 *
 * @module boolean
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore', 'uweaver/string', 'uweaver/exception/InvalidFormatException',
    'uweaver/exception/UnsupportedTypeException'], function(_, string, InvalidFormatException, UnsupportedTypeException) {

    var BOOLEANPATTERN = "true|false";

    /**
     * The method parses a string representation of a boolean using the specified pattern and returns the boolean.
     *
     * ##### Usage

     *
     * @memberof module:boolean
     * @param {String} text - the text to convert.
     * @param {String} [pattern] - specify a boolean pattern to parse the text.
     * @returns {Boolean|undefined}
     */
    function parse(text, pattern) {
        if(text===null||text===undefined) {
            return null;
        } else if(!_.isString(text)) {
            throw new UnsupportedTypeException({
                type: typeof text,
                supportedTypes: ['String']
            });
        } else if(text.length===0) {
            return null;
        }

        pattern || (pattern = BOOLEANPATTERN);

        if(!test(text, pattern)) {
            throw new InvalidFormatException({
                entity: text,
                format: pattern
            });
        }

        var tokens = pattern.split('|');
        var trueValue = tokens[0], falseValue = tokens[1];

        if(text.toLocaleLowerCase()===trueValue) return true;
        else if(text.toLocaleLowerCase()===falseValue) return false;
        else return undefined;

    }


    /**
     * The method formats a boolean to a string representation using the specified pattern.
     *
     * ##### Usage
     *
     * @memberof module:boolean
     * @param {Boolean} value - the boolean to format.
     * @param {String} [pattern] - specify a boolean pattern to format the value.
     * @returns {String}
     */
    function format(value, pattern) {
        if(value===null||value===undefined) {
            return null;
        } else if(!_.isBoolean(value)) {
            throw new UnsupportedTypeException({
                type: typeof value,
                supportedTypes: ["Boolean"]
            });
        }

        if(!pattern) return value.toString();


        var tokens = pattern.split('|');
        var trueValue = tokens[0], falseValue = tokens[1];

        return value ? trueValue : falseValue;
    }

    /**
     *
     * The method converts a value to a boolean using the specified pattern.
     *
     * ##### Usage
     *
     * @memberof module:number
     * @param {String|Number|Boolean} value - the value to convert.
     * @param {String} [pattern] - specify a boolean pattern to parse the text.
     * @returns {Boolean}
     */
    function convert(value, pattern) {
        if(value===undefined||value===null) return value;
        if(_.isBoolean(value)) {
            return value;
        } else if(_.isNumber(value)) {
            return Boolean(value);
        } else if(_.isString(value)) {
            if(value.length===0) return null;
            return parse(value, pattern);
        } else {
            throw new UnsupportedTypeException({
                type: typeof value,
                supportedTypes: ["String", "Boolean",  "Number"]
            });
        }
    }

    /**
     *
     * The method tests if a string representation is a number using the specified pattern.
     *
     * @memberof module:number
     * @param {String} text - the text to check.
     * @param {String} [pattern="true|false"] - specify a boolean pattern to check the text.
     * @returns {Boolean}
     */
    function test(text, pattern) {
        if(!_.isString(text)) {
            throw new UnsupportedTypeException({
                type: typeof text,
                supportedTypes: ['String']
            });
        }

        pattern || (pattern = BOOLEANPATTERN);

        if(pattern.indexOf(text)===-1) return false;

        return true;
    }


    return {
        parse: parse,
        format: format,
        convert: convert,
        test: test
    };
});