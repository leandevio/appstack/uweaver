/**
 * Created by jasonlin on 8/21/14.
 */
define(['uweaver/lang', 'modernizr'], function (lang, modernizr) {

    function locale() {
        return navigator.language || navigator.browserLanguage;
    }

    function os() {
        if(navigator.appVersion.indexOf("iOS")!==-1) return "iOS";
        if(navigator.appVersion.indexOf("Android")!==-1) return "Android";
        if(navigator.appVersion.indexOf("Win")!==-1) return "Windows";
        if(navigator.appVersion.indexOf("Mac")!==-1) return "MacOS";
        if(navigator.appVersion.indexOf("X11")!==-1) return "UNIX";
        if(navigator.appVersion.indexOf("Linux")!==-1) return "Linux";
    }

    function agent() {
        if(navigator.userAgent.indexOf("MSIE")!==-1) return "MSIE";
        if(navigator.userAgent.indexOf("Chrome")!==-1) return "Chrome";
        if(navigator.userAgent.indexOf("Firefox")!==-1) return "Firefox";
        if(navigator.userAgent.indexOf("Safari")!==-1) return "Safari";
    }

    function isMSIE() {
        return agent()==='MSIE';
    }

    function isChrome() {
        return agent()==='Chrome';
    }

    function isFirefox() {
        return agent()==='Firefox';
    }

    function isSafari() {
        return agent()==='Safari';
    }

    function width() {
        return window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
    }

    function height() {
        return window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;
    }

    return {
        locale: locale,
        os: os,
        agent: agent,
        isMSIE: isMSIE,
        isFirefox: isFirefox,
        isChrome: isChrome,
        isSafari: isSafari,
        width: width,
        height: height,
        input: modernizr.input,
        inputtypes: modernizr.inputtypes,
        formvalidation: modernizr.formvalidation
    };

});