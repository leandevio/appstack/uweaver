/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'ss', 'uweaver/lang', 'uweaver/i18n', 'uweaver/dom', 'uweaver/widget/Widget',
    'uweaver/data/Collection', './axis/Axis', './series/ScatterSeries', './series/LineSeries', './series/BarSeries', './series/BaselineSeries',
    './interaction/Tooltips', 'uweaver/graphic/Toolbar', './legend/Legend'], function(_, $, d3, ss, lang, i18n, dom, Widget, Collection, Axis,
    ScatterSeries, LineSeries, BarSeries, BaselineSeries, Tooltips, Toolbar, Legend) {

    var declare = lang.declare;
    var Base = Widget;

    var PADDING = 10, WIDTH = 400, HEIGHT = 300;

    /**
     * A CartesianChart visualizes its data points using the Cartesian coordinate system.
     *
     * ##### JavaScript
     * 
     *     var OrderCubes = declare(Collection, {
     *        resource: 'orderCubes',
     *        orderDate: function(options) {
     *            this.sync('orderDate', {before: new Date()}, options).then(function(response) {
     *                var items = this.parse(response);
     *                this.reset(items);
     *            }, this);
     *        },
     *        area: function(options) {
     *            this.sync('area', {}, options).then(function(response) {
     *                var items = this.parse(response);
     *                this.reset(items);
     *            }, this);
     *        },
     *        orderMonth: function(options) {
     *            this.sync('orderMonth', {}, options).then(function(response) {
     *                var items = this.parse(response);
     *                this.reset(items);
     *            }, this);
     *        },
     *     });

     *     var cubes = new OrderCubes();
     *     
     *     // Generate a temporal scatter chart.
     *     
     *     cubes.orderDate({async: false});
     *     
     *     var temporalScatterChart = new CartesianChart({
     *        el: '[name=temporalScatterChart]',
     *        data: cubes,
     *        //define the x and y-axis configuration.
     *        axes: [{
     *            type: CartesianChart.AXIS.NUMERICAL,
     *            position: CartesianChart.POSITION.LEFT,
     *            unit: 'USD'
     *        }, {
     *            type: CartesianChart.AXIS.TIME,
     *            position: CartesianChart.POSITION.BOTTOM,
     *            title: 'Order Date'
     *        }],
     *        title: "Daily Revenue & Cost"
     *     }).render();
     *
     *     temporalScatterChart.addSeries({
     *          xField: 'orderDate',
     *          yField: 'cost',
     *          title: 'Cost'
     *     });
     *
     *     temporalScatterChart.addSeries({
     *          type: CartesianChart.SERIES.LINE,
     *          xField: 'orderDate',
     *          yField: 'amount',
     *          title: 'Revenue',
     *          tooltips: {
     *              renderer: function(item) {
     *                  return "Revenue in " + formatter.format(item.get('orderDate'), {pattern: "yyyy/MM"}) + ": " + formatter.format(item.get('amount'), {pattern: "$#,##0"});
     *              }
     *          }
     *      });
     *
     *      // draw a trendline
     *     temporalScatterChart.trendline('Revenue');
     *
     *     // draw a gridline
     *     temporalScatterChart.gridline(CartesianChart.POSITION.LEFT);
     *
     *     // draw a baseline
     *     temporalScatterChart.baseline({
     *          xField: 'orderDate',
     *          yField: 'goal',
     *          title: 'Goal'
     *      });
     * 
     *     // Generate a categorical bar chart.
     *     
     *     cubes.area({async: false});
     *     
     *     var categoricalBarChart = new CartesianChart({
     *        data: cubes,
     *        el: '[name=categoricalBarChart]',
     *        //define the x and y-axis configuration.
     *        axes: [{
     *            type: CartesianChart.AXIS.NUMERICAL,
     *            position: CartesianChart.POSITION.LEFT,
     *            unit: 'USD'
     *        }, {
     *            type: CartesianChart.AXIS.CATEGORICAL,
     *            position: CartesianChart.POSITION.BOTTOM,
     *            title: 'Area'
     *        }],
     *        //define the actual data series.
     *        series: [{
     *            type: CartesianChart.SERIES.BAR,
     *            xField: 'area',
     *            yField: 'amount',
     *            title: 'Revenue'
     *        }],
     *        width: 700, height: 400,
     *        title: "Revenue & Cost by Area"
     *     }).render();
     *
     * 
     *      // Generate a temporal two y-axis bar chart.
     *      
     *     cubes.orderDate({async: false});
     *     var twoYAxisTemporalBarChart = new CartesianChart({
     *         data: cubes,
     *         el: '[name=TwoYAxisTemporalBarChart]',
     *         //define the x and y-axis configuration.
     *         axes: [{
     *             type: CartesianChart.AXIS.NUMERICAL,
     *             position: CartesianChart.POSITION.LEFT,
     *             unit: 'USD',
     *             title: 'Amount'
     *         }, {
     *             type: CartesianChart.AXIS.NUMERICAL,
     *             position: CartesianChart.POSITION.RIGHT,
     *             tickFormat: '#0.0%',
     *             title: 'Rate'
     *         }, {
     *             type: CartesianChart.AXIS.TIME,
     *             position: CartesianChart.POSITION.BOTTOM,
     *             title: 'Order Date'
     *         }],
     *         title: "Daily Revenue, Cost & Margin"
     *     }).render();
     *     
     *     twoYAxisTemporalBarChart.addSeries({
     *         type: CartesianChart.SERIES.LINE,
     *         xField: 'orderDate',
     *         yField: 'margin',
     *         yAxis: CartesianChart.POSITION.RIGHT,
     *         title: 'Margin'
     *     });
     *     
     *     twoYAxisTemporalBarChart.addSeries({
     *         type: CartesianChart.SERIES.BAR,
     *         xField: 'orderDate',
     *         yField: 'amount',
     *         yAxis: CartesianChart.POSITION.LEFT,
     *         title: 'Cost'
     *     });
     *     
     *     twoYAxisTemporalBarChart.addSeries({
     *         type: CartesianChart.SERIES.BAR,
     *         xField: 'orderDate',
     *         yField: 'cost',
     *         yAxis: CartesianChart.POSITION.LEFT,
     *         title: 'Revenue'
     *     });
     *
     *      // Generate a temporal stacked bar chart.
     *      
     *     cubes.orderMonth({async: false});
     *     var StackedTemporalBarChart = new CartesianChart({
     *         data: cubes,
     *         el: '[name=StackedTemporalBarChart]',
     *         //define the x and y-axis configuration.
     *         axes: [{
     *             type: CartesianChart.AXIS.NUMERICAL,
     *             position: CartesianChart.POSITION.LEFT,
     *             unit: 'USD',
     *             title: 'Amount'
     *         }, {
     *             type: CartesianChart.AXIS.TIME,
     *             position: CartesianChart.POSITION.BOTTOM,
     *             title: 'Order Date',
     *             tickFormat: 'yyyy/MM'
     *         }],
     *         //define the actual data series.
     *         series: [{
     *             type: CartesianChart.SERIES.BAR,
     *             xField: 'orderDate',
     *             yField: ['north', 'south', 'east', 'west'],
     *             title: ['North', 'South', 'East', 'West'],
     *             tooltips: {
     *                  renderer: function(item, index) {
     *                      var yFields = ['north', 'south', 'east', 'west'];
     *                      var yField = yFields[index];
     *                      return "Revenue in " + formatter.format(item.get('orderDate'), {pattern: "yyyy/MM"}) + ": " + formatter.format(item.get(yField), {pattern: "$#,##0"});
     *                  }
     *              }
     *         }],
     *         title: "Revenue by Month"
     *     }).render();
     * 
     * ##### HTML
     *     <svg name="TemporalScatterChart" style="width:700px;height:400px;">
     *     </svg>
     *      
     *     <svg style="width:700px;height:400px;">
     *          <g name="CategoricalBarChart">
     *          </g>
     *     </svg>
     *
     *     <svg name="TwoYAxisTemporalBarChart" style="width:700px;height:400px;">
     *     </svg>
     *
     *     <svg name="StackedTemporalBarChart" style="width:700px;height:400px;">
     *     </svg>
     *
     * #### JSON
     *     URL: orderCubes/orderDate
     *     Response:
     *     {
     *       "status" : 200,
     *       "items" : [{
     *           "amount": 1200,
     *           "cost": 800,
     *           "goal": 2000,
     *           "orderDate": "2015-03-04T00:00:00.000+0800"
     *       }, {
     *           "amount": 1500,
     *           "cost": 900,
     *           "orderDate": "2015-03-05T00:00:00.000+0800"
     *       }, {
     *           "amount": 800,
     *           "cost": 480,
     *           "orderDate": "2015-03-06T00:00:00.000+0800"
     *       }, {
     *           "amount": 2200,
     *           "cost": 1300,
     *           "orderDate": "2015-03-9T00:00:00.000+0800"
     *       }, {
     *           "amount": 800,
     *           "cost": 500,
     *           "orderDate": "2015-03-10T00:00:00.000+0800"
     *       }, {
     *           "amount": 1200,
     *           "cost": 860,
     *           "orderDate": "2015-03-11T00:00:00.000+0800"
     *       }, {
     *           "amount": 2300,
     *           "cost": 1430,
     *           "goal": 2000,
     *           "orderDate": "2015-03-12T00:00:00.000+0800"
     *       }]
     *     }
     *     
     *     URL: orderCubes/area
     *     Response: 
     *     {
     *       "status" : 200,
     *       "items" : [{
     *         "amount": 1200,
     *         "cost": 1000,
     *         "area": "north"
     *       }, {
     *           "amount": 800,
     *           "cost": 750,
     *           "area": "south"
     *       }, {
     *           "amount": 2200,
     *           "cost": 1800,
     *           "area": "east"
     *       }, {
     *           "amount": 2300,
     *           "cost": 1830,
     *           "area": "west"
     *       }]
     *     }
     *
     *     URL: orderCubes/orderMonth
     *     Response:
     *     {
     *       "status" : 200,
     *       "items" : [{
     *           "north": 21200,
     *           "east": 6800,
     *           "south": 4300,
     *           "west": 32000,
     *           "orderDate": "2015-03-01T00:00:00.000+0800"
     *       }, {
     *           "north": 41200,
     *           "east": 4800,
     *           "south": 7300,
     *           "west": 52000,
     *           "orderDate": "2015-04-01T00:00:00.000+0800"
     *       }, {
     *           "north": 21200,
     *           "east": 8500,
     *           "south": 7300,
     *           "west": 28000,
     *           "orderDate": "2015-05-01T00:00:00.000+0800"
     *       }, {
     *           "north": 31200,
     *           "east": 6800,
     *           "south": 4300,
     *           "west": 20800,
     *           "orderDate": "2015-06-01T00:00:00.000+0800"
     *       }, {
     *           "north": 41200,
     *           "east": 10800,
     *           "south": 8300,
     *           "west": 72000,
     *           "orderDate": "2015-07-01T00:00:00.000+0800"
     *       }, {
     *           "north": 41200,
     *           "east": 6800,
     *           "south": 9300,
     *           "west": 32000,
     *           "orderDate": "2015-09-01T00:00:00.000+0800"
     *       }, {
     *           "north": 41200,
     *           "east": 8000,
     *           "south": 7300,
     *           "west": 22000,
     *           "orderDate": "2015-10-01T00:00:00.000+0800"
     *       }, {
     *           "north": 11200,
     *           "east": 8020,
     *           "south": 3300,
     *           "west": 12000,
     *           "orderDate": "2015-11-01T00:00:00.000+0800"
     *       }, {
     *           "north": 12200,
     *           "east": 8000,
     *           "south": 3600,
     *           "west": 20300,
     *           "orderDate": "2015-12-01T00:00:00.000+0800"
     *       }, {
     *           "north": 21200,
     *           "east": 8800,
     *           "south": 9300,
     *           "west": 12000,
     *           "orderDate": "2015-01-01T00:00:00.000+0800"
     *       }, {
     *           "north": 30200,
     *           "east": 40000,
     *           "south": 10000,
     *           "west": 20200,
     *           "orderDate": "2015-02-01T00:00:00.000+0800"
     *       }]
     *     }
     * 
     * ##### Events:
     * + 'transition' - [data()]{@link CartesianChart#data}
     *
     * @constructor CartesianChart
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {Collection} [config.data=new Collection()] - A collection to specify the models to display.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._component = d3.select(this.el);
        this._axes = {};
        this._series = [];
        this._anchors = {};

        this._color = d3.scale.category10();

        cfg.width && this.width(cfg.width); cfg.height && this.height(cfg.height);
        cfg.bandWidth && this.bandWidth(cfg.bandWidth);
        this.el.style.width || (this.el.style.width = '100%'); this.el.style.height || (this.el.style.height = '100%');

        (cfg.data) ? this.data(cfg.data, {render: false}) : this.data(new Collection(), {render: false});


        var anchors = this._anchors;
        anchors.chart = this._component.append('g').attr('name', 'chart');
        anchors.canvas = anchors.chart.append('g').attr('name', 'canvas');
        anchors.legend = anchors.chart.append('g').attr('name', 'legend');
        anchors.title = anchors.chart.append('text').style({
            'font-size': '20px',
            'text-anchor': 'middle',
            'fill': this.color()
        }).attr('name', 'title');
        this.title(cfg.title);

        anchors.axes = [];

        _.each(cfg.axes, function(cfg) {
            var selection = anchors.chart.append('g');

            var axis = new Axis({
                el: selection.node(),
                type: cfg.type,
                position: cfg.position,
                tickFormat: cfg.tickFormat,
                title: cfg.title,
                unit: cfg.unit,
                style: {
                    'font-size': '12px',
                    stroke: this.color(),
                    fill: this.color()
                }
            });
            this._axes[cfg.position] = axis;
            anchors.axes[cfg.position] = selection;
        }, this);

        _.each(cfg.series, function(cfg) {
            this.addSeries(cfg);
        }, this);

        this._tooltips = new Tooltips({
            el: anchors.canvas.append('g').attr('role', 'tooltips').node(),
            text: ""
        });

        this._toolbar = new Toolbar({
            el: anchors.chart.append('svg').attr('role', 'toolbar').node(),
            data: cfg.actions,
            style: {
                'fill': this.color(),
                'opacity': 0.8
            }
        });

        this._legend = new Legend({
            el: anchors.legend.node()
        });

        this.listenTo(this._toolbar, 'click', onToolbarClick);

        this.listenTo(this.el, 'mouseenter', onMouseenter);
        this.listenTo(this.el, 'mouseleave', onMouseleave);
        this.listenTo(this.el, 'click', onClick);
        this.render();
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        var anchors = this._anchors;
        var axes = this._axes;
        var series = this._series;

        measure.call(this);
        layout.call(this);
        coordinate.call(this);

        _.each(axes, function(axis) {
            axis.render();
        }, this);

        var numberOfBars = _.reduce(series, function(numberOfBars, sequence) {
            (sequence instanceof BarSeries) && numberOfBars++;
            return numberOfBars;
        }, 0);

        var barSize, barIndex = 0, padding;
        var rangeBand;
        if(numberOfBars > 0) {
            var axis = axes[CartesianChart.POSITION.BOTTOM];
            rangeBand = axis.rangeBand() * this.bandWidth();
            barSize = rangeBand / numberOfBars - 2;
        }

        // reorder the rendering sequence to prevent BarSeries from covering LineSeries &  LineSeries covering ScatterSeries.
        series = _.sortBy(series, function(sequence) {
            if(sequence instanceof BarSeries) return 0;
            else if(sequence instanceof LineSeries) return 1;
            else if(sequence instanceof BaselineSeries) return 1;
            else return 2;
        });

        _.each(series, function(sequence) {
            if(sequence instanceof BarSeries) {
                sequence.bar({
                    size: barSize
                });
                if(numberOfBars > 1) {
                    var dx = barSize*(barIndex - (numberOfBars - 1) / 2);
                    d3.select(sequence.el).attr('transform', 'translate(' + dx + ',0)');
                    barIndex++;
                }
            }
            // change the z-index
            sequence.detach(); sequence.attach(anchors.canvas.node());
            sequence.render();

        }, this);

        this._toolbar.render();
        this._toolbar.hide();

        var box = this.el.getBoundingClientRect();
        this._legend.maxWidth((box.width - this._margin.left - this._margin.right) * 0.8);
        this._legend.data(this.legend());

        var axisBox = this._axes.BOTTOM.el.getBoundingClientRect();
        var dx = this._margin.left + (box.width - this._margin.left - this._margin.right - this._legend.width()) / 2;
        var dy = box.height - this._margin.bottom + axisBox.height + 8 ;
        this._legend.attr('transform', 'translate(' + dx + ',' + dy + ')');

        //
        this._tooltips.detach(); this._tooltips.attach(anchors.canvas.node());


        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function measure() {
        var series = this._series;
        var axes = this._axes;
        var data = this._data;

        _.each(axes, function(axis) {
            var scale = axis.scale();
            var extent = [];

            _.each(series, function(sequence) {
                var fields;
                if(sequence.xAxis()===axis) {
                    // xField does not support stacked fields.
                    fields = _.isArray(sequence.xField()) ? sequence.xField() : [sequence.xField()];
                } else if(sequence.yAxis()===axis) {
                    fields = _.isArray(sequence.yField()) ? sequence.yField() : [sequence.yField()];
                } else {
                    return;
                }
                if(axis.type()===CartesianChart.AXIS.NUMERICAL) {
                    extent = data.reduce(function(extent, item) {
                        var value = _.reduce(fields, function(sum, field){
                            var value = item[field] ? item[field]() : item.get(field);
                            value || (value = 0);
                            return sum + value;
                        }, 0);
                        extent[0] = (extent[0]===undefined) ? value : Math.min(extent[0], value);
                        extent[1] = (extent[1]===undefined) ? value : Math.max(extent[1], value);
                        return extent;
                    }, extent);
                } else if(axis.type()===CartesianChart.AXIS.TIME) {
                    // Time axes do not support stacked fields;
                    var field = _.first(fields);
                    extent = data.reduce(function(extent, item) {
                        var value = item[field] ? item[field]() : item.get(field);
                        if(!_.isDate(value)) return extent;
                        extent[0] = extent[0] ? new Date(Math.min(extent[0], value)) : value;
                        extent[1] = extent[1] ? new Date(Math.max(extent[1], value)) : value;
                        return extent;
                    }, extent);
                } else {
                    // Categorical axes do not support stacked fields;
                    var field = _.first(fields);
                    extent = _.union(extent, data.map(function(item) {
                        return item[field] ? item[field]() : item.get(field);
                    }));
                }
            });
            if(axis.type()===CartesianChart.AXIS.NUMERICAL
                && (axis.position()===CartesianChart.POSITION.LEFT || axis.position()===CartesianChart.POSITION.RIGHT)) {
                extent[0] = 0;
            }
            axis.domain(extent);
        });
    }

    function layout() {
        var margin = {top: PADDING*2, right: PADDING*2, bottom: PADDING*2, left: PADDING*2};
        var anchors = this._anchors;
        var axes = this._axes;
        var sizes = {
            axes: {}
        };

        // reserve a space for legend;
        margin.bottom += 24;

        _.each(axes, function(axis, position) {
            var size = axis.sizing();
            if(position===CartesianChart.POSITION.LEFT||position===CartesianChart.POSITION.RIGHT) {
                margin[position.toLowerCase()] = margin[position.toLowerCase()] + size.width + PADDING;
            } else {
                margin[position.toLowerCase()] = margin[position.toLowerCase()] + size.height + PADDING;
            }
            sizes.axes[position.toLowerCase()] = size;
        });

        if(this.title() && this.title().length>0) {
            var size = dom.sizing(anchors.title);
            margin.top = margin.top + size.height + PADDING;
            sizes.title = size;
        }

        var width = this.width() - margin.left - margin.right;
        var height = this.height() - margin.top - margin.bottom;

        anchors.canvas.attr({
            'transform': 'translate(' + margin.left + ',' + margin.top + ')'
        });

        if(this.title() && this.title().length>0) {
            anchors.title.attr({
                x: margin.left + width / 2,
                y: PADDING + sizes.title.height
            });
        }

        _.each(axes, function(axis, position) {
            var node = anchors.axes[position];
            var dx, dy;
            if(position===CartesianChart.POSITION.LEFT) {
                dx = margin.left; dy = margin.top;
            } else if(position===CartesianChart.POSITION.RIGHT) {
                dx = margin.left + width; dy = margin.top;
            } else if(position===CartesianChart.POSITION.BOTTOM) {
                dx = margin.left; dy = margin.top + height;
            }
            node.attr('transform', 'translate(' + dx + ',' + dy +')');
        });


        this._toolbar.attr({
            'x': margin.left + PADDING,
            'y': margin.top
        });

        this._margin = margin;
    }


    function title(text) {
        var anchors = this._anchors;

        if(arguments.length===0) {
            return anchors.title.text();
        }

        anchors.title.text(i18n.translate(text));

        if(text && text.length>0) {
            anchors.title.style('display', '')
        } else {
            anchors.title.style('display', 'none');
        }
    }

    function coordinate() {
        var axes = this._axes;
        var margin = this._margin;
        var series = this._series;
        var width = this.width() - margin.left - margin.right;
        var height = this.height() - margin.top - margin.bottom;

        var numberOfBars = _.reduce(series, function(numberOfBars, sequence) {
            (sequence instanceof BarSeries) && numberOfBars++;
            return numberOfBars;
        }, 0);
        
        _.each(axes, function(axis) {
            var scale = axis.scale();
            var range, padding;

            if(axis.position()===CartesianChart.POSITION.BOTTOM && axis.type()===CartesianChart.AXIS.NUMERICAL) {
                axis.ticks(this.data().size());
            }

            if(axis.position()===CartesianChart.POSITION.LEFT||axis.position()===CartesianChart.POSITION.RIGHT) {
                range = [height, 0];
            } else {
                range = [0, width];
            }

            if(axis.type()===CartesianChart.AXIS.CATEGORICAL) {
                padding = 1;
            } else if(numberOfBars>0 && axis.position()===CartesianChart.POSITION.BOTTOM) {
                padding = 2;
            }
            scale.range(range); axis.padding(padding);
        }, this);

    }
    
    function addSeries(config) {
        config || (config = {});
        var color;
        if(_.isArray(config.yField)) {
            color = [];
            _.each(config.yField, function(field) {
                color.push(this.color(++this._colorIndex));
            }, this);
        } else {
            color = this.color(++this._colorIndex);
        }

        var defautls = {
            type : CartesianChart.SERIES.SCATTER,
            xAxis: CartesianChart.POSITION.BOTTOM,
            yAxis: CartesianChart.POSITION.LEFT,
            color: color
        };

        _.defaults(config, defautls);

        var anchors = this._anchors;
        var selection = anchors.canvas.append('g');

        config.el = selection.node();
        config.data = this._data;
        config.xAxis = this._axes[config.xAxis];
        config.yAxis = this._axes[config.yAxis];

        if(config.yAxis)

        var series;
        if(config.type===CartesianChart.SERIES.SCATTER) {
            series = new ScatterSeries(config);
        } else if(config.type===CartesianChart.SERIES.LINE) {
            series = new LineSeries(config);
        } else if(config.type===CartesianChart.SERIES.BAR) {
            series = new BarSeries(config);
        } else if(config.type===CartesianChart.SERIES.BASELINE) {
            series = new BaselineSeries(config);
        }

        if(series) {
            this._series.push(series);
            this.listenTo(series, "hinton", _.bind(onSeriesHintOn, this));
            this.listenTo(series, "hintoff", _.bind(onSeriesHintOff, this));
            this.isRendered() && this.render();
        }
    }

    function onSeriesHintOn(event) {
        var tooltips = this._tooltips;

        tooltips.css({
            stroke: event.stroke && event.stroke,
            fill: event.fill && event.fill
        });
        tooltips.text(event.data);
        tooltips.show({x: event.x, y: event.y})
    }

    function onSeriesHintOff(event) {
        var tooltips = this._tooltips;

        tooltips.hide();
    }

    function trendline(index) {
        var series = _.isNumber(index) ? this._series[index] : _.find(this._series, function(sequence) {
            return (sequence.title()===index);
        });

        series.trendline();
    }


    function baseline(series) {
        series || (series = {});

        var defautls = {
            xAxis: CartesianChart.POSITION.BOTTOM,
            yAxis: CartesianChart.POSITION.LEFT
        };

        _.defaults(series, defautls);
        series.type = CartesianChart.SERIES.BASELINE;

        this.addSeries(series);
    }


    /**
     *
     * Draw the grid lines.
     *
     * @private
     * @param position
     * @param options
     * @returns {grid}
     */
    function gridline(position, options) {
        options || (options = {});
        var defaults = {
            stroke: 'grey',
            'stroke-width': '0.5px'
        };
        _.defaults(options, defaults);

        //var canvas = this.canvas();

        //canvas.selectAll('[name=' + position + '] g.tick line').attr(options);

        return this;
    }

    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new collection is bound. event.data => the collection.
     *
     * @memberof CartesianChart#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @param {Boolean} [options.render=true] - A false value will prevent the chart from being rendered.
     * @returns {Collection}
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {
            render: true
        };

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = items;

        this.listenTo(this._data, "update reset", _.bind(onItemsUpdate, this));

        options.render && this.render();

        var event = {
            context: this,
            source: this,
            data: items
        };

        (options.silent) || this.trigger('transition', event);

        return this._data;
    }

    function onItemsUpdate() {
        this.render();
    }

    function onMouseenter(event) {
        if(event.target!==this.el) return;

        var el = dom.select(event.target);
        if(event.clientX<20||el.width()-event.clientX<20) {
            this._toolbar.show();
        } else if(event.clientY<20||el.height()-event.clientY<20) {
            this._toolbar.show();
        }

    }

    function onMouseleave(event) {
        if(event.target!==this.el) return;

        var el = dom.select(event.target);
        if(event.clientX<20||el.width()-event.clientX<20) {
            this._toolbar.hide();
        } else if(event.clientY<20||el.height()-event.clientY<20) {
            this._toolbar.hide();
        }
    }

    function onClick(event) {
        if(event.target!==this.el) return;

        this._toolbar.isVisible() ? this._toolbar.hide() : this._toolbar.show();
    }

    function onToolbarClick(event) {
        var action = event.value.toUpperCase();

        if(action===CartesianChart.Action.LEGEND) {
            alert("show legend");
        } else if(action===CartesianChart.Action.SETTINGS) {
            alert("show settings");
        } else {
            this.trigger('action', {
                context: this,
                source: this,
                value: event.value
            });
        }
    }

    function canvas() {
        return this._canvas;
    }

    /**
     * @override
     */
    function width(value) {
        if(!value) {
            var width = this.el.getBoundingClientRect().width;
            width || (width = this.$el.width());
            width || (width = this.el.style.width);
            width || (width = WIDTH);

            return width;
        }


        this.el.style.width = value;
    }

    /**
     * @override
     */
    function height(value) {
        if(!value) {
            var height = this.el.getBoundingClientRect().height;
            height || (height = this.$el.height());
            height || (height = this.el.style.height);
            height || (height = HEIGHT);

            return height;
        }


        this.el.style.width = value;
    }

    function color(index) {
        return this._color(index)
    }

    function bandWidth(bandWidth) {
        if(!bandWidth && bandWidth!==0) return this._bandWidth;

        this._bandWidth = bandWidth;
    }

    function legend() {
        var legend = [];

        _.each(this._series, function(sequence) {
            var titles = _.isArray(sequence.title()) ? sequence.title() : [sequence.title()];
            var colors = _.isArray(sequence.color()) ? sequence.color() : [sequence.color()];

            _.each(titles, function(title, i) {
                legend.push({
                    title: title,
                    color: colors[i]
                });
            });
        });

        return legend;
    }

    var props = {
        _components: undefined,
        _title: undefined,
        _data: undefined,
        _axes: undefined,
        _margin: undefined,
        _measurement: undefined,
        _color: undefined,
        _tooltips: undefined,
        _toolbar: undefined,
        _colorIndex: 0,
        _bandWidth: 0.7,
        tagName: 'g'

    };

    var CartesianChart = declare(Base, {
        initialize: initialize,
        render: render,
        data: data,
        title: title,
        addSeries: addSeries,
        trendline: trendline,
        baseline: baseline,
        gridline: gridline,
        height: height,
        width: width,
        bandWidth: bandWidth,
        color: color,
        legend: legend
    }, props);

    CartesianChart.AXIS = {
        NUMERICAL: "NUMERICAL",
        TIME: "TIME",
        CATEGORICAL: "CATEGORICAL"
    };

    CartesianChart.TICKS = {
        YEARS: "YEARS",
        MONTHS: "MONTHS",
        DAYS: "DAYS",
        HOURS: "HOURS",
        MINUTES: "MINUTES",
        WEEKS: "WEEKS"
    };

    CartesianChart.SERIES = {
        LINE: "LINE",
        SCATTER: "SCATTER",
        BAR: "BAR",
        BASELINE: "BASELINE"
    };

    CartesianChart.POSITION = {
        LEFT: "LEFT",
        TOP: "TOP",
        BOTTOM: "BOTTOM",
        RIGHT: "RIGHT"
    };

    CartesianChart.Action = {
        SETTINGS: "SETTINGS",
        LEGEND: "LEGEND"
    };

    return CartesianChart;

});