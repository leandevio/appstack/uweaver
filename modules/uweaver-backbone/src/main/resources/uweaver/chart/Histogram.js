define(['uweaver/lang', 'uweaver/chart/CartesianChart'], function(lang, CartesianChart) {
    var declare = lang.declare;
    var Base = CartesianChart;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this.bandWidth(1);
    }

    var props = {};

    var Histogram = declare(Base, {
        initialize: initialize
    }, props);

    return Histogram;
});