/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'ss', 'uweaver/lang', 'uweaver/dom', 'uweaver/widget/Widget',
    'uweaver/data/Collection', './axis/RadialAxis', './axis/AngularAxis', './series/PieSeries', './series/RadarSeries',
    './interaction/Tooltips', 'uweaver/graphic/Toolbar'], function(_, $, d3, ss, lang, dom, Widget, Collection,
    RadialAxis, AngularAxis, PieSeries, RadarSeries, Tooltips, Toolbar) {

    var declare = lang.declare;
    var Base = Widget;

    var PADDING = 10, WIDTH = 200, HEIGHT = 200;

    /**
     * A PolarChart visualizes its data points using the Polar coordinate system.
     *
     * ##### JavaScript
     *
     *     var OrderCubes = declare(Collection, {
     *        resource: 'orderCubes',
     *        area: function(options) {
     *            this.sync('area', {}, options).then(function(response) {
     *                var items = this.parse(response);
     *                this.reset(items);
     *            }, this);
     *        }
     *     });

     *     var cubes = new OrderCubes();
     *
     *     // Generate a pie chart.
     *
     *     cubes.area({async: false});
     *
     *     var pieChartNode = $('[name=pieChart]');
     *     var pieChart = new PolarChart({
     *        el: pieChartNode,
     *        data: cubes,
     *        title: "Revenue by Area",
     *     }).render();
     *
     *     pieChart.addSeries({
     *          type: PolarChart.SERIES.PIE,
     *          angleField: 'amount',
     *          labelField: 'area'
     *     });
     *
     *     // Generate a donut chart
     *
     *     cubes.area({async: false});
     *
     *     var donutChartNode = $('[name=donutChart]');
     *     var donutChart = new PolarChart({
     *        el: donutChartNode,
     *        data: cubes,
     *        title: "Revenue by Area",
     *     }).render();
     *
     *     pieChart.addSeries({
     *          type: PolarChart.SERIES.PIE,
     *          angleField: 'amount',
     *          labelField: 'area',
     *          donut: 0.5,
     *          tooltips: {display: 'none'}
     *     });
     *
     *     // Get the legend of the added series.
     *     var legend = chart.getSeries(0).legend();
     *     expect(legend).to.be.an.instanceof(Array);
     *     expect(legend[0]).to.be.an.instanceof(Object);
     *     expect(legend[0].label).to.equal(cubes.at(0).get('area'));
     *     expect(legend[0].color).to.not.be.empty;
     *     expect(legend[0].percentage).to.be.above(0);
     *
     * ##### HTML
     *     <svg name="pieChart" style="width:700px;height:400px;">
     *     </svg>
     *
     *     <svg style="width:700px;height:400px;">
     *          <g name="donutChart">
     *          </g>
     *     </svg>
     *
     *
     * #### JSON
     *     URL: orderCubes/area
     *     Response:
     *     {
     *       "status" : 200,
     *       "items" : [{
     *         "amount": 1200,
     *         "cost": 1000,
     *         "area": "north"
     *       }, {
     *           "amount": 800,
     *           "cost": 750,
     *           "area": "south"
     *       }, {
     *           "amount": 2200,
     *           "cost": 1800,
     *           "area": "east"
     *       }, {
     *           "amount": 2300,
     *           "cost": 1830,
     *           "area": "west"
     *       }]
     *     }
     *
     *
     * ##### Events:
     * + 'transition' - [data()]{@link PolarChart#data}
     *
     * @constructor PolarChart
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {Collection} [config.data=new Collection()] - A collection to specify the models to display.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};

        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._component = d3.select(this.el);
        this._axes = {};
        this._series = [];
        this._anchors = {};

        this._color = d3.scale.category10();

        cfg.width && this.width(cfg.width); cfg.height && this.height(cfg.height);
        this.el.style.width || (this.el.style.width = '100%'); this.el.style.height || (this.el.style.height = '100%');

        (cfg.data) ? this.data(cfg.data, {render: false}) : this.data(new Collection(), {render: false});

        var anchors = this._anchors;
        anchors.chart = this._component.append('g').attr('name', 'chart');
        anchors.canvas = anchors.chart.append('g').attr('name', 'canvas');
        anchors.title = anchors.chart.append('text').style({
            'font-size': '20px',
            'text-anchor': 'middle',
            'fill': this.color()
        }).attr('name', 'title');
        this.title(cfg.title);

        anchors.axes = [];

        _.each(cfg.axes, function(cfg) {
            var selection = anchors.chart.append('g');
            var axis;
            if(cfg.position===PolarChart.POSITION.ANGULAR) {
                axis = new AngularAxis({
                    el: selection.node(),
                    type: cfg.type,
                    title: cfg.title,
                    tickFormat: cfg.tickFormat,
                    unit: cfg.unit,
                    style: {
                        'font-size': '12px',
                        stroke: this.color(),
                        fill: this.color()
                    }
                });
            } else if(cfg.position===PolarChart.POSITION.RADIAL) {
                axis = new RadialAxis({
                    el: selection.node(),
                    type: cfg.type,
                    tickFormat: cfg.tickFormat,
                    title: cfg.title,
                    unit: cfg.unit,
                    style: {
                        'font-size': '12px',
                        stroke: this.color(),
                        fill: this.color()
                    }
                });
            }

            this._axes[cfg.position] = axis;
            anchors.axes[cfg.position] = selection;
        }, this);

        if(!_.has(this._axes, PolarChart.POSITION.RADIAL)) {
            var selection = anchors.chart.append('g');
            var axis = new RadialAxis({
                el: selection.node(),
                style: {
                    'font-size': '12px',
                    stroke: this.color(),
                    fill: this.color()
                }
            });
            this._axes[PolarChart.POSITION.RADIAL] = axis;
            anchors.axes[PolarChart.POSITION.RADIAL] = selection;
        }

        if(!_.has(this._axes, PolarChart.POSITION.ANGULAR)) {
            var selection = anchors.chart.append('g');
            var axis = new AngularAxis({
                el: selection.node(),
                style: {
                    'font-size': '12px',
                    stroke: this.color(),
                    fill: this.color()
                }
            });
            this._axes[PolarChart.POSITION.ANGULAR] = axis;
            anchors.axes[PolarChart.POSITION.ANGULAR] = selection;
        }

        _.each(cfg.series, function(cfg) {
            this.addSeries(cfg);
        }, this);

        this._tooltips = new Tooltips({
            el: anchors.canvas.append('g').node(),
            text: ""
        });

        this._toolbar = new Toolbar({
            el: anchors.chart.append('svg').attr('role', 'toolbar').node(),
            data: cfg.actions,
            style: {
                'fill': this.color(),
                'opacity': 0.8
            }
        });

        this.listenTo(this._toolbar, 'click', onToolbarClick);

        this.listenTo(this.el, 'mouseenter', onMouseenter);
        this.listenTo(this.el, 'mouseleave', onMouseleave);
        this.listenTo(this.el, 'click', onClick);
        this.render();
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        var anchors = this._anchors;
        var axes = this._axes;
        var series = this._series;

        // optimize axes' domain.
        measure.call(this);

        layout.call(this);

        // optimize axes' range.
        coordinate.call(this);

        /**
         * @todo render axes
         */

        // render series.
        _.each(series, function(sequence) {
            sequence.render();

        }, this);


        this._toolbar.render();
        this._toolbar.hide();

        this._tooltips.detach(); this._tooltips.attach(anchors.canvas.node());

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function measure() {
        var series = this._series;
        var axes = this._axes;
        var data = this._data;

        _.each(axes, function(axis) {
            var extent = [];

            _.each(series, function(sequence) {
                var field;
                if(sequence.angularAxis()===axis) {
                    field = sequence.angleField();
                } else if(sequence.radialAxis()===axis) {
                    field = sequence.radiusField();
                } else {
                    return;
                }
                if(axis.type()===PolarChart.AXIS.NUMERICAL) {
                    extent = data.reduce(function(extent, item) {
                        var value = item[field] ? item[field]() : item.get(field);
                        extent[0] = extent[0] ? Math.min(extent[0], value) : value;
                        extent[1] = extent[1] ? Math.max(extent[1], value) : value;
                        return extent;
                    }, extent);
                } else if(axis.type()===PolarChart.AXIS.TIME) {
                    extent = data.reduce(function(extent, item) {
                        var value = item[field] ? item[field]() : item.get(field);
                        extent[0] = extent[0] ? Math.min(extent[0], value) : value;
                        extent[1] = extent[1] ? Math.max(extent[1], value) : value;
                        return extent;
                    }, extent);
                } else {
                    extent = _.union(extent, data.map(function(item) {
                        return item[field] ? item[field]() : item.get(field);
                    }));
                }
            });
            axis.domain(extent);
        });
    }

    function layout() {
        var margin = {top: PADDING*2, right: PADDING*2, bottom: PADDING*2, left: PADDING*2};
        var anchors = this._anchors;
        var axes = this._axes;
        var sizes = {};

        if(this._toolbar.size()>0) {
            margin.bottom = margin.bottom + 24;
        }

        _.each(axes, function(axis) {
            if(axis instanceof AngularAxis) return;
            var size = axis.sizing();
            var margin = size.outerRadius - size.innerRadius;
            margin.top = margin.top + margin;
            margin.right = margin.right + margin;
            margin.bottom = margin.bottom + margin;
            margin.left = margin.left + margin;
        });

        if(this.title() && this.title().length>0) {
            var size = dom.sizing(anchors.title);
            margin.top = margin.top + size.height + PADDING;
            sizes.title = size;
        }

        var width = this.width() - margin.left - margin.right;
        var height = this.height() - margin.top - margin.bottom;

        anchors.canvas.attr({
            'transform': 'translate(' + (margin.left + width / 2) + ',' + (margin.top + height / 2) + ')'
        });

        if(this.title() && this.title().length>0) {
            anchors.title.attr({
                x: margin.left + width / 2,
                y: PADDING + sizes.title.height
            });
        }

        this._toolbar.attr({
            'x': (this.width() - this._toolbar.size()*24)/2,
            'y': this.height() - margin.bottom
        });

        this._margin = margin;
    }

    function title(text) {
        var anchors = this._anchors;

        if(arguments.length===0) {
            return anchors.title.text();
        }

        anchors.title.text(text);

        if(text && text.length>0) {
            anchors.title.style('display', '')
        } else {
            anchors.title.style('display', 'none');
        }
    }

    /**
     * optimize axes' range.
     */
    function coordinate() {
        var radialAxis = this._axes[PolarChart.POSITION.RADIAL];
        var margin = this._margin;
        var width = this.width() - margin.left - margin.right;
        var height = this.height() - margin.top - margin.bottom;
        var radius = Math.min(height, width) / 2;
        var range = [0, radius];

        radialAxis.range(range);
    }

    function addSeries(config) {
        config || (config = {});
        var color = this.color(this._series.length);

        var defautls = {
            style: {
                fill: color,
                stroke: color
            }
        };

        _.defaults(config, defautls);

        var anchors = this._anchors;
        var selection = anchors.canvas.append('g');

        config.el = selection.node();
        config.data = this._data;
        config.angularAxis = this._axes[PolarChart.POSITION.ANGULAR];
        config.radialAxis = this._axes[PolarChart.POSITION.RADIAL];
        config.threshold = this.cfg().threshold;

        var series;
        if(config.type===PolarChart.SERIES.PIE) {
            series = new PieSeries(config);
        } else if(config.type===PolarChart.SERIES.RADAR) {
            series = new RadarSeries(config);
        }

        if(series) {
            this._series.push(series);
            this.listenTo(series, "hinton", _.bind(onSeriesHintOn, this));
            this.listenTo(series, "hintoff", _.bind(onSeriesHintOff, this));
            this.isRendered() && this.render();
        }
    }

    function onSeriesHintOn(event) {
        var tooltips = this._tooltips;

        tooltips.css({
            stroke: event.stroke && event.stroke,
            fill: event.fill && event.fill
        });
        tooltips.text(event.data);
        tooltips.show({x: event.x, y: event.y})
    }

    function onSeriesHintOff(event) {
        var tooltips = this._tooltips;

        tooltips.hide();
    }

    function getSeries(index) {
        return this._series[index];
    }

    function clear() {

        _.each(this._series, function(series) {
            series.destroy();
        });

        this._series = [];
    }

    /**
     *
     * Draw the grid lines.
     *
     * @private
     * @param position
     * @param options
     * @returns {grid}
     */
    function gridline(position, options) {
        options || (options = {});
        var defaults = {
            stroke: 'grey',
            'stroke-width': '0.5px'
        };
        _.defaults(options, defaults);

        //var canvas = this.canvas();

        //canvas.selectAll('[name=' + position + '] g.tick line').attr(options);

        return this;
    }



    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new collection is bound. event.data => the collection.
     *
     * @memberof PolarChart#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @param {Boolean} [options.render=true] - A false value will prevent the chart from being rendered.
     * @returns {Collection}
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {
            render: true
        };

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = items;

        this._dataset = items.toArray();

        this.listenTo(this._data, "update reset", _.bind(onItemsUpdate, this));

        options.render && this.render();

        var event = {
            context: this,
            source: this,
            data: items
        };

        (options.silent) || this.trigger('transition', event);

        return this._data;
    }

    function onItemsUpdate() {
        this.render();
    }

    function onMouseenter(event) {
        if(event.target!==this.el) return;

        var el = dom.select(event.target);
        if(event.clientX<20||el.width()-event.clientX<20) {
            this._toolbar.show();
        } else if(event.clientY<20||el.height()-event.clientY<20) {
            this._toolbar.show();
        }

    }

    function onMouseleave(event) {
        if(event.target!==this.el) return;

        var el = dom.select(event.target);
        if(event.clientX<20||el.width()-event.clientX<20) {
            this._toolbar.hide();
        } else if(event.clientY<20||el.height()-event.clientY<20) {
            this._toolbar.hide();
        }
    }

    function onClick(event) {
        if(event.target!==this.el) return;

        this._toolbar.isVisible() ? this._toolbar.hide() : this._toolbar.show();
    }

    function onToolbarClick(event) {
        var action = event.value.toUpperCase();

        if(action===PolarChart.Action.LEGEND) {
            alert("show legend");
        } else if(action===PolarChart.Action.SETTINGS) {
            alert("show settings");
        } else {
            this.trigger('action', {
                context: this,
                source: this,
                value: event.value
            });
        }
    }

    function refresh() {

    }

    function canvas() {
        return this._canvas;
    }

    /**
     * @override
     */
    function width(value) {
        if(!value) {
            var width = this.el.getBoundingClientRect().width;
            width || (width = this.$el.width());
            width || (width = this.el.style.width);
            width || (width = WIDTH);

            return width;
        }


        this.el.style.width = value;
    }

    /**
     * @override
     */
    function height(value) {
        if(!value) {
            var height = this.el.getBoundingClientRect().height;
            height || (height = this.$el.height());
            height || (height = this.el.style.height);
            height || (height = HEIGHT);

            return height;
        }


        this.el.style.width = value;
    }

    function color(index) {
        return this._color(index)
    }

    function legend() {
        var legend = [];

        _.each(this._series, function(sequence) {
            legend.push({
                title: sequence.title(),
                color: sequence.color()
            });
        });

        return legend;
    }

    var props = {
        _components: undefined,
        _title: undefined,
        _data: undefined,
        _axes: undefined,
        _margin: undefined,
        _measurement: undefined,
        _color: undefined,
        _tooltips: undefined,
        _toolbar: undefined,
        tagName: 'g'

    };

    var PolarChart = declare(Base, {
        initialize: initialize,
        render: render,
        data: data,
        title: title,
        addSeries: addSeries,
        color: color,
        height: height,
        width: width,
        clear: clear,
        getSeries: getSeries,
        legend: legend
    }, props);

    PolarChart.AXIS = {
        NUMERICAL: "NUMERICAL",
        CATEGORICAL: "CATEGORICAL"
    };

    PolarChart.SERIES = {
        PIE: "PIE",
        RADAR: "RADAR"
    };

    PolarChart.POSITION = {
        ANGULAR: "ANGULAR",
        RADIAL: "RADIAL"
    };

    PolarChart.Action = {
        SETTINGS: "SETTINGS",
        LEGEND: "LEGEND"
    };

    return PolarChart;

});