/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang', 'uweaver/widget/Widget',
    'uweaver/scale/LinearScale', 'uweaver/scale/TimeScale',
    'uweaver/scale/OrdinalScale', 'uweaver/formatter'], function(_, $, d3, lang, Widget, LinearScale, TimeScale, OrdinalScale, formatter) {

    var declare = lang.declare;
    var Base = Widget;
    var PADDING = 10;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            type: AngularAxis.TYPE.NUMERICAL,
            range: [0, Math.PI * 2],
            style: {
                'font-size': '12px',
                'fill': 'black'
            },
            tickPadding: 3,
            tickSize: 0
        };
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        if(cfg.type===AngularAxis.TYPE.NUMERICAL) {
            this._scale = new LinearScale();
        } else if(cfg.type===AngularAxis.TYPE.TIME) {
            this._scale = new TimeScale();
        } else {
            this._scale = new OrdinalScale();
        }

        this._component = d3.select(this.el);
        this._anchors = {};
        cfg.tickFormat && this.tickFormat(cfg.tickFormat);
        cfg.ticks && this.ticks(cfg.ticks);
        this.range(cfg.range);
        this.tickPadding(cfg.tickPadding);
        this.tickSize(cfg.tickSize);
        this.css(cfg.style);

        var anchors = this._anchors;
        anchors.axis = this._component.append('g');
        anchors.title = this._component.append('text').style({
            'text-anchor': 'middle',
            'font-size': '14px',
            'stroke': 'none'
        });

        this.title(cfg.title);
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    function scale() {
        return this._scale;
    }

    function tickFormat(format) {
        if(!format) return this._tickFormat;

        this._tickFormat = format;
        this._autoTickFormat = false;
    }

    function tickSize(size) {
        if(!size) return this._tickSize;

        this._tickSize = size;
    }

    function ticks(ticks) {
        if(!ticks) return this._ticks;

        this._ticks = ticks;
        this._autoTicks = false;
    }

    function type() {
        return this.cfg().type;
    }

    function sizing() {
        var outerRadius = _.last(this.range());
        var innerRadius = _.last(this.range());
        
        return {
            innerRadius: innerRadius,
            outerRadius: outerRadius
        };
    }

    function title(text) {
        var anchors = this._anchors;

        if(arguments.length===0) {
            return anchors.title.text();
        }

        anchors.title.text(text);

        if(text && text.length>0) {
            anchors.title.style('display', '')
        } else {
            anchors.title.style('display', 'none');
        }
    }

    function autoTicks() {
        var domain = this.scale().domain();

        if(this.type()===AngularAxis.TYPE.TIME) {
            domain.length<=0 && (domain = [new Date(0), new Date(0)]);
            var start = _.first(domain), end = _.last(domain);
            if(start.getHours()!==end.getHours()) {
                this.ticks(AngularAxis.TICKS.HOURS);
            } else if(start.getDate()!==end.getDate()) {
                this.ticks(AngularAxis.TICKS.DAYS);
            } else if(start.getMonth()!==end.getMonth()) {
                this.ticks(AngularAxis.TICKS.MONTHS);
            } else {
                this.ticks(AngularAxis.TICKS.YEARS);
            }
        } else if(this.type()===AngularAxis.TYPE.NUMERICAL) {
            this.ticks(10);

        } else {
            this.ticks(10);
        }

        this._autoTicks = true;
    }

    function autoTickFormat() {
        var domain = this.scale().domain();

        if(this.type()===AngularAxis.TYPE.TIME) {
            if(this.ticks()===AngularAxis.TICKS.YEARS) {
                this.tickFormat('yyyy');
            } else if(this.ticks()===AngularAxis.TICKS.MONTHS) {
                this.tickFormat('yyyy/MM');
            } else if(this.ticks()===AngularAxis.TICKS.DAYS) {
                this.tickFormat('MM/dd');
            } else if(this.ticks()===AngularAxis.TICKS.HOURS) {
                this.tickFormat('hh');
            } else if(this.ticks()===AngularAxis.TICKS.MINUTES) {
                this.tickFormat('hh:mm');
            } else {
                this.tickFormat('MM/dd');
            }
        } else if(this.type()===AngularAxis.TYPE.NUMERICAL) {
            domain.length<=0 && (domain = [0]);
            var max = _.last(domain);
            if(Math.round(max)===max) {
                this.tickFormat('#,##0');
            } else {
                this.tickFormat('#,##0.00');
            }
        }

        this._autoTickFormat = true;
    }

    function tickPadding(padding) {
        if(!padding) return this._tickPadding;

        this._tickPadding = padding;
    }
    

    function domain(domain) {
        var scale = this._scale;
        if(!domain) return scale.domain();

        scale.domain(domain);

        this._autoTicks && this.autoTicks();
        this._autoTickFormat && this.autoTickFormat();
    }

    function range(range) {
        var scale = this._scale;
        if(!range) return scale.range();

        scale.range(range);
    }


    function project(value) {
        return this.scale().project(value);
    }

    function format(value) {
        return formatter.format(value, {pattern: this._tickFormat})
    }

    var props = {
        _size: {},
        _axis: undefined,
        _scale: undefined,
        _ticks: undefined,
        _tickFormat: undefined,
        _autoTicks: true,
        _autoTickFormat: true,
        tagName: 'g'
    };

    var AngularAxis = declare(Base, {
        initialize: initialize,
        render: render,
        type: type,
        scale: scale,
        autoTicks: autoTicks,
        autoTickFormat: autoTickFormat,
        sizing: sizing,
        ticks: ticks,
        tickFormat: tickFormat,
        tickSize: tickSize,
        title: title,
        tickPadding: tickPadding,
        domain: domain,
        range: range,
        project: project,
        format: format
    }, props);

    AngularAxis.TYPE = {
        NUMERICAL: "NUMERICAL",
        TIME: "TIME",
        CATEGORICAL: "CATEGORICAL"
    };

    AngularAxis.TICKS = {
        DAYS: 'DAYS',
        WEEKS: 'WEEKS',
        MONTHS: 'MONTHS',
        YEARS: 'YEARS',
        HOURS: 'HOURS',
        MINUTES: 'MINUTES',
        WEEKS: 'WEEKS'
    };

    return AngularAxis;

});