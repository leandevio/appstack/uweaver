/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang', 'uweaver/i18n', 'uweaver/widget/Widget',
    'uweaver/scale/LinearScale', 'uweaver/scale/TimeScale', 'uweaver/scale/OrdinalScale',
    'uweaver/formatter', 'uweaver/dom'], function(_, $, d3, lang, i18n, Widget, LinearScale, TimeScale, OrdinalScale, formatter, dom) {

    var declare = lang.declare;
    var Base = Widget;
    var PADDING = 10;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        var defaults = {
            type: Axis.TYPE.NUMERICAL,
            position: 'bottom',
            style: {
                'font-size': '12px',
                'fill': 'black'
            },
            tickSize: 6,
            tickPadding: 3
        };
        var cfg = this.cfg();
        
        _.defaults(cfg, defaults);

        if(cfg.type===Axis.TYPE.NUMERICAL) {
            this._scale = new LinearScale();
        } else if(cfg.type===Axis.TYPE.TIME) {
            this._scale = new TimeScale();
        } else {
            this._scale = new OrdinalScale();
        }
        
        this._component = d3.select(this.el);
        this._axis = d3.svg.axis().scale(this.scale()._scale).tickPadding(PADDING);
        this._anchors = {};
        this.position(cfg.position);
        cfg.tickFormat && this.tickFormat(cfg.tickFormat);
        cfg.ticks && this.ticks(cfg.ticks);
        this.tickPadding(cfg.tickPadding);
        this.tickSize(cfg.tickSize);
        this.css(cfg.style);

        var anchors = this._anchors;
        anchors.axis = this._component.append('g');
        anchors.title = this._component.append('text').style({
            'text-anchor': 'middle',
            'font-size': '14px',
            'stroke': 'none'
        });
        anchors.unit = this._component.append('text').style({
            'text-anchor': 'end',
            'font-size': '12px',
            'stroke': 'none'
        });

        this.title(cfg.title);
        this.unit(cfg.unit);
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        var axis = this._axis;
        var anchors = this._anchors;

        anchors.axis.selectAll('path.domain').remove();
        anchors.axis.selectAll('g.stick').remove();
        
        anchors.axis.call(axis);

        anchors.axis.select('path.domain').style({
            fill: 'none'
        });

        anchors.axis.selectAll('g.tick text').style({
            'stroke': 'none'
        });

        this.sizing();
        var axisSize = this._size['axis'];

        var $labels = $(anchors.axis.node()).find('g.tick text');

        var extended = Math.floor(this.padding() / 2);

        _.each($labels, function(label, i) {
            if((i+1)>extended && (i+1)<=($labels.size()-extended)) return;
            $(label).css('display', 'none');
        }, this);

        if(this.position()===Axis.POSITION.BOTTOM) {
            var labelSize = this._size['label'];

            //var n = Math.min(2, Math.ceil(labelSize.width*($labels.size()-1)/axisSize.width));
            var n = Math.ceil($labels.size() / (Math.floor(axisSize.width/labelSize.width) + 1));

            _.each($labels, function(label, i) {
                i = i - extended;
                if(i%n===0) return;
                $(label).css('display', 'none');
            }, this);
        }


        if(this.title() && this.title().length>0) {
            var x, y;
            if(this.position()===Axis.POSITION.LEFT) {
                x = (axisSize.width - PADDING) * -1;
                y = axisSize.height / 2;
                anchors.title.attr('transform', 'rotate(-90 ' + x + ', ' + y +')');
            } else if(this.position()===Axis.POSITION.RIGHT) {
                x = (axisSize.width - PADDING);
                y = axisSize.height / 2;
                anchors.title.attr('transform', 'rotate(90 ' + x + ', ' + y +')');
            } else {
                x = axisSize.width / 2;
                y = axisSize.height - PADDING;
            }
            anchors.title.attr({
                x: x,
                y: y
            });
        }

        if(this.unit() && this.unit().length>0) {
            var x, y;
            if(this.position()===Axis.POSITION.LEFT) {
                x = (this.tickPadding() + this.tickSize()) * -1;
                y = PADDING * -1;
            } else if(this.position()===Axis.POSITION.RIGHT) {
                x = this.tickPadding() + this.tickSize();
                y = PADDING * -1;
                anchors.unit.style('text-anchor', 'start');
            } else {
                x = axisSize.width - PADDING;
                y = axisSize.height - PADDING - PADDING;
            }
            anchors.unit.attr({
                x: x,
                y: y
            });
        }

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    function scale() {
        return this._scale;
    }

    function tickFormat(format) {
        if(!format) return this._tickFormat;


        this._axis.tickFormat(function(value, i) {
            return formatter.format(value, {pattern: format});
        });

        this._tickFormat = format;
        this._autoTickFormat = false;
    }

    function tickSize(size) {
        if(!size) return this._tickSize;

        this._tickSize = size;
        this._axis.tickSize(size);
    }

    function ticks(ticks) {
        if(!ticks) return this._ticks;

        
        if(ticks===Axis.TICKS.DAYS) {
            this._axis.ticks(d3.time.days);
        } else if(ticks===Axis.TICKS.MONTHS) {
            this._axis.ticks(d3.time.months);
        } else if(ticks===Axis.TICKS.YEARS) {
            this._axis.ticks(d3.time.years);
        } else if(ticks===Axis.TICKS.HOURS) {
            this._axis.ticks(d3.time.hours);
        } else if(ticks===Axis.TICKS.MINUTES) {
            this._axis.ticks(d3.time.minutes);
        } else {
            this._axis.ticks(ticks);
        }

        this._ticks = ticks;
        this._autoTicks = false;
    }

    function type() {
        return this.cfg().type;
    }

    function position(position) {
        if(!position) return this._position;
        
        this._position = position;
        this._axis.orient(position.toLowerCase());
    }

    function sizing() {
        var width = 0, height = 0;
        var domain = this.scale().domain();

        var range = this.scale().range();
        var rangeSize = Math.abs(range[1] - range[0]);
        if(this.position()===Axis.POSITION.LEFT || this.position()===Axis.POSITION.RIGHT) {
            height = rangeSize;
            width =  this.tickSize();
        } else {
            height = this.tickSize();
            width = rangeSize;
        }

        var value = _.max(domain, function(value) {
            return value.toString().length;
        });

        var $label = $('<text>' + formatter.format(value, {pattern: this.tickFormat()}) + '</text>');

        $label.css(this.cfg().style);

        var size = dom.sizing($label);

        this._size['label'] = size;

        if(this.position()===Axis.POSITION.LEFT || this.position()===Axis.POSITION.RIGHT) {
            width = width + size.width + PADDING;
        } else {
            height = height + size.height + PADDING;
        }

        var anchors = this._anchors;
        if(this.title() && this.title().length>0) {
            var size = dom.sizing(anchors.title);

            this._size['title'] = size;

            if(this.position()===Axis.POSITION.LEFT || this.position()===Axis.POSITION.RIGHT) {
                width = width + size.height + PADDING;
            } else {
                height = height + size.height + PADDING;
            }
        }

        this._size['axis'] = {
            height: height,
            width: width
        };

        return this._size['axis'];
    }

    function title(text) {
        var anchors = this._anchors;

        if(arguments.length===0) {
            return anchors.title.text();
        }

        anchors.title.text(i18n.translate(text));

        if(text && text.length>0) {
            anchors.title.style('display', '')
        } else {
            anchors.title.style('display', 'none');
        }
    }

    function unit(text) {
        var anchors = this._anchors;

        if(arguments.length===0) {
            return anchors.unit.text();
        }

        anchors.unit.text(i18n.translate(text));

        if(text && text.length>0) {
            anchors.unit.style('display', '')
        } else {
            anchors.unit.style('display', 'none');
        }
    }

    function autoTicks() {
        var domain = this.scale().domain();

        if(this.type()===Axis.TYPE.TIME) {
            domain.length<=0 && (domain = [new Date(0), new Date(0)]);
            var start = _.first(domain), end = _.last(domain);
            var range = (end - start) / 86400000;
            if(start.getDate()==1&&end.getDate()==1) {
                this.ticks(Axis.TICKS.MONTHS);
            } else if(range <= 1) {
                this.ticks(Axis.TICKS.HOURS);
            } else if(range <= 31) {
                this.ticks(Axis.TICKS.DAYS);
            } else if(range <= 366) {
                this.ticks(Axis.TICKS.MONTHS);
            } else {
                this.ticks(Axis.TICKS.MONTHS);
            }
        } else if(this.type()===Axis.TYPE.NUMERICAL) {
            this.ticks(10);
        } else {
            this.ticks(10);
        }

        this._autoTicks = true;
    }

    function autoTickFormat() {
        var domain = this.scale().domain();

        if(this.type()===Axis.TYPE.TIME) {
            if(this.ticks()===Axis.TICKS.YEARS) {
                this.tickFormat('yyyy');
            } else if(this.ticks()===Axis.TICKS.MONTHS) {
                this.tickFormat('yyyy/MM');
            } else if(this.ticks()===Axis.TICKS.DAYS) {
                this.tickFormat('MM/dd');
            } else if(this.ticks()===Axis.TICKS.HOURS) {
                this.tickFormat('hh');
            } else if(this.ticks()===Axis.TICKS.MINUTES) {
                this.tickFormat('hh:mm');
            } else {
                this.tickFormat('MM/dd');
            }
        } else if(this.type()===Axis.TYPE.NUMERICAL) {
            domain.length<=0 && (domain = [0]);
            if(this.ticks() > domain[1] - domain[0]) {
                this.tickFormat('#,##0.0');
            } else {
                this.tickFormat('#,##0');
            }
        }

        this._autoTickFormat = true;
    }

    function tickPadding(padding) {
        if(!padding) return this._tickPadding;

        this._axis.tickPadding(padding);
        this._tickPadding = padding;
    }

    function padding(padding) {
        if(!padding) return this._padding;

        var scale = this._scale;
        var ticks = this.ticks();
        if(this.type()===Axis.TYPE.TIME) {
            var domain = this.domain();
            var max = new Date(_.last(domain));
            var min = new Date(_.first(domain));
            if(ticks===Axis.TICKS.YEARS) {
                max = max.setYear(max.getYear()+padding/2);
                min = min.setYear(min.getYear()-padding/2);
            } else if(ticks===Axis.TICKS.MONTHS) {
                max = max.setMonth(max.getMonth()+padding/2);
                min = min.setMonth(min.getMonth()-padding/2);
            } else if(ticks===Axis.TICKS.DAYS) {
                max = max.setDate(max.getDate()+padding/2);
                min = min.setDate(min.getDate()-padding/2);
            } else if(ticks===Axis.TICKS.HOURS) {
                max = max.setHours(max.getHours()+padding/2);
                min = min.setHours(min.getHours()-padding/2);
            } else if(ticks===Axis.TICKS.MINUTES) {
                max = max.setMinutes(max.getMinutes()+padding/2);
                min = min.setMinutes(min.getMinutes()-padding/2);
            }
            this.domain([min, max]);
        } else if(this.type()===Axis.TYPE.NUMERICAL) {
            var domain = this.domain();
            var max = _.last(domain);
            var min = _.first(domain);
            var offset = (max - min) / ticks * (padding / 2);

            max = max + Math.round(offset);
            min = min - Math.round(offset);

            this.domain([min, max]);

        } else {
            scale.padding(padding);
        }

        this._padding = padding;
    }

    function domain(domain) {
        var scale = this._scale;
        if(!domain) return scale.domain();

        scale.domain(domain);

        this._autoTicks && this.autoTicks();
        this._autoTickFormat && this.autoTickFormat();
    }

    function range(range) {
        var scale = this._scale;
        if(!range) return scale.range();

        scale.range(range);
    }

    function rangeBand() {
        var range = this.range();
        var length = Math.abs(_.first(range) - _.last(range));
        var ticks;

        if(this.type()===Axis.TYPE.TIME) {
            ticks = this.ticks();
            var domain = this.domain();
            var max = domain[1], min = domain[0];
            var unit;
            if(ticks===Axis.TICKS.YEARS) {
                unit = 86400 * 365 * 1000;
            } else if(ticks===Axis.TICKS.MONTHS) {
                unit = 86400 * 30 * 1000;
            } else if(ticks===Axis.TICKS.DAYS) {
                unit = 86400 * 1000;
            } else if(ticks===Axis.TICKS.HOURS) {
                unit = 3600 * 1000;
            } else if(ticks===Axis.TICKS.MINUTES) {
                unit = 60 * 1000;
            }
            ticks = (max - min) / unit;
        } else if(this.type()===Axis.TYPE.NUMERICAL) {
            // ticks = this.ticks() + this.padding();
            ticks = this.ticks();
        } else {
            // ticks = this.domain().length + this.padding();
            ticks = this.domain().length;

        }
        return length / Math.max(ticks, 1);
    }
    
    function project(value) {
        return this.scale().project(value);
    }

    function format(value) {
        return formatter.format(value, {pattern: this._tickFormat})
    }


    var props = {
        _size: {},
        _axis: undefined,
        _scale: undefined,
        _position: undefined,
        _ticks: undefined,
        _tickFormat: undefined,
        _autoTicks: true,
        _autoTickFormat: true,
        _padding: 0,
        _title: undefined,
        _unit: undefined,
        tagName: 'g'
    };

    var Axis = declare(Base, {
        initialize: initialize,
        render: render,
        type: type,
        position: position,
        scale: scale,
        autoTicks: autoTicks,
        autoTickFormat: autoTickFormat,
        sizing: sizing,
        ticks: ticks,
        tickFormat: tickFormat,
        tickSize: tickSize,
        title: title,
        unit: unit,
        tickPadding: tickPadding,
        padding: padding,
        domain: domain,
        range: range,
        rangeBand: rangeBand,
        project: project,
        format: format
    }, props);
    
    Axis.TYPE = {
        NUMERICAL: "NUMERICAL",
        TIME: "TIME",
        CATEGORICAL: "CATEGORICAL"
    };

    Axis.TICKS = {
        DAYS: 'DAYS',
        WEEKS: 'WEEKS',
        MONTHS: 'MONTHS',
        YEARS: 'YEARS',
        HOURS: 'HOURS',
        MINUTES: 'MINUTES',
        WEEKS: 'WEEKS'
    };

    Axis.POSITION = {
        LEFT: "LEFT",
        TOP: "TOP",
        BOTTOM: "BOTTOM",
        RIGHT: "RIGHT"
    };

    return Axis;

});