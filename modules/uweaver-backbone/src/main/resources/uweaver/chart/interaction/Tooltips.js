/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang', 'uweaver/widget/Widget',
    'uweaver/data/Collection', 'uweaver/dom', 'uweaver/string'], function(_, $, d3, lang, Widget, Collection, dom, string) {

    var declare = lang.declare;
    var Base = Widget;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            style: {
                'fill': 'black',
                'stroke': 'black'
            }
        };
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        cfg.color && (this._color = cfg.color);

        this._component = d3.select(this.el).attr('name', 'infotips').style(cfg.style);
        this._background = this._component.append('path').style({
            fill: 'white',
            stroke: 'none',
            'opacity': 0.8
        });
        this._shape = this._component.append('path').style({
            fill: 'none',
            'stroke-width': 1,
            'opacity': 1
        });
        this._component.append('text').style({
            stroke: 'none',
            'text-anchor': 'middle',
            'opacity': 1
        });

        cfg.text && (this._component.select('text').text(cfg.text));

        this._size = {};

        this.hide();
    }

    function render(options) {
        options || (options = {});
        var defaults = {};
        _.defaults(options, defaults);

        var isVisible = this.isVisible();

        this.hide();

        var component = this._component;
        var PADDING = 10;
        var textElement = component.select('text');
        var textSize = dom.sizing(textElement);
        var size = {
            height: textSize.height + PADDING * 2,
            width: textSize.width + PADDING * 4
        };
        this._shape.attr('d', callout(size));
        textElement.attr({
            x: size.width / 2,
            y: size.height / 2 + textSize.height / 2 * 0.8
        });

        this._background.attr('d', callout(size));
        textElement.attr({
            x: size.width / 2,
            y: size.height / 2 + textSize.height / 2 * 0.8
        });

        this._size = size;

        if(this.render === render) {
            this._isRendered = true;
            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
        }

        isVisible && this.show();

        return this;
    }

    function text(value) {
        var component = this._component;

        if(value===undefined) return component.select('text').text();

        component.select('text').text(value);
        this.render();
    }

    function show(options) {
        options || (options = {});
        var defaults = {
            x: 0,
            y: 0
        };
        _.defaults(options, defaults);

        options.text && this.text(options.text);

        var component = this._component;
        var x = options.x + 10;
        var y = options.y - this.size().height / 2;
        component.attr('transform', 'translate(' + x + ', ' + y + ')')
            .style({display: ''});
    }

    function callout(size) {
        var triangleWidth = size.height * 0.4, triangleHeight = triangleWidth * 10 / 16;
        var de = (size.height - triangleWidth) / 2, ga = de;
        var path = "M${triangleHeight} 0 L${width} 0 L${width} ${height} L${triangleHeight} ${height}" +
            " L${triangleHeight} ${heightMinusDE} L0 ${heightDivideTwo} L${triangleHeight} ${ga} Z";
        return string.substitute(path, {
            triangleHeight: triangleHeight,
            height: size.height,
            width: size.width,
            heightMinusDE: size.height - de,
            heightDivideTwo: size.height / 2,
            ga: ga
        });
    }

    function hide() {
        var component = this._component;
        component.style({display: 'none'});
    }

    function toggle(options) {
        this.isVisible() ? this.hide() : this.show(options);
    }

    function height() {
        return this.size().height;
    }

    function width() {
        return this.size().width;
    }

    function size(value) {
        if(value===undefined) return this._size;
    }

    var props = {
        _component: undefined,
        _size: undefined,
        tagName: 'g'
    };

    var Tooltips = declare(Base, {
        initialize: initialize,
        render: render,
        show: show,
        hide: hide,
        toggle: toggle,
        size: size,
        height: height,
        width: width,
        text: text
    }, props);

    return Tooltips;
});