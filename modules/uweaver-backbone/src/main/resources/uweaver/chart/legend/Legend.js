/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang', 'uweaver/i18n', 'uweaver/widget/Widget'
    ], function(_, $, d3, lang, i18n, Widget) {

    var declare = lang.declare;
    var Base = Widget;
    /**
     * **Legend**
     * This class represents the legend of the chart.
     *
     * @constructor Legend
     * @extends Graphic
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [cfg] - A map of configuration to pass to the constructor.
     * @param {Collection} [cfg.series] - The data series to the chart.
     */
    function initialize(cfg) {
        Base.prototype.initialize.apply(this, arguments);

        var cfg = this.cfg();
        var defaults = {
        };
        _.defaults(cfg, defaults);

    }

    function render(options) {
        options || (options = {});
        // this.hide();

        var component = d3.select(this.el);
        component.selectAll('rect').remove();
        component.selectAll('g').remove();
        var border = component.append('rect');
        var canvas = component.append('g');

        var availableWidth = this._maxWidth;
        var dx = 0, dy = 0;
        _.each(this._data, function(datum) {
            var item = canvas.append('g');
            var marker = item.append('rect');
            var label = item.append('text');
            marker.attr('width', 16).attr('height', 4)
                .style({
                    fill: datum.color,
                    stroke: datum.color
                }).attr('transform', 'translate(0,-2)');
            label.text(i18n.translate(datum.title))
                .style({
                    'fill': datum.color,
                    'font-size': '12px'
                });
            label.attr('transform', 'translate(20,' + (label.node().getBoundingClientRect().height * 0.3) + ')');
            var box = item.node().getBoundingClientRect();
            if(availableWidth<box.width + 5) {
                availableWidth = this._maxWidth;
                dy = dy + box.height + 5;
                dx = 0;
            }
            item.attr('transform', 'translate(' + dx + ',' + (dy + box.height / 2) + ')');
            dx = dx + box.width + 10;
            availableWidth = availableWidth - box.width;
        });

        canvas.attr('transform', 'translate(8,4)');

        border.attr('width', this.width() + 16).attr('height', this.height() + 8)
            .style({
                fill: 'transparent',
                stroke: 'green',
                'stroke-opacity': 0.6
            });

        this.show();

        return this;
    }

    function data(data) {
        this._data = data;
        this.render();
    }

    function maxWidth(maxWidth) {
        this._maxWidth = maxWidth;
    }

    function width() {
        return this.el.getBoundingClientRect().width;
    }

    function height() {
        return this.el.getBoundingClientRect().height;
    }


    var props = {
        _data: undefined,
        _maxWidth: undefined
    };

    var Legend = declare(Base, {
        initialize: initialize,
        render: render,
        data: data,
        width: width,
        height: height,
        maxWidth: maxWidth
    }, props);

    return Legend;


});