/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang', 'uweaver/widget/Widget',
    'uweaver/data/Collection', 'uweaver/dom', 'uweaver/string'], function(_, $, d3, lang, Widget, Collection, dom, string) {

    var declare = lang.declare;
    var Base = Widget;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            bar: {
                size: 8
            },
            color: 'orange',
            tooltips: {}
        };
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._component = d3.select(this.el);

        this.bar(cfg.bar);
        this.title(cfg.title);
        cfg.xField && (this._xField = cfg.xField);
        cfg.yField && (this._yField = cfg.yField);
        cfg.xAxis && (this._xAxis = cfg.xAxis) ;
        cfg.yAxis && (this._yAxis = cfg.yAxis) ;
        (cfg.data) ? this.data(cfg.data, {render: false}) : this.data(new Collection(), {render: false});
        this._color = cfg.color;
        this.css({
            fill: this._color,
            stroke: this._color
        });
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        var dataset = this.dataset();
        var xField = this._xField, yField = this._yField;
        var xAxis = this._xAxis, yAxis = this._yAxis;

        this._component.selectAll('g').remove();


        var range = yAxis.scale().range();
        var length = Math.abs(_.first(range) - _.last(range));
        var width = this.bar().size;

        var fields = _.isArray(yField) ? yField : [yField];
        var yOffset = {};

        _.each(fields, function(field, i) {
            var fill = _.isArray(this.color()) ? this.color()[i] : this.color;
            var index = _.isArray(yField) ? i : undefined;
            this._component.append('g').selectAll('rect')
                .data(dataset).enter().append('rect')
                .attr({fill: fill, stroke: fill})
                .attr('x', function(item) {
                    var value = item[xField] ? item[xField]() : item.get(xField);
                    value || (value = 0);
                    var x = xAxis.scale().project(value);
                    return x - width / 2;
                }).attr('y', function(item) {
                    var xValue = item[xField] ? item[xField]() : item.get(xField);
                    xValue || (xValue = 0);
                    yOffset[xValue] || (yOffset[xValue] = 0);
                    var value = item[field] ? item[field]() : item.get(field);
                    value || (value = 0);
                    var y = yAxis.scale().project(value) - yOffset[xValue];
                    yOffset[xValue] = yOffset[xValue] + length - yAxis.scale().project(value);
                    return y;
                }).attr('height', function(item) {
                    var value = item[field] ? item[field]() : item.get(field);
                    value || (value = 0);
                    var height = length - yAxis.scale().project(value);
                    return height;
                }).attr('width', width).attr('cid', function(item) {return item.cid}).attr('index', index);


            if(this.cfg().tooltips.display!=='none') {
                this.$('g > rect').on('mouseenter', _.bind(onMarkerHoverIn, this));
                this.$('g > rect').on('mouseleave', _.bind(onMarkerHoverOut, this));
            }
        }, this);

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new collection is bound. event.data => the collection.
     *
     * @memberof CartesianChart#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @param {Boolean} [options.render=true] - A false value will prevent the chart from being rendered.
     * @returns {CartesianChart} this
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {
            render: true
        };

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = items;

        this.listenTo(this._data, "update", _.bind(onItemsUpdate, this));

        options.render && this.render();

        var event = {
            context: this,
            source: this,
            data: items
        };

        (options.silent) || this.trigger('transition', event);

        return this;
    }

    function dataset() {
        var dataset = this._data.toArray();

        return dataset;
    }

    function onItemsUpdate() {
        this.refresh();
    }

    function refresh() {

    }

    function title(text) {
        if(!text) return this._title;

        this._title = text;
    }

    function bar(bar) {
        if(!bar) return this._bar;

        this._bar = bar;
    }

    function xField() {
        return this._xField;
    }

    function yField() {
        return this._yField;
    }

    function xAxis() {
        return this._xAxis;
    }

    function yAxis() {
        return this._yAxis;
    }

    function onMarkerHoverIn(event) {
        var marker = event.currentTarget;
        var $marker = $(marker);
        var cid = $marker.attr('cid');
        var rect = marker.getBBox();
        var x = rect.x + rect.width / 2;
        var y = rect.y + rect.height / 2;
        var cfg = this.cfg();
        var index = $marker.attr('index');
        var item = this.data().get(cid);

        var text = (cfg.tooltips.renderer) ? cfg.tooltips.renderer(item, index) : tooltipsRenderer.call(this, item, index);

        this.trigger("hinton", {
            data: text,
            text: text,
            x: x,
            y: y,
            fill: $marker.css('fill'),
            stroke: $marker.css('fill'),
            context: this,
            source: this
        });
    }

    function tooltipsRenderer(item, index) {
        var xField = this._xField, yField = _.isArray(this._yField) ? this._yField[index] : this._yField;
        var xAxis = this._xAxis, yAxis = this._yAxis;
        var title = _.isArray(this._title) ? this._title[index] : this._title;
        var tpl = "${title} ${preposition} ${xValue}: ${yValue}";
        var preposition = "of";

        if(xAxis.type()==='TIME') {
            if(xAxis.ticks()==='HOURS'||xAxis.ticks()==='MINUTES') {
                preposition = "at";
            } else if(xAxis.ticks()==='DAYS') {
                preposition = "on";
            } else {
                preposition = "in";
            }
        }

        return string.substitute(tpl, {
            title: title,
            xValue: xAxis.format(item.get(xField)),
            yValue: yAxis.format(item.get(yField)),
            preposition: preposition
        });
    }

    function onMarkerHoverOut(event) {
        this.trigger("hintoff", {
            context: this,
            source: this
        });
    }

    function color() {
        return this._color;
    }

    var props = {
        _data: undefined,
        _title: undefined,
        _bar: undefined,
        _color: undefined,
        tagName: 'g'
    };

    var ScatterSeries = declare(Base, {
        initialize: initialize,
        render: render,
        bar: bar,
        title: title,
        data: data,
        dataset: dataset,
        xField: xField,
        yField: yField,
        xAxis: xAxis,
        yAxis: yAxis,
        color: color
    }, props);


    return ScatterSeries;

});