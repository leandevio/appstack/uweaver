/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'ss', 'uweaver/lang', 'uweaver/widget/Widget',
    'uweaver/data/Collection', 'uweaver/scale/LinearScale',
    'uweaver/formatter', 'uweaver/dom', 'uweaver/string'], function(_, $, d3, ss, lang, Widget, Collection, LinearScale, formatter, dom, string) {

    var declare = lang.declare;
    var Base = Widget;
    var RADIUS = 300, LABELFONTSIZE = 12;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            donut: 0,
            label: {
                display: 'auto'
            },
            style: {
                'fill': 'blue'
            },
            tooltips: {},
            threshold: 0.05
        };
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._component = d3.select(this.el);

        this.css({fill: 'none', stroke: 'none'});
        this.title(cfg.title);
        cfg.angleField && (this._angleField = cfg.angleField);
        cfg.radiusField && (this._radiusField = cfg.radiusField);
        cfg.labelField && (this._labelField = cfg.labelField);
        cfg.angularAxis && (this._angularAxis = cfg.angularAxis) ;
        cfg.radialAxis && (this._radialAxis = cfg.radialAxis);
        this.donut(cfg.donut);
        (cfg.data) ? this.data(cfg.data, {render: false}) : this.data(new Collection(), {render: false});
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        var cfg = this.cfg();
        var data = this._data;
        var angleField = this._angleField, radiusField = this.__radiusField, labelField = this._labelField;
        var angularAxis = this._angularAxis, radialAxis = this._radialAxis;
        var maxRadius = _.max(radialAxis.range());
        var margin = (maxRadius>=80 && cfg.label.display!=='none') ? Math.min(maxRadius / 2 * 0.8, 80) : 0;
        maxRadius = maxRadius - margin;
        var theme = (data.size()<=10) ? d3.scale.category10() : d3.scale.category20();

        this._component.selectAll('g').remove();

        var dataset = prepareDataset(data, angleField, radiusField, labelField, angularAxis, radialAxis, maxRadius, theme);

        layout.call(this);

        this._legend = [];

        _.each(dataset, function(datum, i) {
            var profile = {};

            profile.label = datum.label;
            profile.color = datum.color;
            profile.percentage = datum.percentage;

            this._legend.push(profile);
        }, this);

        var arcs = renderArc(this._component, dataset, maxRadius, this.donut());

        if(this.cfg().tooltips.display!=='none') {
            $(arcs.node()).find('path').on('mouseenter', _.bind(onMarkerHoverIn, this));
            $(arcs.node()).find('path').on('mouseleave', _.bind(onMarkerHoverOut, this));
        }

        var threshold = this.cfg().threshold;
        renderLabel(this._component, dataset, maxRadius, margin, threshold);
        var value = data.reduce(function(sum, item) {
            var value = item.has(angleField) ? item.get(angleField) : 0;
            return sum + value;
        }, 0);
        renderDonut(this._component, value, maxRadius, this.donut());


        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    function prepareDataset(data, angleField, radiusField, labelField, angularAxis, radialAxis, maxRadius, theme) {
        var sum = data.reduce(function(sum, item) {
            var value = item.has(angleField) ? item.get(angleField) : 0;
            return sum + value;
        }, 0);
        var scale = new LinearScale();
        scale.range(angularAxis.range());
        scale.domain([0, sum]);
        var startAngle = 0;
        var dataset = data.map(function(item, i) {
            var angleValue = item.has(angleField) ? item.get(angleField) : 0;
            var endAngle = startAngle + scale.project(angleValue);
            var percentage = (endAngle - startAngle) / (Math.PI * 2);
            var radius, label;
            if(radiusField) {
                var radiusValue = item.has(radiusField) ? item.get(radiusField) : 0;
                radius = radialAxis.project(radiusValue);
            } else {
                radius = maxRadius;
            }
            if(labelField && item.has(labelField)) {
                label = item.get(labelField);
            }
            var color = theme(i);
            var datum = {
                startAngle: startAngle,
                endAngle: endAngle,
                color: color,
                radius: radius,
                label: label,
                percentage: percentage,
                cid: item.cid
            };
            startAngle = endAngle;

            return datum;
        }, this);

        return  dataset;
    }

    function layout() {
        var layout = {
            title: {},
            pie: {},
            labels: {}
        };
        var dataset = this._dataset, radialAxis = this._radialAxis;
        var maxRadius = _.max(radialAxis.range()), title = this.title();

        //layout.title.fontSize = TITLEFONTSIZE * maxRadius / RADIUS;


        this._layout = layout;
    }

    function renderArc(el, dataset, maxRadius, donut) {
        var arc = d3.svg.arc()
            .outerRadius(function(datum) {return datum.radius})
            .innerRadius(maxRadius * donut)
            //.padAngle(Math.PI/20)
            .startAngle(function (datum) {return datum.startAngle;})
            .endAngle(function (datum) {return datum.endAngle;});

        var node = el.append('g');
        node.selectAll('path').data(dataset).enter()
            .append('path').attr('d', function(datum) {return arc(datum);})
            .style('fill', function(datum) {return datum.color}).attr('cid', function(datum) {return datum.cid});

        return node;
    }

    function renderDonut(el, value, maxRadius, donut) {

        if(donut<=0) return;

        var color = d3.scale.category10();
        var pattern = (Math.round(value)===value) ? '#,##0' : '#,##0.00';
        var text = formatter.format(value, {pattern: pattern});
        var radius = maxRadius * donut;
        var fontSize = radius * 0.5;

        var node = el.append('text').text(text).style({
            'font-size': fontSize,
            stroke: 'none',
            fill: color,
            'text-anchor': 'middle'
        }).attr('transform', 'translate(0, ' + (fontSize / 2) + ')');

        var size = dom.sizing(node);
        if(size.width > radius * 2 * 0.8) {
            node.style('font-size', fontSize * 0.8);
        }

        return node;
    }

    function renderLabel(el, dataset, maxRadius, margin, threshold) {
        //var line = d3.svg.line.radial()
        //    .radius(function(datum) {return datum.radius;})
        //    .angle(function(datum) { return datum.angle;});
        if(margin===0) return;
        var line = d3.svg.line()
            .x(function(datum) {return datum.x})
            .y(function(datum) {return datum.y*-1});

        var node = el.append('g');
        var radius = maxRadius + 1;
        var color = d3.scale.category10();
        var radiusOffset = 0, lastAngle = 0;
        var data = _.filter(dataset, function(datum) {
            return (datum.percentage >= threshold);
        }, this);
        const quadrants = data.reduce((quadrants, datum) => {
            if(datum.endAngle<=Math.PI/2) {
                quadrants[0].push(datum);
            } else if(datum.endAngle<=Math.PI) {
                quadrants[1].push(datum);
            } else if(datum.endAngle<=Math.PI/2*3) {
                quadrants[2].push(datum);
            } else {
                quadrants[3].push(datum);
            }
            return quadrants;
        }, [[],[],[],[]]);
        for(const [index, quadrant] of quadrants.entries()) {
            var dy = radius / quadrant.length;
            for(const [index, datum] of quadrant.entries()) {
                var angle = Math.acos((radius - dy) / radius);
                datum.angle = angle;
            }
        }
        node.selectAll('path').data(data).enter().append('path')
            .attr('d', function(datum, index) {
                var angle = (datum.startAngle + datum.endAngle)/2;
                // var angle = datum.angle;
                // console.log(`${datum.label}: ${angle0} => ${angle}`);
                var x0 = Math.sin(angle) * radius, y0 = Math.cos(angle) * radius;
                var x1 = Math.sin(angle) * (radius + 15 + radiusOffset), y1 = Math.cos(angle) * (radius + 15 + radiusOffset);
                var x2 = (x1>=0) ? x1 + 5 : x1 - 5, y2 = y1;
                var dataset = [{x: x0, y: y0}, {x: x1, y: y1}];
                if(Math.abs(Math.cos(angle))>0.3) {
                    dataset.push({x: x2, y: y2});
                }
                var path = line(dataset);

                return path;
            }).style({fill: 'none', stroke: 'gray', 'stroke-width': 1});

        radiusOffset = 0; lastAngle = 0;
        node.selectAll('text').data(data).enter().append('text').text(function(datum) {
                var text = "";
                if(margin>=80/2*0.8) text = text + datum.label;
                if(margin>=80*0.8) text = text + ": " + formatter.format(datum.percentage, {pattern: '#0.##%'});
                return text;
            }).attr('transform', function(datum, index){
                var angle = (datum.startAngle + datum.endAngle)/2;
                // var angle = datum.angle;
                var x0 = Math.sin(angle) * radius, y0 = Math.cos(angle) * radius;
                var x1 = Math.sin(angle) * (radius + 15 + radiusOffset), y1 = Math.cos(angle) * (radius + 15 + radiusOffset);
                var x2 = (x1>=0) ? x1 + 5 : x1 - 5, y2 = y1;
                var x = (Math.abs(Math.cos(angle))>0.3) ? x2 : x1, y = y1 - 3;

                x = (x>=0) ? x + 2 : x - 2; y = y * -1;

                return 'translate(' + x + ',' + y + ')';
            }).style('text-anchor', function(datum) {
                var angle = (datum.startAngle + datum.endAngle)/2;
                // var angle = datum.angle;
                var textAnchor = 'start';
                if(angle>Math.PI) textAnchor = 'end';
                return textAnchor
            }).style({fill: color, 'font-size': '12px'});


        return node;
    }

    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new collection is bound. event.data => the collection.
     *
     * @memberof PieSeries#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @param {Boolean} [options.render=true] - A false value will prevent the chart from being rendered.
     * @returns {PieSeries} this
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {
            render: true
        };

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = items;

        this._dataset = items.toArray();

        this.listenTo(this._data, "update", _.bind(onItemsUpdate, this));

        options.render && this.render();

        var event = {
            context: this,
            source: this,
            data: items
        };

        (options.silent) || this.trigger('transition', event);

        return this;
    }

    function onItemsUpdate() {
        this.refresh();
    }

    function refresh() {

    }

    function title(text) {
        if(!text) return this._title;

        this._title = text;
    }



    function angleField() {
        return this._angleField;
    }

    function radiusField() {
        return this._radiusField;
    }

    function angularAxis() {
        return this._angularAxis;
    }

    function radialAxis() {
        return this._radialAxis;
    }

    function color(index) {
        return this._color(index);
    }

    function donut(donut) {
        if(donut===undefined) return this._donut;

        this._donut = donut;

        return this;
    }

    function legend() {
        return this._legend;
    }

    function onMarkerHoverIn(event) {
        var marker = event.currentTarget;
        var $marker = $(marker);
        var cid = $marker.attr('cid');
        var rect = marker.getBBox();
        var x = rect.x + rect.width / 2;
        var y = rect.y + rect.height / 2;
        var cfg = this.cfg();
        var item = this.data().get(cid);
        var text = (cfg.tooltips.renderer) ? cfg.tooltips.renderer(item) : tooltipsRenderer.call(this, item);

        this.trigger("hinton", {
            data: text,
            text: text,
            x: x,
            y: y,
            fill: $marker.css('fill'),
            stroke: $marker.css('fill'),
            context: this,
            source: this
        });
    }

    function tooltipsRenderer(item) {
        var angleField = this._angleField, labelField = this._labelField;
        var angularAxis = this._angularAxis;

        return "" + item.get(labelField) + ": " + angularAxis.format(item.get(angleField));
    }

    function onMarkerHoverOut(event) {
        this.trigger("hintoff", {
            context: this,
            source: this
        });
    }

    function color() {
        return this.el.style.fill;
    }

    var props = {
        _data: undefined,
        _title: undefined,
        _angularAxis: undefined,
        _radialAxis: undefined,
        _angleField: undefined,
        _radiusField: undefined,
        _donut: undefined,
        _margin: 0,
        _legend: undefined,
        _layout: undefined,
        tagName: 'g'
    };

    var PieSeries = declare(Base, {
        initialize: initialize,
        render: render,
        title: title,
        data: data,
        angleField: angleField,
        radiusField: radiusField,
        angularAxis: angularAxis,
        radialAxis: radialAxis,
        color: color,
        donut: donut,
        legend: legend,
        color: color
    }, props);


    return PieSeries;

});