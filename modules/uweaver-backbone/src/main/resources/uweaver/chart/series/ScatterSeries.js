/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'ss', 'uweaver/lang', 'uweaver/widget/Widget',
    'uweaver/data/Collection', 'uweaver/dom', 'uweaver/string'], function(_, $, d3, ss, lang, Widget, Collection, dom, string) {

    var declare = lang.declare;
    var Base = Widget;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            marker: {
                type: 'rect',
                width: 8,
                height: 8
            },
            color: 'orange',
            tooltips: {}
        };
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._component = d3.select(this.el);
        this._anchors = {};
        
        this.marker(cfg.marker);
        this.title(cfg.title);
        cfg.xField && (this._xField = cfg.xField);
        cfg.yField && (this._yField = cfg.yField);
        cfg.xAxis && (this._xAxis = cfg.xAxis) ;
        cfg.yAxis && (this._yAxis = cfg.yAxis) ;
        (cfg.data) ? this.data(cfg.data, {render: false}) : this.data(new Collection(), {render: false});
        this._color = cfg.color;        this.css({
            fill: this._color,
            stroke: this._color
        });
    }

    function render(options) {
        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        var dataset = this.dataset();
        var marker = this.marker();
        var xField = this._xField, yField = this._yField;
        var xAxis = this._xAxis, yAxis = this._yAxis;
        var anchors = this._anchors;

        this._component.selectAll('g').remove();


        this._component.append('g').selectAll(marker.type)
            .data(dataset).enter().append(marker.type).attr(marker)
            .attr('x', function(item){
                var value = item.get(xField);
                value || (value = 0);
                var x = xAxis.scale().project(value);
                return x - marker.width / 2;
            }).attr('y', function(item) {
                var value = item.get(yField);
                value || (value = 0);
                var y = yAxis.scale().project(value);
                return y - marker.height / 2;
            }).attr('cid', function(item) {return item.cid});


        if(this.cfg().tooltips.display!=='none') {
            this.$('g > rect').on('mouseenter', _.bind(onMarkerHoverIn, this));
            this.$('g > rect').on('mouseleave', _.bind(onMarkerHoverOut, this));
        }


        anchors.tooltips = this._component.append('g').style({
            'display': 'none'
        });

        anchors.tooltips.append('path').style({
            fill: 'white',
            'stroke-width': 2,
            'opacity': 0.8
            });
        anchors.tooltips.append('text').style({
                stroke: 'none',
                'text-anchor': 'middle'
            });

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    function onMarkerHoverIn(event) {
        var marker = event.currentTarget;
        var $marker = $(marker);
        var cid = $marker.attr('cid');
        var rect = marker.getBBox();
        var x = rect.x + rect.width / 2;
        var y = rect.y + rect.height / 2;
        var cfg = this.cfg();
        var item = this.data().get(cid);
        var text = (cfg.tooltips.renderer) ? cfg.tooltips.renderer(item) : tooltipsRenderer.call(this, item);

        this.trigger("hinton", {
            data: text,
            text: text,
            x: x,
            y: y,
            fill: $marker.css('fill'),
            stroke: $marker.css('fill'),
            context: this,
            source: this
        });
    }

    function tooltipsRenderer(item) {
        var xField = this._xField, yField = this._yField;
        var xAxis = this._xAxis, yAxis = this._yAxis;
        var title = this._title;
        var tpl = "${title} ${preposition} ${xValue}: ${yValue}";
        var preposition = "of";

        if(xAxis.type()==='TIME') {
            if(xAxis.ticks()==='HOURS'||xAxis.ticks()==='MINUTES') {
                preposition = "at";
            } else if(xAxis.ticks()==='DAYS') {
                preposition = "on";
            } else {
                preposition = "in";
            }
        }

        return string.substitute(tpl, {
            title: title,
            xValue: xAxis.format(item.get(xField)),
            yValue: yAxis.format(item.get(yField)),
            preposition: preposition
        });

    }

    function onMarkerHoverOut(event) {
        this.trigger("hintoff", {
            context: this,
            source: this
        });
    }

    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new collection is bound. event.data => the collection.
     *
     * @memberof ScatterSeries#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @param {Boolean} [options.render=true] - A false value will prevent the chart from being rendered.
     * @returns {ScatterSeries} this
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {
            render: true
        };

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = items;

        this.listenTo(this._data, "update", _.bind(onItemsUpdate, this));

        options.render && this.render();

        var event = {
            context: this,
            source: this,
            data: items
        };

        (options.silent) || this.trigger('transition', event);

        return this;
    }

    function dataset() {
        var xField = this._xField, yField = this._yField;

        var dataset = this._data.filter(function(datum) {
            return (datum.has(xField) && datum.has(yField));
        });

        if(this._xAxis.type()!=='CATEGORICAL') {
            dataset = _.sortBy(dataset, function(item) {
                return item.get(xField);
            });
        }

        return dataset;
    }


    function onItemsUpdate() {
        this.refresh();
    }

    function refresh() {

    }

    function title(text) {
        if(!text) return this._title;
        
        this._title = text;
    }
    
    function marker(marker) {
        if(!marker) return this._marker;
        
        this._marker = marker;
    }

    function xField() {
        return this._xField;
    }

    function yField() {
        return this._yField;
    }

    function xAxis() {
        return this._xAxis;
    }

    function yAxis() {
        return this._yAxis;
    }


    function trendline() {
        var xField = this.xField(), yField = this.yField();
        var xAxis = this.xAxis(), yAxis = this.yAxis();
        var dataset = this.dataset();
        var data = _.map(dataset, function(item) {
            var xValue = item[xField] ? item[xField]() : item.get(xField);
            xValue || (xValue = 0);
            var yValue = item[yField] ? item[yField]() : item.get(yField);
            yValue || (yValue = 0);

            return [xAxis.project(xValue), yAxis.project(yValue)];
        });
        var lr = ss.linearRegressionLine(ss.linearRegression(data));

        var trend = d3.svg.line()
            .x(function(item) {
                var value = item[xField] ? item[xField]() : item.get(xField);
                value || (value = 0);
                return xAxis.project(value);
            })
            .y(function(item) {

                var value = item[xField] ? item[xField]() : item.get(xField);
                value || (value = 0);
                return lr(xAxis.project(value));
            });

        var stroke = d3.lab(this.css('stroke')).brighter(1);
        this._component.append('path').attr('d', trend(dataset)).attr({
            fill: 'none',
            'stroke-width': '2px',
            'stroke-dasharray': '3, 3',
            stroke: stroke
        });
    }


    
    function legend() {
        
    }

    function color() {
        return this._color;
    }

    var props = {
        _data: undefined,
        _title: undefined,
        _marker: undefined,
        _lengend: undefined,
        _color: undefined,
        tagName: 'g'
    };

    var ScatterSeries = declare(Base, {
        initialize: initialize,
        render: render,
        marker: marker,
        title: title,
        data: data,
        dataset: dataset,
        xField: xField,
        yField: yField,
        xAxis: xAxis,
        yAxis: yAxis,
        trendline: trendline,
        color: color
    }, props);
    

    return ScatterSeries;

});