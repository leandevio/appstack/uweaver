/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This object represents the POSITION constant.
 *
 * @module lang
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore'], function(_) {
    var CONSTANT = {
        LEFT: 'left',
        RIGHT: 'right',
        TOP: 'top',
        BOTTOM: 'bottom'
    };
    var SEPARATOR = '|';
    var INDEX;

    function initialize() {
        INDEX = _.reduce(CONSTANT, function(memo, value) {
            return memo + value + SEPARATOR;
        }, SEPARATOR);
    }

    function validate(value) {
        if(!_.isString(value)) return false;

        var pattern = SEPARATOR + value.toLowerCase() + SEPARATOR;

        return (INDEX.indexOf(pattern)!==-1);
    }

    initialize();

    return {
        validate: validate,
        LEFT: 'left',
        RIGHT: 'right',
        TOP: 'top',
        BOTTOM: 'bottom'
    };
});