/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['backbone', 'underscore', 'uweaver/lang', 'uweaver/Deferred',
    'uweaver/data/proxy/Proxy', 'uweaver/data/proxy/REST', 'uweaver/data/reader/Reader', 'uweaver/data/reader/Json',
    'uweaver/data/Model', 'uweaver/number', 'uweaver/browser'], function(Backbone, _, lang, Deferred, Proxy, REST, Reader, Json, Model, number, browser) {
    var declare = lang.declare;
    var Base = Backbone.Collection;

    /**
     * Collections are ordered sets of models.
     *
     * This class extends Backbone.Collection with the following changes:
     * * model - change the default from Backbone.Model to uweaver/data/Model.
     * * url() - rewrite the url() to use the resource property to generate the URL of the model's resource on the server.
     * * fetch() & sync() - rewrite to support pagination. see [fetch()]{@link Collection#fetch} for details.
     *
     * ##### Usage
     *     // Declare a new type of collection, Products, with the following designs:
     *     // * the resource - 'products'
     *     var declare = uweaver.lang.declare;
     *     var Products = declare(Collection, {
     *         resource: 'products'
     *     });
     *     var filters = [{"property":"price", "value":0.5, "operator":"GE"}, {"property":"price", "value":10, "operator":"LE"}];
     *     var sorters = [{"property":"price", "direction":"desc"}];
     *
     *     // fetch products from the server.
     *     products.on('update', function(collection) {
     *        ....
     *     });
     *     products.fetch({
     *         limit: 20,
     *         offset: 40,
     *         filters: filters,
     *         sorters: sorters,
     *         async: false
     *     });
     *
     *     // Create a product & save it to the server.
     *     // Assign the product a business id - "sunflower".
     *     // The server will assign a object id for the product after saved.
     *     var products = new Products();
     *
     *     products.on('sync', function(model) {
     *        ....
     *     });
     *     products.create({
     *         id: "sunflower",
     *         name: "Sunflower",
     *         price: 0.5
     *     });
     *
     *     // Create a ad-hoc collection & add an array of items.
     *     var collection = new Collection();
     *     var items = [{_id: "rose", name: "Rose"},{_id: "lily", name: "Lily"},{_id: "tomato", name: "Tomato"}];
     *
     *     collection.add(items);
     *     expect(collection.size()).to.equal(items.length);

     *
     * See [Backbone.Collection's document](http://backbonejs.org/#Collection) for details.
     *
     * ##### Events:
     * See [Backbone.Collection's events](http://backbonejs.org/#Events) for details.
     *
     * @constructor Collection
     * @extends Backbone.Collection
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [models] - the initial array of models.
     * @param {Object} [options] - A map of additional options to pass to the constructor.
     * See [Backbone.Collection's constructor](http://backbonejs.org/#Collection-constructor) for details.
     */
    function initialize(models, options) {
        Base.prototype.initialize.apply(this, arguments);

        options || (options = {});

        var defaults = {};

        _.defaults(options, defaults);

        this._limit = 0;
        this._offset = 0;
        this._filters = [];
        this._sorters = [];
        this._conjunction = "AND";
        this._locale = browser.locale();

        options.model && (this.model = options.model);
        options.proxy && (this._proxy = options.proxy);
    }

    /**
     * Get or set the total property(the total number of records) returned from the server.
     *
     * @memberof Collection#
     * @returns {Integer}
     */
    function total(value) {
        if(arguments.length===0) return this._total;

        this._total = value;
    }

    /**
     * Get the limit property(page size) returned from the server.
     *
     * @memberof Collection#
     * @returns {Integer}
     */
    function limit() {
        return this._limit;
    }

    /**
     * Get the offset property returned from the server.
     *
     * @memberof Collection#
     * @returns {Integer}
     */
    function offset() {
        return this._offset;
    }

    /**
     * Get the filters(criteria) property returned from the server.
     *
     * @memberof Collection#
     * @returns {Object[]}
     */
    function filters() {
        return this._filters;
    }

    /**
     * Get the sorters(ordering) property returned from the server.
     *
     * @memberof Collection#
     * @returns {Object[]}
     */
    function sorters() {
        return this._sorters;
    }

    function conjunction() {
        return this._conjunction;
    }

    function locale() {
        return this._locale;
    }

    /**
     * Get the number of models in the collection.
     *
     * @memberof Collection#
     * @returns {Integer}
     */
    function size() {
        return this.length;
    }

    function proxy(theProxy) {
        if(!theProxy) return this._proxy;

        this._proxy = theProxy;
    }

    function reader(theReader) {
        if(!theReader) return this._reader;

        this._reader = theReader;
    }

    /**
     * Retrieve the data from the server.
     *
     * @memberof Collection#
     * @param {String} method - The method name.
     * @param {Collection} data - The collection to be read.
     * @param {Object} options - success and error callbacks, and all other jQuery request options.
     * See [Backbone.sync()](http://backbonejs.org/#Sync) for details.
     * @returns {Promise}
     */
    function sync(method, data, options) {
        options || (options = {});
        var resource = this.resource;
        var action = method;

        // use the model's resource property when the collection's is not available
        //resource || (resource = (this.constructor.prototype.model && this.constructor.prototype.model.prototype.resource));
        resource || (resource = (this.model && this.model.prototype.resource));

        options.xhr = this.proxy().sync(resource, action, data, options);
        return options.xhr;
    }

    function request(subresource, options) {
        var resource = this.resource;

        // use the model's resource property when the collection's is not available
        //resource || (resource = (this.constructor.prototype.model && this.constructor.prototype.model.prototype.resource));
        resource || (resource = (this.model && this.model.prototype.resource));

        var deferred = new Deferred();

        this.proxy().request(resource, subresource, this, options).then(function(response) {
            var data = this.parse(response);
            deferred.resolve(data);
        }, function(xhr, status, error) {
            deferred.reject(xhr, status, error);
        }, this);

        return deferred.promise();
    }

    function download(subresource, options) {
        var resource = this.resource;

        // use the model's resource property when the collection's is not available
        //resource || (resource = (this.constructor.prototype.model && this.constructor.prototype.model.prototype.resource));
        resource || (resource = (this.model && this.model.prototype.resource));

        return this.proxy().download(resource, subresource, this, options);
    }

    /**
     * called whenever the collection's items are returned by the server, in fetch.
     * The function is passed the raw response object, and should return the array of model attributes to be added to the collection.
     *
     * @memberof Collection#
     * @param {Object} response - The raw response object.
     * @param {Object} options - optional arguments.
     * @returns {Array} the array of item attributes to be added to the collection.
     */
    function parse(response, options) {
        return this.reader().parse(this, response, options);
    }

    /**
     * Fetch the default set of models for this collection from the server, setting them on the collection when they arrive.
     *
     * @memberof Collection#
     * @param {Object} options - optional arguments.
     * ##### Options
     * + limit -  limit the number of results returned .
     * + offset - skip the first N rows before beginning to return results.
     * + filters - An array of objects to specify the criteria of the results. Examples: [{"property":"price", "value":0.5, "operator":"GE"}, {"property":"price", "value":10, "operator":"LE"}].
     * + sorters - An array of objects to specify the ordering of the results. Examples: [{"property":"price", "direction":"DESC"}]
     * + conjunction - A string specify the logical conjunction of the filters. Examples: "AND"
     * + locale - A string specify the locale. Examples: "zh-TW"
     * See [Backbone.Collection.fetch()](http://backbonejs.org/#Collection-fetch) for more options.
     * @returns {Collection} this
     */
    function fetch(options) {
        var resource = this.resource;
        resource || (resource = (this.model && this.model.prototype.resource));

        options || (options = {});

        var defaults = {
            reset: true,
            conjunction: "AND"
        };
        _.defaults(options, defaults);

        if(options.filters) {
            this._filters = options.filters;
            this._offset = 0;
        }
        options.sorters && (this._sorters = options.sorters);
        (options.limit!==undefined) && (this._limit = options.limit);
        (options.offset!==undefined) && (this._offset = options.offset);
        this._conjunction = options.conjunction;
        (options.locale!==undefined) && (this._locale = options.locale);

        this._offset = Math.max(0, this._offset);

        var deferred = new Deferred();
        var settings = {
            reset: true,
            success: success,
            error: error,
            params: {
                deferred: deferred,
                success: options.success,
                error: options.error
            }};

        _.defaults(settings, _.omit(options, "success", "error"));

        Base.prototype.fetch.call(this, settings);

        return deferred.promise();
    }

    function success(collection, response, options) {
        var deferred = options.params.deferred;

        // receive the xhr from REST and remove it from options.
        var xhr = options.xhr; delete options.xhr;
        var status = 'success';
        _.isFunction(options.params.success) && options.params.success.call((options.context||this), response, status, xhr);
        deferred.resolve(response, status, xhr);
    }

    function error(collection, xhr, options) {
        var deferred = options.params.deferred;
        var status = 'error';
        var error = xhr.statusText;
        _.isFunction(options.params.error) && options.params.error.call((options.context||this), xhr, status, error);
        deferred.reject(xhr, status, error);
    }

    function count(options) {
        options || (options = {});

        var resource = this.resource;
        resource || (resource = (this.model && this.model.prototype.resource));

        if(!resource) return;

        var defaults = {
            conjunction: "AND"
        };

        _.defaults(options, defaults);

        var settings = {
            method: "GET",
            async: false,
            data: {}
        };

        options.filters && (settings.data.filters = JSON.stringify(options.filters));

        _.defaults(settings, options);

        _.defaults(settings.data, options.data);

        var promise = this.sync("count", this, settings);

        var c = undefined;
        promise.then(function(response, status, xhr) {
            if(xhr.getResponseHeader('Item-Count')!==null) {
                c = number.parse(xhr.getResponseHeader('Item-Count'));
            }
        });
        
        return c;
    }

    function toArray() {
        return this.models.slice();
    }

    function hasChanged() {
        if(this._hashCode==undefined) return false;
        if(_.keys(this._hashCode).length!==this.size()) return true;

        var result = false;
        for(var i=0; i<this.size(); i++) {
            var model = this.at(i);
            if(hash(model)!==this._hashCode[model.cid]) {
                result = true;
                break;
            }
        }
        return result;
    }

    function hash(model) {
        var hashCode = 0;
        var json = JSON.stringify(model.toJSON());
        for (var i = 0; i < json.length; i++) {
            var char = json.charCodeAt(i);
            hashCode = ((hashCode<<5)-hashCode)+char;
            hashCode = hashCode & hashCode; // Convert to 32bit integer
        }
        return hashCode;
    }

    function baseline() {
        var hashCode = {};

        this.each(function(model) {
            hashCode[model.cid] = hash(model);
        });

        this._hashCode = hashCode;

    }

    function isFetchable() {
        var resource = this.resource;
        resource || (resource = (this.model && this.model.prototype.resource));

        return resource ? true : false;
    }
    
    var props = {
        model: Model,
        resource: undefined,

        _proxy: new REST(),
        _reader: new Json(),
        _total: undefined,
        _limit: undefined,
        _offset: undefined,
        _filters: undefined,
        _sorters: undefined,
        _conjunction: undefined,
        _locale: undefined,
        _hashCode: undefined
    };

    var Collection = declare(Base, {
        initialize: initialize,
        proxy: proxy,
        reader: reader,
        sync: sync,
        request: request,
        download: download,
        parse: parse,
        fetch: fetch,
        count: count,
        size: size,
        total: total,
        limit: limit,
        offset: offset,
        filters: filters,
        sorters: sorters,
        conjunction: conjunction,
        locale: locale,
        toArray: toArray,
        baseline: baseline,
        hasChanged: hasChanged,
        isFetchable: isFetchable
    }, props);

    return Collection;

});