/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define(['underscore', 'jquery', 'uweaver/lang',
    'uweaver/data/Model'], function (_, $, lang, Model) {

    var declare = lang.declare;
    var Base = Model;

    /**
     * A class representing an live data components.
     *
     * @constructor LiveModel
     * @extends Model
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [attributes] - attributes.
     * @param {Object} [options] - A map of additional options to pass to the constructor.
     */
    function initialize(attributes, options) {
        Base.prototype.initialize.apply(this, arguments);

        options || (options = {});

        var defaults = {
            constraints : {}
        };

        _.defaults(options, defaults);

        this._liveFetch = _.throttle(_.bind(Base.prototype.fetch, this), 5000);
    }


    function fetch(options) {
        options || (options = {});

        var defaults = {
            interval: 600
        };

        _.defaults(options, defaults);

        this._liveFetch(options);

        if(this._liveTimer) clearInterval(this._liveTimer);

        this._liveTimer = setInterval(_.bind(this._liveFetch, this, options), options.interval*1000);
    }

    function notify(options) {
        options || (options = {});

        var defaults = {
            interval: 10
        };

        _.defaults(options, defaults);

        this._liveFetch(options);

        if(this._notifyTimer) clearInterval(this._notifyTimer);

        this._notifyTimer = setInterval(_.bind(this._liveFetch, this), options.interval*1000);

        setTimeout(_.bind(clearNotify, this), 120000);
    }

    function stop() {
        if(this._notifyTimer) clearInterval(this._notifyTimer);
        if(this._liveTimer) clearInterval(this._liveTimer);
    }

    function clearNotify() {
        if(this._notifyTimer) clearInterval(this._notifyTimer);
    }

    var props = {
        _liveFetch: undefined,
        _liveTimer: undefined,
        _notifyTimer: undefined
    };

    var LiveModel = declare(Base, {
        initialize: initialize,
        fetch: fetch,
        notify: notify,
        stop: stop
    }, props);


    return LiveModel;

});