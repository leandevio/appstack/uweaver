/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['backbone', 'underscore', 'uweaver/lang', 'uweaver/Deferred',
    'uweaver/data/proxy/Proxy', 'uweaver/data/proxy/REST', 'uweaver/data/reader/Reader', 'uweaver/data/reader/Json',
    'uweaver/validator',  'uweaver/environment', 'uweaver/i18n'], function(Backbone, _, lang, Deferred, Proxy, REST, Reader, Json, validator, env, i18n) {
    var declare = lang.declare;
    var Base = Backbone.Model;

    /**
     * A class representing an data component.
     *
     * This class extends Backbone.Model with the following changes:
     * * url() - rewrite the url() to use the resource property to generate the URL of the model's resource on the server.
     *
     * ##### Usage
     *     // Declare a new type of model, Product, with the following designs:
     *     // * the resource - 'products'
     *     // * the default properties - {price: 0}
     *     // * the business id - the 'id' property.
     *     // * the object id - the '_id' property.
     *     var declare = uweaver.lang.declare;
     *     var Product = declare(Model, {
     *         resource: 'products',
     *         defaults: {
     *             'price': 0
     *         }
     *     });
     *
     *     // Create a product & save it to the server.
     *     // Assign the product a business id - "sunflower".
     *     // The server will assign a object id for the product after saved.
     *     var sunflower = new Product({id: "sunflower", name: "Sunflower", price: 0.5});
     *     sunflower.on('sync', function(product) {
     *         ....
     *     });
     *     sunflower.save();
     *
     *     // fetch a product with a object id, 'F0001', from the server & update the price.
     *     var product = new Product({_id: "F0001"});
     *     product.fetch({async: false});
     *     product.set('price', 2.6);
     * See [Backbone.Model's document](http://backbonejs.org/#Model) for details.
     *
     * ##### Events:
     * See [Backbone.Model's events](http://backbonejs.org/#Events) for details.
     *
     * @constructor Model
     * @extends Backbone.Model
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [attributes] - attributes.
     * @param {Object} [options] - A map of additional options to pass to the constructor.
     * See [Backbone.Model's constructor](http://backbonejs.org/#Model-constructor) for details.
     */
    function initialize(attributes, options) {
        Base.prototype.initialize.apply(this, arguments);

        options || (options = {});

        var defaults = {
            constraints : {}
        };

        _.defaults(options, defaults);

    }


    function proxy(theProxy) {
        if(!theProxy) return this._proxy;

        this._proxy = theProxy;
    }

    function reader(theReader) {
        if(!theReader) return this._reader;

        this._reader = theReader;
    }

    /**
     * Persist the state of a model to the server.
     *
     * @memberof Model#
     * @param {String} method - The method name.
     * @param {Model} data - The model to be saved.
     * @param {Object} [options] - success and error callbacks, and all other jQuery request options.
     * See [Backbone.sync()](http://backbonejs.org/#Sync) for details.
     * @returns {Promise}
     */
    function sync(method, data, options) {
        var resource = this.resource;
        var action = method;

        // use the collection's resource property if the model's is not available.
        resource || (resource = (this.collection && this.collection.resource));

        options.xhr = this.proxy().sync(resource, action, data, options);
        return options.xhr;
    }
    
    function request(subresource, options) {
        var resource = this.resource;

        // use the collection's resource property if the model's is not available.
        resource || (resource = (this.collection && this.collection.resource));

        var deferred = new Deferred();

        this.proxy().request(resource, subresource, this, options).then(function(response) {
            var data = this.parse(response);
            deferred.resolve(data);
        }, function(xhr, status, error) {
            deferred.reject(xhr, status, error);
        }, this);

        return deferred.promise();
    }

    /**
     * called whenever a model's data is returned by the server, in fetch, and save.
     * The function is passed the raw response object, and should return the attributes hash to be set on the model.
     *
     * @memberof Model#
     * @param {Object} response - The raw response object.
     * @param {Object} [options] - optional arguments.
     * See [Backbone.Model.parse()](http://backbonejs.org/#Model-parse) for details.
     * @returns {Object}  the model attributes.
     */
    function parse(response) {
        return this.reader().parse(this, response);
    }

    function fetch(options) {
        options || (options = {});

        var deferred = new Deferred();
        var settings = {
            reset: true,
            success: success,
            error: error,
            params: {
                deferred: deferred,
                success: options.success,
                error: options.error
            }};

        _.defaults(settings, _.omit(options, "success", "error"));

        Base.prototype.fetch.call(this, settings);

        return deferred.promise();
    }

    function save(attributes, options) {
        options || (options = {});

        var deferred = new Deferred();
        var settings = {
            success: success,
            error: error,
            params: {
                deferred: deferred,
                success: options.success,
                error: options.error
            }};

        _.defaults(settings, _.omit(options, "success", "error"));

        Base.prototype.save.call(this, attributes, settings);

        return deferred.promise();
    }

    function destroy(options) {
        options || (options = {});

        var defaults = {
            wait: true
        };

        _.defaults(options, defaults);

        var promise = Base.prototype.destroy.call(this, _.omit(options, "success", "error"));

        promise.then(function(response, status, xhr) {
            _.isFunction(options.success) && options.success.apply((options.context||this), arguments);
        }, function(xhr, status, error) {
            _.isFunction(options.error) && options.error.apply((options.context||this), arguments);
        }, this);

        return promise;

    }

    function get(attribute, options) {
        options || (options = {});

        var rawValue = Base.prototype.get.call(this, attribute);
        var value;

        if(options.locale && isI18nValue(rawValue)) {
            var locale =  i18n.defaultLocale();
            var i18nObject = toI18nObject(rawValue, locale);

            if(_.has(i18nObject, options.locale)) {
                locale = options.locale;
            } else if(_.has(i18nObject, options.locale.substr(0, 2))) {
                locale = options.locale.substr(0, 2);
            }

            value = i18nObject[locale];
        } else {
            value = rawValue;
        }

        return value;
    }

    function set(attribute, value, options) {
        options || (options = {});

        var values = {};

        if(_.isString(attribute)) {
            values[attribute] = value;
        } else {
            values = attribute;
        }

        var attributes = _.reduce(values, function(memo, v, k) {
            var rawValue = this.get(k);
            var value;
            if(options.locale) {
                var i18nObject = toI18nObject(rawValue, i18n.defaultLocale());
                i18nObject[options.locale] = v;
                value = toI18nValue(i18nObject);
            } else {
                value = v;
            }
            memo[k] = value;
            return memo;
        }, {}, this);

        return Base.prototype.set.call(this, attributes, options);
    }

    function toI18nObject(value, locale) {
        var i18nObject;

        try {
            i18nObject = JSON.parse(value);
            if(!_.isObject(i18nObject)) throw new Error("Not a valid i18n JSON string.");
            i18nObject = _.omit(i18nObject, function(v) {return !v});
        } catch(e) {
            i18nObject = {};
            i18nObject[locale] = value;
        }

        return i18nObject;
    }

    function toI18nValue(obj) {
        var i18nObject = _.reduce(obj, function(memo, v, k) {
            v && (memo[k] = v);
            return memo;
        }, {});

        return _.isEmpty(i18nObject) ? null : JSON.stringify(i18nObject);
    }

    function isI18nValue(value) {
        var isTrue = false;

        if(value && _.isString(value)) {
            isTrue = true;
        }

        return isTrue;
    }

    function success(model, response, options) {
        var deferred = options.params.deferred;
        var xhr = options.xhr;
        var status = 'success';
        _.isFunction(options.params.success) && options.params.success.call((options.context||this), response, status, xhr);
        deferred.resolve(response, status, xhr);
    }

    function error(model, xhr, options) {
        var deferred = options.params.deferred;
        var status = 'error';
        var error = null;
        _.isFunction(options.params.error) && options.params.error.call((options.context||this), xhr, status, error);
        deferred.reject(xhr, status, error);
    }

    /**
     * validation logic. Receives the item attributes as well as any options passed to set or save.
     * By default save checks validate before setting any attributes but you may also tell set to validate
     * the new attributes by passing {validate: true} as an option.
     *
     * @memberof Model#
     * @param {Object} attributes - item attributes.
     * @param {Object} [options] - optional arguments.
     * @returns {Array} the violations.
     */
    function validate(attributes, options) {
        var constraints = this.constraints || {};

        var violations = validator.validate(attributes, constraints);

        if(violations.length>0) return violations;
    }
    
    var props = {
        resource: undefined,
        _constraints: undefined,
        _proxy: new REST(),
        _reader: new Json()
    };

    props.idAttribute = env.get('entity').idAttribute;

    var Model = declare(Base, {
        initialize: initialize,
        proxy: proxy,
        reader: reader,
        sync: sync,
        request: request,
        parse: parse,
        validate: validate,
        fetch: fetch,
        save: save,
        destroy: destroy,
        get: get,
        set: set
    }, props);

    return Model;

});