/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/data/Collection', 'uweaver/data/Model', 'uweaver/net'], function(Collection, Model, net) {


    function setLocale(locale) {
        var promise = net.send('session/locale', {
            method: 'PUT',
            data: {
                locale: locale
            }
        });

        promise.then(function(response) {
            var item = this.parse(response);
            this.set(item);
        }, this);

        return promise;
    }

    var Session = declare(Model, {
        setLocale: setLocale
    }, {
        idAttribute: 'uri',
        resource: 'session'
    });
    return Session;
});