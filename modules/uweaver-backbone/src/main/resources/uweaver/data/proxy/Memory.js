/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/data/proxy/Proxy', 'uweaver/environment', 'uweaver/data/Collection',
    'uweaver/Deferred'], function(_, lang, Proxy, env, Collection, Deferred) {
    var declare = lang.declare;
    var Base = Proxy;

    /**
     * REST persist the state of a model/collection to the server
     *
     * @constructor Memory
     * @extends Proxy
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [options] - optional configuration.
     */
    function initialize(options) {
        Base.prototype.initialize.apply(this, arguments);

        options || (options = {});

        var defaults = {};

        _.defaults(options, defaults);

        this._store = (options.store) ? options.store : new Collection();
    }

    /**
     * @override
     */
    function sync(resource, action, data, options) {
        options || (options = {});

        switch(action) {
            case 'create':
                return;
            case 'update':
                return;
            case 'patch':
                return;
            case 'delete':
                return;
            case 'read':
                return read.call(this, resource, data, options);
            default:
                return;
        }
    }

    function create(resource, data, options) {

        var deferred = new Deferred();

        var model = store.create(data.toJSON());
        var status = "ok";

        deferred.resolve(JSON.stringify(model.toJSON()), status);

        return deferred.promise();
    }

    function read(resource, data, options) {

        options || (options = {});

        var store = this.store();
        var response;

        if(data.id) {
            var model = store.get(data.id);
            response = model.toJSON();
        } else {
            var offset = _.isFunction(data.offset) ? data.offset() : 0;
            var limit = _.isFunction(data.limit) ? data.limit() : 500;
            var items = store.filter(function(model, index) {
                var founded = false;
                if(index>=offset && limit>0) {
                    limit--;
                    founded = true;
                }
                return founded;
            });
            response = [];
            _.each(items, function(item) {
                response.push(item.toJSON());
            });

            _.isFunction(data.total) && data.total(store.size());
        }

        var deferred = new Deferred();

        deferred.resolve(response, status);

        _.isFunction(options.success) && options.success.call(this, response, 'ok', null);

        return deferred.promise();
    }

    function update(resource, data, options) {

    }

    function store(items) {
        if(arguments.length===0) return this._store;

        this._store = store;
    }


    var props = {
        _store: undefined
    };

    var Memory = declare(Base, {
        initialize: initialize,
        sync: sync,
        store: store
    }, props);

    return Memory;

});