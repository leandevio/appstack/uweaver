/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/data/proxy/Proxy',
    'uweaver/net', 'uweaver/number', 'uweaver/datetime'], function(_, lang, Proxy, net, number, datetime) {
    var declare = lang.declare;
    var Base = Proxy;

    /**
     * REST persist the state of a model/collection to the server
     *
     * @constructor Proxy
     * @extends Proxy
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [options] - optional configuration.
     */
    function initialize(options) {
        Base.prototype.initialize.apply(this, arguments);

        options || (options = {});

        var defaults = {};

        _.defaults(options, defaults);
    }

    function request(resource, subresource, data, options) {
        options || (options = {});
        var defaults = {};
        _.defaults(options, defaults);

        var settings = {};
        _.defaults(settings, options);
        _.isObject(settings.data) && (settings.data = parseData(settings.data));

        return net.request(makeUrl(resource, data.id, subresource), settings);
    }

    function download(resource, subresource, data, options) {
        options || (options = {});
        var defaults = {};
        _.defaults(options, defaults);

        var settings = {};
        _.defaults(settings, options);
        _.isObject(settings.data) && (settings.data = parseData(settings.data));

        return net.download(makeUrl(resource, data.id, subresource), settings);
    }

    function parseData(any) {
        if(_.isDate(any)) return datetime.format(any);

        if(isPrimitive(any)) return any;

        var json = (_.isObject(any) && _.isFunction(any.toJSON)) ? any.toJSON() : any;
        var data;

        if(_.isArray(json)) {
            data = [];
            _.each(json, function (value) {
                data.push(parseData(value));
            });
        } else {
            data = {};
            _.each(json, function(value, key) {
                data[key] = parseData(value);
            });
        }

        return data;
    }

    function isPrimitive(any) {
        if(!_.isObject(any)) return true;
        if(_.isDate(any)) return true;
        if(_.isNumber(any)) return true;
        if(_.isString(any)) return true;
        if(_.isBoolean(any)) return true;
        if(any instanceof File) return true;

        return false;
    }
    /**
     * @override
     */
    function sync(resource, action, data, options) {
        options || (options = {});

        switch(action) {
            case 'create':
                return create.call(this, resource, data, options);
            case 'update':
                return update.call(this, resource, data, options);
            case 'patch':
                return update.call(this, resource, data, options);
            case 'delete':
                return erase.call(this, resource, data, options);
            case 'read':
                return read.call(this, resource, data, options);
            default:
                return customAction.call(this, resource, action, data, options);
        }
    }

    function customAction(resource, action, data, options) {
        options || (options = {});
        var defaults = {};
        _.defaults(options, defaults);

        var settings = {};
        _.defaults(settings, options);
        _.isObject(settings.data) && (settings.data = parseData(settings.data));

        return net.request(makeUrl(resource, data.id, action), settings);
    }

    function create(resource, data, options) {
        options || (options = {});

        var settings = {
            method: "POST"
        };
        _.defaults(settings, options);
        settings.data = parseData(data);

        return net.request(makeUrl(resource, data.id), settings);
    }

    function read(resource, data, options) {
        options || (options = {});

        var settings = {
            method: "GET",
            data: {}
        };
        _.defaults(settings, options);

        _.isFunction(data.filters) && (settings.data.filters = JSON.stringify(data.filters()));
        _.isFunction(data.sorters) && (settings.data.sorters = JSON.stringify(data.sorters()));
        _.isFunction(data.limit) && (settings.data.limit = data.limit());
        _.isFunction(data.offset) && (settings.data.offset = data.offset());
        _.isFunction(data.conjunction) && (settings.data.conjunction = data.conjunction());
        _.isFunction(data.locale) && (settings.data.locale = data.locale());


        settings.success = function(response, status, xhr) {
            /**
             * @todo check if data has size or total method instead of check if response is an array.
             */
            if(_.isArray(response) && (xhr.getResponseHeader('Item-Count')!==null)) {
                data._total = number.parse(xhr.getResponseHeader('Item-Count'));
            }
            // use the options to send xhr to Collection
            options.xhr = xhr;
            _.isFunction(options.success) && options.success.apply(this, arguments);
        };

        return net.request(makeUrl(resource, data.id), settings);
    }

    function update(resource, data, options) {
        options || (options = {});

        var settings = {
            method: "POST"
        };
        _.defaults(settings, options);
        settings.data = parseData(data);

        return net.request(makeUrl(resource, data.id), settings);
    }

    function erase(resource, data, options) {
        options || (options = {});
        var settings = {
            method: "DELETE"
        };

        _.defaults(settings, options);

        var promise = net.request(makeUrl(resource, data.id), settings);

        return promise;
    }

    function makeUrl(resource, id, action) {
        var url = resource;

        if(id) {
            url = url + '/' + encode(id);
        }

        action && (url = url + '/' + action);

        url = url + '/';

        return url;
    }

    function encode(value) {
        if(!value) return value;

        return value.replace("/", "$(slash)");
    }

    var props = {};

    var REST = declare(Base, {
        initialize: initialize,
        sync: sync,
        request: request,
        download: download
    }, props);

    return REST;

});