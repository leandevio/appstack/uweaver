/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'backbone', 'uweaver/lang', 'uweaver/data/reader/Reader', 'uweaver/environment',
    'uweaver/datetime'], function(_, Backbone, lang, Reader, env, datetime) {
    var declare = lang.declare;
    var Base = Reader;

    // ISO 8601 data & time representation format.
    var DATETIMEPATTERN = "yyyy-MM-ddTHH:mm:ss.SSSZ";

    /**
     * REST persist the state of a model/collection to the server
     *
     * @constructor Json
     * @extends Reader
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [options] - optional configuration.
     */
    function initialize(options) {
        Base.prototype.initialize.apply(this, arguments);
    }

    function parse(data, response) {
        return _.isArray(response) ? parseCollection(data, response) : parseModel(data, response);
    }

    /**
     * called whenever a model's data is returned by the server, in fetch, and save.
     * The function is passed the raw response object, and should return the attributes hash to be set on the model.
     *
     * @memberof Json#
     * @param {Model} model - The model object.
     * @param {Object} response - The raw response object.
     * @returns {Object}  the model attributes.
     */
    function parseModel(model, response) {
        return parseObject(response);
    }

    /**
     * called whenever a collection's data is returned by the server.
     * The function is passed the raw response object, and should return an array of the attributes hash to be set on the model.
     *
     * @memberof Json#
     * @param {Collection} collection - The collection object.
     * @param {Object} response - The raw response object.
     * @returns {Array}  An array of the model attributes.
     */
    function parseCollection(collection, response) {
        return parseArray(response);
    }
    
    function parseAny(any) {
        var value;
        
        if(isPrimitive(any)) {
            if (_.isString(any) && datetime.test(any, DATETIMEPATTERN)) {
                value = datetime.parse(any, DATETIMEPATTERN);
            } else {
                value = any;
            }
        } else if(_.isArray(any)) {
            value = parseArray(any);
        } else {
            value = parseObject(any);
        }
        
        return value;
    }
    
    function parseObject(object) {
        var item = {};
        
        _.each(object, function(value, key) {
            item[key] = parseAny(value);
        });
        
        return item;
    }
    
    function parseArray(array) {
        var items = [];
        _.each(array, function(value) {
            items.push(parseAny(value));
            
        });
        
        return items;
    }

    function isPrimitive(value) {
        if(!_.isObject(value)) return true;
        if(_.isDate(value)) return true;
        if(_.isNumber(value)) return true;
        if(_.isString(value)) return true;
        if(_.isBoolean(value)) return true;

        return false;
    }

    var props = {};

    var Json = declare(Base, {
        initialize: initialize,
        parse: parse,
        parseModel: parseModel,
        parseCollection: parseCollection
    }, props);

    return Json;

});