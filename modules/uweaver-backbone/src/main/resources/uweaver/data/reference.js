/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This module implements the reference data concept(https://en.wikipedia.org/wiki/Reference_data).
 *
 * @module reference
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore', 'uweaver/lang', 'uweaver/data/Model', 'uweaver/data/Collection'], function(_, lang, Model, Collection) {
    var declare = lang.declare;
    var data = {};
    function get(type) {
        var collection = data[type];

        if(!collection||collection.size()==0) {
            var ReferenceDatum = declare(Model, {}, {resource: 'reference/' + type, idAttribute: 'code'});
            var ReferenceData = declare(Collection, {
                translate: function(code, referenceIndex, options) {
                    var referenceDatum = this.get(code);
                    referenceIndex || (referenceIndex = 'text');
                    if(referenceDatum==null) return code;
                    var text = referenceDatum.get(referenceIndex, options);
                    return text ? text : code;
                }
            }, {model: ReferenceDatum});
            collection = new ReferenceData();
            collection.fetch({async: false});
            data[type] = collection;
        }

        return collection;
    }

    var reference = {
        get: get
    };

    return reference;
});