/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * This module provides a collection of utility methods for dom manipulation.
 *
 * @module dom
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore', 'jquery', 'd3', 'uweaver/XElement',
    'uweaver/exception/UnsupportedTypeException'], function(_, $, d3, XElement, UnsupportedTypeException) {
    function toggle(state) {
        var tagName = this.prop('tagName');

        if(tagName!=='TR') {
            var fn = $.fn._toggle;
            return fn.apply(this, arguments);
        }

        if(state!==undefined) {
            this.mark(state);
            return this;
        }

        this.mark(!this.mark());

        return this;
    }

    function mark(state) {
        var selectClass = 'uw-mark';

        if(state===undefined) {
            return this.data('mark') || this.hasClass(selectClass);
        }

        this.data('mark', state);
        this.toggleClass(selectClass, state);

        return this;
    }


    function sizing(any) {
        var html = dom.outerHTML(any);
        var $node = $('<svg>' + html + '</svg>');
        $node.css({display:'', visibility:'hidden', position:'absolute'});

        $node.appendTo($('body'));

        var height = $node.children().outerHeight();
        var width = $node.children().outerWidth();

        // Firebox
        if(!height && !width) {
            var svgRect = $node.children().get(0).getBBox();
            height = svgRect.height;
            width = svgRect.width;
        }

        $node.remove();

        return {
            height: height,
            width: width
        }
    }

    function outerHTML(any) {
        var node = dom.node(any);
        var html = node.outerHTML;

        if(!html) {
            html = $("<div></div>").append($(node).clone()).html();
        }

        return html;
    }

    function node(any) {
        if(any instanceof jQuery) {
            return any.get(0);
        } else if(any instanceof d3.selection) {
            return any.node();
        } else if(any instanceof Element) {
            return any;
        } else if(_.isFunction(any.node)) {
            return any.node();
        }
    }

    function element(any) {
        if(any instanceof jQuery) {
            return any.get(0);
        } else if(any instanceof d3.selection) {
            return any.node();
        } else if(any instanceof Element) {
            return any;
        } else if(_.isFunction(any.node)) {
            return any.node();
        }
    }
    
    function isVisible(any) {
        if(any instanceof jQuery) {
            return any.is(':visible');
        } else if(any instanceof d3.selection) {
            return $(any.node()).is(':visible') ? true : false;
        } else if(any instanceof Element) {
            return $(any).is(':visible') ? true : false;
        } else if(_.isFunction(any.node)) {
            return $(any.node()).is(':visible') ? true : false;
        }
    }

    function select(selector) {
        var xelement;

        if(_.isString(selector)) {
            selector = selector.replace(/^#[0-9]{1}/, "#\\3" + selector.charAt(1) + " ");
            var node = document.querySelector(selector);
            xelement = (node==null) ? null : new XElement(node);
        } else if(selector instanceof Element) {
            xelement = new XElement(selector);
        } else if(selector instanceof XElement) {
            xelement = selector;
        } else {
            throw new UnsupportedTypeException({
                type: typeof any,
                supportedTypes: ['String', 'Element', 'XElement']
            });
        }

        return xelement;
    }

    function createElement(tagName, ns) {
        return new XElement(tagName, {ns: ns});
    }

    var dom = {
        select: select,
        createElement: createElement,
        node: node,
        element: element,
        sizing: sizing,
        isVisible: isVisible,
        outerHTML: outerHTML
    };

    dom.ns = {
        SVG: 'SVG',
        HTML: 'HTML'
    };

    return dom;
});