/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * This module provides the system settings.
 *
 * @module environment
 * @author  Jason Lin
 * @since   1.0
 */
define([
    'underscore', 'jquery', 'uweaver/net'
], function(_, $, net) {

    var _isLoaded = false;

    var _props = {};

    function initialize() {
        net.send("environment", {async:false}).then(
            function(response) {
                var payload = response;

                _props = payload;

                var defaults = {
                    locale: 'en'
                };

                _.defaults(_props, defaults);

                _isLoaded = true;
            },
            function(xhr, status, error) {
                console.log("Failed to load the application properties.");
                console.log("status: " + status);
                console.log("xhr: " + xhr.statusText + "(" + xhr.status + ")");
                console.log("response: " + xhr.responseText);
            }
        );
    }

    /**
     * Get the locale.
     *
     * @memberof module:environment
     * @param {String} [locale] - ETF language tags: <ISO 639 language code>-<ISO 3166-1 country code>.
     * @returns {Object}
     */
    function locale(locale) {
        return this.get('locale');
    }

    function property(name, defaultValue) {
        return _props[name] || defaultValue;
    }

    function get(name, defaultValue) {
        return _props[name] || defaultValue;
    }

    function set(name, value) {
        _props[name] = value;
    }

    function isLoaded() {
        return _isLoaded;
    }

    // function load(url) {
    //     var result = $.ajax(".", {
    //         headers: {
    //             "Content-Type": 'application/json; charset=UTF-8',
    //             "Accept": 'application/json',
    //             "Action": 'helo'
    //         },
    //         dataType: 'json',
    //         async: false
    //     }).then(
    //         function(response) {
    //             var payload = response;
    //
    //             _props = payload;
    //
    //             var defaults = {
    //                 locale: 'en'
    //             };
    //
    //             _.defaults(_props, defaults);
    //
    //             _isLoaded = true;
    //         },
    //         function(xhr, status, error) {
    //             console.log("Failed to load the app.properties");
    //             console.log("status: " + status);
    //             console.log("xhr: " + xhr.statusText + "(" + xhr.status + ")");
    //             console.log("response: " + xhr.responseText);
    //         }
    //     );
    //
    //     return result;
    // }


    return {
        initialize: initialize,
        isLoaded: isLoaded,
        get: get,
        property: property,
        set: set,
        locale: locale
    };

});
