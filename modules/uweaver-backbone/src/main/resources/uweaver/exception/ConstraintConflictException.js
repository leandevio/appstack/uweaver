/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['uweaver/lang', 'uweaver/string', 'uweaver/i18n', 'uweaver/exception/Violation'], function(lang, string, i18n, Violation) {
    var declare = lang.declare;

    var Base = null;

    /**
     * ....
     *
     *
     * @constructor ConstraintConflictException#
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * ##### Options:
     * + entity(Object) - The object caused the exception.
     * + violations(Violation[]) - The violations.
     * + message(String) - The error message.
     */
    function initialize(options) {
        options || (options = {});

        var defaults = {
            entity: null,
            violations: []
        };

        _.defaults(options, defaults);

        options.message && (this.message = options.message);
        this._entity = options.entity;

        this._violations = [];

        _.each(options.violations, function(violation) {
            if(violation instanceof Violation) {
                this._violations.push(violation);
            } else {
                this._violations.push(new Violation(violation));
            }
        }, this);

        this._stack = new Error().stack;
    }

    function entity()  {
        return this._entity;
    }

    function message() {
        return this._message;
    }

    function violations() {
        return this._violations;
    }

    function toString() {
        var message = this.localizedMessage();

        _.each(this._violations, function(violation) {
            message = message + '\n' + violation.toString();
        });

        return message;
    }

    function localizedMessage() {
        return i18n.message(this._message);
    }

    function stack() {
        return this._stack;
    }

    var props = {
        _name: "ConstraintConflictException",
        _message: "Constraint violation!",
        _entity: null,
        _violations: null,
        _stack: null
    };

    var ConstraintConflictException = declare(Base, {
        initialize: initialize,
        entity: entity,
        message: message,
        violations: violations,
        localizedMessage: localizedMessage,
        stack: stack,
        toString: toString
    }, props);

    return ConstraintConflictException;
});