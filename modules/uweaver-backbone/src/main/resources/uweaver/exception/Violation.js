/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/string', 'uweaver/i18n'], function(_, lang, string, i18n) {
    var declare = lang.declare;
    var Base = null;
    var name = "Violation";

    /**
     * ....
     *
     *
     * @constructor Violation
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {Violation.ERROR|String} [config.error=Violation.ERROR.BADINPUT] - The violation error.
     * @param {String[]} [config.properties] - The properties violate the validation.
     * @param {Object[]} [config.parameters] - The arguments related to the error message.
     * @param {String} [config.message] - The error message.
     */
    function initialize(config) {
        config || (config = {});

        var defaults = {};

        _.defaults(config, defaults);

        this._error = config.error || Violation.Error.BADINPUT;
        this._properties = config.properties || [];
        this._parameters = config.parameters || [];
        this._message = config.message ? config.message : Violation.Message[this._error.toUpperCase()];

        calibrate.call(this)
    }

    function calibrate() {
        if(this._error.toUpperCase()===Violation.Error.STEPMISMATCH) {
            var step = (this._parameters[0]===undefined) ? 1 : this._parameters[0];
            if(step===1) {
                this._message = Violation.Message.STEPMISMATCH_INTEGER;
            } else if(/0\.0*1/.test(step)) {
                this._message = Violation.Message.STEPMISMATCH_SCALE;
            }
        }
    }

    function toString() {
        var parameters = this._parameters;
        var defaults = {
            error: this._error,
            properties: this._properties.join('.')
        };

        _.defaults(parameters, defaults);

        var text = _.reduce(this._properties, function(text, property){return text + i18n.translate(property) + ", "}, "");

        if(text.length>0) {
            text = text.substr(0, text.length-2) + " : ";
        }

        text = text + string.substitute(this.localizedMessage(), parameters);

        return text;
    }

    function localizedMessage() {
        var key = (this._message) ? this._message : this._error;
        return i18n.message(key);
    }

    function message() {
        return this._message;
    }

    function error() {
        return this._error;
    }

    function property() {
        return _.first(this._properties);
    }

    function properties() {
        return this._properties;
    }

    var props = {
        _error: null,
        _properties: null,
        _parameters: null,
        _message: "Error: ${error}; Properties: ${properties}."
    };

    var Violation = declare(Base, {
        initialize: initialize,
        error: error,
        properties: properties,
        property: property,
        message: message,
        localizedMessage: localizedMessage,
        toString: toString
    }, props);

    Violation.Error = {
        "TYPEMISMATCH": "TYPEMISMATCH",
        "PATTERMMISMATCH": "PATTERMMISMATCH",
        "TOOLONG": "TOOLONG",
        "TOOSHORT": "TOOSHORT",
        "RANGEUNDERFLOW": "RANGEUNDERFLOW",
        "RANGEOVERFLOW": "RANGEOVERFLOW",
        "STEPMISMATCH": "STEPMISMATCH",
        "VALUEMISSING": "VALUEMISSING",
        "BADINPUT": "BADINPUT"
    };

    Violation.Message = {
        "TYPEMISMATCH": "TYPEMISMATCH",
        "PATTERMMISMATCH": "The value must match the pattern(${0}).",
        "TOOLONG": "The maximum length for this field is ${0}",
        "TOOSHORT": "The minimum length for this field is ${0}",
        "RANGEUNDERFLOW": "Value mush be greater than or equal to ${0}.",
        "RANGEOVERFLOW": "Value mush be less than or equal to ${0}.",
        "STEPMISMATCH": "The legal number intervals for the input field is ${0}.",
        "STEPMISMATCH_INTEGER": "Please enter an integer.",
        "STEPMISMATCH_SCALE": "The maximum number of decimal places is ${0}",
        "VALUEMISSING": "This field is required.",
        "BADINPUT": "The value in this field is invalid."
    };

    return Violation;
});