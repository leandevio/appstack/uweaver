/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * The formatter module provides a collection of utility methods for date & number formatting.
 *
 * The formats are specified by the pattern string. The pattern strings are designed to
 * conform to java.text.SimpleDateFormat and java.text.DecimalFormat.
 *
 * ##### Usage
 *     // Mar 04 2010 05:06:07.008 at local time
 *     var localTime = new Date(2010, 2, 4, 5, 6, 7, 8);
 *
 *     // format by the pattern.
 *     var options = {};
 *
 *     options.pattern = "yyyy/MM/dd HH:mm:ss.SSS";
 *     expect(formatter.format(localTime, options)).to.equal("2010/03/04 05:06:07.008");
 *
 *     options.pattern = "yyyy/MM/dd";
 *     expect(formatter.format(localTime, options)).to.equal("2010/03/04");
 *
 *     options.pattern = "NT$#,##0.##";
 *     expect(formatter.format(123456.789, options)).to.equal("NT$123,456.79");
 *
 *     options.pattern = "#,##0.##";
 *     expect(formatter.format(123456, options)).to.equal("123,456");
 *
 *     // format by the subtype and locale.
 *     var options = {};
 *     var locale = {language: "en", country: "US"};
 *     options.locale = locale;
 *
 *     options.subtype = undefined;
 *     expect(i18n.datetimePattern(locale)).to.equal("MM/dd/yyyy HH:mm:ss");
 *     expect(formatter.format(localTime, options)).to.equal("03/04/2010 05:06:07");
 *
 *     options.subtype = "date";
 *     expect(i18n.datePattern(locale)).to.equal("MM/dd/yyyy");
 *     expect(formatter.format(localTime, options)).to.equal("03/04/2010");
 *
 *     options.subtype = "time";
 *     expect(i18n.timePattern(locale)).to.equal("HH:mm:ss");
 *     expect(formatter.format(localTime, options)).to.equal("05:06:07");
 *
 *     options.subtype = "currency";
 *     expect(i18n.currencyPattern(locale)).to.equal("USD#,##0.##");
 *     expect(formatter.format(123456.789, options)).to.equal("USD123,456.79");
 *
 *     options.subtype = undefined;
 *     expect(i18n.numberPattern(locale)).to.equal("#,##0.##");
 *     expect(formatter.format(123456, options)).to.equal("123,456")
 *
 * @module formatter
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore', 'uweaver/lang', 'uweaver/i18n', 'uweaver/datetime',
    'uweaver/number'], function(_, lang, i18n, datetime, number) {
    /**
     * The method formats a value to a string representation using the specified subtype, locale and pattern.
     *
     * @memberof module:formatter
     * @param {Date|Number|Boolean|String} value - the value to format.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {String} [options.subtype] - specify the subtype of the value to select to pattern to format the value.
     * @param {Locale} [options.locale] - specify the locale to select the pattern to format the value.
     * @param {String} [options.pattern] - specify a pattern to format the value.
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {String}
     */
    function format(value, options) {
        if(value===undefined||value===null) return "";

        if(typeof(value)==="string") return value;

        options || (options = {});

        var type = lang.type(value);

        var text;

        if(type==="date") {
            text = formatDate(value, options);
        } else if(type==="number") {
            text = formatNumber(value, options);
        } else {
            text = value.toString();
        }

        return text;
    }

    /**
     * The method parse a string to a value using the specified type, subtype, locale and pattern.
     *
     * @memberof module:formatter
     * @param {String} text - the string to parse.
     * @param {String} type - the type of return value.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {String} [options.subtype] - specify the subtype of the value to select to pattern to format the value.
     * @param {Locale} [options.locale] - specify the locale to select the pattern to format the value.
     * @param {String} [options.pattern] - specify a pattern to format the value.
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {Date|Number|Boolean|String}
     */
    function parse(text, type, options) {
        if(text===undefined||text===""||text===null) return null;

        options || (options = {});

        var value;
        if(type==="date") {
            value = parseDate(text, options);
        } else if(type==="number") {
            value = parseNumber(text, options);
        } else if(type==='string') {
            value = text;
        }

        return value;
    }

    /**
     * The method tests if a string representation is valid using the specified type, subtype, locale and pattern.
     *
     * @memberof module:formatter
     * @param {String} text - the string to test.
     * @param {String} type - the type of the string representation.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {String} [options.subtype] - specify the subtype of the value to select to pattern to format the value.
     * @param {Locale} [options.locale] - specify the locale to select the pattern to format the value.
     * @param {String} [options.pattern] - specify a pattern to format the value.
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {Boolean}
     */
    function test(text, type, options) {
        if(text===undefined||text===""||text===null) return true;

        options || (options = {});

        var compliant;
        if(type==="date") {
            compliant = testDate(text, options);
        } else if(type==="number") {
            compliant = testNumber(text, options);
        } else if(type==='string') {
            compliant = text;
        }

        return compliant;
    }

    function testDate(text, options) {
        options || (options = {});

        var pattern;
        var subtype = options.subtype;
        var locale = options.locale;

        if(options.pattern) {
            pattern = options.pattern;
        } else if(subtype==="date") {
            pattern = i18n.datePattern(locale);
        } else if(subtype==="time") {
            pattern = i18n.timePattern(locale);
        } else if(subtype==="month") {
            pattern = i18n.monthPattern(locale);
        } else {
            pattern = i18n.datetimePattern(locale);
        }

        return datetime.test(text, pattern);
    }

    function testNumber(text, options) {
        options || (options = {});

        var pattern, decimalSeparator, groupingSeparator;
        var subtype = options.subtype;
        var locale = options.locale;

        if(options.pattern) {
            pattern = options.pattern;
        } else if(subtype==="currency") {
            pattern = i18n.currencyPattern(locale);
        } else {
            pattern = i18n.numberPattern(locale);
        }

        decimalSeparator = options.decimalSeparator ? options.decimalSeparator : i18n.decimalSeparator(locale);
        groupingSeparator = options.groupingSeparator ? options.groupingSeparator : i18n.groupingSeparator(locale);

        return number.test(text, pattern, {
            decimalSeparator: decimalSeparator,
            groupingSeparator: groupingSeparator
        });
    }


    function parseDate(text, options) {
        options || (options = {});

        var pattern;
        var subtype = options.subtype;
        var locale = options.locale;

        if(options.pattern) {
            pattern = options.pattern;
        } else if(subtype==="date") {
            pattern = i18n.datePattern(locale);
        } else if(subtype==="time") {
            pattern = i18n.timePattern(locale);
        } else if(subtype==="month") {
            pattern = i18n.monthPattern(locale);
        } else {
            pattern = i18n.datetimePattern(locale);
        }

        return datetime.parse(text, pattern);
    }

    function parseNumber(text, options) {
        options || (options = {});

        var pattern, decimalSeparator, groupingSeparator;
        var subtype = options.subtype;
        var locale = options.locale;

        if(options.pattern) {
            pattern = options.pattern;
        } else if(subtype==="currency") {
            pattern = i18n.currencyPattern(locale);
        } else {
            pattern = i18n.numberPattern(locale);
        }

        decimalSeparator = options.decimalSeparator ? options.decimalSeparator : i18n.decimalSeparator(locale);
        groupingSeparator = options.groupingSeparator ? options.groupingSeparator : i18n.groupingSeparator(locale);

        return number.parse(text, pattern, {
            decimalSeparator: decimalSeparator,
            groupingSeparator: groupingSeparator
        });
    }

    function formatDate(value, options) {
        options || (options = {});

        var pattern;
        var subtype = options.subtype;
        var locale = options.locale;

        if(options.pattern) {
            pattern = options.pattern;
        } else if(subtype==="date") {
            pattern = i18n.datePattern(locale);
        } else if(subtype==="time") {
            pattern = i18n.timePattern(locale);
        } else if(subtype==='month') {
            pattern = i18n.monthPattern(locale);
        } else {  pattern = i18n.datetimePattern(locale);
        }

        return datetime.format(value, pattern);

    }

    function formatNumber(value, options) {
        options || (options = {});

        var pattern, decimalSeparator, groupingSeparator;
        var subtype = options.subtype;
        var locale = options.locale;

        if(options.pattern) {
            pattern = options.pattern;
        } else if(subtype==="currency") {
            pattern = i18n.currencyPattern(locale);
        } else {
            pattern = i18n.numberPattern(locale);
        }

        decimalSeparator = options.decimalSeparator ? options.decimalSeparator : i18n.decimalSeparator(locale);
        groupingSeparator = options.groupingSeparator ? options.groupingSeparator : i18n.groupingSeparator(locale);

        return number.format(value, pattern, {
            decimalSeparator: decimalSeparator,
            groupingSeparator: groupingSeparator
        });
    }

    return {
        format: format,
        parse: parse,
        test: test
    };
});