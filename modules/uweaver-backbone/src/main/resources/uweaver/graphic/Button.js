/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/lang', 'uweaver/dom', 'uweaver/graphic/Graphic',
        'uweaver/Icon',
        'uweaver/exception/UnsupportedTypeException'],
    function (_, lang, dom, Graphic,
              Icon,
              UnsupportedTypeException){

        var declare = lang.declare;
        var Base = Graphic;
        var WIDTH = 24, HEIGHT = 24, BACKGROUND = 'aliceblue', DATA = lookup(NAME);
        /**
         * **Button**
         * This class represents a Button.
         *
         * @constructor Button
         * @extends Graphic
         * @author  Jason Lin
         * @since   1.0
         * @param {Object} [cfg] - A map of configuration to pass to the constructor.
         * @param {Object} [cfg.text] - Specifies the text of the button.
         * @param {Object} [cfg.icon] - Specifies the name of the icon.
         * @param {Object} [cfg.background] - Specifies the css style of the background.
         * @param {Object} [cfg.value] - Specifies the data of event when clicked.
         * @param {Float} [cfg.width] - Specifies the width of the button.
         * @param {Float} [cfg.height] - Specifies the height of the button.
         */
        function initialize(cfg) {
            Base.prototype.initialize.apply(this, arguments);

            var cfg = this.cfg();
            var defaults = {};
            _.defaults(cfg, defaults);

            this._background = this._el.query('rect') || (this._background = this._el.append('rect', dom.ns.SVG));
            this._background.style({fill: BACKGROUND});
            this._path = this._el.query('path') || (this._path = this._el.append('path', dom.ns.SVG));

            this._el.node().addEventListener('click', _.bind(onClick, this));
            cfg.name && this._path.attr('name', cfg.name);
            cfg.width && this.width(cfg.width);
            cfg.height && this.height(cfg.height);
            cfg.background && this.background(cfg.background);

            this.width()===0 && this.width(WIDTH);
            this.height()===0 && this.height(HEIGHT);
        }

        function render(options) {
            options || (options = {});

            var isVisible = this.isVisible();
            this.hide();

            var item = lookup(this.name());
            this._path.attr('name', item.name);
            this._el.attr('viewBox', item.viewBox);
            this._path.attr('d', item.path);
            this._background.style({width:"100%", height:"100%"});

            (!options.hidden || isVisible) && this.show();

            return this;
        }

        function name(text) {
            if(text===undefined) return this._path.attr('name');
            this._path.attr('name', text);
            this.render();
        }

        function background(name, value) {
            this._background.style.apply(this._background, arguments);
        }

        function onClick(event) {
            this.trigger('click', {
                data: this,
                context: this,
                source: this
            });
        }

        function lookup(name) {
            var item = _.find(data, function(datum) {
                return datum.name===name;
            });

            return item ? item : DATA;
        }

        var props = {
            _background: undefined,
            _path: undefined
        };

        var Button = declare(Base, {
            initialize: initialize,
            render: render,

            name: name,
            background: background
        }, props);

        return Button;


    });