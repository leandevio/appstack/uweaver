/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['backbone', 'underscore', 'uweaver/lang', 'uweaver/dom', 'uweaver/BaseObject', 'uweaver/XElement',
    'uweaver/exception/UnsupportedTypeException'], function(Backbone, _, lang, dom, BaseObject, XElement, UnsupportedTypeException) {
    var declare = lang.declare;
    var Base = BaseObject;

    /**
     * A class representing an user interface components.
     *
     * The Graphic use html template to layout & construct the DOM element.
     *
     * ##### Events:
     * + 'render' - [render()]{@link Graphic#render}
     * + 'attach' - [attach()]{@link Graphic#attach}
     * + 'detach' - [detach()]{@link Graphic#detach}
     * + 'destroy' - [destroy()]{@link Graphic#destroy}
     *
     * @constructor Graphic
     * @extends BaseObject
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [cfg] - A map of configuration to pass to the constructor.
     */
    function initialize(cfg) {
        var defaults = {};

        cfg || (cfg = {});

        this._cfg = _.extend(cfg, defaults);
        this._el = cfg.el ? dom.select(cfg.el) : new XElement(this.tagName, {ns: XElement.ns.SVG});

        this._oid = _.uniqueId();

        this.prop('id') || (this.prop('id', this._oid));
        cfg.style && this.style(cfg.style);
    }

    /**
     * Render the Graphic.
     *
     * This method compiles the template to generate the html, then construct the DOM element from html.
     *
     * @memberof Graphic#
     * @protected
     * @param {Object} [options] - Rendering options.
     * @param {Boolean} [options.hidden=false] - A boolean value to specify the visibility after rendered.
     * @returns {Graphic} this.
     */
    function render(options) {
        var defaults = {
            hidden: false
        };
        options || (options = {});
        _.defaults(options, defaults);

        this.hide();

        if(this.render===render) {
            this.trigger('render', {
                data: this,
                context: this,
                source: this
            });
            options.hidden || this.show();
        }
        return this;
    }

    /**
     * Get or set the value of the config.
     *
     * @memberof Graphic#
     * @returns {Object} the config.
     */
    function cfg() {
        return this._cfg;
    }

    function oid() {
        return this._oid;
    }

    /**
     * Get or set the value of a style property.
     *
     * @memberof Graphic#
     * @param {Object} name - The css property name.
     * @param {Object} [value] - A value to set for the property.
     * @returns {Object} css style.
     */
    function style(name, value) {
        var rtnValue = this._el.style.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    /**
     * Get or set the value of an attribute.
     *
     * @memberof Graphic#
     * @param {String} name - The name of the attribute.
     * @param {Object} [value] - A value to set for the attribute.
     * @returns {*} the value of the attribute.
     */
    function attr(name, value) {
        var rtnValue = this._el.attr.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }


    /**
     * Get or set the value of a property.
     *
     * @memberof Graphic#
     * @param {String} name - The name of the property.
     * @param {Object} [value] - A value to set for the property.
     * @returns {*} The value of the property.
     */
    function prop(name, value) {
        var rtnValue = this._el.prop.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    /**
     * Add the specified class(es) to the Graphic.
     *
     * @memberof Graphic#
     * @param {String} classNames - One or more space-separated classes to be added.
     * @returns {Graphic} this
     */
    function addClass(classNames) {
        var rtnValue = this._el.addClass.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    /**
     * Remove the specified class(es) from the Graphic.
     *
     * @memberof Graphic#
     * @param {String} names - One or more space-separated classes to be removed.
     * @returns {Graphic} this
     */
    function removeClass(names) {
        var rtnValue = this._el.removeClass.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    /**
     * Determine whether the Graphic are assigned the given class.
     *
     * @memberof Graphic#
     * @param {String} name - The class name to search for.
     * @returns {Boolean}
     */
    function hasClass() {
        return this._el.hasClass.apply(this._el, arguments);
    }

    /**
     * Display the Graphic.
     *
     * @memberof Graphic#
     * @param {Object} [options] - A map of additional options to pass to the method.
     * See [jQuery.show(options)](https://api.jquery.com/show/) for details.
     * @returns {Graphic} this
     */
    function show(options) {
        this._el.show.apply(this._el, arguments);

        this.trigger('show', {
            data: this,
            context: this,
            source: this
        });

        return this;
    }

    /**
     * Hide the Graphic.
     *
     * @memberof Graphic#
     * @param {Object} [options] - A map of additional options to pass to the method.
     * See [jQuery.hide(options)](https://api.jquery.com/hide/) for details.
     * @returns {Graphic} this
     */
    function hide(options) {
        this._el.hide.apply(this._el, arguments);

        this.trigger('hide', {
            data: this,
            context: this,
            source: this
        });

        return this;
    }

    /**
     * Display or hide the Graphic.
     *
     * @memberof Graphic#
     * @param {Object} [options] - A map of additional options to pass to the method.
     * See [jQuery.toggle(options)](https://api.jquery.com/toggle/) for details.
     * @returns {Graphic} this
     */
    function toggle(options) {
        this._el.toggle.apply(this._el, arguments);
        return this;
    }

    function isVisible() {
        return this._el.isVisible();
    }

    function isRendered() {
        return this._isRendered;
    }


    /**
     * Destroy the Graphic from the DOM.
     *
     * This method removes the Graphic from the DOM & destroy the Graphic.
     * This method also remove the event handlers registered using .listenTo().
     *
     * ##### Events:
     * + 'destroy' - triggered before destroy.
     *
     * @memberof Graphic#
     * @return void
     */
    function destroy() {
        this.trigger('destroy', {
            data: {},
            context: this,
            source: this
        });
        this.stopListening();
        this.remove();
    }

    /**
     * A helper method to find the dom elements matched the selector.
     *
     * @memberof Graphic#
     * @param {String} selector - css selector.
     * @returns {NodeList}
     */
    function query(selector) {
        return this._el.query.apply(this._el, arguments);
    }

    function queryAll(selector) {
        return this._el.queryAll.apply(this._el, arguments);
    }

    function width(value) {
        var rtnValue = this._el.width.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function innerWidth(value) {
        var rtnValue = this._el.innerWidth.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function height(value) {
        var rtnValue = this._el.height.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function innerHeight(value) {
        var rtnValue = this._el.innerHeight.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function padding(position, value) {
        var rtnValue = this._el.padding.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function borderWidth(position, value) {
        var rtnValue = this._el.borderWidth.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function left(value) {
        var rtnValue = this._el.left.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function top(value) {
        var rtnValue = this._el.top.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function right(value) {
        var rtnValue = this._el.right.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function bottom(value) {
        var rtnValue = this._el.bottom.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function move(x, y) {
        this._el.move.apply(this._el, arguments);
        return this;
    }

    function html(value) {
        var rtnValue = this._el.html.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function text(value) {
        var rtnValue = this._el.text.apply(this._el, arguments);
        return (rtnValue===this._el) ? this : rtnValue;
    }

    function append(any) {
        var element;
        if(any instanceof Graphic) {
            element = any.node();
        } else if(_.isString(any)) {
            element = new XElement(any, {ns: XElement.ns.SVG});
        } else if(any instanceof SVGElement) {
            element = new XElement(any);
        } else {
            throw new UnsupportedTypeException({
                type: typeof any,
                supportedTypes: ['String', 'SVGElement', 'Graphic']
            });
        }

        this._el.append(element);
        return element;
    }

    function appendTo(any) {
        var element;
        if(any instanceof Graphic) {
            element = any.node();
        } else if(any instanceof XElement) {
            element = any;
        } else {
            element = dom.select(any);
        }

        element.append(this.node());

        return this;
    }

    function remove() {
        this._el.remove.apply(this._el, arguments);
        return this;
    }

    function node() {
        return this._el.node();
    }

    function el() {
        return this._el;
    }

    function toJSON() {
        return {
            class: this._class,
            oid: this._oid,
            tagName: this.tagName,
            className: this.className,
            html: this._el.html()
        }
    }

    function disable() {
        this._availability = false;
        return this;
    }

    function enable() {
        this._availability = true;
        return this;
    }

    function isAvailable() {
        return this._availability;
    }

    function listenTo(target, event, callback, options) {
        if(!target) return;

        options || (options = {});
        var defaults = {
            context: this
        };
        _.defaults(options, defaults);
        var fn = _.bind(callback, options.context);
        if(target instanceof Element) {
            target.addEventListener(event, fn);
        } else if(target instanceof XElement) {
            target.addEventListener(event, fn);
        } else if(target instanceof Backbone.View) {
            Base.prototype.listenTo.call(this, target, event, fn);
        } else if(target instanceof Backbone.Model) {
            Base.prototype.listenTo.call(this, target, event, fn);
        } else if(target instanceof Backbone.Collection) {
            Base.prototype.listenTo.call(this, target, event, fn);
        } else if(target.on===Base.prototype.on) {
            Base.prototype.listenTo.call(this, target, event, fn);
        }
    }

    var props = {
        /**
         * The initial configuration of the Graphic.
         *
         * @memberof Graphic#
         * @protected
         * @type Object
         */
        _cfg: undefined,
        _el: undefined,
        _oid: undefined,
        _class: 'Graphic',
        _availability: false,

        /**
         * The html tag name of the Graphic.
         *
         * @memberof Graphic#
         * @protected
         * @type String
         * @default "svg"
         */
        tagName: 'svg'
    };

    var Graphic = declare(Base, {
        initialize: initialize,
        cfg: cfg,
        oid: oid,
        node: node,
        el: el,
        style: style,
        prop: prop,
        attr: attr,
        height: height,
        innerHeight: innerHeight,
        width: width,
        innerWidth: innerWidth,
        padding: padding,
        borderWidth: borderWidth,
        left: left,
        top: top,
        right: right,
        bottom: bottom,
        move: move,
        html: html,
        text: text,
        append: append,
        appendTo: appendTo,
        remove: remove,
        toJSON: toJSON,

        listenTo: listenTo,

        addClass: addClass,
        removeClass: removeClass,
        hasClass: hasClass,
        show: show,
        hide: hide,
        toggle: toggle,
        enable: enable,
        disable: disable,
        isVisible: isVisible,
        isAvailable: isAvailable,
        destroy: destroy,
        query: query,
        queryAll: queryAll,
        render: render,
        isRendered: isRendered
    }, props);

    return Graphic;

});