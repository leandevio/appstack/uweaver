/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/lang', 'uweaver/dom', 'uweaver/graphic/Graphic',
    'uweaver/exception/UnsupportedTypeException',
    'text!./icon.json'],
    function (_, lang, dom, Graphic,
              UnsupportedTypeException,
              resource){

    var declare = lang.declare;
    var Base = Graphic;
    var data = JSON.parse(resource);
    var MISSINGIMAGE = lookup('broken image'), DEFAULTIMAGE = lookup('link');
    /**
     * **Icon**
     * This class represents a icon.
     *
     * @constructor Icon
     * @extends Graphic
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [cfg] - A map of configuration to pass to the constructor.
     * @param {String} [cfg.value='link'] - The value of the svg to load.
     * @param {Object} [cfg.background] - Specifies the css style of the background.
     * @param {Float} [cfg.width] - Specifies the width of the icon.
     * @param {Float} [cfg.height] - Specifies the height of the icon.
     */
    function initialize(cfg) {
        Base.prototype.initialize.apply(this, arguments);
        if(!(this.node() instanceof SVGSVGElement)) {
            throw new UnsupportedTypeException({
                type: this.node().tagName,
                supportedTypes: ['SVG']
            });
        }

        var cfg = this.cfg();
        var defaults = {};
        _.defaults(cfg, defaults);

        this._background = this.query('rect[role=background]') || this.append('rect', dom.ns.SVG).attr('role', 'background')
                .style({opacity: "0", fill: 'white'});
        this._background.style({width:"100%", height:"100%"});

        this._canvas = this.query('g[role=canvas]') || this.append('g', dom.ns.SVG).attr('role', 'canvas');

        this._width = this.width();
        this._height = this.height();

        this.node().addEventListener('click', _.bind(onClick, this));

        cfg.value && this.value(cfg.value, {render: false});
        cfg.width && this.width(cfg.width, {render: false});
        cfg.height && this.height(cfg.height, {render: false});
    }

    function render(options) {
        options || (options = {});

        value = this.value();

        if(value===null||value===undefined) return this;

        this.hide();
        this._canvas.empty();

        var isVisible = this.isVisible();
        var item = lookup(value);

        this._width || Base.prototype.width.call(this, item.width);
        this._height || Base.prototype.height.call(this, item.height);

        this.attr('viewBox', item.viewBox);
        _.each(item.paths, function(path) {
            this._canvas.append('path', dom.ns.SVG).attr(path);
        }, this);

        (!options.hidden || isVisible) && this.show();

        return this;
    }

    function value(text, options) {
        if(arguments.length===0) return this._value;

        options || (options = {});
        var defautls = {
            render: true
        };
        _.defaults(options, defautls);

        this._value = text;
        options.render && this.render();

        return this;
    }

    function width(value, options) {
        var rtnValue = Base.prototype.width.apply(this, arguments);

        if(arguments.length===0) return rtnValue;

        options || (options = {});
        var defautls = {
            render: true
        };
        _.defaults(options, defautls);

        this._width = value;
        options.render && this.render();

        return this;
    }

    function height(value, options) {
        var rtnValue = Base.prototype.height.apply(this, arguments);

        if(arguments.length===0) return rtnValue;

        options || (options = {});
        var defautls = {
            render: true
        };
        _.defaults(options, defautls);

        this._height = value;
        options.render && this.render();

        return this;
    }

    function onClick(event) {
        this.trigger('click', {
            data: this.value(),
            context: this,
            source: this
        });
    }

    function lookup(value) {
        if(!_.isString(value)) return MISSINGIMAGE;

        var tag = value.replace(' ', '_');
        var item = _.find(data, function(datum) {
            return _.contains(datum.tags, tag);
        });

        return item ? item : MISSINGIMAGE;
    }

    var props = {
        _background: undefined,
        _canvas: undefined,
        _value: undefined,
        _width: undefined,
        _height: undefined
    };

    var Icon = declare(Base, {
        initialize: initialize,
        render: render,

        value: value,
        width: width,
        height: height
    }, props);

    return Icon;


});