/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * This module provides a collection of utility methods for internationalization.
 *
 * ##### Usage
 *     var locale = {
 *         language: "zh",
 *         country: "TW"
 *     };
 *
 *     var options = {
 *         locale: locale,
 *     };
 *     expect(i18n.translate("Purchase Order", options)).to.equal("採購單");
 *     expect(i18n.translate("total: ${0} records", options)).to.equal("總計：${0} 筆記錄");
 *
 *     options.namespace = "message";
 *     expect(i18n.translate("Please select one record.", options)).to.equal("請選擇一筆記錄．");
 *
 *     i18n.locale(locale);
 *     expect(i18n.locale()).to.deep.equal(locale);
 *
 *     expect(i18n.translate("Purchase Order")).to.equal("採購單");
 *
 * @module i18n
 * @author  Jason Lin
 * @since   1.0
 */

define(['underscore', 'uweaver/browser', 'i18next', 'uweaver/net'], function (_, browser, i18next, net) {
    var DEFAULTPATTERN = {
        'date': "yyyy-MM-dd",
        'datetime': "yyyy-MM-dd HH:mm",
        'month': "yyyy-MM",
        'time': 'HH:mm',
        "number": "##,##0.###",
        "currency": "$##,##0.###"
    };

    var TW2CNMAP = {};
    var CN2TWMAP = {};

    var _defaultLocale = 'en';

    /**
     * initialize the i18n module.
     *
     * @memberof module:i18n
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Object} [options.locale] - The locale for i18n.
     * @param {Object} [options.namespaces=['term', 'message']] - The message resources.
     */
    function initialize(options) {
        options || (options = {});

        var defaults = {
            namespaces: ['term', 'app', 'message']
        };

        _.defaults(options, defaults);

        options.locale && (_defaultLocale = options.locale);

        var settings = {
            resGetPath: 'nls/__lng__/__ns__.json',
            fallbackLng: _defaultLocale,
            detectLngQS: 'lang',
            getAsync: false,
            ns: {
                namespaces: options.namespaces,
                defaultNs: options.namespaces[0]
            },
            nsseparator: '::',
            keyseparator: '->'
        };

        i18next.init(settings);

        locale(browser.locale());

        net.send('i18n/terms/CHARACTER', {
            responseType: 'json'
        }).then(function(response, status, xhr) {
            _.each(response, function(entry) {
                TW2CNMAP[entry.tw] = entry.cn;
                CN2TWMAP[entry.cn] = entry.tw;
            });
        });

    }

    /**
     * translate the words.
     *
     * @memberof module:i18n
     * @param {String|Object} words - the words to translate.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {String} [options.locale] - The locale to use for translation.
     * @param {String} [options.namespace] - The namespace to lookup the translation.
     * @param {String} [options.defaultValue] - The default value to return when nothing match.
     * @returns {String|Object}
     */
    function translate(words, options) {
        options || (options = {});

        var defaults = {};

        _.defaults(options, defaults);

        if(typeof(words) === 'string') {
            return find(words, options);
        } else if(words instanceof Object) {
            var values = {};
            _.each(words, function (words, key) {
                if(words instanceof Object) {
                    values[key] = translate(words)
                } else {
                    values[key] = find(words, options);
                }
            });
            return values;
        }
    }

    function message(key, options) {
        options || (options = {});

        var defaults = {
            namespace: 'message'
        };

        _.defaults(options, defaults);

        return find(key, options);
    }

    function find(key, options) {
        options || (options = {});

        key && (key = key.trim());

        var lang = options.locale;

        var text = i18next.translate(key, {lng: lang, defaultValue: options.defaultValue});

        (text===key) && (text = i18next.translate(key, {ns: 'message', lng: lang, defaultValue: options.defaultValue}));

        return text;
    }

    /**
     * convert the words from Traditional Chinese to Simplified Chinese.
     *
     * @memberof module:i18n
     * @param {String|Object|Array} any - anything to convert.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Object} [options.exclude] - the key to exclude from the conversion.
     * @returns {String|Object}
     */
    function tw2cn(any, options) {
        options || (options = {});

        var defaults = {
            exclude: []
        };
        var data;
        _.defaults(options, defaults);
        _.isString(options.exclude) && (options.exclude = [options.exclude]);

        if(_.isString(any)) {
            var s = [];
            for(var i=0; i<any.length; i++) {
                var c_tw = any.charAt(i);
                var c_cn = TW2CNMAP[c_tw];
                s.push(c_cn ? c_cn : c_tw);
            }
            data = s.join('');
        } else if(_.isArray(any)) {
            data = [];
            _.each(any, function (value) {
                data.push(tw2cn(value));
            });
        } else if(!isPrimitive(any)) {
            data = {};
            _.each(any, function (value, key) {
                data[key] = _.contains(options.exclude, key) ? value : tw2cn(value);
            });
        } else {
            data = any;
        }
        return data;
    }

    /**
     * convert the words from Simplified Chinese to Traditional Chinese.
     *
     * @memberof module:i18n
     * @param {String|Object|Array} any - anything to convert.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Object} [options.exclude] - the key to exclude from the conversion.
     * @returns {String|Object}
     */
    function cn2tw(any, options) {
        options || (options = {});

        var defaults = {
            exclude: []
        };
        var data;
        _.defaults(options, defaults);
        _.isString(options.exclude) && (options.exclude = [options.exclude]);

        if(_.isString(any)) {
            var s = [];
            for(var i=0; i<any.length; i++) {
                var c_cn = any.charAt(i);
                var c_tw = CN2TWMAP[c_cn];
                s.push(c_tw ? c_tw : c_cn);
            }
            data = s.join('');
        } else if(_.isArray(any)) {
            data = [];
            _.each(any, function (value) {
                data.push(cn2tw(value));
            })
        } else if(!isPrimitive(any)) {
            data = {};
            _.each(any, function (value, key) {
                data[key] = _.contains(options.exclude, key) ? value : cn2tw(value);
            });
        } else {
            data = any;
        }
        return data;
    }

    function isPrimitive(any) {
        if(!_.isObject(any)) return true;
        if(_.isDate(any)) return true;
        if(_.isNumber(any)) return true;
        if(_.isString(any)) return true;
        if(_.isBoolean(any)) return true;
        if(any instanceof File) return true;

        return false;
    }

    /**
     * Get or set the locale of i18n.
     *
     * @memberof module:i18n
     * @param {String} [locale] - the IETF locale.
     * @returns {String}
     */
    function locale(locale) {
        var lang = i18next.lng();
        if(!locale) return lang;

        i18next.setLng(locale);
    }

    function defaultLocale() {
        return _defaultLocale;
    }

    /**
     * Get the datetime representation format.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function datetimePattern(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: DEFAULTPATTERN['datetime']
        };

        var text = find('datetimePattern', options);

        return (text==="") ? null : text;

    }

    /**
     * Get the simple date representation format.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function datePattern(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: DEFAULTPATTERN['date']
        };

        var text = find('datePattern', options);

        return (text==="") ? null : text;
    }

    /**
     * Get the time representation format.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function timePattern(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: DEFAULTPATTERN['time']
        };

        var text = find('timePattern', options);

        return (text==="") ? null : text;
    }

    /**
     * Get the month representation format.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function monthPattern(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: DEFAULTPATTERN['month']
        };

        var text = find('monthPattern', options);

        return (text==="") ? null : text;
    }

    /**
     * Get the number representation format.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function numberPattern(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: DEFAULTPATTERN['number']
        };

        var text = find('numberPattern', options);

        return (text==="") ? null : text;
    }

    /**
     * Get the currency representation format.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function currencyPattern(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: DEFAULTPATTERN['currency']
        };

        var text = find('currencyPattern', options);

        return (text==="") ? null : text;

    }

    /**
     * Get the decimal separator.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function decimalSeparator(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: "."
        };

        var text = find('decimalSeparator', options);

        return (text==="") ? null : text;
    }

    /**
     * Get the grouping separator.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function groupingSeparator(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: ","
        };


        var text = find('groupingSeparator', options);

        return (text==="") ? null : text;

    }

    /**
     * Get the currency sign.
     *
     * @memberof module:i18n
     * @param {String} [locale] - The locale to use for internalize.
     * @returns {String}
     */
    function currencySign(locale) {
        var options = {
            namespace: 'app',
            locale: locale,
            defaultValue: "$"
        };

        var text = find('currencySign', options);

        return (text==="") ? null : text;
    }

    function isLocale(locale) {
        var isTrue = false;

        if(locale.indexOf(2)==='-') {
            isTrue = true;
        } else {
            var language = locale.substr(0, 2);
            if(language==='en' || language==='zh') {
                isTrue = true;
            }
        }
        return isTrue;
    }

    return {
        initialize: initialize,
        translate: translate,
        message: message,
        locale: locale,
        defaultLocale: defaultLocale,
        isLocale: isLocale,
        tw2cn: tw2cn,
        cn2tw: cn2tw,
        datePattern: datePattern,
        datetimePattern: datetimePattern,
        monthPattern: monthPattern,
        timePattern: timePattern,
        currencyPattern: currencyPattern,
        numberPattern: numberPattern,
        decimalSeparator: decimalSeparator,
        groupingSeparator: groupingSeparator,
        currencySign: currencySign
    };
});
