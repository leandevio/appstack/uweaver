/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * This module provides a collection of utility methods for performing asynchronous HTTP(Ajax) requests.
 *
 * @module net
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore', 'jquery', 'uweaver/Deferred', 'uweaver/datetime',
        'uweaver/Logger'], function(_, $, Deferred, datetime, Logger){

    var LOGGER = new Logger("uweaver/net");
    var properties;
    var authorization, authUrl;
    var handlers = {};
    var filters = {};

    function initialize() {
        resetHandler();
        helo().then(function(data) {
            properties = data;
        });
    }

    function helo() {
        return send(".", {
            async: false,
            responseType: 'json',
            headers: {
                "Action": "HELO"
            }
        });
    }

    /**
     * Perform a asynchronous HTTP request.
     *
     * @memberof module:net
     * @param {String} path - A string containing the URL to which the request is sent.
     * @param {Object} [options] - A map of additional options to pass to the method. All jQuery.ajax options can be passed directly as request options.
     * @param {String} [options.headers={}] - An object of additional header key/value pairs to send along with requests using the XMLHttpRequest transport.
     * @param {String} [options.contentType=application/json; charset=UTF-8] - An object of additional header key/value pairs to send along with requests using the XMLHttpRequest transport.
     * @param {String} [options.dataType=json] - The type of data that you're expecting back from the server. If you need synchronous requests, set this option to false
     * @param {Boolean} [options.async=true] - By default, all requests are sent asynchronously.
     * @returns {Deferred} the response
     */
    function request(path, options) {
        options || (options = {});

        var defaults = {
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            headers: {}
        };
        var settings = _.extend(defaults, options);

        var url = makeUrl(path, options);

        _.each(filters[Event.REQUEST], function(fn) {
            settings.data = fn(settings.data, url, options);
        });

        _.isObject(settings.data) && (settings.data = parseRequest(settings.data, options));

        var caller = arguments.callee.caller.name;
        LOGGER.debug("Request sent by ${0} with ${1}", caller, {url: url, options: _.omit(options, 'context')});

        var deferred = new Deferred();
        var promise = deferred.promise();

        authorization && (settings.headers.Authorization = authorization);

        if(options.username) {
            var params = options.username + ":" + (options.password ? options.password : "");
            var credentials = "Basic " + btoa(params);
            settings.headers.Authorization = credentials;
            authUrl = url;
        }

        (settings.headers.Authorization==="Basic" && !url) && (url = authUrl);

        if(settings.data instanceof FormData) {
            settings.contentType = false;
            settings.processData = false;
            settings.cache = false;
        }

        settings.dataFilter = function(response, type) {
            var data = response;
            _.each(filters[Event.RESPONSE], function(fn) {
                data = fn(data);
            });
            return data;
        };

        var jqXHR = $.ajax(url,_.omit(settings, "success", "error", "username", "password"));
        jqXHR.then(function(response, status, xhr) {
            LOGGER.debug("Response received by ${0} with ${1}", caller,
                {url: url, options: _.omit(options, 'context'), payload: response, status: xhr.status});

            xhr.getResponseHeader('Authorization') && (authorization = xhr.getResponseHeader('Authorization'));

            _.each(handlers[Event.DONE], function(fn) {
                fn(response, status, xhr);
            });
            _.defaults(promise, xhr);
            deferred.resolve(response, status, xhr);
            _.isFunction(settings.success) && settings.success.apply((settings.context||this), arguments);
        }, function(xhr, status, error) {
            var params = {
                url: url,
                settings: settings
            };
            var message = xhr.statusText + "(" + xhr.status + "): " + xhr.responseText;
            LOGGER.error("Error received by ${0} with ${1}", caller, {params: params, message: message});

            if(xhr.status===401||xhr.status===440) {
                authorization = undefined;
                authUrl = undefined;
            }

            _.each(handlers[Event.FAIL], function(fn) {
                fn(xhr, status, error);
            });

            _.defaults(promise, xhr);
            deferred.reject(xhr, status, error);
            _.isFunction(settings.error) && settings.error.apply((settings.context||this), arguments);
        });

        return promise;
    }

    function download(path, options) {
        options || (options = {});

        var params = authorization ? "Authorization=" + encodeURIComponent(authorization) + "&" : "&";
        _.isObject(options.data) && _.each(options.data, function(value, key) {
            if(value===undefined||value===null) return;

            var s = (value instanceof Date) ? datetime.format(value) : value;
            params =  params + key + "=" + encodeURIComponent(s) + "&";

        });
        params = params.substring(0, params.length-1);

        var url = makeUrl(path) + "?" + params;
        var win = window.open(url+"", '_blank');
        win.focus();
    }

    function send(path, options) {
        options || (options = {});
        var defaults = {
            method: 'GET',
            contentType: 'application/json; charset=UTF-8',
            responseType: "json",
            async: true,
            headers: {}
        };
        _.defaults(options, defaults);

        authorization && (options.headers.Authorization = authorization);

        var xhr = new XMLHttpRequest();

        var url = makeUrl(path, options);
        xhr.open(options.method, url, options.async, options.username, options.password);

        _.each(options.headers, function(value, key) {
            xhr.setRequestHeader(key, value);
        });


        xhr.setRequestHeader('Content-Type', options.contentType);

        if(options.async) {
            xhr.responseType = options.responseType;
        } else {
            options.accept && xhr.setRequestHeader('Accept', options.accept);
        }
        var deferred = new Deferred();
        xhr.onreadystatechange = function() {
            if (xhr.readyState==4) {
                if(xhr.status==200) {
                    xhr.getResponseHeader('Authorization') && (authorization = xhr.getResponseHeader('Authorization'));
                    var data = parseResponse(xhr.response, options.responseType);
                    var status = "success";
                    deferred.resolve(data, status, xhr);
                } else {
                    var status = "error";
                    var error = xhr.statusText;
                    deferred.reject(xhr, status, error);
                }
            }
        };
        var data;

        _.isObject(options.data) && (data = JSON.stringify(options.data));

        xhr.send(data);
        return deferred.promise();
    }

    function makeUrl(path, options) {
        options || (options = {});

        var url = path;

        if(options.headers && options.headers['Action']) {
            return url;
        }

        if(/^(http|\/){1}/.test(url)) return url;

        // if(url.indexOf('_/')===0) {
        //     url = webAppContext.systemContext + '/' + url.substring(1);
        // } else if(url.indexOf('_')===0) {
        //     url = webAppContext.uwContext + '/' + url.substring(1);
        // } else {
        //     url = webAppContext.serviceContext + '/' + url;
        // }

        var contextPath = properties.servlet.contextPath;
        properties.restlet.contextPath && (contextPath = contextPath + '/' + properties.restlet.contextPath);

        return contextPath + '/' + url;
    }

    function parseResponse(response, responseType) {
        var data;

        // The XMLHttpRequest in some browsers does not support responseType (or all responseTypes).
        if(responseType==='json' && !_.isObject(response)) {
            data = JSON.parse(response);
        } else {
            data = response;
        }

        return data;
    }

    function addHandler(event, fn) {
        if(!_.has(handlers, event)) return;

        if(_.contains(handlers[event], fn)) return;

        handlers[event].push(fn) ;
    }
    
    function removeHandler(event, fn) {
        if(!_.has(handlers, event)) return;

        handlers[event] = _.without(handlers[event], fn);
    }

    function removeAllHandler() {
        resetHandler();
    }

    function resetHandler() {
        handlers[Event.DONE] = [];
        handlers[Event.FAIL] = [];
        handlers[Event.PROGRESS] = [];
    }

    function addFilter(event, fn) {
        if(!_.has(filters, event)) {
            filters[event] = [];
        } else if(_.contains(filters[event], fn)) {
            return;
        }

        filters[event].push(fn) ;
    }

    function removeFilter(event, fn) {
        if(!_.has(filters, event)) return;

        filters[event] = _.without(filters[event], fn);
    }

    function removeAllFilters() {
        filters = {};
    }

    function parseRequest(data, options) {
        var requestData = data;

        if(options.method==='POST') {
            var formData = new FormData();
            var files = parseFiles(requestData);
            _.each(files, function(file) {
                formData.append('attachments', file);
            });
            formData.append('payload', JSON.stringify(requestData));
            _.each(files, function(file) {
                delete file.toJSON;
            });

            requestData = formData;
        }

        return requestData;
    }

    function parseFiles(any) {
        var files = [];

        if(!isPrimitive(any)) {
            _.each(any, function(value) {
                if(value instanceof File) {
                    files.push(value);
                    value.toJSON = fileToJSON;
                } else if(!isPrimitive(value)) {
                    files = files.concat(parseFiles(value));
                }
            });
        }

        return files;
    }

    function isPrimitive(any) {
        if(!_.isObject(any)) return true;
        if(_.isDate(any)) return true;
        if(_.isNumber(any)) return true;
        if(_.isString(any)) return true;
        if(_.isBoolean(any)) return true;

        return false;
    }

    function fileToJSON() {
        var json = {};

        _.each(this, function(value, key) {
            json[key] = value;
        });

        json.name = this.name;
        json.type = this.type;
        json.size = this.size;

        return json;
    }

    var Event = {
        DONE: 'DONE',
        FAIL: 'FAIL',
        PROGRESS: 'PROGRESS',
        REQUEST: 'REQUEST',
        RESPONSE: 'RESPONSE'
    };

    var net = {
        request: request,
        download: download,
        send: send,
        addHandler: addHandler,
        removeHandler: removeHandler,
        removeAllHandler: removeAllHandler,
        addFilter: addFilter,
        removeFilter: removeFilter,
        removeAllFilters: removeAllFilters,
        makeUrl: makeUrl,
        Event: Event
    };

    initialize();

    return net;
});