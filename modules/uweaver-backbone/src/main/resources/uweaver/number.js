/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * The number module provides a collection of utility methods for manipulating numbers and allows you to choosing
 * user-defined patterns for number formatting.
 *
 * Number formats are specified by number pattern strings. The pattern strings are designed to
 * conform to java.text.DecimalFormat.
 *
 * ##### Usage
 *    expect(number.format(1234)).to.equal("1234");
 *
 *    expect(number.format(1234.567, "##,##0.##")).to.equal("1,234.57");
 *    expect(number.format(-1234.567, "##,##0.##")).to.equal("-1,234.57");
 *
 *    expect(number.format(1234.5, "##,##0.00")).to.equal("1,234.50");
 *
 *    expect(number.format(1234.5, "$##,##0.00")).to.equal("$1,234.50");
 *
 *    expect(number.format(0.12345, "#0.##%")).to.equal("12.35%");
 *
 *    expect(number.parse("1234")).to.equal(1234);
 *
 *    expect(number.parse("1,234.56", "##,##0.##")).to.equal(1234.56);
 *    expect(number.parse("-1234.56", "##,##0.##")).to.equal(-1234.56);
 *
 *    expect(number.parse(".1", "##,###.##")).to.equal(0.1);
 *
 *    expect(number.parse("$1,234.50", "$##,##0.00")).to.equal(1234.5);
 *
 * @module number
 * @author  Jason Lin
 * @since   1.0
 */
define(['underscore', 'uweaver/string', 'uweaver/exception/InvalidFormatException',
    'uweaver/exception/UnsupportedTypeException'], function(_, string, InvalidFormatException, UnsupportedTypeException) {

    var DECIMALSEPARATOR = ".", GROUPINGSEPARATOR = ",", CURRENCYSIGN = "$";

    /**
     * The method parses a string representation of a number using a specified pattern and returns the number.
     *
     * ##### Usage
     *    expect(number.parse("1234")).to.equal(1234);
     *
     *    expect(number.parse("1,234", "##,##0.##")).to.equal(1234);
     *    expect(number.parse("1,234.5", "##,##0.##")).to.equal(1234.5);
     *    expect(number.parse("1,234.56", "##,##0.##")).to.equal(1234.56);
     *    expect(number.parse("-1234.56", "##,##0.##")).to.equal(-1234.56);
     *
     *    expect(number.parse("", "##,###.##")).to.equal(0);
     *    expect(number.parse(".1", "##,###.##")).to.equal(0.1);
     *
     *    expect(number.parse(".10", "##,###.00")).to.equal(0.1);
     *
     *    expect(number.parse("$1,234.50", "$##,##0.00")).to.equal(1234.5);
     *
     *    expect(number.parse("($1,234.50)", "$##,##0.00;($##,##0.00)")).to.equal(-1234.5);
     *
     * @memberof module:number
     * @param {String} text - the text to convert.
     * @param {String} [pattern] - specify a number pattern to parse the text.
     * @param {Object} [options] - specify a set of symbols (such as the decimal separator, the grouping separator, and so on).
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {Number}
     */
    function parse(text, pattern, options) {
        if(text===null||text===undefined) {
            return null;
        } else if(!_.isString(text)) {
            throw new UnsupportedTypeException({
                type: typeof text,
                supportedTypes: ['String']
            });
        } else if(text.length===0) {
            return null;
        }


        var compliant = test(text, pattern, options);
        if(!compliant) {
            throw new InvalidFormatException({
                entity: text,
                format: pattern
            });
        }

        options || (options = {});

        var decimalSeparator = options.decimalSeparator || DECIMALSEPARATOR,
            groupingSeparator = options.groupingSeparator || GROUPINGSEPARATOR;

        var n = new Number(text.replace(groupingSeparator, '').replace(decimalSeparator, '.').replace(/[^0-9\.]/g, ''));

        if(compliant===2) n = n * -1;

        return n.valueOf();

    }


    /**
     * The method formats a number to a string representation using the specified pattern.
     *
     * ##### Usage
     *    expect(number.format(1234)).to.equal("1234");
     *
     *    expect(number.format(0, "##,##0.##")).to.equal("0");
     *    expect(number.format(1234, "##,##0.##")).to.equal("1,234");
     *    expect(number.format(1234.56, "##,##0.##")).to.equal("1,234.56");
     *    expect(number.format(1234.567, "##,##0.##")).to.equal("1,234.57");
     *    expect(number.format(-1234.567, "##,##0.##")).to.equal("-1,234.57");
     *
     *    expect(number.format(0, "##,##0.00")).to.equal("0.00");
     *    expect(number.format(1234.5, "##,##0.00")).to.equal("1,234.50");
     *    expect(number.format(0.1, "##,###.00")).to.equal(".10");
     *
     *    expect(number.format(0, "##,###.##")).to.equal("");
     *    expect(number.format(0.1, "##,###.##")).to.equal(".1");
     *
     *    expect(number.format(1234.5, "$##,##0.00")).to.equal("$1,234.50");
     *
     *    expect(number.format(-1,234.5, "$##,##0.00;($##,##0.00)")).to.equal("($1,234.50)");
     *
     * @memberof module:number
     * @param {Number} value - the number to format.
     * @param {String} [pattern] - specify a date pattern to format the value.
     * @param {Object} [options] - specify a set of symbols (such as the decimal separator, the grouping separator, and so on).
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {String}
     */
    function format(value, pattern, options) {
        if(value===null||value===undefined) {
            return null;
        } else if(!_.isNumber(value)) {
            throw new UnsupportedTypeException({
                type: typeof text,
                supportedTypes: ["Number"]
            });
        }

        if(!pattern) return value.toString();

        options || (options = {});

        var decimalSeparator = options.decimalSeparator || DECIMALSEPARATOR,
            groupingSeparator = options.groupingSeparator || GROUPINGSEPARATOR;

        var patterns = translate(pattern, decimalSeparator, groupingSeparator).split(';');
        var positive = patterns[0];
        var negative = (patterns.length>1) ? patterns[1] : '-' + positive;

        hasPercentage(positive) && (value = value * 100);

        var scale = calScale(value, positive);
        var groupingSize = calGroupingSize(positive);

        var result = Math.abs(value).toFixed(scale);

        if(value>-1 && value<1 && parseZero(positive)==="#") {
            result = result.substring(1);
        }

        if(groupingSize > 0) {
            var expr = '(\\d)(?=(\\d{' + groupingSize + '})+(\\.|$))';
            var regExp = new RegExp(expr, 'g');
            result = result.replace(regExp, '$1,');
        }

        var template = (value >= 0) ? positive : negative;

        var patternRegExp = new RegExp(genPatternExpr(groupingSize, scale));

        result = template.replace(patternRegExp, result);

        return result.replace(/[,\.]/, function(match) {
            if(match==='.') {
                return decimalSeparator;
            } else if(match===',') {
                return groupingSeparator;
            }
        });
    }

    function hasPercentage(pattern) {
        return /(^%|%$)/.test(pattern);
    }
    /**
     *
     * The method calculates the grouping size.
     *
     * Extract the string of # & 0 between the decimal separator & the grouping separator. The size of the string is the
     * grouping size.
     *
     * @private
     * @param {String} pattern
     * @returns {number}
     */

    function calGroupingSize(pattern) {
        var matches = /,[0#]+(\.|$)/.exec(pattern);

        if(!matches) return 0;

        return _.first(matches).replace(/[,.]/g, '').length;
    }

    /**
     *
     * The method calculates the scale.
     *
     * @private
     * @param {number|String} value
     * @param {String} pattern
     * @returns {number}
     */

    function calScale(value, pattern) {
        var text = value.toString();

        // calculate the scale specified by pattern.
        var matches = /\.[0#]+/.exec(pattern);
        var pScale = (matches) ? _.first(matches).length-1 : 0;

        // calculate the scale of the value.
        matches = /\.\d+/.exec(text);
        var vScale = (matches) ? _.first(matches).length-1 : 0;

        if(pScale <= vScale) return pScale;

        // substitute with zero.
        matches = /\.[0]+/.exec(pattern);
        pScale = (matches) ? _.first(matches).length-1 : 0;

        return Math.max(vScale, pScale);
    }

    /**
     *
     */
    function parseZero(pattern) {
        var matches = /[0#]{1}(\.|$|%)/.exec(pattern);

        return (matches) ? _.first(matches).charAt(0) : '#';


    }

    /**
     *
     * The method converts a value to a number using the specified pattern.
     *
     * ##### Usage
     *    expect(number.convert(1234)).to.equal(1234);
     *    expect(number.convert("1234")).to.equal(1234);
     *    expect(number.convert("1,234", "##,###")).to.equal(1234);
     *    expect(number.convert("$1,234.50", "$##,##0.00")).to.equal(1234.5);
     *    expect(number.convert("($1,234.50)", "$##,##0.00;($##,##0.00)")).to.equal(-1234.5);
     *
     * @memberof module:number
     * @param {String|Number} value - the value to convert.
     * @param {String} [pattern] - specify a number pattern to parse the text.
     * @param {Object} [options] - specify a set of symbols (such as the decimal separator, the grouping separator, and so on).
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {Number}
     */
    function convert(value, pattern, options) {
        if(value===undefined||value===null) return value;
        if(_.isNumber(value)) {
            return value;
        } else if(_.isString(value)) {
            if(value.length===0) return null;
            return parse(value, pattern, options);
        } else {
            throw new UnsupportedTypeException({
                type: typeof value,
                supportedTypes: ["String", "Number"]
            });
        }
    }

    /**
     *
     * The method tests if a string representation is a number using the specified pattern.
     *
     * @memberof module:number
     * @param {String} text - the text to check.
     * @param {String} [pattern] - specify a number pattern to test the text.
     * @param {Object} [options] - specify a set of symbols (such as the decimal separator, the grouping separator, and so on).
     * @param {String} [options.decimalSeparator="."] - decimal separator.
     * @param {String} [options.groupingSeparator=","] - grouping separator.
     * @returns {Integer} - 0 => not a number, 1 => positive number, 2 => negative number.
     */
    function test(text, pattern, options) {
        if(!_.isString(text)) {
            throw new UnsupportedTypeException({
                type: typeof text,
                supportedTypes: ['String']
            });
        }

        if(!pattern) {
            if(!/[^0-9\.]/.test(text)) {
                return 1;
            } else if(!/[^0-9\.\-]/.test(text)) {
                return 2;
            } else {
                return 0;
            }
        }

        options || (options = {});

        var decimalSeparator = options.decimalSeparator || DECIMALSEPARATOR,
            groupingSeparator = options.groupingSeparator || GROUPINGSEPARATOR;

        var patterns = translate(pattern, decimalSeparator, groupingSeparator).split(';');
        var positive = patterns[0];
        var negative = (patterns.length>1) ? patterns[1] : '-' + positive;

        var scale = calScale(text, positive);
        var zero = parseZero(positive);
        var groupingSize = calGroupingSize(positive);

        var numberExpr = genNumberExpr(groupingSize, scale, zero);
        var patternRegExp = new RegExp(genPatternExpr(groupingSize, scale));

        var regExp = new RegExp(escapeSpecialChar(positive).replace(patternRegExp, numberExpr));
        var matches = regExp.exec(text);
        if(matches && matches[0]==text) return 1;

        regExp = new RegExp(escapeSpecialChar(negative).replace(patternRegExp, numberExpr));
        var matches = regExp.exec(text);
        if(matches && matches[0]==text) return 2;

        return 0;
    }

    /**
     *
     * translate the localized pattern to the standard pattern.
     *
     * @private
     * @param pattern
     */
    function translate(pattern, decimalSeparator, groupingSeparator) {
        var regExp = new RegExp("[\\" + decimalSeparator + "\\" + groupingSeparator + "]", 'g');
        return pattern.replace(regExp, function(match) {
            if(match===decimalSeparator) {
                return '.';
            } else if(match===groupingSeparator) {
                return ',';
            }
        });
    }

    function escapeSpecialChar(text) {
        return text.replace(/[\(\$\)]/g, "\\$&");
    }

    function genNumberExpr(groupingSize, scale, zero) {
        var expr;

        var minWholdDigits = (zero==='0') ? 1 : 0;

        if(groupingSize>0) {
            expr = '([0-9]{' + minWholdDigits + ',' + (groupingSize - 1) + '})+(,[0-9]{3})*';
        } else {
            expr = '[0-9]{' + minWholdDigits + ',}';
        }

        if(scale>0) {
            expr = expr + '\\.([0-9]{' + scale + '})';
        }

        return expr;
    }

    function genPatternExpr(groupingSize, scale) {
        var expr;

        if(groupingSize>0) {
            expr = '[#0,]*,[#0]{' + groupingSize + '}';
        } else {
            expr = '[#0]*';
        }

        expr = expr + '(\\.[#0]*)?';

        return expr;
    }

    return {
        parse: parse,
        format: format,
        convert: convert,
        test: test
    };
});