/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang'], function(_, $, d3, lang) {

    var declare = lang.declare;
    var Base = null;

    function initialize() {
        this._scale = d3.scale.ordinal();
        this.range([0, 1]);
    }

    function domain(domain) {
        var scale = this._scale;
        if(!domain) return scale.domain();

        scale.domain(domain);
    }

    function range(range) {
        if(!range) return this._range;

        var scale = this._scale;
        scale.rangePoints(range, this.padding());
        this._range = range;
    }
    
    function padding(padding) {
        if(!padding) return this._padding;

        var scale = this._scale;
        scale.rangePoints(this.range(), padding);
        this._padding = padding;
    }

    function project(value) {
        return this._scale(value);
    }

    function invert(value) {
        return this._scale.invert(value);
    }


    var props = {
        _scale: undefined,
        _range: undefined,
        _padding: 0
    };

    var OrdinalScale = declare(Base, {
        initialize: initialize,
        domain: domain,
        range: range,
        padding: padding,
        project: project,
        invert: invert
    }, props);


    return OrdinalScale;

});