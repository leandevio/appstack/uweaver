/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'jquery', 'd3', 'uweaver/lang'], function(_, $, d3, lang) {

    var declare = lang.declare;
    var Base = null;


    function initialize() {
        this._scale = d3.time.scale();
    }

    function domain(domain) {
        var scale = this._scale;
        if(!domain) return scale.domain();

        scale.domain(domain);
    }

    function range(range) {
        var scale = this._scale;
        if(!range) return scale.range();

        scale.range(range);
    }

    function project(value) {
        return this._scale(value);
    }

    function invert(value) {
        return this._scale.invert(value);
    }
    
    
    var props = {
        _scale: undefined
    };

    var TimeScale = declare(Base, {
        initialize: initialize,
        domain: domain,
        range: range,
        project: project,
        invert: invert
    }, props);

    return TimeScale;

});