/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/BaseObject',
    'uweaver/data/Applets'], function(BaseObject, Applets) {
    var Base = BaseObject;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._applets = new Applets();

        this.listenTo(this._applets, 'all', onAppletChange);
    }

    function onAppletChange(event) {
        this.trigger('change change:applet', {
            data: undefined,
            source: this._applets,
            context: this
        })
    }

    function applets() {
        (this._applets.size()===0) && this._applets.fetch();
        return this._applets;
    }

    function applet(uri) {
        return this._applets.get(uri);
    }

    var props = {
        _applets: undefined
    };

    var AppManager = declare(Base, {
        initialize: initialize,
        applets: applets,
        applet: applet
    }, props);

    function getDefaultInstance() {
        return new AppManager();
    }

    return {
        getDefaultInstance: getDefaultInstance
    };

});