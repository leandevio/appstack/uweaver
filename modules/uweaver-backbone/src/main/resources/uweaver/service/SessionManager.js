/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/BaseObject', 'uweaver/Deferred',
    'uweaver/data/Session'], function(BaseObject, Deferred, Session) {
    var Base = BaseObject;
    var deferred = new Deferred();

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._session = new Session();

        this.listenTo(this._site, 'all', onSessionChange);

        this._session.fetch().then(function() {
            deferred.resolve(this._site);
        }, this);
    }

    function ready() {
        return deferred.promise(this._session);
    }

    function connect() {
        return this._session.fetch();
    }


    function login(credential) {
        return this._session.fetch(credential);
    }

    function logout() {
        var options = {
            headers : {Authorization : "Basic"},
            async: false
        };
        return this._session.fetch(options);
    }

    function onSessionChange(event) {
        this.trigger('change change:session', {
            data: undefined,
            source: this._site,
            context: this
        })
    }

    function session() {
        return this._session;
    }

    function setLocale(locale) {
        return this._session.setLocale(locale);
    }

    var props = {
        _session: undefined
    };

    var SessionManager = declare(Base, {
        initialize: initialize,
        session: session,
        ready: ready,
        connect: connect,
        login: login,
        logout: logout,
        setLocale: setLocale
    }, props);

    var INSTANCE = new SessionManager();

    function getDefaultInstance() {
        return INSTANCE;
    }

    return {
        getDefaultInstance: getDefaultInstance
    };
});