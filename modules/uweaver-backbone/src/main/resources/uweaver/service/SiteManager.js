/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/BaseObject', 'uweaver/Deferred',
    'uweaver/data/Site'], function(BaseObject, Deferred, Site) {
    var Base = BaseObject;
    var deferred = new Deferred();

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._site = new Site();

        this.listenTo(this._site, 'all', onSiteChange);

        this._site.fetch().then(function() {
            deferred.resolve(this._site);
        }, this);
    }

    function ready() {
        return deferred.promise(this._site);
    }

    function onSiteChange(event) {
        this.trigger('change change:site', {
            data: undefined,
            source: this._site,
            context: this
        })
    }

    function site() {
        return this._site;
    }

    var props = {
        _site: undefined
    };

    var SiteManager = declare(Base, {
        initialize: initialize,
        site: site,
        ready: ready
    }, props);

    function getDefaultInstance() {
        return new SiteManager();
    }

    return {
        getDefaultInstance: getDefaultInstance
    };
});