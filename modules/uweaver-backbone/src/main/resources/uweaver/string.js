/**
 * Created by jasonlin on 6/17/14.
 */
define(['underscore'], function(_) {
    function format(/*String*/ template, /*Object||Array*/ values) {
        if(!template || !template instanceof String) return template;

        if(!(_.isObject(values)|| _.isArray(values))) {
            var pattern = new RegExp("\\$\\{0\\}","g");
            template = template.replace(pattern, values);
            return template;
        }

        _.each(values, function(value, key) {
           var pattern = new RegExp("\\$\\{" + key + "\\}","g");
           var args =  (_.isObject(value)) ? JSON.stringify(value) : value;
            template = template.replace(pattern, args);
        });

        return template;
    }

    function startsWith(str, searchvalue) {
        if(_.isUndefined(str)||_.isNull(str)) return false;
        if(_.isUndefined(searchvalue)||_.isNull(searchvalue)) return false;
        if(searchvalue.length>str.length) return false;

        return (str.substr(0, searchvalue.length)===searchvalue);
    }

    function endsWith(str, searchvalue) {
        if(_.isUndefined(str)||_.isNull(str)) return false;
        if(_.isUndefined(searchvalue)||_.isNull(searchvalue)) return false;
        if(searchvalue.length>str.length) return false;

        return (str.substr(str.length-searchvalue.length, searchvalue.length)===searchvalue);
    }

    return {
        format: format,
        substitute: format,
        startsWith: startsWith,
        endsWith: endsWith
    };
});