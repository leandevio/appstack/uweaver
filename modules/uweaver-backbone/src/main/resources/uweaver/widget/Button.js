/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/i18n', 'uweaver/Deferred',
    'uweaver/widget/Widget', 'uweaver/widget/Icon', 'uweaver/widget/Popup',
    'uweaver/identifier', 'uweaver/calibrator',
    'text!./tpl/Button.html'], function ($, _, lang, i18n, Deferred, Widget, Icon, Popup, identifier, calibrator, tpl){
    var declare = lang.declare;
    var Base = Widget;

    /**
     * **Button**
     * This class represents a button.
     *
     * **Configs:**
     * - tpl(String): A html string to layout & construct the DOM.
     *
     * @constructor Button
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the button.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};

        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        var $button = this.$el;

        $button.on('click', _.bind(onButtonClick, this));

        this._$component = $button;

        this.title() && this.title(this.title());

        this.el.children.length===0 && (this.text() && this.text(this.text()));

        cfg.text && this.text(cfg.text);
        cfg.value && this.value(cfg.value);
        cfg.name && this.name(cfg.name);
        cfg.title && this.title(cfg.title);

        this.render();
    }

    /**
     * @override
     */
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        this.hide();

        enhance.call(this);

        this.show();

        return this;
    }

    function enhance() {
        var spans = this.el.querySelectorAll("span");
        _.each(spans, function(span) {
            span.innerText = i18n.translate(span.innerText);
        });
        var anchors = this.el.querySelectorAll("a");
        _.each(anchors, function(anchor) {
            anchor.innerText = i18n.translate(anchor.innerText);
        });
    }

    function value(value) {
        var $button = this._$component;

        if(arguments.length===0) return $button.prop('value') || $button.attr('value');

        $button.prop('value', value);
    }
    

    function name(text) {
        var $button = this._$component;
        if(arguments.length===0) return $button.attr('name');
        $button.attr('name', text);
    }

    function text(text) {
        var $button = this._$component;
        if(arguments.length===0) return $button.text();
        $button.text(i18n.translate(text));
    }

    function title(text) {
        var $button = this._$component;
        if(arguments.length===0) return $button.attr('title');
        $button.attr('title', i18n.translate(text));
    }

    function onButtonClick(event) {
        (this.el.type!='reset') && event.preventDefault();
        this.click();
    }

    function click() {
        if(!this.value()) return;

        var event = {
            context: this,
            source: this,
            data: this.value()
        };

        confirm.call(this).then(function() {
            this.trigger("click", event);
        }, this);
    }

    function disabled(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.prop('disabled');
        $input.prop('disabled', value);
    }

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value.toUpperCase();

        this.disabledState() && this.disabled(_.contains(this.disabledState().toUpperCase().split(","), this._state));
        this.hiddenState() && _.contains(this.hiddenState().toUpperCase().split(","), this._state) ? this.hide() : this.show();
    }
    

    function disabledState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-disabled-state');
        $input.attr('data-disabled-state', value);
    }

    function hiddenState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-hidden-state');
        $input.attr('data-hidden-state', value);
    }

    function confirm() {
        var deferred = new Deferred();
        if(this.el.hasAttribute('confirmation')) {
            var messages = this.el.getAttribute('confirmation').split(':').reverse();
            Popup.confirm(messages[0], messages[1]).then(function() {
                deferred.resolve();
            }, function(action) {
                deferred.reject(action);
            });
        } else {
            deferred.resolve();
        }

        return deferred.promise();
    }

    var props = {
        /**
         * @override
         */
        tagName: 'button'
    };

    var Button = declare(Base, {
        initialize: initialize,
        render: render,
        click: click,
        name: name,
        value: value,
        text: text,
        title: title,
        disabled: disabled,
        state: state,
        disabledState: disabledState,
        hiddenState: hiddenState
    }, props);

    return Button;


});