/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Inputbox'], function ($, _, lang, Inputbox){
    var declare = lang.declare;
    var Base = Inputbox;
    var SUPPORTEDTYPES = ['checkbox'];
    var SUPPORTEDDATATYPES = [];

    /**
     * A Checkbox is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * var checkbox = new Checkbox({
     *     el: $('[name=availability]')
     * });
     * checkbox.render();
     *
     * checkbox.value(true);
     *
     * expect(checkbox.value()).to.equal(true);
     * ```
     * ##### HTML
     * + input[type=checkbox]: required.
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <div class="checkbox">
     *      <label>
     *          <input type="checkbox" name="availability"> In Stock
     *      </label>
     *  </div>
     * ...
     * </form>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Checkbox#value}

     *
     * @constructor Checkbox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value=true] - Specifies the value of an <input> element.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
    }


    /**
     * Get or set the value.
     *
     * @param {Boolean|String} value
     * @returns {*|undefined}
     */
    function value(value, options) {
        options || (options = {});

        var $input = this.$el;
        var checked = $input.prop('checked');
        
        var currentValue = this.parse(checked);

        if(arguments.length===0) return currentValue;
        
        (value===undefined) && (value=null);

        if(this.format(value)===checked) return;

        $input.prop('checked', this.format(value));
        
        this.validate() && (options.silent || this.change());
    }

    function clear() {
        var $input = this.$el;
        $input.prop('checked', false);
        this.clearValidation();
    }

    function format(value) {
        var $input = this.$el;
        var checked;

        if(_.isBoolean(value)) {
            checked = value;
        } else if(_.isString(value) && value.indexOf($input.attr('value'))!==-1) {
            checked = true;
        } else {
            checked = false;
        }
        
        return checked;
    }
    
    /**
     * Parse the text of the inputbox to a value.
     *
     */
    function parse(checked) {
        var $input = this.$el;
        var text = $input.attr('value');
        var value;

        if(text.length==0) return null;

        if(text) {
            value = checked ? text : null;
        } else {
            value = checked;
        }

        return value;
    }
    

    var props = {
        SUPPORTEDTYPES: SUPPORTEDTYPES,
        SUPPORTEDDATATYPES: SUPPORTEDDATATYPES
    };

    var Checkbox = declare(Base, {
        initialize: initialize,
        value: value,
        clear: clear,
        format: format,
        parse: parse
    }, props);

    return Checkbox;


});