/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(["underscore", "jquery", "uweaver/lang", 'uweaver/dom', "uweaver/widget/Widget", 'uweaver/data/Collection',
    'uweaver/data/Model', 'uweaver/data/reference', 'uweaver/data/code', 'uweaver/i18n',
    'uweaver/widget/Popup', 'uweaver/widget/Tooltips', 'uweaver/exception/Violation',
    'text!./tpl/combobox/Suggestion.html'], function(_, $, lang, dom, Widget, Collection, Model, reference, code, i18n,
                                                     Popup, Tooltips, Violation, tplSuggestion) {
    var declare = lang.declare;
    var Base = Widget;
    var CACHELIMIT = 500;


    /**
     * A Combobox visualizes a combination of a drop-down list or list box and a single-line editable textbox,
     * allowing the user to either type a value directly or select a value from the list.
     *
     * ##### JavaScript
     * ```javascript
     * var Products = declare(Collection, {
     *      resource: 'products'
     * });
     * var products = new Products();
     *
     * var combobox = new Combobox({
     *      el: $('[name=combobox]')
     * }).render();
     *
     * combobox.data(products);
     *
     * combobox.valueField('id');
     * combobox.displayField('name');
     *
     * combobox.value("rose,daisy");
     *
     * expect(combobox.value()).to.eq("rose,daisy");
     *
     * ```
     * ##### HTML
     * + div: required.
     *
     * ```html
     * <div name="combobox" class="form-control" style="width: 200px;></div>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Combobox#value}
     *
     * @constructor Combobox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {Collection} [config.data=new Collection()] - The collection to provide selection options data.
     * @param {String} [config.valueField='value'] - Specifies the value field.
     * @param {String} [config.displayField='title'] - Specifies the display field.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     * @param {Boolean} [config.maxLength] - Specifies the maximum number of selections allowed.
     * @param {Boolean} [config.minLength] - Specifies the minimum number of selections required.
     * @param {Boolean} [config.manual=false] - Setting manual to true will allow users to type new values not in the options.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        var tooltips = new Tooltips({
            target: this
        }).render();

        this._tooltips = tooltips;

        this._value = [];
        this.reference(this.reference());
        this.code(this.code());
        this.valueField() || this.valueField("value");
        this.displayField() || this.displayField("title");
        this._manual = cfg.manual;

        (cfg.data) ? this.data(cfg.data) : this.data(new Collection());

        var anchors = this._anchors;
        this.addClass('uw-combobox');
        this.$el.append($("<input type='search' maxlength='10' size='10'>"));
        if(anchors.$input) anchors.$input.off();
        anchors.$input = this.$("input");
        anchors.$input.on("keyup", _.bind(onKeyUp, this));
        anchors.$input.on("keydown", _.bind(onKeyDown, this));

        this._suggestion = new Suggestion();
        this.listenTo(this._suggestion, 'select', _.bind(onSelect, this));
        this._popup = new Popup().render();
        this._popup.content(this._suggestion);
        this.$el.on('click', _.bind(onClick, this));

        (cfg.required!==undefined) && this.required(cfg.required);
        (cfg.maxLength!==undefined) && this.maxLength(cfg.maxLength);
        (cfg.minLength!==undefined) && this.minLength(cfg.minLength);
        (cfg.readonly!==undefined) && this.readonly(cfg.readonly);
        (cfg.disabled!==undefined) && this.disabled(cfg.disabled);

        this.readonly() && this.readonly(true);
        this.disabled() && this.disabled(true);

        this.clearValidation();

        var referenceData;
        if(this.reference()) {
            referenceData = reference.get(this.reference());
        } else if(this.code()) {
            referenceData = code.get(this.code());
        }

        if(referenceData) {
            this.data(referenceData);
        } else if(cfg.data) {
            this.data(cfg.data);
        } else {
            this.data(new Collection());
        }
    }

    function render(options) {

        var defaults = {
            hidden: false
        };
        options || (options = {});
        _.defaults(options, defaults);

        this.$('div').remove();
        _.each(this._value, function(value) {
            lookup.call(this, value, this.valueField());
            addTag.call(this, value);
        }, this);

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function onKeyUp(event) {
        var $input = this._anchors.$input;
        var value = $input.val();

        if(/(;|,| )$/.test(value)) {
            push.call(this, value.substr(0, value.length-1));
            $input.val('');
            return false;
        }

        var keyword = value;
        suggest.call(this, keyword);
    }


    function onKeyDown(event) {
        var $input = this._anchors.$input;
        var value = $input.val();

        if(event.which===8) {
            // When the backspace key is pressed.
            if(value.length===0) {
                // Remove the last added choice if no keyword.
                pop.call(this, _.last(this._value));
                return;
            } else {
                // suggest if there is keyword.
                var keyword = value.substring(0, value.length-1);
                suggest.call(this, keyword);
            }
        } else if(event.which===40) {
            // suggest when the down arrow key is pressed.
            var keyword = $input.val();
            suggest.call(this, keyword);
        }
    }

    function push(value) {

        if(!value||value.length===0) return;

        if(this._value.length>=this.maxLength()) return;

        if(_.contains(this._value, value)) return;

        if(!this._manual&&(find.call(this, value)===undefined)) return;

        this._value.push(value);

        addTag.call(this, value);

        this.validate() && this.change();
    }

    function addTag(value) {
        var $input = this._anchors.$input;
        var displayField = this.displayField();
        var item = find.call(this, value);
        var locale = i18n.locale();
        var title = item ? item.get(displayField, {locale: locale}) : value;

        var $tag = $("<div><span>x</span>" + title + "</div>");
        $tag.data('value', value);
        $input.before($tag);

        $tag.find('span').one('click', _.bind(onRemoverClick, this));
    }

    function onRemoverClick(event) {
        if(this.disabled() || this.readonly()) return;
        var $tag = $(event.target.parentElement);
        var value = $tag.data('value');
        pop.call(this, value);

    }

    function removeTag(value) {
        var index = _.indexOf(this._value, value);
        this.$('div').eq(index).remove();
    }

    function find(value) {
        var valueField = this.valueField();
        var displayField = this.displayField();
        var locale = i18n.locale();
        return this._data.find(function(item) {
            return (item.get(valueField)===value)||(item.get(displayField, {locale: locale})===value);
        }, this);
    }

    function pop(value) {
        removeTag.call(this, value);
        this._value = _.without(this._value, value);

        this.validate() && this.change();
    }

    function change() {
        var event = {
            data: this.value(),
            context: this,
            source: this
        };

        this.trigger("change", event);
    }

    function suggest(keyword) {
        var data = this._data;

        var items = match.call(this, keyword, this.displayField());
        if(items.length===0 && data.size()<=data.limit()) lookup.call(this, keyword, this.displayField());
        items = match.call(this, keyword, this.displayField());

        this._suggestion.data(items);
        this._suggestion.outerWidth(this.outerWidth(true), true);
        this._popup.show({
            positionTo: this
        });

        this._suggestion.$('ul').css('height', '100%');

        var height = window.innerHeight - this._popup.$el.position().top;

        if(this._suggestion.outerHeight()>height) {
            this._suggestion.$('ul').outerHeight(height - 30 - 20);
        }
    }

    function match(keyword, searchField) {
        var data = this._data;
        var valueField = this.valueField();
        var displayField = this.displayField();
        var locale = i18n.locale();
        var keyword = keyword;

        var items = data.filter(function(item) {
            var value = item.get(searchField, {locale: locale});
            if(value && (value===keyword || value.toUpperCase().indexOf(keyword.toUpperCase())!==-1)) return true;
            return false;
        }, this);

        items = _.map(items, function(item){
            return {
                title: item.get(displayField, {locale: locale}),
                value: item.get(valueField)
            }
        }, this);

        return items;
    }

    function lookup(keyword, searchField) {
        var data = this._data;
        var locale = i18n.locale();

        var regexp = new RegExp(keyword, "i");
        var index = data.findIndex(function(model) {
            var text = model.get(searchField, {locale: locale});
            if(text.search(regexp)!=-1) return true;
            return false;
        }, this);

        if(index!=-1) return;

        if(_.isFunction(this._lookup)) {
            this._lookup(keyword, searchField);
            return;
        }

        if(!data.isFetchable()) return;

        /**
         * @todo preserve the collection's filters.
         */

        var filters = [];
        if(keyword!=null && keyword.length>0) {
            filters.push({
                property: searchField,
                value: keyword,
                operator: "LIKE"
            });
        }
        
        data.fetch({
            filters: filters,
            limit: CACHELIMIT,
            async: false
        });
    }

    function onSelect(event) {
        var value = event.data;

        this._anchors.$input.val("");

        _.contains(this._value, value) ? pop.call(this, value) : push.call(this, value);

    }

    function onClick(event) {
        if(this.disabled() || this.readonly()) return;
        var $input = this._anchors.$input;
        $input.focus();
        var keyword = $input.val();
        suggest.call(this, keyword);
    }

    /**
     * Get or set the data.
     *
     * @memberof Combobox#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @returns {Collection}
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {};

        _.defaults(options, defaults);

        if(items instanceof Collection) {
            this._data = items;
        } else if(_.isArray(items)) {
            this._data = new Collection();
            _.each(items, function(item) {
                var model;
                if(_.isObject(item)) {
                    model = new Model(item);
                } else {
                    model = new Model({
                        value: item,
                        title: item
                    })
                }
                this._data.add(model);
            }, this);
        }

        this._data.comparator = this.displayField();

        if(this._data.size()===0 && this._data.isFetchable()) {
            this._data.fetch({
                limit: CACHELIMIT,
                async: false
            });
        } else {
            this._data.sort();
        }
        
        return this._data;
    }

    function valueField(field) {
        if(arguments.length===0) return this.$el.attr('valueField');

        this.$el.attr('valueField', field);
    }

    function displayField(field) {
        if(arguments.length===0) return this.$el.attr('displayField');

        this.$el.attr('displayField', field);
    }
    
    /**
     *
     * Get or set the value.
     *
     * @param {String|String[]} value
     * @returns {string|undefined}
     */
    function value(value, options) {
        options || (options = {});
        
        if(arguments.length===0) {
            return (this._value && this._value.length>0) ? this._value.join() : null;
        }

        value || (value = []);

        _.isArray(value) || (value = value.split(','));

        if(value.length>0 && _.difference(value, this._value).length===0) return;

        this._value = value;

        this.render();

        this.validate() && (options.silent || this.change());
    }

    function clear() {
        this._value = [];
        this.render();
        this.clearValidation();
    }

    function name(name) {

        if(!name) return this.$el.attr('name');

        this.$el.attr('name', name);
    }

    function type() {
        var $input = this._$component;

        return $input.attr('type');
    }

    function size() {
        return this._value.length;
    }

    /**
     * @todo
     * @param hint
     * @returns {*}
     */
    function placeholder(hint) {
    }


    function clearValidation() {
        this._violations = [];
        this._tooltips.content("");
        this.removeClass('uw-border-warn');
    }

    function readonly(value) {
        if(arguments.length===0) return this.el.hasAttribute('readonly');

        value ? this.el.setAttribute('readonly', null) : this.el.removeAttribute('readonly');
        
        var $input = this._anchors.$input;
        $input.prop('disabled', value);
    }

    function required(value) {
        if(arguments.length===0) return this.el.hasAttribute('required');

        value ? this.el.setAttribute('required', null) : this.el.removeAttribute('required');
    }

    function disabled(value) {
        if(arguments.length===0) return this.el.hasAttribute('disabled');

        value ? this.el.setAttribute('disabled', null) : this.el.removeAttribute('disabled');

        var $input = this._anchors.$input;
        $input.prop('disabled', value);
    }

    function maxLength(value) {
        var $component = this.$el;
        if(arguments.length===0) return Number($component.attr('maxLength'));
        $component.attr('maxLength', value);
    }

    function minLength(value) {
        var $component = this.$el;
        if(arguments.length===0) return Number($component.attr('minLength'));
        $component.attr('minLength', value);
    }


    function referencType(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('reference');
        $component.attr('reference', value);
        if(this.reference()) {
            this.valueField('code');
            this.displayField() || this.displayField('text');
        }
    }

    function codeType(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('code');
        $component.attr('code', value);
        if(this.code()) {
            this.valueField('code');
            this.displayField('text');
        }
    }


    /**
     * Validate the value of the inputbox.
     */
    function validate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $control = this.$el;
        var validity = $control.prop('validity');
        var valid = true;

        this.clearValidation();

        (valid && _.isObject(validity)) && (valid = this.checkValidity(options));

        (valid && this.useCustomType()) && (valid = this.customValidate(options));

        return valid;
    }

    function checkValidity(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $input = this.$el;
        var validity = $input.prop('validity');

        if(!validity.valid) {
            _.each(VALIDATIONERRORS, function(prop, error) {
                if(_.contains(options.ignore, error.toUpperCase())) return;

                if(validity[error]) {
                    var parameters = [];
                    if(prop) {
                        parameters.push($input.prop(prop)||$input.attr(prop));
                    }
                    this.violate(new Violation({
                        properties: [this.name()],
                        error: error,
                        parameters: parameters
                    }));
                }
            }, this);

            var validationMessage = "";
            _.each(this._violations, function(violation) {
                validationMessage = validationMessage + '\n' + violation.toString();
            });
            $input.get(0).setCustomValidity(validationMessage);
        }

        return (this._violations.length===0);
    }

    function useCustomType() {
        return true;
    }

    function customValidate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var valid = true;

        _.contains(options.ignore, Violation.Error.VALUEMISSING) || this.validateRequired() || (valid = false);
        _.contains(options.ignore, Violation.Error.TOOLONG) || this.validateMaxLength() || (valid = false);
        _.contains(options.ignore, Violation.Error.TOOSHORT) || this.validateMinLength() || (valid = false);

        return valid;
    }

    function validateRequired() {
        if(!this.required()) return true;

        if(this.size()===0) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.VALUEMISSING
            }));
            return false;
        }

        return true;
    }

    function validateMaxLength() {
        if(!this.maxLength()) return true;

        if(this.size()>this.maxLength()) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.TOOLONG,
                parameters: [this.maxLength()]
            }));
            return false;
        }

        return true;
    }

    function validateMinLength() {
        if(!this.minLength()) return true;

        if(this.size()<this.minLength()) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.TOOSHORT,
                parameters: [this.minLength()]
            }));
            return false;
        }

        return true;
    }

    function violate(violation) {
        if(!violation) return;

        this._violations.push(violation);

        this.hasClass('uw-border-warn') || this.addClass('uw-border-warn');

        this._tooltips.append(violation.toString() + "<br>");

        return violation;
    }


    function violations() {
        return this._violations;
    }

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value.toUpperCase();

        this.readonlyState() && this.readonly(_.contains(this.readonlyState().toUpperCase().split(","), this._state));
        this.disabledState() && this.disabled(_.contains(this.disabledState().toUpperCase().split(","), this._state));
        this.hiddenState() && _.contains(this.hiddenState().toUpperCase().split(","), this._state) ? this.hide() : this.show();
    }

    function readonlyState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-readonly-state');
        $input.attr('data-readonly-state', value);
    }

    function disabledState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-disabled-state');
        $input.attr('data-disabled-state', value);
    }

    function hiddenState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-hidden-state');
        $input.attr('data-hidden-state', value);
    }

    var Suggestion = declare(Widget, {
        initialize: function() {
            this.$el.html(_.template(tplSuggestion)(this));
        },
        render: function() {
            var $content = this.$('ul');
            $content.empty();
            _.each(this._data, function(datum) {
                var $item = $("<li class='list-group-item'>" + datum.title + "</li>");
                $item.data('value', datum.value);
                $content.append($item);
            });
            var $items = this.$('li');

            $items.on('click', _.bind(this.onSelect, this));
        },
        data: function(items) {
            this._data = items;
            this.render();
        },
        onSelect: function(event) {
            var $item = $(event.target);

            this.trigger('select', {
                context: this,
                source: this,
                data: $item.data('value')
            });
        }
    }, {
        _data: []
    });

    var props = {
        _data: undefined,
        _value: undefined,
        _manual: undefined,
        _tooltips: undefined,
        _state: undefined
    };

    var Combobox = declare(Base, {
        initialize: initialize,
        render: render,
        value: value,
        clear: clear,
        size: size,
        change: change,
        data: data,
        valueField: valueField,
        displayField: displayField,
        name: name,
        type: type,
        placeholder: placeholder,
        validate: validate,
        violate: violate,
        violations: violations,
        checkValidity: checkValidity,
        useCustomType: useCustomType,
        customValidate: customValidate,
        clearValidation: clearValidation,
        validateRequired: validateRequired,
        validateMaxLength: validateMaxLength,
        validateMinLength: validateMinLength,
        readonly: readonly,
        required: required,
        disabled: disabled,
        maxLength: maxLength,
        minLength: minLength,
        reference: referencType,
        code: codeType,
        state: state,
        readonlyState: readonlyState,
        disabledState: disabledState,
        hiddenState: hiddenState
    }, props);

    return Combobox ;

});