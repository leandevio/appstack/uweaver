/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Inputbox', 'uweaver/browser', 'uweaver/exception/Violation',
    'uweaver/datetime', 'uweaver/i18n', 'uweaver/widget/Datepicker',
    'uweaver/widget/Popup'], function ($, _, lang, Inputbox, browser, Violation, datetime, i18n, Datepicker, Popup){
    var declare = lang.declare;
    var Base = Inputbox;
    var SUPPORTEDTYPES = ['date', 'datetime-local', 'month'];
    var SUPPORTEDDATATYPES = ['date', 'datetime', 'month'];
    var HTML5DATEPATTERN = {
        'date': "yyyy-MM-dd",
        'datetime-local': "yyyy-MM-ddTHH:mm",
        'month': "yyyy-MM"

    };

    /**
     * A Textbox is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * var datebox = new Datebox({
     *     el: $('[name=effectiveDate]')
     * });
     * datebox.render();
     *
     * var now = new Date();
     * 
     * datebox.value(now);
     *
     * expect(datebox.value()).to.equal(now);
     * ```
     * ##### HTML
     * + selector: input[type=datetime-local].
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <input type="date" name="effectiveDate"> 
     * ...
     * </form>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Datebox#value}
     *
     * @constructor Datebox
     * @extends Inputbox
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value=true] - Specifies the value of an <input> element.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     * @param {Integer} [config.max] - Specifies the maximum value for an <input> element.
     * @param {Integer} [config.min] - Specifies a minimum value for an <input> element.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        
        var type = this.type();
        var scale;
        
        if(type==='date') {
            scale = Datepicker.Scale.DATE;
        } else if(type==='month') {
            scale = Datepicker.Scale.MONTH;
        } else {
            scale = Datepicker.Scale.TIME;
        }
        
        this._datepicker = new Datepicker({scale: scale}).render();
        this._datepickerPopup = new Popup().render();
        this._datepickerPopup.content(this._datepicker);
        this.listenTo(this._datepicker, 'select', _.bind(onDatepickerSelect, this));
    }
    
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        options || (options = {});

        var $input = this.$el;

        if(this.useCustomType()) {
            $input.on('click', _.bind(toggleDatepicker, this));
        }

        if(this.render === render) {
            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }
    

    function format(value) {
        return datetime.format(value, this.pattern());
    }

    /**
     * Parse the text of the inputbox to a value.
     *
     */
    function parse(text) {
        var value;
        
        if(text===null||text===undefined||text.trim()==='') {
            value = null;
        } else {
            value = datetime.parse(text, this.pattern());
        }

        return value;
    }

    function customValidate() {
        var $input = this.$el;
        var pattern = this.pattern();
        var valid = true;

        if(!datetime.test($input.val(), pattern)) {
            var parameters = [pattern];
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.PATTERMMISMATCH,
                parameters: parameters
            }));
            valid = false;
        }
        return valid;
    }

    function useCustomType() {
        return (!this.type()||this.type()===''||!browser.inputtypes.date);
    }

    function pattern() {
        var pattern;
        var type = this.type();
        var subtype;

        if(type==='datetime-local') {
            subtype = 'datetime'
        } else {
            subtype = type;
        }

        if(!this.useCustomType()) {
            pattern = HTML5DATEPATTERN[type];
        } else if(subtype==="date") {
            pattern = i18n.datePattern();
        } else if(subtype==="time") {
            pattern = i18n.timePattern();
        } else if(subtype==="month") {
            pattern = i18n.monthPattern();
        } else {
            pattern = i18n.datetimePattern();
        }

        return pattern;
    }
    
    function toggleDatepicker() {
        if(this._datepickerPopup.isVisible()) {
            this._datepickerPopup.hide();
            return;
        }

        var value = this.value();
        this._datepicker.select(value, {silent: true});
        this._datepicker.goto(value);
        this._datepickerPopup.show({
            positionTo: this
        });
    }
    
    function onDatepickerSelect(event) {
        this.value(event.data);
    }

    function change() {
        Base.prototype.change.apply(this, arguments);
        if(this._datepickerPopup.isVisible()) {
            var value = this.value();
            this._datepicker.select(value, {silent: true});
            this._datepicker.goto(value);
        }
    }

    var props = {
        className: 'form-control',
        SUPPORTEDTYPES: SUPPORTEDTYPES,
        SUPPORTEDDATATYPES: SUPPORTEDDATATYPES,
        _datepicker: undefined,
        _datepickerPopup: undefined
    };

    var Datebox = declare(Base, {
        initialize: initialize,
        render: render,
        format: format,
        parse: parse,
        useCustomType: useCustomType,
        customValidate: customValidate,
        pattern: pattern,
        change: change
    }, props);

    return Datebox;


});