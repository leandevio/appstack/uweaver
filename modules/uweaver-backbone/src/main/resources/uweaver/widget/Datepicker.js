/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Widget', 'uweaver/datetime', 'uweaver/formatter',
    'uweaver/i18n', 'text!./tpl/Datepicker.html'], function ($, _, lang, Widget, datetime, formatter, i18n, tpl) {
    var declare = lang.declare;
    var Base = Widget;
    
    var MONTHETAG = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var DAYTAG = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    /**
     * A Calendar is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * ```
     * ##### HTML
     *
     * ```html

     * ```
     *
     * ##### Events:
     * + 'select' - [select()]{@link Datepicker#select}
     *
     * @constructor Datepicker
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {Datepicker.Scale} [config.scale] - specify the scale of the datepicker.
     */
    function initialize(config) {
        var defaults = {
            scale: Datepicker.Scale.DATE
        };

        _.defaults(config, defaults);

        Base.prototype.initialize.apply(this, arguments);

        this._scale = config.scale;
        this._current = new Date();
        this._selection = [];
    }
    
    function render(options) {
        options || (options = {});

        this.$el.off();

        var dayTags = renderDayTags.call(this);
        var dateCells = renderDateCells.call(this);

        this.$el.html(_.template(tpl)({
            current: formatter.format(this._current, {subtype: 'month'}),
            dayTags: dayTags,
            dateCells: dateCells
        }));

        this.$el.on('click', 'thead tr i[value]', _.bind(onCommandTrigger, this));
        this.$el.on('click', 'tbody tr td', _.bind(onDateSelect, this));

        (this._scale===Datepicker.Scale.TIME) ? this.$('[data-role=clock]').show() : this.$('[data-role=clock]').hide();

        if(this.render === render) {
            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function onCommandTrigger(event) {
        event.stopPropagation();

        var value = $(event.currentTarget).attr('value');

        if(value===Datepicker.Command.PREVIOUS) {
            this.previous();
        } else if(value===Datepicker.Command.NEXT) {
            this.next();
        } else if(value===Datepicker.Command.TODAY) {
            this.goto(new Date());
        }
    }

    function onDateSelect(event) {
        event.stopPropagation();

        var $cell = $(event.currentTarget);
        
        var date = new Date($cell.attr('data-year'), $cell.attr('data-month'), $cell.text());
        
        this.select(date);
        this.goto(date);

    }
    
    function renderDayTags() {
        var dayTags = "<tr>";

        _.each(DAYTAG, function(tag) {
            dayTags = dayTags + "<th class='uw-text-center uw-border-bottom' style='padding-left:5px;padding-right:5px;'>" + tag + "</th>";
        });

        dayTags = dayTags + "</tr>";
        
        return dayTags;
    }

    function renderDateCells() {
        var dateCells = "";

        var current = this._current;
        var firstDate = new Date(current.getFullYear(), current.getMonth(), 1);
        var day = firstDate.getDay();
        var date = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() - day);

        do {
            dateCells = dateCells + "<tr>";
            for(var i=0; i<7; i++) {
                dateCells = dateCells + "<td class='uw-text-center uw-clickable' data-year='" + date.getFullYear()
                    + "' data-month='" + date.getMonth() + "'>" + date.getDate() + "</td>";
                date.setDate(date.getDate() + 1);
            }
            dateCells = dateCells + "</tr>";
        } while (date.getMonth()===current.getMonth());

        return dateCells;
    }

    function select(date, options) {
        options || (options = {});

        if(!date) {
            this._selection = [];
            return;
        }

        if(this._selection.length>0 && date.getTime()===_.first(this._selection).getTime()) {
            return;
        }

        this._selection = [];

        this._selection.push(date);
        
        options.silent || this.trigger('select', {
            context: this,
            source: this,
            data: date
        });
    }

    function selection() {
        return this._selection;
    }

    function goto(date) {
        if(!date) return;

        if(this._current.getFullYear()===date.getFullYear() && this._current.getMonth()===date.getMonth()) return;

        this._current = date;
        this.render();
    }

    function next() {
        this._current.setMonth(this._current.getMonth()+1);
        this.render();
    }

    function previous() {
        this._current.setMonth(this._current.getMonth()-1);
        this.render();
    }

    var props = {
        _scale: undefined,
        _current: undefined,
        _selection: undefined
    };

    var Datepicker = declare(Base, {
        initialize: initialize,
        render: render,
        select: select,
        selection: selection,
        goto: goto,
        previous: previous,
        next: next
    }, props);


    Datepicker.Scale = {
        MONTH: "MONTH",
        DATE: "DATE",
        TIME: "TIME"
    };

    Datepicker.Command = {
        PREVIOUS: "PREVIOUS",
        NEXT: "NEXT",
        TODAY: "TODAY"
    };
    
    return Datepicker;
});