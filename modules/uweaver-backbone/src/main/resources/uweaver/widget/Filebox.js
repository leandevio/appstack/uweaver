/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/i18n', 'uweaver/widget/Widget', 'uweaver/widget/Tooltips',
    'uweaver/exception/Violation'], function ($, _, lang, i18n, Widget, Tooltips, Violation){
    var declare = lang.declare;
    var Base = Widget;

    var VALIDATIONERRORS = {
        'valueMissing': null,
        'typeMismatch': 'type',
        'patternMismatch': 'pattern',
        'tooLong': 'maxLength',
        'tooShort': 'minLength',
        'rangeUnderFlow': 'min',
        'rangeOverflow': 'max',
        'stepMismatch': 'step',
        'badInput': null};

    /**
     * A Filebox is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * var pictures = new Filebox({
     *     el: $('[name=pictures]')
     * });
     * pictures.render();
     *
     * expect(pictures.value()).to.be.instanceof(Array);
     * ```
     * ##### HTML
     * + selector: input[type=file].
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <input type="file" name="pictures">
     * ...
     * </form>
     * ```
     * 
     * ##### Events:
     * + 'change' - [value()]{@link Filebox#value}, [add()]{@link Filebox#add}, [remove()]{@link Filebox#remove}
     *
     *
     * @constructor Filebox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} [config.value] - Specifies the value(files) of the filebox.
     * @param {Boolean} [config.multiple=false] - specifies that the user is allowed to enter more than one file in the filedropper.
     * @param {Boolean} [config.required=false] - Specifies that the filebox must have at least one file.
     * @param {Boolean} [config.disabled=false] - Specifies that the filebox should be disabled.
     * @param {Integer} [config.maxLength] - Specifies the maximum number of file allowed in the filebox.
     * @param {Integer} [config.minLength] - Specifies the minimum number of files allowed in the filebox.
     * @param {String} [config.chooseHandler] - Specifies the handler to invoke when file(s) chosen to upload.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        var defaults = {};
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._items = [];

        this.$el.on("change", _.bind(onChange, this));


        var tooltips = new Tooltips({
            target: this
        }).render();

        this._tooltips = tooltips;

        (cfg.multiple!==undefined) && this.multiple(cfg.multiple);
        (cfg.accept!==undefined) && this.accept(cfg.accept);
        (cfg.required!==undefined) && this.required(cfg.required);
        (cfg.maxLength!==undefined) && this.maxLength(cfg.maxLength);
        (cfg.minLength!==undefined) && this.minLength(cfg.minLength);
        (cfg.readonly!==undefined) && this.readonly(cfg.readonly);
        (cfg.disabled!==undefined) && this.disabled(cfg.disabled);


        this.clearValidation();
    }

    function render(options) {
        Base.prototype.render.apply(this, arguments);

        this.hide();

        enhance.call(this);

        this.show();

        return this;
    }

    function enhance() {
        var spans = this.el.parentElement.querySelectorAll("span");
        _.each(spans, function(span) {
            span.innerText = i18n.translate(span.innerText);
        });
    }

    function onChange(event) {
        event.stopPropagation();
        event.preventDefault();

        this.choose(event.target.files);
    }

    /**
     * Get or set the handler to invoke when the user select the files to add.
     *
     * The files dropped are passed as the parameter and the handler should return the files to be stored.
     *
     * ##### JavaScript
     * ```javascript
     * var pictures = new Filedropper({
     *     el: $('[name=pictures]')
     * });
     *
     * function handler(files) {
     *      Date now = new Date();
     *
     *      _.each(files, function(file){
     *          file.dateAdded = now;
     *      });
     *
     *      return files;
     * }
     *
     * pictures.addHandler(handler);
     * ```
     *
     * @memberof Filedropper#
     * @param {Function} fn - The handler.
     * @returns {Function|void}
     */
    function chooseHandler(fn) {
        if(arguments.length===0) return this._chooseHandler;

        this._chooseHandler = fn;
    }

    /**
     * Add a file to the filedropper.
     *
     * ##### Events:
     * + 'change' - triggered after added. event.data => the files.
     *
     * @memberof Filedropper#
     * @param {File|Object} file - The file to add.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.selected=true] - A false value to prevent the new file from being selected.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Filedropper} this
     */
    function add(item, options) {
        if(item===undefined) return this;

        var defaults = {
            index: this.size()
        };

        options || (options = {});

        _.defaults(options, defaults);

        item instanceof File && (item.toJSON = fileToJSON);

        // force this.value() to change.
        var items = this._items.slice();
        items.splice(options.index, 0, item);
        this._items = items;

        if(options.silent) return this;

        this.validate() && this.change();

        return this;
    }

    /**
     * Remove a file to the filebox.
     *
     * ##### Events:
     * + 'change' - triggered after added. event.data => the files.
     *
     * @memberof Filebox#
     * @param {File|Object|Integer} file - The file(or index of file) to remove.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Filedropper} this
     */
    function remove(item, options) {
        options || (options = {});

        var index = _.isNumber(item) ? item : _.findIndex(this.value(), function(n) {
            return n === item;
        });

        if(!index && index!==0) return;

        // force this.value() to change.
        var items = this._items.slice();
        items.splice(index, 1);
        this._items = items;

        options.silent || this.change();

        return this;
    }

    function removeAll(options) {
        options || (options = {});

        if(this.size()===0) return;

        this._items = [];

        options.silent || this.change();

        return this;
    }

    /**
     * Get the file.
     *
     * @memberof Filebox#
     * @param {Integer} index - The index of file. zero-based.
     * @returns {File}
     */
    function get(index) {
        if(!index && index!==0) return;

        if(index>this.size() || index<0) return null;

        return this._items[index];
    }


    function size() {
        return this._items.length;
    }

    function value(items, options) {
        options || (options = {});
        
        if(arguments.length===0) {
            if(this.multiple()) {
                return (this.size()===0) ? null : this._items;
            } else {
                return (this.size()===0) ? null : _.first(this._items);
            }
        }

        items || (items = []);

        _.isArray(items) || (items = [items]);

        if(equals.call(this, items)) return;

        _.each(items, function(item) {
            item instanceof File && (item.toJSON = fileToJSON);
        });

        this._items = items;

        if(this._items.length==0) this.el.value = '';

        this.validate() && (options.silent || this.change());
    }

    function clear() {
        var $input = this.$el;

        $input.val(null);

        this._items = [];

        this.clearValidation();
    }

    function equals(items) {
        return JSON.stringify(items) === JSON.stringify(this._items);
    }

    function fileToJSON() {
        var json = {};

        _.each(this, function(value, key) {
            json[key] = value;
        });

        json.name = this.name;
        json.type = this.type;
        json.size = this.size;

        return json;
    }

    function change() {
        this.trigger('change', {
            data: this.value(),
            context: this,
            source: this
        });
    }

    function choose(files) {
        this.chooseHandler() && (files = this.chooseHandler()(files));

        if(!files||files.length===0) return;

        // append or replace?
        this.removeAll();

        if(this.multiple()) {
            if(this.size() + files.length > this.maxLength()) {
                return;
            }
        } else {
            ;
        }

        _.each(files, function(file) {
            this.add(file, {silent: true});
        }, this);

        this.trigger('choose', {
            data: files,
            context: this,
            source: this
        });

        this.render();

        this.validate() && this.change();
    }

    function name(name) {

        if(!name) return this.$el.attr('name');

        this.$el.attr('name', name);
    }

    function type() {
        var $component = this.$el;

        return $component.attr('type');
    }

    function clearValidation() {
        this._violations = [];
        this._tooltips.content("");
        this.removeClass('uw-border-warn');
    }

    function readonly(value) {
        if(arguments.length===0) return this.el.hasAttribute('readonly');

        if(value) {
            this.el.setAttribute('readonly', null);
        } else {
            this.el.removeAttribute('readonly');
        }
    }

    function required(value) {
        if(arguments.length===0) return this.el.hasAttribute('required');

        value ? this.el.setAttribute('required', null) : this.el.removeAttribute('required');
    }

    function disabled(value) {
        if(arguments.length===0) return this.el.hasAttribute('disabled');

        if(value) {
            this.el.setAttribute('disabled', null);
        } else {
            this.el.removeAttribute('disabled');
        }
    }

    function maxLength(value) {
        var $component = this.$el;

        if(arguments.length===0) {
            if(!this.el.hasAttribute('multiple')) return 1;

            var value = Number($component.attr('maxLength'));

            return value;
        }

        $component.attr('maxLength', value);
    }

    function minLength(value) {
        var $component = this.$el;
        if(arguments.length===0) return Number($component.attr('minLength'));
        $component.attr('minLength', value);
    }

    function multiple(value) {
        if(arguments.length===0) return this.el.hasAttribute('multiple');

        value ? this.el.setAttribute('multiple', null) : this.el.removeAttribute('multiple');
    }

    function accept(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('accept');
        $component.attr('accept', value);
    }

    /**
     * Validate the value of the inputbox.
     */
    function validate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $control = this.$el;
        var validity = $control.prop('validity');
        var valid = true;

        this.clearValidation();

        (valid && _.isObject(validity)) && (valid = this.checkValidity(options));

        (valid && this.useCustomType()) && (valid = this.customValidate(options));

        return valid;
    }

    function checkValidity(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $input = this.$el;
        var validity = $input.prop('validity');

        if(!validity.valid) {
            _.each(VALIDATIONERRORS, function(prop, error) {
                if(_.contains(options.ignore, error.toUpperCase())) return;

                if(validity[error]) {
                    var parameters = [];
                    if(prop) {
                        parameters.push($input.prop(prop)||$input.attr(prop));
                    }
                    this.violate(new Violation({
                        properties: [this.name()],
                        error: error.toUpperCase(),
                        parameters: parameters
                    }));
                }
            }, this);

            var validationMessage = "";
            _.each(this._violations, function(violation) {
                validationMessage = validationMessage + '\n' + violation.toString();
            });
            $input.get(0).setCustomValidity(validationMessage);
        }

        return (this._violations.length===0);
    }

    function useCustomType() {
        return true;
    }

    function customValidate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var valid = true;
        _.contains(options.ignore, Violation.Error.VALUEMISSING) || this.validateRequired() || (valid = false);
        _.contains(options.ignore, Violation.Error.TOOLONG) || this.validateMaxLength() || (valid = false);
        _.contains(options.ignore, Violation.Error.TOOSHORT) || this.validateMinLength() || (valid = false);

        return valid;
    }

    function validateRequired() {
        if(!this.required()) return true;

        if(this.size()===0) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.VALUEMISSING
            }));
            return false;
        }

        return true;
    }

    function validateMaxLength() {
        if(!this.maxLength()) return true;

        if(this.size()>this.maxLength()) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.TOOLONG,
                parameters: [this.maxLength()]
            }));
            return false;
        }

        return true;
    }

    function validateMinLength() {
        if(!this.minLength()) return true;

        if(this.size()<this.minLength()) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.TOOSHORT,
                parameters: [this.minLength()]
            }));
            return false;
        }

        return true;
    }

    function violate(violation) {
        if(!violation) return;

        this._violations.push(violation);

        this.hasClass('uw-border-warn') || this.addClass('uw-border-warn');

        this._tooltips.append(violation.toString() + "<br>");

        return violation;
    }


    function violations() {
        return this._violations;
    }

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value.toUpperCase();

        this.readonlyState() && this.readonly(_.contains(this.readonlyState().toUpperCase().split(","), this._state));
        this.disabledState() && this.disabled(_.contains(this.disabledState().toUpperCase().split(","), this._state));
        this.hiddenState() && _.contains(this.hiddenState().toUpperCase().split(","), this._state) ? this.hide() : this.show();
    }

    function readonlyState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-readonly-state');
        $input.attr('data-readonly-state', value);
    }

    function disabledState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-disabled-state');
        $input.attr('data-disabled-state', value);
    }

    function hiddenState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-hidden-state');
        $input.attr('data-hidden-state', value);
    }
    /**
     * @todo
     * @param hint
     * @returns {*}
     */
    function placeholder(hint) {
    }

    var props = {
        _items: undefined,
        _index: undefined,
        _addHandler: undefined,
        _rest: undefined,
        _state: undefined
    };

    var Filebox = declare(Base, {
        initialize: initialize,
        render: render,
        value: value,
        clear: clear,
        size: size,
        add: add,
        remove: remove,
        removeAll: removeAll,
        choose: choose,
        name: name,
        type: type,
        placeholder: placeholder,
        validate: validate,
        violate: violate,
        violations: violations,
        checkValidity: checkValidity,
        useCustomType: useCustomType,
        customValidate: customValidate,
        clearValidation: clearValidation,
        validateRequired: validateRequired,
        validateMaxLength: validateMaxLength,
        validateMinLength: validateMinLength,
        multiple: multiple,
        readonly: readonly,
        required: required,
        disabled: disabled,
        maxLength: maxLength,
        minLength: minLength,
        accept: accept,
        state: state,
        readonlyState: readonlyState,
        disabledState: disabledState,
        hiddenState: hiddenState,
        change: change,
        chooseHandler: chooseHandler
    }, props);

    return Filebox;
});