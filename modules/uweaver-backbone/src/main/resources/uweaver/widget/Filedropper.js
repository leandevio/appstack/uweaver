/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Widget' ,'uweaver/data/proxy/REST', 'uweaver/net', 'uweaver/widget/Tooltips',
    'uweaver/exception/Violation', 'uweaver/string', 'uweaver/formatter'], function ($, _, lang, Widget, REST, net, Tooltips, Violation, string, formatter){
    var declare = lang.declare;
    var Base = Widget;
    var VALIDATIONERRORS = {
        'valueMissing': null,
        'typeMismatch': 'type',
        'patternMismatch': 'pattern',
        'tooLong': 'maxLength',
        'tooShort': 'minLength',
        'rangeUnderFlow': 'min',
        'rangeOverflow': 'max',
        'stepMismatch': 'step',
        'badInput': null};

    var ACCEPTITEMTYPES = {
        'image/*': /^image\//,
        'audio/*': /^audio\//,
        'video/*': /^video\//,
        '.txt': 'text/plain',
        '.csv': 'text/csv',
        '.pdf': 'application/pdf',
        '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        '.doc': 'application/msword',
        '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        '.ppt': 'application/vnd.ms-powerpoint',
        '.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        '.zip': 'application/zip',
        '.json': 'application/json'
    };

    var FILEEXTICONS = {
        '.csv': 'file-text-o',
        '.txt': 'file-text-o',
        '.pdf': 'file-pdf-o',
        '.xlsx': 'file-excel-o',
        '.xls': 'file-excel-o',
        '.doc': 'file-word-o',
        '.docx': 'file-word-o',
        '.ppt': 'file-powerpoint-o',
        '.pptx': 'file-powerpoint-o'
    };

    var FILETYPEICONS = {};

    var EXTENTIONS = {
        'image/*': /[^\s]+(\.(jpg|png|gif|bmp))$/
    };

    /**
     * A Filebox is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * var pictures = new Filedropper({
     *     el: $('[name=pictures]')
     * });
     * pictures.render();
     *
     * expect(pictures.value()).to.be.instanceof(Array);
     * ```
     * ##### HTML
     * + selector: div.
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <div data-control="filedropper" name="pictures">
     * ...
     * </form>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Filedropper#value}, [add()]{@link Filedropper#add}, [remove()]{@link Filedropper#remove}
     *
     * @constructor Filedropper
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value] - Specifies the value(files) of the filedropper.
     * @param {Boolean} [config.multiple=false] - specifies that the user is allowed to enter more than one file in the filedropper.
     * @param {String} [config.accept] - the accepted content type.
     * @param {Boolean} [config.readonly=false] - Specifies that the filedropper is read-only.
     * @param {Boolean} [config.required=false] - Specifies that the filedropper must have at least one file.
     * @param {Boolean} [config.disabled=false] - Specifies that the filedropper should be disabled.
     * @param {Integer} [config.maxLength] - Specifies the maximum number of files allowed in the filedropper.
     * @param {Integer} [config.minLength] - Specifies the minimum number of files allowed in the filedropper.
     * @param {Function} [config.dropHandler] - Specifies the handler to invoke when file(s) dropped.
     * @param {Function} [config.deleteHandler] - Specifies the handler to invoke before the user deletes the file.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this.cfg();
        
        _.defaults(cfg, defaults);

        cfg.dropHandler && (this._dropHandler = cfg.dropHandler);
        cfg.deleteHandler && (this._deleteHandler = cfg.deleteHandler);

        this._items = [];
        this._index = 0;

        this.$el.css({
            'position': 'relative',
            'overflow': 'hidden'
        });

        this._$image = $('<img>');
        this._$image.css({
            position: 'relative',
            display: 'block',
            'margin-left': 'auto',
            'margin-right': 'auto'
        });
        this.$el.append(this._$image);
        this._$image.hide();
        this._$image.on('load', _.bind(onImageLoad, this));
        this._$image.on('error', _.bind(onImageError, this));

        this._$text = $('<div></div>');
        this._$text.css({
            position: 'relative',
            display: 'block',
            'margin-left': 'auto',
            'margin-right': 'auto'
        });
        this.$el.append(this._$text);
        this._$text.hide();

        this._$info = $('<div></div>');
        this._$info.css({
            position: 'relative',
            display: 'block',
            'margin-left': 'auto',
            'margin-right': 'auto'
        });
        this.$el.append(this._$info);
        this._$info.hide();

        this._$remove = $('<i>');
        this._$remove.addClass('fa fa-remove');
        this._$remove.css({
            position: 'absolute',
            cursor: 'pointer',
            display: 'none',
            'font-size': 'larger'
        });
        this.$el.append(this._$remove);

        this._$readonlyCover = $("<div class='uw-background-gray' style='position:absolute;opacity:0.2'></div>");
        this._$readonlyCover.css({left:0, top:0});
        this.$el.append(this._$readonlyCover);

        this._$previous = $('<i>');
        this._$previous.addClass('fa fa-chevron-left');
        this._$previous.css({
            position: 'absolute',
            cursor: 'pointer',
            display: 'none',
            'font-size': 'larger'
        });
        this.$el.append(this._$previous);

        this._$next = $('<i>');
        this._$next.addClass('fa fa-chevron-right');
        this._$next.css({
            position: 'absolute',
            cursor: 'pointer',
            display: 'none',
            'font-size': 'larger'
        });
        this.$el.append(this._$next);

        this.$el.on("dragover", _.bind(onDragOver, this));
        this.$el.on("drop", _.bind(onDrop, this));
        // this.$el.on("dragleave", _.bind(onDragLeave, this));

        this._$previous.on('click', _.bind(onPrevious, this));
        this._$next.on('click', _.bind(onNext, this));
        this._$remove.on('click', _.bind(onRemove, this));

        this._$disabledCover = $("<div class='uw-background-gray' style='position:absolute;opacity:0.6'></div>");
        this._$disabledCover.css({left:0, top:0});
        this.$el.append(this._$disabledCover);


        $(window).on('resize', _.bind(render, this));

        this._rest = new REST();

        var tooltips = new Tooltips({
            target: this
        }).render();

        this._tooltips = tooltips;

        (cfg.multiple!==undefined) && this.multiple(cfg.multiple);
        (cfg.accept!==undefined) && this.accept(cfg.accept);
        (cfg.required!==undefined) && this.required(cfg.required);
        (cfg.maxLength!==undefined) && this.maxLength(cfg.maxLength);
        (cfg.minLength!==undefined) && this.minLength(cfg.minLength);
        (cfg.readonly!==undefined) && this.readonly(cfg.readonly);
        (cfg.disabled!==undefined) && this.disabled(cfg.disabled);

        this.disabled(this.disabled());
        this.readonly(this.readonly());
        this.accept(this.accept());

        this.clearValidation();

        (this.mold()===Filedropper.Mold.CAROUSEL) && this.play();

    }

    function play() {
        this.index()===(this.size()-1) ? this.first() : this.next();
        _.delay(_.bind(this.play, this), 6000);
    }

    function render(options) {
        if(this.$el.width()<=0) {
            _.delay(_.bind(render, this), 1000, options);
            return this;
        }

        options || (options = {});

        var item = this.get();

        renderItem.call(this, item);

        renderControls.call(this);

        this._$disabledCover.outerWidth(this.$el.outerWidth()); this._$disabledCover.outerHeight(this.$el.outerHeight());
        this._$readonlyCover.outerWidth(this.$el.outerWidth()); this._$readonlyCover.outerHeight(this.$el.outerHeight());

        return this;
    }

    function renderControls() {

        var item = this.get();

        if(this.size()>1) {
            this._$previous.css({
                top: this.$el.outerHeight() - 30,
                left: 10
            });
            this._$next.css({
                top: this.$el.outerHeight() - 30,
                left: this.$el.outerWidth() - 30
            });
            this._$previous.show();
            this._$next.show();
        } else {
            this._$previous.hide();
            this._$next.hide();
        }

        if(this.size()>0 && (!this.deleteHandler() || this.deleteHandler()(item))) {
            this._$remove.css({
                top: 10,
                left: this.$el.outerWidth() - 30
            });
            this._$remove.show();
        } else {
            this._$remove.hide();
        }
    }

    function renderItem(item) {
        this._$image.hide();
        this._$text.hide();
        this._$info.hide();

        if(!item) return;

        renderInfo.call(this, item);

        if(/^image\/.*$/.test(item.type)) {
            renderImageItem.call(this, item);
        }
        
    }
    
    function renderImageItem(item) {
        this._$image.css({
            height: 'auto',
            width: 'auto'
        });
        
        if(item instanceof File) {
            var reader = new FileReader();
            reader.onload = _.bind(function(event) {
                this._$image.prop('src', event.target.result);

            }, this);

            reader.readAsDataURL(item);
        } else {

            var url = _.isFunction(item.url) ? item.url() : item.url;

            var promise = net.send(url, {responseType: 'blob'});
            promise.then(function(response, status, xhr) {
                window.URL.revokeObjectURL(this._$image.prop('src'));
                this._$image.prop('src', window.URL.createObjectURL(response));
            }, this);
        }

        this._$image.data('item', item);
    }
    
    function renderTextItem(item) {
        
    }
    
    function renderInfo(item) {
        var ext = /\.[0-9a-z]+$/i.exec(item.name);
        var template = "<table><tr><td><i class='fa fa-${icon}'></i></td><td>${name}<br>${size} bytes</td></tr></table>";
        var icon = _.has(FILEEXTICONS, ext) ? FILEEXTICONS[ext] : 'file';
        var size = (item.size || item.size===0) ? formatter.format(item.size) : '?';

        var html = string.substitute(template, {
            icon: icon,
            name: item.name,
            size: size
        });

        this._$info.html(html);

        this._$info.find('table').css({
            'margin-left': 'auto',
            'margin-right' : 'auto',
            height: '100%'
        });
        this._$info.find('i').css({
            'font-size': '60px',
            'margin-right': '12px'
        });

        this._$info.height(this.$el.height());
        this._$info.data('item', item);
        this.mold()===Filedropper.Mold.CAROUSEL || this._$info.show();
    }

    function onImageLoad() {
        var item = this._$image.data('item');
        if(this._$info.data('item')!==item) return;

        this._$info.hide();
        var wRatio = this._$image.width() / this.$el.width();
        var hRatio = this._$image.height() / this.$el.height();
        if(wRatio > hRatio) {
            this._$image.width(this.$el.width());
        } else {
            this._$image.height(this.$el.height());
        }

        var top = (this.$el.height() - this._$image.height()) / 2;
        this._$image.css('top', top);

        this._$image.show();
    }

    function onImageError() {
        var item = this._$image.data('item');
        if(this._$info.data('item')!==item) return;

        this._$info.hide();
        var template = "<table><tr><td><i class='fa fa-${icon}'></i></td><td>${name}<br>${size} bytes</td></tr></table>";
        var icon = 'chain-broken';
        var size = (item.size || item.size===0) ? formatter.format(item.size) : '?';

        var html = string.substitute(template, {
            icon: icon,
            name: item.name,
            size: size
        });

        this._$info.html(html);

        this._$info.find('table').css({
            'margin-left': 'auto',
            'margin-right' : 'auto',
            height: '100%'
        });
        this._$info.find('i').css({
            'font-size': '60px',
            'margin-right': '12px'
        });

        this._$info.height(this.$el.height());
        this._$info.show();
    }

    
    function onDrop(event) {
        event.stopPropagation();
        event.preventDefault();

        if(this.readonly()||this.disabled()) return;

        this.drop(event.dataTransfer.files);
    }

    /**
     * Get or set the handler to invoke when the files dropped.
     *
     * The files dropped are passed as the parameter and the handler should return the files to be stored.
     *
     * ##### JavaScript
     * ```javascript
     * var pictures = new Filedropper({
     *     el: $('[name=pictures]')
     * });
     *
     * function handler(files) {
     *      Date now = new Date();
     *
     *      _.each(files, function(file){
     *          file.dateAdded = now;
     *      });
     *
     *      return files;
     * }
     *
     * pictures.dropHandler(handler);
     * ```
     *
     * @memberof Filedropper#
     * @param {Function} fn - The handler.
     * @returns {Function|void}
     */
    function dropHandler(fn) {
        if(arguments.length===0) return this._dropHandler;

        this._dropHandler = fn;
    }

    /**
     * Get or set the handler to invoke before the user deletes the file.
     *
     * The file is passed as the parameter to the handler. The handler return true to allow the file to be deleted.
     *
     * ##### JavaScript
     * ```javascript
     * var pictures = new Filedropper({
     *     el: $('[name=pictures]')
     * });
     *
     * function handler(file) {
     *      return file.readonly;
     * }
     *
     * pictures.deleteHandler(handler);
     * ```
     *
     * @memberof Filedropper#
     * @param {Function} fn - The handler.
     * @returns {Function|void}
     */
    function deleteHandler(fn) {
        if(arguments.length===0) return this._deleteHandler;

        this._deleteHandler = fn;
    }

    function onDragOver(event) {
        event.stopPropagation();
        event.preventDefault();
        
        var items = event.dataTransfer.items;

        if(this.readonly()||this.disabled()|| !validateTransferItems.call(this, items)
            ||(this.size() + items.length> this.maxLength())) {
            event.dataTransfer.dropEffect = 'none';
        } else {
            event.dataTransfer.dropEffect = 'copy';
        }
    }

    function validateTransferItems(items) {
        var valid = true;

        _.each(items, function(item) {
            item.kind==='file' || (valid = false);
        });

        if(!valid) return false;

        var acceptItemTypes = this._acceptItemTypes;

        if(!acceptItemTypes) return true;

        _.each(items, function(item) {
            if(!valid) return;

            var type = _.find(acceptItemTypes, function(type) {
                if(type instanceof RegExp) {
                    return type.test(item.type);
                } else {
                    return (item.type===type);
                }
            });

            valid = (type) ? true : false;
        });

        return valid;
    }

    function validateFiles(files) {
        var valid = true;
        var regExp;
        if(!regExp) {
            var pattern = accept.replace(',', '|').replace('\.', '');
            regExp = new RegExp("[^\\s]+(\\.(" + pattern + "))$");
        }
        _.each(items, function(item) {
            if(!reg.test(item.type)) {
                valid = false;
            }
        });
        return valid;

    }

    // function onDragLeave(event) {
    //     if(event.target!==this._$image.get(0)) return;
    //
    //     event.stopPropagation();
    //     event.preventDefault();
    //
    //     var item = $(event.target).data('item');
    //
    //     if(item!==this.get()) return;
    //
    //     this.remove(item);
    // }

    function onRemove() {
        this.remove();
    }


    function onPrevious() {
        this.previous();
    }

    function onNext() {
        this.next();
    }

    /**
     * Add a file to the filedropper.
     *
     * ##### Events:
     * + 'change' - triggered after added. event.data => the files.
     *
     * @memberof Filedropper#
     * @param {File|Object} file - The file to add.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.selected=true] - A false value to prevent the new file from being selected.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Filedropper} this
     */
    function add(item, options) {
        if(item===undefined) return this;

        var defaults = {
            index: this.size()
        };

        options || (options = {});

        _.defaults(options, defaults);

        item instanceof File && (item.toJSON = fileToJSON);

        // force the this.value() to change
        var items = this._items.slice();
        items.splice(options.index, 0, item);
        this._items = items;

        if(options.silent) return this;

        this.goto(options.index);

        this.validate() && this.change();

        return this;
    }

    /**
     * Remove a file to the filedropper.
     *
     * ##### Events:
     * + 'change' - triggered after added. event.data => the files.
     *
     * @memberof Filedropper#
     * @param {File|Object|Integer} file - The file(or index of file) to remove.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A true value will prevent the events from being triggered.
     * @returns {Filedropper} this
     */
    function remove(item, options) {
        if(item===undefined) item = this._index;

        options || (options = {});

        var index = _.isNumber(item) ? item : _.findIndex(this.value(), function(n) {
            return n === item;
        });

        if(!index && index!==0) return;

        // force the this.value() to change
        var items = this._items.slice();
        items.splice(index, 1);
        this._items = items;

        if(options.silent) return this;

        this.goto(index-1);

        this.validate() && this.change();

        return this;
    }

    /**
     * Get the file.
     *
     * @memberof Filedropper#
     * @param {Integer} index - The index of file. zero-based.
     * @returns {File}
     */
    function get(index) {
        if(arguments.length===0) index = this._index;

        if(!index && index!==0) return;

        if(index>this.size() || index<0) return null;
        
        return this._items[index];
    }

    function size() {
        return this._items.length;
    }

    function previous() {
        return this.goto(this._index-1);
    }

    function next() {
        return this.goto(this._index+1);
    }
    
    function first() {
        return this.goto(0);
    }
    
    function last() {
        return this.goto(this.size()-1);
    }

    function goto(which) {
        if(which===undefined) return this;

        if(this.current()===which) return this;

        var index = _.isNumber(which) ? which : _.findIndex(this._items, function(n) {
            return n === which;
        });

        this._index = Math.max(Math.min(index, this.size()-1), 0);

        this.render();

        this.trigger('select', {
            data: which,
            context: this,
            source: this
        });

        return this;
    }
    
    function value(items, options) {
        options || (options = {});
        
        if(arguments.length===0) {
            if(this.multiple()) {
                return (this.size()===0) ? null : this._items;
            } else {
                return (this.size()===0) ? null : _.first(this._items);
            }
        }

        items || (items = []);

        _.isArray(items) || (items = [items]);

        if(equals.call(this, items)) return;

        _.each(items, function(item) {
            item instanceof File && (item.toJSON = fileToJSON);
        });

        this._items = items;
        this._index = 0;

        this.goto(0);

        this.validate() && (options.silent || this.change());
    }

    function index() {
        return this._index;
    }

    function current() {
        return this.get();
    }
    
    function clear() {
        this._items = [];
        this._index = 0;

        this.render();
        this.clearValidation();
    }

    function equals(items) {
        return JSON.stringify(items) === JSON.stringify(this._items);
    }

    function fileToJSON() {
        var json = {};

        _.each(this, function(value, key) {
            json[key] = value;
        });

        json.name = this.name;
        json.type = this.type;
        json.size = this.size;

        return json;
    }
    
    function change() {
        this.trigger('change', {
            data: this.value(),
            context: this,
            source: this
        });
    }

    function drop(files) {
        this.dropHandler() && (files = this.dropHandler()(files));

        if(!files||files.length===0) return;

        if(this.size() + files.length > this.maxLength()) {
            return;
        }
        _.each(files, function(file) {
            this.add(file, {silent: true});
        }, this);

        this.last();

        this.trigger('drop', {
            data: files,
            context: this,
            source: this
        });

        this.render();

        this.validate() && this.change();
    }

    function name(name) {

        if(!name) return this.$el.attr('name');

        this.$el.attr('name', name);
    }

    function type() {
        var $component = this.$el;

        return $component.attr('type');
    }

    function clearValidation() {
        this._violations = [];
        this._tooltips.content("");
        this.removeClass('uw-border-warn');
    }

    function readonly(value) {
        if(arguments.length===0) return this.el.hasAttribute('readonly');

        if(value) {
            this.el.setAttribute('readonly', null);
            this._$readonlyCover.show();
        } else {
            this.el.removeAttribute('readonly');
            this._$readonlyCover.hide();
        }
    }

    function required(value) {
        if(arguments.length===0) return this.el.hasAttribute('required');

        value ? this.el.setAttribute('required', null) : this.el.removeAttribute('required');
    }

    function disabled(value) {
        if(arguments.length===0) return this.el.hasAttribute('disabled');

        if(value) {
            this.el.setAttribute('disabled', null);
            this._$disabledCover.show();
        } else {
            this.el.removeAttribute('disabled');
            this._$disabledCover.hide();
        }
    }

    function maxLength(value) {
        var $component = this.$el;

        if(arguments.length===0) {
            if(!this.el.hasAttribute('multiple')) return 1;

            var value = Number($component.attr('maxLength'));

            return value;
        }

        $component.attr('maxLength', value);
    }

    function minLength(value) {
        var $component = this.$el;
        if(arguments.length===0) return Number($component.attr('minLength'));
        $component.attr('minLength', value);
    }

    function multiple(value) {
        if(arguments.length===0) return this.el.hasAttribute('multiple');

        value ? this.el.setAttribute('multiple', null) : this.el.removeAttribute('multiple');
    }
    
    function accept(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('accept');
        $component.attr('accept', value);


        if(value && value.length>0) {
            var accepts = this.accept().split(',');

            var itemTypes = [];

            _.each(accepts, function(accept) {

                if(_.has(ACCEPTITEMTYPES, accept)) {
                    itemTypes.push(ACCEPTITEMTYPES[accept]);
                } else if(accept.indexOf('/')!==-1) {
                    itemTypes.push(accept);
                } else {
                    itemTypes.push(/^(.)+\/(.)+/);
                    itemTypes.push('');

                }
            });
            this._acceptItemTypes = itemTypes;
        } else {
            this._acceptItemTypes = undefined;
        }

    }

    /**
     * Validate the value of the inputbox.
     */
    function validate() {
        var $control = this.$el;
        var validity = $control.prop('validity');
        var valid = true;

        this.clearValidation();

        (valid && _.isObject(validity)) && (valid = this.checkValidity());

        (valid && this.useCustomType()) && (valid = this.customValidate());

        return valid;
    }

    function checkValidity() {
        var $input = this.$el;
        var validity = $input.prop('validity');

        if(!validity.valid) {
            _.each(VALIDATIONERRORS, function(prop, error) {
                if(validity[error]) {
                    var parameters = [];
                    if(prop) {
                        parameters.push($input.prop(prop)||$input.attr(prop));
                    }
                    this.violate(new Violation({
                        properties: [this.name()],
                        error: error.toUpperCase(),
                        parameters: parameters
                    }));
                }
            }, this);

            var validationMessage = "";
            _.each(this._violations, function(violation) {
                validationMessage = validationMessage + '\n' + violation.toString();
            });
            $input.get(0).setCustomValidity(validationMessage);
        }


        return validity.valid;
    }

    function useCustomType() {
        return true;
    }

    function customValidate() {
        var valid = true;

        this.validateRequired() || (valid = false);
        this.validateMaxLength() || (valid = false);
        this.validateMinLength() || (valid = false);

        return valid;
    }

    function validateRequired() {
        if(!this.required()) return true;

        if(this.size()===0) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.VALUEMISSING
            }));
            return false;
        }

        return true;
    }

    function validateMaxLength() {
        if(!this.maxLength()) return true;

        if(this.size()>this.maxLength()) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.TOOLONG,
                parameters: [this.maxLength()]
            }));
            return false;
        }

        return true;
    }

    function validateMinLength() {
        if(!this.minLength()) return true;

        if(this.size()<this.minLength()) {
            this.violate(new Violation({
                properties: [this.name()],
                error: Violation.Error.TOOSHORT,
                parameters: [this.minLength()]
            }));
            return false;
        }

        return true;
    }

    function violate(violation) {
        if(!violation) return;

        this._violations.push(violation);

        this.hasClass('uw-border-warn') || this.addClass('uw-border-warn');

        this._tooltips.append(violation.toString() + "<br>");

        return violation;
    }


    function violations() {
        return this._violations;
    }

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value.toUpperCase();

        this.readonlyState() && this.readonly(_.contains(this.readonlyState().toUpperCase().split(","), this._state));
        this.disabledState() && this.disabled(_.contains(this.disabledState().toUpperCase().split(","), this._state));
        this.hiddenState() && _.contains(this.hiddenState().toUpperCase().split(","), this._state) ? this.hide() : this.show();
    }

    function readonlyState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-readonly-state');
        $input.attr('data-readonly-state', value);
    }

    function disabledState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-disabled-state');
        $input.attr('data-disabled-state', value);
    }

    function hiddenState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-hidden-state');
        $input.attr('data-hidden-state', value);
    }

    function mold(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('mold');

        $input.attr('mold', value);
    }

    /**
     * @todo
     * @param hint
     * @returns {*}
     */
    function placeholder(hint) {
    }

    var props = {
        _items: undefined,
        _index: undefined,
        _$image: undefined,
        _$text: undefined,
        _$info: undefined,
        _$previous: undefined,
        _$next: undefined,
        _$remove: undefined,
        _dropHandler: undefined,
        _deleteHandler: undefined,
        _rest: undefined,
        _state: undefined,
        _acceptItemTypes: undefined
    };

    var Filedropper = declare(Base, {
        initialize: initialize,
        render: render,
        value: value,
        index: index,
        current: current,
        clear: clear,
        previous: previous,
        first: first,
        last: last,
        next: next,
        goto: goto,
        play: play,
        mold: mold,
        get: get,
        size: size,
        add: add,
        remove: remove,
        drop: drop,
        name: name,
        type: type,
        placeholder: placeholder,
        validate: validate,
        violate: violate,
        violations: violations,
        checkValidity: checkValidity,
        useCustomType: useCustomType,
        customValidate: customValidate,
        clearValidation: clearValidation,
        validateRequired: validateRequired,
        validateMaxLength: validateMaxLength,
        validateMinLength: validateMinLength,
        multiple: multiple,
        readonly: readonly,
        required: required,
        disabled: disabled,
        maxLength: maxLength,
        minLength: minLength,
        accept: accept,
        state: state,
        readonlyState: readonlyState,
        disabledState: disabledState,
        hiddenState: hiddenState,
        change: change,
        dropHandler: dropHandler,
        deleteHandler: deleteHandler
    }, props);

    Filedropper.Mold = {
        CAROUSEL: "carousel"
    };

    return Filedropper;
});