/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'jquery', 'uweaver/lang', 'uweaver/i18n',
    'uweaver/widget/Widget', 'uweaver/data/Model', 'uweaver/widget/Textbox', 'uweaver/widget/Numberbox',
    'uweaver/widget/Datebox', 'uweaver/widget/Checkbox', 'uweaver/widget/Radiobutton', 'uweaver/widget/Selectbox',
    'uweaver/widget/Combobox', 'uweaver/widget/Filedropper',
    'uweaver/widget/Filebox'], function (_, $, lang, i18n, Widget, Model, Textbox, Numberbox, Datebox, Checkbox,
                                             Radiobutton, Selectbox, Combobox, Filedropper, Filebox) {
    var declare = lang.declare;
    var Base = Widget;


    /**
     * A Form is a container containing form controls, such as Inputbox, Selectbox, Listbox and so on.
     *
     * ##### JavaScript
     *
     *     var Product = declare(Model, {
     *          resource: 'products'
     *     });
     *
     *     var item = new Product({_id: "_rose"});
     *
     *     var form = new Form({
     *         el: $('#form'),
     *         data: item
     *     });
     *     form.render();
     *
     *     item.fetch({async: false});
     *
     *     var priceControl = form.control('price')
     *     expect(priceControl.value()).to.equal(item.get('price'));
     *
     *     item.set('price', 4.8);
     *     expect(priceControl.value()).to.equal(item.get('price'));
     *
     *     priceControl.value(9.6);
     *     expect(priceControl.value()).to.equal(item.get('price'));
     *
     *
     * ##### HTML
     *     <!-- the following dom are required.
     *     + form - the DOM element.
     *     -->
     *     <form id="form" class="container form-horizontal">
     *         <label class="control-label col-sm-1 gutter">Name</label>
     *         <div class="col-sm-3 gutter">
     *             <input name="name" class="form-control" placeholder="Product Name">
     *         </div>
     *         <label class="control-label col-sm-1 gutter">Price</label>
     *         <div class="col-sm-3 gutter">
     *             <input name="price" type="number" class="form-control" placeholder="Unit Price">
     *         </div>
     *     </form>
     *
     * ##### Events:
     * + 'change' - [value()]{@link Form#value}, [values()]{@link Form#values}
     *
     * @constructor Form
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * ##### Options:
     * + tpl:String - A html string to layout & construct the DOM element. Optional.
     * + data:Model - Thd data source. Optional.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        
        var defaults = {};
        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        this._anchors = {};
        this._controls = [];
        this._disabled = {};

        (cfg.data) ? this.data(cfg.data) : this.data(new Model());

        cfg.locale && this.attr('locale', cfg.locale);
        this.hasAttr('locale') || this.attr('locale', i18n.locale());
    }

    /**
     * @override
     */
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };
        var cfg = this._cfg;
        var anchors = this._anchors;

        options || (options = {});
        _.defaults(options, defaults);

        clean.call(this);

        var controls = this._controls;

        // label
        var $labels = this.$('label');
        _.each($labels, function(element) {
            _.each(element.childNodes, function(node) {
                if(node instanceof Text) {
                    node.textContent = i18n.translate(node.textContent);
                }
            });
        }, this);

        // textbox
        var $textboxes = this.$('input[type=text][name], input[type=password][name], textarea[name]');
        _.each($textboxes, function(element) {
            var $textbox = $(element);
            var textbox = new Textbox({
                el: $textbox
            });
            textbox.render();
            this.listenTo(textbox, 'change', _.bind(onControlChange, this));
            controls.push(textbox);
        }, this);


        // numberbox
        var $numberboxes = this.$('input[type=number][name]');
        _.each($numberboxes, function(element) {
            var $numberbox = $(element);
            var numberbox = new Numberbox({
                el: $numberbox
            });
            numberbox.render();
            this.listenTo(numberbox, 'change', _.bind(onControlChange, this));
            controls.push(numberbox);
        }, this);

        // datebox
        var $dateboxes = this.$('input[type=date][name], input[type=datetime-local][name], input[type=month][name]');
        _.each($dateboxes, function(element) {
            var $datebox = $(element);
            var datebox = new Datebox({
                el: $datebox
            });
            datebox.render();
            this.listenTo(datebox, 'change', _.bind(onControlChange, this));
            controls.push(datebox);
        }, this);


        // checkbox
        var $checkboxes = this.$('input[type=checkbox][name]');
        _.each($checkboxes, function(element) {
            var $checkbox = $(element);
            var checkbox = new Checkbox({
                el: $checkbox
            });
            checkbox.render();
            this.listenTo(checkbox, 'change', _.bind(onControlChange, this));
            controls.push(checkbox);
        }, this);

        // radiobutton
        var $radiobuttons = this.$('input[type=radio][name]');
        _.each($radiobuttons, function(element) {
            var $radiobutton = $(element);
            var radiobutton = new Radiobutton({
                el: $radiobutton
            });
            radiobutton.render();
            this.listenTo(radiobutton, 'change', _.bind(onControlChange, this));
            controls.push(radiobutton);
        }, this);

        // selectbox
        var $selectboxes = this.$('select');
        _.each($selectboxes, function(element) {
            var $selectbox = $(element);
            var selectbox = new Selectbox({
                el: $selectbox
            });
            selectbox.render();
            this.listenTo(selectbox, 'change', _.bind(onControlChange, this));
            controls.push(selectbox);
        }, this);

        // combobox
        var $comboboxes = this.$('div[data-control=combobox][name]');
        _.each($comboboxes, function(element) {
            var $combobox = $(element);
            var combobox = new Combobox({
                el: $combobox
            });
            combobox.render();
            this.listenTo(combobox, 'change', _.bind(onControlChange, this));
            controls.push(combobox);
        }, this);

        // filedropper
        var $filedroppers = this.$('div[data-control=filedropper][name]');
        _.each($filedroppers, function(element) {
            var $filedropper = $(element);
            var filedropper = new Filedropper({
                el: $filedropper
            });
            filedropper.render();
            this.listenTo(filedropper, 'change', _.bind(onControlChange, this));
            controls.push(filedropper);
        }, this);

        // filebox
        var $fileboxes = this.$('input[type=file][name]');
        _.each($fileboxes, function(element) {
            var $filebox = $(element);
            var filebox = new Filebox({
                el: $filebox
            });
            filebox.render();
            this.listenTo(filebox, 'change', _.bind(onControlChange, this));
            controls.push(filebox);
        }, this);

        // label
        this._labels = [];
        var labels = this.el.querySelectorAll('label[name]');
        _.each(labels, function(label) {
            var name = label.getAttribute('name');
            var input = this.input(name);
            if(!input) return;
            if(input.required()) label.classList.add('uw-label-required');
            this._labels[name] = label;
        }, this);

        _.each(this.inputs(), function(input) {
            input.value(null, {silent: true});
        });
        update.call(this, this._data.toJSON());
        this.clearValidation();

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }


    function onControlChange(event) {
        var control = event.context;

        this.save(control.name());
    }

    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new model is bound. event.data => the data.
     *
     * @memberof Form#
     * @param {Model} item - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * ##### Options:
     * + silent(Boolean) - A false value will prevent the events from being triggered. Default = false.
     * @returns {Form} this
     */
    function data(item, options) {
        if(!item) return this._data;

        options || (options = {});

        var defaults = {};

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = item;

        this.listenTo(this._data, "change", _.bind(onItemUpdate, this));

        this.clearValidation();

        this.isRendered() && reset.call(this);

        (options.silent) || this.trigger('transition', {
            context: this,
            source: this,
            data: item
        });

        return this;
    }

    function onItemUpdate(event) {
        update.call(this, event.changedAttributes());
    }

    function controls(name) {
        var controls = this._controls;

        if(!name) return controls;

        return _.filter(controls, function(control) {
            return (control.name()===name)
        });
    }

    function control(name) {
        var controls = this._controls;

        return _.find(controls, function(control) {
            return (control.name()===name)
        });
    }

    function update(attributes) {
        var controls = this._controls;
        var data = this._data;
        var locale = this.locale();

        _.each(controls, function(control) {
            var name = control.name();
            var value = (control.i18n && control.i18n()) ? data.get(name, {locale: locale}) : attributes[name];
            _.has(attributes, name) && (control.value(value));
        }, this);
    }

    function save(name) {
        if(arguments.length===0) {
            if(!this.validate()) return;
            _.each(this._controls, function(control) {
                this.save(control.name());
            }, this);
            return;
        }

        var controls = this.controls(name);
        var value;

        if(controls.length===0) return;

        if(controls.length===1||(controls[0].type()!==controls[1].type())) {
            value = controls[0].value();
        } else {
            var values = _.reduce(controls, function(memo, control){
                if(control.value()!==null&&control.value()!==undefined) {
                    memo.push(control.value());
                }
                return memo;
            }, []);
            if(values.length===0) {
                value = null;
            } else if(values.length===1) {
                value = values[0]
            } else {
                value = values.join(',');
            }
        }

        var options = (controls[0].i18n && controls[0].i18n()) ? {locale: this.locale()} : null;

        nullify(this._data.get(name, options))===nullify(value) || this._data.set(name, value, options);
    }

    function nullify(value) {
        return (value===null||value===undefined) ? null : value;
    }

    function reset() {
        var controls = this._controls;
        var data = this._data;
        var locale = this.locale();
        _.each(controls, function(control) {
            var name = control.name();
            var value = (control.i18n && control.i18n()) ? data.get(name, {locale: locale}) : data.get(name);
            control.clear();
            data.has(name) && (control.value(value, {silent:true}));
        });
    }

    function clear() {
        var controls = this._controls;
        this._data.clear({silent: true});
        _.each(controls, function(control) {
            control.clear();
        });
    }

    /**
     * @override
     */
    function destroy() {
        clean.call(this);
        Base.prototype.destroy.apply(this, arguments);
    }

    function clean() {
        var controls = this._controls;
        var control;

        while(controls.length>0) {
            control = controls.shift();
            control.destroy();
        }
    }

    function checkValidity() {
        return (this.violations().length===0);
    }

    function violations() {
        var controls = this._controls;
        var violations = [];
        var index = {};
        _.each(controls, function(control) {
            if(index[control.name()]) {
                return
            } else if(control.violations()) {
                violations = violations.concat(control.violations());
                index[control.name()] = true;
            }
        });
        return violations;
    }

    function validate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var controls = this._controls;
        var valid = true;
        _.each(controls, function(control) {
            control.validate(options) || (valid = false);
        });

        return valid;
    }

    function violate(violations) {
        _.isArray(violations) || (violations = [violations]);
        _.each(violations, function(violation) {
            var properties = violation.properties();
            _.each(properties, function(property) {
                var control = this.control(property);
                control && control.violate(violation);
            }, this);
        }, this);
    }

    function clearValidation() {
        var controls = this._controls;
        _.each(controls, function(control) {
            control.clearValidation();
        });
    }

    /**
     * Get or set the state.
     *
     * ##### HTML
     *     <!--
     *     + username - will become readonly in edit & read state.
     *     + displayName - will become readonly in the read state.
     *     + quota - will become hidden in the add state and readonly in the read state.
     *     -->
     *
     *     <form id="form" class="container form-horizontal">
     *         <label class="control-label col-sm-1 gutter">Username</label>
     *         <div class="col-sm-3 gutter">
     *             <input name="username" class="form-control" placeholder="Username" data-readonly-state="edit,read">
     *         </div>
     *         <label class="control-label col-sm-1 gutter">Display Name</label>
     *         <div class="col-sm-3 gutter">
     *             <input name="displayName" class="form-control" placeholder="Display Name" data-readonly-state="read">
     *         </div>
     *         <label class="control-label col-sm-1 gutter">Quota</label>
     *         <div class="col-sm-3 gutter">
     *             <input name="quota" type="number" class="form-control" placeholder="Quota" data-hidden-state="add" data-readonly-state="read">
     *         </div>
     *     </form>
     *
     * @memberof Form#
     * @param {String} value - the state.
     * @returns {String|void}
     */
    function state(value) {
        if(arguments.length===0) return this._state;
        
        this._state = value;
        
        var controls = this._controls;
        _.each(controls, function(control) {
            control.state(value);
        });
        
    }

    function disable() {
        var controls = this._controls;
        _.each(controls, function(control) {
            this._disabled[control.oid()] = control.disabled();
            control.disabled(true);
        }, this);
    }

    function enable() {
        var controls = this._controls;
        _.each(controls, function(control) {
            _.has(this._disabled, control.oid()) && control.disabled(this._disabled[control.oid()]);
        }, this);

        this._disabled = {};
    }

    function locale(locale) {
        if(!locale) return this.attr('locale');

        if(locale===this.locale()) return;

        this.attr('locale', locale);
        this.reset();
    }

    var props = {
        _controls: undefined,
        _labels: undefined,
        _data: undefined,
        _state: undefined,
        _disabled: undefined,
        /**
         * @override
         */
        className: 'form'
    };

    var Form = declare(Base, {
        initialize: initialize,
        render: render,
        destroy: destroy,
        data: data,
        controls: controls,
        inputs: controls,
        control: control,
        input: control,
        checkValidity: checkValidity,
        violations: violations,
        validate: validate,
        violate: violate,
        clearValidation: clearValidation,
        state: state,
        disable: disable,
        enable: enable,
        clear: clear,
        reset: reset,
        save: save,
        locale: locale
    }, props);

    return Form

});