/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define(['underscore', 'jquery', 'uweaver/lang', 'uweaver/formatter', 'uweaver/string', 'uweaver/number', 'uweaver/boolean',
    'uweaver/widget/Widget', 'uweaver/data/Collection', 'uweaver/data/Model',
    'uweaver/i18n', 'uweaver/data/reference', 'uweaver/data/code',
    'uweaver/widget/Textbox', 'uweaver/widget/Numberbox', 'uweaver/widget/Datebox',
    'text!./tpl/Grid.html'], function (_, $, lang, formatter, string, number, boolean, Widget, Collection, Model,
                                       i18n, reference, code,
                                       Textbox, Numberbox, Datebox, tpl) {

    var declare = lang.declare;
    var Base = Widget;

    /**
     * This class represents a table.
     *
     * ##### Usage
     *     // Declare a new type of collection, Products, with the following designs:
     *     // * the resource - 'products'
     *     var declare = uweaver.lang.declare;
     *     var Products = declare(Collection, {
     *         resource: 'products'
     *     });
     *
     *     var items = new Products();
     *
     *     var renderer = function(item, dataIndex, value) {
     *          return '<i class="fa fa-trash uw-hover uw-clickable" style="font-size:24px;"></i>';
     *     };
     *
     *     var handler = function(item, dataIndex, value) {
     *          expect(item.cid).to.not.be.empty;
     *     };
     *
     *     var grid = new Grid({
     *         data: items,
     *         columns: [
     *             {text: "", handler: Grid.HANDLER.SELECTOR},
     *             {text: "", renderer: renderer, handler: handler},
     *             {text: "ID", dataIndex: "id", style: {width: '100px'}},
     *             {text: "Name", dataIndex: "name", style: {width: '150px'}},
     *             {text: "Price", dataIndex: "price", editable: true, style: {width: '80px'}},
     *             {text: "Note", dataIndex: "note"}],
     *        mode: Grid.MODE.SINGLE,
     *        el: $('#grid')
     *     });
     *     grid.render();
     *
     *     var pagination = new Pagination({
     *         data: items,
     *         el: this.$('#pagination')
     *     });
     *     pagination.render();
     *
     *     // fetch the data(models).
     *     // the grid & pagination will re-render after fetch completely.
     *     items.fetch();
     *
     * ##### Template
     *     <!-- the following dom are required.
     *     + table - the table.
     *     -->
     *     <table class="table table-striped"></table>
     *
     * ##### Events:
     * + 'select' - [select()]{@link Grid#select}, [toggle()]{@link Grid#toggle}
     * + 'deselect' - [deselect()]{@link Grid#deselect}, [toggle()]{@link Grid#toggle}
     * + 'transition' - [data()]{@link Grid#data}
     *
     * @constructor Grid
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} [config.tpl] - A html string to layout & construct the DOM element.
     * @param {Collection} [config.data=new Collection()] - A collection to specify the models to display.
     * @param {Grid.MODE} [config.mode=Grid.MODE.NONE] - A value to specify the selection model. Grid.MODE.SINGLE for single selection.
     * Grid.MODE.MULTI for multiple selection or Grid.MODE.NONE to disable selection.
     * @param {boolean} [allowNullSelection=true] - A value to specify if the user can deselect all the selections.
     * @param {int} [config.maxRowsToShow] - A value to specify height of the grid based on the number of rows to show.
     * @param {Object[]} [config.columns=[]] - An array of column configuration to define the columns. For examples:
     * [{text: "ID", dataIndex: "id"},{text: "Name", dataIndex: "name"}].
     * #####Column Configuration:
     * + text:String - the caption. required.
     * + dataIndex:String - the property of the model. required.
     * + type:String - the type of the property. optional.
     * + subtype:String - the subtype of the property. required.
     * + step:number - Specifies the legal number intervals for the number field. optional.
     * + style:Object - the css of the column. optional.
     * + editable:Boolean - A boolean value to specify if the column is editable. default: false.
     * + editor:Inputbox - Specifies an inputbox for the user to edit the column if the column is editable.
     * + pattern:String - the presentation format of the column. optional.
     * + renderer:Function(item, dataIndex, value) - a function to render the cells of the column. The current model,
     * dataIndex & value are passed as the item parameter and the function should return the html to display inside
     * the cell. optional.
     * + handler:Function(item, dataIndex, value) - a function to be call when the cells of the column are clicked.
     * The current model, dataIndex & value are passed as the item parameter. optional.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var props = readProps.call(this);

        var defaults = {
            data: new Collection(),
            mode: Grid.MODE.NONE,
            allowNullSelection: true,
            columns: []
        };
        var cfg = this._cfg;

        _.defaults(cfg, props);
        _.defaults(cfg, defaults);

        this._columns = cfg.columns;

        cfg.locale && this.attr('locale', cfg.locale);
        this.hasAttr('locale') || this.attr('locale', i18n.locale());

        this.mode(cfg.mode);

        this.data(cfg.data, {render: false});

        cfg.tpl || (cfg.tpl = tpl);

        $(document).on('click', _.bind(onDocumentClick, this));

        (this.$('table').length===0 && this.el.tagName!=='TABLE') && this.$el.append('<table></table>');

        this._$table = (this.$('table').length===0) ? this.$el : this.$('table');
        this._$heading = this._$table.clone();
        this._$heading.attr('role', 'heading');
        this._$heading.css({
            'margin-bottom' : '0px'
        });
        this._$table.css({
            'margin-top' : '0px'
        });

        (this._$table.find('thead').size()===0) && this._$table.append('<thead></thead>');
        (this._$table.find('tbody').size()===0) && this._$table.append('<tbody></tbody>');
        (this._$heading.find('thead').size()===0) && this._$heading.append('<thead></thead>');

        this._freezeRenderer = _.throttle(freezeRenderer, 100);

        $(window).on('resize', _.bind(this._freezeRenderer, this));
    }

    function readProps() {
        var props = {};

        props.mode = this.attr('mode');
        props.maxRowsToShow = parseInt(this.attr('maxRowsToShow'));
        props.allowNullSelection = (this.attr('allowNullSelection')==='true');

        var columns = [];
        _.each(this.el.querySelectorAll('thead th'), function(th){
            if(!th.hasAttribute('dataIndex') && !th.hasAttribute('handler')) return;
            var column = {
                text: th.innerText,
                handler: th.getAttribute('handler'),
                dataIndex: th.getAttribute('dataIndex'),
                type: th.getAttribute('type'),
                subtype: th.getAttribute('subtype'),
                step: th.getAttribute('step'),
                pattern: th.getAttribute('pattern'),
                i18n: boolean.parse(th.getAttribute('i18n')),
                editable: boolean.parse(th.getAttribute('editable')),
                style: toCssObject(th.style.cssText),
                code: th.getAttribute('code'),
                reference: th.getAttribute('reference'),
                referenceIndex: th.getAttribute('referenceIndex')
            };
            columns.push(column);
        });

        if(columns.length>0) {
            props.columns = columns;
        }
        return props;
    }

    function toCssObject(cssText) {
        var style = {};
        var pairs = cssText.split(';');
        _.each(pairs, function(pair) {
            var tokens = pair.split(':');
            if(tokens.length<2) return;
            style[tokens[0].trim()] = tokens[1].trim();
        });
        return style;
    }

    /**
     * @override
     */
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };

        options || (options = {});
        _.defaults(options, defaults);

        this._editableCell = undefined;
        if(this._editor) {
            this._editor.detach();
            this.stopListening(this._editor);
            this._editor = undefined;
        }

        var $headingThead = this._$heading.find('thead');
        var $tableThead = this._$table.find('thead');
        var $tbody = this._$table.find('tbody');

        $headingThead.empty();
        $tableThead.empty();
        $tbody.empty();

        var data = this._data;
        var row = "<tr>";
        var maxRowsToShow = this.cfg().maxRowsToShow || data.size();
        _.each(this._columns, function(element, index){
            var text = i18n.translate(element.text);
            var style = element.style || {};
            var indicator = "";

            if(element.editable) {
                var referenceData;

                if(element.reference) {
                    referenceData = reference.get(element.reference);
                } else if(element.code) {
                    referenceData = code.get(element.code);
                }
                referenceData || (indicator = indicator + "<i class='fa fa-pencil' style='margin-left:5px;'></i>");
            } else if(element.handler===Grid.HANDLER.SELECTOR && this._mode!==Grid.MODE.NONE) {
                if(this._mode===Grid.MODE.MULTI) {
                    indicator = indicator + "<input type='checkbox'>";
                }
                style = {width: '40px', 'text-align': 'center'};
                this._selectorIndex = index;
            }

            if((data.length>0) && (!style['text-align']) && (element.type==='number'|| _.isNumber(data.at(0).get(element.dataIndex)))) {
                style['text-align'] = 'right';
                style['padding-right'] = '12px';
            }
            row = row + "<th style= '" + toCssText(style) + "'><span>" + text + indicator + "</span></th>";
        }, this);
        row = row + "</tr>";
        $headingThead.append(row);
        $tableThead.append(row);

        data.each(function(item, index){
            var row = "<tr>";
            _.each(this._columns, function(element, index){
                var renderer = element.renderer;
                var style = element.style || {};
                var dataIndex = element.dataIndex;
                var locale = this.locale();
                var value = item.get(dataIndex);
                var text = "";

                if(element.reference) {
                    text = translateIntoReference(value, element.reference, element.referenceIndex, locale);
                } else if(element.code) {
                    text = translateIntoCode(value, element.code, locale);
                } else if(_.isFunction(renderer)) {
                    text = renderer(item, dataIndex, value);
                    if(element.type==='number') {
                        _.defaults(style, {
                            'text-align': 'right',
                            'padding-right': '12px'
                        });
                    }
                } else if(element.handler===Grid.HANDLER.SELECTOR) {
                    text = "<input type='checkbox' value='" + item.cid + "'>";
                    style = {width: '40px', 'text-align': 'center'};
                } else {
                    text = formatter.format(item.get(dataIndex, {locale: locale}), {
                        subtype: element.subtype,
                        pattern: element.pattern
                    });
                    if(element.type==='number'||_.isNumber(value)) {
                        _.defaults(style, {
                            'text-align': 'right',
                            'padding-right': '12px'
                        });
                    }
                }


                row = row + "<td data-uw-index='" + index + "' style= '" + toCssText(style) + "'>"
                    + ((text==undefined||text==null) ? "" : text) + "</td>";
            }, this);
            row = row + "</tr>";
            var $row = $(row);
            (index < maxRowsToShow) || $row.hide();
            $row.data('cid', item.cid);
            $tbody.append($row);
        }, this);

        if(this._mode===Grid.MODE.MULTI&&this._selectorIndex!==undefined) {
            var th = $headingThead.find('tr>th').get(this._selectorIndex);
            var $selector = $(th).find('input[type=checkbox]');
            $selector.on('click', _.bind(onAllSelectorClick, this));

            th = $tableThead.find('tr>th').get(this._selectorIndex);
            $selector = $(th).find('input[type=checkbox]');
            $selector.on('click', _.bind(onAllSelectorClick, this));
        }
        if(data.size()===0) {
            var nodata = "<tr><td colspan='${columns}' style='text-align:center'>No Data</td></tr>";
            $tbody.append(string.substitute(nodata, {columns: this._columns.length}));
        } else {
            $tbody.find('tr').on('click', _.bind(onRowClick, this));
            $tbody.find('tr>td').on('click', _.bind(onCellClick, this));
            this.renderSelection();
        }

        this._$table.css({
            'margin-top': '0px'
        });

        this._$heading.hide();

        options.hidden || this.show();

        postRender.call(this);

        return this;
    }

    function translateIntoReference(value, referenceType, referenceIndex, locale) {
        var referenceData = reference.get(referenceType);
        if(!referenceData) return value;

        var data = _.isArray(value) ? value : [value];
        var text = [];
        _.each(data, function(datum) {
            text.push(referenceData.translate(datum, referenceIndex, {locale: locale}));
        });

        return text.join(", ");
    }

    function translateIntoCode(value, codeType, locale) {
        var referenceData = code.get(codeType);
        if(!referenceData) return value;

        var data = _.isArray(value) ? value : [value];
        var text = [];
        _.each(data, function(datum) {
            text.push(referenceData.translate(datum, {locale: locale}));
        });

        return text.join(", ");
    }


    function onAllSelectorClick(event) {
        var $selector = $(event.currentTarget);

        $selector.prop('checked') ? this.selectAll() : this.deselectAll();
    }

    function postRender() {
        if(!this.isVisible()) {
            setTimeout(_.bind(postRender, this), 100);
            return;
        }

        if(this._$wrapper===undefined) {
            this._$wrapper = $("<div role='wrapper'><div role='scroller' style='overflow-y: auto'></div></div>");
            this._$table.wrap(this._$wrapper);
            this._$wrapper = this.el.parentElement.parentElement;
            this._$scroller = this._$table.parent();
            this._$scroller.before(this._$heading);
            this._$scroller.css({
                'margin-bottom': this._$table.css('margin-bottom')
            });
            this._$table.css({
                'margin-bottom': '0px'
            });
            var locale = this.attr('locale');
            this.setElement(this._$wrapper);
            this.attr('locale', locale);
        }

        this._$scroller.height(this._$table.outerHeight());

        this._isRendered = true;

        this.trigger('render', {
            data: {},
            context: this,
            source: this
        });

        this._freezeRenderer.call(this);
    }

    function freezeRenderer() {
        if(!this.isRendered()) return;

        var maxRowsToShow = this.cfg().maxRowsToShow;
        var $rows = this._$table.find('tbody > tr');
        var size = $rows.length;
        var $tableThs = this._$table.find('thead > tr > th');
        var $theadingThs = this._$heading.find('thead > tr > th');


        if(size===0||!maxRowsToShow) return;

        this._$heading.get(0).className = this._$table.get(0).className;

        $rows.show();
        $theadingThs.each(function(index, th) {
            if(index===$theadingThs.length-1) {
                $(th).css('width', '');
                return;
            }
            var width = $($tableThs.get(index)).outerWidth();
            $(th).outerWidth(width);
            $(th).outerWidth(width);
        });

        this._$heading.show();

        var theadHeight = this._$table.find('thead').outerHeight()
            + number.parse(this._$table.css('border-top-width'), "##0.########px") * 2;
        this._$table.css({
            'margin-top': (theadHeight * -1) + 'px'
        });

        var scrollTop = this._$scroller.scrollTop();
        for(var i=maxRowsToShow; i<size; i++) {
            $($rows.get(i)).hide();
        }
        this._$scroller.height(this._$table.outerHeight() - this._$heading.outerHeight());
        $rows.show();
        this._$scroller.scrollTop(scrollTop);
    }

    function toCssText(style) {
        var cssText = "";
        _.each(style, function(value, key) {
            cssText = cssText + key + ":" + value + ";";
        });
        return cssText;
    }

    function onRowClick(event) {
        if(this._editor && event.target===this._editor.el) return;

        var $row = $(event.currentTarget);
        var cid = $row.data('cid');
        this.toggle(cid);
    }

    function onCellClick(event) {
        if(this._editor && event.target===this._editor.el) return;

        var $cell = $(event.currentTarget);
        var index = $cell.attr('data-uw-index');
        var column = this._columns[index];
        var $row = $cell.parent();
        var cid = $row.data('cid');
        var locale = column.i18n ? this.locale() : null;
        var item = this.data().get(cid);
        var dataIndex = column.dataIndex;
        var value = item.get(dataIndex, {locale: locale});

        if(column.handler) {
            if(column.handler!==Grid.HANDLER.SELECTOR) {
                column.handler(item, dataIndex, value);
                event.stopPropagation();
            }
        }

        if(!column.editable) return;

        if(_.first($cell)=== _.first(this._editableCell)) {
            return;
        }

        renderCell.call(this, this._editableCell);
        makeCellEditable.call(this, $cell);
        this._freezeRenderer.call(this);

    }

    function onDocumentClick(event) {
        if(!this.isVisible()) return;

        if(this._editor && event.target===this._editor.el) return;

        var $cell = $(event.target).closest('td');

        if(_.first($cell)=== _.first(this._editableCell)) {
            return;
        }

        renderCell.call(this, this._editableCell);

        this._editableCell = undefined;

        this._freezeRenderer.call(this);
    }


    function makeCellEditable(cell) {
        var $cell = cell;
        if($cell.data('mode')==='edit') return;

        var index = $cell.attr('data-uw-index');
        var column = this._columns[index];
        var dataIndex = column.dataIndex;
        var locale = column.i18n ? this.locale() : null;
        var $row = $cell.parent();
        var cid = $row.data('cid');
        var value = this.data().get(cid).get(dataIndex, {locale: locale});
        var subtype = column.subtype;

        // preserve cell's style
        $cell.data('padding-top', $cell.css('padding-top'));
        $cell.data('padding-bottom', $cell.css('padding-bottom'));

        $cell.css('padding-top', '4px');
        $cell.css('padding-bottom', '4px');

        var editor = column.editor || buildEditor.call(this, value, column);
        editor.clear();
        editor.attr('cid', cid);
        editor.attr('columnIndex', index);
        editor.css('width', "100%");editor.outerHeight($cell.height());
        editor.css('padding', '0px 6px');
        $cell.empty();
        editor.attach($cell);
        // Set the value of the form control(editor) after attached.
        // Because in Safari, the select element's value will change to the 1'st option after attached.
        editor.value(value);
        this.listenTo(editor, 'change', _.bind(onEditorChange, this));

        this._editableCell = $cell;
        this._editor = editor;
    }

    function buildEditor(value, column) {
        var editor;
        var type = column.type;
        var subtype = column.subtype;

        if(!type) {
            if(subtype==='currency') {
                type = 'number';
            } else if(subtype==='datetime'||subtype==='date'||subtype==='time'||subtype==='month') {
                type = 'date';
            }
        }

        if(_.isNumber(value)||type==='number') {
            var step;
            if(column.step) {
                step = column.step;
            } else {
                var p = precision(value);
                step = p > 0 ? 1/(p*10) : 0;
            }

            editor = new Numberbox({
                type: 'number',
                step: step
            });
        } else if(_.isDate(value)||type==='date') {
            var type;
            if(subtype=='date') {
                type = 'date';
            } else if(subtype=='datetime') {
                type = 'datetime-local';
            } else if(subtype=='time') {
                type = 'time';
            } else {
                type = 'datetime-local';
            }
            editor = new Datebox({
                type: type
            });
        } else {
            editor = new Textbox();
        }

        column.editor = editor;

        return editor;
    }

    function precision(n) {
        if (!isFinite(n)) return 0;
        var e = 1, p = 0;
        while (Math.round(n * e) / e !== n) { e *= 10; p++; }
        return p;
    }

    function onEditorChange(event) {
        var editor = event.context;
        var cid = editor.attr('cid');
        var columnIndex = editor.attr('columnIndex');
        var column = this._columns[columnIndex];
        var dataIndex = column.dataIndex;
        var locale = column.i18n ? this.locale() : null;

        var model = this.data().get(cid);
        model.set(dataIndex, editor.value(), {
            locale: locale
        });
    }

    function renderCell(cell) {
        if(!cell) return;

        if(this._editor) {
            this._editor.detach();
            this.stopListening(this._editor);
            this._editor = undefined;
        }

        var $cell = cell;
        var index = $cell.attr('data-uw-index');
        var column = this._columns[index];
        var dataIndex = column.dataIndex;
        var pattern = column.pattern;
        var step = column.step;
        var type = column.type;
        var locale = column.i18n ? this.locale() : null;
        var subtype = column.subtype;
        var renderer = column.renderer;
        var $row = $cell.parent();
        var cid = $row.data('cid');
        var item = this.data().get(cid);
        var value = item.get(dataIndex, {locale: locale});
        var html = _.isFunction(renderer) ? renderer(item, dataIndex, value) : formatter.format(value, {
            subtype: subtype,
            pattern: pattern
        });

        //restore cell's style
        $cell.css('padding-top', $cell.data('padding-top'));
        $cell.css('padding-bottom', $cell.data('padding-bottom'));
        $cell.data('padding-top', undefined);
        $cell.data('padding-bottom', undefined);

        $cell.html(html);
    }

    /**
     * Toggle a model's selection status.
     *
     * ##### Events:
     * + 'select' - triggered after a model is selected. event.data => the selected model.
     * + 'deselect' - triggered after a model is deselected. event.data => the deselected model.
     *
     * @memberof Grid#
     * @param {Object} item - The model, model.id or model.cid.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @returns {Grid} this
     */
    function toggle(item, options) {
        if(!item) return this;

        if(this._mode===Grid.MODE.NONE) return this;

        options || (options = {});
        var allowNullSelection = this.cfg().allowNullSelection;
        var model = (item instanceof Model) ? item : this.data().get(item);
        var cid = model.cid;

        if(_.contains(this._selection, cid)) {
            (this._selection.length>1 || allowNullSelection) && this.deselect(item, options);
        } else {
            this.select(item, options);
        }

        return this;
    }

    /**
     * Deselect a model.
     *
     * ##### Events:
     * + 'deselect' - triggered after a model is selected. event.data => the deselected model.
     *
     * @memberof Grid#
     * @param {Object} item - The model, model.id or model.cid
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @returns {Grid} this
     */
    function deselect(item, options) {
        if(!item) return this;

        var mode = this._mode;

        if(mode===Grid.MODE.NONE) return this;

        options || (options = {});

        var model = (item instanceof Model) ? item : this.data().get(item);

        if(!model) return this;

        var cid = model.cid;

        if(!_.contains(this._selection, cid)) return this;

        this._selection = _.without(this._selection, cid);

        this.renderSelection();

        if(options.silent) return this;
        var data = [];
        data.push(model);
        var event = {
            context: this,
            source: this,
            data: (this._mode===Grid.MODE.MULTI) ? data : model
        };
        this.trigger('deselect', event);
        return this;

    }

    /**
     * Select a model.
     *
     * ##### Events:
     * + 'select' - triggered after a model is selected. event.data => the selected model.
     *
     * @memberof Grid#
     * @param {Object} item - The model, model.id or model.cid
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @returns {Grid} this
     *
     * @todo select multiple items.
     */
    function select(item, options) {
        if(!item) return this;

        var mode = this._mode;

        if(mode===Grid.MODE.NONE) return this;

        options || (options = {});

        var model = (item instanceof Model) ? item : this.data().get(item);

        if(!model) return this;

        var cid = model.cid;

        if(mode===Grid.MODE.SINGLE) {
            this.deselect(_.first(this._selection));
        }

        this._selection.push(cid);

        this.renderSelection();

        if(options.silent) return this;
        var data = [];
        data.push(model);
        var event = {
            context: this,
            source: this,
            data: (this._mode===Grid.MODE.MULTI) ? data : model
        };
        this.trigger('select', event);
        return this;
    }

    function selectAll(options) {
        options || (options = {});

        var items = this.data();

        this._selection = [];

        items.each(function(item) {
            this._selection.push(item.cid);
        }, this);

        this.renderSelection();

        if(options.silent) return this;
        var event = {
            context: this,
            source: this,
            data: this._data.toArray()
        };
        this.trigger('select', event);
        return this;
    }

    function deselectAll(options) {
        options || (options = {});

        this._selection = [];


        this.renderSelection();

        if(options.silent) return this;
        var event = {
            context: this,
            source: this,
            data: this._data.toArray()
        };
        this.trigger('deselect', event);
        return this;
    }

    /**
     * Reset the grid to deselect all selection.
     *
     * @memberof Grid#
     * @returns {Grid} this
     */
    function reset() {
        this._selection = [];
        this.renderSelection();
        return this;
    }

    /**
     * Get the selected model.
     *
     * @memberof Grid#
     * @returns {Model[]} the array of selected model
     */
    function selection() {
        var items = [];
        var data = this._data;

        _.each(this._selection, function(id) {
            items.push(data.get(id));
        });

        return items;
    }

    function renderSelection() {
        _.each(this.$('tbody>tr'), function(element) {
            var $element = $(element);
            var cid = $element.data('cid');
            var selector = (this._selectorIndex===undefined) ? undefined : $(element).find('>td')[this._selectorIndex];
            var checked = false;
            if(_.contains(this._selection, cid)) {
                $element.addClass('uw-mark');
                checked = true;
            } else {
                $element.removeClass('uw-mark');
                checked = false;
            }

            if(selector) {
                $(selector).children().prop('checked', checked);
            }
        }, this);
    }

    /**
     * Get or set the data.
     *
     * ##### Events:
     * + 'transition' - triggered after a new collection is bound. event.data => the collection.
     *
     * @memberof Grid#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {Boolean} [options.silent=false] - A false value will prevent the events from being triggered.
     * @param {Boolean} [options.render=true] - A false value will prevent the grid from being rendered.
     * @returns {Collection|Grid}
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});

        var defaults = {
            render: true
        };

        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        this._data = items;

        this.listenTo(this._data, "update reset sync", _.bind(onItemsUpdate, this));
        this.listenTo(this._data, "change", _.bind(onItemChange, this));

        this._selection = [];

        options.render && this.render();

        var event = {
            context: this,
            source: this,
            data: items
        };

        (options.silent) || this.trigger('transition', event);

        return this;
    }


    /**
     * Get or set the selection model.
     *
     * @memberof Grid#
     * @param {Grid.MODE} [value] - An value to specify the selection model.
     * ##### Options:
     * + Grid.MODE.SINGLE - set the grid to single selection.
     * + Grid.MODE.MULTI - set the grid to multiple selection.
     * + Grid.MODE.NONE - set the grid to disable selection.
     * @returns {Grid.MODE}
     */
    function mode(value) {
        if(!value) return this._mode;

        this._mode = value.toString().toLowerCase();
        if(this._mode===Grid.MODE.NONE) {
            this.removeClass('uw-selectable');
        } else {
            this.addClass('uw-selectable');
        }
    }

    function locale(locale) {
        if(!locale) return this.attr('locale');

        if(locale===this.locale()) return;

        this.attr('locale', locale);
        this.render();
    }


    function onItemsUpdate(event) {
        this._selection = [];
        this.render();
    }

    function onItemChange(item, options) {
        if(this===options.context) {
            return;
        }

        var editableCell = this._editableCell;
        var editor = this._editor;
        var columns = this._columns;
        var locale = this.locale();

        this._$table.find('tbody tr').each(function() {
            var $tr = $(this);
            if(item.cid!==$tr.data('cid')) return;
            $tr.find('td').each(function(index) {
                var column = columns[index];
                var dataIndex = column.dataIndex;

                if(!dataIndex) return;

                var renderer = column.renderer;
                var value = item.get(dataIndex);

                if(editableCell&&editableCell.get(0)===this) {
                    editor.value(value);
                    return;
                }

                var text = "";
                if(column.reference) {
                    text = translateIntoReference(value, column.reference, column.referenceIndex, locale);
                } else if(column.code) {
                    text = translateIntoCode(value, column.code, locale);
                } else if(_.isFunction(renderer)) {
                    text = renderer(item, dataIndex, value);
                } else {
                    text = formatter.format(item.get(dataIndex, {locale: locale}), {
                        subtype: column.subtype,
                        pattern: column.pattern
                    });
                }
                this.innerHTML = text;
            });
        });
    }


    function addHandler(handler, column, context) {
        this._columns[column].handler = _.bind(handler, context);
    }

    function addRenderer(renderer, column, context) {
        this._columns[column].renderer = _.bind(renderer, context);
    }


    var props = {
        tagName: 'table',
        className: 'table table-striped',
        _data: undefined,
        _columns: undefined,
        _mode: undefined,
        _selection: undefined,
        _editableCell: undefined,
        _selectorIndex: undefined,
        _$table: undefined,
        _$wrapper: undefined,
        _$scroller: undefined,
        _editor: undefined
    };

    var Grid = declare(Base, {
        initialize: initialize,
        render: render,
        select: select,
        selectAll: selectAll,
        deselect: deselect,
        deselectAll: deselectAll,
        toggle: toggle,
        reset: reset,
        data: data,
        mode: mode,
        locale: locale,
        selection: selection,
        renderSelection: renderSelection,
        addHandler: addHandler,
        addRenderer: addRenderer
    }, props);

    // Enumeration of the selection model
    Grid.MODE = {
        SINGLE: 'single',
        MULTI: 'multi',
        NONE: 'none'
    };

    Grid.HANDLER = {
        SELECTOR: 'SELECTOR'
    };

    return Grid;
});