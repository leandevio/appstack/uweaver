/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang',
    'uweaver/widget/Widget', 'uweaver/widget/Tooltips', 'uweaver/browser', 'uweaver/exception/Violation',
    'uweaver/exception/UnsupportedTypeException'], function ($, _, lang, Widget, Tooltips, browser, Violation, UnsupportedTypeException){
    var declare = lang.declare;
    var Base = Widget;

    var SUPPORTEDTYPES = ['text', 'password', 'textarea'];
    var SUPPORTEDDATATYPES = ['text'];
    var VALIDATIONERRORS = {
        'valueMissing': null,
        'typeMismatch': 'type',
        'patternMismatch': 'pattern',
        'tooLong': 'maxLength',
        'tooShort': 'minLength',
        'rangeUnderFlow': 'min',
        'rangeOverflow': 'max',
        'stepMismatch': 'step',
        'badInput': null};

    /**
     * A Inputbox is a widget for ....
     *
     * ##### Events:
     * + 'change' - [value()]{@link Inputbox#value}
     *
     * @constructor Inputbox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value] - Specifies the value of an <input> element.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     * @param {Integer} [config.max] - Specifies the maximum value for an <input> element.
     * @param {Integer} [config.min] - Specifies a minimum value for an <input> element.
     * @param {Integer} [config.step=1] - Specifies the legal number intervals for an input field.
     * @param {Integer} [config.maxLength=524288] - Specifies the maximum number of characters allowed in the <input> element.
     * @param {Integer} [config.minLength=0] - Specifies the minimum number of characters allowed in the <input> element.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        this._anchors = {};

        var tooltips = new Tooltips({
            target: this
        }).render();

        this._tooltips = tooltips;

        var $input = this.$el;

        $input.on('change', _.bind(onChange, this));

        (cfg.type!==undefined) && this.type(cfg.type);
        (cfg.dataType!==undefined) && this.dataType(this, cfg.dataType);
        (cfg.dataPattern!==undefined) && this.dataPattern(this, cfg.dataPattern);
        (cfg.readonly!==undefined) && this.readonly(cfg.readonly);
        (cfg.required!==undefined) && this.required(cfg.required);
        (cfg.disabled!==undefined) && this.disabled(cfg.disabled);
        (cfg.max!==undefined) && this.max(cfg.max);
        (cfg.min!==undefined) && this.min(cfg.min);
        (cfg.step!==undefined) && this.step(cfg.step);
        (cfg.maxLength!==undefined) && this.maxLength(cfg.maxLength);
        (cfg.minLength!==undefined) && this.minLength(cfg.minLength);
        
        this.type(this.type()); this.dataType(this.dataType());

        this.clearValidation();
    }
    
    function onChange(event) {
        this.validate() && this.change();
    }

    /**
     * Get or set the value.
     *
     * @param {String} value
     * @returns {*|undefined}
     */
    function value(value, options) {
        options || (options = {});
        var $input = this.$el;
        var text = $input.val();
        
        var currentValue = this.parse(text);

        if(arguments.length===0) return currentValue;

        (value===undefined) && (value=null);

        if(value===currentValue) return;

        $input.val(this.format(value));

        this.validate() && (options.silent || this.change());
    }
    
    function clear() {
        var $input = this.$el;
        $input.val(null);
        this.clearValidation();
    }

    /**
     * 
     * Format the the value and return a text to display in the inputbox.
     *
     * @memberof Inputbox#
     * @protected
     * @param value
     * @returns {*}
     */
    function format(value) {
        return value;
    }

    /**
     * Parse the text of the inputbox and return a value.
     *
     * @memberof Inputbox#
     * @protected
     */
    function parse(text) {
        var value;

        if(text===undefined||text.trim()==='') {
            value = null;
        } else {
            value = text;
        }

        return value;
    }


    /**
     * Validate the value of the inputbox.
     */
    function validate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $input = this.$el;
        var validity = $input.prop('validity');
        var valid = true;

        this.clearValidation();

        (valid && _.isObject(validity)) && (valid = this.checkValidity(options));

        (valid && this.useCustomType()) && (valid = this.customValidate(options));

        return valid;
    }

    function change() {
        this.trigger('change', {
            data: this.value(),
            context: this,
            source: this
        });
    }

    function checkValidity(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $input = this.$el;
        var validity = $input.prop('validity');

        if(!validity.valid) {
            _.each(VALIDATIONERRORS, function(prop, error) {
                if(_.contains(options.ignore, error.toUpperCase())) return;
                if(validity[error]) {
                    var parameters = [];
                    if(prop) {
                        parameters.push($input.prop(prop)||$input.attr(prop));
                    }
                    this.violate(new Violation({
                        properties: [this.name()],
                        error: error.toUpperCase(),
                        parameters: parameters
                    }));
                }
            }, this);

            var validationMessage = "";
            _.each(this._violations, function(violation) {
                validationMessage = validationMessage + '\n' + violation.toString();
            });
            $input.get(0).setCustomValidity(validationMessage);
        }

        return (this._violations.length===0);
    }

    function customValidate(options) {
        return true;
    }

    function name(name) {
        var $input = this.$el;

        if(!name) return $input.attr('name');

        $input.attr('name', name);
    }

    function type(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('type');
        if(value && !_.contains(this.SUPPORTEDTYPES, value)) {
            throw new UnsupportedTypeException({
                type: value,
                supportedTypes: this.SUPPORTEDTYPES
            });
        }
        $input.attr('type', value);
    }
    
    function dataType(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-type');
        if(value && !_.contains(this.SUPPORTEDDATATYPES, value)) {
            throw new UnsupportedTypeException({
                type: value,
                supportedTypes: this.SUPPORTEDDATATYPES
            });
        }
        $input.attr('data-type', value);
    }

    function dataPattern(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-pattern');
        $input.attr('data-pattern', value);
    }

    function useCustomType() {
        return ((!this.type()||this.type()==='')&&this.dataType());
    }

    function placeholder(hint) {
        var $input = this.$el;

        if(!hint) return $input.attr('placeholder');

        $input.attr('placeholder', hint);
    }

    function clearValidation() {
        this._violations = [];
        this._tooltips.content("");
        this.removeClass('uw-border-warn');
    }

    function readonly(value) {
        if(arguments.length===0) return this.el.hasAttribute('readonly');

        value ? this.el.setAttribute('readonly', null) : this.el.removeAttribute('readonly');
    }

    function required(value) {
        if(arguments.length===0) return this.el.hasAttribute('required');

        value ? this.el.setAttribute('required', null) : this.el.removeAttribute('required');
    }

    function disabled(value) {
        if(arguments.length===0) return this.el.hasAttribute('disabled');

        value ? this.el.setAttribute('disabled', null) : this.el.removeAttribute('disabled');
    }

    function max(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.prop('max');
        $input.prop('max', value);
    }

    function min(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.prop('min');
        $input.prop('min', value);
    }


    function step(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.prop('step');
        $input.prop('step', value);
    }

    function violate(violation) {

        this._violations.push(violation);

        this.hasClass('uw-border-warn') || this.addClass('uw-border-warn');

        this._tooltips.append(violation.toString() + "<br>");

        return violation;
    }

    function violations() {
        return this._violations;
    }

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value.toUpperCase();

        this.readonlyState() && this.readonly(_.contains(this.readonlyState().toUpperCase().split(","), this._state));
        this.disabledState() && this.disabled(_.contains(this.disabledState().toUpperCase().split(","), this._state));
        this.hiddenState() && _.contains(this.hiddenState().toUpperCase().split(","), this._state) ? this.hide() : this.show();
    }

    function readonlyState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-readonly-state');
        $input.attr('data-readonly-state', value);
    }

    function disabledState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-disabled-state');
        $input.attr('data-disabled-state', value);
    }

    function hiddenState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-hidden-state');
        $input.attr('data-hidden-state', value);
    }

    var props = {
        /**
         * @override
         */
        tagName: 'input',

        _violations: undefined,
        _tooltips: undefined,
        _state: undefined,

        SUPPORTEDTYPES: SUPPORTEDTYPES,
        SUPPORTEDDATATYPES: SUPPORTEDDATATYPES

    };

    var Inputbox = declare(Base, {
        initialize: initialize,
        value: value,
        clear: clear,
        format: format,
        parse: parse,
        change: change,
        name: name,
        type: type,
        dataType: dataType,
        dataPattern: dataPattern,
        useCustomType: useCustomType,
        placeholder: placeholder,
        validate: validate,
        checkValidity: checkValidity,
        customValidate: customValidate,
        violate: violate,
        violations: violations,
        clearValidation: clearValidation,
        readonly: readonly,
        required: required,
        disabled: disabled,
        max: max,
        min: min,
        step: step,
        state: state,
        readonlyState: readonlyState,
        disabledState: disabledState,
        hiddenState: hiddenState
    }, props);

    return Inputbox;


});