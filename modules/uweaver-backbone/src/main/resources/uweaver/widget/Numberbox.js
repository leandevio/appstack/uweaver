/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Inputbox', 'uweaver/browser',
    'uweaver/number'], function ($, _, lang, Inputbox, browser, number){
    var declare = lang.declare;
    var Base = Inputbox;

    var SUPPORTEDTYPES = ['number'];
    var SUPPORTEDDATATYPES = ['number'];

    /**
     * A Numberbox is a widget for ....
     **
     * ##### JavaScript
     * ```javascript
     * var numberbox = new Numberbox({
     *     el: $('[name=price]')
     * });
     * numberbox.render();
     *
     * numberbox.value(2.4);
     *
     * expect(numberbox.value()).to.equal(2.4);
     * ```
     * ##### HTML
     * + selector: input[type=number].
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <input type="number" name="price">
     * ...
     * </form>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Numberbox#value}
     *
     * @constructor Numberbox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value] - Specifies the value of an <input> element.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     * @param {Integer} [config.max] - Specifies the maximum value for an <input> element.
     * @param {Integer} [config.min] - Specifies a minimum value for an <input> element.
     * @param {Integer} [config.step=1] - Specifies the legal number intervals for an input field.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
    }

    function format(value) {
        var text;

        if(this.useCustomType()) {
            text = number.format(value, this.dataPattern());
        } else {
            text = value;
        }

        return text;
    }

    /**
     * Parse the text to a value.
     *
     */
    function parse(text) {
        var value;

        if(text===null||text===undefined||text.trim()==='') {
            value = null;
        } else if(this.useCustomType()) {
            value = number.parse(text, this.dataPattern());
        } else {
            value = Number(text);
        }

        return value;
    }


    function customValidate() {
        var $input = this.$el;
        if(number.test($input.val())===0) {
            this.violate('type');
            return false;
        }
        return true;
    }


    function useCustomType() {
        return (!this.type()||this.type()===''||!browser.inputtypes.number);
    }


    var props = {
        className: 'form-control',
        SUPPORTEDTYPES: SUPPORTEDTYPES,
        SUPPORTEDDATATYPES: SUPPORTEDDATATYPES
    };

    var Numberbox = declare(Base, {
        initialize: initialize,
        format: format,
        parse: parse,
        useCustomType: useCustomType,
        customValidate: customValidate
    }, props);

    return Numberbox;


});