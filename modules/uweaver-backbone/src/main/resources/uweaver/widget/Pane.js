/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2018 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/widget/Widget'], function(_, lang, Widget) {

    var declare = lang.declare;
    var Base = Widget;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        this._heading = this.el.querySelector("div.panel-heading");
        this._heading.classList.add("uw-clickable");

        this._heading && this._heading.addEventListener("click", _.bind(onClick, this));

        this._body = this.el.querySelector("div.panel-body");

        config.collapse && (this.collapse());
    }

    function render() {

    }

    function onClick(event) {
        this.toggle();
    }

    function collapse() {
        this._body.style.display = 'none';
    }

    function toggle() {
        this.isCollapsed() ? this.expand() : this.collapse();
    }

    function expand() {
        this._body.style.display = '';
    }

    function isCollapsed() {
        return (this._body.style.display==='none');
    }

    var props = {
        _heading: undefined,
        _body: undefined
    };


    var Pane = declare(Base, {
        initialize: initialize,
        render: render,
        collapse: collapse,
        expand: expand,
        toggle: toggle,
        isCollapsed: isCollapsed
    }, props);

    return Pane;
});

