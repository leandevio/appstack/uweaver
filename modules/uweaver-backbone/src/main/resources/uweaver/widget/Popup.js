/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define(['underscore', 'jquery', 'uweaver/lang', 'uweaver/dom', 'uweaver/i18n', 'uweaver/Deferred',  'uweaver/widget/Widget', 'uweaver/XElement',
    'uweaver/widget/Container', 'uweaver/widget/ProgressBar',
    'uweaver/util/UIMLResourceBundle', 'text!./Popup.html'],
    function (_, $, lang, dom, i18n, Deferred, Widget, XElement, Container, ProgressBar,
              UIMLResourceBundle, uiml) {

    var resource = new UIMLResourceBundle(uiml);
    var declare = lang.declare;
    var Base = Container;
    var zIndex = 0;
    // width:(height+120) = 16:9
    var PANELBODYFOOTERDIMENSION = "min-width: 320px;  min-height: 60px;";
    // width:(height+60) = 16:9
    var PANELBODYDIMENSION = "min-width: 320px; min-height: 120px;";

    /**
     * A Popup is a overlay container positioned within the viewport.
     *
     * @constructor Popup
     * @extends Container
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the popup.
     * @param {String} [config.tpl] - A html string to layout & construct the DOM element.
     * @param {Boolean} [config.modal=false] - A true value to display a modal popup.
     * @param {Boolean} [config.dismissable=true] - A true value to close the popup when clicking outside the popup or pressing Escape.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            dismissable: true,
            modal: false
        };

        var anchors = this._anchors;

        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        cfg.modal && (cfg.dismissable = false);

        var z = ++zIndex * 10;

        this.css('z-index', z);

        anchors.$content = this.$el;

        this.css('overflow', 'auto');
        this.el.style.position = 'fixed';

        if(cfg.dismissable) {
            // add touchstart to support touch device.
            $(document).on('click touchstart', _.bind(onDocumentClick, this));
        }

        if(cfg.modal) {
            this._backpanel = $("<div class='uw-background-gray' style='position:fixed;opacity:0.7'></div>");
            this._backpanel.css({left:0, top:0, 'z-index': z-1});
            this._backpanel.on('click touchstart', function(event){event.stopPropagation();});
            $('body').append(this._backpanel);
            this._backpanel.hide();
        }
    }

    function onDocumentClick(event) {
        if(this._skipDocumentClick) {
            this._skipDocumentClick = false;
            return;
        }

        if(!this.isVisible()) return;

        var selector = '#' + this.prop('id');

        if($(event.target).closest(selector).size()>0) return;

        this.hide();
    }

    /**
     * @override
     */
    function render(options) {
        Base.prototype.render.apply(this, arguments);

        this.hide();

        var defaults = {
            hidden: true
        };

        var anchors = this._anchors;
        var cfg = this._cfg;

        options || (options = {});
        _.defaults(options, defaults);

        cfg.el || this.attach($('body'));

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function show(options) {
        if(_.isEmpty(this.$el.html())) return;
        options || (options = {});
        var position;
        var cfg = this._cfg;

        if(cfg.modal) {
            this._backpanel.width($(window).width()); this._backpanel.height($(window).height());
            this._backpanel.show();
        }

        if(options.position) {
            position = options.position;
        } else if(options.positionTo) {
            position = translatePosition.call(this, options.positionTo);
        } else if(!this._positioned) {
            this.css('opacity', '0');
            _.delay(_.bind(_moveToCenterOfWindow, this), 300);
        }

        position && this.moveTo(position) && (this._positioned = true);

        this._skipDocumentClick = true;

        Base.prototype.show.apply(this, arguments);
    }

    function _moveToCenterOfWindow() {
        var content = dom.select(this.el.firstElementChild);
        this.css('opacity', '1');
        var position = {
            left: ($(window).outerWidth() - content.width()) * 0.5,
            top: ($(window).outerHeight() - content.height()) * 0.5
        };
        this.css('opacity', '0');
        this.moveTo(position) && (this._positioned = true);
        this.css('opacity', '1');
    }

    function hide() {
        var cfg = this._cfg;

        if(cfg.modal) {
            this._backpanel.hide();
        }

        Base.prototype.hide.apply(this, arguments);
    }

    function translatePosition(component) {
        var position;
        var $node;

        if(component instanceof jQuery) {
            $node = component;
        } else if(component instanceof Widget) {
            $node = component.$el;
        } else if(component instanceof XElement) {
            $node = $(component.node());
        } else {
            $node = $(component);
        }

        position = {
            left: $node.offset().left + ($node.outerWidth() - this.width()) * 0.5,
            top: $node.offset().top + $node.outerHeight()
        };
        position = {left: Math.max(position.left, 0), top: Math.max(position.top, 0)};
        position.left = Math.min(position.left, $(window).outerWidth()-this.width() - 10);

        return position;
    }

    function toggle(options) {
        this.isVisible() ? this.hide() : this.show(options);
    }

    var props = {
        _skipDocumentClick: false,
        _positioned: false,
        _backpanel: undefined
    };

    var Popup = declare(Base, {
        initialize: initialize,
        render: render,
        show: show,
        hide: hide,
        toggle: toggle
    }, props);

    function renderMessagebox(popup, message, title) {
        var el = popup.el;
        el.querySelector('.panel-heading>span').innerHTML = i18n.translate(title);
        var content = el.querySelector('.panel-body>[class*="text-"]');
        content.style.cssText = el.querySelector('.panel-body>div:nth-child(2)') ? PANELBODYFOOTERDIMENSION : PANELBODYDIMENSION;
        var text;
        if(_.isArray(message)) {
            text = _.reduce(message, function(sum, section){return sum + i18n.translate(section.toString()) + "<br>"}, "");
        } else {
            text = i18n.translate(message);
        }
        content.innerHTML = text;
        var close = popup.el.querySelector('[action=close]');
        close && close.addEventListener('click', function() {
            popup.hide();
            popup.destroy();
        });

        var spans = popup.el.querySelectorAll('button span');
        _.each(spans, function(span) {
            span.innerText = i18n.translate(span.innerText);
        });

        popup.render();
    }

        function renderDialog(popup, widget, title) {
            var el = popup.el;
            el.querySelector('.panel-heading>span').innerHTML = i18n.translate(title);
            var content = el.querySelector('.panel-body>[class*="text-"]');
            content.style.cssText = el.querySelector('.panel-body>div:nth-child(2)') ? PANELBODYFOOTERDIMENSION : PANELBODYDIMENSION;

            content.appendChild(widget.el);
            var close = popup.el.querySelector('[action=close]');
            close && close.addEventListener('click', function() {
                popup.hide();
                popup.destroy();
            });

            var spans = popup.el.querySelectorAll('button span');
            _.each(spans, function(span) {
                span.innerText = i18n.translate(span.innerText);
            });

            popup.render();
        }

    // give user an alert when something wrong
    Popup.alert = function(message, title) {
        title || (title = 'Alert');
        var popup = new Popup({modal: true});
        popup.el.innerHTML = resource.get('alert');
        renderMessagebox(popup, message, title);
        popup.show();
    };

    // give user a warning when
    Popup.warn = function(message, title) {
        title || (title = 'Warning');
        var popup = new Popup({modal: true});
        popup.el.innerHTML = resource.get('warn');
        renderMessagebox(popup, message, title);
        popup.show();
    };

    //
    Popup.info = function(message, title) {
        title || (title = 'Information');
        var popup = new Popup({dismissable:false});
        popup.el.innerHTML = resource.get('info');
        popup.el.addEventListener('click', function() {
            popup.hide();
            popup.destroy();
        });
        renderMessagebox(popup, message, title);
        popup.show();
        _.delay(function() {
            popup.hide();
            popup.destroy();
        }, 3000);
    };

    //
    Popup.notify = function(message, title) {
        title || (title = 'Notification');
        var popup = new Popup();
        popup.el.innerHTML = resource.get('notify');
        renderMessagebox(popup, message, title);
        popup.show();
    };

    Popup.confirm = function(message, title) {
        title || (title = 'Confirmation');
        var popup = new Popup();
        popup.el.innerHTML = resource.get('confirm');
        renderMessagebox(popup, message, title);
        var deferred = new Deferred();
        _.each(popup.el.querySelectorAll('button'), function(el) {
           el.addEventListener('click', function(event) {
               popup.hide();
               popup.destroy();
               var action = event.currentTarget.getAttribute('value');
               _.defer(function() {
                   if(action==='ok') {
                       deferred.resolve();
                   } else {
                       deferred.reject();
                   }
               });
           });
        });
        popup.show();

        return deferred.promise();
    };

    Popup.prompt = function(widget, title) {
        title || (title = 'Prompt');
        var popup = new Popup();
        popup.el.innerHTML = resource.get('prompt');
        renderDialog(popup, widget, title);
        var deferred = new Deferred();
        _.each(popup.el.querySelectorAll('button'), function(el) {
            el.addEventListener('click', function(event) {
                if(event.currentTarget.getAttribute('value')==='ok') {
                    if(widget.validate && !widget.validate()) {
                        // display a warning.
                        return;
                    }
                    popup.hide();
                    popup.destroy();
                    _.defer(function() {
                        var data;
                        if(widget.selection) {
                            data = widget.selection();
                            if(data.size()===0) {
                                // display a warning.
                                return;
                            }
                        } else if(widget.data) {
                            data = widget.data();
                            if(data==undefined) {
                                // display a warning.
                                return;
                            }
                        }
                        deferred.resolve(data);
                    });
                } else {
                    popup.hide();
                    popup.destroy();
                    _.defer(function() {
                        deferred.reject();
                    });
                }
            });
        });
        popup.show();
        return deferred.promise();
    };

    Popup.progress = function(progress, wait, context) {
        progress || ((progress=indetermine) && (wait=0.3));
        wait || (wait=3);
        context || (context=this);
        var popup = new Popup({dismissable: false}).render();
        popup.el.innerHTML = resource.get('progress');
        var body = popup.el.querySelector('.panel-body');
        var progressBar = new ProgressBar({el: body}).render();
        popup.show();

        _.defer(complete);

        function complete() {
            if(!popup.isVisible()) {
                popup.destroy();
                return;
            }
            if(progressBar.value()>=1) {
                popup.hide();
                popup.destroy();
            }
            progressBar.complete(progress.call(context, progressBar.value()));
            _.delay(complete, (progressBar.value()>=1) ? 2000 : wait * 1000);
        }

        function indetermine(value) {
            var progress = value * 100;
            var increment = (progress>=90 ? 2 : 10);
            progress = (progress>=98) ? 0 : (progress + increment);
            return progress/100;
        }

        return popup;
    };

    return Popup;
});