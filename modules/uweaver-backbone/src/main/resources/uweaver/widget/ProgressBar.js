/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2018 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/widget/Widget',
        'uweaver/util/UIMLResourceBundle', 'text!./ProgressBar.html'],
    function (_, lang, Widget,
              UIMLResourceBundle, uiml) {

    var resource = new UIMLResourceBundle(uiml);
    var declare = lang.declare;
    var Base = Widget;

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this.hide();

        var html = resource.get('bar');

        this.hasClass('progress') || this.$el.html(html);
    }

    function render() {
        this.hide();
        var bar = this.el.querySelector('.progress-bar');
        var width = this._value * 100;
        bar.style.width = width + '%';
        var span = bar.children[0];
        span && (span.textContent = width + "% Complete");
        this.show();
        return this;
    }

    function complete(progress, wait, context) {
        progress || (progress=1);
        this._value = progress;
        this.render();
    }

    function determine(progress, wait, context) {
        wait || (wait=0.5);
        context || (context=this);
        this.complete(progress.call(context, this.value()));
        if(this.value()<1) {
            _.delay(_.bind(determine, this), wait * 1000, progress, wait, context);
        } else {
            _.delay(_.bind(progress, context), 2000, this.value());
        }
    }

    function indetermine() {
        _indetermine.call(this);
    }

    function _indetermine() {
        var wait = 500;
        var progress = this._value;
        var increment = (progress>=0.8 ? 0.001 : 0.005);
        progress = progress + increment;
        this.complete(progress);
        (progress<=0.95 && _.delay(_.bind(_indetermine, this), wait));
        return progress;
    }

    function reset() {
        this._value = 0;
    }

    function value() {
        return this._value;
    }

    var props = {
        _value: 0
    };

    var Progress = declare(Base, {
        initialize: initialize,
        render: render,
        complete: complete,
        determine: determine,
        indetermine: indetermine,
        reset: reset,
        value: value
    }, props);

    return Progress;
});