/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Inputbox'], function ($, _, lang, Inputbox){
    var declare = lang.declare;
    var Base = Inputbox;
    var SUPPORTEDTYPES = ['radio'];
    var SUPPORTEDDATATYPES = [];

    /**
     * A Checkbox is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * var radiobutton = new Radiobutton({
     *     el: $('[name=delivery]')
     * });
     * radiobutton.render();
     *
     * radiobutton.value(true);
     *
     * expect(radiobutton.value()).to.equal(true);
     * ```
     * ##### HTML
     * + input[type=radio]: required.
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <label class="control-label col-sm-1 gutter">Delivery</label>
     *  <div class="col-sm-3 gutter">
     *      <div class="radio">
     *          <label>
     *              <input type="radio" name="delivery" value="inhouse">
     *              In-House
     *          </label>
     *      </div>
     *      <div class="radio">
     *          <label>
     *              <input type="radio" name="delivery" value="outsource">
     *              Outsource
     *          </label>
     *      </div>
     *  </div>
     * ...
     * </form>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Radiobutton#value}

     *
     * @constructor Radiobutton
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value] - Specifies the value of an <input> element.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
    }

    /**
     * Get or set the value.
     *
     * @param {String} value
     * @returns {*|undefined}
     */
    function value(value, options) {
        options || (options = {});
        var $input = this.$el;
        var checked = $input.prop('checked');

        var currentValue = this.parse(checked);

        if(arguments.length===0) return currentValue;

        (value===undefined) && (value=null);

        if(this.format(value)===checked) return;

        $input.prop('checked', this.format(value));

        this.validate() && (options.silent || this.change());
    }

    function clear() {
        var $input = this.$el;
        $input.prop('checked') && $input.prop('checked', false);
        this.clearValidation();
    }

    function format(value) {
        var $input = this.$el;
        var checked;

        checked = (value!==null && value.toString()===$input.attr('value'));

        return checked;
    }

    /**
     * Parse the text of the inputbox to a value.
     *
     */
    function parse(checked) {
        var $input = this.$el;
        var text = $input.attr('value');
        var value;

        if(text.length==0) return null;

        value = checked ? text : null;

        if(value==='true') {
            value = true;
        } else if(value==='false') {
            value = false;
        }

        return value;
    }

    function checked(value) {
        if(value===undefined) return this.el.checked;

        this.el.checked = value;
    }


    var props = {
        SUPPORTEDTYPES: SUPPORTEDTYPES,
        SUPPORTEDDATATYPES: SUPPORTEDDATATYPES
    };

    var Radiobutton = declare(Base, {
        initialize: initialize,
        value: value,
        clear: clear,
        format: format,
        parse: parse,
        checked: checked
    }, props);

    return Radiobutton;


});