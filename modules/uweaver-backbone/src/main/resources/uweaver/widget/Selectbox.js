/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Widget', 'uweaver/widget/Tooltips', 'uweaver/browser',
    'uweaver/exception/Violation', 'uweaver/data/Collection', 'uweaver/data/Model', 'uweaver/data/reference', 'uweaver/data/code',
    'uweaver/formatter', 'uweaver/i18n'], function($, _, lang, Widget, Tooltips, browser, Violation, Collection, Model, reference, code, formatter, i18n) {
    var declare = lang.declare;
    var Base = Widget;
    var LIMIT = 50;
    var VALIDATIONERRORS = {
        'valueMissing': null,
        'typeMismatch': 'type',
        'patternMismatch': 'pattern',
        'tooLong': 'maxLength',
        'tooShort': 'minLength',
        'rangeUnderFlow': 'min',
        'rangeOverflow': 'max',
        'stepMismatch': 'step',
        'badInput': null};


    /**
     * A Selectbox .....
     *
     * ##### JavaScript
     * ```javascript
     * var Countries = declare(Collection, {
     *      resource: 'countries'
     * });
     * var countries = new Countries();
     *
     * var country = new Selectbox({
     *      el: $('[name=country]')
     * }).render();
     *
     * country.data(countries);
     *
     * country.valueField('id');
     * country.displayField('name');
     *
     * country.value("taiwan");
     *
     * expect(country.value()).to.eq("taiwan");
     *
     * ```
     * ##### HTML
     * + div: required.
     *
     * ```html
     * <select name="taiwan" class="form-control">
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Selectbox#value}, [values()]{@link Selectbox#values}
     *
     * @constructor Selectbox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {Collection} [config.data=new Collection()] - The collection to provide selection options data.
     * @param {String} [config.valueField='value'] - Specifies the value field.
     * @param {String} [config.displayField='title'] - Specifies the display field.
     * @param {Boolean} [config.multiple=false] - specifies that multiple options can be selected at once.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     * @param {Boolean} [config.maxLength] - Specifies the maximum number of selections allowed.
     * @param {Boolean} [config.minLength] - Specifies the minimum number of selections required.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};

        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        this._anchors = {};

        var tooltips = new Tooltips({
            target: this
        }).render();

        this._tooltips = tooltips;

        var $select = this.$el;

        $select.on('change', _.bind(onChange, this));

        this._values = [];
        this.reference(this.reference());
        this.code(this.code());
        this.valueField() || this.valueField("value");
        this.displayField() || this.displayField("title");

        (cfg.readonly!==undefined) && this.readonly(cfg.readonly);
        (cfg.required!==undefined) && this.required(cfg.required);
        (cfg.disabled!==undefined) && this.disabled(cfg.disabled);
        (cfg.maxLength!==undefined) && this.maxLength(cfg.maxLength);
        (cfg.minLength!==undefined) && this.minLength(cfg.minLength);
        (cfg.valueField!==undefined) && this.valueField(cfg.valueField);
        (cfg.displayField!==undefined) && this.displayField(cfg.displayField);

        // synchronize the readonly & disabled
        this.readonly(this.readonly());

        var clear = '<i class="fa fa-close uw-clickable" style="position:absolute;top: 10px;right: 32px;color:lightslategray;"></i>';
        this.$el.wrap('<span></span>');
        this.$el.parent().prepend(clear);
        this._$clear = this.$el.parent().find('i');
        if(browser.isMSIE()) {
            this._$clear.css('right', '54px')
        } else if(browser.isFirefox()) {
            this._$clear.css('right', '36px')
        }
        this._$clear.hide();
        this._$clear.on('click', _.bind(onClearClick, this));

        (cfg.reference!==undefined) && this.reference(cfg.reference);
        (cfg.code!==undefined) && this.code(cfg.code);

        this.clearValidation();

        var referenceData;
        if(this.reference()) {
            referenceData = reference.get(this.reference());
        } else if(this.code()) {
            referenceData = code.get(this.code());
        }

        if(referenceData) {
            this.data(referenceData);
        } else if(cfg.data) {
            this.data(cfg.data);
        } else {
            this.data(new Collection());
        }
    }

    function onClearClick() {
        this.value(null);
        this._$clear.hide();
    }

    function render(options) {

        var defaults = {
            hidden: false
        };
        options || (options = {});
        _.defaults(options, defaults);

        var value = this.el.value;
        var locale = i18n.locale();

        this.$el.empty();
        this._data.each(function(datum) {
            var value = datum.get(this.valueField());
            var text = datum.get(this.displayField(), {locale: locale});
            var $option = $("<option></option>");

            $option.attr('value', formatter.format(value)).text(text);
            $option.attr('cid', datum.cid);
            this.$el.append($option)
        }, this);

        //Fix some browser make the first option selected after options appended.
        this.el.value = value;

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        _.delay(_.bind(renderClearButton, this), 500);

        //Fix some browser(such as IE 10-) make the first option selected after rendered.
        browser.isMSIE() && _.defer(_.bind(SimulateChangeEvent, this));

        return this;
    }

    /**
     * Fix some browser(such as IE 10-) make the first option selected after rendering.
     * @param value
     */
    function SimulateChangeEvent(value) {
        var event;
        if(typeof(Event) === 'function') {
            event = new Event('change', {
                bubbles: true
            });
        } else {
            event = document.createEvent('Event');
            event.initEvent('change', true, true);
        }
        this.el.dispatchEvent(event);
    }

    function onChange(event) {
        renderClearButton.call(this);
        this.validate() && this.change();
    }

    function renderClearButton() {
        var $select = this.$el;
        this._$clear.hide();
        if(!this.readonly() && !this.disabled() && $select.val()!=null) {
            // var top = (this.$el.outerHeight() - this._$clear.outerHeight())/2;
            // var left = this.$el.outerWidth() - this._$clear.outerWidth() - 10;
            // this._$clear.css({
            //     top: Math.floor(top) + 'px',
            //     left: Math.floor(left) + 'px'
            // });
            this._$clear.show();
        }
    }

    function value(value, options) {
        options || (options = {});

        var $select = this.$el;
        var selectedIndex = this.el.selectedIndex;
        var currentValue;
        var valueField = this.valueField();

        if(selectedIndex===-1) {
            currentValue = null;
        } else {
            var option = this.el.options[selectedIndex];
            if(option.hasAttribute('cid')) {
                currentValue = nullify(this._data.get(option.getAttribute('cid')).get(valueField));
            } else {
                currentValue = this.parse(option.value);
            }
        }

        if(arguments.length===0) return currentValue;

        (value===undefined) && (value=null);

        if(value===currentValue) return;

        $select.val(this.format(value));

        this.validate() && (options.silent || this.change());
    }

    function nullify(value) {
        return (value===null||value===undefined) ? null : value;
    }

    function clear() {
        var $select = this.$el;
        $select.val(null);
        this.clearValidation();
    }

    /**
     *
     * Format the the value and return a text to display in the selectbox.
     *
     * @memberof Selectbox#
     * @protected
     * @param value
     * @returns {*}
     */
    function format(value) {
        return formatter.format(value);
    }

    /**
     * Parse the text of the selectbox and return a value.
     *
     * @memberof Selectbox#
     * @protected
     */
    function parse(text) {
        var value;

        if(text===null||text===undefined||text.trim()==='') {
            value = null;
        } else {
            value = text;
        }

        return value;
    }


    /**
     * Validate the value of the inputbox.
     */
    function validate(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $select = this.$el;
        var validity = $select.prop('validity');
        var valid = true;

        this.clearValidation();

        (valid && _.isObject(validity)) && (valid = this.checkValidity(options));

        valid && (valid = this.customValidate(options));

        return valid;
    }

    function change() {
        this.trigger('change', {
            data: this.value(),
            context: this,
            source: this
        });
    }

    function checkValidity(options) {
        var defaults = {
            ignore: []
        };
        options || (options = {});
        _.defaults(options, defaults);
        _.isArray(options.ignore) || (options.ignore = [options.ignore]);

        var $select = this.$el;
        var validity = $select.prop('validity');

        if(!validity.valid) {
            _.each(VALIDATIONERRORS, function(prop, error) {
                if(_.contains(options.ignore, error.toUpperCase())) return;

                if(error==='valueMissing' && this.value()!==null) return;
                if(validity[error]) {
                    var parameters = [];
                    if(prop) {
                        parameters.push($select.prop(prop)||$select.attr(prop));
                    }
                    this.violate({
                        error: error.toUpperCase(),
                        parameters: parameters
                    });
                }
            }, this);

            var validationMessage = "";
            _.each(this._violations, function(violation) {
                validationMessage = validationMessage + '\n' + violation.toString();
            });
            $select.get(0).setCustomValidity(validationMessage);
        }

        return (this._violations.length===0);
    }

    function customValidate(options) {
        return true;
    }

    /**
     * Get or set the data.
     *
     * @memberof Selectbox#
     * @param {Collection} items - the data.
     * @param {Object} [options] - A map of additional options to pass to the method.
     * @param {String} [config.valueField='value'] - Specifies the value field.
     * @param {String} [config.displayField='title'] - Specifies the display field.
     * @returns {Collection|Selectbox}
     */
    function data(items, options) {
        if(!items) return this._data;

        options || (options = {});
        var defaults = {};
        _.defaults(options, defaults);

        if(this._data) this.stopListening(this._data);

        if(items instanceof Collection) {
            this._data = items;
        } else if(_.isArray(items)) {
            this._data = new Collection();
            _.each(items, function(item) {
                var model;
                if(_.isObject(item)) {
                    model = new Model(item);
                } else {
                    model = new Model({
                        value: item,
                        title: item
                    })
                }
                this._data.add(model);
            }, this);
        }

        if(this._data.size()===0 && this._data.resource) {
            this._data.fetch({
                async: false,
                limit: LIMIT
            });
        }

        options.valueField && this.valueField(options.valueField);
        options.displayField && this.displayField(options.displayField);

        this.render();

        this.listenTo(this._data, "all", _.bind(onDataChange, this));

        return this;
    }

    function onDataChange() {
        this.render();
    }

    function valueField(field) {
        if(arguments.length===0) return this.$el.attr('valueField');

        this.$el.attr('valueField', field);
    }

    function displayField(field) {
        if(arguments.length===0) return this.$el.attr('displayField');

        this.$el.attr('displayField', field);
    }


    function name(name) {

        if(!name) return this.$el.attr('name');

        this.$el.attr('name', name);
    }

    function type() {
        return this.$el.attr('type');
    }

    /**
     * @todo
     * @param hint
     * @returns {*}
     */
    function placeholder(hint) {
    }


    function clearValidation() {
        this._violations = [];
        this._tooltips.content("");
        this.removeClass('uw-border-warn');
    }

    function readonly(value) {
        if(arguments.length===0) return (this.el.hasAttribute('readonly')||this.el.hasAttribute('disabled'));

        if(value) {
            this.el.setAttribute('readonly', null);
            this.el.setAttribute('disabled', null);
        } else {
            this.el.removeAttribute('readonly');
            this.el.removeAttribute('disabled');
        }

    }

    function required(value) {
        if(arguments.length===0) return this.el.hasAttribute('required');

        value ? this.el.setAttribute('required', null) : this.el.removeAttribute('required');
    }

    function disabled(value) {
        this.readonly.apply(this, arguments);
    }

    function maxLength(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('maxLength');
        $component.attr('maxLength', value);
    }

    function minLength(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('minLength');
        $component.attr('minLength', value);
    }

    function referencType(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('reference');
        $component.attr('reference', value);
        if(this.reference()) {
            this.valueField('code');
            this.displayField() || this.displayField('text');
        }
    }

    function codeType(value) {
        var $component = this.$el;
        if(arguments.length===0) return $component.attr('code');
        $component.attr('code', value);
        if(this.code()) {
            this.valueField('code');
            this.displayField('text');
        }
    }

    function locale(locale) {
        if(!locale) return this.attr('locale');

        if(locale===this.locale()) return;

        this.attr('locale', locale);
        this.render();
    }

    function violate(options) {
        var defaults = {
            properties: [this.name()]
        };

        options || (options = {});
        _.defaults(options, defaults);

        var violation = new Violation(options);

        this._violations.push(violation);

        this.hasClass('uw-border-warn') || this.addClass('uw-border-warn');

        this._tooltips.append(violation.toString() + "<br>");

        return violation;
    }


    function violations() {
        return this._violations;
    }

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value.toUpperCase();

        this.readonlyState() && this.readonly(_.contains(this.readonlyState().toUpperCase().split(","), this._state));
        this.disabledState() && this.disabled(_.contains(this.disabledState().toUpperCase().split(","), this._state));
        this.hiddenState() && _.contains(this.hiddenState().toUpperCase().split(","), this._state) ? this.hide() : this.show();
    }

    function readonlyState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-readonly-state');
        $input.attr('data-readonly-state', value);
    }

    function disabledState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-disabled-state');
        $input.attr('data-disabled-state', value);
    }

    function hiddenState(value) {
        var $input = this.$el;
        if(arguments.length===0) return $input.attr('data-hidden-state');
        $input.attr('data-hidden-state', value);
    }


    var props = {
        /**
         * @override
         */
        tagName: 'select',
        className: 'form-control',

        _violations: undefined,
        _tooltips: undefined,
        _data: undefined,
        _$clear: undefined
    };

    var Selectbox = declare(Base, {
        initialize: initialize,
        render: render,
        value: value,
        clear: clear,
        format: format,
        parse: parse,
        change: change,
        data: data,
        valueField: valueField,
        displayField: displayField,
        name: name,
        type: type,
        placeholder: placeholder,
        validate: validate,
        checkValidity: checkValidity,
        customValidate: customValidate,
        violate: violate,
        violations: violations,
        clearValidation: clearValidation,
        readonly: readonly,
        required: required,
        disabled: disabled,
        maxLength: maxLength,
        minLength: minLength,
        reference: referencType,
        code: codeType,
        state: state,
        readonlyState: readonlyState,
        disabledState: disabledState,
        hiddenState: hiddenState
    }, props);

    return Selectbox ;

});