/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['jquery', 'underscore', 'uweaver/lang', 'uweaver/widget/Inputbox', 'uweaver/i18n'], function ($, _, lang, Inputbox, i18n){
    var declare = lang.declare;
    var Base = Inputbox;

    /**
     * A Textbox is a widget for ....
     *
     * ##### JavaScript
     * ```javascript
     * var textbox = new Textbox({
     *     el: $('[name=note]')
     * });
     * textbox.render();
     *
     * textbox.value("Daisy");
     *
     * expect(textbox.value()).to.equal("Daisy");
     * ```
     * ##### HTML
     * + selector: input[type=text], textarea.
     *
     * ```html
     * <form name="form" class="container form-horizontal">
     * ...
     *  <input type="text" name="note">
     * ...
     * </form>
     * ```
     *
     * ##### Events:
     * + 'change' - [value()]{@link Textbox#value}
     *
     * @constructor Textbox
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     * @param {String} config.name - A text string to specify the name. Required.
     * @param {String} [config.value] - Specifies the value of an <input> element.
     * @param {Boolean} [config.readonly=false] - Specifies that an input field is read-only.
     * @param {Boolean} [config.required=false] - Specifies that an input field must be filled out.
     * @param {Boolean} [config.disabled=false] - Specifies that an <input> element should be disabled.
     * @param {Integer} [config.maxLength=524288] - Specifies the maximum number of characters allowed in the <input> element.
     * @param {Integer} [config.minLength=0] - Specifies the minimum number of characters allowed in the <input> element.
     * @param {String} [config.pattern] - Specifies a regular expression that the <input> element's value is checked against.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

    }

    function i18n() {
        return this.hasAttr('i18n');
    }

    var props = {
        className: 'form-control',
        _value: undefined
    };

    var Textbox = declare(Base, {
        initialize: initialize,
        i18n: i18n
    }, props);

    return Textbox;
});