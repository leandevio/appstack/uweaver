/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2016 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/lang', 'uweaver/dom', 'uweaver/graphics/Graphic',
    'uweaver/graphics/Icon', 'uweaver/data/Collection',
    'uweaver/exception/UnsupportedTypeException'], function (
        _, lang, dom, Graphic,
        Icon, Collection,
        UnsupportedTypeException){

    var declare = lang.declare;
    var Base = Graphic;
    /**
     * **Toolbar**
     * This class represents a group of buttons.
     *
     * @constructor Toolbar
     * @extends Graphic
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [cfg] - A map of configuration to pass to the constructor.
     * @param {Collection} [cfg.data] - The data to construct the button.
     * @param {Object} [cfg.background] - Specifies the css style of the background.
     * @param {Float} [cfg.width] - Specifies the width of the toolbar.
     * @param {Float} [cfg.height] - Specifies the height of the toolbar.
     */
    function initialize(cfg) {
        Base.prototype.initialize.apply(this, arguments);

        var cfg = this.cfg();
        var defaults = {};
        _.defaults(cfg, defaults);

        this._background = this._el.append('rect', dom.ns.SVG).attr('role', 'background')
            .style({width:"100%", height:"100%", opacity: "0", fill: 'white'});
        this._buttons = this._el.append('g', dom.ns.SVG).attr('role', 'buttons');

        var width = cfg.width || parseFloat(this.attr('width'));
        var height = cfg.height || parseFloat(this.attr('height'));
        width && this.width(width, {render: false});
        height && this.height(height, {render: false});
        cfg.background && this.background(cfg.background);
        cfg.data && this.data((cfg.data || new Collection()), {render: false});
    }

    function render(options) {
        options || (options = {});

        var isVisible = this.isVisible();
        this.hide();

        this._buttons.empty();

        if(this._data.size()===0) return this;

        var buttonWidth = this._width ? this._width/this._data.size() : undefined;
        var height = 0, width = 0;
        this._data.each(function(datum) {
            var icon = new Icon({
                value: datum.get('icon'),
                width: buttonWidth,
                height: this._height
            }).render();
            icon.appendTo(this._buttons);
            icon.move(width, 0);
            width += icon.width();
            height = icon.height();
        }, this);

        this._width || this.width(width, {render: false});
        this._height && this.height(height, {render: false});

        if(this.render===render) {
            this.trigger('render', {
                data: this,
                context: this,
                source: this
            });
            (!options.hidden || isVisible) && this.show();
        }

        return this;
    }

    function onClick(event) {
        this.trigger('click', {
            context: this,
            source: this
        });
    }

    function data(data, options) {
        if(data===undefined) return this._data;

        options || (options={});
        var defaults = {
            render: true
        };
        _.defaults(options, defaults);

        this.stopListening(this._data);
        this._data = (data instanceof Collection) ? data : new Collection(data);
        this.listenTo(this._data, "change update reset sync", _.bind(onDataChange, this));
        options.render && this.render();

        return this;
    }

    function onDataChange(event) {
        this.render();
    }

    function size() {
        return this._data.size();
    }

    function width(value, options) {
        var rtnValue = Base.prototype.width.apply(this, arguments);

        if(value===undefined) return rtnValue;

        options || (options = {});
        var defaults = {
            render: true
        };
        _.defaults(options, defaults);

        this._width = value;
        options.render && this.render();
        return this;
    }

    function height(value, options) {
        var rtnValue = Base.prototype.height.apply(this, arguments);

        if(value===undefined) return rtnValue;

        options || (options = {});
        var defaults = {
            render: true
        };
        _.defaults(options, defaults);
        this._height = value;
        options.render && this.render();
        return this;
    }

    var props = {
        _data: undefined,
        _background: undefined,
        _buttons: undefined,
        _width: undefined,
        _height: undefined
    };

    var Toolbar = declare(Base, {
        initialize: initialize,
        render: render,

        data: data,
        size: size,
        width: width,
        height: height

    }, props);

    return Toolbar;


});