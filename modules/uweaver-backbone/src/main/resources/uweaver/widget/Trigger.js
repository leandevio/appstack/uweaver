/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'jquery', 'uweaver/lang',
    'uweaver/widget/Widget', 'uweaver/widget/Button'], function (_, $, lang, Widget, Button) {
    var declare = lang.declare;
    var Base = Widget;


    /**
     * **Trigger**
     * This class represents a group of triggers.
     *
     * **Configs:**
     * - tpl(String): A html string to layout & construct the DOM.
     *
     * @constructor Trigger
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the trigger.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            selector: "button,a"
        };
        var cfg = this.cfg();

        _.defaults(cfg, defaults);

        var buttons = [];
        var $buttons = this.$(cfg.selector);
        _.each($buttons, function(element) {
            var $button = $(element);
            if(!$button.attr('value')) return;
            var button = new Button({
                el: $button
            });
            button.render();
            this.listenTo(button, 'click', _.bind(onButtonClick, this));
            buttons.push(button);
        }, this);

        this._buttons = buttons;

        this.render();
    }

    function buttons() {
        return this._buttons;
    }

    function get(name) {
        return _.find(this._buttons, function(button) {
            if(button.name() && button.name().toLowerCase()===name.toLowerCase()) {
                return button;
            }
        });
    }

    function first() {
        return _.first(this._buttons);
    }

    function last() {
        return _.last(this._buttons);
    }

    function find(criteria) {
        criteria || (criteria = {});
        return _.filter(this._buttons, function (button) {
            var name = button.name(), value = button.value();
            if(criteria.value &&  value && criteria.value.toLowerCase()===value.toLowerCase()) {
                return button;
            }  else if(criteria.name &&  name && crieria.name.toLowerCase()===name.toLowerCase()) {
                return button;
            }
        });
    }

    function destroy() {
        _.each(this._buttons, function(button) {
            button.destroy();
        });

        Base.prototype.destroy.apply(this, arguments);
    }

    function onButtonClick(event) {
        event.context = this;
        this.trigger('execute trigger ' + event.data, event);
    }

    /**
     * Get or set the state.
     *
     *
     *
     * ##### HTML
     *     <!--
     *     + save button - will be disabled in the read state.
     *     + delete button - will be disabled in the new & read state.
     *     -->
     *
     *     <button value="SAVE" data-disabled-state="read"
     *     class="btn btn-default" style="margin:5px;width:100px;color:steelblue;padding:8px 0px;">
     *          <div data-role="text" class="uw-text-center uw-text-normal" style="margin-top:3px;">Save</div>
     *     </button>
     *     <button value="DELETE" data-disabled-state="new,read"
     *     class="btn btn-default" style="margin:5px;width:100px;color:steelblue;padding:8px 0px;">
     *          <div data-role="text" class="uw-text-center uw-text-normal" style="margin-top:3px;">Delete</div>
     *     </button>
     *     <button value="CLOSE"
     *     class="btn btn-default" style="margin:5px;width:100px;color:steelblue;padding:8px 0px;">
     *          <div data-role="text" class="uw-text-center uw-text-normal" style="margin-top:3px;">Close</div>
     *     </button>
     *
     * @memberof Trigger#
     * @param {String} value - the state.
     * @returns {String|void}
     */

    function state(value) {
        if(arguments.length===0) return this._state;

        this._state = value;

        var buttons = this._buttons;
        _.each(buttons, function(button) {
            button.state(value);
        });

    }

    var props = {
        _buttons: undefined,
        _state: undefined
    };

    var Trigger = declare(Base, {
        initialize: initialize,
        buttons: buttons,
        controls: buttons,
        get: get,
        find: find,
        first: first,
        last: last,
        state: state
    }, props);

    return Trigger

});