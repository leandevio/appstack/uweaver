/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'jquery', 'uweaver/lang',
    'uweaver/widget/Widget', 'uweaver/widget/Button'], function (_, $, lang, Widget, Button) {
    var declare = lang.declare;
    var Base = Widget;


    /**
     * **Triggers**
     * This class represents a group of triggers.
     *
     * **Configs:**
     * - tpl(String): A html string to layout & construct the DOM.
     *
     * @deprecated
     * @constructor Triggers
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - The configuration of the triggers.
     */
    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {
            selector: "button,a"
        };
        var cfg = this._cfg;

        _.defaults(cfg, defaults);

        this._buttons = [];
    }

    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };
        var cfg = this._cfg;

        options || (options = {});
        _.defaults(options, defaults);

        this.hide();

        var buttons = this._buttons;

        var $buttons = this.$(cfg.selector);
        _.each($buttons, function(element) {
            var $button = $(element);
            var button = new Button({
                el: $button
            });
            button.render();
            this.listenTo(button, 'click', _.bind(onButtonClick, this));
            buttons.push(button);
        }, this);

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('render', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function get(name) {
        return _.find(this._buttons, function(button) {
            if(button.name()===name) {
                return button;
            }
        });
    }

    function first() {
        return _.first(this._buttons);
    }

    function last() {
        return _.last(this._buttons);
    }

    function find(criteria) {
        return _.filter(this._buttons, function (button) {
            if(button.name()===button.name || button.value().toUpperCase()===criteria.value.toUpperCase()) {
                return button;
            }
        });
    }

    function destroy() {
        _.each(this._buttons, function(button) {
            button.destroy();
        });

        Base.prototype.destroy.apply(this, arguments);
    }

    function onButtonClick(event) {
        event.context = this;
        event.data = {
            value: event.data
        };
        this.trigger('trigger', event);
    }
    var props = {
        _buttons: undefined
    };

    var Triggers = declare(Base, {
        initialize: initialize,
        render: render,
        get: get,
        find: find,
        first: first,
        last: last
    }, props);

    return Triggers

});