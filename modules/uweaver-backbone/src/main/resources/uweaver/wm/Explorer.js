/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['underscore', 'uweaver/dom', 'uweaver/Engine', 'uweaver/environment', 'uweaver/string', 'uweaver/i18n', 'uweaver/theme',
    'uweaver/widget/Widget', 'uweaver/widget/Trigger',
    'uweaver/service/SiteManager', 'uweaver/service/AppManager',
    'text!./Explorer.html'], function(_, dom, Engine, environment, string, i18n, theme, Widget, Trigger, SiteManager, AppManager, tpl) {

    var Base = Widget;
    var engine = Engine.getDefaultInstance();
    var siteManager = SiteManager.getDefaultInstance();
    var appManager = AppManager.getDefaultInstance();
    var resources = dom.createElement("div").html(tpl);

    /**
     * This class represents a explorer style window manager suitable for large screen devices.
     *
     * ##### Usage
     *
     * ##### Events:
     *
     * @constructor Navigator
     * @extends Widget
     * @author  Jason Lin
     * @since   1.0
     * @param {Object} [config] - A map of configuration to pass to the constructor.
     */
    function initialize(config) {
        this._el = dom.select(this.el);

        var html = resource.call(this, 'main');
        this._el.html(html);
        var body = dom.select('body');
        body.empty();
        body.append(this.el);

        this._dock = new Trigger({
            el: this.query('[name=dock]'),
            selector: 'div[value]'
        });

        this.listenTo(siteManager, 'change:site', renderBanner);
        this.listenTo(appManager, 'change:applet', renderDock);
        this.listenTo(engine, 'open', onAppletOpen);
        this.listenTo(engine, 'close', onAppletClose);
        this.listenTo(this._dock, 'trigger', onDockTrigger);
    }

    function render(options) {
        this.hide();
        renderBanner.call(this);
        renderDock.call(this);
        _.delay(_.bind(this.show, this), 500);
    }

    function renderDock() {
        var tpl = resource('shortcut');
        var html = "";
        var width = 3;
        var size = 12 / width;
        var home = environment.property('wm').home;

        var title, icon;
        if(appManager.applet(home)) {
            var applet = appManager.applet(home);
            title = applet.get('abbr') || applet.get('title') || 'Home';
            icon = applet.get('icon') || 'home';
        } else {
            title = 'Home';
            icon = "home";
        }
        html = html + string.format(tpl, {
                id: home,
                title: i18n.translate(title),
                icon: icon,
                width: width
            });
        size--;

        var applets = appManager.applets().first(12/width-1);
        _.each(applets, function(applet) {
            if(applet.id===home) return;
            if(size<=1) return;
            var icon = applet.get('icon') || 'gear';
            html = html + string.format(tpl, {
                    id: applet.id,
                    title: i18n.translate(applet.get('abbr') || applet.get('title')),
                    icon: icon,
                    width: width
                });
            size--;
        }, this);

        html = html + string.format(tpl, {
                id: 'uweaver/applet/Launcher',
                title: i18n.translate('More'),
                icon: 'ellipsis-h',
                width: width
            });
        size--;

        this._dock.query('[name=shortcuts]').html(html);
        this._dock.render();
    }

    function renderBanner() {
        var site = siteManager.site();
        site && this.query('[name=title]').text(site.get('title'));
    }

    function onAppletOpen(event) {
        activate.call(this, event.data);
    }

    function onAppletClose(event) {
        deactivate.call(this, event.data);
    }

    function onDockTrigger(event) {
        engine.open(event.data);
    }

    function activate(applet) {
        deactivate.call(this, this._activeApplet);
        var workspace = this.$('[name=workspace]');
        applet.attach(workspace);
        this._activeApplet = applet;

    }

    function deactivate(applet) {
        if(!applet || applet!==this._activeApplet) return;
        applet.detach();
        this._activeApplet = null;
    }

    function resource(name) {
        return resources.query('[name=' + name + ']').html()
    }

    var props = {
        _resources: undefined,
        _trigger: undefined
    };

    var Explorer = declare(Base, {
        initialize: initialize,
        render: render
    }, props);

    return Explorer;
});