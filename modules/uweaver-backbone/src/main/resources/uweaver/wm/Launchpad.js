/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(['uweaver/dom', 'uweaver/string',
    'uweaver/widget/Widget', 'uweaver/widget/Toolbox',
    'uweaver/Engine',
    'uweaver/service/AppManager'], function(dom, string, Widget, Toolbox, Engine, AppManager) {
    var Base = Widget;
    var engine = Engine.getDefaultInstance();
    var appManager = AppManager.getDefaultInstance();

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);
        this._el = dom.select(this.el);

        this._items = new Toolbox({
            data: appManager.applets(),
            textField: 'title'
        });
        this._items.width("252px");
        this._items.attach(this.el);

        this.listenTo(this._items, 'select', onItemSelect);
        this.listenTo(appManager, 'change:applet', render);
    }

    function render(options) {
        this.hide();
        this._items.render();
        this.show();
        return this;
    }

    function onItemSelect(event) {
        var id = event.data;
        var applet = appManager.applet(id);
        this.open(applet);
    }

    function open(applet) {
        engine.open(applet);
    }

    var props = {
        _resources: undefined,
        _data: undefined,
        _folders: undefined
    };

    var Launchpad = declare(Base, {
        initialize: initialize,
        render: render,
        open: open
    }, props);

    return Launchpad;
});