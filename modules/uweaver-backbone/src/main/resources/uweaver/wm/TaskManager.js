/**
 * Created by jasonlin on 9/23/14.
 */

define(['underscore', 'jquery',
        'uweaver/lang', 'uweaver/string', 'uweaver/identifier', 'uweaver/Deferred',
        'uweaver/widget/Widget',
        'text!./tpl/TaskManager.html',
        'uweaver/Logger'], function (_, $, lang, string, identifier, Deferred, Widget, tpl, Logger) {
    var declare = lang.declare;
    var Base = Widget;

    var TaskManager = declare(Base, {
        /**
         * constructors
         */
        initialize: initialize,

        /**
         * public interfaces
         */
        start: start,
        select: select,
        remove: remove,
        task: task,
        stop: stop,

        /**
         * protected members
         */
        logger: new Logger("TaskManager"),
        render: render,
        destroy: destroy,

        /**
         * private members
         */
        _anchors: undefined,
        _tasks: undefined,

        /**
         * event handler
         */
        onTaskSelect: function(event) {
            var $currentTarget = $(event.currentTarget);
            var id = $currentTarget.attr('name');


            if(event.target.getAttribute('value')==='stop') {
                this.stop(id);
            } else {
                this.select(id);
            }
        }
    });

    function initialize(config) {
        Base.prototype.initialize.apply(this, arguments);

        var defaults = {};
        var cfg = this._cfg;

        _.defaults(cfg, defaults);
        this._anchors = {
            $content: undefined
        };

        cfg.tpl = tpl;

        this._tasks = {};

    }

    function start(module) {
        var context = this;
        var deferred = new Deferred();

        this.logger.debug("Start Task: ${0}", {module: module});

        require([module], function(Applet) {
            var task;

            task = _.find(context._tasks, function(task){
                if(task instanceof Applet) {
                    return task;
                }
            });

            if(task) {
                deferred.resolve(task);
                return deferred.promise();
            }

            try {
                task = new Applet();
            } catch(err) {
                context.logger.error(err);
                return;
            }

            task.start();

            context.listenTo(task, 'stop', _.bind(onStop, context));

            context._tasks[task.pid()] = task;



            var tpl = "<li class='list-group-item uw-clickable uw-hover' name='${pid}'>${title}<i value='stop' class='fa fa-close pull-right'></i></li>";
            var entry = string.substitute(tpl, {
                pid: task.pid(),
                title: task.title()
            });
            var $entry = $(entry);

            context._anchors.$content.append($entry);
            $entry.on('click', _.bind(context.onTaskSelect, context));
            deferred.resolve(task);
        });
        
        return deferred.promise();
    }

    function onStop(event) {
        var task = event.context;
        this.remove(task);
    }

    function remove(pid) {
        var task = this.task(pid);
        var anchors = this._anchors;

        var $entry = anchors.$content.find("[name='" + pid + "']");
        $entry.detach();

        // remove the task entry from tasks
        delete this._tasks[pid];

        var event = {
            context: this,
            data: {
                task: task
            }
        };

        this.trigger('remove', event);
    }

    function stop(pid) {
        var task = this.task(pid);
        task.stop();
        this.remove(pid);
        var event = {
            context: this,
            data: {
                task: task
            }
        };

        this.trigger('stop', event);
    }

    function select(pid) {
        var task = this.task(pid);

        var event = {
            context: this,
            data: {
                task: task
            }
        };

        this.trigger('select', event);
    }

    function task(pid) {
        return this._tasks[pid];
    }

    function render(options) {
        Base.prototype.render.apply(this, arguments);

        var defaults = {
            hidden: false
        };
        var cfg = this._cfg;
        var anchors = this._anchors;

        options || (options = {});
        _.defaults(options, defaults);

        anchors.$content = this.$anchor('content');

        if(this.render === render) {
            this._isRendered = true;

            this.trigger('rendered', {
                data: {},
                context: this,
                source: this
            });
            options.hidden || this.show();
        }

        return this;
    }

    function destroy() {
        _.each(this._tasks, function(task) {
            task.destroy();
        });

        Base.prototype.destroy.apply(this, arguments);
    }


    return TaskManager;
});
