/**
 * @license
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/dom', 'uweaver/Engine', 'uweaver/environment', 'uweaver/string', 'uweaver/Deferred',
    'uweaver/widget/Widget', 'uweaver/widget/Trigger', 'uweaver/wm/Launchpad', 'uweaver/widget/Popup', 'uweaver/widget/Icon', 'uweaver/applet/Logon',
    'uweaver/service/SiteManager', 'uweaver/service/SessionManager',
    'uweaver/util/UIMLResourceBundle', 'text!./Workbench.html'], function(
        _, dom, Engine, environment, string, Deferred,
        Widget, Trigger, Launchpad, Popup, Icon, Logon,
        SiteManager, SessionManager,
        UIMLResourceBundle, uiml) {

    var Base = Widget;
    var engine = Engine.getDefaultInstance();
    var siteManager = SiteManager.getDefaultInstance();
    var sessionManager = SessionManager.getDefaultInstance();
    var resources = new UIMLResourceBundle(uiml);
    var i18n = uweaver.i18n;

    function initialize(config) {
        this.hide();
        this._el = dom.select(this.el);

        var html = resources.get('main');
        this._el.html(html);
        var body = dom.select('body');
        body.empty();
        body.append(this.el);

        this._dock = new Trigger({
            el: this.query('[name=dock]'),
            selector: 'i[value]'
        });
        this._dock.render();
        this.listenTo(this._dock, 'trigger', onDockTrigger);

        this._launchpad = new Launchpad().render();
        this._launcher = new Popup().render();
        this._launcher.content(this._launchpad);
        this._tasklist = new Tasklist({workbench: this});
        this._tasklist.render();
        this._localePref = new LocalePref();
        this._localePref.render();
        this._userInfo = new UserInfo();
        this._userInfo.render();

        this._home_uri = environment.get("home").uri;
        this._logon_uri = environment.get("logon").uri;
        this._logonPopup = new Popup({modal:true}); this._logonPopup.render();

        this._menu = this.$('[name=menu]');
        this._icon = new Icon().render();

        this.listenTo(siteManager, 'change:site', onSiteChange);
        this.listenTo(engine, 'open', onAppletOpen);
        this.listenTo(engine, 'start', onAppletStart);
        this.listenTo(engine, 'close', onAppletClose);
        this.listenTo(this._tasklist, 'select', onTasklistSelect);
        this.listenTo(this._localePref, 'select', onLocalePrefSelect);
        this.listenTo(this._userInfo, 'logout', onUserInfoLogout);

        window.router = this;
    }

    function render(options) {
        options || (options = {});

        siteManager.ready().then(function(site) {
            if(site.get("allowAnonymous")) {
                _render.call(this);
            } else {
                sessionManager.connect().then(function() {
                    if(sessionManager.session().get("username")) {
                        _render.call(this);
                    } else {
                        this.hide();
                        engine.open(this._logon_uri);
                    }
                }, this);
            }
        }, this);
    }

    function _render() {
        var username = sessionManager.session().get("username");
        var locale = sessionManager.session().get("locale");
        this.el.querySelector('[name=username]').innerText = username;
        i18n.locale(locale);

        var site = siteManager.site();
        this.query('[name=title]').text(site.get('title'));
        var button = this._dock.get('user');
        site.get("allowAnonymous") ? button.hide() : button.show();

        this._launchpad.render();

        this.resize();

        _.delay(_.bind(this.show, this), 500);

        engine.open(this._home_uri).then(function(process) {
            this._home = process;
        }, this);

        this._dock.get('inbox').hide();

        this._menu.append(this._icon.$el);

        this.show();
    }

    function onSiteChange() {
        var site = siteManager.site();
        site && this.query('[name=title]').text(site.get('title'));
        var button = this._dock.get('user');
        site.get("allowAnonymous") ? button.hide() : button.show();
        this._icon.name(site.get('icon'));
    }

    function onAppletOpen(event) {
        var process = event.data;
        if(process instanceof Logon) {
            this._logonPopup.content(process);
            this._logonPopup.show();
        } else {
            activate.call(this, process);
        }
    }

    function onAppletStart(event) {
        var process = event.data;
        if(process instanceof Logon) {
            this._logon = process;
            this.listenTo(this._logon, "login", _.bind(onLogonExecute, this));
        }
    }

    function onLogonExecute(event) {
        var username = event.data.username;
        var password = event.data.password;

        sessionManager.login({username: username, password:password}).then(function() {
            this._logonPopup.hide();
            _render.call(this);
        }, function(response) {
            var message = response.responseJSON.message;
            Popup.alert(message, "Login failed");
        }, this)
    }

    function onAppletClose(event) {
        deactivate.call(this, event.data);
    }

    function onTasklistSelect(event) {
        var process = engine.process(event.data);
        activate.call(this, process);
    }

    function onLocalePrefSelect(event) {
        engine.close(this._home);
        this._home = undefined;
        sessionManager.setLocale(event.data).then(function() {
            this.render();
            this._localePref.render();
        }, this);

    }

    function onUserInfoLogout(event) {
        sessionManager.logout().then(function() {
            location.reload();
        });
    }

    function onDockTrigger(event) {
        var value = event.data;

        switch (value) {
            case Workbench.COMMAND.LAUNCHPAD:
                toggleLaunchpad.call(this);
                break;
            case Workbench.COMMAND.TASKLIST:
                toggleTasklist.call(this);
                break;
            case Workbench.COMMAND.LOCALE:
                toggleLocalePref.call(this);
                break;
            case Workbench.COMMAND.EXIT:
                this.exit();
                break;
            case Workbench.COMMAND.HOME:
                engine.open(this._home_uri);
                break;
            case Workbench.COMMAND.USER:
                toggleUserInfo.call(this);
                break;
            case Workbench.COMMAND.MENU:
                toggleMenu.call(this);
                break;
            default:
                engine.open(event.data);
                break;
        }
    }

    function toggleLaunchpad() {
        var dock = this._dock;
        this._launcher.toggle({
            position: {
                right: 0,
                top: dock.height()
            }
        });
        // first time show up
        this.resize();
    }

    function toggleTasklist() {
        var button = this._dock.get('tasklist');
        this._tasklist.render();
        this._tasklist.toggle({positionTo: button});
    }

    function toggleLocalePref() {
        var button = this._dock.get('locale');
        this._localePref.render();
        this._localePref.toggle({positionTo: button});
    }

    function toggleUserInfo() {
        var button = this._dock.get('user');
        this._userInfo.render();
        this._userInfo.toggle({positionTo: button});
    }

    function toggleMenu() {
        var button = this._dock.get('menu');
        this._menu.toggle({positionTo: button});
    }

    function activate(applet) {
        deactivate.call(this, this._activeApplet);
        var workspace = this.$('[name=workspace]');
        applet.attach(workspace);
        var menu = applet.menu();
        if(menu) {
            this._icon.detach();
            if(menu instanceof Trigger) {
                this._menu.append(menu.el);
            } else {
                this._menu.append(menu);
            }
        }

        this.caption(applet.title());

        this._activeApplet = applet;
        this._launcher.hide();
        applet.focus();
    }

    function caption(text) {
        var el = this.el.querySelector('[name=caption]');
        if(arguments.length==0) return el.innerText;

        el.innerText = i18n.translate(text);
    }

    function deactivate(applet) {
        if(!applet || applet!==this._activeApplet) return;
        applet.detach();
        var menu = applet.menu();
        if(menu) {
            menu.detach();
        }
        this._menu.append(this._icon.$el);
        this._activeApplet = null;
        applet.blur();
    }

    function resize() {
        var dock = this.query('[name=dock]');
        var workspace = this.query('[name=workspace]');
        var workspaceHeight = $(window).height() - dock.height();
        workspace.height(workspaceHeight);
        this._launchpad.height(workspace.height());
    }

    function navigate(uri) {
        var deferred = new Deferred();
        var self = this;

        engine.open(uri).then(function(applet) {
            activate.call(self, applet);
            deferred.resolve(applet);
        });

        return deferred.promise();
    }

    var props = {
        _trigger: undefined,
        _home_uri: undefined,
        _home: undefined,
        _tasks: undefined,
        _tasklist: undefined,
        _localePref: undefined,
        _logon_uri: undefined,
        _logon: undefined,
        _logonPopup: undefined,
        _menu: undefined,
        _icon: undefined
    };

    var Workbench = declare(Base, {
        initialize: initialize,
        render: render,
        resize: resize,
        caption: caption,
        navigate: navigate
    }, props);

    // Enumeration of build in commands
    Workbench.COMMAND = {
        LAUNCHPAD: 'launchpad',
        TASKLIST: 'tasklist',
        LOCALE: 'locale',
        HOME: 'home',
        USER: 'user',
        MENU: 'menu',
        EXIT: 'exit'
    };

    var Tasklist = declare(Popup, {
        initialize: function(options) {
            Popup.prototype.initialize.apply(this, arguments);
            options || (options = {});
            this._workbench = options.workbench;

        },

        render: function() {
            Popup.prototype.render.apply(this, arguments);
            this.hide();
            var $html = $(resources.get('tasklist'));
            this.content($html);
            this.$('label').each(function(element){
                var $el = $(this);
                $el.text(i18n.translate($el.text()));
            });

            var $list = this.$('[name=list]');
            var task = '<li class="list-group-item uw-clickable" pid="${pid}">${title}<i value="close" pid="${pid}" class="fa fa-close pull-right" style="display:${display};"></i></li>';
            var tasks = "";
            $list.empty();
            _.each(engine.processes(), function(process) {
                if(process instanceof Logon) return;
                var title = process.title();
                var display = 'inline';
                if(!title) {
                    if(process===this._workbench._home) {
                        title = 'HOME';
                    } else {
                        title = process.pid();
                    }
                }
                if(process===this._workbench._home) {
                    display = 'none';
                }
                tasks = tasks + string.substitute(task, {pid: process.pid(), title: i18n.translate(title), display: display});
            }, this);
            $list.append(tasks);
            var context = this;
            this.$('.list-group-item').on('click', function(event) {
                var $listitem = $(event.currentTarget);
                var pid = $listitem.attr('pid');
                context.trigger('select', {
                    data: pid,
                    source: context,
                    context: context
                })
            });
            this.$('.list-group-item i[value=close]').on('click', function(event) {
                var $listitem = $(event.currentTarget);
                var pid = $listitem.attr('pid');
                engine.close(pid);
                context.render();
                event.stopPropagation();
            });
        }
    }, {
        _workbench: undefined
    });

    var LocalePref = declare(Popup, {
        initialize: function() {
            Popup.prototype.initialize.apply(this, arguments);
            var $html = $(resources.get('localePref'));
            this.content($html);
            this._options = [{"locale": "zh-TW", "title": "繁體中文"}, {"locale": "zh-CN", "title": "简体中文"}, {"locale": "en-US", "title": "English"}];
        },
        render: function() {
            Popup.prototype.render.apply(this, arguments);
            this.hide();
            var $list = this.$('[name=list]');
            var tpl = '<li class="list-group-item uw-clickable" locale="${locale}">${title}<i class="fa fa-check pull-right" style="display:${display};"></i></li>';
            var html = "";
            $list.empty();
            var selection = _.find(this._options, function(option) {
                var locale = option.locale;
                var language = option.locale.split('-')[0];
                return (locale==i18n.locale()||language==i18n.locale());
            });
            _.each(this._options, function(option) {
                var display = (option.locale==selection.locale) ? 'inline' : 'none';
                html = html + string.substitute(tpl, {locale: option.locale, title: option.title, display: display});
            }, this);
            $list.append(html);
            var context = this;
            this.$('.list-group-item').on('click', function(event) {
                var $listitem = $(event.currentTarget);
                var locale = $listitem.attr('locale');
                context.trigger('select', {
                    data: locale,
                    source: context,
                    context: context
                })
            });
        }
    }, {
        _options: undefined
    });

    var UserInfo = declare(Popup, {
        initialize: function() {
            Popup.prototype.initialize.apply(this, arguments);
            var $html = $(resources.get('userInfo'));
            this.content($html);
            this._trigger = new Trigger({
                el: this.el
            }).render();
            this.listenTo(this._trigger, 'execute', function(event) {
                this.trigger(event.data);
            }, this);
        },
        render: function() {
            Popup.prototype.render.apply(this, arguments);
            this.hide();
            var greeting = 'Hi, ' + sessionManager.session().get("username");
            this.el.querySelector('[name=greeting]').innerText = greeting;
        }
    }, {
        _options: undefined
    });

    return Workbench;
});