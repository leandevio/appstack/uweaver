/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.rest;

import org.springframework.web.bind.annotation.*;
import org.uweaver.bpm.entity.ProcessInstance;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Collection;
import org.uweaver.data.Condition;
import org.uweaver.data.DataInstaller;
import org.uweaver.data.Filter;
import org.uweaver.service.app.Applet;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AppletController obj = new AppletController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController
@RequestMapping("applets")
public class AppletController {
    private JSONParser parser = new JSONParser();
    private DataInstaller dataInstaller = new DataInstaller();
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    String search(@RequestParam(value="filters", required=false) String filters,
                  @RequestParam(value="sorters", required=false) String sorters,
                  @RequestParam(value="limit", required=false) Integer limit,
                  @RequestParam(value="offset", required=false) Long offset,
                  @RequestParam(value="conjunction", required = false) String conjunction,
                  HttpServletResponse response) {
        Applet[] applets = dataInstaller.read(Applet[].class);
        long count = applets.length;
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(applets);
    }

}
