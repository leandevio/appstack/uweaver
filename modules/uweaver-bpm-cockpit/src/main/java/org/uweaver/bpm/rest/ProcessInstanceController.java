/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.uweaver.bpm.entity.ProcessInstance;
import org.uweaver.bpm.entity.Task;
import org.uweaver.bpm.entity.TaskLog;
import org.uweaver.bpm.engine.ProcessEngine;
import org.uweaver.core.json.JSONParser;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.security.Pass;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessInstanceController obj = new ProcessInstanceController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController
@RequestMapping("processInstances")
public class ProcessInstanceController {
    private JSONParser parser = new JSONParser();

    @Autowired
    ProcessEngine processEngine;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String search(@RequestParam(value="filters", required=false) String filters,
                  @RequestParam(value="sorters", required=false) String sorters,
                  @RequestParam(value="limit", required=false) Integer limit,
                  @RequestParam(value="offset", required=false) Long offset,
                  @RequestParam(value="conjunction", required = false) String conjunction,
                  HttpServletResponse response) {
        Condition condition = new Condition(parser.readValue(filters,Filter[].class),conjunction);
        List<ProcessInstance> items = processEngine.createProcessInstanceQuery().list();
        long count = items.size();
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(items);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String fetch(@PathVariable String id) {
        ProcessInstance item = processEngine.createProcessInstanceQuery().id(id).singleResult();

        return parser.writeValueAsString(item);
    }

    @RequestMapping(value = "/{id}/suspend", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public void suspend(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
        processEngine.suspend(id);

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return;
    }

    @RequestMapping(value = "/{id}/activate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public void activate(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
        processEngine.activate(id);

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return;
    }

    @RequestMapping(value = "/{id}/cancel", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public void cancel(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
        processEngine.cancel(id, "n/a");

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return;
    }

    @RequestMapping(value = "/{id}/tasks", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String tasks(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
        List<Task> tasks = processEngine.createTaskQuery().processInstanceId(id).list();

        long count = tasks.size();
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(tasks);
    }

    @RequestMapping(value = "/{id}/taskLogs", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String taskLogs(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
        List<TaskLog> taskLogs = processEngine.createTaskLogQuery().processInstanceId(id).list();

        long count = taskLogs.size();
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(taskLogs);
    }
}
