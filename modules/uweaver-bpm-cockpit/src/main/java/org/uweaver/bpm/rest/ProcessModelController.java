/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uweaver.bpm.engine.ProcessEngine;
import org.uweaver.bpm.entity.ProcessModel;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Model;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessModelController obj = new ProcessModelController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@RestController
@RequestMapping({"processModels", "reference/processModels"})
public class ProcessModelController {

    private JSONParser parser = new JSONParser();

    @Autowired
    ProcessEngine processEngine;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    String search(@RequestParam(value="filters", required=false) String filters,
                  @RequestParam(value="sorters", required=false) String sorters,
                  @RequestParam(value="limit", required=false) Integer limit,
                  @RequestParam(value="offset", required=false) Long offset,
                  @RequestParam(value="conjunction", required = false) String conjunction,
                  HttpServletResponse response) {
        Condition condition = new Condition(parser.readValue(filters,Filter[].class),conjunction);
        List<ProcessModel> items = processEngine.createProcessModelQuery().latestVersion().list();
        long count = items.size();
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(items);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public void deploy(@RequestParam(value="payload", required = false) String payload, @RequestParam("attachments") MultipartFile bpmn,
                       HttpServletResponse response) throws IOException {
        Map map = parser.readValue(payload, Map.class);

        byte[] bytes = bpmn.getBytes();
        String name = bpmn.getOriginalFilename();
        processEngine.deploy(name, bytes);

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return;
    }

    @RequestMapping(value = "/{id}/start", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public void start(@PathVariable String id, @RequestParam(value = "payload") String payload, HttpServletRequest request, HttpServletResponse response) {
        Model data = parser.readValue(payload, Model.class);
        Model variables = new Model(data.get("variables"));
        String executor = data.getAsString("executor");
        processEngine.start(id, variables, executor);

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return;
    }

//    @RequestMapping(value="/{uuid}", method = RequestMethod.GET,produces="application/json;charset=UTF-8")
//    public @ResponseBody String findOne(@PathVariable String uuid){
//        return writeProductAsString(processEngine.findOne(uuid));
//    }
//
//    @RequestMapping(value="/{uuid}",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
//    public @ResponseBody String save(@PathVariable String uuid, @RequestParam(value="payload") String payload,
//                                     HttpServletResponse response) {
//        ProcessModel processModel = processEngine.findOne(uuid);
//
//        Model model = parser.readValue(payload, Model.class);
//        model.set("manufacturer", processEngine.findOne(model.getAsString("manufacturer")));
//
//        Item item = new Item(processModel);
//        item.populate(model);
//
//        processModel = processEngine.save(processModel);
//
//        return writeProductAsString(processModel);
//    }
//
//    private String writeProductAsString(ProcessModel processModel) {
//        Model model = new Model(processModel);
//        return parser.writeValueAsString(model);
//    }
}
