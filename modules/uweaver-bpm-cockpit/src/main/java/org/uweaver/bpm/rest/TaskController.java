/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.uweaver.bpm.entity.Task;
import org.uweaver.bpm.engine.ProcessEngine;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Model;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessInstanceController obj = new ProcessInstanceController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController
@RequestMapping("tasks")
public class TaskController {
    private JSONParser parser = new JSONParser();

    @Autowired
    ProcessEngine processEngine;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String search(@RequestParam(value="filters", required=false) String filters,
                  @RequestParam(value="sorters", required=false) String sorters,
                  @RequestParam(value="limit", required=false) Integer limit,
                  @RequestParam(value="offset", required=false) Long offset,
                  @RequestParam(value="conjunction", required = false) String conjunction,
                  HttpServletResponse response) {
        Condition condition = new Condition(parser.readValue(filters,Filter[].class),conjunction);
        List<Task> items = processEngine.createTaskQuery().list();
        long count = items.size();
        response.setIntHeader("Item-Count", (int) count);

        return parser.writeValueAsString(items);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String fetch(@PathVariable String id) {
        Task item = processEngine.createTaskQuery().id(id).singleResult();

        return parser.writeValueAsString(item);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody String save(@PathVariable String id, @RequestParam(value="payload") String payload) {
        Model data = parser.readValue(payload, Model.class);
        Task task = processEngine.createTaskQuery().id(id).singleResult();
        if(data.has("owner")) task.setOwner(data.getAsString("owner"));
        if(data.has("assignee")) task.setAssignee(data.getAsString("assignee"));
        if(data.has("followUpDate")) task.setFollowUpDate(data.getAsDate("followUpDate"));
        if(data.has("dueDate")) task.setDueDate(data.getAsDate("dueDate"));
        task.variables().copyFrom(data.get("variables"));
        processEngine.taskService().update(task);

        return parser.writeValueAsString(task);
    }

    @RequestMapping(value = "/{id}/complete", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public void complete(@PathVariable String id, @RequestParam(value = "payload") String payload, HttpServletRequest request, HttpServletResponse response) {
        Model data = parser.readValue(payload, Model.class);
        Model variables = new Model(data.get("variables"));
        String executor = data.getAsString("executor");
        processEngine.complete(id, variables, executor);

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return;
    }

}
