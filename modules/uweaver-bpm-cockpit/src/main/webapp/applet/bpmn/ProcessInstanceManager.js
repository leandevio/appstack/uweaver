/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/applet/TDIApplet', 'uweaver/applet/Gadget',
        'uweaver/widget/Widget', 'uweaver/widget/Trigger', 'uweaver/widget/Grid', 'uweaver/widget/Pagination', 'uweaver/widget/Form', 'uweaver/widget/Popup',
        'data/bpmn/ProcessInstances', 'data/bpmn/ProcessInstance', 'data/bpmn/ProcessModels',
        'uweaver/util/UIMLResourceBundle', 'text!./ProcessInstanceManager.html'],
    function(_, Base, Gadget, Widget, Trigger, Grid, Pagination, Form, Popup, ProcessInstances, ProcessInstance, ProcessModels, UIMLResourceBundle, uiml) {

        var resources = new UIMLResourceBundle(uiml);
        var TITLE = "Process Instance Management";

        function initialize(config) {
            Base.prototype.initialize.apply(this, arguments);

            this._finder = new Finder();
            this.listenTo(this._finder, 'edit', function(event) {
                this.edit(event.data);
            });
            this.add(this._finder);
            var menu = new Trigger({el: resources.get('Menu')});
            this.listenTo(menu, 'start', start);

            this._processModelSelector = new ProcessModelSelector();

            this.title(TITLE);
            this.menu(menu);
        }

        function start() {
            this._processModelSelector.render();
            Popup.prompt(this._processModelSelector, "Start a process instance?").then(function(processModel) {
                var startForm = new TaskSubmitForm();
                var title = processModel.get('key') + " - " + processModel.get('name');
                Popup.prompt(startForm, title).then(function(data) {
                    var options = {
                        data: data
                    };
                    processModel.start(options).then(function() {
                        Popup.info("The process instance was started successfully.");
                        this._finder.search();
                    }, function() {
                        Popup.alert("Failed to start the process.");
                    }, this);
                }, this);

            }, this)
        }

        function edit(id) {
            var editor = new Editor(id);
            this.add(editor, {closable: true});
        }

        var props = {
            _finder: undefined
        };


        var ProcessInstanceManager = declare(Base, {
            initialize: initialize,
            edit: edit
        }, props);

        var Finder = declare(Gadget, {
            initialize: function(config) {
                Gadget.prototype.initialize.apply(this, arguments);

                this.$el.html(resources.get('Finder'));

                this._data = new ProcessInstances();

                this._grid = this.query('table[name=grid]', Grid);
                this._grid.data(this._data);
                this._grid.render();
                this._pagination = this.query('ul[name=pagination]', Pagination);
                this._pagination.data(this._data);

                this.title('Finder');
                this._criteria = this.query('form[name=criteria]', Form).render();
                this._trigger = this.query('form[name=criteria]', Trigger).render();
                this.listenTo(this._trigger, 'search', this.search);
                this._commandbar = this.query('div[name=commandbar]', Trigger).render();
                this.listenTo(this._commandbar, 'edit', this.edit);
                this.listenTo(this._commandbar, 'suspend', this.suspend);
                this.listenTo(this._commandbar, 'activate', this.activate);
                this.listenTo(this._commandbar, 'cancel', this.cancel);
                this.render();
                this.search();
            },
            render: function() {
                Gadget.prototype.render.apply(this, arguments);
                this.hide();
                this.search();
                this.show();
            },
            search: function() {
                this._data.fetch();
            },
            edit: function() {
                if(this._grid.selection().length===0) {
                    Popup.info("Please select a process.");
                    return;
                }
                var item = _.first(this._grid.selection());
                this.trigger('edit', item.id, this);
            },
            suspend: function() {
                if(this._grid.selection().length===0) {
                    Popup.info("Please select a process.");
                    return;
                }
                var processInstance = _.first(this._grid.selection());
                processInstance.suspend().then(function() {
                    Popup.info("The process instance was suspended successfully.");
                    this.render();
                }, function() {
                    Popup.alert("Failed to suspend the process instance.");
                }, this);
            },
            activate: function() {
                if(this._grid.selection().length===0) {
                    Popup.info("Please select a process.");
                    return;
                }
                var processInstance = _.first(this._grid.selection());
                processInstance.activate().then(function() {
                    Popup.info("The process instance was activated successfully.");
                    this.render();
                }, function() {
                    Popup.alert("Failed to activate the process instance.");
                }, this);
            },
            cancel: function() {
                if(this._grid.selection().length===0) {
                    Popup.info("Please select a process.");
                    return;
                }
                var processInstance = _.first(this._grid.selection());
                processInstance.cancel().then(function() {
                    Popup.info("The process instance was cancelled successfully.");
                    this.render();
                }, function() {
                    Popup.alert("Failed to cancel the process instance.");
                }, this);
            },
            focus: function() {
                Gadget.prototype.focus.apply(this, arguments);
                this.render();
            }
        });

        var ProcessModelSelector = declare(Widget, {
            initialize: function() {
                Widget.prototype.initialize.apply(this, arguments);

                this.$el.html(resources.get('ProcessModelSelector'));
                this._grid = this.query('table', Grid).render();
                this._data = new ProcessModels();
                this._grid.data(this._data);
            },
            render: function() {
                Widget.prototype.render.apply(this, arguments);
                this.hide();
                this._data.fetch();
                this.show();
            },
            data: function() {
                return _.first(this._grid.selection());
            }
        });

        var Editor = declare(Gadget, {
            initialize: function(id) {
                Gadget.prototype.initialize.apply(this, arguments);

                this._data = new ProcessInstance();

                this._data.id = id;
                this._data.fetch().then(function() {
                    this.title(this._data.get('key'));
                }, this);

                this.$el.html(resources.get('Editor'));
                this._form = this.query('form', Form).render();
                this._form.data(this._data);

                this._processInstanceTrigger = this.query('[name=processInstanceTrigger]', Trigger).render();
                this.listenTo(this._processInstanceTrigger, 'close', this.close);
                this.listenTo(this._processInstanceTrigger, 'suspend', this.suspend);
                this.listenTo(this._processInstanceTrigger, 'activate', this.activate);
                this.listenTo(this._processInstanceTrigger, 'cancel', this.cancel);

                this._tasksInput = this.query('table[name=tasks]', Grid);
                this._tasksInput.addRenderer(function(task) {
                    return resources.get('VariablesButton');
                }, 10);
                this._tasksInput.addHandler(this.viewVariables, 10, this);
                this._tasksInput.render();

                this._tasksTrigger = this.query('[name=tasksTrigger]', Trigger).render();
                this.listenTo(this._tasksTrigger, 'edit', this.edit);
                this.listenTo(this._tasksTrigger, 'complete', this.complete);

                this._taskLogsInput = this.query('table[name=taskLogs]', Grid).render();

                this.render();

            },
            render: function() {
                Gadget.prototype.render.apply(this, arguments);
                this.hide();
                this._data.tasks().then(function(tasks) {
                    this._tasksInput.data(tasks);
                }, this);
                this._data.taskLogs().then(function(taskLogs) {
                    this._taskLogsInput.data(taskLogs);
                }, this);
                this.show();
                return this;
            },
            viewVariables: function(task) {
                var data = {
                    variables: task.get('variables')
                };
                var variablesViewer = new VariablesViewer(data);
                var title = task.get('activityId') + " - " + task.id;
                Popup.prompt(variablesViewer, title);
            },
            complete: function() {
                var task = _.first(this._tasksInput.selection());
                if(!task) {
                    Popup.info("Please select a task.");
                    return;
                }
                var data = {
                    executor: task.get('assignee'),
                    variables: task.get('variables')
                };
                var taskSubmitForm = new TaskSubmitForm(data);
                var title = task.get('activityId') + " - " + task.id;
                Popup.prompt(taskSubmitForm, title).then(function(data) {
                    var options = {
                        data: data
                    };
                    task.complete(options).then(function() {
                        Popup.info("The task was completed successfully.");
                        this.render();
                    }, function() {
                        Popup.alert("Failed to complete the task.");
                    }, this);
                }, this);

            },
            edit: function() {
                var task = _.first(this._tasksInput.selection());
                if(!task) {
                    Popup.info("Please select a task.");
                    return;
                }
                var data = {
                    owner: task.get('owner'),
                    assignee: task.get('assignee'),
                    followUpDate: task.get('followUpDate'),
                    dueDate: task.get('dueDate'),
                    variables: task.get('variables')
                };
                var taskEditForm = new TaskEditForm(data);
                var title = task.get('activityId') + " - " + task.id;
                Popup.prompt(taskEditForm, title).then(function(data) {
                    task.set(data.toJSON());
                    task.save().then(function() {
                        Popup.info("The task was update successfully.");
                        this.render();
                    }, function() {
                        Popup.alert("Failed to update the task.");
                    }, this);
                }, this);

            },
            suspend: function() {
                this._data.suspend().then(function() {
                    Popup.info("The process instance was suspended successfully.");
                    this.render();
                }, function() {
                    Popup.alert("Failed to suspend the process instance.");
                }, this);
            },
            activate: function() {
                this._data.activate().then(function() {
                    Popup.info("The process instance was activated successfully.");
                    this.render();
                }, function() {
                    Popup.alert("Failed to activate the process instance.");
                }, this);
            },
            cancel: function() {
                this._data.cancel().then(function() {
                    Popup.info("The process instance was cancelled successfully.");
                    this.close();
                }, function() {
                    Popup.alert("Failed to cancel the process instance.");
                }, this);
            }
        });

        var TaskEditForm = declare(Widget, {
            initialize: function(data) {
                data || (data = {});
                data.variables && (data.variables = JSON.stringify(data.variables, null, 4));
                this.$el.html(resources.get('TaskEditForm'));
                this._form = this.query('form', Form).render();
                this._form.data().set(data);
            },
            validate: function() {
                var variables = this._form.data().get('variables');
                try {
                    JSON.parse(variables);
                } catch(error) {
                    alert(error);
                    return false;
                }
                return true;
            },
            data: function() {
                var data = this._form.data().clone();
                var variables = data.get('variables');
                variables || (variables = "{}");
                data.set('variables', JSON.parse(variables));
                return data;
            }
        });

        var TaskSubmitForm = declare(Widget, {
            initialize: function(data) {
                data || (data = {});
                data.variables && (data.variables = JSON.stringify(data.variables, null, 4));
                this.$el.html(resources.get('TaskSubmitForm'));
                this._form = this.query('form', Form).render();
                this._form.data().set(data);
            },
            validate: function() {
                var variables = this._form.data().get('variables');
                try {
                    JSON.parse(variables);
                } catch(error) {
                    alert(error);
                    return false;
                }
                return true;
            },
            data: function() {
                var data = this._form.data().clone();
                var variables = data.get('variables');
                variables || (variables = "{}");
                data.set('variables', JSON.parse(variables));
                return data;
            }
        });

        var VariablesViewer = declare(Widget, {
            initialize: function(data) {
                data || (data = {});
                data.variables && (data.variables = JSON.stringify(data.variables, null, 4));
                this.$el.html(resources.get('VariablesViewer'));
                this._form = this.query('form', Form).render();
                this._form.data().set(data);
            }
        });

        return ProcessInstanceManager;

    });