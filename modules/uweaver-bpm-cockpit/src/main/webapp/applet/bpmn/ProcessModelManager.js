/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['underscore', 'uweaver/applet/TDIApplet', 'uweaver/applet/Gadget',
        'uweaver/widget/Widget', 'uweaver/widget/Trigger', 'uweaver/widget/Grid', 'uweaver/widget/Pagination', 'uweaver/widget/Form', 'uweaver/widget/Popup',
        'data/bpmn/ProcessModels', 'data/bpmn/ProcessModel',
        'uweaver/util/UIMLResourceBundle', 'text!./ProcessModelManager.html'],
    function(_, Base, Gadget, Widget, Trigger, Grid, Pagination, Form, Popup, ProcessModels, ProcessModel, UIMLResourceBundle, uiml) {

        var resources = new UIMLResourceBundle(uiml);
        var TITLE = "Process Model Management";

        function initialize(config) {
            Base.prototype.initialize.apply(this, arguments);

            this._finder = new Finder();
            this.listenTo(this._finder, 'edit', function(event) {
                edit(event.data());
            });
            this.add(this._finder);
            var menu = new Trigger({el: resources.get('menu')});
            this.listenTo(menu, 'deploy', deploy);

            this.title(TITLE);
            this.menu(menu);
        }

        function deploy() {
            var dialog = new DeploymentDialog().render();
            Popup.prompt(dialog, "Deploy the process model?").then(function(data) {
                data.save().then(function() {
                    this._finder.search();
                    Popup.info("The process model was deployed successfully.");
                }, function() {
                    Popup.alert("Failed to deploy the process model.");
                }, this);
            }, this);
        }

        function edit(id) {
            var editor = new Editor(id).render();
            this.add(editor, {closable: true});
        }

        var props = {
            _finder: undefined
        };

        var ProcessModelManager = declare(Base, {
            initialize: initialize
        }, props);

        var Finder = declare(Gadget, {
            initialize: function(config) {
                Gadget.prototype.initialize.apply(this, arguments);

                this.$el.html(resources.get('finder'));

                this._data = new ProcessModels();

                this._grid = this.query('table[name=grid]', Grid);
                this._grid.data(this._data);
                this._grid.render();
                this._pagination = this.query('ul[name=pagination]', Pagination);
                this._pagination.data(this._data);

                this.title('Finder');
                this._criteria = this.query('form[name=criteria]', Form).render();
                this._trigger = this.query('form[name=criteria]', Trigger).render();
                this.listenTo(this._trigger, 'search', this.search);
                this.render();
                this.search();
            },
            search: function() {
                this._data.fetch();
            },
            edit: function(item) {
                this.trigger('edit', item.id);
            }
        });

        var DeploymentDialog = declare(Widget, {
            initialize: function() {
                Widget.prototype.initialize.apply(this, arguments);

                this.$el.html(resources.get('deploymentDialog'));
                this._form = this.query('form', Form).render();
                var data = new ProcessModel();
                this._form.data(data);
            },
            data: function() {
                return this._form.data();
            },
            validate: function() {
                return this._form.validate();
            }
        });

        var Editor = declare(Gadget, {
            initialize: function(id) {
                Gadget.prototype.initialize.apply(this, arguments);

                this._data = new ProcessModel();

                this._data.id = id;
                this._data.fetch();

                this.$el.html(resources.get('editor'));
                this._form = this.query('form', Form).render();
                this._form.data(this._data);

                this._trigger = this.query('form', Trigger).render();
                this.listenTo(this._trigger, 'save', function() {
                    this.save();
                }, this);
                this.listenTo(this._trigger, 'reset', function() {
                    this.reset();
                }, this);
                this.listenTo(this._data, 'change:name', function() {
                    this.title(this._data.get('name'));
                }, this)
            },
            save: function() {
                var data = this._data;
                if(!this._form.validate()) {
                    Popup.warn(message, "Validation error");
                    return;
                }
                data.save().then(function() {
                    Popup.info("The item saved successfully.");
                }, function() {
                    Popup.alert("Failed to save the item.");
                });
            },
            reset: function() {
                this._data.fetch();
            }
        });

        return ProcessModelManager;

    });