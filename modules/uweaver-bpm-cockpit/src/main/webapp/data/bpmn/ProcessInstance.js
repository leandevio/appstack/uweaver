/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2017 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define(['uweaver/data/Model', './Tasks', './TaskLogs', 'uweaver/Deferred'], function(Model, Tasks, TaskLogs, Deferred) {

    function suspend(options) {
        return this.request('suspend', {method: 'POST'});
    }

    function activate(options) {
        return this.request('activate', {method: 'POST'});
    }

    function cancel(options) {
        return this.request('cancel', {method: 'POST'});
    }

    function tasks(options) {
        var deferred  = new Deferred();
        this.request('tasks').then(function(items) {
            this.set('tasks', new Tasks(items));
            deferred.resolve(this.get('tasks'));
        }, function() {
            deferred.reject();
        }, this);

        return deferred.promise();
    }

    function taskLogs(options) {
        var deferred  = new Deferred();
        this.request('taskLogs').then(function(items) {
            this.set('taskLogs', new TaskLogs(items));
            deferred.resolve(this.get('taskLogs'));
        }, function() {
            deferred.reject();
        }, this);

        return deferred.promise();
    }

    var ProcessInstance = declare(Model, {
        suspend: suspend,
        activate: activate,
        cancel: cancel,
        tasks: tasks,
        taskLogs: taskLogs
    }, {
        resource: 'processInstances'
    });

    return ProcessInstance;
});