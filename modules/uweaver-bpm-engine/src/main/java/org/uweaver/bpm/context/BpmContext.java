/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.context;

import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.camunda.bpm.dmn.engine.impl.DefaultDmnEngineConfiguration;
import org.camunda.bpm.engine.spring.ProcessEngineFactoryBean;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.engine.spring.container.ManagedProcessEngineFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.uweaver.bpm.engine.ProcessEngine;
import org.uweaver.bpm.resolver.IdentityResolver;
import org.uweaver.bpm.resolver.Resolver;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.log.Logger;
import org.uweaver.core.log.LoggerManager;
import org.uweaver.core.util.Environment;
import org.uweaver.jpa.repository.JpaRepositoryImpl;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * BpmContext obj = new BpmContext();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Configuration
@EnableJpaRepositories(basePackages = {"org.uweaver.bpm.repository"},
        repositoryBaseClass = JpaRepositoryImpl.class)
public class BpmContext implements ApplicationContextAware {
    private static final Logger LOGGER = LoggerManager.getLogger(BpmContext.class);
    private static final String DEFAULTIDENTITYRESOLVER = "org.uweaver.bpm.resolver.DefaultIdentityResolver";
    private static final String[] entityPackages = {"org.uweaver.bpm.entity"};
    private static final String[] servicePackages = {"org.uweaver.bpm.service"};
    private static final String[] repositoryPackages = {"org.uweaver.bpm.repository"};
    private Environment environment = Environment.getDefaultInstance();

    private static ApplicationContext applicationContext = null;

    public BpmContext() {
        LOGGER.debug("Establishing uweaver-bpm-engine context ....");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> type) {
        return applicationContext.getBean(type);
    }

    public static List<String> entityPackages() {
        return Arrays.asList(entityPackages);
    }

    public static List<String> servicePackages() {
        return Arrays.asList(servicePackages);
    }

    public static List<String> repositoryPackages() {
        return Arrays.asList(repositoryPackages);
    }

    @Bean
    public SpringProcessEngineConfiguration processEngineConfiguration() throws Exception {
        DefaultDmnEngineConfiguration dmnEngineConfiguration = (DefaultDmnEngineConfiguration)
                DmnEngineConfiguration.createDefaultDmnEngineConfiguration();
        dmnEngineConfiguration.setDefaultInputExpressionExpressionLanguage("groovy");

        DataSource dataSource = getBean(DataSource.class);
        PlatformTransactionManager txManager = new DataSourceTransactionManager(dataSource);

        SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();
        configuration.setProcessEngineName("BPM Engine");
        configuration.setDataSource(dataSource);
        configuration.setTransactionManager(txManager);
        configuration.setJobExecutorActivate(false);
        configuration.setDatabaseSchemaUpdate("true");
        configuration.setDmnEngineConfiguration(dmnEngineConfiguration);
        configuration.setCreateDiagramOnDeploy(true);

        Map<Object, Object> beans = new HashMap<>();


        String resolversSetting = environment.property("bpm.resolvers");
        if(resolversSetting!=null) {
            String[] resolverClassNames = resolversSetting.split(",");
            for(String resolverClassName : resolverClassNames) {
                Resolver resolver = createResolver(resolverClassName);
                beans.put(resolver.name(), resolver);
            }
        }

        IdentityResolver resolver = identityResolver();

        beans.put("identity", resolver);

        configuration.setBeans(beans);

        return configuration;
    }

    private Resolver createResolver(String className) {
        Resolver resolver;
        try {
            Class clazz = Class.forName(className);
            resolver = (Resolver) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        }

        return resolver;
    }

    private IdentityResolver identityResolver() {
        String className = environment.property("bpm.identityResolver", DEFAULTIDENTITYRESOLVER);

        IdentityResolver resolver;
        try {
            Class clazz = Class.forName(className);
            resolver = (IdentityResolver) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        }

        return resolver;
    }

    @Bean
    public org.camunda.bpm.engine.ProcessEngine nativeProcessEngine() throws Exception {
        ProcessEngineFactoryBean factoryBean = new ManagedProcessEngineFactoryBean();
        factoryBean.setProcessEngineConfiguration(processEngineConfiguration());
        return factoryBean.getObject();
    }

    @Bean
    public ProcessEngine processEngine() throws Exception {
        ProcessEngine processEngine = new ProcessEngine(nativeProcessEngine());
        return processEngine;
    }

}
