/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.uweaver.bpm.entity.Task;
import org.uweaver.service.identity.IdentityManager;

import java.util.List;
/**
 *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * NotificationService obj = new NotificationService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class NotificationManager {
    private final ProcessEngine processEngine;
    private IdentityManager identityManager;
    private String template = "Please complete: http://localhost:8080/?applet=TaskApplet&credential=${credential}&task=${task.id}";

    public NotificationManager(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.identityManager = new IdentityManager();
    }

    public void notify(List<Task> tasks) {

    }

    public void notify(Task task) {
//        String assignee = task.assignee();
//        User user = identityManager.createUserQuery().username(assignee).singleResult();
//
//        Message message = new Message();
//        message.setTo(user.email());
//        message.setFrom("Process Engine");
//        message.setSubject(String.format("Task assigned: %s(%s)", user.name(), user.username()));
//        message.setText(compile(task));
//        messenger.send(message);
    }

//    private String compile(Task task) {
//
//    }
}
