/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;
import org.uweaver.bpm.context.BpmContext;
import org.uweaver.bpm.entity.ProcessInstance;
import org.uweaver.bpm.entity.ProcessModel;
import org.uweaver.bpm.entity.Task;
import org.uweaver.core.cipher.Encryptor;
import org.uweaver.core.event.AbstractListenable;
import org.uweaver.core.event.Listenable;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.entity.Notification;
import org.uweaver.i18n.I18n;
import org.uweaver.security.Pass;
import org.uweaver.service.identity.IdentityManager;
import org.uweaver.service.messenger.Messenger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessEngine obj = new ProcessEngine();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessEngine extends AbstractListenable implements Listenable {
    private static final Logger LOGGER = LogManager.getLogger(ProcessEngine.class);
    private static final Environment environment = Environment.getDefaultInstance();
    private static final I18n i18n = new I18n();
    private static final String NOTIFICATIONTEMPLATE = "Please complete: http://localhost:8080/?applet=TaskApplet&credential=${credential}&task=${task.id}";
    private IdentityManager identityManager = new IdentityManager();
    protected org.camunda.bpm.engine.ProcessEngine nativeProcessEngine;
    protected RepositoryService repositoryService;
    protected RuntimeService runtimeService;
    protected TaskService taskService;
    protected TaskLogger taskLogger;
    protected Messenger messenger;

    private static final Encryptor encryptor = new Encryptor(Encryptor.Algorithm.DES, environment.property("encryptor.key", "0000"));

    protected org.camunda.bpm.engine.ProcessEngine nativeProcessEngine() {
        return this.nativeProcessEngine;
    }

    private static class SingletonHolder {
        private static final ProcessEngine INSTANCE = singleton();
    }

    public static ProcessEngine getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static ProcessEngine singleton() {
        ProcessEngine singleton = BpmContext.getBean(ProcessEngine.class);
        return singleton;
    }

    public ProcessEngine(org.camunda.bpm.engine.ProcessEngine nativeProcessEngine) {
        this.nativeProcessEngine = nativeProcessEngine;
        this.repositoryService = new RepositoryService(this);
        this.runtimeService = new RuntimeService(this);
        this.taskLogger = new TaskLogger(this);
        this.taskService = new TaskService(this);
        this.messenger = new Messenger();
    }

    protected ProcessEngine() {}

    public ProcessModelQuery createProcessModelQuery() {
        ProcessModelQuery query = new ProcessModelQuery(this);
        return query;
    }

    public RepositoryService repositoryService() {
        return this.repositoryService;
    }

    public RuntimeService runtimeService() {
        return this.runtimeService;
    }

    public TaskService taskService() {
        return this.taskService;
    }

    public TaskLogger taskLogger() {
        return this.taskLogger;
    }

    public void deploy(String name, byte[] bytes) {
        repositoryService().deploy(name, bytes);
    }

    public ProcessInstance start(String processModelId, Map<String,Object> variables, String initiator, String processInstanceKey) {
        return runtimeService().start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance start(String processModelId, Map<String,Object> variables, String initiator) {
        return runtimeService().start(processModelId, variables, initiator);
    }

    public ProcessInstance start(String processModelId, String initiator) {
        return runtimeService().start(processModelId, initiator);
    }


    public ProcessInstance start(String processModelId, String initiator, String processInstanceKey) {
        return runtimeService().start(processModelId, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String,Object> variables, String initiator, String processInstanceKey) {
        return runtimeService().startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String,Object> variables, String initiator) {
        return runtimeService().startByKey(processModelKey, variables, initiator);
    }

    public ProcessInstance startByKey(String processModelKey, String initiator) {
        return runtimeService().startByKey(processModelKey, initiator);
    }


    public ProcessInstance startByKey(String processModelKey, String initiator, String processInstanceKey) {
        return runtimeService().startByKey(processModelKey, initiator, processInstanceKey);
    }


    public ProcessInstanceQuery createProcessInstanceQuery() {
        ProcessInstanceQuery query = new ProcessInstanceQuery(this);
        return query;
    }

    public ProcessLogQuery createProcessLogQuery() {
        ProcessLogQuery query = new ProcessLogQuery(this);
        return query;
    }

    public void suspend(String processInstanceId) {
        runtimeService().suspend(processInstanceId);
    }

    public void activate(String processInstanceId) {
        runtimeService().activate(processInstanceId);

    }

    public void cancel(String processInstanceId, String reason) {
        runtimeService().cancel(processInstanceId, reason);
    }

    public void claim(String taskId, String assignee) {
        taskService().claim(taskId, assignee);
    }

    public void complete(String taskId, Map<String,Object> variables, String executor) {
        complete(taskId, variables, executor, true);
    }

    public void complete(String taskId, Map<String,Object> variables, String executor, boolean notification) {
        taskService().complete(taskId, variables, executor, notification);
    }

    public void update(String taskId, Map<String,Object> variables) {
        Task task = createTaskQuery().id(taskId).singleResult();
        task.variables().copyFrom(variables);
        taskService().update(task);
    }

    public void update(Task task) {
        taskService().update(task);
    }

    public void complete(String taskId, String executor) {
        taskService().complete(taskId, executor);
    }

    public void pushNotification(String processInstanceId) {
        List<Task> tasks = createTaskQuery().processInstanceId(processInstanceId).list();
        for(Task task : tasks) {
            pushNotification(task);
        }
    }

    public void pushNotification(Task task) {
        String assignee = task.assignee();
        Pass pass = identityManager.createPassQuery().username(assignee).singleResult();
        if(pass==null) return;

        ProcessModel processModel = createProcessModelQuery().id(task.processModelId()).singleResult();
        Notification notification = new Notification();
        String credential = this.credential(assignee);
        String recipientName = (pass.text() == null) ? assignee : pass.text();
        String appName = i18n.translate(environment.appName());
        String processModelName = i18n.translate(processModel.getName());
        notification.setSender(ProcessEngine.class.getCanonicalName());
        notification.setRecipient(assignee);
//        notification.setFollowers(task.followers());
        notification.setChannel(processModel.getKey());
        notification.setData(task.id());

        // subject
        String subject = String.format(i18n.translate("%s: A task (%s) of the %s waiting for you to complete"), appName, task.name(), processModelName);
        StringBuffer content = new StringBuffer();
        // greeting
        content.append("<p>").append(String.format(i18n.translate("Dear %s:"), recipientName)).append("</p>");
        // message
        content.append("<p>").append(String.format(i18n.translate("You have a task (%s) of the %s waiting for you to complete."), task.name(), processModelName)).append("</p>");
        // action
        content.append("<p>").append(String.format(i18n.translate("Please click the link to complete the task. %s?credential=%s&channel=%s&task=%s"),
                environment.url(), encode(credential), encode(notification.getChannel()), encode(task.id()))).append("</p>");
        // sign-off
        content.append("<p>").append(i18n.translate("Regards,")).append("</p>");
        // signature
        content.append("<p>").append(appName).append("</p>");
        // note
        content.append("<p>").append(i18n.translate("Note: this e-mail was sent from a notification-only e-mail address that cannot accept incoming e-mail. Please do not reply to this message.")).append("</p>");
        notification.setSubject(subject);
        notification.setContent(content.toString());
        messenger.push(notification);
        this.trigger("notify", notification);
    }

    public TaskQuery createTaskQuery() {
        TaskQuery query = new TaskQuery(this);
        return query;
    }

    public TaskLogQuery createTaskLogQuery() {
        TaskLogQuery query = new TaskLogQuery(this);
        return query;
    }

    private String credential(String username) {
        return encryptor.cipher(username);
    }

    private String encode(String s) {
        String code;
        try {
            code = URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ApplicationException(e);
        }

        return code;
    }
}
