/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.camunda.bpm.engine.RuntimeService;
import org.uweaver.bpm.entity.ProcessInstance;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessInstanceQuery obj = new ProcessInstanceQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessInstanceQuery {
    private ProcessEngine processEngine;
    private org.camunda.bpm.engine.runtime.ProcessInstanceQuery nativeQuery;

    public ProcessInstanceQuery(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.nativeQuery = processEngine.nativeProcessEngine().getRuntimeService().createProcessInstanceQuery();
    }

    public List<ProcessInstance> list() {
        List<org.camunda.bpm.engine.runtime.ProcessInstance> nativeProcessInstances = this.nativeQuery.list();
        RuntimeService runtimeService = processEngine.nativeProcessEngine().getRuntimeService();

        List<ProcessInstance> processInstances = new ArrayList<>();
        for(org.camunda.bpm.engine.runtime.ProcessInstance nativeProcessInstance : nativeProcessInstances) {
            ProcessInstance processInstance = new ProcessInstance(nativeProcessInstance);
            processInstance.setActiveActivityIds(runtimeService.getActiveActivityIds(processInstance.getId()));
            processInstances.add(processInstance);
        }

        return processInstances;
    }

    public ProcessInstance singleResult() {
        List<ProcessInstance> processInstances = list();
        return processInstances.size() > 0 ? processInstances.get(0) : null;
    }

    public ProcessInstanceQuery id(String id) {
        nativeQuery.processInstanceId(id);
        return this;
    }

    public ProcessInstanceQuery key(String key) {
        nativeQuery.processInstanceBusinessKey(key);
        return this;
    }

    public ProcessInstanceQuery processModelId(String processModelId) {
        nativeQuery.processDefinitionId(processModelId);
        return this;
    }

    public ProcessInstanceQuery processModelKey(String processModelKey) {
        nativeQuery.processDefinitionKey(processModelKey);
        return this;
    }
}
