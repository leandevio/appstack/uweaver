/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstanceQuery;
import org.camunda.bpm.engine.history.HistoricVariableInstance;
import org.uweaver.bpm.entity.ProcessLog;
import org.uweaver.bpm.entity.TaskLog;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessLogQuery obj = new ProcessLogQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessLogQuery {
    private final HistoricProcessInstanceQuery nativeQuery;
    private final ProcessEngine processEngine;

    public ProcessLogQuery(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.nativeQuery = processEngine.nativeProcessEngine().getHistoryService().createHistoricProcessInstanceQuery();
    }

    public List<ProcessLog> list() {
        List<HistoricProcessInstance> nativeProcessLogs = this.nativeQuery.list();

        List<ProcessLog> processLogs = new ArrayList<>();
        for(HistoricProcessInstance nativeProcessLog : nativeProcessLogs) {
            ProcessLog processLog = new ProcessLog(nativeProcessLog);
            TaskLog taskLog = processEngine.createTaskLogQuery().processInstanceId(processLog.id()).singleResult();
            processLog.setInitiator(taskLog.getVariables().getAsString("initiator"));
            processLogs.add(processLog);
        }

        return  processLogs;
    }

    public ProcessLog singleResult() {
        List<ProcessLog> processLog = list();
        return processLog.size() > 0 ? processLog.get(0) : null;
    }

    public ProcessLogQuery id(String id) {
        nativeQuery.processInstanceId(id);
        return this;
    }

    public ProcessLogQuery key(String key) {
        nativeQuery.processInstanceBusinessKey(key);
        return this;
    }

    public ProcessLogQuery processModelId(String processModelId) {
        nativeQuery.processDefinitionId(processModelId);
        return this;
    }

    public ProcessLogQuery processModelKey(String processModelKey) {
        nativeQuery.processDefinitionKey(processModelKey);
        return this;
    }
}
