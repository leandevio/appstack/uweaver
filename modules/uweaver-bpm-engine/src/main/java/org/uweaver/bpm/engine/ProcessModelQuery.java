/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.uweaver.bpm.entity.ProcessModel;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessModelQuery obj = new ProcessModelQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessModelQuery {
    private ProcessEngine processEngine;
    private ProcessDefinitionQuery nativeQuery;

    public ProcessModelQuery(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.nativeQuery = processEngine.nativeProcessEngine().getRepositoryService().createProcessDefinitionQuery();
    }

    public List<ProcessModel> list() {
        List<ProcessDefinition> processDefinitions = this.nativeQuery.list();

        List<ProcessModel> processModels = new ArrayList<>();

        for(ProcessDefinition processDefinition : processDefinitions) {
            ProcessModel processModel = new ProcessModel(processDefinition);
            processModels.add(processModel);
        }

        return processModels;
    }


    public ProcessModel singleResult() {
        List<ProcessModel> processModels = list();
        return processModels.size() > 0 ? processModels.get(0) : null;
    }

    public ProcessModelQuery id(String id) {
        this.nativeQuery.processDefinitionId(id);
        return this;
    }

    public ProcessModelQuery latestVersion() {
        this.nativeQuery.latestVersion();
        return this;
    }

    public ProcessModelQuery key(String key) {
        nativeQuery.processDefinitionKey(key);
        return this;
    }

}
