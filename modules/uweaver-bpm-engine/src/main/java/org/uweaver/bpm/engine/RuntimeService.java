/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.uweaver.bpm.entity.ProcessInstance;
import org.uweaver.bpm.entity.ProcessLog;
import org.uweaver.core.event.AbstractListenable;
import org.uweaver.core.event.Listenable;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RuntimeService obj = new RuntimeService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RuntimeService extends AbstractListenable implements Listenable {
    private static final Logger LOGGER = LogManager.getLogger(RuntimeService.class);
    private final ProcessEngine processEngine;
    private final org.camunda.bpm.engine.RuntimeService nativeRuntimeService;

    public RuntimeService(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.nativeRuntimeService = processEngine.nativeProcessEngine().getRuntimeService();
    }

    public ProcessInstance start(String processModelId, Map<String, Object> variables, String initiator, String processInstanceKey) {
        variables.put("initiator", initiator);
        org.camunda.bpm.engine.runtime.ProcessInstance nativeProcessInstance =
                nativeRuntimeService.startProcessInstanceById(processModelId, processInstanceKey, variables);
        ProcessInstance processInstance = new ProcessInstance(nativeProcessInstance);

        /**
         * Remove the notification of starting the process.
         */
//        try {
//            processEngine.pushNotification(processInstance.id());
//        } catch(Exception e) {
//            LOGGER.warn(e);
//        }

        return processInstance;
    }


    public ProcessInstance start(String processModelId, String initiator, String processInstanceKey) {
        Map<String, Object> variables = new HashMap<>();
        return start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance start(String processModelId, Map<String, Object> variables, String initiator) {
        String processInstanceKey = UUID.randomUUID().toString();
        return start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance start(String processModelId, String initiator) {
        Map<String, Object> variables = new HashMap<>();
        String processInstanceKey = UUID.randomUUID().toString();
        return start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String, Object> variables, String initiator, String processInstanceKey) {
        variables.put("initiator", initiator);
        org.camunda.bpm.engine.runtime.ProcessInstance nativeProcessInstance =
                nativeRuntimeService.startProcessInstanceByKey(processModelKey, processInstanceKey, variables);

        ProcessInstance processInstance = new ProcessInstance(nativeProcessInstance);


        /**
         * Remove the notification of starting the process.
         */
//        try {
//            processEngine.pushNotification(processInstance.id());
//        } catch(Exception e) {
//            LOGGER.warn(e);
//        }
        return processInstance;
    }

    public ProcessInstance startByKey(String processModelKey, String initiator, String processInstanceKey) {
        Map<String, Object> variables = new HashMap<>();
        return startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String, Object> variables, String initiator) {
        String processInstanceKey = UUID.randomUUID().toString();
        return startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, String initiator) {
        Map<String, Object> variables = new HashMap<>();
        String processInstanceKey = UUID.randomUUID().toString();
        return startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public void suspend(String id) {
        nativeRuntimeService.suspendProcessInstanceById(id);
    }

    public void activate(String id) {
        nativeRuntimeService.activateProcessInstanceById(id);
    }

    public void cancel(String id, String reason) {
        nativeRuntimeService.deleteProcessInstance(id, reason);
    }

    public ProcessInstanceQuery createProcessInstanceQuery() {
        return new ProcessInstanceQuery(processEngine);
    }

    public void check(String id) {
        ProcessLog processLog = processEngine.createProcessLogQuery().id(id).singleResult();
        if(processLog!=null && processLog.getEndTime()!=null) {
            this.trigger("finish", processLog);
        }
    }
}
