/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.uweaver.bpm.entity.ProcessLog;
import org.uweaver.bpm.entity.ProcessModel;
import org.uweaver.bpm.entity.TaskLog;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.search.Sort;
import org.uweaver.search.Sorter;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskLogQuery obj = new TaskLogQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskLogQuery {
    private ProcessEngine processEngine;
    private TaskLogger taskLogger;
    private List<Predicate> predicates = new ArrayList<>();
    private Sorter sorter = new Sorter();

    public TaskLogQuery(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.taskLogger = processEngine.taskLogger();
    }

    public List<TaskLog> list(Pageable pageable) {
        return taskLogger.list(predicates, sorter, pageable);
    }

    public List<TaskLog> list() {
        return taskLogger.list(predicates, sorter);
    }

    public TaskLog singleResult() {
        List<TaskLog> taskLogs = list();
        return taskLogs.size() > 0 ? taskLogs.get(0) : null;
    }

    public TaskLogQuery id(String id) {
        Predicate predicate = new Predicate("id", id);
        predicates.add(predicate);
        return this;
    }

    public TaskLogQuery processInstanceId(String processInstanceId) {
        Predicate predicate = new Predicate("processInstanceId", processInstanceId);
        predicates.add(predicate);
        return this;
    }

    public TaskLogQuery processInstanceKey(String processInstanceKey) {
        List<ProcessLog> processLogs = processEngine.createProcessLogQuery().key(processInstanceKey).list();
        List<String> subject = new ArrayList<>();
        for(ProcessLog processLog : processLogs) {
            subject.add(processLog.getId());
        }

        Predicate predicate = new Predicate("processInstanceId", subject, Predicate.Comparator.IN);
        predicates.add(predicate);
        return this;
    }

    public TaskLogQuery processModleId(String processModelId) {
        Predicate predicate = new Predicate("processModelId", processModelId);
        predicates.add(predicate);
        return this;
    }

    public TaskLogQuery processModelKey(String processModelKey) {
        List<ProcessModel> processModels = processEngine.createProcessModelQuery().key(processModelKey).list();
        List<String> subject = new ArrayList<>();
        for(ProcessModel processModel : processModels) {
            subject.add(processModel.getId());
        }

        Predicate predicate = new Predicate("processModelId", subject, Predicate.Comparator.IN);
        predicates.add(predicate);
        return this;
    }

    public TaskLogQuery executor(String executor) {
        Predicate predicate = new Predicate("executor", executor);
        predicates.add(predicate);
        return this;
    }

    public TaskLogQuery orderByCompleteTime() {
        return orderByCompleteTime(Sort.Direction.ASC);
    }

    public TaskLogQuery orderByCompleteTime(Sort.Direction direction) {
        this.sorter.by(direction, "completeTime");
        return this;
    }
}
