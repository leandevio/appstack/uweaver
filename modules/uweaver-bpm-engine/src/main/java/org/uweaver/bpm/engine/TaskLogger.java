/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.uweaver.bpm.entity.TaskLog;
import org.uweaver.bpm.service.TaskLogService;
import org.uweaver.core.event.AbstractListenable;
import org.uweaver.core.event.Listenable;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.search.Sorter;

import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskLogger obj = new TaskLogger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskLogger extends AbstractListenable implements Listenable {
    private static final Logger LOGGER = LogManager.getLogger(TaskLogger.class);
    private ProcessEngine processEngine;
    private TaskLogService taskLogService;

    protected TaskLogger() {}

    public TaskLogger(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.taskLogService = TaskLogService.getDefaultInstance();
    }

    public void log(TaskLog taskLog) {
        taskLogService.save(taskLog);
        this.trigger("add", taskLog);
    }

    public List<TaskLog> list(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        return taskLogService.search(predicates, sorter, pageable);
    }

    public List<TaskLog> list(List<Predicate> predicates, Pageable pageable) {
        return taskLogService.search(predicates, pageable);
    }

    public List<TaskLog> list(List<Predicate> predicates, Sorter sorter) {
        return taskLogService.search(predicates, sorter);
    }

    public List<TaskLog> list(List<Predicate> predicates) {
        return taskLogService.search(predicates);
    }
}
