/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.engine.runtime.Execution;
import org.uweaver.bpm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskQuery obj = new TaskQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskQuery {
    private ProcessEngine processEngine;
    private org.camunda.bpm.engine.task.TaskQuery nativeQuery;

    public TaskQuery(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.nativeQuery = processEngine.nativeProcessEngine().getTaskService().createTaskQuery();
    }

    public List<Task> list() {
        List<org.camunda.bpm.engine.task.Task> nativeTasks = this.nativeQuery.list();
        RuntimeService runtimeService = processEngine.nativeProcessEngine().getRuntimeService();

        List<Task> tasks = new ArrayList<>();

        for(org.camunda.bpm.engine.task.Task nativeTask : nativeTasks) {
            Execution execution = runtimeService.createExecutionQuery().executionId(nativeTask.getExecutionId()).singleResult();
            String activityId = ((ExecutionEntity) execution).getActivityId();
            Task task = new Task(nativeTask);
            task.setActivityId(activityId);
            task.variables().copyFrom(runtimeService.getVariables(nativeTask.getExecutionId()));
            tasks.add(task);
        }

        return tasks;
    }

    public Task singleResult() {
        List<Task> tasks = list();
        return tasks.size() > 0 ? tasks.get(0) : null;
    }

    public TaskQuery id(String id) {
        nativeQuery.taskId(id);
        return this;
    }

    public TaskQuery assignee(String assignee) {
        nativeQuery.taskAssignee(assignee);
        return this;
    }

    public TaskQuery processModelId(String processModelId) {
        nativeQuery.processDefinitionId(processModelId);
        return this;
    }

    public TaskQuery processModelKey(String processModelKey) {
        nativeQuery.processDefinitionKey(processModelKey);
        return this;
    }

    public TaskQuery processInstanceId(String processInstanceId) {
        nativeQuery.processInstanceId(processInstanceId);
        return this;
    }

    public TaskQuery processInstanceKey(String processInstanceKey) {
        nativeQuery.processInstanceBusinessKey(processInstanceKey);
        return this;
    }
}
