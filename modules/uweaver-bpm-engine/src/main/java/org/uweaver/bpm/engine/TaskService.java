/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.engine;

import org.uweaver.bpm.entity.Task;
import org.uweaver.bpm.entity.TaskLog;
import org.uweaver.core.event.AbstractListenable;
import org.uweaver.core.event.Listenable;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Model;

import java.util.Date;
import java.util.List;
import java.util.Map;
/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskService obj = new TaskService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskService extends AbstractListenable implements Listenable {
    private static final Logger LOGGER = LogManager.getLogger(TaskService.class);
    private final ProcessEngine processEngine;
    private final org.camunda.bpm.engine.TaskService nativeTaskService;
    private final TaskLogger taskLogger;

    public TaskService(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.nativeTaskService = processEngine.nativeProcessEngine().getTaskService();
        this.taskLogger = processEngine.taskLogger();
    }

    public void claim(String taskId, String assignee) {
        nativeTaskService.claim(taskId, assignee);
    }

    public void complete(String taskId, String executor, boolean notification) {
        complete(taskId, new Model(), executor, notification);
    }

    public void complete(String taskId, String executor) {
        complete(taskId, new Model(), executor);
    }

    public void complete(String taskId, Map<String, Object> variables, String executor) {
        complete(taskId, variables, executor, true);
    }

    public void complete(String taskId, Map<String, Object> variables, String executor, boolean notification) {
        Task task = processEngine.createTaskQuery().id(taskId).singleResult();

        Model model = new Model(task);
        TaskLog taskLog = model.asBean(TaskLog.class);
        if(variables!=null) taskLog.variables().copyFrom(variables);
        taskLog.setCompleteTime(new Date());
        taskLog.setExecutor(executor);

        if(variables==null) {
            this.nativeTaskService.complete(taskId);
        } else {
            this.nativeTaskService.complete(taskId, variables);
        }

        taskLogger.log(taskLog);

        try {
            if(notification) {
                processEngine.pushNotification(task.processInstanceId());
            }
        } catch(Exception e) {
            LOGGER.warn(e);
        }

//        this.trigger("complete", task);

        List<Task> todos = createTaskQuery().processInstanceId(task.processInstanceId()).list();
        for(Task todo : todos) {
            this.trigger("add", todo);
        }

        if(todos.size()==0) {
            processEngine.runtimeService().check(task.processInstanceId());
        }
    }

    public TaskQuery createTaskQuery() {
        return new TaskQuery(processEngine);
    }

    public void update(Task task) {
        org.camunda.bpm.engine.task.Task nativeTask = nativeTaskService.createTaskQuery().taskId(task.id()).singleResult();

        nativeTask.setOwner(task.owner());
        nativeTask.setAssignee(task.assignee());
        nativeTask.setFollowUpDate(task.followUpDate());
        nativeTask.setDueDate(task.dueDate());

        nativeTaskService.saveTask(nativeTask);
        nativeTaskService.setVariables(task.id(), task.variables());
    }
}
