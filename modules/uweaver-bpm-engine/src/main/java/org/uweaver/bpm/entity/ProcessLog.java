/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.entity;

import org.camunda.bpm.engine.history.HistoricProcessInstance;

import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessInstanceRecord obj = new ProcessInstanceRecord();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessLog {
    private String id;
    private String key;
    private String processModelId;
    private String processModelKey;
    private Date startTime;
    private Date endTime;
    private String startActivityId;
    private String initiator;

    public ProcessLog(HistoricProcessInstance nativeProcessLog) {
        this.id = nativeProcessLog.getId();
        this.key = nativeProcessLog.getBusinessKey();
        this.processModelId = nativeProcessLog.getProcessDefinitionId();
        this.processModelKey = nativeProcessLog.getProcessDefinitionKey();
        this.startTime = nativeProcessLog.getStartTime();
        this.endTime = nativeProcessLog.getEndTime();
        this.startActivityId = nativeProcessLog.getStartActivityId();
    }

    public String getId() {
        return id;
    }

    public String id() {
        return getId();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getProcessModelId() {
        return processModelId;
    }

    public void setProcessModelId(String processModelId) {
        this.processModelId = processModelId;
    }

    public String getProcessModelKey() {
        return processModelKey;
    }

    public void setProcessModelKey(String processModelKey) {
        this.processModelKey = processModelKey;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getStartActivityId() {
        return startActivityId;
    }

    public void setStartActivityId(String startActivityId) {
        this.startActivityId = startActivityId;
    }

    public String getInitiator() {
        return this.initiator;
    }

    public String initiator() {
        return getInitiator();
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
}
