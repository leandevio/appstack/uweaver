/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.entity;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Model;
import org.uweaver.entity.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskRecord obj = new TaskRecord();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Entity
public class TaskLog {
    JSONParser parser = new JSONParser();
    /** 版本 */
    private Integer version;
    /** 編號 */
    private String id;

    private String processInstanceId;
    private String processModelId;
    private String activityId;

    private String name;
    private String description;
    private Date createTime;
    private Date dueDate;
    private Date followUpDate;
    private String owner;
    private String assignee;
    /** 自訂變數 */
    private Model variables;
    private String executor;
    private Date completeTime;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #id}.
     * @return {@link #id}.
     */
    @Id
    @Column(length=100)
    public String getId() {
        return id;
    }

    /**
     * Setter: {@link #id}.
     * @param id {@link #id}.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter: {@link #processModelId}.
     * @return {@link #processModelId}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    public String getProcessModelId() {
        return processModelId;
    }

    /**
     * Setter: {@link #processModelId}.
     * @param processModelId {@link #processModelId}.
     */
    public void setProcessModelId(String processModelId) {
        this.processModelId = processModelId;
    }

    /**
     * Getter: {@link #processInstanceId}.
     * @return {@link #processInstanceId}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    /**
     * Setter: {@link #processInstanceId}.
     * @param processInstanceId {@link #processInstanceId}.
     */
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    /**
     * Getter: {@link #activityId}.
     * @return {@link #activityId}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    public String getActivityId() {
        return activityId;
    }

    /**
     * Setter: {@link #activityId}.
     * @param activityId {@link #activityId}.
     */
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    /**
     * Getter: {@link #name}.
     * @return {@link #name}.
     */
    @Column(length = 100)
    public String getName() {
        return name;
    }


    /**
     * Setter: {@link #name}.
     * @param name {@link #name}.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter: {@link #description}.
     * @return {@link #description}.
     */
    @Column(length = 2000)
    public String getDescription() {
        return description;
    }

    /**
     * Setter: {@link #description}.
     * @param description {@link #description}.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter: {@link #createTime}.
     * @return {@link #createTime}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * Setter: {@link #createTime}.
     * @param createTime {@link #createTime}.
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * Getter: {@link #dueDate}.
     * @return {@link #dueDate}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Setter: {@link #dueDate}.
     * @param dueDate {@link #dueDate}.
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Getter: {@link #followUpDate}.
     * @return {@link #followUpDate}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getFollowUpDate() {
        return followUpDate;
    }

    /**
     * Setter: {@link #followUpDate}.
     * @param followUpDate {@link #followUpDate}.
     */
    public void setFollowUpDate(Date followUpDate) {
        this.followUpDate = followUpDate;
    }

    /**
     * Getter: {@link #owner}.
     * @return {@link #owner}.
     */
    @Column(length = 100)
    public String getOwner() {
        return owner;
    }


    /**
     * Setter: {@link #owner}.
     * @param owner {@link #owner}.
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * Getter: {@link #assignee}.
     * @return {@link #assignee}.
     */
    @Column(length = 100)
    public String getAssignee() {
        return assignee;
    }


    /**
     * Setter: {@link #assignee}.
     * @param assignee {@link #assignee}.
     */
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Transient
    public Model getVariables() {
        return variables;
    }

    public Model variables() {
        return getVariables();
    }

    public void setVariables(Model variables) {
        this.variables = variables;
    }

    /**
     * Getter: {@link #data}.
     * @return {@link #data}.
     */
    @Lob
    public String getData() {
        return parser.writeValueAsString(variables);
    }

    /**
     * Setter: {@link #data}.
     * @param data {@link #data}.
     */
    public void setData(String data) {
        this.variables = parser.readValue(data, Model.class);
    }

    /**
     * Getter: {@link #completeTime}.
     * @return {@link #completeTime}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getCompleteTime() {
        return completeTime;
    }

    /**
     * Setter: {@link #completeTime}.
     * @param completeTime {@link #completeTime}.
     */
    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    /**
     * Getter: {@link #executor}.
     * @return {@link #executor}.
     */
    @Column(length = 100)
    public String getExecutor() {
        return executor;
    }

    /**
     * Setter: {@link #executor}.
     * @param executor {@link #executor}.
     */
    public void setExecutor(String executor) {
        this.executor = executor;
    }
}
