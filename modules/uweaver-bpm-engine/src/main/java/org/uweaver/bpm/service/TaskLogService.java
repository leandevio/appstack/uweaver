/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.bpm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.bpm.context.BpmContext;
import org.uweaver.bpm.entity.TaskLog;
import org.uweaver.bpm.repository.TaskLogRepository;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.search.Sorter;

import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskLogger obj = new TaskLogger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Service
@Transactional
public class TaskLogService {

    @Autowired
    private TaskLogRepository taskLogRepository;

    public List<TaskLog> search(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        return (List<TaskLog>) taskLogRepository.search(predicates, sorter, pageable);
    }

    public List<TaskLog> search(List<Predicate> predicates) {
        return (List<TaskLog>) taskLogRepository.search(predicates);
    }

    public List<TaskLog> search(List<Predicate> predicates, Sorter sorter) {
        return (List<TaskLog>) taskLogRepository.search(predicates, sorter);
    }

    public List<TaskLog> search(List<Predicate> predicates, Pageable pageable) {
        return (List<TaskLog>) taskLogRepository.search(predicates, pageable);
    }

    private static class SingletonHolder {
        private static final TaskLogService INSTANCE = singleton();
    }

    private static TaskLogService singleton() {
        TaskLogService singleton = BpmContext.getBean(TaskLogService.class);
        return singleton;
    }

    protected TaskLogService() {}

    public static TaskLogService getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void save(TaskLog taskLog) {
        taskLogRepository.save(taskLog);
    }
}
