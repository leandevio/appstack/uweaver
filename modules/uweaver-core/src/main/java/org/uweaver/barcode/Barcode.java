/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.barcode;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Barcode obj = new Barcode();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Barcode {
    private static final int DEFAULTWIDTH = 100;
    private static final int DEFAULTHEIGHT = 100;

    private String data;
    private Format format;
    private int width;
    private int height;

    public Barcode(String data, Format format, int width, int height) {
        this.data = data;
        this.format = format;
        this.width = width;
        this.height = height;
    }

    public Barcode(String data, Format format) {
        this(data, format, DEFAULTWIDTH, DEFAULTHEIGHT);
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int width() {
        return width;
    }

    public int height() {
        return height;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String data() {
        return data;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public Format format() {
        return format;
    }

    public enum Format {
        AZTEC("AZTEC"), CODABAR("CODABAR"), CODE_39("CODE_39"), CODE_93("CODE_93"), CODE_128("CODE_128"),
        DATA_MATRIX("DATA_MATRIX"), EAN_8("EAN_8"), EAN_13("EAN_13"), ITF("ITF"), MAXICODE("MAXICODE"),
        PDF_417("PDF_417"), QR_CODE("QR_CODE"), RSS_14("RSS_14"), RSS_EXPANDED("RSS_EXPANDED"),
        UPC_A("UPC_A"), UPC_E("UPC_E"), UPC_EAN_EXTENSION("UPC_EAN_EXTENSION");

        Format(String name) {}
    }

}
