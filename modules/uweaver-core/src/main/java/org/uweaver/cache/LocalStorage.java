/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.cache;

import org.uweaver.core.util.Item;
import org.uweaver.core.util.Model;
import org.uweaver.search.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * LocalStorage obj = new LocalStorage();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class LocalStorage<T> implements Searchable<T> {
    private List<T> data = new ArrayList<>();

    public LocalStorage() {}

    public LocalStorage(List<T> beans) {
        this.data.clear();
        this.data.addAll(beans);
    }

    @Override
    public List<T> search(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        sort(sorter);

        List<T> resultset = new ArrayList<>();
        int offset = Long.valueOf(pageable.offset()).intValue();
        for(int i = offset; i<this.data.size(); i++) {
            resultset.add(this.data.get(i));
            if(resultset.size()>=pageable.size()) break;
        }
        return resultset;
    }

    @Override
    public long count(List<Predicate> predicates) {
        List<T> resultset = new ArrayList<>();
        for(int i= 0; i<this.data.size(); i++) {
            resultset.add(this.data.get(i));
        }
        return resultset.size();
    }

    private void sort(Sorter sorter) {
        if (sorter.size() == 0) return;

        data.sort((T datum1, T datum2) -> {
            int compare = 0;
            Model item1 = new Model(datum1);
            Model item2 = new Model(datum2);
            for(Sort sort : sorter.sorts()) {
                String property = sort.property();
                Sort.Direction direction = sort.direction();
                Object value1 = item1==null ? null : item1.get(property);
                Object value2 = item2==null ? null : item2.get(property);
                if(value1==value2 || (value1!=null && value1.equals(value2))) {
                    compare = 0;
                } else if(value1==null || value2==null) {
                    compare = (value1==null) ? -1 : 1;
                } else if(value1 instanceof Number) {
                    Number n1 = (Number) value1;
                    Number n2 = (Number) value2;
                    compare = (n1.doubleValue()>n2.doubleValue()) ? 1 : -1;
                } else if(value1 instanceof String) {
                    String s1 = (String) value1;
                    String s2 = (String) value2;
                    compare = s1.compareTo(s2);
                } else if(value1 instanceof Date) {
                    Date d1 = (Date) value1;
                    Date d2 = (Date) value2;
                    compare = (d1.getTime()>d2.getTime()) ? 1 : -1;
                }

                if(compare!=0) {
                    if(direction.equals(Sort.Direction.DESC)) compare = compare * -1;
                    break;
                }
            }
            return compare;
        });
    }
}
