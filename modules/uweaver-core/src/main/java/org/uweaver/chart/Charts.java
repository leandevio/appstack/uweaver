/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.chart;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Charts obj = new Charts();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Charts {
    public static List<Extents> distribute(Extents extents, int numOfGroup) {
        List<Extents> groups = distributeByUnit(extents);

        int sizeBeforeSplit = -1;
        while(groups.size()<numOfGroup && groups.size()!=sizeBeforeSplit) {
            sizeBeforeSplit = groups.size();
            groups = split(groups);
        }
        return groups;
    }

    private static List<Extents> split(List<Extents> groups) {
        List<Extents> list = new ArrayList<>();

        list.addAll(groups);

        Extents target = null;
        double stdevMax = 0;
        for(Extents group : list) {
            double stdev = stdev(group);
            if(stdev >= stdevMax) {
                target = group;
                stdevMax = stdev;
            }
        }

        if(target!=null) {
            list.remove(target);
            list.addAll(split(target, stdevMax * 2));
        }

        return list;
    }

    private static double stdev(Extents extents) {
        double sum = 0;
        double mean = mean(extents);
        for(Extent extent : extents) {
            sum += Math.pow(mean(extent) - mean, 2);
        }
        int n = extents.size();
        return (n>1) ? sum / (n-1) : 0;
    }

    private static double mean(Extents extents) {
        double sum = 0;
        for(Extent extent : extents) {
            double mean = mean(extent);
            sum += mean;
        }
        return extents.size()==0 ? 0 : sum/extents.size();
    }

    private static double mean(Extent extent) {
        return (extent.max() + extent.min()) / 2;
    }

    private static List<Extents> split(Extents group, double threshold) {
        List<Extents> list = new ArrayList<>();
        int size = group.size();
        if(size<=1) {
            list.add(group);
        } else if(group.size()==2) {
            Extents extents = new Extents();
            extents.add(group.get(0));
            list.add(extents);
            extents = new Extents();
            extents.add(group.get(1));
            list.add(extents);
        }

        if(list.size()>0) return list;

        double mean = mean(group);
        Extents positive = new Extents();
        Extents negative = new Extents();
        Extents above = new Extents();
        Extents below = new Extents();
        for(Extent extent : group) {
            double deviation = mean(extent) - mean;
            if(deviation>=0) {
                positive.add(extent);
            } else {
                negative.add(extent);
            }

            if(Math.abs(deviation)>=threshold) {
                above.add(extent);
            } else {
                below.add(extent);
            }
        }

        if(positive.size()>0 && negative.size()>0) {
            list.add(positive);
            list.add(negative);
        } else if(above.size()>0 && below.size()>0) {
            list.add(above);
            list.add(below);
        } else {
            list = split((above.size()>0) ? above : below, threshold / 2);
        }

        return list;
    }

    private static List<Extents> distributeByUnit(Extents extents) {
        Map<String, Extents> groups = new HashMap<>();

        for(Extent extent : extents) {
            String unit = extent.unit();

            if(!groups.containsKey(unit)) {
                groups.put(unit, new Extents());
            }
            groups.get(unit).add(extent);
        }

        List<Extents> list = new ArrayList<>();

        list.addAll(groups.values());

        return list;
    }


}
