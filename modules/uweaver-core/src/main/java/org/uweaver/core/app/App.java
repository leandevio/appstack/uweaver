/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.app;

import org.uweaver.core.event.server.ReadyEvent;
import org.uweaver.core.event.server.ServerEvent;
import org.uweaver.core.event.server.StartEvent;
import org.uweaver.core.event.server.StopEvent;
import org.uweaver.core.observer.annotation.Subscribe;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * The interface defines ... The definitions provides ...
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class App {
    private final Subject<ServerEvent, ServerEvent> subject;

    private static class SingletonHolder {
        public static final App INSTANCE = new App();
    }

    public static App getDefaultInstance() {
        return App.SingletonHolder.INSTANCE;
    }

    private App() {
        subject = new SerializedSubject<>(PublishSubject.<ServerEvent>create());
    }


    public rx.Subscription subscribe(Class<? extends ServerEvent> type, Action1<ServerEvent> onNext) {
        rx.Subscription subscription = subject.filter(new Filter(type)).subscribe(onNext);

        return subscription;
    }

    public void subscribe(Object listener) {
        for(Method method : listener.getClass().getMethods()) {
            if(method.getAnnotation(Subscribe.class)==null) continue;
            if(method.getParameterTypes().length!=1) continue;
            Class type = method.getParameterTypes()[0];
            if(!ServerEvent.class.isAssignableFrom(type)) continue;

            Filter filter = new Filter(type);
            Action action = new Action(listener, method);
            subject.filter(filter).subscribe(action);
        }
    }

    private class Action implements Action1<ServerEvent> {
        private Object listener;
        private Method method;
        Action(Object listener, Method method) {
            this.listener = listener;
            this.method = method;
        }

        @Override
        public void call(ServerEvent event) {
            try {
                method.invoke(listener, event);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
    private class Filter implements Func1<ServerEvent, Boolean> {
        Class<? extends ServerEvent> type;
        Filter(Class<? extends ServerEvent> type) {
            this.type = type;
        }

        @Override
        public Boolean call(ServerEvent event) {
            return event.getClass().equals(type);
        }
    }


    private void trigger(ServerEvent event) {
        subject.onNext(event);
    }

    public void start(Map<String, Object> data) {
        StartEvent event = new StartEvent(data);
        trigger(event);
    }

    public void start() {
        start(new HashMap<String, Object>());
    }

    public void ready(Map<String, Object> data) {
        ReadyEvent event = new ReadyEvent(data);
        trigger(event);
    }

    public void ready() {
        ready(new HashMap<String, Object>());
    }

    public void stop(Map<String, Object> data) {
        StopEvent event = new StopEvent(data);
        trigger(event);
    }

    public void stop() {
        stop(new HashMap<String, Object>());
    }
}
