package org.uweaver.core.cipher;

import org.uweaver.core.exception.UnsupportedAlgorithmException;
import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 將資料用指定的 Algorithm 編碼.
 * <p/>
 * Created by jasonlin on 2/21/14.
 */
public class Digester {

    private Algorithm algorithm = Algorithm.MD5;
    private MessageDigest encoder = null;
    private String[] supportedAlgorithms;

    public Digester() {
        initialize();
    }

    public Digester(Algorithm algorithm) {
        this.algorithm = algorithm;
        initialize();
    }

    private void initialize() {
        List<String> list = new ArrayList();
        for (Algorithm algorithm : Algorithm.values()) {
            list.add(algorithm.getName());
        }
        supportedAlgorithms = list.toArray(new String[list.size()]);
    }

    public byte[] digest(byte[] message) {
        if(message==null) return null;
        MessageDigest encoder = getMessageDigest();
        return encoder.digest(message);
    }

    public String digest(String message) {
        if(message==null) return null;
        byte[] value = digest(message.getBytes());

        String hash = Base64.encodeBase64String(value);

        return hash;
    }

    public boolean verify(byte[] message, byte[] hash) {
        if(hash==null) return (message==null);
        byte[] answer = digest(message);
        return Arrays.equals(answer, hash);
    }

    public boolean verify(String message, String hash) {
        if(hash==null) return (message==null);
        String answer = digest(message);
        return hash.equals(answer);
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    private void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    private MessageDigest getMessageDigest() {
        if (encoder == null) {
            try {
                encoder = MessageDigest.getInstance(algorithm.toString());
            } catch (NoSuchAlgorithmException e) {
                throw new UnsupportedAlgorithmException("${0} is not supported. Supported algorithms: ${1}.", e, algorithm.toString(), supportedAlgorithms);
            }
        }
        return encoder;
    }

    public enum Algorithm {
        MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256");

        private final String name;

        Algorithm(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }
    }
}
