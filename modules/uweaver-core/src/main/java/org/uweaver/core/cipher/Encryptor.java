package org.uweaver.core.cipher;

import org.uweaver.core.exception.OperationFailureException;
import org.uweaver.core.exception.UnsupportedAlgorithmException;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 *  將資料用指定的 Algorithm 加密/解密.
 *
 * Created by jasonlin on 2/21/14.
 */
public class Encryptor {
    private Algorithm algorithm = Algorithm.AES;
    private int keyLength = Algorithm.AES.getKeyLength();
    private Object privateKey = null;

    private Cipher encoder = null;
    private Cipher decoder = null;
    private SecretKeySpec secretPrivateKey = null;

    private String[] supportedAlgorithms;

    public Encryptor() {
        initialize();
    }

    public Encryptor(Algorithm algorithm) {
        this.algorithm = algorithm;
        this.keyLength = algorithm.getKeyLength();
        initialize();
    }

    public Encryptor(Algorithm algorithm, int keyLength) {
        this.algorithm = algorithm;
        this.keyLength = keyLength;
        initialize();
    }

    public Encryptor(Algorithm algorithm, Object privateKey) {
        this.algorithm = algorithm;
        this.keyLength = algorithm.getKeyLength();
        this.privateKey = privateKey;
        initialize();
    }

    public Encryptor(Algorithm algorithm, Object privateKey, int keyLength) {
        this.algorithm = algorithm;
        this.privateKey = privateKey;
        this.keyLength = keyLength;
        initialize();
    }

    private void initialize() {
        List<String> list = new ArrayList<>();
        for(Algorithm algorithm : Algorithm.values()) {
            list.add(algorithm.getName());
        }
        supportedAlgorithms = list.toArray(new String[list.size()]);
    }

    public byte[] cipher(byte[] message) {
        byte[] cypher = null;
        try {
            cypher = getEncoder().doFinal(message);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new OperationFailureException(e);
        }

        return cypher;

    }

    public byte[] decipher(byte[] cypher) {
        byte[] message = null;
        try {
            message = getDecoder().doFinal(cypher);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new OperationFailureException(e);
        }

        return message;
    }

    public String cipher(String message) {
        byte[] value = cipher(message.getBytes());

        String hash = Base64.encodeBase64String(value);

        return hash;

    }

    public String decipher(String cypher) {
        byte[] value = Base64.decodeBase64(cypher);

        byte[] message = decipher(value);

        return new String(message);

    }

    private Cipher getEncoder() {

        if(encoder==null) {
            try {
                encoder = Cipher.getInstance(algorithm.toString());
                encoder.init(Cipher.ENCRYPT_MODE, getSecretPrivateKey());
            } catch (NoSuchAlgorithmException e) {
                throw new UnsupportedAlgorithmException("${0} is not supported. Supported algorithms: ${1}.", e, algorithm, supportedAlgorithms);
            } catch (NoSuchPaddingException e) {
                throw new UnsupportedAlgorithmException(e);
            } catch (InvalidKeyException e) {
                throw new UnsupportedAlgorithmException(e);
            }
        }

        return encoder;

    }

    private Cipher getDecoder() {
        if(decoder==null) {
            try {
                decoder = Cipher.getInstance(algorithm.toString());
                decoder.init(Cipher.DECRYPT_MODE, getSecretPrivateKey());
            } catch (NoSuchAlgorithmException e) {
                throw new UnsupportedAlgorithmException("${0} is not supported. Supported algorithms: ${1}.", e, algorithm, supportedAlgorithms);
            } catch (NoSuchPaddingException e) {
                throw new UnsupportedAlgorithmException(e);
            } catch (InvalidKeyException e) {
                throw new UnsupportedAlgorithmException(e);
            }
        }

        return decoder;

    }

    private SecretKeySpec getSecretPrivateKey() {
        if(secretPrivateKey==null) {
            byte[] key;

            if(privateKey instanceof String) {
                key = ((String) privateKey).getBytes();
            } else {
                key = (byte[]) privateKey;
            }

            key = paddingSecretKey(key);

            secretPrivateKey = new SecretKeySpec(key, algorithm.getName());

        }
        return secretPrivateKey;
    }


    private byte[] paddingSecretKey(byte key[]) {
        byte[] newKey = new byte[this.keyLength];

        Arrays.fill(newKey, (byte) 0);

        System.arraycopy(key, 0, newKey, 0, Math.min(newKey.length, key.length));

        return newKey;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    private void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Object getPrivateKey() {
        return privateKey;
    }

    private void setPrivateKey(Object privateKey) {
        this.privateKey = privateKey;
    }

    public enum Algorithm {
        DES("DES"), AES("AES"), ABC("ABC");

        private final String name;
        private final String mode = "ECB";
        private final String padding = "PKCS5Padding";
        private final int keyLength;

        private Algorithm(String name) {
            this.name = name;
            if(name.equals("DES")) {
                keyLength = 8;
            } else if(name.equals("AES")) {
                keyLength = 16;
            } else {
                keyLength = 16;
            }
        }

        public String getName() {
            return name;
        }

        public int getKeyLength() {
            return keyLength;
        }

        public String toString(){
            return name + "/" + mode + "/" + padding;
        }
    }
}
