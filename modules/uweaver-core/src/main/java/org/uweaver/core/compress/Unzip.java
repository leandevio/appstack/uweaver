/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.compress;

import org.uweaver.core.exception.*;

import java.io.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * UnZip obj = new UnZip();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Unzip {
    private ZipInputStream in;
    private int bufferSize = 2048;
    private ZipEntry zipEntry;

    public Unzip(InputStream in) {
        init(in);
    }

    public Unzip(File file) {
        try {
            InputStream in = new FileInputStream(file);
            init(in);
        } catch (FileNotFoundException e) {
            throw new org.uweaver.core.exception.FileNotFoundException(e);
        }
    }

    private void init(InputStream in) {
        this.in = new ZipInputStream(in);
    }

    public ZipEntry nextEntry() {
        try {
            return in.getNextEntry();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void extract(OutputStream out) {
        byte data[] = new byte[bufferSize];
        int count;
        try {
            while ((count = in.read(data)) > 0) {
                out.write(data, 0, count);
            }
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void extract(File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            extract(out);
            out.close();
        } catch (FileNotFoundException e) {
            throw new org.uweaver.core.exception.FileNotFoundException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void close() {
        try {
            in.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    /**
     * @todo
     * @param dir
     */
    public void extractAll(File dir) {
    }
}
