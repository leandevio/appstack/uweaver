/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.compress;
import org.uweaver.core.exception.*;

import java.io.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Zip obj = new Zip();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Zip {
    private ZipOutputStream out;
    private int bufferSize = 2048;
    private ZipEntry zipEntry;

    public Zip(OutputStream out) {
        init(out);
    }

    public Zip(File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            init(out);
        } catch (FileNotFoundException e) {
            throw new org.uweaver.core.exception.FileNotFoundException(e);
        }
    }

    private void init(OutputStream out) {
        this.out = new ZipOutputStream(new BufferedOutputStream(out));
    }

    public ZipEntry addNewEntry(String name) {
        zipEntry = new ZipEntry(name);
        try {
            out.putNextEntry(zipEntry);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return zipEntry;
    }

    public void compress(File file) {
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
            compress(in);
            in.close();
        } catch (FileNotFoundException e) {
            throw new org.uweaver.core.exception.FileNotFoundException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void compress(InputStream in) {
        try {
            BufferedInputStream origin = (in instanceof  BufferedInputStream) ? (BufferedInputStream) in : new BufferedInputStream(in, bufferSize);
            byte data[] = new byte[bufferSize];
            int count;
            while((count = origin.read(data, 0, bufferSize)) != -1) {
                out.write(data, 0, count);
            }
            origin.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void close() {
        try {
            out.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }
}
