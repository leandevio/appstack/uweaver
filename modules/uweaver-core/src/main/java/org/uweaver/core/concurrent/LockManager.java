package org.uweaver.core.concurrent;

import static java.util.concurrent.TimeUnit.*;

import org.joda.time.DateTime;
import org.uweaver.core.exception.LockException;
import org.uweaver.core.exception.TimeoutException;
import org.uweaver.core.util.Environment;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by jasonlin on 6/3/15.
 */
public class LockManager {
    private final int TTL = 60;

    private ConcurrentMap<Object, Lock> locks = new ConcurrentHashMap<>();
    private int ttl = Environment.getDefaultInstance().propertyAsInteger("lock.ttl", TTL);
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public LockManager() {
        initialize();
    }

    public LockManager(int ttl) {
        this.ttl = ttl;
        initialize();
    }

    private void initialize() {
        PurgeTask purgeTask = new PurgeTask(this);
        scheduler.scheduleAtFixedRate(purgeTask, ttl, ttl, SECONDS);
    }

    public synchronized void lock(Object key) {
        Lock lock = locks.get(key);
        if(lock!=null&&lock.isLocked()) {
            throw new LockException("Unable to lock. The object(${0}) has been locked.", key);
        }

        lock = new Lock();

        lock.lock(ttl);

        locks.put(key, lock);
    }

    public synchronized void unlock(Object key) {
        locks.remove(key);
    }

    public synchronized void extend(Object key) {
        Lock lock = locks.get(key);

        if(lock==null) {
            throw new LockException("Unable to extend the lock. The object(${0}) is not locked.", key);
        }

        if(!lock.isLocked()) {
            throw new TimeoutException("Unable to extend the lock. The lock has been released. Object: ${0}, TTL: ${1}. Elapsed time: ${2}",
                    key, (long) ttl, (long) lock.getElapsedTime());
        }

        lock.lock(ttl);
    }

    public synchronized boolean isLocked(Object key) {
        Lock lock = locks.get(key);

        if(lock==null) return false;

        return lock.isLocked();

    }

    public synchronized boolean isAvailable(Object key) {
        return !isLocked(key);
    }

    public synchronized void purge() {
        for(Iterator<Map.Entry<Object, Lock>> it=locks.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Object, Lock> entry = it.next();
            Lock lock = entry.getValue();
            if(!lock.isLocked()) locks.remove(entry.getKey());
        }
    }

    public synchronized void shutdown() {
        scheduler.shutdown();
    }

    private class Lock {
        private DateTime expiredDate;

        public void lock(int seconds) {
            DateTime now = new DateTime();
            expiredDate = now.plusSeconds(seconds);
        }

        public void unlock() {
            expiredDate = new DateTime();
        }

        public boolean isLocked() {
            return expiredDate.isAfterNow();
        }

        public int getElapsedTime() {
            DateTime now = new DateTime();
            int elapsedTime = now.getMillisOfSecond() - expiredDate.getMillisOfSecond();
            return elapsedTime;
        }
    }

    private class PurgeTask implements Runnable
    {

        private LockManager lockManager;

        public PurgeTask(LockManager lockManager) {
            this.lockManager = lockManager;
        }

        @Override
        public void run() {
            lockManager.purge();
        }
    }
}
