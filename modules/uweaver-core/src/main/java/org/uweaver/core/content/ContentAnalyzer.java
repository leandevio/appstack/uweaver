/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.content;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.metadata.Metadata;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.util.MediaType;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * BlobAnalyzer obj = new BlobAnalyzer();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ContentAnalyzer {
    TikaConfig tika;

    public ContentAnalyzer() {
        try {
            tika = new TikaConfig();
        } catch (TikaException e) {
            throw new ApplicationException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public MediaType detect(Path path) {
        MediaType mediaType;
        Metadata metadata = new Metadata();
        metadata.set(Metadata.RESOURCE_NAME_KEY, path.toFile().toString());
        try {
            org.apache.tika.mime.MediaType tikaMediaType = tika.getDetector().detect(TikaInputStream.get(path), metadata);
            mediaType = new MediaType(tikaMediaType.getType(), tikaMediaType.getSubtype());
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        return mediaType;
    }

    public MediaType detect(InputStream input) {
        MediaType mediaType;
        Metadata metadata = new Metadata();
        try {
            org.apache.tika.mime.MediaType tikaMediaType = tika.getDetector().detect(TikaInputStream.get(input), metadata);
            mediaType = new MediaType(tikaMediaType.getType(), tikaMediaType.getSubtype());
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        return mediaType;
    }
}
