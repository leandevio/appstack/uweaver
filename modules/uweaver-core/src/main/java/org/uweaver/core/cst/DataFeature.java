package org.uweaver.core.cst;

/**
 * Created by jasonlin on 7/15/16.
 */
public enum DataFeature {
    ALLCONSTRAINT("ALLCONSTRAINT"), REFERENTIALCONSTRAINT("REFERENTIALCONSTRAINT");

    private final String name;

    DataFeature(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
