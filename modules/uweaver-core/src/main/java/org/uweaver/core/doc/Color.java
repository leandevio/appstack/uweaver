/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Color obj = new Color();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Color {
    private java.awt.Color awtColor;

    public static final Color WHITE = new Color(255, 255, 255);
    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color RED = new Color(255, 0, 0);
    public static final Color GREEN = new Color(0, 255, 0);
    public static final Color BLUE = new Color(0, 0, 255);

    public Color(int r, int g, int b) {
        awtColor = new java.awt.Color(r, g, b);
    }

    public Color(int rgb) {
        awtColor = new java.awt.Color(rgb);
    }

    public Color clone() {
        return new Color(getRGB());
    }

    public int getRGB() {
        return awtColor.getRGB();
    }

    public int getRed() {
        return awtColor.getRed();
    }

    public int getGreen() {
        return awtColor.getGreen();
    }

    public int getBlue() {
        return awtColor.getBlue();
    }

    public Color brighter() {
        awtColor.brighter();
        return this;
    }

    public Color darker() {
        awtColor.darker();
        return this;
    }

}
