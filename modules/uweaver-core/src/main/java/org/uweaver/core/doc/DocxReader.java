/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.io.InputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DocxReader obj = new DocxReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DocxReader implements DocumentReader {
    XWPFDocument xwpfDocument = null;

    @Override
    public void open(InputStream in) {
        try {
            xwpfDocument = new XWPFDocument(in);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public void close() {
        try {
            xwpfDocument.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public int getNumberOfPages() {
        return xwpfDocument.getProperties().getExtendedProperties().getUnderlyingProperties().getPages();
    }

    @Override
    public String getText() {
        XWPFWordExtractor extractor = new XWPFWordExtractor(xwpfDocument);
        return extractor.getText();
    }
}
