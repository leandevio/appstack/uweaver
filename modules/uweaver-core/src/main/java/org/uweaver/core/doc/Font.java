/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.awt.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Font obj = new Font();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Font {
    private FontFamily fontFamily;
    private Double size;
    private Weight weight;
    private Color color = Color.BLACK;

    public Font(FontFamily fontFamily, Double size, Color color) {
        init(fontFamily, size, color);
    }

    public Font(FontFamily fontFamily, Double size) {
        init(fontFamily, size, Color.BLACK);
    }

    private void init(FontFamily fontFamily, Double size, Color color) {
        this.fontFamily = fontFamily;
        this.size = size;
        this.color = color;
    }

    public FontFamily getFontFamily() {
        return fontFamily;
    }

    public Font setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public Weight getWeight() {
        return weight;
    }

    public Font setWeight(Weight weight) {
        this.weight = weight;
        return this;
    }

    public Double getSize() {
        return size;
    }

    public Font setSize(Double size) {
        this.size = size;
        return this;
    }

    public Color getColor() {
        return color;
    }

    public Font setColor(Color color) {
        this.color = color;
        return this;
    }


    public enum Weight {
        NORMAL("NORMAL"), BOLD("BOLD"), BOLDER("BOLDER"), LIGHT("LIGHT"), LIGHTER("LIGHTER");

        private String name;

        Weight(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }
    }
}
