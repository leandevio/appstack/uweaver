/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * FontFamily obj = new FontFamily();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class FontFamily {
    private String name = null;
    private Locale locale = null;
    private String encoding = "UTF-8";

    public static FontFamily DEFAULT = new FontFamily(Locale.getDefault());
    public static FontFamily US = new FontFamily(Locale.US);
    public static FontFamily TAIWAN = new FontFamily(Locale.TAIWAN);
    public static FontFamily CHINA = new FontFamily(Locale.CHINA);
    public static FontFamily JAPAN = new FontFamily(Locale.JAPAN);
    public static FontFamily KOREA = new FontFamily(Locale.KOREA);

    public FontFamily(Locale locale, String name, String encoding) {
        init(locale, name, encoding);
    }

    public FontFamily(Locale locale, String name) {
        init(locale, name, null);
    }

    public FontFamily(Locale locale) {
        init(locale, null, null);
    }

    public FontFamily(String name) {
        init(null, name, null);
    }

    private void init(Locale locale, String name, String encoding) {
        if(name!=null) this.name = name;
        if(locale!=null) this.locale = locale;
        if(encoding!=null) this.encoding = encoding;
    }

    public FontFamily clone() {
        return new FontFamily(locale, name, encoding);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
