package org.uweaver.core.doc;

/**
 * Created by jasonlin on 8/2/16.
 */
public enum FontWeight {
    NORMAL("NORMAL"), BOLD("BOLD"), BOLDER("BOLDER"), LIGHT("LIGHT"), LIGHTER("LIGHTER");

    private String name;

    FontWeight(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
