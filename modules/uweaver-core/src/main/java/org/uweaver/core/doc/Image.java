/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.uweaver.core.util.MediaType;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.InputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Image obj = new Image();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Image implements Element {
    private Rectangle rect;
    private byte[] data;
    private MediaType mediaType;
    private Dimension dimension = null;

    private Double paddingLeft = null;
    private Double paddingRight = null;
    private Double paddingTop = null;
    private Double paddingBottom = null;
    private ImageAlign imageAign = null;
    private VerticalAlign verticalAlign = null;

    public Image(InputStream in, MediaType mediaType) {
        try {
            data = new byte[in.available()];
            this.mediaType = mediaType;
            in.read(data);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

    }

    public Image(byte[] data, MediaType mediaType) {
        this.data = data;
        this.mediaType = mediaType;
    }

    public byte[] getBytes() {
        return data;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    @Override
    public Rectangle getRect() {
        return rect;
    }

    @Override
    public void setRect(Rectangle rect) {
        this.rect = rect;
    }


    @Override
    public Double getPaddingLeft() {
        return paddingLeft;
    }

    @Override
    public Image setPaddingLeft(Double padding) {
        this.paddingLeft = padding;
        return this;
    }

    @Override
    public Double getPaddingRight() {
        return paddingRight;
    }

    @Override
    public Image setPaddingRight(Double padding) {
        this.paddingRight = padding;
        return this;
    }

    @Override
    public Double getPaddingTop() {
        return paddingTop;
    }

    @Override
    public Image setPaddingTop(Double padding) {
        this.paddingTop = padding;
        return this;
    }

    @Override
    public Double getPaddingBottom() {
        return paddingBottom;
    }

    @Override
    public Image setPaddingBottom(Double padding) {
        this.paddingBottom = padding;
        return this;
    }

    @Override
    public VerticalAlign getVerticalAlign() {
        return verticalAlign;
    }

    @Override
    public Image setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    public ImageAlign getImageAlign() {
        return imageAign;
    }

    public Image setImageAlign(ImageAlign textAlign) {
        this.imageAign = textAlign;
        return this;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public Dimension getDimension() {
        return this.dimension;
    }
}
