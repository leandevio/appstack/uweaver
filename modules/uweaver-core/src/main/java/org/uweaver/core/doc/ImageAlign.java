package org.uweaver.core.doc;

/**
 * Created by jasonlin on 6/21/16.
 */
public enum ImageAlign {
    LEFT("LEFT"), RIGHT("RIGHT"), CENTER("CENTER");
    private final String value;

    ImageAlign(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }
}