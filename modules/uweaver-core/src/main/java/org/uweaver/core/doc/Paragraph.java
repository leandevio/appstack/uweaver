/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TextParagraph obj = new TextParagraph();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Paragraph {
    private List<TextRun> textRuns = new ArrayList<>();

    private FontFamily fontFamily = null;
    private FontWeight fontWeight = null;
    private Double fontSize = null;
    private TextAlign textAlign = null;
    private Color color = null;

    private String marker = null;

    private Section container = null;

    Paragraph() {}

    Paragraph(String text) {
        setText(text);
    }

    public Paragraph addTextRun(TextRun textRun) {
        textRun.setContainer(this);
        textRuns.add(textRun);
        return this;
    }

    public List<TextRun> getTextRuns() {
        return Collections.unmodifiableList(textRuns);
    }

    public void setText(String text) {
        textRuns.clear();
        if(text==null) return;
        TextRun textRun = new TextRun(text);
        addTextRun(textRun);
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();

        for(TextRun textRun : textRuns) {
            sb.append(textRun.getText());
        }
        return sb.toString();
    }

    public FontFamily getFontFamily() {
        if(fontFamily==null) {
            return (getContainer()==null) ? FontFamily.DEFAULT : getContainer().getFontFamily();
        }
        return fontFamily;
    }

    public Paragraph setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public FontWeight getFontWeight() {
        if(fontWeight==null) {
            return (getContainer()==null) ? FontWeight.NORMAL : getContainer().getFontWeight();
        }
        return fontWeight;
    }

    public Paragraph setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    public Double getFontSize() {
        if(fontSize==null) {
            return (getContainer()==null) ? 12d : getContainer().getFontSize();
        }
        return fontSize;
    }

    public Paragraph setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public Color getColor() {
        if(color==null) {
            return (getContainer()==null) ? Color.BLACK : getContainer().getColor();
        }
        return color;
    }

    public Paragraph setColor(Color color) {
        this.color = color;
        return this;
    }

    public TextAlign getTextAlign() {
        if(textAlign==null) {
            return (getContainer()==null) ? TextAlign.LEFT : getContainer().getTextAlign();
        }
        return textAlign;
    }

    public Paragraph setTextAlign(TextAlign textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }


    public Section getContainer() {
        return container;
    }

    public void setContainer(Section container) {
        this.container = container;
    }
}
