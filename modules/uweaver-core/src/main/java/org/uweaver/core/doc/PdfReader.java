/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PdfReader obj = new PdfReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PdfReader implements DocumentReader {
    PDDocument document = null;
    PDFTextStripper pdfTextStripper;

    public PdfReader() {
        try {
            pdfTextStripper = new PDFTextStripper();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public void open(InputStream in) {
        try {
            document = PDDocument.load(in);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public void close() {
        try {
            document.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public int getNumberOfPages() {
        return document.getNumberOfPages();
    }

    @Override
    public String getText() {
        String text = null;
        try {
            text = pdfTextStripper.getText(document);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return text;
    }
}
