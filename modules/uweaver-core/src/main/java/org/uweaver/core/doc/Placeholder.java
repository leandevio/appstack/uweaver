/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Placeholder obj = new Placeholder();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Placeholder implements TextElement {
    private String name;
    private Element content = null;
    private Rectangle rect = null;
    private List<Section> sections = new ArrayList<>();

    private Double paddingLeft = null;
    private Double paddingTop = null;
    private Double paddingRight = null;
    private Double paddingBottom = null;
    private VerticalAlign verticalAlign = VerticalAlign.TOP;

    private FontFamily fontFamily = null;
    private Double fontSize = 12d;
    private FontWeight fontWeight = FontWeight.NORMAL;
    private Color color = Color.BLACK;
    private TextAlign textAlign = TextAlign.LEFT;

    public Placeholder(String name, Rectangle rect) {
        this.name = name;
        this.rect = rect;
    }

    public Placeholder(String name) {
        this.name = name;
    }

    public Placeholder() {}

    @Override
    public Placeholder clone() {
        Rectangle rect = new Rectangle(getRect().getX(), getRect().getY(), getRect().getWidth(), getRect().getHeight());
        Placeholder placeholder = new Placeholder(getName(), rect);
        placeholder.setPaddingLeft(paddingLeft);
        placeholder.setPaddingRight(paddingRight);
        placeholder.setPaddingTop(paddingTop);
        placeholder.setPaddingBottom(paddingBottom);
        placeholder.setVerticalAlign(verticalAlign);
        if(fontFamily!=null) placeholder.setFontFamily(fontFamily.clone());
        placeholder.setFontSize(fontSize.doubleValue());
        placeholder.setFontWeight(fontWeight);
        placeholder.setColor(color.clone());
        placeholder.setTextAlign(textAlign);

        return placeholder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Rectangle getRect() {
        return rect;
    }

    @Override
    public void setRect(Rectangle rect) {
        this.rect = rect;
    }

    @Override
    public FontFamily getFontFamily() {
        return fontFamily;
    }

    @Override
    public Placeholder setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    @Override
    public Double getFontSize() {
        return fontSize;
    }

    @Override
    public Placeholder setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    @Override
    public FontWeight getFontWeight() {
        return fontWeight;
    }

    @Override
    public Placeholder setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    @Override
    public Double getPaddingLeft() {
        return paddingLeft;
    }

    @Override
    public Placeholder setPaddingLeft(Double padding) {
        this.paddingLeft = padding;
        return this;
    }

    @Override
    public Double getPaddingRight() {
        return paddingRight;
    }

    @Override
    public Placeholder setPaddingRight(Double padding) {
        this.paddingRight = padding;
        return this;
    }

    @Override
    public Double getPaddingTop() {
        return paddingTop;
    }

    @Override
    public Placeholder setPaddingTop(Double padding) {
        this.paddingTop = padding;
        return this;
    }

    @Override
    public Double getPaddingBottom() {
        return paddingBottom;
    }

    @Override
    public Placeholder setPaddingBottom(Double padding) {
        this.paddingBottom = padding;
        return this;
    }

    @Override
    public VerticalAlign getVerticalAlign() {
        return verticalAlign;
    }

    @Override
    public Placeholder setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    @Override
    public TextAlign getTextAlign() {
        return textAlign;
    }

    @Override
    public Placeholder setTextAlign(TextAlign textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public Placeholder setColor(Color color) {
        this.color = color;
        return this;
    }

    public void setContent(Element content) {
        this.content = content;
        content.setRect(this.getRect());
        if(TextElement.class.isAssignableFrom(content.getClass())) {
            TextElement textElement = (TextElement) content;
            if(getFontFamily()!=null) textElement.setFontFamily(getFontFamily().clone());
            if(getFontSize()!=null) textElement.setFontSize(getFontSize());
            if(getFontWeight()!=null) textElement.setFontWeight(getFontWeight());
            if(getColor()!=null) textElement.setColor(getColor());
        }
    }

    public Element getContent() {
        return content;
    }

    public void clear() {
        sections.clear();
        content = null;
    }

    public void setText(String text) {
        clear();
        if(text==null) return;
        addParagraph(new Paragraph(text));
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();

        for(Section section : sections) {
            sb.append(section.getText()).append('\n');
        }
        return sb.toString();
    }

    public void addSection(Section section) {
        sections.add(section);
        section.setContainer(this);
    }

    public List<Section> getSections() {
        return Collections.unmodifiableList(sections);
    }

    public void removeSection(Section section) {
        sections.remove(section);
    }

    public void addParagraph(Paragraph paragraph) {
        getLastSection().addParagraph(paragraph);
    }

    public void addParagraphs(java.util.List<Paragraph> paragraphs) {
        for(Paragraph paragraph : paragraphs) {
            addParagraph(paragraph);
        }
    }

    public void removeParagraph(Paragraph paragraph) {
        for(Section section : sections) {
            section.removeParagraph(paragraph);
        }
    }

    public List<Paragraph> getParagraphs() {
        List<Paragraph> paragraphs = new ArrayList<>();

        for(Section section : sections) {
            paragraphs.addAll(section.getParagraphs());
        }
        return Collections.unmodifiableList(paragraphs);
    }

    private Section getLastSection() {
        int size = sections.size();
        Section section;
        if(size==0) {
            section = new Section();
            addSection(section);
        } else {
            section = sections.get(size-1);
        }
        return section;
    }

    public boolean isEmpty() {
        return (content==null && sections.size()==0);
    }
}
