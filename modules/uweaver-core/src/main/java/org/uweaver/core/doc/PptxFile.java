/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.sl.draw.DrawPaint;
import org.apache.poi.sl.usermodel.TextParagraph;
import org.apache.poi.sl.usermodel.VerticalAlignment;
import org.apache.poi.xslf.usermodel.*;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.MediaType;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PPTXFile obj = new PPTXFile();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PptxFile {
    Map<SlideLayout, XSLFSlideLayout> slideLayouts = new HashMap<>();
    List<Slide> slides = new ArrayList<>();
    XMLSlideShow xmlSlideShow = null;

    private static JSONParser jsonParser = new JSONParser();

    public PptxFile() {
        xmlSlideShow = new XMLSlideShow();
    }

    public PptxFile(InputStream in) {
        try {
            xmlSlideShow = new XMLSlideShow(in);
            XSLFSlideMaster slideMaster = xmlSlideShow.getSlideMasters().get(0);
            for(XSLFSlideLayout xslfSlideLayout : slideMaster.getSlideLayouts()) {
                slideLayouts.put(toSlideLayout(xslfSlideLayout), xslfSlideLayout);
            }

            for(XSLFSlide xslfSlide : xmlSlideShow.getSlides()) {
                slides.add(toSlide(xslfSlide));
            }
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public XSLFPictureData addPicture(InputStream in, MediaType mediaType) {
        byte[] picture = new byte[0];

        try {
            picture = new byte[in.available()];
            in.read(picture);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return addPicture(picture, mediaType);
    }

    public XSLFPictureData addPicture(byte[] picture, MediaType mediaType) {
        return xmlSlideShow.addPicture(picture, mediaTypeToXSLFPictureType(mediaType));
    }

    private XSLFPictureData.PictureType mediaTypeToXSLFPictureType(MediaType mediaType) {
        if(mediaType.equals(MediaType.IMAGE_PNG)) {
            return XSLFPictureData.PictureType.PNG;
        } else if(mediaType.equals(MediaType.IMAGE_JPEG)) {
            return XSLFPictureData.PictureType.JPEG;
        } else if(mediaType.equals(MediaType.IMAGE_BMP)) {
            return XSLFPictureData.PictureType.BMP;
        } else if(mediaType.equals(MediaType.IMAGE_TIFF)) {
            return XSLFPictureData.PictureType.TIFF;
        } else if(mediaType.equals(MediaType.IMAGE_GIF)) {
            return XSLFPictureData.PictureType.GIF;
        } else {
            return XSLFPictureData.PictureType.PNG;
        }
    }


    public Slide createSlide(String layout) {
        Slide slide = new Slide(getSlideLayout(layout));
        this.slides.add(slide);
        return slide;
    }

    public List<Slide> getSlides() {
        return this.slides;
    }

    public void write(OutputStream out) {
        try {
            for(Slide slide : slides) {
                createXSLFSlide(slide);
            }
            xmlSlideShow.write(out);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void write(File file) {
        try {
            FileOutputStream out = new FileOutputStream(file);
            write(out);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public SlideLayout getSlideLayout(String name) {
        SlideLayout slideLayout = null;

        for(SlideLayout layout : slideLayouts.keySet()) {
            if(layout.getName().equals(name)) {
                slideLayout = layout;
                break;
            }
        }

        return slideLayout;
    }

    private SlideLayout toSlideLayout(XSLFSlideLayout xslfSlideLayout) {
        SlideLayout slideLayout = new SlideLayout();
        //slideLayout.setType(layout.getType());
        slideLayout.setName(xslfSlideLayout.getName());
        for(XSLFTextShape shape : xslfSlideLayout.getPlaceholders()) {
            String name = shape.getPlaceholder().name();
            Placeholder placeholder = slideLayout.createPlaceholder(name);
            Rectangle2D anchor = shape.getAnchor();
            Rectangle rect = new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
            placeholder.setRect(rect);
        }

        return slideLayout;
    }

    private Slide toSlide(XSLFSlide xslfSlide) {
        Slide slide = new Slide(getSlideLayout(xslfSlide.getSlideLayout()));
        slide.setTitle(xslfSlide.getTitle());

        for(XSLFTextShape shape : xslfSlide.getPlaceholders()) {
//            Placeholder.Type type = toPlaceholderType(shape.getPlaceholder());
//            Placeholder placeholder = slide.createPlaceholder(type);
//            placeholder.setAnchor(shape.getAnchor());
        }

        for(XSLFShape shape : xslfSlide.getShapes()) {
            Element element = toElement(shape);
            slide.addElement(element);
        }

        return slide;
    }

    private Element toElement(XSLFShape shape) {
        Element element = null;

        if(shape instanceof XSLFTextShape) {

        } else if(shape instanceof XSLFPictureShape) {

        } else if(shape instanceof XSLFTable) {
            element = toTable((XSLFTable) shape);
        }

        return element;
    }

    private Table toTable(XSLFTable xslfTable) {
        Table table = new Table(xslfTable.getNumberOfRows(), xslfTable.getNumberOfColumns());

        return table;
    }

    private SlideLayout getSlideLayout(XSLFSlideLayout xslfSlideLayout) {
        SlideLayout slideLayout = null;
        for(Map.Entry<SlideLayout, XSLFSlideLayout> entry : slideLayouts.entrySet()) {
            if(entry.getValue().equals(xslfSlideLayout)) {
                slideLayout = entry.getKey();
                break;
            }
        }

        return slideLayout;
    }

    private XSLFTextShape getXSLFPlaceholderByPlaceholder(XSLFSlide xslfSlide, Placeholder placeholder) {
        XSLFTextShape xslfPlaceholder = null;

        for(XSLFTextShape shape : xslfSlide.getPlaceholders()) {
            if(shape.getPlaceholder().name().equals(placeholder.getName())) {
                Rectangle2D anchor = shape.getAnchor();
                Rectangle rect = placeholder.getRect();
                if(anchor.getX()==rect.getX() && anchor.getY()==rect.getY()
                        && anchor.getWidth()==rect.getWidth() && anchor.getHeight()==rect.getHeight()) {
                    xslfPlaceholder = shape;
                    break;
                }
            }
        }

        return xslfPlaceholder;
    }

    private XSLFSlide createXSLFSlide(Slide slide) {
        XSLFSlide xslfSlide = xmlSlideShow.createSlide(slideLayouts.get(slide.getSlideLayout()));
        List<XSLFTextShape> shapesToBeRemoved = new ArrayList<>();

        for(Element element : slide.getElements()) {
            if(element instanceof Placeholder) {
                Placeholder placeholder = (Placeholder) element;
                if(placeholder.isEmpty()) continue;
                Element content = placeholder.getContent();
                XSLFTextShape xslfPlaceholder = getXSLFPlaceholderByPlaceholder(xslfSlide, placeholder);
                if(content==null && placeholder.getText()!=null) {
                    xslfPlaceholder.setText(placeholder.getText());
                } else if(content instanceof Image) {
                    Image image = (Image) content;
                    XSLFPictureData xslfPictureData = addPicture(image.getBytes(), image.getMediaType());
                    XSLFPictureShape xslfPictureShape = xslfSlide.createPicture(xslfPictureData);
                    double wRatio = xslfPictureShape.getAnchor().getWidth() / placeholder.getRect().getWidth();
                    double hRatio = xslfPictureShape.getAnchor().getHeight() / placeholder.getRect().getHeight();
                    double width, height, x, y;
                    if(wRatio > hRatio) {
                        width = placeholder.getRect().getWidth();
                        height = xslfPictureShape.getAnchor().getHeight() / wRatio;
                    } else {
                        height = placeholder.getRect().getHeight();
                        width = xslfPictureShape.getAnchor().getWidth() / hRatio;
                    }
                    x = placeholder.getRect().getX() + (placeholder.getRect().getWidth() - width) / 2;
                    y = placeholder.getRect().getY() + (placeholder.getRect().getHeight() - height) / 2;
                    Rectangle2D rect = new Rectangle2D.Double(x, y, width, height);
                    xslfPictureShape.setAnchor(rect);
                    shapesToBeRemoved.add(xslfPlaceholder);
                } else if(content instanceof Table) {
                    Border.Edge[] borderEdges = {Border.Edge.BOTTOM, Border.Edge.TOP, Border.Edge.LEFT, Border.Edge.RIGHT};
                    Table table = (Table) content;
                    XSLFTable xslfTable = xslfSlide.createTable();
                    for(TableRow row : table.getRows()) {
                        XSLFTableRow xslfTableRow = xslfTable.addRow();
                        if(row.getHeight()!=null) xslfTableRow.setHeight(row.getHeight());
                        for(TableCell cell : row.getCells()) {
                            XSLFTableCell xslfTableCell = xslfTableRow.addCell();
                            xslfTableCell.clearText();
                            for(Border.Edge edge : borderEdges) {
                                org.apache.poi.sl.usermodel.TableCell.BorderEdge xslfBorderEdge = org.apache.poi.sl.usermodel.TableCell.BorderEdge.valueOf(edge.getValue().toLowerCase());
                                Color color = cell.getBorderColor(edge);
                                xslfTableCell.setBorderWidth(xslfBorderEdge, 1.0);
                                if(color!=null) xslfTableCell.setBorderColor(xslfBorderEdge, new java.awt.Color(color.getRGB()));
                            }
                            XSLFTextParagraph p = xslfTableCell.addNewTextParagraph();
                            TextAlign textAlign = cell.getTextAlign();
                            Double fontSize = cell.getFontSize();
                            String fontFamily = (cell.getFontFamily()==null) ? null : cell.getFontFamily().getName();
                            Color color = cell.getColor();

                            if(cell.getVerticalAlign()!=null) {
                                xslfTableCell.setVerticalAlignment(toPptxVerticalAlignment(cell.getVerticalAlign()));
                            }

                            if(textAlign!=null) {
                                p.setTextAlign(TextParagraph.TextAlign.valueOf(textAlign.getValue()));
                            }
                            XSLFTextRun r = p.addNewTextRun();
                            if(fontSize!=null) {
                                r.setFontSize(fontSize);
                            }
                            if(fontFamily!=null) {
                                r.setFontFamily(fontFamily);
                            }
                            if(color!=null) {
                                r.setFontColor(DrawPaint.createSolidPaint(new java.awt.Color(color.getRGB())));
                            }
                            r.setText(cell.getText());
                        }
                    }

                    Rectangle rect = placeholder.getRect();
                    Rectangle2D rectangle2D = new Rectangle2D.Double(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
                    xslfTable.setAnchor(rectangle2D);

                    double availableWidth = xslfTable.getAnchor().getWidth();
                    int columnsWithoutWidth = 0;
                    for(int i=0; i<table.getNumberOfColumns(); i++) {
                        Double columnWidth = table.getColumnWidth(i);
                        if(columnWidth!=null) {
                            availableWidth = availableWidth - columnWidth;
                            xslfTable.setColumnWidth(i, columnWidth);
                        } else {
                            columnsWithoutWidth++;
                        }
                    }

                    double width = availableWidth / columnsWithoutWidth;
                    for(int i=0; i<table.getNumberOfColumns(); i++) {
                        if(table.getColumnWidth(i)==null) {
                            xslfTable.setColumnWidth(i, width);
                        }
                    }
                    shapesToBeRemoved.add(xslfPlaceholder);
                }
            }

        }

        for(int i=0; i<shapesToBeRemoved.size(); i++) {
            xslfSlide.removeShape(shapesToBeRemoved.get(i));
        }

        return xslfSlide;
    }

    private VerticalAlignment toPptxVerticalAlignment(VerticalAlign verticalAlign) {
        if(verticalAlign.equals(VerticalAlign.MIDDLE)) {
            return VerticalAlignment.MIDDLE;
        } else if(verticalAlign.equals(VerticalAlign.BOTTOM)) {
            return VerticalAlignment.BOTTOM;
        } else {
            return VerticalAlignment.TOP;
        }

    }

}
