/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.sl.usermodel.*;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.xslf.usermodel.*;
import org.openxmlformats.schemas.presentationml.x2006.main.CTBackground;
import org.uweaver.core.util.MediaType;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PptxReader obj = new PptxReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PptxReader implements SlideShowReader {
    private static final Map<PictureData.PictureType, MediaType> PICTURETYPETOMEDIATYPEMAP = new HashMap<>();
    private static final Map<org.apache.poi.sl.usermodel.TableCell.BorderEdge, Border.Edge> BORDEREDGEMAP = new HashMap<>();
    private static final Map<TextParagraph.TextAlign, TextAlign> TEXTALIGNMAP = new HashMap<>();
    private static final Map<VerticalAlignment, VerticalAlign> VERTICALALIGNMAP = new HashMap<>();

    private InputStream inputStream;
    private XMLSlideShow xmlSlideShow;
    private Map<XSLFSlideLayout, SlideLayout> slideLayouts = new HashMap<>();

    static {
        PICTURETYPETOMEDIATYPEMAP.put(XSLFPictureData.PictureType.PNG, MediaType.IMAGE_PNG);
        PICTURETYPETOMEDIATYPEMAP.put(XSLFPictureData.PictureType.JPEG, MediaType.IMAGE_JPEG);
        PICTURETYPETOMEDIATYPEMAP.put(XSLFPictureData.PictureType.BMP, MediaType.IMAGE_BMP);
        PICTURETYPETOMEDIATYPEMAP.put(XSLFPictureData.PictureType.TIFF, MediaType.IMAGE_TIFF);
        PICTURETYPETOMEDIATYPEMAP.put(XSLFPictureData.PictureType.GIF, MediaType.IMAGE_GIF);

        BORDEREDGEMAP.put(org.apache.poi.sl.usermodel.TableCell.BorderEdge.left, Border.Edge.LEFT);
        BORDEREDGEMAP.put(org.apache.poi.sl.usermodel.TableCell.BorderEdge.top, Border.Edge.TOP);
        BORDEREDGEMAP.put(org.apache.poi.sl.usermodel.TableCell.BorderEdge.right, Border.Edge.RIGHT);
        BORDEREDGEMAP.put(org.apache.poi.sl.usermodel.TableCell.BorderEdge.bottom, Border.Edge.BOTTOM);

        TEXTALIGNMAP.put(TextParagraph.TextAlign.LEFT, TextAlign.LEFT);
        TEXTALIGNMAP.put(TextParagraph.TextAlign.CENTER, TextAlign.CENTER);
        TEXTALIGNMAP.put(TextParagraph.TextAlign.RIGHT, TextAlign.RIGHT);
        TEXTALIGNMAP.put(TextParagraph.TextAlign.JUSTIFY, TextAlign.JUSTIFY);
        TEXTALIGNMAP.put(TextParagraph.TextAlign.JUSTIFY_LOW, TextAlign.JUSTIFY);
        TEXTALIGNMAP.put(TextParagraph.TextAlign.DIST, TextAlign.DISTRIBUTE);

        VERTICALALIGNMAP.put(VerticalAlignment.TOP, VerticalAlign.TOP);
        VERTICALALIGNMAP.put(VerticalAlignment.MIDDLE, VerticalAlign.MIDDLE);
        VERTICALALIGNMAP.put(VerticalAlignment.BOTTOM, VerticalAlign.BOTTOM);
        VERTICALALIGNMAP.put(VerticalAlignment.DISTRIBUTED, VerticalAlign.DISTRIBUTE);
        VERTICALALIGNMAP.put(VerticalAlignment.JUSTIFIED, VerticalAlign.JUSTIFY);
    }

    public PptxReader() {
        this.inputStream = null;
    }

    public PptxReader(InputStream in) {
        this.inputStream = in;
    }

    private MediaType pictureTypeToMediaType(XSLFPictureData.PictureType pictureType) {
        return PICTURETYPETOMEDIATYPEMAP.get(pictureType);
    }

    @Override
    public List<Slide> readSlides() {
        List<Slide> slides = new ArrayList<>();
        for(XSLFSlide xslfSlide : xmlSlideShow.getSlides()) {
            slides.add(toSlide(xslfSlide));
        }
        return slides;
    }

    @Override
    public Slide readSlide(int index) {
        return toSlide(xmlSlideShow.getSlides().get(index));
    }

    @Override
    public Image readSlidAsImage(int index) {
        return toImage(xmlSlideShow.getSlides().get(index));
    }

    @Override
    public void open() {
        try {
            xmlSlideShow = (inputStream == null) ? new XMLSlideShow() : new XMLSlideShow(inputStream);
            getSlideLayouts();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    private SlideLayout toSlideLayout(XSLFSlideLayout xslfSlideLayout) {
        SlideLayout slideLayout = new SlideLayout();
        slideLayout.setName(xslfSlideLayout.getName());

        XSLFSlideMaster xslfSlideMaster = xslfSlideLayout.getSlideMaster();
        for(XSLFShape shape : xslfSlideMaster.getShapes()) {
            if(shape.getPlaceholder()!=null) continue;
            Element element = toElement(shape);
            slideLayout.addElement(element);
        }

        for(XSLFShape shape : xslfSlideLayout.getShapes()) {
            Element element = toElement(shape);
            slideLayout.addElement(element);
        }
        slideLayout.setBackground(getBackground(xslfSlideLayout));

        return slideLayout;
    }

    private Slide toSlide(XSLFSlide xslfSlide) {
        SlideLayout slideLayout = slideLayouts.get(xslfSlide.getSlideLayout());
        Slide slide = new Slide();
        slide.setTitle(xslfSlide.getTitle());
        slide.setBackground(getBackground(xslfSlide));

        for(XSLFShape xslfShape : xslfSlide.getShapes()) {
            Element element = toElement(xslfShape);
            slide.addElement(element);
        }

        slide.apply(slideLayout);

        return slide;
    }

    private Image getBackground(XSLFSlide xslfSlide) {
        Image image;
        try {
            XSLFBackground xslfBackground = xslfSlide.getSlideLayout().getBackground();
            CTBackground xmlBg = (CTBackground) xslfBackground.getXmlObject();
            String relId = xmlBg.getBgPr().getBlipFill().getBlip().getEmbed();

            XSLFPictureData pictureData = (XSLFPictureData) xslfSlide.getSlideLayout().getRelationById(relId);
            image = new Image(pictureData.getData(), pictureTypeToMediaType(pictureData.getType()));

            Rectangle2D anchor = xslfBackground.getAnchor();
            Rectangle rect = new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
            image.setRect(rect);
        } catch(NullPointerException e) {
            image = null;
        }

        return image;
    }

    private Image getBackground(XSLFSlideLayout xslfSlideLayout) {
        Image image;
        try {
            XSLFBackground xslfBackground = xslfSlideLayout.getBackground();
            CTBackground xmlBg = (CTBackground) xslfBackground.getXmlObject();
            String relId = xmlBg.getBgPr().getBlipFill().getBlip().getEmbed();

            XSLFPictureData pictureData = (XSLFPictureData) xslfSlideLayout.getRelationById(relId);
            image = new Image(pictureData.getData(), pictureTypeToMediaType(pictureData.getType()));

            Rectangle2D anchor = xslfBackground.getAnchor();
            Rectangle rect = new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
            image.setRect(rect);
        } catch(NullPointerException e) {
            image = null;
        }

        return image;
    }

    private Placeholder toPlaceholder(XSLFAutoShape xslfAutoShape) {
        Placeholder placeholder = new Placeholder();
        if(xslfAutoShape.getPlaceholder()!=null) placeholder.setName(xslfAutoShape.getPlaceholder().name());

        Rectangle2D anchor = xslfAutoShape.getAnchor();
        Rectangle rect = new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
        placeholder.setRect(rect);

        placeholder.setPaddingLeft(xslfAutoShape.getLeftInset());
        placeholder.setPaddingRight(xslfAutoShape.getRightInset());
        placeholder.setPaddingTop(xslfAutoShape.getTopInset());
        placeholder.setPaddingBottom(xslfAutoShape.getBottomInset());

        placeholder.setVerticalAlign(toVerticalAlign(xslfAutoShape.getVerticalAlignment()));

        for(XSLFTextParagraph xslfTextParagraph: xslfAutoShape.getTextParagraphs()) {
            placeholder.setTextAlign(toTextAlign(xslfTextParagraph.getTextAlign()));
            if(xslfTextParagraph.getTextRuns().size()>0) {
                XSLFTextRun xslfTextRun = xslfTextParagraph.getTextRuns().get(0);
                placeholder.setFontFamily(toFontFamily(xslfTextRun.getFontFamily()));
                placeholder.setFontSize(xslfTextRun.getFontSize());
                placeholder.setFontWeight(xslfTextRun.isBold() ? FontWeight.BOLD : FontWeight.NORMAL);
                placeholder.setColor(toColor(xslfTextRun.getFontColor()));
            }
            Paragraph paragraph = toParagraph(xslfTextParagraph);
            placeholder.addParagraph(paragraph);
        }

        return placeholder;
    }

    private Element toElement(XSLFShape xslfShape) {
        Element element;

        if(xslfShape instanceof XSLFAutoShape) {
            if(xslfShape instanceof XSLFTextBox) {
                element = toTextBox((XSLFTextBox) xslfShape);
            } else if(xslfShape instanceof XSLFFreeformShape) {
                element = toImage(xslfShape);
            } else if(xslfShape instanceof XSLFFreeformShape) {
                element = toImage(xslfShape);
            } else if(((XSLFAutoShape) xslfShape).getFillStyle().getPaint()!=null) {
                element = toImage(xslfShape);
            } else {
                element = toPlaceholder((XSLFAutoShape) xslfShape);
            }
        } else if(xslfShape instanceof XSLFTable) {
            element = toTable((XSLFTable) xslfShape);
        } else {
            element = toImage(xslfShape);
        }

        return element;
    }

    private TextBox toTextBox(XSLFTextShape xslfTextShape) {
        TextBox textBox = new TextBox();
        for(XSLFTextParagraph xslfTextParagraph: xslfTextShape.getTextParagraphs()) {
            Paragraph paragraph = toParagraph(xslfTextParagraph);
            textBox.addParagraph(paragraph);
        }

        Rectangle2D anchor = xslfTextShape.getAnchor();
        if(anchor!=null) {
            Rectangle rect = new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
            textBox.setRect(rect);
        }

        textBox.setPaddingLeft(xslfTextShape.getLeftInset());
        textBox.setPaddingRight(xslfTextShape.getRightInset());
        textBox.setPaddingTop(xslfTextShape.getTopInset());
        textBox.setPaddingBottom(xslfTextShape.getBottomInset());

        textBox.setVerticalAlign(toVerticalAlign(xslfTextShape.getVerticalAlignment()));

        return textBox;

    }

    private VerticalAlign toVerticalAlign(VerticalAlignment xslfVerticalAlignment) {
        return VERTICALALIGNMAP.get(xslfVerticalAlignment);
    }

    private Paragraph toParagraph(XSLFTextParagraph xslfTextParagraph) {
        Paragraph paragraph = new Paragraph();
        paragraph.setFontFamily(toFontFamily(xslfTextParagraph.getDefaultFontFamily()));
        paragraph.setFontSize(toFontSize(xslfTextParagraph.getDefaultFontSize()));
        paragraph.setTextAlign(toTextAlign(xslfTextParagraph.getTextAlign()));
        if(xslfTextParagraph.getBulletStyle()!=null) {
            paragraph.setMarker(xslfTextParagraph.getBulletStyle().getBulletCharacter());
        }

        for(org.apache.poi.sl.usermodel.TextRun textRun : xslfTextParagraph.getTextRuns()) {
            TextRun phrase = toTextRun(textRun);
            paragraph.addTextRun(phrase);
        }

        return paragraph;
    }


    private TextRun toTextRun(org.apache.poi.sl.usermodel.TextRun pptxTextRun) {
        TextRun textRun = new TextRun(pptxTextRun.getRawText());
        textRun.setFontFamily(toFontFamily(pptxTextRun.getFontFamily()));
        textRun.setFontSize(toFontSize(pptxTextRun.getFontSize()));
        textRun.setFontWeight(pptxTextRun.isBold() ? FontWeight.BOLD : FontWeight.NORMAL);
        textRun.setItalic(pptxTextRun.isItalic());
        textRun.setUnderlined(pptxTextRun.isUnderlined());
        textRun.setStrikethrough(pptxTextRun.isStrikethrough());
        textRun.setSuperscript(pptxTextRun.isSuperscript());
        textRun.setSubscript(pptxTextRun.isSubscript());
        textRun.setColor(toColor(pptxTextRun.getFontColor()));
        return textRun;
    }

    private Color toColor(PaintStyle pptxColor) {
        if(pptxColor==null) return null;

        int rgb = ((PaintStyle.SolidPaint) pptxColor).getSolidColor().getColor().getRGB();

        Color color = new Color(rgb);

        return color;
    }

    private Color toColor(java.awt.Color pptxColor) {
        if(pptxColor==null) return null;

        Color color = new Color(pptxColor.getRGB());

        return color;
    }

    private FontFamily toFontFamily(String pptxFontFamily) {
        if(pptxFontFamily==null) return null;

        FontFamily fontFamily = new FontFamily(pptxFontFamily);

        return fontFamily;
    }

    public Double toFontSize(Double pptxFontSize) {
        if(pptxFontSize==null || pptxFontSize==0) return null;

        return pptxFontSize;
    }

    private TextAlign toTextAlign(TextParagraph.TextAlign pptxTextAlign) {
        return TEXTALIGNMAP.get(pptxTextAlign);
    }

    private Image toImage(XSLFShape xslfShape) {
        Image image;
        Rectangle2D anchor = xslfShape.getAnchor();
        if(xslfShape instanceof XSLFPictureShape) {
            PictureData pictureData = ((XSLFPictureShape) xslfShape).getPictureData();
            image = new Image(pictureData.getData(), pictureTypeToMediaType(pictureData.getType()));
            image.setDimension(toDimension(pictureData.getImageDimension()));
        } else {
            image = new Image(toPNG(xslfShape), MediaType.IMAGE_PNG);
            image.setDimension(new Dimension(anchor.getWidth(), anchor.getHeight()));
        }

        image.setRect(toRectangle(anchor));

        return image;
    }

    private Image toImage(XSLFSlide xslfSlide) {
        Image image = new Image(toPNG(xslfSlide), MediaType.IMAGE_PNG);
        java.awt.Dimension pageSize = xslfSlide.getSlideShow().getPageSize();

        image.setDimension(toDimension(pageSize));
        image.setRect(new Rectangle(0, 0, pageSize.getWidth(), pageSize.getHeight()));

        return image;
    }

    private Rectangle toRectangle(Rectangle2D anchor) {
        if(anchor==null) return null;
        return new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
    }


    private byte[] toPNG(XSLFShape xslfShape) {
        byte[] bytes;

        Rectangle2D anchor = xslfShape.getAnchor();

        double height = Math.min(1, anchor.getHeight());
        double width = Math.min(1, anchor.getWidth());
        BufferedImage img = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = img.createGraphics();

        Rectangle2D canvas = new Rectangle2D.Double(0, 0, anchor.getWidth(), anchor.getHeight());
        //clear the drawing area
        graphics.setPaint(java.awt.Color.white);
        graphics.fill(canvas);

        //render
        xslfShape.draw(graphics, canvas);

        //creating an image file as output
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            javax.imageio.ImageIO.write(img, "png", out);
            bytes = out.toByteArray();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                throw new org.uweaver.core.exception.IOException(e);
            }
        }

        return bytes;
    }

    private byte[] toPNG(XSLFSlide xslfSlide) {
        byte[] bytes;

        java.awt.Dimension pageSize = xslfSlide.getSlideShow().getPageSize();

        BufferedImage img = new BufferedImage((int) pageSize.getWidth(), (int) pageSize.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = img.createGraphics();

        Rectangle2D canvas = new Rectangle2D.Double(0, 0, pageSize.getWidth(), pageSize.getHeight());
        //clear the drawing area
        graphics.setPaint(java.awt.Color.white);
        graphics.fill(canvas);

        //render
        xslfSlide.draw(graphics);

        //creating an image file as output
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            javax.imageio.ImageIO.write(img, "png", out);
            bytes = out.toByteArray();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                throw new org.uweaver.core.exception.IOException(e);
            }
        }

        return bytes;
    }

    private Dimension toDimension(java.awt.Dimension pptxDimension) {
        return (pptxDimension==null) ? null : new Dimension(pptxDimension.getWidth(), pptxDimension.getHeight());
    }

    private Table toTable(XSLFTable xslfTable) {
        Table table = new Table();
        Rectangle2D anchor = xslfTable.getAnchor();
        if(anchor!=null) {
            Rectangle rect = new Rectangle(anchor.getX(), anchor.getY(), anchor.getWidth(), anchor.getHeight());
            table.setRect(rect);
        }
        for(XSLFTableRow xslfTableRow : xslfTable.getRows()) {
            table.addRow(toTableRow(xslfTableRow));
        }

        for(int i=0; i<xslfTable.getNumberOfColumns(); i++) {
            table.setColumnWidth(i, xslfTable.getColumnWidth(i));
        }
        return table;
    }

    private TableRow toTableRow(XSLFTableRow xslfTableRow) {
        TableRow tableRow = new TableRow();
        tableRow.setHeight(xslfTableRow.getHeight());
        for(XSLFTableCell xslfTableCell : xslfTableRow.getCells()) {
            tableRow.addCell(toTableCell(xslfTableCell));
        }
        return tableRow;
    }

    private TableCell toTableCell(XSLFTableCell xslfTableCell) {
        TableCell tableCell = new TableCell();
        tableCell.setPaddingLeft(xslfTableCell.getLeftInset()).setPaddingTop(xslfTableCell.getTopInset())
                .setPaddingRight(xslfTableCell.getRightInset()).setPaddingBottom(xslfTableCell.getBottomInset())
                .setVerticalAlign(toVerticalAlign(xslfTableCell.getVerticalAlignment()));
        for(Map.Entry<org.apache.poi.sl.usermodel.TableCell.BorderEdge, Border.Edge> entry : BORDEREDGEMAP.entrySet()) {
            tableCell.setBorderWidth(entry.getValue(), xslfTableCell.getBorderWidth(entry.getKey()));
            tableCell.setBorderColor(entry.getValue(), toColor(xslfTableCell.getBorderColor(entry.getKey())));
        }
        for(XSLFTextParagraph xslfTextParagraphs : xslfTableCell.getTextParagraphs()) {
            tableCell.addParagraph(toParagraph(xslfTextParagraphs));
        }
        return tableCell;
    }

    private Border.Edge toBorderEdge(org.apache.poi.sl.usermodel.TableCell.BorderEdge edge) {
        return BORDEREDGEMAP.get(edge);
    }

    private XSLFTextShape getXSLFPlaceholderByPlaceholder(XSLFSlide xslfSlide, Placeholder placeholder) {
        XSLFTextShape xslfPlaceholder = null;

        for(XSLFTextShape shape : xslfSlide.getPlaceholders()) {
            if(shape.getPlaceholder().name().equals(placeholder.getName())&&shape.getAnchor().equals(placeholder.getRect())) {
                xslfPlaceholder = shape;
                break;
            }
        }

        return xslfPlaceholder;
    }

    @Override
    public List<SlideLayout> getSlideLayouts() {
        List<SlideLayout> slideLayouts = new ArrayList<>();

        if(this.slideLayouts.size()==0) {
            XSLFSlideMaster slideMaster = xmlSlideShow.getSlideMasters().get(0);
            for(XSLFSlideLayout xslfSlideLayout : slideMaster.getSlideLayouts()) {
                this.slideLayouts.put(xslfSlideLayout, toSlideLayout(xslfSlideLayout));
            }
        }

        slideLayouts.addAll(this.slideLayouts.values());

        return slideLayouts;
    }


    @Override
    public void close() {
        try {
            xmlSlideShow.close();
            inputStream.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public Dimension getPageSize() {
        java.awt.Dimension pageSize = xmlSlideShow.getPageSize();
        return new Dimension(pageSize.getWidth(), pageSize.getHeight());
    }
}
