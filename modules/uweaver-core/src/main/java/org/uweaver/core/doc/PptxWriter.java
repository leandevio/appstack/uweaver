/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import java.io.IOException;
import java.io.OutputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PptxWriter obj = new PptxWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PptxWriter implements SlideShowWriter {
    OutputStream out;
    Double marginLeft = null;
    Double marginTop = null;
    Double marginRight = null;
    Double marginBottom = null;

    XMLSlideShow xmlSlideShow = null;

    public PptxWriter(OutputStream out) {
        init(out);
    }

    private void init(OutputStream out) {
        this.out = out;
        xmlSlideShow = new XMLSlideShow();
    }

    @Override
    public Dimension getPageSize() {
        java.awt.Dimension pptxPageSize = xmlSlideShow.getPageSize();
        return new Dimension(pptxPageSize.getWidth(), pptxPageSize.getHeight());
    }

    @Override
    public PptxWriter setPageSize(Dimension pageSize) {
        java.awt.Dimension pptxPageSize = new java.awt.Dimension((int) pageSize.getWidth(), (int) pageSize.getHeight());
        xmlSlideShow.setPageSize(pptxPageSize);
        return this;
    }

    @Override
    public PptxWriter setMargins(Double left, Double top, Double right, Double bottom) {
        this.marginLeft = left; this.marginTop = top; this.marginRight = right; this.marginBottom = bottom;
        return this;
    }

    @Override
    public Double getMarginLeft() {
        return marginLeft;
    }

    @Override
    public PptxWriter setMarginLeft(Double margin) {
        this.marginLeft = margin;
        return this;
    }

    @Override
    public Double getMarginTop()
    {
        return marginTop;
    }

    @Override
    public PptxWriter setMarginTop(Double margin) {
        this.marginTop = margin;
        return this;
    }

    @Override
    public Double getMarginRight() {
        return marginRight;
    }

    @Override
    public PptxWriter setMarginRight(Double margin) {
        this.marginRight = margin;
        return this;
    }

    @Override
    public Double getMarginBottom() {
        return marginBottom;
    }

    @Override
    public PptxWriter setMarginBottom(Double margin) {
        marginBottom = margin;
        return this;
    }

    @Override
    public void write(Slide slide) {

    }

    @Override
    public void open() {
    }

    @Override
    public void close() {
        try {
            xmlSlideShow.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

}
