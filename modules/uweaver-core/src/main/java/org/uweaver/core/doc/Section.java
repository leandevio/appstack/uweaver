/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Section obj = new Section();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Section {
    private FontFamily fontFamily = null;
    private Double fontSize = null;
    private FontWeight fontWeight = null;
    private Color color = null;
    private TextAlign textAlign = null;
    
    private TextElement container = null;
    
    private List<Paragraph> paragraphs = new ArrayList<>();

    public void addParagraph(Paragraph paragraph) {
        paragraphs.add(paragraph);
        paragraph.setContainer(this);
    }

    public void removeParagraph(Paragraph paragraph) {
        paragraphs.remove(paragraph);
        paragraph.setContainer(null);
    }

    public List<Paragraph> getParagraphs() {
        return Collections.unmodifiableList(paragraphs);
    }

    public void clear() {
        paragraphs.clear();
    }

    public void setText(String text) {
        paragraphs.clear();
        if(text==null) return;
        Paragraph paragraph = new Paragraph(text);
        paragraphs.add(paragraph);
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        int len;

        for(Paragraph paragraph : paragraphs) {
            sb.append(paragraph.getText()).append('\n');
        }
        len = sb.length();

        return len>0 ? sb.substring(0, len-1) : "";
    }
    
    public TextElement getContainer() {
        return container;
    }
    
    public void setContainer(TextElement container) {
        this.container = container;
    }

    public FontFamily getFontFamily() {
        if(fontFamily==null) {
            return (getContainer()==null) ? FontFamily.DEFAULT : getContainer().getFontFamily();
        }
        return fontFamily;
    }

    public Section setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public FontWeight getFontWeight() {
        if(fontWeight==null) {
            return (getContainer()==null) ? FontWeight.NORMAL : getContainer().getFontWeight();
        }
        return fontWeight;
    }

    public Section setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    public Double getFontSize() {
        if(fontSize==null) {
            return (getContainer()==null) ? 12d : getContainer().getFontSize();
        }
        return fontSize;
    }

    public Section setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public Color getColor() {
        if(color==null) {
            return (getContainer()==null) ? Color.BLACK : getContainer().getColor();
        }
        return color;
    }

    public Section setColor(Color color) {
        this.color = color;
        return this;
    }

    public TextAlign getTextAlign() {
        if(textAlign==null) {
            return (getContainer()==null) ? TextAlign.LEFT : getContainer().getTextAlign();
        }
        return textAlign;
    }

    public Section setTextAlign(TextAlign textAlign) {
        this.textAlign = textAlign;
        return this;
    }


}
