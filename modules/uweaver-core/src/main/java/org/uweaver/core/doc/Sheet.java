/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Spreadsheet obj = new Spreadsheet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Sheet {
    private Object rawData;
    private String name;
    private Map<Integer, SheetRow> rows = new HashMap<>();

    public Sheet(String name) {
        this(name, null);
    }

    public Sheet(String name, Object rawData) {
        this.name = name;
        this.rawData = rawData;
    }

    public SheetRow getRow(int rownum) {
        SheetRow row = rows.get(rownum);
        if(row==null) {
            row = createSheetRow(rownum);
        }
        return row;
    }

    public Collection<SheetRow> getRows() {
        return rows.values();
    }

    public Object getRawData() {
        return rawData;
    }

    public String getName() {
        return name;
    }

    public SheetRow createSheetRow(int rownum) {
        SheetRow row = new SheetRow(this);
        rows.put(rownum, row);
        return row;
    }

    public Integer getRowNum(SheetRow row) {
        Integer rownum = null;
        for(Map.Entry<Integer, SheetRow> entry : rows.entrySet()) {
            if(row.equals(entry.getValue())) {
                rownum = entry.getKey();
                break;
            }
        }
        return rownum;
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        for(SheetRow row : this.rows.values()) {
            sb.append(row.getText()).append('\n');
        }
        return sb.toString();
    }
}
