/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SheetRow obj = new SheetRow();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SheetRow {
    private Sheet sheet;
    private Map<Integer, SheetCell> cells = new HashMap<>();

    protected SheetRow(Sheet sheet) {
        this.sheet = sheet;
    }

    public SheetCell getCell(int cellnum) {
        SheetCell cell = cells.get(cellnum);
        if(cell==null) {
            cell = createSheetCell(cellnum);
        }

        return cell;
    }

    public SheetCell createSheetCell(int cellnum) {
        SheetCell cell = new SheetCell(this);
        cells.put(cellnum, cell);
        return cell;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public int getRowNum() {
        return getSheet().getRowNum(this);
    }

    public Collection<SheetCell> getCells() {
        return cells.values();
    }

    public Integer getCellNum(SheetCell cell) {
        Integer cellNum = null;
        for(Map.Entry<Integer, SheetCell> entry : cells.entrySet()) {
            if(entry.getValue().equals(cell)) {
                cellNum = entry.getKey();
            }
        }
        return cellNum;
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        for(SheetCell cell : this.cells.values()) {
            sb.append(cell.getText()).append('\t');
        }
        return sb.toString();
    }

}
