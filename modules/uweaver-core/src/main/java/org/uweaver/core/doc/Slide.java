/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.uweaver.core.util.MediaType;

import java.io.InputStream;
import java.util.*;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Slide obj = new Slide();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Slide {
    private String title;
    private SlideLayout slideLayout = null;
    private java.util.List<Element> templates = new ArrayList<>();
    private java.util.List<Element> elements = new ArrayList<>();
    private Image background = null;
    private Locale defaultLocale = null;
    private SlideShow parent;

    public Slide() {}

    public Slide(SlideLayout slideLayout) {
        apply(slideLayout);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Placeholder getPlaceholder(String name) {
        Placeholder result = null;

        for(Element element: elements) {
            if(!Placeholder.class.isAssignableFrom(element.getClass())) continue;
            Placeholder placeholder = (Placeholder) element;
            if(name==null) {
                if(placeholder.getName()==null) {
                    result = placeholder;
                    break;
                }
            } else if(name.equals(placeholder.getName())) {
                result = placeholder;
                break;
            }
        }

        return result;
    }

    public java.util.List<Placeholder> getPlaceholders(String name) {
        java.util.List<Placeholder> placeholders = new ArrayList<>();

        for(Element element: elements) {
            if(!Placeholder.class.isAssignableFrom(element.getClass())) continue;
            Placeholder placeholder = (Placeholder) element;
            if(name==null) {
                if(placeholder.getName()==null) {
                    placeholders.add(placeholder);
                }
            } else if(name.equals(placeholder.getName())) {
                placeholders.add(placeholder);
            }
        }

        return placeholders;
    }

    public Image createImage(InputStream in, MediaType mediaType) {
        return new Image(in, mediaType);
    }

    public SlideLayout getSlideLayout() {
        return slideLayout;
    }

    public void apply(SlideLayout slideLayout) {
        this.slideLayout = slideLayout;

        // remove the templates except those are modified (i.e, the placeholders with contents).
        java.util.List<Element> templates = new ArrayList<>();
        for(Element element : templates) {
            if(Placeholder.class.isAssignableFrom(element.getClass())) {
                if(((Placeholder) element).isEmpty()) templates.add(element);
            } else {
                templates.add(element);
            }
        }
        elements.removeAll(templates);

        // add the templates from the slide layout except those are already in the slide (the placeholders with the same name).
        templates.clear();
        for(Element element : slideLayout.getElements()) {
            if(Placeholder.class.isAssignableFrom(element.getClass())) {
                Placeholder placeholder = (Placeholder) element;
                if(getPlaceholder(placeholder.getName())==null) {
                    templates.add(((Placeholder) element).clone());
                }
            } else {
                templates.add(element);
            }
        }
        elements.addAll(0, templates);

        this.templates = slideLayout.getElements();
        background = slideLayout.getBackground();

    }

    public Table createTable(int rows, int columns) {
        Table table = new Table(rows, columns);

        return table;
    }

    public java.util.List<Placeholder> getPlaceholders() {
        java.util.List<Placeholder> placeholders = new ArrayList<>();

        for(Element element: templates) {
            if(Placeholder.class.isAssignableFrom(element.getClass())) {
                placeholders.add((Placeholder) element);
            }
        }

        return placeholders;
    }


    public java.util.List<Element> getElements() {
        return Collections.unmodifiableList(elements);
    }


    public void addElement(Element element) {
        elements.add(element);
    }

    public void addElements(List<Element> elements) {
        this.elements.addAll(elements);
    }

    public void removerElement(Element element) {
        elements.remove(element);
    }

    public Image getBackground() {
        return background;
    }

    public void setBackground(Image background) {
        this.background = background;
    }

    public Locale getDefaultLocale() {
        return (defaultLocale==null) ? getParent().getDefaultLocale() : defaultLocale;
    }

    public void setDefaultLocale(Locale locale) {
        this.defaultLocale = defaultLocale;
    }

    public SlideShow getParent() {
        return parent;
    }

    protected void setParent(SlideShow parent) {
        this.parent = parent;
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        for(Element element : getElements()) {
            if(element instanceof TextElement) {
                sb.append(((TextElement)element).getText());
            }
        }
        return sb.toString();
    }

}
