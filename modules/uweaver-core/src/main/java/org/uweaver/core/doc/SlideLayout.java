/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SlideLayout obj = new SlideLayout();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SlideLayout {
    private String name;
    List<Element> elements = new ArrayList<>();
    Image background = null;

    public String getName() {
        return name;
    }

    public void addElement(Element element) {
        elements.add(element);
    }

    public void addElements(List<Element> elements) {
        this.elements.addAll(elements);
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Element> getElements() {
        return Collections.unmodifiableList(elements);
    }

    public Placeholder createPlaceholder(String type) {
        Placeholder placeholder = new Placeholder(type);
        addElement(placeholder);
        return placeholder;
    }

    public Image getBackground() {
        return background;
    }

    public void setBackground(Image background) {
        this.background = background;
    }
}
