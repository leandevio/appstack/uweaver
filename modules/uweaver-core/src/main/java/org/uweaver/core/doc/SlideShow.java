/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SlideShow obj = new SlideShow();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SlideShow {
    private SlideShowReader reader = null;
    private SlideShowWriter writer = null;
    private Dimension pageSize;
    private List<SlideLayout> slideLayouts;
    private List<Slide> slides;
    private Locale defaultLocale = Locale.getDefault();

    public SlideShow(SlideShowReader reader, SlideShowWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public SlideShow(SlideShowReader reader) {
        this.reader = reader;
    }

    public SlideShow(SlideShowWriter writer) {
        this.writer = writer;
    }

    public SlideShow() {}

    public List<Slide> getSlides() {
        if(slides==null) {
            slides = this.reader.readSlides();
            for(Slide slide : slides) {
                slide.setParent(this);
            }
        }
        return slides;
    }

    public void open() {
        reader.open();
        slides = null;
        setPageSize(reader.getPageSize());
        if(writer!=null) writer.open();
    }

    public void close() {
        reader.close();
        flush();
        writer.close();
    }

    public void setPageSize(Dimension pageSize) {
        this.pageSize = pageSize;
        if(writer!=null) writer.setPageSize(pageSize);
    }

    public Dimension getPageSize() {
        return pageSize;
    }

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(Locale defaultLocale) {
        this.defaultLocale = defaultLocale;
    }


    private void flush() {
        for(Slide slide: getSlides()) {
            writer.write(slide);
        }
    }

    public SlideLayout getSlideLayout(String name) {
        SlideLayout which = null;

        for(SlideLayout slideLayout : getSlideLayouts()) {
            if(slideLayout.getName().equals(name)) {
                which = slideLayout;
                break;
            }
        }

        return which;
    }
    public List<SlideLayout> getSlideLayouts() {
        if(slideLayouts==null) {
            slideLayouts = reader.getSlideLayouts();
        }

        return Collections.unmodifiableList(slideLayouts);
    }

    public Slide createSlide(String layout) {
        Slide slide = new Slide(getSlideLayout(layout));
        slide.setParent(this);
        getSlides().add(slide);
        return slide;
    }
}
