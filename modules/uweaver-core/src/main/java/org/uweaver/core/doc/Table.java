/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Table obj = new Table();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Table implements TextElement {
    private Rectangle rect;
    private List<TableRow> rows = new ArrayList<>();
    private Map<Integer, Double> columnWidth = new HashMap<>();
    private Double rowHeight = null;

    private Double paddingLeft = null;
    private Double paddingRight = null;
    private Double paddingTop = null;
    private Double paddingBottom = null;
    private VerticalAlign verticalAlign = null;

    private FontFamily fontFamily = null;
    private Double fontSize = null;
    private FontWeight fontWeight = null;
    private Color color = null;
    private TextAlign textAlign = null;

    private Map<Border.Edge, Double> borderWidth = new HashMap<>();
    private Map<Border.Edge, Color> borderColor = new HashMap<>();


    public Table() {}

    public Table(int columns, int rows) {
        for(int i = 0; i<rows; i++) {
            TableRow row = createRow();
            for(int j = 0; j<columns; j++) {
                row.createCell();
            }
        }
    }


    public TableRow createRow() {
        TableRow row = new TableRow();
        rows.add(row);
        row.setParent(this);
        return row;
    }

    public List<TableRow> getRows() {
        return rows;
    }

    public TableRow getRow(int i) {
        return rows.get(i);
    }

    public Table setColumnWidth(int column, double width) {
        columnWidth.put(column, width);
        return this;
    }

    public Double getColumnWidth(int column) {
        return columnWidth.get(column);
    }

    public int getNumberOfColumns() {
        if(rows.size()==0) return 0;

        TableRow firstRow = rows.get(0);
        return firstRow.getCells().size();

    }


    public Table setBorderWidth(Border.Edge edge, double width) {
        borderWidth.put(edge, width);
        return this;
    }

    public Double getBorderWidth(Border.Edge edge) {
        return borderWidth.get(edge);
    }

    public Table setBorderColor(Border.Edge edge, Color color) {
        borderColor.put(edge, color);
        return this;
    }
    public Color getBorderColor(Border.Edge edge) {
        return borderColor.get(edge);
    }

    @Override
    public FontFamily getFontFamily() {
        return fontFamily;
    }

    @Override
    public Table setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    @Override
    public Double getPaddingLeft() {
        return paddingLeft;
    }

    @Override
    public Table setPaddingLeft(Double padding) {
        this.paddingLeft = padding;
        return this;
    }

    @Override
    public Double getPaddingRight() {
        return paddingRight;
    }

    @Override
    public Table setPaddingRight(Double padding) {
        this.paddingRight = padding;
        return this;
    }

    @Override
    public Double getPaddingTop() {
        return paddingTop;
    }

    @Override
    public Table setPaddingTop(Double padding) {
        this.paddingTop = padding;
        return this;
    }

    @Override
    public Double getPaddingBottom() {
        return paddingBottom;
    }

    @Override
    public Table setPaddingBottom(Double padding) {
        this.paddingBottom = padding;
        return this;
    }

    @Override
    public VerticalAlign getVerticalAlign() {
        return verticalAlign;
    }

    @Override
    public Table setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    @Override
    public TextAlign getTextAlign() {
        return textAlign;
    }

    @Override
    public Table setTextAlign(TextAlign textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    @Override
    public Double getFontSize() {
        return fontSize;
    }

    @Override
    public Table setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    @Override
    public FontWeight getFontWeight() {
        return fontWeight;
    }

    @Override
    public Table setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public Table setColor(Color color) {
        this.color = color;
        return this;
    }

    @Override
    public Rectangle getRect() {
        return rect;
    }

    @Override
    public void setRect(Rectangle rect) {
        this.rect = rect;
    }

    public void addRow(TableRow row) {
        rows.add(row);
        row.setParent(this);
    }

    public Table setRowHeight(double rowHeight) {
        this.rowHeight = rowHeight;
        return this;
    }

    public Double getRowHeight() {
        return rowHeight;
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        for(TableRow row : rows) {
            sb.append(row.getText()).append('\n');
        }
        return sb.toString();
    }
}
