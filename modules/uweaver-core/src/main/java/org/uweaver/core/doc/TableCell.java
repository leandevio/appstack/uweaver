/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Cell obj = new Cell();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TableCell implements TextElement {
    private java.util.List<Section> sections = new ArrayList<>();
    private Element content = null;

    private FontFamily fontFamily = null;
    private FontWeight fontWeight = null;
    private Double fontSize = null;
    private TextAlign textAlign = null;
    private Color color = null;

    private Double paddingLeft = null;
    private Double paddingRight = null;
    private Double paddingTop = null;
    private Double paddingBottom = null;
    private VerticalAlign verticalAlign = null;

    private Map<Border.Edge, Double> borderWidth = new HashMap<>();
    private Map<Border.Edge, Color> borderColor = new HashMap<>();
    private TableRow parent;

    public void setParent(TableRow parent) {
        this.parent = parent;
    }

    public TableCell setBorderWidth(Border.Edge edge, Double width) {
        borderWidth.put(edge, width);
        return this;
    }

    public Double getBorderWidth(Border.Edge edge) {
        return (borderWidth.containsKey(edge)) ? borderWidth.get(edge) : parent.getBorderWidth(edge);
    }

    public TableCell setBorderColor(Border.Edge edge, Color color) {
        borderColor.put(edge, color);
        return this;
    }

    public Color getBorderColor(Border.Edge edge) {
        return (borderColor.containsKey(edge)) ? borderColor.get(edge) : parent.getBorderColor(edge);
    }


    public void setContent(Element content) {
        this.content = content;
    }

    public Element getContent() {
        return content;
    }

    public void clear() {
        sections.clear();
        content = null;
    }

    public void setText(String text) {
        clear();
        if(text==null) return;
        addParagraph(new Paragraph(text));
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        int len;

        for(Section section : sections) {
            sb.append(section.getText()).append('\n');
        }
        len = sb.length();

        return len>0 ? sb.substring(0, len-1) : "";
    }

    public FontFamily getFontFamily() {
        if(fontFamily==null) {
            return (getParent()==null) ? FontFamily.DEFAULT : getParent().getFontFamily();
        }
        return fontFamily;
    }

    public TableCell setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public FontWeight getFontWeight() {
        if(fontWeight==null) {
            return (getParent()==null) ? FontWeight.NORMAL : getParent().getFontWeight();
        }
        return fontWeight;
    }

    public TableCell setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    public Double getFontSize() {
        if(fontSize==null) {
            return (getParent()==null) ? 12d : getParent().getFontSize();
        }
        return fontSize;
    }

    public TableCell setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public TextAlign getTextAlign() {
        if(textAlign==null) {
            return (getParent()==null) ? TextAlign.LEFT : getParent().getTextAlign();
        }
        return textAlign;
    }

    public TableCell setTextAlign(TextAlign textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    public Color getColor() {
        if(color==null) {
            return (getParent()==null) ? Color.BLACK : getParent().getColor();
        }
        return color;
    }

    public TableCell setColor(Color color) {
        this.color = color;
        return this;
    }
    
    public void addSection(Section section) {
        sections.add(section);
        section.setContainer(this);
    }

    public java.util.List<Section> getSections() {
        return Collections.unmodifiableList(sections);
    }

    public void removeSection(Section section) {
        sections.remove(section);
    }

    public void addParagraph(Paragraph paragraph) {
        getLastSection().addParagraph(paragraph);
    }

    public void addParagraphs(java.util.List<Paragraph> paragraphs) {
        for(Paragraph paragraph : paragraphs) {
            addParagraph(paragraph);
        }
    }

    public void removeParagraph(Paragraph paragraph) {
        for(Section section : sections) {
            section.removeParagraph(paragraph);
        }
    }

    public java.util.List<Paragraph> getParagraphs() {
        List<Paragraph> paragraphs = new ArrayList<>();

        for(Section section : sections) {
            paragraphs.addAll(section.getParagraphs());
        }
        return Collections.unmodifiableList(paragraphs);
    }

    private Section getLastSection() {
        int size = sections.size();
        Section section;
        if(size==0) {
            section = new Section();
            addSection(section);
        } else {
            section = sections.get(size-1);
        }
        return section;
    }


    @Override
    public Double getPaddingLeft() {
        return (paddingLeft==null && parent!=null) ? parent.getPaddingLeft() : paddingLeft;
    }

    @Override
    public TableCell setPaddingLeft(Double padding) {
        this.paddingLeft = padding;
        return this;
    }

    @Override
    public Double getPaddingRight() {
        return (paddingRight==null && parent!=null) ? parent.getPaddingRight() : paddingRight;
    }

    @Override
    public TableCell setPaddingRight(Double padding) {
        this.paddingRight = padding;
        return this;
    }

    @Override
    public Double getPaddingTop() {
        return (paddingTop==null && parent!=null) ? parent.getPaddingTop() : paddingTop;
    }

    @Override
    public TableCell setPaddingTop(Double padding) {
        this.paddingTop = padding;
        return this;
    }

    @Override
    public Double getPaddingBottom() {
        return (paddingBottom==null && parent!=null) ? parent.getPaddingBottom() : paddingBottom;
    }

    @Override
    public TableCell setPaddingBottom(Double padding) {
        this.paddingBottom = padding;
        return this;
    }

    @Override
    public VerticalAlign getVerticalAlign() {
        return (verticalAlign==null && parent!=null) ? parent.getVerticalAlign() : verticalAlign;
    }

    @Override
    public TableCell setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public void setRect(Rectangle rect) {}


    public TableRow getParent() {
        return parent;
    }
}
