/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Row obj = new Row();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TableRow implements TextElement {
    Table parent;

    List<TableCell> cells = new ArrayList<>();
    
    private FontFamily fontFamily = null;
    private FontWeight fontWeight = null;
    private Double fontSize = null;
    private TextAlign textAlign = null;
    private Color color = null;

    private Double paddingLeft = null;
    private Double paddingRight = null;
    private Double paddingTop = null;
    private Double paddingBottom = null;
    private VerticalAlign verticalAlign = null;
    
    private Map<Border.Edge, Double> borderWidth = new HashMap<>();
    private Map<Border.Edge, Color> borderColor = new HashMap<>();

    private Double height = null;

    public TableCell createCell() {
        TableCell cell = new TableCell();
        cells.add(cell);
        cell.setParent(this);
        return cell;
    }

    public List<TableCell> getCells() {
        return cells;
    }

    public TableCell getCell(int i) {
        return cells.get(i);
    }


    public TableRow setBorderWidth(Border.Edge edge, double width) {
        borderWidth.put(edge, width);
        return this;
    }

    public Double getBorderWidth(Border.Edge edge) {
        return (borderWidth.containsKey(edge)) ? borderWidth.get(edge) : parent.getBorderWidth(edge);
    }

    public TableRow setBorderColor(Border.Edge edge, Color color) {
        borderColor.put(edge, color);
        return this;
    }
    public Color getBorderColor(Border.Edge edge) {
        return (borderColor.containsKey(edge)) ? borderColor.get(edge) : parent.getBorderColor(edge);
    }

    public FontFamily getFontFamily() {
        if(fontFamily==null) {
            return (getParent()==null) ? FontFamily.DEFAULT : getParent().getFontFamily();
        }
        return fontFamily;
    }

    public TableRow setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public FontWeight getFontWeight() {
        if(fontWeight==null) {
            return (getParent()==null) ? FontWeight.NORMAL : getParent().getFontWeight();
        }
        return fontWeight;
    }

    public TableRow setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    public Double getFontSize() {
        if(fontSize==null) {
            return (getParent()==null) ? 12d : getParent().getFontSize();
        }
        return fontSize;
    }

    @Override
    public TableRow setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public Color getColor() {
        if(color==null) {
            return (getParent()==null) ? Color.BLACK : getParent().getColor();
        }
        return color;
    }

    public TableRow setColor(Color color) {
        this.color = color;
        return this;
    }

    public TextAlign getTextAlign() {
        if(textAlign==null) {
            return (getParent()==null) ? TextAlign.LEFT : getParent().getTextAlign();
        }
        return textAlign;
    }

    public TableRow setTextAlign(TextAlign textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public void setRect(Rectangle rect) {}

    @Override
    public Double getPaddingLeft() {
        return (paddingLeft==null && parent!=null) ? parent.getPaddingLeft() : paddingLeft;
    }

    @Override
    public TableRow setPaddingLeft(Double padding) {
        this.paddingLeft = padding;
        return this;
    }

    @Override
    public Double getPaddingRight() {
        return (paddingRight==null && parent!=null) ? parent.getPaddingRight() : paddingRight;
    }

    @Override
    public TableRow setPaddingRight(Double padding) {
        this.paddingRight = padding;
        return this;
    }

    @Override
    public Double getPaddingTop() {
        return (paddingTop==null && parent!=null) ? parent.getPaddingTop() : paddingTop;
    }

    @Override
    public TableRow setPaddingTop(Double padding) {
        this.paddingTop = padding;
        return this;
    }

    @Override
    public Double getPaddingBottom() {
        return (paddingBottom==null && parent!=null) ? parent.getPaddingBottom() : paddingBottom;
    }

    @Override
    public TableRow setPaddingBottom(Double padding) {
        this.paddingBottom = padding;
        return this;
    }

    @Override
    public VerticalAlign getVerticalAlign() {
        return (verticalAlign==null && parent!=null) ? parent.getVerticalAlign() : verticalAlign;
    }

    @Override
    public TableRow setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }


    public Table getParent() {
        return parent;
    }

    public void setParent(Table parent) {
        this.parent = parent;
    }

    public TableRow setHeight(Double height) {
        this.height = height;
        return this;
    }

    public Double getHeight() {
        return (height==null && parent!=null) ? parent.getRowHeight() : height;
    }

    public void addCell(TableCell cell) {
        cell.setParent(this);
        cells.add(cell);
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        for(TableCell cell : cells) {
            sb.append(cell.getText()).append('\t');
        }
        return sb.toString();
    }
}
