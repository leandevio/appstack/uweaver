package org.uweaver.core.doc;

/**
 * Created by jasonlin on 6/21/16.
 */
public enum TextAlign {
    LEFT("LEFT"), RIGHT("RIGHT"), CENTER("CENTER"), JUSTIFY("JUSTIFY"), DISTRIBUTE("DISTRIBUTE");
    private final String value;

    TextAlign(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }
}