/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;


/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Paragraph obj = new Paragraph();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TextRun {
    private String text;
    private FontFamily fontFamily = null;
    private FontWeight fontWeight = null;
    private Double fontSize = null;
    private Color color = null;

    private boolean italic = false;
    private boolean strikethrough = false;
    private boolean underlined = false;
    private boolean superscript = false;
    private boolean subscript = false;

    private Paragraph container;

    public TextRun(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public FontFamily getFontFamily() {
        if(fontFamily==null) {
            return (getContainer()==null) ? FontFamily.DEFAULT : getContainer().getFontFamily();
        }
        return fontFamily;
    }

    public TextRun setFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public FontWeight getFontWeight() {
        if(fontWeight==null) {
            return (getContainer()==null) ? FontWeight.NORMAL : getContainer().getFontWeight();
        }
        return fontWeight;
    }

    public TextRun setFontWeight(FontWeight fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    public Double getFontSize() {
        if(fontSize==null) {
            return (getContainer()==null) ? 12d : getContainer().getFontSize();
        }
        return fontSize;
    }

    public TextRun setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public Color getColor() {
        if(color==null) {
            return (getContainer()==null) ? Color.BLACK : getContainer().getColor();
        }
        return color;
    }

    public TextRun setColor(Color color) {
        this.color = color;
        return this;
    }

    public boolean isItalic() {
        return italic;
    }

    public TextRun setItalic(boolean italic) {
        this.italic = italic;
        return this;
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public TextRun setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
        return this;
    }

    public boolean isUnderlined() {
        return underlined;
    }

    public TextRun setUnderlined(boolean underlined) {
        this.underlined = underlined;
        return this;
    }

    public boolean isSuperscript() {
        return superscript;
    }

    public TextRun setSuperscript(boolean superscript) {
        this.superscript = superscript;
        return this;
    }

    public boolean isSubscript() {
        return subscript;
    }

    public TextRun setSubscript(boolean subscript) {
        this.subscript = subscript;
        return this;
    }

    protected void setContainer(Paragraph paragraph) {
        this.container = paragraph;
    }

    protected Paragraph getContainer() {
        return container;
    }
}
