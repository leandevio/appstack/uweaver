package org.uweaver.core.doc;

/**
 * Created by jasonlin on 8/2/16.
 */
public enum VerticalAlign {
    TOP("TOP"), MIDDLE("MIDDLE"), BOTTOM("BOTTOM"), DISTRIBUTE("DISTRIBUTE"), JUSTIFY("JUSTIFY");
    private final String value;

    VerticalAlign(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }
}
