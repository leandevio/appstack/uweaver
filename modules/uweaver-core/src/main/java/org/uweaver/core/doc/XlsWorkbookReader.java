/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import java.io.IOException;
import java.io.InputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * XlsWorkbookReader obj = new XlsWorkbookReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class XlsWorkbookReader implements WorkbookReader {
    private InputStream in;
    private HSSFWorkbook hssfWorkbook;

    public XlsWorkbookReader(InputStream in) {
        this.in = in;
    }

    public void open() {
        try {
            hssfWorkbook = new HSSFWorkbook(in);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public int getNumberOfSheets() {
        return hssfWorkbook.getNumberOfSheets();
    }

    @Override
    public int getSheetIndex(Sheet sheet) {
        return hssfWorkbook.getSheetIndex((HSSFSheet) sheet.getRawData());
    }

    @Override
    public Sheet getSheet(int index) {
        return toSheet(hssfWorkbook.getSheetAt(index));
    }

    @Override
    public Sheet getSheet(String name) {
        return toSheet(hssfWorkbook.getSheet(name));
    }

    public void close() {
        try {
            hssfWorkbook.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    private Sheet toSheet(HSSFSheet hssfSheet) {
        Sheet sheet = new Sheet(hssfSheet.getSheetName(), hssfSheet);
        int first = hssfSheet.getFirstRowNum();
        int last = hssfSheet.getLastRowNum();

        for(int rownum=first; rownum<=last; rownum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rownum);
            if(hssfRow==null) continue;
            SheetRow row = sheet.createSheetRow(rownum);
            copyHSSFRowToSheetRow(hssfRow, row);
        }

        return sheet;
    }

    private void copyHSSFRowToSheetRow(HSSFRow hssfRow, SheetRow row) {
        int first = hssfRow.getFirstCellNum();
        int last = hssfRow.getLastCellNum();
        for(int cellnum=first; cellnum<last; cellnum++) {
            HSSFCell hssfCell = hssfRow.getCell(cellnum);
            if(hssfCell==null) continue;
            SheetCell cell = row.createSheetCell(cellnum);
            copyHSSFCellToSheetCell(hssfCell, cell);
        }
    }

    private void copyHSSFCellToSheetCell(HSSFCell hssfCell, SheetCell cell) {
        int hssfCellType = hssfCell.getCellType();
        Object value;
        if(hssfCellType==Cell.CELL_TYPE_STRING) {
            value = hssfCell.getStringCellValue();
            if (value!=null && (((String) value).equalsIgnoreCase("NULL")||((String)value).length()==0)) {
                value = null;
            }
        } else if(hssfCellType==Cell.CELL_TYPE_NUMERIC) {
            if (DateUtil.isCellDateFormatted(hssfCell)) {
                value = hssfCell.getDateCellValue();
            } else {
                value = hssfCell.getNumericCellValue();
            }
        } else if(hssfCellType==Cell.CELL_TYPE_BOOLEAN) {
            value = hssfCell.getBooleanCellValue();
        } else if(hssfCellType==Cell.CELL_TYPE_BLANK) {
            value = null;
        } else {
            value = null;
        }
        cell.setValue(value);
    }

    protected HSSFWorkbook getHSSFWorkbook() {
        return hssfWorkbook;
    }
}
