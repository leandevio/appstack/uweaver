/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.joda.time.DateTime;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.DateTimeConverter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * XlsWorkbookWriter obj = new XlsWorkbookWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class XlsWorkbookWriter implements WorkbookWriter {
    private OutputStream out;
    private HSSFWorkbook hssfWorkbook;
    private Converters converters = new Converters();
    private CellStyle dateCellStyle;
    private String dateTimePattern = "yyyy/MM/dd HH:mm:ss";

    public XlsWorkbookWriter(OutputStream out) {
        this.out = out;
        hssfWorkbook = new HSSFWorkbook();
    }

    @Override
    public boolean canConnect(Class<? extends WorkbookReader> aClass) {
        return XlsWorkbookReader.class.equals(aClass);
    }

    @Override
    public void connect(WorkbookReader reader) {
        hssfWorkbook = ((XlsWorkbookReader) reader).getHSSFWorkbook();
    }

    @Override
    public void open() {
        dateCellStyle = hssfWorkbook.createCellStyle();
        CreationHelper helper = hssfWorkbook.getCreationHelper();
        dateCellStyle.setDataFormat(helper.createDataFormat().getFormat(dateTimePattern));
    }

    @Override
    public void close() {
        try {
            hssfWorkbook.write(out);
            out.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public void write(Sheet sheet) {
        HSSFSheet hssfSheet = hssfWorkbook.getSheet(sheet.getName());

        if(hssfSheet==null) {
            hssfSheet = hssfWorkbook.createSheet(sheet.getName());
        }

        copySheetToHSSFSheet(sheet, hssfSheet);
    }

    @Override
    public String getDateTimePattern() {
        return dateTimePattern;
    }

    @Override
    public void setDateTimePattern(String dateTimePattern) {

        this.dateTimePattern = dateTimePattern;
        DateTimeConverter dtConverter = (DateTimeConverter) converters.lookup(Date.class);
        dtConverter.setDateTimePattern(dateTimePattern);
        dateCellStyle = hssfWorkbook.createCellStyle();
        CreationHelper helper = hssfWorkbook.getCreationHelper();
        dateCellStyle.setDataFormat(helper.createDataFormat().getFormat(dateTimePattern));
    }

    private void copySheetToHSSFSheet(Sheet sheet, HSSFSheet hssfSheet) {
        for(SheetRow row : sheet.getRows()) {
            HSSFRow hssfRow = hssfSheet.getRow(row.getRowNum());
            if(hssfRow==null) hssfRow = hssfSheet.createRow(row.getRowNum());
            copySheetRowToHSSFRow(row, hssfRow);
        }
    }

    private void copySheetRowToHSSFRow(SheetRow row, HSSFRow hssfRow) {
        for(SheetCell cell : row.getCells()) {
            HSSFCell hssfCell = hssfRow.getCell(cell.getCellNum());
            if(hssfCell==null) hssfCell = hssfRow.createCell(cell.getCellNum());
            copySheetCellToHSSFCell(cell, hssfCell);
        }
    }

    private void copySheetCellToHSSFCell(SheetCell cell, HSSFCell hssfCell) {
        Object value = cell.getValue();
        if(value == null) {
            hssfCell.setCellValue("");
        } else if(value instanceof String) {
            hssfCell.setCellType(Cell.CELL_TYPE_STRING);
            hssfCell.setCellValue((String) value);
        } else if(value instanceof Number) {
            hssfCell.setCellType(Cell.CELL_TYPE_NUMERIC);
            hssfCell.setCellValue(converters.convert(value, Double.class));
        } else if(value instanceof Date || value instanceof DateTime || value instanceof Calendar) {
            hssfCell.setCellType(Cell.CELL_TYPE_NUMERIC);
            hssfCell.setCellStyle(dateCellStyle);
            hssfCell.setCellValue(converters.convert(value, Date.class));
        } else if (value instanceof Boolean) {
            hssfCell.setCellType(Cell.CELL_TYPE_BOOLEAN);
            hssfCell.setCellValue((Boolean) value);
        } else {
            hssfCell.setCellType(Cell.CELL_TYPE_STRING);
            hssfCell.setCellValue(converters.convert(value, String.class));
        }
    }

}
