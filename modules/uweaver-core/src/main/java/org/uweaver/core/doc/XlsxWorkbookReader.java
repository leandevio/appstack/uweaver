/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * XlsxWorkbookReader obj = new XlsxWorkbookReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class XlsxWorkbookReader implements WorkbookReader {
    private InputStream in;
    private XSSFWorkbook xssfWorkbook;

    public XlsxWorkbookReader(InputStream in) {
        this.in = in;
    }

    @Override
    public void open() {
        try {
            xssfWorkbook = new XSSFWorkbook(in);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public void close() {
        try {
            xssfWorkbook.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public int getNumberOfSheets() {
        return xssfWorkbook.getNumberOfSheets();
    }

    @Override
    public int getSheetIndex(Sheet sheet) {
        return xssfWorkbook.getSheetIndex((XSSFSheet) sheet.getRawData());
    }

    @Override
    public Sheet getSheet(int index) {
        return toSheet(xssfWorkbook.getSheetAt(index));
    }

    @Override
    public Sheet getSheet(String name) {
        return toSheet(xssfWorkbook.getSheet(name));
    }

    private Sheet toSheet(XSSFSheet xssfSheet) {
        Sheet sheet = new Sheet(xssfSheet.getSheetName(), xssfSheet);
        int first = xssfSheet.getFirstRowNum();
        int last = xssfSheet.getLastRowNum();

        for(int rownum=first; rownum<=last; rownum++) {
            XSSFRow xssfRow = xssfSheet.getRow(rownum);
            if(xssfRow==null) continue;
            SheetRow row = sheet.createSheetRow(rownum);
            copyXSSFRowToSheetRow(xssfRow, row);
        }

        return sheet;
    }

    private void copyXSSFRowToSheetRow(XSSFRow xssfRow, SheetRow row) {
        int first = xssfRow.getFirstCellNum();
        int last = xssfRow.getLastCellNum();
        for(int cellnum=first; cellnum<last; cellnum++) {
            XSSFCell xssfCell = xssfRow.getCell(cellnum);
            if(xssfCell==null) continue;
            SheetCell cell = row.createSheetCell(cellnum);
            copyXSSFCellToSheetCell(xssfCell, cell);
        }
    }

    private void copyXSSFCellToSheetCell(XSSFCell xssfCell, SheetCell cell) {
        int xssfCellType = xssfCell.getCellType();
        Object value;
        if(xssfCellType==Cell.CELL_TYPE_STRING) {
            value = xssfCell.getStringCellValue();
            if (value!=null && (((String) value).equalsIgnoreCase("NULL")||((String)value).length()==0)) {
                value = null;
            }
        } else if(xssfCellType==Cell.CELL_TYPE_NUMERIC) {
            if (DateUtil.isCellDateFormatted(xssfCell)) {
                value = xssfCell.getDateCellValue();
            } else {
                value = xssfCell.getNumericCellValue();
            }
        } else if(xssfCellType==Cell.CELL_TYPE_BOOLEAN) {
            value = xssfCell.getBooleanCellValue();
        } else if(xssfCellType==Cell.CELL_TYPE_BLANK) {
            value = null;
        } else {
            value = null;
        }
        cell.setValue(value);
    }

    protected XSSFWorkbook getXSSFWorkbook() {
        return xssfWorkbook;
    }
}
