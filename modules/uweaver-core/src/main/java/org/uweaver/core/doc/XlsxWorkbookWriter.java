/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.uweaver.core.exception.FileIOException;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.DateTimeConverter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * XlsxWorkbookWriter obj = new XlsxWorkbookWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class XlsxWorkbookWriter implements WorkbookWriter {
    private OutputStream out;
    private XSSFWorkbook xssfWorkbook;
    private Converters converters = new Converters();
    private CellStyle dateCellStyle;
    private String dateTimePattern = "yyyy/MM/dd HH:mm:ss";

    public XlsxWorkbookWriter(OutputStream out) {
        this.out = out;
        xssfWorkbook = new XSSFWorkbook();
    }

    @Override
    public boolean canConnect(Class<? extends WorkbookReader> aClass) {
        return XlsxWorkbookReader.class.equals(aClass);
    }

    @Override
    public void connect(WorkbookReader reader) {
        xssfWorkbook = ((XlsxWorkbookReader) reader).getXSSFWorkbook();
    }

    @Override
    public void open() {
        dateCellStyle = xssfWorkbook.createCellStyle();
        CreationHelper helper = xssfWorkbook.getCreationHelper();
        dateCellStyle.setDataFormat(helper.createDataFormat().getFormat(dateTimePattern));
    }

    @Override
    public void close() {
        try {
            xssfWorkbook.write(out);
            out.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    @Override
    public void write(Sheet sheet) {
        XSSFSheet xssfSheet = xssfWorkbook.getSheet(sheet.getName());

        if(xssfSheet==null) {
            xssfSheet = xssfWorkbook.createSheet(sheet.getName());
        }

        copySheetToXSSFSheet(sheet, xssfSheet);
    }

    @Override
    public String getDateTimePattern() {
        return dateTimePattern;
    }

    @Override
    public void setDateTimePattern(String dateTimePattern) {

        this.dateTimePattern = dateTimePattern;
        DateTimeConverter dtConverter = (DateTimeConverter) converters.lookup(Date.class);
        dtConverter.setDateTimePattern(dateTimePattern);
        dateCellStyle = xssfWorkbook.createCellStyle();
        CreationHelper helper = xssfWorkbook.getCreationHelper();
        dateCellStyle.setDataFormat(helper.createDataFormat().getFormat(dateTimePattern));
    }

    private void copySheetToXSSFSheet(Sheet sheet, XSSFSheet xssfSheet) {
        for(SheetRow row : sheet.getRows()) {
            XSSFRow xssfRow = xssfSheet.getRow(row.getRowNum());
            if(xssfRow==null) xssfRow = xssfSheet.createRow(row.getRowNum());
            copySheetRowToXSSFRow(row, xssfRow);
        }
    }

    private void copySheetRowToXSSFRow(SheetRow row, XSSFRow xssfRow) {
        for(SheetCell cell : row.getCells()) {
            XSSFCell xssfCell = xssfRow.getCell(cell.getCellNum());
            if(xssfCell==null) xssfCell = xssfRow.createCell(cell.getCellNum());
            copySheetCellToXSSFCell(cell, xssfCell);
        }
    }

    private void copySheetCellToXSSFCell(SheetCell cell, XSSFCell xssfCell) {
        Object value = cell.getValue();
        if(value == null) {
            xssfCell.setCellValue("");
        } else if(value instanceof String) {
            xssfCell.setCellType(Cell.CELL_TYPE_STRING);
            xssfCell.setCellValue((String) value);
        } else if(value instanceof Number) {
            xssfCell.setCellType(Cell.CELL_TYPE_NUMERIC);
            xssfCell.setCellValue(converters.convert(value, Double.class));
        } else if(value instanceof Date || value instanceof DateTime || value instanceof Calendar) {
            xssfCell.setCellType(Cell.CELL_TYPE_NUMERIC);
            xssfCell.setCellStyle(dateCellStyle);
            xssfCell.setCellValue(converters.convert(value, Date.class));
        } else if (value instanceof Boolean) {
            xssfCell.setCellType(Cell.CELL_TYPE_BOOLEAN);
            xssfCell.setCellValue((Boolean) value);
        } else {
            xssfCell.setCellType(Cell.CELL_TYPE_STRING);
            xssfCell.setCellValue(converters.convert(value, String.class));
        }
    }

}
