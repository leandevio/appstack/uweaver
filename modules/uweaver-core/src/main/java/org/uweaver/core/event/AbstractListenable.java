/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.event;

import rx.Subscription;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AbstractListenable obj = new AbstractListenable();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public abstract class AbstractListenable implements Listenable {
    public PublishSubject<Event> subject = PublishSubject.create();
    public Map<Consumer<Event>, List<Subscription>> listeners = new HashMap<>();

    @Override
    public Subscription addEventListener(Event event, Consumer<Event> fn) {
        Subscription subscription =  subject.subscribe(subject -> {
            if(event.getClass().isAssignableFrom(subject.getClass())) fn.accept(subject);
        });

        List<Subscription> subscriptions = listeners.containsKey(fn) ? listeners.get(fn) : new ArrayList<>();
        subscriptions.add(subscription);
        listeners.put(fn, subscriptions);

        return subscription;
    }

    @Override
    public Subscription addEventListener(String eventName, Consumer<Event> fn) {
        Subscription subscription = subject.subscribe(subject -> {
            if(subject.name().equalsIgnoreCase(eventName)) fn.accept(subject);
        });
        List<Subscription> subscriptions = listeners.containsKey(fn) ? listeners.get(fn) : new ArrayList<>();
        subscriptions.add(subscription);
        listeners.put(fn, subscriptions);
        return subscription;
    }

    @Override
    public void removeEventListener(Consumer<Event> fn) {
        if(!listeners.containsKey(fn)) return;

        for(Subscription subscription : listeners.get(fn)) {
            subscription.unsubscribe();
        }
        listeners.remove(fn);
    }

    public void trigger(String eventName, Object data) {
        Event event = new Event(eventName, this, data);
        trigger(event);
    }

    public void trigger(Event event) {
        subject.onNext(event);
    }
}


