/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.event;

/**
 * The interface defines ... The definitions provides ...
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Event {
    private String name;
    private Listenable target;
    private Object data;

    public Event(String name, Listenable target, Object data) {
        this.name = name;
        this.target = target;
        this.data = data;
    }

    public Event(String name, Object data) {
        this(name, null, data);
    }

    public String name() {
        return this.name;
    }

    /** The most relevant data associate with the event */
    public Object data() {
        return this.data;
    }

    /** The event target to associate with the event. The event target that defines the path through which the event will travel when posted. */
    Listenable target() {
        return this.target;
    }

    /** The object on which the Event initially occurred. The event source is the origin of the event in the event dispatch chain. */
    Object source() {
        return this.target;
    }

}
