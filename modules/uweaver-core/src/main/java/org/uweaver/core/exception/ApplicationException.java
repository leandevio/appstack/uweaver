package org.uweaver.core.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * 應用程式發生錯誤.
 * Created by jasonlin on 6/3/14.
 */
public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 9150633846462503386L;
    protected Object[] parameters = new Object[0];

    public ApplicationException(String message, Object... parameters) {
        super(message);
        this.parameters = parameters;
    }

    /**
     * When throw an exception, encourage to send the message first, then forward the cause.
     *
     * @param message
     * @param cause
     * @param parameters
     */
    public ApplicationException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause);
        this.parameters = parameters;
    }

    public ApplicationException(Throwable cause, Object... parameters) {
        super(cause);
        this.parameters = parameters;
    }

    public ApplicationException() {
        super();
    }
    public Map<String, Object> toJSON() {

        Map<String, Object> json = new HashMap<>();

        json.put("message", this.getMessage());

        return json;
    }

    public Object parameter(int index) {
        return parameters[0];
    }

    public Object[] parameters() {
        return parameters;
    }

    public String message() {
        return this.getMessage();
    }

    public Throwable cause() {
        return this.getCause();
    }
}
