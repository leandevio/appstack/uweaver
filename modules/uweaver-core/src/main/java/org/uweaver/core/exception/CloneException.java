package org.uweaver.core.exception;

/**
 * 無法複製指定的物件.
 *
 * Created by jasonlin on 2/18/14.
 */
public class CloneException extends ApplicationException {
    private static final long serialVersionUID = -7695735665276704952L;

    public CloneException(String message, Object... parameters) {
        super(message, parameters);
    }

    public CloneException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public CloneException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public CloneException() {
        super();
    }

}
