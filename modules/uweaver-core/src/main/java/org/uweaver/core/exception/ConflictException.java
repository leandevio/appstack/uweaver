package org.uweaver.core.exception;

/**
 * 資料完整性衝突．
 * Created by jasonlin on 6/3/14.
 */
public class ConflictException extends ApplicationException {

    private static final long serialVersionUID = 106465182975415457L;

    public ConflictException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ConflictException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ConflictException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ConflictException() {
        super();
    }
}
