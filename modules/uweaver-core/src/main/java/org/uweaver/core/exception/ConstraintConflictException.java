package org.uweaver.core.exception;

import java.util.*;

/**
 * 違反檢核規則.
 * 
 * Created by jasonlin on 6/11/14.
 */
public class ConstraintConflictException extends ConflictException {
    private static final long serialVersionUID = 5060718520032492418L;

    private Violation[] violations = new Violation[1];

    public ConstraintConflictException(String message, Object... parameters) {
        super(message, parameters);
        this.violations[0] = new Violation(message, parameters);
    }

    public ConstraintConflictException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
        this.violations[0] = new Violation(message, parameters);
    }

    public ConstraintConflictException(Throwable cause, Object... parameters) {
        super(cause, parameters);
        this.violations[0] = new Violation(this.getMessage(), parameters);
    }

    public ConstraintConflictException() {
        super();
    }

    public ConstraintConflictException(String[] properties, String message, Object... parameters) {
        super();
        this.violations[0] = new Violation(properties, message, parameters);

    }

    public ConstraintConflictException(Violation... violations) {
        super();
        this.violations = violations;
    }

    public ConstraintConflictException(List<Violation> violations) {
        super();
        this.violations = violations.toArray(new Violation[violations.size()]);
    }

    public Violation[] violations() {
        return violations;
    }

    @Override
    public Map<String, Object> toJSON() {

        Map<String, Object> json = super.toJSON();

        List<Map<String, Object>> violationsJSON = new ArrayList<>();
        for(Violation violation: violations) {
            violationsJSON.add(violation.toJSON());
        }
        json.put("violations", violationsJSON);

        return json;
    }

}
