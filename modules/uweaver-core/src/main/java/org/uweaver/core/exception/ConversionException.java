package org.uweaver.core.exception;

/**
 * Created by jasonlin on 5/4/15.
 */
public class ConversionException extends ApplicationException  {

    private static final long serialVersionUID = -6082843005231415189L;

    public ConversionException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ConversionException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ConversionException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ConversionException() {
        super();
    }
}
