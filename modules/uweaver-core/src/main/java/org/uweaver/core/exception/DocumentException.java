/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 * 
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.exception;

/**
 * <h1>DocumentException</h1>
 * The DocumentException class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 * 
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-06-12
 */
public class DocumentException extends ApplicationException {

    private static final long serialVersionUID = -4501528278967715234L;

    public DocumentException(String message, Object... parameters) {
        super(message, parameters);
    }

    public DocumentException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public DocumentException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public DocumentException() {
        super();
    }
}
