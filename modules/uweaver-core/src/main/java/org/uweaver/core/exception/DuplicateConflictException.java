package org.uweaver.core.exception;

/**
 * 資料重複．
 * Created by jasonlin on 2/18/14.
 */
public class DuplicateConflictException extends ConflictException {
    private static final long serialVersionUID = -7515711858709367895L;

    public DuplicateConflictException(String message, Object... parameters) {
        super(message, parameters);
    }

    public DuplicateConflictException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public DuplicateConflictException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public DuplicateConflictException() {
        super();
    }

}
