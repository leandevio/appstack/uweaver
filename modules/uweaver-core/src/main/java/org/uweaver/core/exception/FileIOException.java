package org.uweaver.core.exception;

/**
 * Created by jasonlin on 5/12/15.
 */
public class FileIOException extends ApplicationException {
    private static final long serialVersionUID = 4328841045842226644L;

    public FileIOException(String message, Object... parameters) {
        super(message, parameters);
    }

    public FileIOException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public FileIOException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public FileIOException() {
        super();
    }
}
