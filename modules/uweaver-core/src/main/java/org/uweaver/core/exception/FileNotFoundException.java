package org.uweaver.core.exception;

/**
 * Created by jasonlin on 5/18/14.
 */
public class FileNotFoundException extends FileIOException {
    private static final long serialVersionUID = 583431306502427052L;

    public FileNotFoundException(String message, Object... parameters) {
        super(message, parameters);
    }

    public FileNotFoundException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public FileNotFoundException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public FileNotFoundException() {
        super();
    }
}

