package org.uweaver.core.exception;

/**
 * 權限不足．
 *
 * Created by jasonlin on 5/28/14.
 */
public class ForbiddenException extends ApplicationException {

    private static final long serialVersionUID = 5911627041012987085L;

    public ForbiddenException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ForbiddenException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ForbiddenException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ForbiddenException() {
        super();
    }

}
