/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 * 
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.exception;

/**
 * <h1>IOException</h1>
 * The IOException class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 * 
 * @author   Jason Lin
 * @version  1.0
 * @since    2015-06-16
 */
public class IOException extends ApplicationException {
    private static final long serialVersionUID = -7954359919734118562L;

    public IOException(String message, Object... parameters) {
        super(message, parameters);
    }

    public IOException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public IOException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public IOException() {
        super();
    }

}
