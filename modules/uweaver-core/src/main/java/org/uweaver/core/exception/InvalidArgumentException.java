/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.exception;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * InvalidArgumentException obj = new InvalidArgumentException();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class InvalidArgumentException extends ApplicationException {

    private static final long serialVersionUID = -834004133256316225L;

    public InvalidArgumentException(String message, Object... parameters) {
        super(message, parameters);
    }

    public InvalidArgumentException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public InvalidArgumentException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public InvalidArgumentException() {
        super();
    }

}
