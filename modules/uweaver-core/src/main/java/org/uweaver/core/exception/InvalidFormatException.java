package org.uweaver.core.exception;


/**
 * Created by jasonlin on 6/8/15.
 */
public class InvalidFormatException extends ApplicationException {

    private static final long serialVersionUID = -1634417413758328491L;

    public InvalidFormatException(String message, Object... parameters) {
        super(message, parameters);
    }

    public InvalidFormatException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public InvalidFormatException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public InvalidFormatException() {
        super();
    }
}