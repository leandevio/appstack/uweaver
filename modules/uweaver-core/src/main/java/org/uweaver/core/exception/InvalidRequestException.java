package org.uweaver.core.exception;

/**
 * 請求無效．嘗試呼叫不正確的API．
 *
 * Created by jasonlin on 2/8/14.
 */
public class InvalidRequestException extends ApplicationException {
    private static final long serialVersionUID = -7099580034819693707L;

    public InvalidRequestException(String message, Object... parameters) {
        super(message, parameters);
    }

    public InvalidRequestException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public InvalidRequestException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public InvalidRequestException() {
        super();
    }

}