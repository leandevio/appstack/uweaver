package org.uweaver.core.exception;


/**
 * Created by jasonlin on 6/3/14.
 */
public class LockException extends ApplicationException {
    private static final long serialVersionUID = -4318365968251900176L;

    public LockException(String message, Object... parameters) {
        super(message, parameters);
    }

    public LockException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public LockException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public LockException() {
        super();
    }
}
