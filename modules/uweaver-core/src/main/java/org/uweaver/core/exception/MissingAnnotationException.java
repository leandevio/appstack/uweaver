/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.exception;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * MissingAnnotationException obj = new MissingAnnotationException();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class MissingAnnotationException extends ApplicationException {
    private static final long serialVersionUID = 3597253910277346387L;

    public MissingAnnotationException(String message, Object... parameters) {
        super(message, parameters);
    }

    public MissingAnnotationException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public MissingAnnotationException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public MissingAnnotationException() {
        super();
    }
}
