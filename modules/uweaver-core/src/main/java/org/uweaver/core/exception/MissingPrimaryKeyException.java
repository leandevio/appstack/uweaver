package org.uweaver.core.exception;

/**
 * 沒有主索引鍵．
 *  
 * Created by jasonlin on 2/8/14.
 */
public class MissingPrimaryKeyException extends ApplicationException {
    private static final long serialVersionUID = -2529030362965646072L;

    public MissingPrimaryKeyException(String message, Object... parameters) {
        super(message, parameters);
    }

    public MissingPrimaryKeyException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public MissingPrimaryKeyException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public MissingPrimaryKeyException() {
        super();
    }
}
