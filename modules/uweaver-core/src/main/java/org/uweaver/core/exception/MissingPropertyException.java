package org.uweaver.core.exception;


/**
 * 代表因為沒有特定屬性造成的 Exception.
 *
 * Created by jasonlin on 2/19/14.
 */
public class MissingPropertyException extends ApplicationException {
    private static final long serialVersionUID = -4204270575469701425L;

    public MissingPropertyException(String message, Object... parameters) {
        super(message, parameters);
    }

    public MissingPropertyException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public MissingPropertyException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public MissingPropertyException() {
        super();
    }
}
