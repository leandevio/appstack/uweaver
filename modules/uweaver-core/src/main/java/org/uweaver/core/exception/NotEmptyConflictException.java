package org.uweaver.core.exception;

/**
 * 不得為空字串或NULL.
 *
 * Created by jasonlin on 2/18/14.
 */
public class NotEmptyConflictException extends ConflictException {
    private static final long serialVersionUID = -6327289328225212497L;

    public NotEmptyConflictException(String message, Object... parameters) {
        super(message, parameters);
    }

    public NotEmptyConflictException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public NotEmptyConflictException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public NotEmptyConflictException() {
        super();
    }
}
