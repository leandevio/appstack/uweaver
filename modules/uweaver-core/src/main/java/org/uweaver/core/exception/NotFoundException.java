package org.uweaver.core.exception;

/**
 * 找不到指定的資料.
 *
 * Created by jasonlin on 2/18/14.
 */
public class NotFoundException extends ApplicationException  {
    private static final long serialVersionUID = -4583865808494631842L;

    public NotFoundException(String message, Object... parameters) {
        super(message, parameters);
    }

    public NotFoundException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public NotFoundException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public NotFoundException() {
        super();
    }
}
