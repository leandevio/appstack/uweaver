package org.uweaver.core.exception;


/**
 * 資料已經過期或失效.
 *
 * Created by jasonlin on 2/18/14.
 */
public class ObsoleteException extends ApplicationException {
    private static final long serialVersionUID = 973744126444116200L;

    public ObsoleteException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ObsoleteException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ObsoleteException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ObsoleteException() {
        super();
    }
}
