package org.uweaver.core.exception;

/**
 * 作業失敗．
 * 
 * Created by jasonlin on 5/28/14.
 */
public class OperationFailureException extends ApplicationException {
    private static final long serialVersionUID = 8460195562075456014L;

    public OperationFailureException(String message, Object... parameters) {
        super(message, parameters);
    }

    public OperationFailureException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public OperationFailureException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public OperationFailureException() {
        super();
    }
}
