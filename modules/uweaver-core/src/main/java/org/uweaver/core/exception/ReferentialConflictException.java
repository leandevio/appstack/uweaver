package org.uweaver.core.exception;


/**
 * 參考完整性衝突．
 *
 * Created by jasonlin on 2/18/14.
 */
public class ReferentialConflictException extends ConflictException {
    private static final long serialVersionUID = -1291365226479935696L;

    public ReferentialConflictException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ReferentialConflictException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ReferentialConflictException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ReferentialConflictException() {
        super();
    }
}
