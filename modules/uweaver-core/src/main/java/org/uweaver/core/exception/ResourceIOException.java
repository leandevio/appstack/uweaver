package org.uweaver.core.exception;

/**
 * Created by jasonlin on 5/18/14.
 */
public class ResourceIOException extends ApplicationException {
    private static final long serialVersionUID = -2370594195655069549L;

    public ResourceIOException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ResourceIOException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ResourceIOException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ResourceIOException() {
        super();
    }
}
