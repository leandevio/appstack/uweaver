package org.uweaver.core.exception;

/**
 * 找不到指定的資源.
 * 
 * 資源通常是外部的資料來源. 例如檔案或連線等.
 * 
 * Created by jasonlin on 6/5/14.
 */
public class ResourceNotFoundException extends ResourceIOException {
    private static final long serialVersionUID = 1114234073281940090L;

    public ResourceNotFoundException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ResourceNotFoundException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public ResourceNotFoundException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public ResourceNotFoundException() {
        super();
    }
}
