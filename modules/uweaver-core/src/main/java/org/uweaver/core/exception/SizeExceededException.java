package org.uweaver.core.exception;

/**
 * 長度或大小超過系統限制.
 * 
 * Created by jasonlin on 5/29/14.
 */
public class SizeExceededException extends ApplicationException {
    private static final long serialVersionUID = 1132784386476051815L;

    public SizeExceededException(String message, Object... parameters) {
        super(message, parameters);
    }

    public SizeExceededException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public SizeExceededException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }
    public SizeExceededException() {
        super();
    }
}
