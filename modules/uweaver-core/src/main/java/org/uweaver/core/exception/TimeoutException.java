package org.uweaver.core.exception;


/**
 * 作業逾時.
 * 
 * Created by jasonlin on 5/28/14.
 */
public class TimeoutException extends ApplicationException {
    private static final long serialVersionUID = -9006205983209243685L;

    public TimeoutException(String message, Object... parameters) {
        super(message, parameters);
    }

    public TimeoutException(String message, Throwable cause, Object... parameters)
    {
        super(message, cause, parameters);
    }

    public TimeoutException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public TimeoutException() {
        super();
    }
}
