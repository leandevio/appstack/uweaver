package org.uweaver.core.exception;

/**
 * 無法辨識用戶身份.
 *
 * Created by jasonlin on 5/28/14.
 */
public class UnauthorizedException extends ApplicationException {
    private static final long serialVersionUID = 7758555948206136289L;

    public UnauthorizedException(String message, Object... parameters) {
        super(message, parameters);
    }

    public UnauthorizedException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
    }

    public UnauthorizedException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public UnauthorizedException() {
        super();
    }
}
