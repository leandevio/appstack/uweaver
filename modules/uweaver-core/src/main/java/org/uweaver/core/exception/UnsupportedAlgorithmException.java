package org.uweaver.core.exception;

/**
 * 不支援指定的演算法．
 * 
 * Created by jasonlin on 2/21/14.
 */
public class UnsupportedAlgorithmException extends ApplicationException  {
    private static final long serialVersionUID = -3493289640551664778L;

    public UnsupportedAlgorithmException(String message, Object... parameters) {
        super(message, parameters);
    }

    public UnsupportedAlgorithmException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
    }

    public UnsupportedAlgorithmException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public UnsupportedAlgorithmException() {
        super();
    }
}
