package org.uweaver.core.exception;

/**
 * Created by jasonlin on 5/5/15.
 */
public class UnsupportedTypeException extends ApplicationException {
    private static final long serialVersionUID = -209361907500574673L;

    public UnsupportedTypeException(String message, Object... parameters) {
        super(message, parameters);
    }

    public UnsupportedTypeException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
    }

    public UnsupportedTypeException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public UnsupportedTypeException() {
        super();
    }
}
