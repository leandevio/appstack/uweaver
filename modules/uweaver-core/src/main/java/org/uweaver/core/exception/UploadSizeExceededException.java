package org.uweaver.core.exception;

/**
 * 上傳資料的大小超過系統限制.
 * 
 * Created by jasonlin on 6/10/14.
 */
public class UploadSizeExceededException extends SizeExceededException {
    private static final long serialVersionUID = -8726972137534394686L;

    public UploadSizeExceededException(String message, Object... parameters) {
        super(message, parameters);
    }

    public UploadSizeExceededException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
    }

    public UploadSizeExceededException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public UploadSizeExceededException() {
        super();
    }

}
