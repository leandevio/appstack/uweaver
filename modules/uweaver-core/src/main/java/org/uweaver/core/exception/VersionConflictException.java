package org.uweaver.core.exception;

/**
 * 版本衝突．
 *
 * Created by jasonlin on 2/18/14.
 */
public class VersionConflictException extends ConflictException {
    private static final long serialVersionUID = 2281270866177713524L;

    public VersionConflictException(String message, Object... parameters) {
        super(message, parameters);
    }

    public VersionConflictException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
    }

    public VersionConflictException(Throwable cause, Object... parameters) {
        super(cause, parameters);
    }

    public VersionConflictException() {
        super();
    }
}
