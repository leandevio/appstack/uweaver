package org.uweaver.core.exception;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jasonlin on 1/27/15.
 */
public class Violation implements Serializable {

    private static final long serialVersionUID = -4500233372892879218L;

    private String[] properties = new String[0];
    private String message = "";
    private Object[] parameters = new Object[0];

    public Violation(String[] properties, String message, Object... parameters) {
        this.message = message;
        this.parameters = parameters;
        this.properties = properties;
    }

    public Violation(String message, Object... parameters) {
        this.message = message;
        this.parameters = parameters;
    }

    public Violation() {}

    public String message() {
        return this.message;
    }

    public Object parameter(int index) {
        return parameters[0];
    }

    public Object[] parameters() {
        return parameters;
    }

    public String property(int index) {
        return properties[0];
    }

    public String[] properties() {
        return properties;
    }

    public Map<String, Object> toJSON() {

        Map<String, Object> json = new HashMap<>();

        json.put("message", this.message);
        json.put("parameters", this.parameters);
        json.put("properties", this.properties);

        return json;
    }
}
