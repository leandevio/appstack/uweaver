/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * business application.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;
import org.uweaver.core.exception.FileIOException;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.DateTimeConverter;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.util.*;

/**
 * <h1>CsvDataFileReader</h1>
 * The CsvDataFileReader class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-04-27
 */
public class CsvDataFileReader implements DataFileReader {

    private Logger LOGGER = LoggerFactory.getLogger(CsvDataFileReader.class);
    private Environment environment = Environment.getDefaultInstance();

    private File file = null;
    private List<String> header = null;
    private List<Class> type = null;
    private char quote = '"';
    private char delimiter = ',';
    private String eol = "\n";
    private String encoding = environment.fileencoding();
    private boolean hasHeader = false;
    private int leadingBlank = 0;
    private int trailingBlank = 0;
    private Integer length = null;
    private int current = -1;
    private String dateTimePattern = environment.datetimePattern();

    private ICsvListReader listReader = null;
    private Converters converters = new Converters();

    public CsvDataFileReader(File file, Map<String, ?> cfg) {
        this.file = file;
        config(cfg);
        initialize();
    }

    public CsvDataFileReader(File file) {
        this.file = file;
        initialize();
    }

    private void config(Map<String, ?> cfg) {
        if (cfg.containsKey("hasHeader")) hasHeader = (Boolean) cfg.get("hasHeader");
        if (cfg.containsKey("header")) {
            header = (List<String>) cfg.get("header");
            hasHeader = true;
        }
        if (cfg.containsKey("type")) type = (List<Class>) cfg.get("type");
        if (cfg.containsKey("encoding")) encoding = (String) cfg.get("encoding");
        if (cfg.containsKey("leadingBlank")) leadingBlank = converters.convert(cfg.get("leadingBlank"), Integer.class);
        if (cfg.containsKey("trailingBlank")) trailingBlank = converters.convert(cfg.get("trailingBlank"), Integer.class);
        if (cfg.containsKey("dateTimePattern")) setDateTimePattern((String) cfg.get("dateTimePattern"));

        if (cfg.containsKey("quote")) quote = (Character) cfg.get("quote");
        if (cfg.containsKey("delimiter")) delimiter = converters.convert(cfg.get("delimiter"), Character.class);
        if (cfg.containsKey("eol")) eol = (String) cfg.get("eol");
    }

    private void initialize() {
        CsvPreference preference = new CsvPreference.Builder(quote, delimiter, eol).build();
        try {
            listReader = new CsvListReader(new InputStreamReader(new FileInputStream(file), encoding), preference);
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }

        if (hasHeader && header==null) {
            readHeader();
        }
        if (leadingBlank > 0) {
            skip(leadingBlank);
        }
        current = 0;
    }

    @Override
    public void setAnchor(Object anchor) {}

    @Override
    public void close() {
        try {
            if (listReader != null) listReader.close();
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }
    }

    @Override
    public void rewind() {
        close();
        initialize();
    }

    @Override
    public List<String> getHeader() {
        return this.header;
    }

    @Override
    public boolean hasHeader() {
        return this.hasHeader;
    }

    @Override
    public String getDateTimePattern() {
        return dateTimePattern;
    }

    @Override
    public void setDateTimePattern(String dateTimePattern) {
        this.dateTimePattern = dateTimePattern;
        DateTimeConverter dtConverter = (DateTimeConverter) converters.lookup(Date.class);
        dtConverter.setDateTimePattern(dateTimePattern);
    }

    @Override
    public int size() {
        if (length == null) {
            try {
                LineNumberReader lnr = new LineNumberReader(new FileReader(file));
                lnr.skip(Long.MAX_VALUE);
                length = Math.max(lnr.getLineNumber() - leadingBlank - trailingBlank - (hasHeader ? 1 : 0), 0);
            } catch(IOException ex) {
                throw new FileIOException(ex);
            }
        }
        return length;
    }

    @Override
    public List<Object> nextRow(int n) {
        if (trailingBlank > 0 && current + n + trailingBlank >= size()) {
            current = size();
            return null;
        }

        List<Object> row = null;

        try {
            for (int i = 0; i < n; i++) {
                row = (List<Object>)(List<?>) listReader.read();
                if (row == null) {
                    this.length = current;
                    break;
                } else {
                    current++;
                }
            }
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }


        if (this.type != null && row != null) {
            for (int i = 0; i < row.size(); i++) {
                Class clazz = (i < this.type.size() ? this.type.get(i) : String.class);
                row.set(i, converters.convert(row.get(i), clazz));
            }
        }

        return row;
    }

    @Override
    public List<Object> nextRow() {
        return nextRow(1);
    }

    @Override
    public List<Object> getRow(int n) {
        rewind();
        return nextRow(n);
    }

    @Override
    public Map<String, Object> nextItem(int n) {
        return toItem(nextRow(n));
    }

    @Override
    public Map<String, Object> nextItem() {
        return nextItem(1);
    }

    @Override
    public Map<String, Object> getItem(int n) {
        rewind();
        return nextItem(n);
    }

    @Override
    public int getRowNumber() {
        return current;
    }

    @Override
    public boolean hasColumn(String name) {
        if (this.header == null) return false;
        for (String columnName : this.header) {
            if (name.equals(columnName)) return true;
        }
        return false;
    }

    @Override
    public List<String> getAnchors() {
        return new ArrayList<>();
    }

    private void readHeader() {

        try {
            header = this.listReader.read();
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }

    }

    private void skip(int n) {
        try {
            for(int i = 0; i < n; i++) {
                if(this.listReader.read()==null) break;
            }
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }
    }

    protected Map<String, Object> toItem(List<Object> row) {
        if (row == null) return null;

        Map<String, Object> item = new HashMap<>();

        for (int i = 0; i < row.size(); i++) {
            String key;
            if(hasHeader && i < header.size()) {
                key = header.get(i);
            } else {
                key = Integer.toString(i);
            }
            item.put(key, row.get(i));
        }

        return item;
    }


}
