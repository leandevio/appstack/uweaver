/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * business application.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;
import org.uweaver.core.exception.ConversionException;
import org.uweaver.core.exception.FileIOException;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.DateTimeConverter;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.util.*;

/**
 * <h1>CsvDataFileWriter</h1>
 * The CsvDataFileWriter class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-04-27
 */
public class CsvDataFileWriter implements DataFileWriter {
    private Logger LOGGER = LoggerFactory.getLogger(CsvDataFileWriter.class);
    private Environment environment = Environment.getDefaultInstance();
    private File file = null;
    private List<String> header = null;
    private List<Class> type = null;
    private char quote = '"';
    private char delimiter = ',';
    private String eol = "\n";
    private String encoding = environment.fileencoding();
    private String dateTimePattern = environment.datetimePattern();

    private ICsvListWriter listWriter = null;
    private Converters converters = new Converters();


    public CsvDataFileWriter(File file, Map<String, Object> cfg) throws IOException {
        this.file = file;
        config(cfg);
        initialize();
    }

    public CsvDataFileWriter(File file) throws IOException {
        this.file = file;
        initialize();
    }

    @SuppressWarnings("unchecked")
    private void config(Map<String, Object> cfg) {
        if(cfg.containsKey("header")) header = (List<String>) cfg.get("header");
        if (cfg.containsKey("type")) type = (List<Class>) cfg.get("type");
        if(cfg.containsKey("encoding")) encoding = (String) cfg.get("encoding");
        if(cfg.containsKey("dateTimePattern")) setDateTimePattern((String) cfg.get("dateTimePattern"));

        if(cfg.containsKey("quote")) quote = (char) cfg.get("quote");
        if(cfg.containsKey("delimiter")) delimiter = converters.convert(cfg.get("delimiter"), Character.class);
        if(cfg.containsKey("eol")) eol = (String) cfg.get("eol");
    }

    private void initialize() {
        CsvPreference preference = new CsvPreference.Builder(quote, delimiter, eol).build();
        try {
            listWriter = new CsvListWriter(new OutputStreamWriter(new FileOutputStream(file), encoding),  preference);
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }
    }

    @Override
    public void setAnchor(Object anchor) {

    }

    @Override
    public void writeHeader()  {
        try {
            if(header!=null) {
                listWriter.write(header);
            }
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }

    }

    public void close() {
        try {
            if(listWriter!=null) listWriter.close();
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }
    }

    public void writeRow(List<?> values) {
        List<Object> row = new ArrayList<>();
        for(Object any : values) {
            Object value;
            if(any==null) {
                value = null;
            } else if(any.getClass().isPrimitive() || any instanceof Number || any instanceof Boolean) {
                value = any;
            } else if(any instanceof String) {
                value = any;
            } else if(any instanceof byte[]) {
                byte [] data = (byte[]) any;
                value = new String(data);
            } else if(any instanceof Reader) {
                Reader reader = (Reader) any;
                StringBuilder sb = new StringBuilder();
                String oneline;
                try {
                    BufferedReader br = new BufferedReader(reader);
                    while ((oneline=br.readLine())!=null) {
                        sb.append(oneline);
                        sb.append(System.getProperty("line.separator"));
                    }
                } catch (Exception e) {
                    throw new ConversionException(e);
                }
                value = sb.toString();
            } else {
                value = converters.convert(any, String.class);
            }
            row.add(value);
        }

        if (this.type != null) {
            for (int i = 0; i < row.size(); i++) {
                Class clazz = (i < this.type.size() ? this.type.get(i) : null);
                if(clazz != null) row.set(i, converters.convert(row.get(i), clazz));
            }
        }

        try {
            listWriter.write(row);
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }

    }

    @Override
    public void writeItem(Map<String, ?> item) {
        List<Object> values = new ArrayList<>();

        for(String key : getHeader()) {
            values.add(item.get(key));
        }
        writeRow(values);
    }

    @Override
    public List<String> getHeader() {
        return this.header;
    }

    @Override
    public void setHeader(List<String> header) {
        this.header = header;
    }

    @Override
    public String getDateTimePattern() {
        return dateTimePattern;
    }

    @Override
    public void setDateTimePattern(String dateTimePattern) {
        this.dateTimePattern = dateTimePattern;
        DateTimeConverter dtConverter = (DateTimeConverter) converters.lookup(Date.class);
        dtConverter.setDateTimePattern(dateTimePattern);
    }

    @Override
    public List<String> getAnchors() {
        return new ArrayList<>();
    }

}
