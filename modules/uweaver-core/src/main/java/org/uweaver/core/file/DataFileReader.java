/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * business application.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import java.util.List;
import java.util.Map;

/**
 * <h1>DataFileReader</h1>
 * The DataFileReader interface defines ... The definitions provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author Jason Lin
 * @version 1.0
 * @since 2014-04-27
 */
public interface DataFileReader {
    void close();

    void rewind();

    int size();

    List<String> getHeader();

    boolean hasHeader();

    String getDateTimePattern();

    void setDateTimePattern(String dateTimePattern);

    List<Object> nextRow(int n);

    List<Object> nextRow();

    List<Object> getRow(int n);


    Map<String, Object> nextItem(int n);

    Map<String, Object> nextItem();

    Map<String, Object> getItem(int n);

    boolean hasColumn(String name);

    int getRowNumber();

    void setAnchor(Object anchor);

    List<String> getAnchors();

}
