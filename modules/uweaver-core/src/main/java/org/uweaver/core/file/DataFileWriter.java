/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * business application.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import java.util.List;
import java.util.Map;

/**
 * <h1>DataFileWriter</h1>
 * The DataFileWriter interface defines ... The definitions provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author Jason Lin
 * @version 1.0
 * @since 2014-04-27
 */
public interface DataFileWriter {
    void close();

    void writeHeader();

    void writeRow(List<?> row);

    void writeItem(Map<String, ?> item);

    List<String> getHeader();

    void setHeader(List<String> header);

    String getDateTimePattern();

    void setDateTimePattern(String dateTimePattern);

    void setAnchor(Object anchor);

    List<String> getAnchors();
}
