/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.doc.*;
import org.uweaver.core.search.*;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * FileIndexer obj = new FileIndexer();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class FileIndexer {
    private static Logger LOGGER = LoggerFactory.getLogger(FileIndexer.class);

    private static Date smallestTime = new DateTime(1900, 1, 1, 0, 0).toDate();
    private Path dataRepository;
    private IndexRepository indexRepository;
    private IndexWriter indexWriter;

    public FileIndexer(Path dataRepository) {
        initialize(dataRepository, IndexRepository.getDefaultInstance());
    }

    public FileIndexer(Path dataRepository, IndexRepository indexRepository) {
        initialize(dataRepository, indexRepository);
    }

    public Path getDataRepository() {
        return this.dataRepository;
    }

    public IndexRepository getIndexRepository() {
        return this.indexRepository;
    }

    private void initialize(Path dataRepository, IndexRepository indexRepository) {
        this.dataRepository = dataRepository;
        this.indexRepository = indexRepository;
        this.indexWriter = new IndexWriter(indexRepository);
    }

    public void index(Path path) {
        String name = dataRepository.relativize(path).toString();
        if(indexRepository.getCollection(name)!=null) return;
        IndexCollection indexCollection = new IndexCollection(name);
        List<Field> fields = new ArrayList<>();
        fields.add(new Field("path"));
        fields.add(new Field("filename"));
        fields.add(new Field("content"));
        indexCollection.setFields(fields);
        List<Field> key = new ArrayList<>();
        key.add(new Field("path"));
        indexCollection.setKey(key);

        indexRepository.saveCollection(indexCollection);
    }


    public void put(String collection, Map<String, Object> record) {
        IndexCollection indexCollection = indexRepository.getCollection(collection);

        Map<String, Object> predicate = new HashMap<>();
        for(org.uweaver.core.search.Field field : indexCollection.getKey()) {
            predicate.put(field.getName(), record.get(field.getName()));
        }
        remove(collection, predicate);
        indexWriter.add(indexCollection, record);
    }

    public void remove(String collection, Map<String, Object> predicate) {
        IndexQuery query = new IndexQuery().from(collection);
        indexWriter.remove(query.where(predicate));
    }

    public void build() {
        List<IndexCollection> collections = indexRepository.getCollections();
        for(int i=0; i<collections.size(); i++) {
            build(collections.get(i));
        }
    }

    public void build(Path collection) {
        String name = dataRepository.relativize(collection).toString();

        build(name);
    }

    public void build(String collection) {
        IndexCollection indexCollection = indexRepository.getCollection(collection);
        build(indexCollection);
    }

    private void build(IndexCollection indexCollection) {
        Path path = dataRepository.resolve(indexCollection.getName());

        if(LOGGER.isDebugEnabled()) LOGGER.debug("Building the index for " + indexCollection.getName());

        Date indexUpdateTime = indexCollection.getUpdateTime()==null ? smallestTime : indexCollection.getUpdateTime();
        Date now = new Date();

        List<Path> files = searchDirectoryByModifiedTime(path, indexUpdateTime);

        for(Path file : files) {
            Map<String, Object> record = new HashMap<>();
            record.put("path", dataRepository.relativize(file).toString());
            record.put("filename", file.getFileName());
            record.put("content", getContent(file));
            put(indexCollection.getName(), record);
        }

        indexCollection.setUpdateTime(now);
        indexRepository.saveCollection(indexCollection);


        if(LOGGER.isDebugEnabled()) LOGGER.debug("The index for " + indexCollection.getName() + " is up-to-date.");
    }

    private List<Path> searchDirectoryByModifiedTime(Path path, Date modifiedTime) {
        Visitor visitor = new Visitor(modifiedTime);
        try {
            Files.walkFileTree(path, visitor);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return visitor.getFiles();
    }

    private String getContent(Path path) {
        String filename = path.toString();
        if(filename.endsWith("pptx")||filename.endsWith("ppt")) {
            return getSlideShowContent(path);
        } else if(filename.endsWith("xlsx")||filename.endsWith("xls")) {
            return getWorkbookContent(path);
        } else {
            return getTextContent(path);
        }
    }

    private String getTextContent(Path path) {
        StringBuilder content = new StringBuilder();
        TextFileReader reader;
        InputStream in = null;
        try {
            in = new FileInputStream(path.toFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        reader = new TextFileReader(in);
        reader.open();
        for(String line=reader.readLine(); line!=null; line=reader.readLine()) {
            content.append(line).append('\n');
        }
        reader.close();
        return content.toString();
    }

    private String getSlideShowContent(Path path) {
        StringBuilder content = new StringBuilder();
        SlideShowReader reader;
        InputStream in = null;
        try {
            in = new FileInputStream(path.toFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        reader = new PptxReader(in);
        reader.open();
        for(Slide slide : reader.readSlides()) {
            content.append(slide.getText());
        }
        reader.close();
        return content.toString();
    }

    private String getWorkbookContent(Path path) {
        StringBuilder content = new StringBuilder();
        WorkbookReader reader;
        InputStream in = null;
        try {
            in = new FileInputStream(path.toFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if(path.toString().endsWith("xls")) {
            reader = new XlsWorkbookReader(in);
        } else {
            reader = new XlsxWorkbookReader(in);
        }
        reader.open();
        for(int i=0; i<reader.getNumberOfSheets(); i++) {
            Sheet sheet = reader.getSheet(i);
            content.append(sheet.getText()).append('\n');
        }
        reader.close();
        return content.toString();
    }


    private class Visitor extends SimpleFileVisitor<Path> {
        private List<Path> files = new ArrayList<>();
        private Date modifiedTime;

        public Visitor(Date modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public List<Path> getFiles() {
            return this.files;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            if(file.toFile().lastModified() > modifiedTime.getTime()) {
                files.add(file);
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
