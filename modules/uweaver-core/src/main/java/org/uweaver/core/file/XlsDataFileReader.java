/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * business application.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import org.apache.poi.POIXMLException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.exception.FileIOException;
import org.uweaver.core.exception.InvalidFormatException;
import org.uweaver.core.exception.NotFoundException;
import org.uweaver.core.util.*;

import java.io.*;
import java.util.*;

/**
 * <h1>XlsDataFileReader</h1>
 * The XlsDataFileReader class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author Jason Lin
 * @version 1.0
 * @since 2014-04-27
 */
public class XlsDataFileReader implements DataFileReader {
    private Logger LOGGER = LoggerFactory.getLogger(XlsDataFileReader.class);

    private InputStream input = null;
    private List<String> header = null;
    private boolean hasHeader = false;
    private List<Class> type = null;
    private String dateTimePattern = Environment.getDefaultInstance().datetimePattern();
    private int leadingBlank = 0;
    private int trailingBlank = 0;
    private Integer length = null;
    private int current = -1;
    private Converters converters = new Converters();
    private Object anchor = null;

    private HSSFWorkbook workbook;
    private HSSFSheet sheet;

    public XlsDataFileReader(InputStream input, Map<String, ?> cfg) {
        this.input = input;
        init(cfg);
    }

    public XlsDataFileReader(InputStream input) {
        Map<String, ?> cfg = new HashMap<>();
        this.input = input;
        init(cfg);
    }

    public XlsDataFileReader(byte[] bytes, Map<String, ?> cfg) {
        this.input = new ByteArrayInputStream(bytes);
        init(cfg);
    }

    public XlsDataFileReader(byte[] bytes) {
        Map<String, ?> cfg = new HashMap<>();
        this.input = new ByteArrayInputStream(bytes);
        init(cfg);
    }


    public XlsDataFileReader(File file, Map<String, ?> cfg) {
        try {
            this.input = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new FileIOException(e);
        }
        init(cfg);
    }

    public XlsDataFileReader(File file) {
        Map<String, ?> cfg = new HashMap<>();
        try {
            this.input = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new FileIOException(e);
        }
        init(cfg);
    }



    private void init(Map<String, ?> cfg) {
        if (cfg.containsKey("hasHeader")) hasHeader = (Boolean) cfg.get("hasHeader");
        if (cfg.containsKey("header")) header = (List<String>) cfg.get("header");
        if (cfg.containsKey("type")) type = (List<Class>) cfg.get("type");
        if (cfg.containsKey("leadingBlank")) leadingBlank = converters.convert(cfg.get("leadingBlank"), Integer.class);
        if (cfg.containsKey("trailingBlank")) trailingBlank = converters.convert(cfg.get("trailingBlank"), Integer.class);
        if (cfg.containsKey("dateTimePattern")) setDateTimePattern((String) cfg.get("dateTimePattern"));

        if (cfg.containsKey("anchor")) anchor = cfg.get("anchor");

        try {
            workbook = new HSSFWorkbook(input);
        } catch (POIXMLException e) {
            if(e.getCause() instanceof org.apache.poi.openxml4j.exceptions.InvalidFormatException) {
                throw new InvalidFormatException(e);
            }
            throw new ApplicationException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        setAnchor(anchor);
    }

    public void setAnchor(Object anchor) {
        if (anchor instanceof String) {
            sheet = workbook.getSheet((String) anchor);
        } else {
            sheet = workbook.getSheetAt(converters.convert(anchor, Integer.class, 0));
        }

        if(sheet==null) {
            throw new NotFoundException(anchor.toString());
        }

        if (hasHeader) {
            readHeader();
        }

        current = 0;
        length = null;

        this.anchor = anchor;

    }


    @Override
    public void close() {
        try {
            input.close();
        } catch(IOException ex) {
            throw new org.uweaver.core.exception.IOException(ex);
        }
    }

    @Override
    public void rewind() {
        current = 0;
    }

    @Override
    public List<String> getHeader() {
        return this.header;
    }

    @Override
    public boolean hasHeader() {
        return this.hasHeader;
    }

    @Override
    public int size() {
        if (length == null) {
            length = Math.max(sheet.getLastRowNum()+1 - leadingBlank - trailingBlank - (hasHeader ? 1 : 0), 0);
        }
        return length;
    }

    @Override
    public List<Object> getRow(int n) {
        if (n > size()) {
            current = size();
            return null;
        }

        List<Object> row = new ArrayList<>();
        int columns;

        Row record = sheet.getRow(n);

        if (record == null) {
            return null;
        }

        if (header != null && header.size() > 0) {
            columns = header.size();
        } else {
            columns = record.getLastCellNum();
        }

        for (int col = 0; col < columns; col++) {
            Cell cell = record.getCell(col);
            if (cell == null) {
                row.add(null);
                continue;
            }
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    String value = cell.getStringCellValue();
                    if (value == null || value.equalsIgnoreCase("NULL") || value.length() == 0) {
                        value = null;
                    }
                    row.add(value);
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        row.add(cell.getDateCellValue());
                    } else {
                        row.add(cell.getNumericCellValue());
                    }
                    break;
                case Cell.CELL_TYPE_BOOLEAN:
                    row.add(cell.getBooleanCellValue());
                    break;
                case Cell.CELL_TYPE_BLANK:
                    row.add(null);
                    break;
                default:
                    break;
            }
        }

        if (this.type != null) {
            for (int i = 0; i < row.size(); i++) {
                Class clazz = (i < this.type.size() ? this.type.get(i) : String.class);
                row.set(i, converters.convert(row.get(i), clazz));
            }
        }

        return row;
    }

    @Override
    public Map<String, Object> getItem(int n) {
        return toItem(getRow(n));
    }

    @Override
    public List<Object> nextRow() {
        return nextRow(1);
    }

    @Override
    public List<Object> nextRow(int n) {
        if ((trailingBlank > 0) && ((current + trailingBlank) >= size())) return null;

        List<Object> row = getRow(current + n);

        if (row != null) {
            current++;
        }
        return row;
    }

    @Override
    public Map<String, Object> nextItem() {
        return nextItem(1);
    }

    @Override
    public Map<String, Object> nextItem(int n) {
        if (trailingBlank > 0 && current + trailingBlank >= size()) return null;

        Map<String, Object> item = getItem(current + n);

        if (item != null) {
            current++;
        }

        return item;
    }

    @Override
    public boolean hasColumn(String name) {
        if (this.header == null) return false;
        for (String columnName : this.header) {
            if (name.equals(columnName)) return true;
        }
        return false;
    }

    @Override
    public int getRowNumber() {
        return current;
    }


    @Override
    public String getDateTimePattern() {
        return dateTimePattern;
    }

    @Override
    public void setDateTimePattern(String dateTimePattern) {
        this.dateTimePattern = dateTimePattern;
        DateTimeConverter dtConverter = (DateTimeConverter) converters.lookup(Date.class);
        dtConverter.setDateTimePattern(dateTimePattern);
    }

    public List<String> getAnchors() {
        List<String> anchors = new ArrayList<>();

        for(int i=0; i<workbook.getNumberOfSheets(); i++) {
            anchors.add(workbook.getSheetName(i));
        }

        return anchors;
    }


    private void readHeader() {
        List<String> row = new ArrayList<>();
        int columns;

        Row record = sheet.getRow(0);

        if (record == null) {
            return;
        }

        columns = record.getLastCellNum();

        for (int col = 0; col < columns; col++) {
            Cell cell = record.getCell(col);
            if (cell != null && cell.getCellType()==Cell.CELL_TYPE_STRING) {
                String value = cell.getStringCellValue().trim();
                if (value.equalsIgnoreCase("NULL") || value.length() == 0) {
                    value = Integer.toString(col);
                }
                row.add(value);
            } else {
                row.add(Integer.toString(col));
            }
        }

        header = row;
    }

    private Map<String, Object> toItem(List<Object> row) {
        if (row == null) return null;

        Map<String, Object> item = new HashMap<>();

        for (int i = 0; i < row.size(); i++) {
            String key;
            if(hasHeader && i < header.size()) {
                key = header.get(i);
            } else {
                key = Integer.toString(i);
            }
            item.put(key, row.get(i));
        }

        return item;
    }
}
