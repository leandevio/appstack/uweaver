/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * business application.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.file;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.FileIOException;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.DateTimeConverter;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * <h1>XlsDataFileWriter</h1>
 * The XlsDataFileWriter class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author Jason Lin
 * @version 1.0
 * @since 2014-04-27
 */
public class XlsDataFileWriter implements DataFileWriter {
    private Logger LOGGER = LoggerFactory.getLogger(XlsDataFileWriter.class);

    private File file = null;
    private List<String> header = null;
    private List<Class> type = null;
    private String dateTimePattern = null;
    private Object anchor = null;

    private HSSFWorkbook workbook;
    private HSSFSheet sheet;
    private CellStyle dateCellStyle;
    private Converters converters = new Converters();

    public XlsDataFileWriter(File file, Map<String, ?> cfg) {
        this.file = file;
        initialize(cfg);
    }

    public XlsDataFileWriter(File file) {
        Map<String, ?> cfg = new HashMap<>();
        this.file = file;
        initialize(cfg);
    }

    @SuppressWarnings("unchecked")
    private void config(Map cfg) {
        if (cfg.containsKey("header")) header = (List<String>) cfg.get("header");
        if (cfg.containsKey("type")) type = (List<Class>) cfg.get("type");
        if (cfg.containsKey("dateTimePattern")) setDateTimePattern((String) cfg.get("dateTimePattern"));

        if (cfg.containsKey("anchor")) anchor = cfg.get("anchor");
    }

    private void initialize(Map<String, ?> cfg) {
        workbook = new HSSFWorkbook();

        if (cfg.containsKey("header")) header = (List<String>) cfg.get("header");
        if (cfg.containsKey("type")) type = (List<Class>) cfg.get("type");
        if (cfg.containsKey("anchor")) anchor = cfg.get("anchor");

        if (anchor instanceof String) {
            sheet = workbook.createSheet((String) anchor);
        } else {
            sheet = workbook.createSheet();
        }
        if (cfg.containsKey("dateTimePattern")) {
            setDateTimePattern((String) cfg.get("dateTimePattern"));
        } else {
            setDateTimePattern(Environment.getDefaultInstance().datetimePattern());
        }
    }

    @Override
    public void setAnchor(Object anchor) {
        if (anchor instanceof String) {
            sheet = workbook.getSheet((String) anchor);
            if(sheet==null) sheet = workbook.createSheet((String) anchor);
        } else if (anchor instanceof Integer){
            sheet = workbook.getSheetAt((Integer) anchor);
            if(sheet==null) sheet = workbook.createSheet();
        }

        this.anchor = anchor;
    }

    @Override
    public void close() {
        try {
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        } catch(IOException ex) {
            throw new FileIOException(ex);
        }
    }

    @Override
    public void writeHeader()  {
        if(header!=null) {
            Row record = sheet.createRow(0);
            for(int i=0; i<header.size(); i++) {
                Cell cell = record.createCell(i);
                cell.setCellValue(header.get(i));
            }
        }
    }

    @Override
    public void writeRow(List<?> row) {
        Row record = sheet.createRow(sheet.getLastRowNum()+1);
        for(int i=0; i< row.size(); i++) {
            Cell cell = record.createCell(i);
            Object value = row.get(i);
            if(value == null) {
                cell.setCellValue("");
            } else if(value instanceof String) {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue((String) value);
            } else if(value instanceof Number) {
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellValue(converters.convert(value, Double.class));
            } else if(value instanceof Date || value instanceof DateTime || value instanceof Calendar) {
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellStyle(dateCellStyle);
                cell.setCellValue(converters.convert(value, Date.class));
            } else if(value instanceof Boolean) {
                cell.setCellType(Cell.CELL_TYPE_BOOLEAN);
                cell.setCellValue((Boolean) value);
            } else {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(converters.convert(value, String.class));
            }
        }
    }

    @Override
    public void writeItem(Map<String, ?> item) {
        List<Object> row = new ArrayList<>();

        for(String key : getHeader()) {
            row.add(item.get(key));
        }

        if (this.type != null) {
            for (int i = 0; i < row.size(); i++) {
                Class clazz = (i < this.type.size() ? this.type.get(i) : null);
                if(clazz != null) row.set(i, converters.convert(row.get(i), clazz));
            }
        }

        writeRow(row);
    }

    @Override
    public List<String> getHeader() {
        return this.header;
    }

    @Override
    public void setHeader(List<String> header) {
        this.header = header;
    }

    @Override
    public String getDateTimePattern() {
        return dateTimePattern;
    }

    @Override
    public void setDateTimePattern(String dateTimePattern) {

        this.dateTimePattern = dateTimePattern;
        DateTimeConverter dtConverter = (DateTimeConverter) converters.lookup(Date.class);
        dtConverter.setDateTimePattern(dateTimePattern);
        dateCellStyle = workbook.createCellStyle();
        CreationHelper helper = workbook.getCreationHelper();
        dateCellStyle.setDataFormat(helper.createDataFormat().getFormat(dateTimePattern));
    }

    @Override
    public List<String> getAnchors() {
        List<String> anchors = new ArrayList<>();

        for(int i=0; i<workbook.getNumberOfSheets(); i++) {
            anchors.add(workbook.getSheetName(i));
        }

        return anchors;
    }

}
