/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.json;

import org.joda.time.DateTime;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.exception.FileIOException;
import org.uweaver.core.exception.InvalidFormatException;
import org.uweaver.core.reflection.TypeUtils;
import org.uweaver.core.util.*;
import org.uweaver.core.util.Collection;

import java.io.*;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * ## JSON Parser
 * ### The class implements the JSON serialization & deserialization of Java objects.
 *
 * The implementation provides:
 * - ISO 8601 compliance: Serialize & deserialize the java.util.Date, java.util.Calendar, java.sql.Date & org.joda.time.DateTime to ISO 8601 data & time representation format.
 *
 * Usage:
 *
 * ```java
 * JSONParser obj = new JSONParser();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class JSONParser {
    private static final String JSONFORMAT = "<ref>https://en.wikipedia.org/wiki/JSON</ref>";
    private static final Class[] SCALARTYPES = {Byte.class, Short.class, Integer.class, Long.class, Float.class,
            Double.class, Boolean.class, Character.class,
            BigInteger.class, BigDecimal.class,
            Date.class, java.sql.Date.class, Calendar.class, DateTime.class,
            String.class, StringBuffer.class,
            UUID.class};
    private static final Class[] IGNORETYPES = {Class.class, byte[].class, byte.class};

    private Converters converters = new Converters();

    private Set<JSONFeature> features = new HashSet<>();

    public JSONParser() {}

    public void register(Class<?> type, Converter converter) {
        converters.register(type, converter);
    }

    public void setDateTimePattern(String dateTimePattern) {
        converters.setDateTimePattern(dateTimePattern);
    }

    public <T> T readValue(Reader reader, Class<T> clazz, String ... excludes) {
        T any = null;
        JSONReader jsonReader = new JSONReader(reader);
        any = (T) parse(jsonReader, clazz, excludes);
        return any;
    }

    public <T> T readValue(InputStream in, Class<T> clazz, String ... excludes) {
        Reader reader = new InputStreamReader(in);

        return readValue(reader, clazz, excludes);
    }

    public <T> T readValue(Path file, Class<T> clazz, String ... excludes) {
        return readValue(file.toFile(), clazz, excludes);
    }

    public <T> T readValue(File file, Class<T> clazz, String ... excludes) {
        T value;
        try {
            Reader reader = new FileReader(file);
            value = readValue(reader, clazz, excludes);
            reader.close();
        } catch (FileNotFoundException e) {
            throw new FileIOException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return value;
    }

    public <T> T readValue(String json, Class<T> clazz, String ... excludes) {
        T any = null;
        if(json==null) return any;
        JSONReader reader = new JSONReader(json);
        any = (T) parse(reader, clazz, excludes);
        return any;
    }

    public Object readValue(String json, Type type, String ... excludes) {
        Object any = null;
        if(json==null) return any;
        JSONReader reader = new JSONReader(json);
        any = parse(reader, type, excludes);
        return any;
    }


    private Object parse(JSONReader reader, Type type, String ... excludes) {
        Object any;
        Class<?> rawType = TypeUtils.getRawType(type);
        if(java.util.Collection.class.isAssignableFrom(rawType)) {
            any = parseCollection(reader, type, excludes);
        } else if(Map.class.isAssignableFrom(rawType)) {
            any = parseMap(reader, type, excludes);
        } else if(rawType.isArray()) {
            any = parseArray(reader, type, excludes);
        } else {
            any = parseBean(reader, type, excludes);
        }

        return any;
    }

    private Object parseBean(JSONReader reader, Type type, String ... excludes) {
        Class rawType = TypeUtils.getRawType(type);
        Object any = TypeUtils.newInstance(rawType);
        Item item = new Item(any);

        JSONToken jsonToken = (reader.currentToken()==null) ? reader.nextToken() : reader.currentToken();
        while(!jsonToken.isObjectStart()&&!reader.isClosed()) {
            jsonToken = reader.nextToken();
        }

        if(!jsonToken.isObjectStart()) return null;

        List<String> excludeList = Arrays.asList(excludes);
        jsonToken = reader.nextToken();
        while(!jsonToken.isObjectEnd()) {
            if(jsonToken.isFieldName()) {
                String fieldname = reader.getText();
                jsonToken = reader.nextToken();
                if(excludeList.contains(fieldname)) {
                    reader.skipChildren();
                    continue;
                }

                Method setter = item.getWriteMethod(fieldname);
                if(setter==null) {
                    reader.skipChildren();
                    continue;
                }
                Type paraType = setter.getGenericParameterTypes()[0];
                Class<?> paraRawType = TypeUtils.getRawType(paraType);
                if(isIgnoreType(paraRawType)) {
                    reader.skipChildren();
                    continue;
                } else if(jsonToken.isNull()) {
                    item.put(fieldname, jsonToken.getValue());
                } else if(isScalarType(paraRawType)) {
                    item.put(fieldname, converters.convert(jsonToken.getValue(), paraRawType));
                } else if(paraRawType.equals(Path.class)) {
                    item.put(fieldname, Paths.get(jsonToken.getValueAsString()));
                } else if(paraRawType.equals(URL.class)) {
                    try {
                        item.put(fieldname, new URL(jsonToken.getValueAsString()));
                    } catch (MalformedURLException e) {
                        throw new InvalidFormatException(e);
                    }
                } else if(paraRawType==Object.class) {
                    if(jsonToken.isArrayStart()) {
                        item.put(fieldname, parseCollection(reader, List.class, excludes));
                    } else if(jsonToken.isObjectStart()) {
                        item.put(fieldname, parseMap(reader, Map.class, excludes));
                    } else {
                        item.put(fieldname, jsonToken.getValue());
                    }
                } else if(paraRawType.isEnum()) {
                    String name = jsonToken.getValueAsString();
                    item.put(fieldname, TypeUtils.newEnum(paraRawType, name));
                } else {
                    item.put(fieldname, parse(reader, paraType, excludes));
                }
            }
            jsonToken = reader.nextToken();
        }
        return any;
    }

    private Map parseMap(JSONReader reader, Type type, String ... excludes) {
        Class rawType = TypeUtils.getRawType(type);
        Map any = (rawType.isInterface()) ? new HashMap() : (Map) TypeUtils.newInstance(rawType);
        Type paraType;
        if(type instanceof ParameterizedType && ((ParameterizedType) type).getActualTypeArguments().length>=2) {
            paraType = ((ParameterizedType) type).getActualTypeArguments()[1];
        } else {
            paraType = Object.class;
        }

        Class<?> paraRawType = TypeUtils.getRawType(paraType);

        JSONToken jsonToken = (reader.currentToken()==null) ? reader.nextToken() : reader.currentToken();
        while(!jsonToken.isObjectStart()&&!reader.isClosed()) {
            jsonToken = reader.nextToken();
        }

        if(!jsonToken.isObjectStart()) return null;

        List<String> excludeList = Arrays.asList(excludes);
        jsonToken = reader.nextToken();
        while(!jsonToken.isObjectEnd()) {
            if(jsonToken.isFieldName()) {
                String fieldname = reader.getText();
                jsonToken = reader.nextToken();
                if(excludeList.contains(fieldname)||isIgnoreType(paraRawType)) {
                    reader.skipChildren();
                    continue;
                } else if(jsonToken.isNull()) {
                    any.put(fieldname, jsonToken.getValue());
                } else if(isScalarType(paraRawType)) {
                    any.put(fieldname, converters.convert(jsonToken.getValue(), paraRawType));
                } else if(paraRawType == Object.class) {
                    if(jsonToken.isArrayStart()) {
                        any.put(fieldname, parseCollection(reader, List.class, excludes));
                    } else if(jsonToken.isObjectStart()) {
                        any.put(fieldname, parseMap(reader, Map.class, excludes));
                    } else if(paraRawType.isEnum()) {
                        String name = jsonToken.getValueAsString();
                        any.put(fieldname, TypeUtils.newEnum(paraRawType, name));
                    } else {
                        any.put(fieldname, jsonToken.getValue());
                    }
                } else if(paraRawType.isEnum()) {
                    String name = jsonToken.getValueAsString();
                    any.put(fieldname, TypeUtils.newEnum(paraRawType, name));
                } else {
                    any.put(fieldname, parse(reader, paraType));
                }
            }
            jsonToken = reader.nextToken();
        }


        return any;
    }

    private java.util.Collection parseCollection(JSONReader reader, Type type, String ... excludes) {
        JSONToken jsonToken = (reader.currentToken()==null) ? reader.nextToken() : reader.currentToken();
        while(!jsonToken.isArrayStart()&&!reader.isClosed()) {
            jsonToken = reader.nextToken();
        }

        if(!jsonToken.isArrayStart()) return null;
        jsonToken = reader.nextToken();

        Class rawType = TypeUtils.getRawType(type);
        java.util.Collection any;
        if(!rawType.isInterface()) {
            any = (java.util.Collection) TypeUtils.newInstance(rawType);
        } else if(List.class.equals(rawType)){
            any = new ArrayList<>();
        } else if(Set.class.equals(rawType)) {
            any = new HashSet<>();
        } else {
            any = new ArrayList<>();
        }

        Type paraType;
        if(type.equals(Collection.class)) {
            paraType = Model.class;
        } else if(type instanceof ParameterizedType) {
            paraType = ((ParameterizedType) type).getActualTypeArguments()[0];
        } else if(jsonToken.isObjectStart()) {
            paraType = Map.class;
        } else {
            paraType = Object.class;
        }

        Class<?> paraRawType = TypeUtils.getRawType(paraType);

        while(!jsonToken.isArrayEnd()) {
            if(isIgnoreType(paraRawType)) {
                reader.skipChildren();
                continue;
            } else if(isScalarType(paraRawType)) {
                any.add(converters.convert(jsonToken.getValue(), paraRawType));
            } else if(paraRawType==Object.class) {
                any.add(jsonToken.getValue());
            } else {
                any.add(parse(reader, paraType, excludes));
            }
            jsonToken = reader.nextToken();
        }

        return any;
    }

    private Object parseArray(JSONReader reader, Type type, String ... excludes) {
        JSONToken jsonToken = (reader.currentToken()==null) ? reader.nextToken() : reader.currentToken();
        while(!jsonToken.isArrayStart()&&!reader.isClosed()) {
            jsonToken = reader.nextToken();
        }

        if(!jsonToken.isArrayStart()) return null;

        jsonToken = reader.nextToken();

        List any = new ArrayList();

        Type paraType = ((Class) type).getComponentType();

        Class<?> paraRawType = ((Class) type).getComponentType();

        while(!jsonToken.isArrayEnd()) {
            if(isIgnoreType(paraRawType)) {
                reader.skipChildren();
                continue;
            } else if(isScalarType(paraRawType)) {
                any.add(converters.convert(jsonToken.getValue(), paraRawType));
            } else if(paraRawType==Object.class) {
                any.add(jsonToken.getValue());
            } else {
                any.add(parse(reader, paraType, excludes));
            }
            jsonToken = reader.nextToken();
        }


        Object[] objects = (Object[]) Array.newInstance(paraRawType, any.size());

        return any.toArray(objects);
    }



    public String writeValueAsString(Object any, String ... excludes){
        if(any==null) return null;

        StringWriter stringWriter = new StringWriter();
        JSONWriter writer = new JSONWriter(stringWriter);
        for(JSONFeature feature : features) {
            writer.enable(feature);
        }

        write(writer, any, excludes);

        writer.close();

        return stringWriter.toString();

    }

    private void write(JSONWriter writer, Object any, String ... excludes) {
        Class<?> rawType = any.getClass();
        if(java.util.Collection.class.isAssignableFrom(rawType)) {
            writeCollection(writer, (java.util.Collection) any, excludes);
        } else if(Map.class.isAssignableFrom(rawType)) {
            writeMap(writer, (Map) any, excludes);
        } else if(rawType.isArray()) {
            writeArray(writer, any, excludes);
        } else {
            writeBean(writer, any, excludes);
        }

    }

    private void writeBean(JSONWriter writer, Object any, String ... excludes) {
        Method method = null;
        try {
            method = any.getClass().getMethod("toJSON");
            Map<String, Object> map =  (Map<String, Object>) method.invoke(any);
            writeMap(writer, map, excludes);
        } catch (NoSuchMethodException e) {
            Item item = new Item(any);
            writeItem(writer, item, excludes);
        } catch (InvocationTargetException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }


    }

    private void writeCollection(JSONWriter writer, java.util.Collection any, String ... excludes) {
        writer.writeArrayStart();

        for(Object value : any) {
            if(value==null) {
                writer.writeNull();
                continue;
            }

            Class<?> type = value.getClass();
            if(isIgnoreType(type)) {
                continue;
            } else if(isScalarType(type)) {
                writer.writeValue(value);
            } else {
                write(writer, value, excludes);
            }
        }
        writer.writeArrayEnd();
    }


    private void writeMap(JSONWriter writer, Map any, String ... excludes) {
        List<String> excludeList = Arrays.asList(excludes);
        writer.writeObjectStart();
        for(Object key: any.keySet()) {
            String fieldname = (String) key;
            if(excludeList.contains(fieldname)) continue;

            Object value = any.get(key);

            if(value==null) {
                writer.writeFieldName(fieldname);
                writer.writeNull();
                continue;
            }

            Class<?> type = value.getClass();
            if(isIgnoreType(type)) {
                continue;
            } else if(isScalarType(type)) {
                writer.writeFieldName(fieldname);
                writer.writeValue(value);
            } else if(type.isEnum()) {
                writer.writeFieldName(fieldname);
                writer.writeValue(MethodUtils.invokeMethod(value, "name"));
            } else {
                writer.writeFieldName(fieldname);
                write(writer, value);
            }
        }

        writer.writeObjectEnd();
    }

    private void writeItem(JSONWriter writer, Item item, String ... excludes) {
        List<String> excludeList = Arrays.asList(excludes);
        writer.writeObjectStart();

        for(Map.Entry entry : item.entrySet()) {
            String fieldname = (String) entry.getKey();
            if(excludeList.contains(fieldname)) continue;
            Method getter = item.getReadMethod(fieldname);
            if(getter==null) {
                continue;
            }
            Object value = entry.getValue();
            if(value==null) {
                writer.writeFieldName(fieldname);
                writer.writeNull();
                continue;
            }
            Class<?> type = value.getClass();
            if(isIgnoreType(type)) {
                continue;
            } else if(isScalarType(type)) {
                writer.writeFieldName(fieldname);
                writer.writeValue(value);
            } else if(type.isEnum()) {
                writer.writeFieldName(fieldname);
                writer.writeValue(MethodUtils.invokeMethod(value, "name"));
            } else {
                writer.writeFieldName(fieldname);
                write(writer, value);
            }
        }

        writer.writeObjectEnd();
    }

    private void writeArray(JSONWriter writer, Object any, String ... excludes) {
        writeCollection(writer, Arrays.asList((Object[]) any), excludes);
    }


    public JSONParser enable(JSONFeature feature) {
        features.add(feature);
        return this;
    }

    public JSONParser disable(JSONFeature feature) {
        features.remove(feature);
        return this;
    }

    private boolean isScalarType(Class type) {
        boolean result = false;

        if(type.isPrimitive()) result = true;
        else {
            for(int i=0; i<SCALARTYPES.length; i++) {
                if(SCALARTYPES[i].isAssignableFrom(type)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    private boolean isIgnoreType(Class type) {
        boolean result = false;
        for(int i=0; i<IGNORETYPES.length; i++) {
            if(IGNORETYPES[i].isAssignableFrom(type)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
