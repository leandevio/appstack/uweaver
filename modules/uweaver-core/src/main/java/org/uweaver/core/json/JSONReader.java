/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import org.uweaver.core.util.DateTimeConverter;

import java.io.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ## JSON Reader
 * ###The class implements the streaming JSON deserialization for Java objects.
 *
 * The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * JSONReader obj = new JSONReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */

public class JSONReader {

    // ISO 8601 data & time representation format.
    private static final String DEFAULTDATETIMEPATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private Pattern datetimePattern;

    JsonFactory jsonFactory = new JsonFactory();
    JsonParser jsonParser;

    DateTimeConverter dateTimeConverter = new DateTimeConverter();

    public JSONReader(Reader reader) {
        init(reader);
    }

    public JSONReader(InputStream in) {
        init(new InputStreamReader(in));
    }

    public JSONReader(String s) {
        init(new StringReader(s));
    }

    private void init(Reader reader) {
        try {
            jsonParser = jsonFactory.createParser(reader);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        datetimePattern = Pattern.compile("^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}(\\.[0-9]{1,3})?(\\+[0-9]{4}|Z)");
    }

    public boolean isClosed() {
        return jsonParser.isClosed();
    }

    public void close() {
        try {
            jsonParser.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public JSONToken nextToken() {
        try {
            jsonParser.nextToken();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return currentToken();
    }

    public JSONToken currentToken() {
        JsonToken jsonToken = jsonParser.getCurrentToken();

        JSONToken token = null;

        try {
            if(jsonToken==JsonToken.START_OBJECT) {
                token = new JSONToken(JSONToken.Type.OBJECTSTART, jsonParser.getText());
            } else if(jsonToken==JsonToken.END_OBJECT) {
                token = new JSONToken(JSONToken.Type.OBJECTEND, jsonParser.getText());
            } else if(jsonToken==JsonToken.START_ARRAY) {
                token = new JSONToken(JSONToken.Type.ARRAYSTART, jsonParser.getText());
            } else if(jsonToken==JsonToken.END_ARRAY) {
                token = new JSONToken(JSONToken.Type.ARRAYEND, jsonParser.getText());
            } else if(jsonToken==JsonToken.FIELD_NAME) {
                token = new JSONToken(JSONToken.Type.FIELDNAME, jsonParser.getText());
            } else if(jsonToken==JsonToken.VALUE_NULL) {
                token = new JSONToken(JSONToken.Type.VALUENULL, null);
            } else if(jsonToken==JsonToken.VALUE_NUMBER_INT) {
                token = new JSONToken(JSONToken.Type.VALUEINTEGER, jsonParser.getValueAsInt());
            } else if(jsonToken==JsonToken.VALUE_NUMBER_FLOAT) {
                token = new JSONToken(JSONToken.Type.VALUEFLOAT, jsonParser.getValueAsDouble());
            } else if(jsonToken==JsonToken.VALUE_TRUE||jsonToken==JsonToken.VALUE_FALSE) {
                token = new JSONToken(JSONToken.Type.VALUEBOOLEAN, jsonParser.getValueAsBoolean());
            } else if(jsonToken==JsonToken.VALUE_STRING) {
                String value = jsonParser.getValueAsString();
                if(isDate(value)) {
                    token = new JSONToken(JSONToken.Type.VALUEDATE, dateTimeConverter.convert(value, Date.class));
                } else {
                    token = new JSONToken(JSONToken.Type.VALUESTRING, value);
                }
            }
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }



        return token;
    }

    public String getText() {
        try {
            return jsonParser.getText();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public String getValueAsString() {
        try {
            return jsonParser.getValueAsString();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public JSONReader skipChildren() {
        try {
            jsonParser.skipChildren();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return this;
    }

    private boolean isDate(String value) {
        Matcher m = datetimePattern.matcher(value);

        return m.matches();

    }


}
