/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.json;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * JSONToken obj = new JSONToken();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class JSONToken<T> {

    Type type;
    T value;


    JSONToken(Type type, T value) {
        this.type = type;
        this.value = value;
    }

    public Type getType() {
        return this.type;
    }

    public T getValue() {
        return this.value;
    }

    public String getValueAsString() {
        return (String) this.value;
    }

    public boolean isObjectStart() {
        return type == Type.OBJECTSTART;
    }

    public boolean isObjectEnd() {
        return type == Type.OBJECTEND;
    }

    public boolean isArrayStart() {
        return type == Type.ARRAYSTART;
    }

    public boolean isArrayEnd() {
        return type == Type.ARRAYEND;
    }


    public boolean isScalar() {
        return isNumeric() || isDate() || isBoolean() || isString();
    }

    public boolean isString() {
        return (type == Type.VALUESTRING);
    }

    public boolean isNumeric() {
        return (type == Type.VALUEINTEGER) || (type == Type.VALUEFLOAT);
    }

    public boolean isInteger() {
        return (type == Type.VALUEINTEGER);
    }

    public boolean isFloat() {
        return (type == Type.VALUEFLOAT);
    }

    public boolean isDate() {
        return (type == Type.VALUEDATE);
    }

    public boolean isBoolean() {
        return (type == Type.VALUEBOOLEAN);
    }

    public boolean isFieldName() {
        return (type == Type.FIELDNAME);
    }

    public boolean isNull() {
        return type == Type.VALUENULL;
    }

    public enum Type {
        OBJECTSTART, OBJECTEND, ARRAYSTART, ARRAYEND,
        FIELDNAME,
        VALUESTRING,
        VALUEINTEGER, VALUEFLOAT,
        VALUEBOOLEAN,
        VALUEDATE,
        VALUENULL;
    }

}
