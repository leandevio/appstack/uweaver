/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.joda.time.DateTime;
import org.uweaver.core.util.DateTimeConverter;

import java.io.*;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * ## JSON Writer
 * ### The class implements the streaming JSON serialization for Java objects.
 *
 * The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * JSONWriter obj = new JSONWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class JSONWriter {
    // ISO 8601 data & time representation format.
    private static final String DEFAULTDATETIMEPATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    private static final String OBJECTSTART = "{";
    private static final String OBJECTEND = "}";
    private static final String ARRAYSTART = "[";
    private static final String ARRAYEND = "]";

    JsonFactory jsonFactory = new JsonFactory();
    JsonGenerator jsonGenerator;

    DateTimeConverter dateTimeConverter = new DateTimeConverter();

    public JSONWriter(Writer writer) {
        init(writer);
    }

    public JSONWriter(OutputStream in) {
        init(new OutputStreamWriter(in));
    }


    private void init(Writer writer) {
        try {
            jsonGenerator = jsonFactory.createGenerator(writer);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void close() {
        try {
            jsonGenerator.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void enable(JSONFeature feature) {
        if(feature.equals(JSONFeature.PRETTY)) {
            jsonGenerator.useDefaultPrettyPrinter();
        }
    }

    public void writeObjectStart() {
        try {
            jsonGenerator.writeStartObject();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeObjectEnd() {
        try {
            jsonGenerator.writeEndObject();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeArrayStart() {
        try {
            jsonGenerator.writeStartArray();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeArrayEnd() {
        try {
            jsonGenerator.writeEndArray();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeFieldName(String fieldName) {
        try {
            jsonGenerator.writeFieldName(fieldName);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeValue(Object value) {
        if(value==null) {
            writeNull();
            return;
        }

        Class type = value.getClass();

        if(Date.class.isAssignableFrom(type)) {
            writeDate((Date) value);
        } else if(Number.class.isAssignableFrom(type)) {
            writeNumber((Number) value);
        } else if(Boolean.class.isAssignableFrom(type)) {
            writeBoolean((Boolean) value);
        } else {
            writeString(value.toString());
        }
    }

    public void writeNull() {
        try {
            jsonGenerator.writeNull();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeString(String s) {
        try {
            jsonGenerator.writeString(s);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeNumber(Number value) {
        try {
            jsonGenerator.writeNumber(value.toString());
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeBoolean(Boolean value) {
        try {
            jsonGenerator.writeBoolean(value);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void writeDate(Date value) {
        writeString(dateTimeConverter.convert(value, String.class));
    }

    public void writeDate(DateTime value) {
        writeString(dateTimeConverter.convert(value, String.class));
    }

    public void writeDate(java.sql.Date value) throws IOException {
        writeString(dateTimeConverter.convert(value, String.class));
    }

    public void writeDate(Calendar value) {
        writeString(dateTimeConverter.convert(value, String.class));
    }


}
