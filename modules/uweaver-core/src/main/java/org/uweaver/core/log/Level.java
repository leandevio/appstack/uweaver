package org.uweaver.core.log;

/**
 * Created by jasonlin on 2/22/17.
 */
public enum Level {
    ERROR(4), WARN(3), INFO(2), DEBUG(1), TRACE(0);
    private final int value;
    private static final String[] labels = {"TRACE", "DEBUG", "INFO", "WARN", "ERROR"};

    Level(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return labels[value];
    }
}
