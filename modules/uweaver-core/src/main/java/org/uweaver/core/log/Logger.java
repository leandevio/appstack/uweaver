/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.log;

import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Logger obj = new Logger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Logger {
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    private static Environment environment = Environment.getDefaultInstance();
    private static int logLevel = environment.logLevel();
    private static PrintWriter writer = null;
    private static String timemark = null;
    String name;

    protected Logger(String name) {
        this.name = name;
    }

    /**
     * Log a message if the logging level is the specified level.
     * @param level the logging level
     * @param msg the message string to be logged
     */
    public void log(Level level, CharSequence msg) {
        if(level.getValue()<logLevel) return;
        StringBuilder sb = new StringBuilder(formatter.format(new Date()));
        sb.append("@").append(Thread.currentThread().getId());
        int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();;
        sb.append(" ").append(level.toString()).append(" ").append(name).append(":").append(lineNumber).append(" - ").append(msg);
        write(sb);
    }

    private void write(CharSequence msg) {
        System.out.println(msg);
        getWriter().println(msg);
        getWriter().flush();
    }

    private void write(Throwable t) {
        StringWriter out = new StringWriter();
        PrintWriter writer = new PrintWriter(out);
        t.printStackTrace(writer);
        writer.flush();
        System.out.println(out.toString());
        getWriter().println(out.toString());
        getWriter().flush();
        writer.close();
    }

    private static PrintWriter getWriter() {
        String timemark = dateFormatter.format(new Date());
        if(Logger.timemark !=null&&Logger.timemark.equals(timemark)) {
            return Logger.writer;
        }
        if(Logger.timemark!=null) {
            Logger.writer.close();
        }
        String filename = String.format("%s-%s.log", environment.appName(), timemark);
        Writer fileWriter;
        try {
            fileWriter = new FileWriter(environment.logFolder().resolve(filename).toFile(), true);
        } catch (IOException e) {
            e.printStackTrace();
            fileWriter = new NullWriter();
        }
        PrintWriter writer = new PrintWriter(fileWriter);
        Logger.timemark = timemark;
        Logger.writer = writer;
        return writer;
    }

    /**
     * Log a message according to the specified format and arguments.
     * @param format
     * @param arguments
     */
    public void log(Level level, String format, Object... arguments) {
        if(level.getValue()<logLevel) return;
        String msg = String.format(format, arguments);
        StringBuilder sb = new StringBuilder(formatter.format(new Date()));
        sb.append("@").append(Thread.currentThread().getId());
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();;
        sb.append(" ").append(level.toString()).append(" ").append(name).append(":").append(lineNumber).append(" - ").append(msg);
        write(sb);
    }

    /**
     * Log an exception (throwable) with an accompanying message.
     * @param msg the message string to be logged
     * @param t the Throwable to be logged
     */
    public void log(Level level, CharSequence msg, Throwable t) {
        if(level.getValue()<logLevel) return;
        StringBuilder sb = new StringBuilder(formatter.format(new Date()));
        sb.append("@").append(Thread.currentThread().getId());
        int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();;
        sb.append(" ").append(level.toString()).append(" ").append(name).append(":").append(lineNumber).append(" - ").append(msg);
        write(sb);
        write(t);
    }

    /**
     * Log an exception (throwable).
     * @param t
     */
    public void log(Level level, Throwable t) {
        if(level.getValue()<logLevel) return;
        String msg = "";
        StringBuilder sb = new StringBuilder(formatter.format(new Date()));
        sb.append("@").append(Thread.currentThread().getId());
        int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();;
        sb.append(" ").append(level.toString()).append(" ").append(name).append(":").append(lineNumber).append(" - ").append(msg);
        write(sb);
        write(t);
    }

    /**
     * Log a message at the ERROR level.
     * @param msg
     */
    public void error(CharSequence msg) {
        log(Level.ERROR, msg);
    }

    /**
     * Log a message at the ERROR level according to the specified format and arguments.
     * @param format
     * @param arguments
     */
    public void error(String format, Object... arguments) {
        log(Level.ERROR, format, arguments);
    }

    /**
     * Log an exception (throwable) at the ERROR level with an accompanying message.
     * @param msg
     * @param t
     */
    public void error(CharSequence msg, Throwable t) {
        log(Level.ERROR, msg, t);
    }

    /**
     * Log an exception (throwable) at the ERROR level.
     * @param t
     */
    public void error(Throwable t) {
        log(Level.ERROR, t);
    }

    /**
     * Log a message at the WARN level.
     * @param msg
     */
    public void warn(CharSequence msg) {
        log(Level.WARN, msg);
    }

    /**
     * Log a message at the WARN level according to the specified format and arguments.
     * @param format
     * @param arguments
     */
    public void warn(String format, Object... arguments) {
        log(Level.WARN, format, arguments);
    }

    /**
     * Log an exception (throwable) at the WARN level with an accompanying message.
     * @param msg
     * @param t
     */
    public void warn(CharSequence msg, Throwable t) {
        log(Level.WARN, msg, t);
    }

    /**
     * Log an exception (throwable) at the WARN level.
     * @param t
     */
    public void warn(Throwable t) {
        log(Level.WARN, t);
    }

    /**
     * Log a message at the INFO level.
     * @param msg
     */
    public void info(CharSequence msg) {
        log(Level.INFO, msg);
    }

    /**
     * Log a message at the INFO level according to the specified format and arguments.
     * @param format
     * @param arguments
     */
    public void info(String format, Object... arguments) {
        log(Level.INFO, format, arguments);
    }

    /**
     * Log an exception (throwable) at the INFO level with an accompanying message.
     * @param msg
     * @param t
     */
    public void info(CharSequence msg, Throwable t) {
        log(Level.INFO, msg, t);
    }

    /**
     * Log an exception (throwable) at the INFO level.
     * @param t
     */
    public void info(Throwable t) {
        log(Level.INFO, t);
    }

    /**
     * Log a message at the DEBUG level.
     * @param msg
     */
    public void debug(CharSequence msg) {
        log(Level.DEBUG, msg);
    }

    /**
     * Log a message at the DEBUG level according to the specified format and arguments.
     * @param format
     * @param arguments
     */
    public void debug(String format, Object... arguments) {
        log(Level.DEBUG, format, arguments);
    }

    /**
     * Log an exception (throwable) at the DEBUG level with an accompanying message.
     * @param msg
     * @param t
     */
    public void debug(CharSequence msg, Throwable t) {
        log(Level.DEBUG, msg, t);
    }

    /**
     * Log an exception (throwable) at the DEBUG level.
     * @param t
     */
    public void debug(Throwable t) {
        log(Level.DEBUG, t);
    }

    /**
     * Log a message at the TRACE level.
     * @param msg
     */
    public void trace(CharSequence msg) {
        log(Level.TRACE, msg);
    }

    /**
     * Log a message at the TRACE level according to the specified format and arguments.
     * @param format
     * @param arguments
     */
    public void trace(String format, Object... arguments) {
        log(Level.TRACE, format, arguments);
    }

    /**
     * Log an exception (throwable) at the TRACE level with an accompanying message.
     * @param msg
     * @param t
     */
    public void trace(CharSequence msg, Throwable t) {
        log(Level.TRACE, msg, t);
    }

    /**
     * Log an exception (throwable) at the TRACE level.
     * @param t
     */
    public void trace(Throwable t) {
        log(Level.TRACE, t);
    }

}
