/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.log;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * public class Customers {
 *      private static final Logger LOGGER = LoggerManager.getLogger(Customer.class);
 *
 *      ...
 *
 *      public void placeOrder() {
 *          ....
 *          LOGGER.debug("Order placed.")
 *     }
 *
 *     ...
 * }
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */

public class LoggerManager {
    /**
     * Return a logger named corresponding to the class passed as parameter
     * @param clazz
     * @return
     */
    public static Logger getLogger(Class<?> clazz) {
        return getLogger(clazz.getCanonicalName());
    }

    /**
     * Return a logger named corresponding to the name parameter
     * @param name
     * @return
     */
    public static Logger getLogger(String name) {
        return new Logger(name);
    }
}
