package org.uweaver.core.mail;

/**
 * Created by jasonlin on 5/13/14.
 */
public class HtmlMail extends Mail {

    public HtmlMail() {
        super();
        mimeType = "text/html";
    }

}
