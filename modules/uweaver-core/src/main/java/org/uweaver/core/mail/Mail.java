package org.uweaver.core.mail;

import org.uweaver.core.exception.ConversionException;
import org.uweaver.core.exception.InvalidRequestException;
import org.uweaver.core.exception.NotEmptyConflictException;
import org.uweaver.core.util.Environment;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by jasonlin on 5/13/14.
 */
public class Mail {
    protected String subject = null;
    protected String text = null;
    protected String content = null;
    protected List<File> attachments = new ArrayList<>();
    protected Date sentDate = null;
    protected Address from = null;
    protected Map<RecipientType, Address[]> recipients;
    protected String charset = Environment.getDefaultInstance().property("mail.charset", "UTF-8");
    protected String mimeType = "text/plain";

    protected MailSender sender = null;

    public Mail() {
        initialize();
    }

    private void initialize() {
        recipients = new HashMap<>();
        recipients.put(RecipientType.TO, new Address[0]);
        recipients.put(RecipientType.CC, new Address[0]);
        recipients.put(RecipientType.BCC, new Address[0]);
    }

    public void setRecipients(RecipientType type, Address... address) {
        recipients.put(type, address);
    }

    public void setRecipients(RecipientType type, String... address) {
        recipients.put(type, convertToMailAddress(address));
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public void setAttachments(List<File> attachments) {
        this.attachments = attachments;
    }

    public List<File> getAttachments() {
        return attachments;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public void setFrom(Address from) {
        this.from = from;
    }

    public Address getFrom() {
        return from;
    }

    public void setFrom(String address, String name) {
        this.from = convertToMailAddress(address, name);
    }

    public void setFrom(String address) {
        this.from = convertToMailAddress(address, null);
    }

    public void setTo(String... address) {
        setRecipients(RecipientType.TO, address);
    }

    public Address[] getTo() {
        return recipients.get(RecipientType.TO);
    }

    public void setCC(Address... address) {
        setRecipients(RecipientType.CC, address);
    }

    public void setCC(String... address) {
        setRecipients(RecipientType.CC, address);
    }

    public Address[] getCC() {
        return recipients.get(RecipientType.CC);
    }

    public void setBCC(String... address) {
        setRecipients(RecipientType.BCC, address);
    }

    public Address[] getBCC() {
        return recipients.get(RecipientType.BCC);
    }

    public void setSender(MailSender sender) {
        this.sender = sender;
    }

    public MailSender getSender() {
        return sender;
    }

    public void send() {
        sender.send(this);
    }

    public MimeMessage toMimeMessage() {

        if(sender==null) throw new NotEmptyConflictException("{0} is required.", "sender");

        MimeMessage message = sender.createMessage();

        try {
            if(from!=null) message.setFrom(from);
            message.setRecipients(Message.RecipientType.TO, getTo());
            message.setRecipients(Message.RecipientType.CC, getCC());
            message.setRecipients(Message.RecipientType.BCC, getBCC());
            if(subject!=null) message.setSubject(subject, charset);
            if(sentDate!=null) message.setSentDate(sentDate);

            if(isMultipart()) {
                Multipart multipart = new MimeMultipart();
                appendTextPart(multipart);
                appendAttachmentPart(multipart);
                message.setContent(multipart);
            } else {
                if(text!=null) message.setText(text, charset);
                if(content!=null) {
                    String contentType = mimeType + "; charset=" + charset;
                    message.setContent(content, contentType);
                }
            }
        } catch (MessagingException e) {
            throw new InvalidRequestException(e);
        }

        return message;
    }

    private boolean isMultipart() {
        return (attachments.size()>0);
    }

    private void appendTextPart(Multipart multipart) {
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        try {
            if(text!=null) messageBodyPart.setText(text, charset);
            if(content!=null) {
                String contentType = mimeType + "; charset=" + charset;
                messageBodyPart.setContent(content, contentType);
            }
            multipart.addBodyPart(messageBodyPart);
        } catch (MessagingException e) {
            throw new InvalidRequestException(e);
        }
    }

    private void appendAttachmentPart(Multipart multipart) {
        try {
            for(File attachment : attachments) {
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(attachment);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(attachment.getName());
                multipart.addBodyPart(messageBodyPart);
            }
        } catch (MessagingException e) {
            throw new InvalidRequestException(e);
        }
    }

    private Address[] convertToMailAddress(String[] addresses) {
        List<Address> iAddress = new ArrayList<>();
        for(String address : addresses) {
            try {
                iAddress.add(new InternetAddress(address));
            } catch (AddressException e) {
                throw new ConversionException(address, InternetAddress.class.getName(), e);
            }
        }

        return iAddress.toArray(new Address[0]);
    }

    private Address convertToMailAddress(String address, String name) {
        InternetAddress iAddress = null;

        try {
            iAddress = (name==null) ? new InternetAddress(address) : new InternetAddress(address, name);
        } catch (AddressException e) {
            throw new ConversionException(address, InternetAddress.class.getName(), e);
        } catch (UnsupportedEncodingException e) {
            throw new ConversionException(address, InternetAddress.class.getName(), e);
        }
        return iAddress;
    }

    public enum RecipientType {
        TO("TO"), CC("CC"), BCC("BCC");

        private final String type;

        private RecipientType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public String toString() {
            return type;
        }
    }

}
