package org.uweaver.core.mail;

import org.uweaver.core.exception.OperationFailureException;
import org.uweaver.core.exception.UnauthorizedException;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by jasonlin on 5/12/14.
 */
public class MailSender {
    private static final Logger LOGGER = LogManager.getLogger(MailSender.class);
    private Environment environment = Environment.getDefaultInstance();
    protected Session session = null;
    protected boolean auth = environment.propertyAsBoolean("mail.smtp.auth", true);
    protected String host = environment.property("mail.smtp.host");
    // port number 的型別一定要用字串, 已知使用數字在 Tomcat 下會沒有作用。
    protected int port = environment.propertyAsInteger("mail.smtp.port", 0);
    protected boolean ssl = environment.propertyAsBoolean("mail.smtp.ssl.enable", false);
    protected boolean starttls = environment.propertyAsBoolean("mail.smtp.starttls.enable", false);

    protected String username = environment.property("mail.smtp.username", null);
    protected String password = environment.property("mail.smtp.password", null);


    public MailSender() {
        init();
    }

    public MailSender(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        init();
    }

    private void init() {
        if(port == 0) {
            port = (ssl) ? 465 : 25;
        }
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", Boolean.toString(auth));
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", Integer.toString(port));
        properties.put("mail.smtp.ssl.enable", Boolean.toString(ssl));
        properties.put("mail.smtp.starttls.enable", Boolean.toString(starttls));
        properties.put("mail.smtp.ssl.trust", properties.getProperty("mail.smtp.host"));
        session = Session.getInstance(properties);
        setDebug(environment.propertyAsBoolean("mail.debug", false));
        Properties props = System.getProperties();
        props.put("mail.mime.encodefilename", "true");
    }

    public boolean test() {
        boolean result = true;
        try {
            Transport transport = session.getTransport("smtp");
            if(auth) {
                transport.connect(username, password);
            } else {
                transport.connect();
            }
            transport.close();
        } catch (Exception e) {
            LOGGER.debug(e);
            result = false;
        }

        return result;
    }

    public MimeMessage createMessage() {
        MimeMessage message = new MimeMessage(session);

        return message;
    }

    public void send(Mail mail) {
        send(mail.toMimeMessage());
    }

    public void send(Mail[] mails) {
        for(Mail mail : mails) {
            send(mail.toMimeMessage());
        }
    }

    public void setDebug(boolean debug) {
        session.setDebug(debug);
    }

    public boolean isDebug() {
        return session.getDebug();
    }

    public void send(MimeMessage message) {
        try {
            Transport transport = session.getTransport();
            if(auth) {
                transport.connect(username, password);
            } else {
                transport.connect();
            }
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }  catch (AuthenticationFailedException e) {
            throw new UnauthorizedException(username, e);
        }  catch (MessagingException e) {
            throw new OperationFailureException(e);
        }
    }


    public void send(MimeMessage[] messages) {
        for(MimeMessage message : messages) {
            send(message);
        }
    }

}
