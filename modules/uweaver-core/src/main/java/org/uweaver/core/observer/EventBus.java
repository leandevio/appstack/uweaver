/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.observer;

import org.uweaver.core.event.Event;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * EventBus obj = new EventBus();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class EventBus {
    private final Subject<Event, Event> rxSubject = new SerializedSubject<>(PublishSubject.<Event>create());


    private static class SingletonHolder {
        public static final EventBus INSTANCE = new EventBus();
    }

    public static EventBus getDefaultInstance() {
        return EventBus.SingletonHolder.INSTANCE;
    }

    public <T extends Event> rx.Subscription subscribe(final Class<T> eventClass, Action1<T> onNext) {
        return rxSubject.filter(new Func1() {
            @Override
            public Object call(Object event) {
                return event.getClass().equals(eventClass);
            }
        }).map(new Func1() {
            @Override
            public Object call(Object event) {
                return (T) event;
            }
        }).subscribe(onNext);

    }


    public void post(Event event) {
        rxSubject.onNext(event);
    }

}
