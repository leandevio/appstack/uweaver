/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.observer;

import org.uweaver.core.event.server.ServerEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Subscriber obj = new Subscriber();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public abstract class Subscriber<T> implements Observer<T>, Subscription {
    protected Subscriber() {

    }
    protected 	Subscriber(Subscriber<?> subscriber) {

    }

    public final void add(Subscription s) {

    }

    public final void unsubscribe() {

    }

    public final boolean isUnsubscribed() {
        return false;
    }

    public rx.Subscriber<T> toRxSubscriber() {
       rx.Subscriber<T> rxSubscriber = null;

       return rxSubscriber;
    }

    public static rx.Subscriber create(Class type) {
        return new Wrapper(type);
    }

    private static class Wrapper extends rx.Subscriber {
        private Object instance;
        private Method subscribe;
        Wrapper(Class type) {
            try {
                instance = type.newInstance();
                subscribe = type.getMethods()[0];
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {

        }

        @Override
        public void onNext(Object t) {
            try {
                subscribe.invoke(instance, t);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}
