/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.reflection;

import com.google.common.base.Predicate;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.uweaver.core.query.Query;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TypeQuery obj = new TypeQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

public class TypeQuery implements Query<TypeQuery, Class<?>> {
    private Reflections reflections;

    private Class<? extends Annotation> annotation = null;
    private List<String> includeRegexps = new ArrayList<>();
    private List<String> excludeRegexps = new ArrayList<>();

    TypeQuery(Reflections reflections) {
        this.reflections = reflections;
    }

    @Override
    public long count() {
        return list().size();
    }

    @Override
    public List<Class<?>> list() {
        List<Class<?>> types = new ArrayList<>();

        FilterBuilder filterBuilder = new FilterBuilder();
        for(String name : includeRegexps) {
            filterBuilder.include(name);
        }

        for(String name : excludeRegexps) {
            filterBuilder.exclude(name);
        }

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        Predicate<String> filters = filterBuilder;

        configurationBuilder.filterInputsBy(filters);
        configurationBuilder.setUrls(reflections.getUrls());
        org.reflections.Reflections reflections = new org.reflections.Reflections(configurationBuilder);

        types.addAll(reflections.getTypesAnnotatedWith(annotation));

        return types;
    }

    @Override
    public Class<?> singleResult() {
        List<Class<?>> types = list();
        return types.size()==0 ? null : types.get(0);
    }

    public TypeQuery annotatedWith(Class<? extends Annotation> annotation) {
        this.annotation = annotation;
        return this;
    }

    public TypeQuery exclude(String regexp) {
        excludeRegexps.add(regexp);
        return this;
    }

    public TypeQuery include(String regexp) {
        includeRegexps.add(regexp);
        return this;
    }

}
