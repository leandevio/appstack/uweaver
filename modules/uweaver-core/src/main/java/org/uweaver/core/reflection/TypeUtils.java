/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.reflection;

import org.joda.time.DateTime;
import org.uweaver.core.exception.ApplicationException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TypeUtils obj = new TypeUtils();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TypeUtils {

    private static final Map<Class, Class> wrappers = new HashMap<>();
    private static final Class[] SCALARTYPES = {Byte.class, Short.class, Integer.class, Long.class, Float.class,
            Double.class, Boolean.class, Character.class,
            BigInteger.class, BigDecimal.class,
            Date.class,
            String.class, StringBuilder.class, StringBuffer.class,
            UUID.class};
    static {
        wrappers.put(byte.class, Byte.class);
        wrappers.put(short.class, Short.class);
        wrappers.put(int.class, Integer.class);
        wrappers.put(long.class, Long.class);
        wrappers.put(float.class, Float.class);
        wrappers.put(double.class, Double.class);
        wrappers.put(char.class, Character.class);
        wrappers.put(boolean.class, Boolean.class);
    }

    public static Class<?> getRawType(Type type) {
        if(type instanceof Class) return (Class<?>) type;
        if(type instanceof ParameterizedType) return (Class<?>) ((ParameterizedType) type).getRawType();
        return null;
    }

    public static Type[] getTypeArguments(Type type) {
        Type[] paraTypes;
        if(type instanceof ParameterizedType) {
            paraTypes = ((ParameterizedType) type).getActualTypeArguments();
        } else {
            paraTypes = null;
        }
        return paraTypes;
    }

    public static Object newInstance(Class rawType) {
        Object obj = null;
        try {
            obj = rawType.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }

        return obj;
    }

    public static Object newEnum(Class rawType, String name) {
        Object obj = null;
        Method method = null;
        try {
            method = rawType.getMethod("valueOf", String.class);
            obj = method.invoke(rawType, name);
        } catch (NoSuchMethodException e) {
            throw new ApplicationException(e);
        } catch (InvocationTargetException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
        return obj;
    }

    public static boolean isGetter(Method method){
        if(!method.getName().startsWith("get"))      return false;
        if(method.getParameterTypes().length != 0)   return false;
        if(void.class.equals(method.getReturnType())) return false;
        return true;
    }

    public static boolean isSetter(Method method){
        if(!method.getName().startsWith("set")) return false;
        if(method.getParameterTypes().length != 1) return false;
        return true;
    }

    public static boolean isAssignable(Class type, Class toType) {
        if(toType.isAssignableFrom(type)) return true;
        if(wrappers.containsKey(type)&&toType.isAssignableFrom(wrappers.get(type))) return true;
        if(wrappers.containsValue(type)) {
            Class primitive = null;
            for(Map.Entry<Class, Class> entry : wrappers.entrySet()) {
                if(entry.getValue().equals(type)) {
                    primitive = entry.getKey();
                }
            }
            if(primitive!=null&&primitive.equals(toType)) return true;
        }

        return false;
    }

    public static Class wrapper(Class primitive) {
        return wrappers.get(primitive);
    }

    public static boolean isScalarType(Class type) {
        boolean result = false;

        if(type.isPrimitive()) result = true;
        else {
            for(int i=0; i<SCALARTYPES.length; i++) {
                if(SCALARTYPES[i].isAssignableFrom(type)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
}
