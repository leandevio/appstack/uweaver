/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2014 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.util.Environment;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The class implements ... The implementation provides ...
 *
 * The following describes how to escape the special characters \, ', { and }:
 *
 * - escape ' with another ' => ''
 * - escape \ with another \ => \\
 * - enclose } with ' => '}'
 * - enclose { with ' => '{'
 *
 * Usage:
 *
 * ```java
 * Locale en_US = new Locale("en", "US");
 * MessageResource messageResource = new MessageResource(en_US);
 * String welcome = messageResource.getMessage("WELCOME", "Jason Lin", "Flora Management System");
 * assertEquals(welcome, WELCOME);
 *
 * Locale zh_TW = new Locale("zh", "TW");
 * messageResource = new MessageResource(zh_TW);
 * String welcome_zh_TW = messageResource.getMessage("WELCOME", "林晉陞", "花坊管理系統");
 * assertEquals(welcome_zh_TW, WELCOME_zh_TW);
 * ```
 *
 * @author   Jason Lin
 * @since    1.0
 */
public class MessageResource  implements Serializable {
    private static final long serialVersionUID = 3722398823792758093L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageResource.class);
    private static final String DEFAULTCATALOG = "message";

    private Environment environment = Environment.getDefaultInstance();

    protected String catalog;
    protected Locale locale;
    protected String encoding;

    public MessageResource(String catalog, Locale locale, String encoding) {
        init(catalog, locale, encoding);
    }

    public MessageResource(String catalog, Locale locale) {

        init(catalog, locale, null);
    }

    public MessageResource(String catalog) {
        init(catalog, null, null);
    }

    public MessageResource(Locale locale) {
        init(null, locale, null);
    }

    public MessageResource() {
        init(null, null, null);
    }

    private void init(String catalog, Locale locale, String encoding) {
        setCatalog((catalog!=null) ? catalog : DEFAULTCATALOG);
        //setLocale((locale!=null) ? locale : environment.getLocale());
        setLocale(locale);
        setEncoding((encoding!=null) ? encoding : environment.property("file.encoding", "UTF-8"));
    }

    public String getMessage(String key, Locale locale, Object... args) {
        ResourceBundle resourceBundle = getResourceBundle(locale);
        String message = null;
        try {
            message = resourceBundle.getString(key);
        } catch (Exception ex) {
            LOGGER.debug("" + ex);
            return key;
        }
        MessageFormat formatter = new MessageFormat(message);
        formatter.setLocale(locale);
        return formatter.format(args);
    }

    public String getMessage(String key, Object... args) {
        return getMessage(key, locale, args);

    }

    public String getMessage(String key) {
        return getMessage(key, locale);
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    protected ResourceBundle getResourceBundle(Locale locale) {
        ResourceBundle resourceBundle = null;

        try {
            if(locale!=null) {
                resourceBundle = ResourceBundle.getBundle(catalog, locale, new ResourceEncodingControl(encoding));
            } else {
                resourceBundle = ResourceBundle.getBundle(catalog, new ResourceEncodingControl(encoding));
            }
        } catch (java.util.MissingResourceException e) {
            LOGGER.debug("Exception:", e);
        }

        return resourceBundle;
    }

}
