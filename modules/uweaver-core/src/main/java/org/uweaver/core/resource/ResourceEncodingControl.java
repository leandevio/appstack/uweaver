package org.uweaver.core.resource;

import org.uweaver.core.util.Environment;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * Created by jasonlin on 6/4/14.
 */
public class ResourceEncodingControl extends ResourceBundle.Control {
    private String encoding;

    public ResourceEncodingControl() {
        init(null);
    }

    public ResourceEncodingControl(String encoding) {
        init(encoding);
    }

    private void init(String encoding) {
        this.encoding = (encoding!=null) ? encoding : Environment.getDefaultInstance().property("file.encoding", "UTF-8");
    }

    public ResourceBundle newBundle
            (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException
    {
        // The below is a copy of the default implementation.
        String bundleName = toBundleName(baseName, locale);
        String resourceName = toResourceName(bundleName, "properties");
        ResourceBundle bundle = null;
        InputStream stream = null;
        if (reload) {
            URL url = loader.getResource(resourceName);
            if (url != null) {
                URLConnection connection = url.openConnection();
                if (connection != null) {
                    connection.setUseCaches(false);
                    stream = connection.getInputStream();
                }
            }
        } else {
            stream = loader.getResourceAsStream(resourceName);
        }
        if (stream != null) {
            try {
                // Only this line is changed to make it to read properties files as UTF-8.
                bundle = new PropertyResourceBundle(new InputStreamReader(stream, encoding));
            } finally {
                stream.close();
            }
        }
        return bundle;
    }
}
