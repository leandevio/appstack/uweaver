package org.uweaver.core.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.util.Environment;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by jasonlin on 6/3/14.
 */
public class TermResource extends MessageResource {

    private static final long serialVersionUID = -6258312965856001604L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TermResource.class);
    private static final String DEFAULTCATALOG = "term";
    private Environment environment = Environment.getDefaultInstance();

    public TermResource(String catalog, Locale locale, String encoding) {
        init(catalog, locale, encoding);
    }

    public TermResource(String catalog, Locale locale) {

        init(catalog, locale, null);
    }

    public TermResource(String catalog) {
        init(catalog, null, null);
    }

    public TermResource(Locale locale) {
        init(null, locale, null);
    }

    public TermResource() {
        init(null, null, null);
    }

    private void init(String catalog, Locale locale, String encoding) {
        setCatalog((catalog!=null) ? catalog : DEFAULTCATALOG);
        setLocale((locale!=null) ? locale : environment.locale());
        setEncoding((encoding!=null) ? encoding : environment.property("file.encoding", "UTF-8"));
    }

    public String getTerm(String key, Locale locale) {
        ResourceBundle resourceBundle = getResourceBundle(locale);
        String term = null;
        try {
            String effectiveKey = key.toUpperCase().replaceAll(" ", "_");
            term = resourceBundle.getString(effectiveKey);
        } catch (Exception ex) {
            LOGGER.info("" + ex);
            term = key;
        }
        return term;
    }

    public String getTerm(String key) {
        return getTerm(key, locale);
    }

    @Override
    public String getMessage(String key, Locale locale, Object... args) {
        return getTerm(key, locale);
    }

    @Override
    public String getMessage(String key, Object... args) {
        return getTerm(key);
    }

    @Override
    public String getMessage(String key) {
        return getTerm(key);
    }

}
