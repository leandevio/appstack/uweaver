/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * CronTrigger obj = new CronTrigger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class CronTrigger implements Trigger{
    private Job job;
    private String name = null;
    private Date startTime = new Date();
    private Date endTime = null;
    private int priority = 0;
    private Map<String, Object> data = Collections.synchronizedMap(new HashMap<String, Object>());
    private Long delay = null;
    private Date lastFireTime = null;

    private Schedule schedule;

    public CronTrigger(String name, Job job, Schedule schedule) {
        this.name = name;
        this.job = job;
        this.schedule = schedule;
    }

    public CronTrigger(Job job, Schedule schedule) {
        this.job = job;
        this.schedule = schedule;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Job getJob() {
        return job;
    }

    @Override
    public Date getStartTime() {
        return startTime;
    }

    @Override
    public Date getEndTime() {
        return endTime;
    }

    @Override
    public Long getDelay() {
        return (delay==null) ? 0 : delay;
    }

    @Override
    public void setDelay(Long delay) {

    }

    @Override
    public Integer getPriority() {
        return priority;
    }

    @Override
    public void setPriority(Integer priority) {

    }

    @Override
    public Date lastFireTime() {
        return null;
    }

    @Override
    public Date nextFireTime() {
        return null;
    }

    @Override
    public boolean canFire(Date time) {
        return false;
    }

    @Override
    public boolean mayFireAgain() {
        return false;
    }

    @Override
    public void fire() {
        this.lastFireTime = new Date();
    }

    @Override
    public Map<String, Object> getData() {
        return data;
    }
}
