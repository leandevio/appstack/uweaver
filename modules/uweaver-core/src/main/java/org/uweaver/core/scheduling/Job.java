/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Job obj = new Job();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Job {
    private static Logger LOGGER = LoggerFactory.getLogger(Job.class);

    private String name;
    private Class<? extends Task> task;

    private Map<String, Object> properties = Collections.synchronizedMap(new HashMap<String, Object>());
    private Long throttle = null;


    public Job(String name, Class<? extends Task> task) {
        this.name = name;
        this.task = task;
    }

    public Job(Class<? extends Task> task) {
        this.name = task.getName();
        this.task = task;
    }

    public Class<? extends Task> getTask() {
        return task;
    }

    public String getName() {
        return name;
    }

    public Long getThrottle() {
        return this.throttle;
    }

    public void setThrottle(Long throttle) {
        this.throttle = throttle;
    }

    public void setThrottle(Long throttle, TimeUnit unit) {
        setThrottle(TimeConverter.toSeconds(throttle, unit));
    }

    public Object getProperty(String name) {
        return properties.get(name);
    }

    public void setProperty(String name, Object value) {
        properties.put(name, value);
    }
}
