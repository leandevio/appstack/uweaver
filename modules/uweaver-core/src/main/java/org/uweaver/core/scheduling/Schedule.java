/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import org.uweaver.core.util.Environment;

import java.util.TimeZone;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Trigger obj = new Trigger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Schedule {
    private static Environment environment = Environment.getDefaultInstance();

    private int[] years = null;
    private DayOfWeek[] dayOfWeeks = null;
    private Month[] months = null;
    private int[] dayOfMonths = null;
    private int[] hours = null;
    private int[] minutes = null;
    private TimeZone timeZone;

    public Schedule(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Schedule() {
        this.timeZone = environment.timezone();
    }

    public Schedule clone() {
        Schedule schedule = new Schedule(timeZone);
        if(years!=null) schedule.setYeas(years.clone());
        if(dayOfWeeks!=null) schedule.setDayOfWeeks(dayOfWeeks.clone());
        if(months!=null) schedule.setMonths(months.clone());
        if(dayOfMonths!=null) schedule.setDayOfMonths(dayOfMonths.clone());
        if(hours!=null) schedule.setHours(hours.clone());
        if(minutes!=null) schedule.setMinutes(minutes.clone());
        return schedule;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public int[] getYears() {
        return years;
    }

    public void setYeas(int... years) {
        this.years = years;
    }

    public DayOfWeek[] getDayOfWeeks() {
        return dayOfWeeks;
    }

    public void setDayOfWeeks(DayOfWeek... dayOfWeeks) {
        this.dayOfWeeks = dayOfWeeks;
    }

    public Month[] getMonths() {
        return months;
    }

    public void setMonths(Month... months) {
        this.months = months;
    }

    public int[] getDayOfMonths() {
        return dayOfMonths;
    }

    public void setDayOfMonths(int... dayOfMonths) {
        this.dayOfMonths = dayOfMonths;
    }

    public int[] getHours() {
        return hours;
    }

    public void setHours(int... hours) {
        this.hours = hours;
    }

    public int[] getMinutes() {
        return minutes;
    }

    public void setMinutes(int... minutes) {
        this.minutes = minutes;
    }

    public enum DayOfWeek {
        SUN(0), MON(1), TUE(2), WED(3), THU(4), FRI(5), SAT(6);
        private final int value;

        DayOfWeek(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public String toString() {
            return this.name();
        }
    }

    public enum Month {
        JAN(1), FEB(2), MAR(3), APR(4), MAY(5), JUN(6), JUL(7), AUG(8), SEP(9), OCT(10), NOV(11), DEC(12);
        private final int value;

        Month(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public String toString() {
            return this.name();
        }
    }
}
