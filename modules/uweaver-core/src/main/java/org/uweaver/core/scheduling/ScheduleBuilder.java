/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import java.util.TimeZone;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ScheduleBuilder obj = new ScheduleBuilder();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ScheduleBuilder {
    private Schedule schedule = new Schedule();

    public ScheduleBuilder betweenDayOfWeek(Schedule.DayOfWeek start, Schedule.DayOfWeek end) {
        Schedule.DayOfWeek[] dayOfWeeks = new Schedule.DayOfWeek[end.getValue()-start.getValue()+1];
        for(int i=0; i<dayOfWeeks.length; i++) {
            dayOfWeeks[i] = Schedule.DayOfWeek.values()[start.getValue()+i];
        }
        schedule.setDayOfWeeks(dayOfWeeks);
        return this;
    }

    public ScheduleBuilder betweenHours(int start, int end) {
        int[] hours = new int[end-start+1];
        for(int i=0; i<hours.length; i++) {
            hours[i] = start + i;
        }
        schedule.setHours(hours);
        return this;
    }

    public ScheduleBuilder betweenMinutes(int start, int end) {
        int[] minutes = new int[end-start+1];
        for(int i=0; i<minutes.length; i++) {
            minutes[i] = start + i;
        }
        schedule.setMinutes(minutes);
        return this;
    }

    public ScheduleBuilder inTimeZone(TimeZone timeZone) {
        schedule.setTimeZone(timeZone);
        return this;
    }

    public Schedule build() {
        return schedule.clone();
    }

}
