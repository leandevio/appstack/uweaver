/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ScheduledJob obj = new ScheduledJob();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ScheduledJob {
    private static Logger LOGGER = LoggerFactory.getLogger(ScheduledJob.class);
    private Trigger trigger;
    private Future future;

    private Date startTime = null;
    private Date endTime = null;
    private StateMachine<State, Event> stateMachine;

    public ScheduledJob(Trigger trigger, Future future) {
        this.trigger = trigger;
        this.future = future;
        startTime = new Date();

        stateMachine = new StateMachine(isDelayed() ? State.WAITING : State.RUNNING);

        sync();
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public Job getJob() {
        return trigger.getJob();
    }

    public boolean isDone() {
        return (future.isDone());
    }

    public boolean isCancelled() {
        return (future.isCancelled());
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void start() {
        stateMachine.signal(Event.START);
    }

    public boolean cancel(boolean interrupt) {
        stateMachine.signal(Event.CANCEL);
        return future.cancel(interrupt);
    }

    public State getState() {
        return stateMachine.getState();
    }

    protected void sync() {
        if(endTime!=null) return;

        if(isDelayed() && getDelay(TimeUnit.SECONDS)<=0) {
            stateMachine.signal(Event.READY);
        }
        if(future.isDone() && endTime==null) {
            Event event = future.isCancelled()?Event.CANCELED:Event.COMPLETED;
            endTime = new Date();
            stateMachine.signal(event);
        }
    }

    private Long getDelay(TimeUnit unit) {
        return (isDelayed()) ? ((Delayed) future).getDelay(unit) : null;
    }

    private boolean isDelayed() {
        return Delayed.class.isAssignableFrom(future.getClass());
    }

    public enum State implements StateMachine.State {
        INITIALIZED("INITIALIZED") {
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.START==event) state = WAITING;
                else if(Event.READY==event) state = RUNNING;
                else state = null;

                return state;
            }
        },
        WAITING("WAITING") {
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.READY==event) state = RUNNING;
                else state = null;

                return state;
            }
        }, RUNNING("RUNNING") {
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.COMPLETED==event) state = COMPLETED;
                else if(Event.CANCEL==event) state = CANCELING;
                else if(Event.CANCELED==event) state = CANCELLED;
                else if(Event.FAILED==event) state = FAILED;
                else state = null;

                return state;
            }
        }, COMPLETED("COMPLETED") {
            public StateMachine.State next(StateMachine.Event event) {
                return null;
            }
        }, CANCELING("CANCELING") {
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.CANCELED==event) state = CANCELLED;
                else if(Event.COMPLETED==event) state = COMPLETED;
                else state = null;

                return state;
            }
        }, CANCELLED("CANCELLED") {
            public StateMachine.State next(StateMachine.Event event) {
                return null;
            }
        }, FAILED("FAILED") {
            public StateMachine.State next(StateMachine.Event event) {
                return null;
            }
        };

        private final String value;

        State(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String toString() {
            return value;
        }

    }

    public enum Event implements StateMachine.Event {
        START, READY, COMPLETED, CANCEL, CANCELED, FAILED
    }
}