/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.util.Environment;

import java.util.*;
import java.util.concurrent.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Scheduler obj = new Scheduler();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Scheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(Scheduler.class);

    private static final int POOLMAX = 20;
    private ScheduledExecutorService timerExecutorService;
    private ScheduledExecutorService jobExecutorService;
    private Set<Job> jobs = Collections.synchronizedSet(new HashSet<Job>());
    private List<Trigger> triggers = Collections.synchronizedList(new ArrayList<Trigger>());
    private Map<Job, Trigger> throttledTriggers = Collections.synchronizedMap(new HashMap<Job, Trigger>());
    private List<ScheduledJob> scheduledJobs = Collections.synchronizedList(new ArrayList<ScheduledJob>());
    private long tickTockInterval = 1;
    private long gcInterval = 10;
    private StateMachine<State, Event> stateMachine = new StateMachine<>(State.SHUTDOWN);
    private long counter = 0;

    private static class SingletonHolder {
        private static final int POOLMAX = 20;
        private static final Environment environment = Environment.getDefaultInstance();
        private static final Scheduler INSTANCE = new Scheduler(environment.propertyAsInteger("scheduler.pool.max", POOLMAX));
    }

    public static Scheduler getDefaultInstance() {
        return Scheduler.SingletonHolder.INSTANCE;
    }

    public static Scheduler getInstance() {
        return new Scheduler(POOLMAX);
    }

    protected Scheduler(int poolsize) {
        timerExecutorService = Executors.newScheduledThreadPool(2);
        jobExecutorService = Executors.newScheduledThreadPool(poolsize);
    }

    public State getState() {
        return stateMachine.getState();
    }

    public Job getJob(String name) {
        Job result = null;

        synchronized (jobs) {
            for(Job job : jobs) {
                if(job.getName()!=null && job.getName().equals(name)) {
                    result = job;
                    break;
                }
            }
        }

        return result;
    }

    public synchronized void start() {
        stateMachine.signal(Event.START);
        timerExecutorService.scheduleWithFixedDelay(new TimerTask(this), 0, tickTockInterval, TimeUnit.SECONDS);
        stateMachine.signal(Event.READY);
        LOGGER.debug(String.format("Scheduler(%s) started.", toString()));
    }

    public synchronized void stop(long delay, TimeUnit unit) {
        boolean isDone = false;
        long delayInSeconds = TimeConverter.toSeconds(delay, unit);

        stateMachine.signal(Event.STOP);

        while(!isDone) {
            isDone = true;
            synchronized (scheduledJobs) {
                for(ScheduledJob scheduledJob : scheduledJobs) {
                    if(!scheduledJob.isDone()) {
                        isDone = false;
                        break;
                    }
                }
            }
            if(!isDone && delayInSeconds>0) try {
                LOGGER.debug(String.format("Shutdown deferred for %d seconds.", delayInSeconds));
                Thread.sleep(1000 * tickTockInterval);
                delayInSeconds = delayInSeconds - tickTockInterval;
            } catch (InterruptedException e) {
                throw new ApplicationException(e);
            }
        }

        timerExecutorService.shutdown();
        jobExecutorService.shutdown();

        stateMachine.signal(Event.STOPPED);

        if(isDone) {
            LOGGER.debug(String.format("Scheduler(%s) shutdown gracefully.", toString()));
        } else {
            LOGGER.debug(String.format("Scheduler(%s) shutdown.", toString()));
        }

    }

    public synchronized void stopNow() {
        stop(1, TimeUnit.SECONDS);
    }

    public boolean isShutdown() {
        return (stateMachine.getState()==State.SHUTDOWN);
    }


    public void setTickTockInterval(long interval) {
        this.tickTockInterval = interval;
    }

    public long getTickTockInterval() {
        return tickTockInterval;
    }

//    public Job schedule(Runnable task, long delay) {
//        if(isThrottled(task)) {
//            LOGGER.info("{} is throttled.", task.toString());
//            return null;
//        }
//        Job job = new Job(task);
//        Date now = new Date();
//        jobs.add(job);
//        ScheduledFuture future = scheduledExecutorService.schedule(task, delay, TimeUnit.SECONDS);
//        job.setFuture(future);
//        job.setStartTime(new Date(now.getTime() + delay * 1000));
//        return job;
//    }

//    public Job schedule(Runnable task, long delay, TimeUnit unit) {
//        long delayInSeconds = TIMETABLE.get(unit) * delay;
//        return schedule(task, delayInSeconds);
//    }

//    public Job schedule(Runnable task) {
//        return schedule(task, 0, TimeUnit.SECONDS);
//    }

//    public Job schedule(Runnable task, Date startTime) {
//        return null;
//    }

    public void schedule(Job job) {
        SimpleTriggerBuilder builder = new SimpleTriggerBuilder(job);
        Trigger trigger = builder.startNow().build();
        schedule(trigger);
    }

    public void scheduleWithFixedDelay(Job job, long delay, long interval, TimeUnit unit) {
        SimpleTriggerBuilder builder = new SimpleTriggerBuilder(job);
        Trigger trigger = builder.delayFor(delay, unit).repeatWithInterval(interval, unit).startNow().build();
        schedule(trigger);
    }

    public void schedule(Trigger trigger) {
        jobs.add(trigger.getJob());
        triggers.add(trigger);
    }

    public void scheduleWithFixedDelay(Job job, long interval, TimeUnit unit) {
        scheduleWithFixedDelay(job, 0, interval, unit);
    }


//    public Job scheduleWithFixedDelay(Runnable task, long initialDelay, long delay) {
//        Job job = new Job(task);
//        Date now = new Date();
//        jobs.add(job);
//        ScheduledFuture future = scheduledExecutorService.scheduleWithFixedDelay(task, initialDelay, delay, TimeUnit.SECONDS);
//        job.setFuture(future);
//        job.setStartTime(new Date(now.getTime() + initialDelay * 1000));
//        return job;
//    }
//
//    public Job scheduleWithFixedDelay(Runnable task, long initialDelay, long delay, TimeUnit unit) {
//        long initialDelayDelayInSeconds = TIMETABLE.get(unit) * initialDelay;
//        long delayInSeconds = TIMETABLE.get(unit) * delay;
//        return scheduleWithFixedDelay(task, initialDelayDelayInSeconds, delayInSeconds);
//    }

//    public Job scheduleWithFixedDelay(Runnable task, long delay) {
//        return scheduleWithFixedDelay(task, 0, delay, TimeUnit.SECONDS);
//    }

//    public Job scheduleWithFixedDelay(Runnable task, Date startTime, long delay) {
//        return null;
//    }
//
//    public Job scheduleAtFixedRate(Runnable task, Date startTime, long period) {
//        return null;
//    }
//
//    public Job scheduleAtFixedRate(Runnable command, long delay, long period, TimeUnit unit) {
//        return null;
//    }
//
//    public Job scheduleAtFixedRate(Runnable task, long period) {
//        return scheduleAtFixedRate(task, 0, period, TimeUnit.SECONDS);
//    }
//
//    public Job schedule(Runnable task, Trigger trigger) {
//        return null;
//    }


    public boolean isThrottled(Job job) {
        if(job.getThrottle()==null) return false;

        boolean throttled = false;
        Date now = new Date();
        Date lastExecuteTime = null;

        synchronized(scheduledJobs) {
            for(ScheduledJob scheduledJob : scheduledJobs) {
                if(!scheduledJob.getJob().equals(job)) continue;

                if(scheduledJob.isDone()) {
                    Date endTime = scheduledJob.getEndTime();
                    if(lastExecuteTime==null) {
                        lastExecuteTime = endTime;
                    } else if(endTime.after(lastExecuteTime)) {
                        lastExecuteTime =  endTime;
                    }
                } else {
                    throttled = true;
                    break;
                }

            }
        }

        if(lastExecuteTime!=null) {
            Date throttledTime = new Date(lastExecuteTime.getTime() + (job.getThrottle() * 1000));
            if(now.before(throttledTime)) {
                throttled = true;
            }
        }

        return throttled;
    }
//
//    private List<Job> findJobs(Object target) {
//        return (target instanceof Class) ? findJobsByType((Class) target) : findJobsByTask((Runnable) target);
//    }

//    private List<Job> findJobsByType(Class type) {
//        List<Job> result = new ArrayList<>();
//
//        synchronized(jobs) {
//            for(Job job : jobs) {
//                if(type.isAssignableFrom(job.getTask().getClass())) {
//                    result.add(job);
//                }
//            }
//        }
//
//        return result;
//    }
//
//    private List<Job> findJobsByTask(Runnable task) {
//        List<Job> result = new ArrayList<>();
//
//        synchronized(jobs) {
//            for(Job job : jobs) {
//                if(job.getTask().equals(task)) {
//                    result.add(job);
//                }
//            }
//        }
//
//        return result;
//    }


    protected void track() {
        synchronized(scheduledJobs) {
            for(ScheduledJob scheduledJob : scheduledJobs) {
                scheduledJob.sync();
            }
        }
    }

    protected void trigger() {
        if(stateMachine.getState()!=State.RUNNING) return;

        Date now = new Date();

        List<Job> removed = new ArrayList<>();
        synchronized (throttledTriggers) {
            for(Trigger trigger : throttledTriggers.values()) {
                if(isThrottled(trigger.getJob())) {
                    continue;
                }
                execute(trigger);
                removed.add(trigger.getJob());
            }
        }
        for(Job job : removed) {
            throttledTriggers.remove(job);
        }

        synchronized (triggers) {
            for(Trigger trigger : triggers) {
                if(isRunning(trigger)) continue;
                if(!trigger.canFire(now)) continue;
                trigger.fire();
                LOGGER.debug(String.format("Trigger(%s) was fired.", trigger.toString()));
                if(isThrottled(trigger.getJob())) {
                    throttle(trigger);
                    LOGGER.debug(String.format("Trigger(%s) was throttled", trigger.toString()));
                    continue;
                }
                execute(trigger);
            }
        }
    }

    private boolean isRunning(Trigger trigger) {
        boolean running = false;
        synchronized (scheduledJobs) {
            for(ScheduledJob scheduledJob : scheduledJobs) {
                if(scheduledJob.getTrigger().equals(trigger) && !scheduledJob.isDone()) {
                    running = true;
                    break;
                }
            }
        }
        return running;
    }

    private ScheduledJob findLastestScheduledJob(Trigger trigger) {
        ScheduledJob lastScheduledJob = null;
        synchronized (scheduledJobs) {
            for(ScheduledJob scheduledJob : scheduledJobs) {
                if(scheduledJob.getTrigger().equals(trigger)) {
                    if(lastScheduledJob==null) {
                        lastScheduledJob = scheduledJob;
                    } else {
                        lastScheduledJob = latestScheduledJob(lastScheduledJob, scheduledJob);
                    }
                }
            }
        }
        return lastScheduledJob;
    }


    private ScheduledJob latestScheduledJob(ScheduledJob one, ScheduledJob another) {
        ScheduledJob latest;
        if(one.getEndTime()==null) {
            if(another.getEndTime()==null) {
                latest = (one.getStartTime().after(another.getStartTime())) ? one : another;
            } else {
                latest = one;
            }
        } else {
            if(another.getEndTime()==null) {
                latest = another;
            } else {
                latest = (one.getEndTime().after(another.getEndTime())) ? one : another;
            }
        }
        return latest;
    }

    protected void info() {
        if(!LOGGER.isInfoEnabled()) return;

        int running = 0;

        LOGGER.info(String.format("State: %s", stateMachine.getState().toString()));

        synchronized(scheduledJobs) {
            for(ScheduledJob scheduledJob : scheduledJobs) {
                LOGGER.debug(String.format("Job(%s) of the trigger(%s): %s.", scheduledJob.getJob(), scheduledJob.getTrigger(), scheduledJob.getState()));
                if(!scheduledJob.isDone()) running++;
            }
        }
        LOGGER.info(String.format("# of jobs running: %d", running));
        LOGGER.info(String.format("# of jobs waiting: %d", throttledTriggers.size()));
        LOGGER.info(String.format("# of triggers: %d", triggers.size()));

        if(!LOGGER.isDebugEnabled()) return;

        LOGGER.debug("Job List:");
        synchronized (jobs) {
            for(Job job : jobs) {
                LOGGER.debug(String.format("- %s.", job));
            }
        }

        LOGGER.debug("Trigger List:");
        synchronized (triggers) {
            for(Trigger trigger : triggers) {
                LOGGER.debug(String.format("- %s.", trigger));
            }
        }

        LOGGER.debug("Throttled Queue:");
        synchronized (throttledTriggers) {
            for(Trigger trigger : throttledTriggers.values()) {
                LOGGER.debug(String.format("- %s.", trigger));
            }
        }
    }

    private void execute(Trigger trigger) {
        ExecutionContext executionContext = new ExecutionContext(this, trigger);
        Class<? extends Task> task = trigger.getJob().getTask();
        TaskRunner scheduledTask;
        try {
            scheduledTask = new TaskRunner(task.newInstance(), executionContext);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
        Future future = jobExecutorService.submit(scheduledTask);

        ScheduledJob scheduledJob = new ScheduledJob(trigger, future);
        scheduledJob.start();

        scheduledJobs.add(scheduledJob);
    }

    private void log(Exception e) {
        LOGGER.debug("Exception: ", e);
    }

    private void throttle(Trigger trigger) {
        if(throttledTriggers.containsKey(trigger.getJob())) return;
        throttledTriggers.put(trigger.getJob(), trigger);
    }

    protected void gc() {
        List<Trigger> recyclingTriggers = new ArrayList<>();
//        List<Job> recyclingJobs = new ArrayList<>();

        synchronized(triggers) {
            for(Trigger trigger : triggers) {
                if(trigger.mayFireAgain()||throttledTriggers.containsValue(trigger)) continue;
                recyclingTriggers.add(trigger);
//                recyclingJobs.add(trigger.getJob());
            }
        }
        triggers.removeAll(recyclingTriggers);

//        synchronized(triggers) {
//            for(Trigger trigger : triggers) {
//                recyclingJobs.remove(trigger.getJob());
//            }
//        }
//
//        jobs.removeAll(recyclingJobs);

        List<ScheduledJob> recyclingScheduledJobs = new ArrayList<>();

        synchronized (scheduledJobs) {
            for(ScheduledJob scheduledJob : scheduledJobs) {
                if(scheduledJob.isDone() && !jobs.contains(scheduledJob.getJob())) {
                    recyclingScheduledJobs.add(scheduledJob);
                }
            }
        }

        scheduledJobs.removeAll(recyclingScheduledJobs);

    }

    private class TimerTask implements Runnable {
        private Scheduler scheduler;

        TimerTask(Scheduler scheduler) {
            this.scheduler = scheduler;
        }

        @Override
        public void run() {
            try {
                scheduler.track();
                scheduler.trigger();
                scheduler.gc();
                if(counter%60==0) {
                    scheduler.info();
                    counter = 0;
                }
                counter++;
            } catch(Exception e) {
                scheduler.log(e);
            }
        }
    }

    private class TaskRunner implements Runnable {
        private Task task;
        private ExecutionContext executionContext;

        TaskRunner(Task task, ExecutionContext executionContext) {
            this.task = task;
            this.executionContext = executionContext;
        }

        @Override
        public void run() {
            task.execute(executionContext);
        }
    }


    public enum State implements StateMachine.State {
        SHUTDOWN("SHUTDOWN") {
            @Override
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.START==event) state = SUSPENDING;
                else if(Event.READY==event) state = RUNNING;
                else state = null;

                return state;
            }

        }, RUNNING("RUNNING") {
            @Override
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.STOP==event) state = SUSPENDING;
                else if(Event.STOPPED==event) state = SHUTDOWN;
                else if(Event.SUSPEND==event) state = SUSPENDING;
                else if(Event.SUSPENDED==event) state = STANDBY;
                else state = null;

                return state;
            }
        }, STANDBY("STANDBY") {
            @Override
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if(Event.RESUME==event) state = SUSPENDING;
                else if(Event.RESUMED==event) state = RUNNING;
                else if(Event.STOP==event) state = SUSPENDING;
                else if(Event.STOPPED==event) state = SHUTDOWN;
                else state = null;

                return state;
            }
        }, SUSPENDING("SUSPENDING") {
            @Override
            public StateMachine.State next(StateMachine.Event event) {
                State state;

                if (Event.READY == event) state = RUNNING;
                else if (Event.STOPPED == event) state = SHUTDOWN;
                else state = null;

                return state;
            }
        };

        private final String value;

        State(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String toString() {
            return value;
        }


    }


    public enum Event implements StateMachine.Event {
        START, READY, STOP, STOPPED, SUSPEND, SUSPENDED, RESUME, RESUMED
    }
}
