/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import org.uweaver.core.util.ObjectUtils;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SimpleTrigger obj = new SimpleTrigger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SimpleTrigger implements Trigger {
    private Job job;
    private String name = null;
    private Date startTime = null;
    private Date endTime = null;
    private Integer priority = null;
    private Map<String, Object> data = Collections.synchronizedMap(new HashMap<String, Object>());
    private Long delay = null;
    private Long interval = null;
    private Date lastFireTime = null;

    public SimpleTrigger(String name, Job job) {
        this.name = name;
        this.job = job;
    }

    public SimpleTrigger(Job job) {
        this.job = job;
    }

    public SimpleTrigger clone() {
        SimpleTrigger simpleTrigger = new SimpleTrigger(name, job);
        simpleTrigger.setStartTime(ObjectUtils.clone(startTime));
        simpleTrigger.setEndTime(ObjectUtils.clone(endTime));
        simpleTrigger.setPriority(ObjectUtils.clone(priority));
        simpleTrigger.setDelay(ObjectUtils.clone(delay));
        simpleTrigger.setInterval(ObjectUtils.clone(interval));
        return simpleTrigger;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Job getJob() {
        return job;
    }

    @Override
    public Date getStartTime() {
        return ObjectUtils.clone(startTime);
    }

    @Override
    public Date getEndTime() {
        return ObjectUtils.clone(endTime);
    }

    @Override
    public Long getDelay() {
        return (delay==null) ? 0 : ObjectUtils.clone(delay);
    }

    @Override
    public Date lastFireTime() {
        return ObjectUtils.clone(lastFireTime());
    }

    @Override
    public Date nextFireTime() {
        if(startTime==null) return null;

        Date nextFireTime;
        Date now = new Date();

        if(endTime!=null && now.after(endTime)) return null;

        if(lastFireTime==null) {
            nextFireTime = new Date(startTime.getTime() + getDelay() * 1000);
        } else if(interval!=null){
            nextFireTime = new Date(lastFireTime.getTime() + interval * 1000);
        } else {
            nextFireTime = null;
        }

        if(endTime!=null && nextFireTime!=null && nextFireTime.after(endTime)) {
            nextFireTime = null;
        }

        return ObjectUtils.clone(nextFireTime);
    }

    @Override
    public boolean canFire(Date time) {
        Date fireTime = nextFireTime();

        if(fireTime==null) return false;

        return !time.before(fireTime);
    }

    @Override
    public boolean mayFireAgain() {
        return (nextFireTime()!=null);
    }

    @Override
    public synchronized void fire() {
        this.lastFireTime = new Date();
    }

    @Override
    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public Integer getPriority() {
        return (priority==null) ? 0 : ObjectUtils.clone(priority);
    }


    public synchronized void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public synchronized void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public synchronized void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public synchronized void setDelay(Long delay) {
        this.delay = delay;
    }


    public synchronized void setInterval(Long interval) {
        this.interval = interval;
    }


}
