/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SimpleTriggerBuilder obj = new SimpleTriggerBuilder();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SimpleTriggerBuilder {
    private SimpleTrigger simpleTrigger;

    public SimpleTriggerBuilder(String name, Job job) {
        simpleTrigger = new SimpleTrigger(name, job);
    }

    public SimpleTriggerBuilder(Job job) {
        simpleTrigger = new SimpleTrigger(job);
    }

    public SimpleTriggerBuilder startNow() {
        simpleTrigger.setStartTime(new Date());
        return this;
    }

    public SimpleTriggerBuilder starAt(Date starTime) {
        simpleTrigger.setStartTime(starTime);
        return this;
    }

    public SimpleTriggerBuilder endAt(Date endTime) {
        simpleTrigger.setEndTime(endTime);
        return this;
    }

    public SimpleTriggerBuilder delayFor(long delay) {
        simpleTrigger.setDelay(delay);
        return this;
    }

    public SimpleTriggerBuilder delayFor(long delay, TimeUnit unit) {
        return delayFor(TimeConverter.toSeconds(delay, unit));
    }

    public SimpleTriggerBuilder repeatWithInterval(long interval) {
        simpleTrigger.setInterval(interval);
        return this;
    }

    public SimpleTriggerBuilder repeatWithInterval(long interval, TimeUnit unit) {
        return repeatWithInterval(TimeConverter.toSeconds(interval, unit));
    }

    public SimpleTrigger build() {
        return simpleTrigger.clone();
    }
}
