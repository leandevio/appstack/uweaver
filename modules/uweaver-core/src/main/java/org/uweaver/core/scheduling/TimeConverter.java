/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.scheduling;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TimeConverter obj = new TimeConverter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TimeConverter {

    private static final Map<TimeUnit, Long> TIMETABLE = new HashMap<>();

    static {
        TIMETABLE.put(TimeUnit.SECONDS, Long.valueOf(1));
        TIMETABLE.put(TimeUnit.MINUTES, Long.valueOf(60));
        TIMETABLE.put(TimeUnit.HOURS, Long.valueOf(60*60));
        TIMETABLE.put(TimeUnit.DAYS, Long.valueOf(60*60*24));
    }

    public static Long toSeconds(Long time, TimeUnit unit) {
        if(time==null) return null;

        return  time * TIMETABLE.get(unit);
    }
}
