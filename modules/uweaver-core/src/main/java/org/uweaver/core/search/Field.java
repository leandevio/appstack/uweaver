/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.search;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Field obj = new Field();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Field {
    /** 名稱 */
    private String name;

    /** 型別 */
    private Class type;


    public Field() {}

    public Field(String name) {
        this.name = name;
    }

    /**
     * Getter: {@link #name}.
     * @return {@link #name}.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter: {@link #name}.
     * @param name {@link #name}.
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter: {@link #name}.
     * @return {@link #name}.
     */
    public Class getType() {
        return type;
    }

    /**
     * Setter: {@link #type}.
     * @param type {@link #type}.
     */
    public void setType(Class type) {
        this.type = type;
    }


}
