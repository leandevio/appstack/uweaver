/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.search;

import org.uweaver.core.util.XMap;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * IndexCollection obj = new IndexCollection();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class IndexCollection {
    private String name;
    private Date updateTime;
    private List<Field> key = new ArrayList<>();
    private XMap<String, Field> fields = new XMap<>();

    public IndexCollection(String name, List<Field> fields, List<Field> key) {
        this.name = name;
        setFields(fields);
        setKey(key);
    }

    public IndexCollection(String name, List<Field> fields) {
        this.name = name;
        setFields(fields);
    }

    public IndexCollection(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setUpdateTime(Date time) {
        this.updateTime = time;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void addField(Field field) {
        this.fields.put(field.getName(), field);
    }

    public Field getField(String name) {
        return this.fields.get(name);
    }

    public void removeField(String name) {
        this.fields.remove(name);
    }

    public List<Field> getFields() {
        List<Field> fields = new ArrayList<Field>();
        fields.addAll(this.fields.values());
        return java.util.Collections.unmodifiableList(fields);
    }

    public void setFields(List<Field> fields) {
        this.fields.clear();
        for(Field field : fields) {
            addField(field);
        }
    }

    public boolean hasField(String name) {
        return this.fields.containsKey(name);
    }

    public void setKey(String[] fields) {
        this.key.clear();
        for(String name: fields) {
            Field field = getField(name);
            if(field!=null) this.key.add(field);
        }
    }

    public void setKey(List<Field> fields) {
        this.key.clear();
        for(Field field: fields) {
            Field f = getField(field.getName());
            if(f!=null) this.key.add(f);
        }
    }

    public List<Field> getKey() {
        return java.util.Collections.unmodifiableList(this.key);
    }

    public boolean isKey(String name) {
        return this.key.contains(this.fields.get(name));
    }

}
