/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.search;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * IndexQuery obj = new IndexQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class IndexQuery {
    private static final String SEPARATOR = ":";
    private static final String QUOTE = "\"";
    private static final String AND = "AND";
    private static final String OR = "OR";
    private static final String SPACE = " ";
    private static final String WILDCARD = "*";
    private static final String COLLECTIONFIELD = "_collection";
    private CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
    private Pattern pattern = Pattern.compile("[ \\+\\-\\!(){}\\[\\]\\^\\\"\\~\\*\\?:\\\\]");

    private String collection = null;
    private String predicate = "";


    public IndexQuery from(String name) {
        this.collection = name;
        return this;
    }

    public IndexQuery where(Map<String, Object> predicate) {
        StringBuilder querystr = new StringBuilder();
        String conjunction = SPACE + OR + SPACE;
        for(Map.Entry<String, Object> entry : predicate.entrySet()) {
            Object value = toTerm(entry.getValue());
            if(value==null) continue;
            querystr.append(entry.getKey()).append(SEPARATOR).append(value).append(conjunction);
        }
        if(querystr.length()>conjunction.length()) querystr.delete(querystr.length()-conjunction.length(),  querystr.length());
        this.predicate = querystr.toString();
        return this;
    }

    public IndexQuery where(String querystr) {
        this.predicate = querystr;
        return this;
    }

    public String kql() {
        StringBuilder sb = new StringBuilder();
        String conjunction = SPACE + AND + SPACE;

        sb.append(COLLECTIONFIELD).append(SEPARATOR).append(QUOTE).append(this.collection).append(QUOTE);
        if(this.predicate.length()>0) sb.append(conjunction).append("(").append(this.predicate).append(")");

        return sb.toString();
    }

    private String toTerm(Object value) {
        if(value==null) return null;

        Class type = value.getClass();
        String term;

        if(value instanceof String) {
            term = (String) value;
        } else if(StringBuilder.class.isAssignableFrom(type)||StringBuffer.class.isAssignableFrom(type)) {
            term = value.toString();
        } else {
            return null;
        }

        if(containsSpecialChars(term)) return QUOTE + term + QUOTE;

        if(asciiEncoder.canEncode(term)) {
            return term + WILDCARD;
        }

        return QUOTE + term + QUOTE;
    }

    private boolean containsSpecialChars(String term) {
        return pattern.matcher(term).find();
    }



}
