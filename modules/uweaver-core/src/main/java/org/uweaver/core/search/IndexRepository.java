/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.XMap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * IndexRepository obj = new IndexRepository();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class IndexRepository {
    private static Logger LOGGER = LogManager.getLogger(org.uweaver.core.search.IndexSearcher.class);
    private static final String DATAFOLDER = "data";
    private static final String METAFOLDER = "meta";

    private static final String COLLECTIONFIELD = "_collection";
    private static final String NAMEFIELD = "name";
    private static final String LNAMEFIELD = "lname";
    private static final String FIELDSFIELD = "fields";
    private static final String KEYFIELD = "key";
    private static final String UPDATETIMEFIELD = "updatetime";

    private static final String COLLECTION = "collection";
    Environment environment = Environment.getDefaultInstance();
    Path data;
    Path meta;
    private XMap<String, IndexCollection> collections;

    private static final JSONParser parser = new JSONParser();

    private static class SingletonHolder {
        private static final IndexRepository INSTANCE = IndexRepository.getInstance();
    }

    public static IndexRepository getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static IndexRepository getInstance() {
        return new IndexRepository();
    }

    public IndexRepository() {
        Path path = environment.indexFolder();
        initialize(path);
    }

    public IndexRepository(Path path) {
        initialize(path);
    }

    private void initialize(Path path) {
        Path data = path.resolve(DATAFOLDER);
        Path meta = path.resolve(METAFOLDER);

        try {
            Files.createDirectories(data);
            Files.createDirectories(meta);
        } catch (IOException e) {
            throw new ApplicationException(e);
        }

        this.data = data;
        this.meta = meta;
        this.collections = new XMap<>();
    }

    protected Path getData() {
        return this.data;
    }
    protected Path getMeta() {
        return this.meta;
    }

    public void reset() {
        reset(data);
        reset(meta);
        collections.clear();
    }

    private void reset(Path path) {
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        try (Directory directory = FSDirectory.open(path);
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(directory, config)) {
            writer.deleteAll();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void scan() {
        Analyzer analyzer = new StandardAnalyzer();

        collections.clear();
        IndexQuery query = new IndexQuery();
        Map<String, Object> predicate = new HashMap<>();
        query.from(COLLECTION).where(predicate);

        try (Directory directory = FSDirectory.open(meta); IndexReader reader = DirectoryReader.open(directory)){
            int hitsPerPage = 1000;
            IndexSearcher searcher = new IndexSearcher(reader);
            TopDocs docs = searcher.search(toRawQuery(query, analyzer), hitsPerPage);
            ScoreDoc[] hits = docs.scoreDocs;

            for(int i=0; i<hits.length; i++) {
                org.apache.lucene.document.Document document = searcher.doc(hits[i].doc);
                IndexCollection collection = new IndexCollection(document.get(NAMEFIELD));
                IndexableField updateTime = document.getField(UPDATETIMEFIELD);
                if(updateTime!=null) collection.setUpdateTime(new Date(updateTime.numericValue().longValue()));
                collection.setFields(Arrays.asList(parser.readValue(document.get(FIELDSFIELD), Field[].class)));
                collection.setKey(Arrays.asList(parser.readValue(document.get(KEYFIELD), Field[].class)));

                collections.put(collection.getName(), collection);
            }

        } catch (IndexNotFoundException e) {

        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public List<IndexCollection> getCollections() {
        List<IndexCollection> list = new ArrayList<>();

        if(collections.size()==0) scan();
        list.addAll(collections.values());

        return list;
    }

    public IndexCollection getCollection(String name) {
        if(collections.size()==0) scan();
        IndexCollection indexCollection = collections.get(name);
        return indexCollection;
    }

    public void saveCollection(IndexCollection collection) {
        deleteCollection(collection);
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        try (Directory directory = FSDirectory.open(meta);
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(directory, config)) {
            Document document = new Document();
            document.put(LNAMEFIELD, collection.getName().toLowerCase());
            document.put(NAMEFIELD, collection.getName());
            document.put(UPDATETIMEFIELD, collection.getUpdateTime());
            document.put(FIELDSFIELD, parser.writeValueAsString(collection.getFields()));
            document.put(KEYFIELD, parser.writeValueAsString(collection.getKey()));
            writer.addDocument(toRawDocument(COLLECTION, document));
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        this.collections.put(collection.getName(), collection);
        LOGGER.debug(String.format("Create Index: %s", collection.getName()));
    }

    public void deleteCollection(IndexCollection collection) {
        deleteCollection(collection.getName());
    }

    public void deleteCollection(String name) {
        this.collections.remove(name);
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexQuery query = new IndexQuery();
        Map<String, Object> predicate = new HashMap<>();
        predicate.put(LNAMEFIELD, name.toLowerCase());
        query.from(COLLECTION).where(predicate);

        try (Directory directory = FSDirectory.open(meta);
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(directory, config)) {
            writer.deleteDocuments(toRawQuery(query, analyzer));
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }


    private Query toRawQuery(IndexQuery query, Analyzer analyzer) {
        Query rawQuery;
        try {
            rawQuery = new QueryParser("_NA", analyzer).parse(query.kql());
        } catch (ParseException e) {
            throw new org.uweaver.core.exception.InvalidFormatException(e);
        }
        return rawQuery;
    }

    private org.apache.lucene.document.Document toRawDocument(String collection, Map<String, Object> document) {
        org.apache.lucene.document.Document rawDocument = new org.apache.lucene.document.Document();
        rawDocument.add(new TextField(COLLECTIONFIELD, collection, org.apache.lucene.document.Field.Store.YES));
        for(Map.Entry<String, Object> entry : document.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            org.apache.lucene.document.Field field = null;

            if(value==null) continue;

            if (isText(value)) {
                field = new TextField(key, value.toString(), org.apache.lucene.document.Field.Store.YES);
            } else if (Long.class.isAssignableFrom(value.getClass())) {
                field = new LongField(key, (Long) value, org.apache.lucene.document.Field.Store.YES);
            } else if (Date.class.isAssignableFrom(value.getClass())) {
                value = ((Date) value).getTime();
                field = new LongField(key, (Long) value, org.apache.lucene.document.Field.Store.YES);
            }
            if(field!=null) rawDocument.add(field);
        }
        return rawDocument;
    }

    private boolean isText(Object value) {
        boolean result = false;
        if(value instanceof String) {
            result = true;
        }
        return result;
    }


}
