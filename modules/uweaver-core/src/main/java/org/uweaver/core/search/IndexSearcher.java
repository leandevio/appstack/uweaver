/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;

import java.io.IOException;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * IndexSearcher obj = new IndexSearcher();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class IndexSearcher {
    private static Logger LOGGER = LogManager.getLogger(IndexSearcher.class);
    private IndexRepository repository;

    public IndexSearcher(IndexRepository repository) {
        this.repository = repository;
    }

    public Hits search(IndexQuery query, int limit, int offset) {
        Hits hits;
        Analyzer analyzer = new StandardAnalyzer();

        LOGGER.debug(query.kql());
        try (Directory directory = FSDirectory.open(repository.getData())) {
            IndexReader reader = DirectoryReader.open(directory);
            org.apache.lucene.search.IndexSearcher searcher = new org.apache.lucene.search.IndexSearcher(reader);
            TopDocs topDocs = searcher.search(toRawQuery(query, analyzer), offset + limit);
            hits = toHits(topDocs, limit, offset);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        return hits;
    }


    public int count(IndexQuery query) {
        int n;
        Analyzer analyzer = new StandardAnalyzer();

        try (Directory directory = FSDirectory.open(repository.getData())) {
            IndexReader reader = DirectoryReader.open(directory);
            org.apache.lucene.search.IndexSearcher searcher = new org.apache.lucene.search.IndexSearcher(reader);
            n = searcher.count(toRawQuery(query, analyzer));
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        return n;
    }


    public Document get(int docId) {
        Document document = null;

        try (Directory directory = FSDirectory.open(repository.getData())) {
            IndexReader reader = DirectoryReader.open(directory);
            org.apache.lucene.search.IndexSearcher searcher = new org.apache.lucene.search.IndexSearcher(reader);
            org.apache.lucene.document.Document rawDocument = searcher.doc(docId);
            document = toDocument(rawDocument);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        return document;
    }


    private Hits toHits(TopDocs topDocs, int limit, int offset) {
        Hits hits = new Hits();

        hits.setTotal(topDocs.totalHits);
        hits.setMaxScore(topDocs.getMaxScore());

        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        for(int i=offset;i<Math.min(offset+limit, scoreDocs.length);++i) {
            ScoreDoc scoreDoc = scoreDocs[i];
            Hit hit = new Hit();
            hit.setScore(scoreDoc.score);
            hit.setDoc(scoreDoc.doc);
            hits.add(hit);
        }

        return hits;
    }


    private Query toRawQuery(IndexQuery query, Analyzer analyzer) {
        Query rawQuery;
        try {
            rawQuery = new QueryParser("_todo", analyzer).parse(query.kql());
        } catch (ParseException e) {
            throw new org.uweaver.core.exception.InvalidFormatException(e);
        }
        return rawQuery;
    }

    private Document toDocument(org.apache.lucene.document.Document rawDocument) {
        Document document = new Document();
        for(IndexableField field : rawDocument.getFields()) {
            document.put(field.name(), field.stringValue());
        }
        return document;
    }

}
