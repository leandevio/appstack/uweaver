/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * IndexWriter obj = new IndexWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class IndexWriter {
    private static final String COLLECTIONFIELD = "_collection";
    private IndexRepository repository;

    public IndexWriter(IndexRepository repository) {
        this.repository = repository;
    }

    public void remove(IndexQuery query) {
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        try (Directory directory = FSDirectory.open(repository.getData());
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(directory, config)) {
            writer.deleteDocuments(toRawQuery(query, analyzer));
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void add(IndexCollection collection, Map<String, Object> document) {
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        try (Directory directory = FSDirectory.open(repository.getData());
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(directory, config)) {
            writer.addDocument(toRawDocument(collection, document));
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    private Document toRawDocument(IndexCollection collection, Map<String, Object> document) {
        Document rawDocument = new Document();
        rawDocument.add(new TextField(COLLECTIONFIELD, collection.getName(), org.apache.lucene.document.Field.Store.YES));
        for(org.uweaver.core.search.Field field : collection.getFields()) {
            String key = field.getName().toLowerCase();
            Object value = document.get(key);
            org.apache.lucene.document.Field _field = null;
            if(value==null) continue;

            org.apache.lucene.document.Field.Store store = collection.isKey(key) ? org.apache.lucene.document.Field.Store.YES : org.apache.lucene.document.Field.Store.NO;

            if (isText(value)) {
                _field = new TextField(key, value.toString(), store);
            } else if (Long.class.isAssignableFrom(value.getClass())) {
                _field = new LongField(key, (Long) value, store);
            } else if (Date.class.isAssignableFrom(value.getClass())) {
                value = ((Date) value).getTime();
                _field = new LongField(key, (Long) value, store);
            }
            if(_field!=null) rawDocument.add(_field);
        }
        return rawDocument;
    }


    private Query toRawQuery(IndexQuery query, Analyzer analyzer) {
        Query rawQuery;
        try {
            rawQuery = new QueryParser("_NA", analyzer).parse(query.kql());
        } catch (ParseException e) {
            throw new org.uweaver.core.exception.InvalidFormatException(e);
        }
        return rawQuery;
    }

    private boolean isText(Object value) {
        return CharSequence.class.isAssignableFrom(value.getClass());
    }
}
