/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.ConversionException;
import org.uweaver.core.exception.NotEmptyConflictException;
import org.uweaver.core.exception.UnsupportedTypeException;

import java.util.Arrays;

/**
 * <h1>BaseConverter</h1>
 * The BaseConverter class implements the basic functionality required by any converters.
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-05-6
 */
public abstract class BaseConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseConverter.class);

    protected Object defaultValue = null;
    protected Class[] supportedTypes = {};
    protected boolean useDefault = false;
    protected boolean nullable = true;

    @Override
    public abstract <T> T convert(Object any, Class<T> type);

    @Override
    public <T> T convert(Object any, Class<T> type, T defaultValue) {
        T another = defaultValue;

        try {
            another = convert(any, type);
        } catch(Exception ex) {
            LOGGER.warn("" + ex);
        }

        return (another==null) ? defaultValue : another;
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public <T> T getDefaultValue(Class<T> type) {
        return convert(defaultValue, type);
    }

    @Override
    public Class<?>[] getSupportedTypes() {
        return supportedTypes;
    }

    @Override
    public void setUseDefault(boolean useDefault) {
        if(!isNullable() && getDefaultValue(Object.class) == null) {
            throw new NotEmptyConflictException("${0} is required.", "defaultValue");
        }
        this.useDefault = useDefault;
    }

    @Override
    public boolean isUseDefault() {
        return useDefault;
    }

    @Override
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    @Override
    public boolean isNullable() {
        return nullable;
    }

    @Override
    public <T> boolean isSupported(Class<T> type) {
        boolean result = false;

        for(Class clazz : Arrays.asList(supportedTypes)) {
            if(clazz.isAssignableFrom(type)) {
                result = true;
                break;
            }
        }

        return result;
    }

    protected Object handleUnsupportedError(Object any, Class<?> type, Throwable cause) {

        String[] supportedTypes = new String[getSupportedTypes().length];

        for(int i=0; i<getSupportedTypes().length; i++) {
            supportedTypes[i] = getSupportedTypes()[i].getName();
        }


        throw new UnsupportedTypeException("${0} is not supported for conversion. Supported types: ${1}", cause, type, supportedTypes);
    }

    protected Object handleUnsupportedError(Object any, Class<?> type) {

        return handleUnsupportedError(any, type, null);
    }

    protected Object convertNullValue() {
        if(!nullable && defaultValue==null) {
            throw new NotEmptyConflictException("${0} is required.", "defaultValue");
        }

        return (useDefault) ? defaultValue : null;
    }

    protected Object handleConversionError(Object any, Class<?> type, Throwable cause) {
        if(isUseDefault()) {
            return getDefaultValue(type);
        }

        String[] supportedTypes = new String[getSupportedTypes().length];

        for(int i=0; i<getSupportedTypes().length; i++) {
            supportedTypes[i] = getSupportedTypes()[i].getName();
        }

        throw new ConversionException(cause);
    }

    protected Object handleConversionError(Object any, Class<?> type) {

        return handleConversionError(any, type, null);

    }
}
