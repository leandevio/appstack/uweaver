package org.uweaver.core.util;

import java.util.Locale;

/**
 * Created by jasonlin on 5/6/15.
 */
public abstract class BaseLocaleConverter extends BaseConverter implements LocaleConverter {

    protected Locale locale = Environment.getDefaultInstance().locale();

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
