/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.UnsupportedTypeException;

import java.math.BigInteger;

/**
 * <h1>BooleanConverter</h1>
 * The BooleanConverter class implements the conversion of Boolean compatible objects. The implementation provides
 * the conversion between the following types:
 * <ul>
 *     <li>Boolean</li>
 *     <li>Integer</li>
 *     <li>BigInteger</li>
 *     <li>String</li>
 * </ul>
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-05-12
 */
public class BooleanConverter extends BaseConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(BooleanConverter.class);

    public BooleanConverter() {
        initialize();
    }

    public BooleanConverter(Object defaultValue) {
        setDefaultValue(defaultValue);
        initialize();
    }

    private void initialize() {
        Class[] supportedTypes = {Boolean.class,
                Integer.class, Long.class, BigInteger.class,
                String.class};

        this.supportedTypes = supportedTypes;
    }

    @Override
    public <T> T convert(Object any, Class<T> type) {
        T another = null;

        if(any==null) {
            return (T) convertNullValue();
        } else if(type.isInstance(any)) {
            return (T) any;
        }

        if(Boolean.class.isAssignableFrom(type)||boolean.class==type) {
            another = (T) convertToBoolean(any);
        } else if(Integer.class.isAssignableFrom(type)) {
            another = (T) convertToInteger(any);
        } else if(Long.class.isAssignableFrom(type)) {
            another = (T) convertToLong(any);
        } else if(BigInteger.class.isAssignableFrom(type)) {
            another = (T) convertToBigInteger(any);
        } else if(String.class.isAssignableFrom(type)) {
            another = (T) convertToString(any);
        } else {
            another = (T) handleUnsupportedError(any, type);
        }

        return another;
    }

    private Boolean convertToBoolean(Object any) {
        Boolean another;

        if(any instanceof Boolean) {
            another = (Boolean) any;
        } else if(any instanceof String) {
            try {
                another = new Boolean((String) any);
            } catch(IllegalArgumentException ex) {
                LOGGER.warn("" + ex);
                another = (Boolean) handleConversionError(any, Boolean.class);
            }
        } else if(any instanceof Number) {
            another = (((Number) any).intValue() == 0) ? false : true;
        } else {
            another = (Boolean) handleUnsupportedError(any, Boolean.class);
        }

        return another;
    }

    private Integer convertToInteger(Object any){
        Integer another;
        Boolean b;

        try {
            b = convertToBoolean(any);
        } catch (UnsupportedTypeException uex) {
            b = (Boolean) handleUnsupportedError(any, Integer.class, uex);
        } catch (Exception ex) {
            b = (Boolean) handleConversionError(any, Integer.class, ex);
        }

        another = b ? 1 : 0;

        return another;
    }

    private Long convertToLong(Object any){
        Long another;
        Boolean b;

        try {
            b = convertToBoolean(any);
        } catch (UnsupportedTypeException uex) {
            b = (Boolean) handleUnsupportedError(any, Long.class, uex);
        } catch (Exception ex) {
            b = (Boolean) handleConversionError(any, Long.class, ex);
        }

        another = b ? 1L : 0L;

        return another;
    }


    private BigInteger convertToBigInteger(Object any){
        BigInteger another;
        Boolean b;

        try {
            b = convertToBoolean(any);
        } catch (UnsupportedTypeException uex) {
            b = (Boolean) handleUnsupportedError(any, BigInteger.class, uex);
        } catch (Exception ex) {
            b = (Boolean) handleConversionError(any, BigInteger.class, ex);
        }

        another = b ? BigInteger.valueOf(1L) : BigInteger.valueOf(0L);

        return another;
    }

    private String convertToString(Object any){
        String another;
        another = any.toString();

        return another;
    }

}
