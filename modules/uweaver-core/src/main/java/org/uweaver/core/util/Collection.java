/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.uweaver.cache.LocalStorage;
import org.uweaver.core.event.AbstractListenable;
import org.uweaver.core.event.Listenable;
import org.uweaver.search.*;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Models obj = new Models();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Collection extends AbstractListenable implements List<Model>, Listenable {
    private static int DEFAULTPAGESIZE = 16;
    private static Pageable DEFAULTPAGEREQUEST = new PageRequest(0, DEFAULTPAGESIZE);
    private List<Model> data = new ArrayList<>();
    private List<Predicate> predicates = new ArrayList<>();
    private Sorter sorter = new Sorter();
    private Pageable pageable = DEFAULTPAGEREQUEST;
    private Searchable proxy = null;

    public Collection() {
        super();
    }

    public Collection(List<?> beans) {
        super();
        this.reset(beans);
    }

    public Collection(Searchable proxy) {
        super();
        this.proxy = proxy;
    }

    public <T> List<T> asBeans(Class<T> type, String ... excludes) {
        List<T> beans = new ArrayList<>();

        for (Model model : data) {
            beans.add(model.asBean(type, excludes));
        }

        return beans;
    }

    public boolean isPaginal() {
        return (this.proxy!=null&&this.pageable!=null);
    }

    public Pageable pageable() {
        return this.pageable;
    }

    public Sorter sorter() {
        return this.sorter;
    }

    public Collection filter(List<Predicate> predicates) {
        Collection match = new Collection();
        for(Model model : data) {
            for(Predicate predicate : predicates) {
                String subject = predicate.subject();
                Object expectation = predicate.expectation();
                if(expectation==null) {
                    if(model.get(subject)==null) match.add(model);
                } else if(expectation.equals(model.get(subject))) {
                    match.add(model);
                }
            }
        }
        return match;
    }

    public Model find(List<Predicate> predicates) {
        Model match = null;
        for(Model model : data) {
            for(Predicate predicate : predicates) {
                String subject = predicate.subject();
                Object expectation = predicate.expectation();
                if(expectation==null) {
                    if(model.get(subject)==null) match = model;
                } else if(expectation.equals(model.get(subject))) {
                    match = model;
                }
            }
            if(match!=null) break;
        }
        return match;
    }

    public Collection filter(Predicate predicate) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(predicate);
        return filter(predicates);
    }

    public Model find(Predicate predicate) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(predicate);
        return find(predicates);
    }

    public Collection filter(java.util.function.Predicate<Model> predicate) {
        Collection match = new Collection();
        for(Model model : data) {
            if(predicate.test(model)) {
                match.add(model);
            }
        }
        return match;
    }

    public Model find(java.util.function.Predicate<Model> predicate) {
        Model match = null;
        for(Model model : data) {
            if(predicate.test(model)) {
                match = model;
                break;
            }
        }
        return match;
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return data.contains(o);
    }

    @Override
    public Iterator<Model> iterator() {
        return data.iterator();
    }

    @Override
    public Object[] toArray() {
        return data.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return data.toArray(a);
    }

    @Override
    public boolean add(Model model) {
        return data.add(model);
    }

    @Override
    public boolean remove(Object o) {
        return data.remove(o);
    }

    @Override
    public boolean containsAll(java.util.Collection<?> c) {
        return data.containsAll(c);
    }

    @Override
    public boolean addAll(java.util.Collection<? extends Model> c) {
        return data.addAll(c);
    }

    @Override
    public boolean addAll(int index, java.util.Collection<? extends Model> c) {
        return data.addAll(index, c);
    }

    @Override
    public boolean removeAll(java.util.Collection<?> c) {
        return data.removeAll(c);
    }

    @Override
    public boolean retainAll(java.util.Collection<?> c) {
        return data.retainAll(c);
    }

    @Override
    public void clear() {
        this.predicates.clear();
        this.pageable = DEFAULTPAGEREQUEST;
        data.clear();
    }

    @Override
    public Model get(int index) {
        return data.get(index);
    }

    @Override
    public Model set(int index, Model element) {
        Model previousModel = data.set(index, element);
        trigger("change", element);
        return previousModel;
    }

    @Override
    public void add(int index, Model element) {
        data.add(index, element);
        trigger("add", element);
        trigger("update", element);
    }

    @Override
    public Model remove(int index) {
        Model model = data.remove(index);
        trigger("remove", model);
        trigger("update", model);
        return model;
    }

    @Override
    public int indexOf(Object o) {
        return data.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return data.lastIndexOf(o);
    }

    @Override
    public ListIterator<Model> listIterator() {
        return data.listIterator();
    }

    @Override
    public ListIterator<Model> listIterator(int index) {
        return data.listIterator(index);
    }

    @Override
    public List<Model> subList(int fromIndex, int toIndex) {
        return data.subList(fromIndex, toIndex);
    }

    public Model at(int index) {
        return get(index);
    }

    public long count() {
        return (this.proxy==null) ? size() : this.proxy.count(predicates);
    }

    public void set(List<?> beans) {
        if(beans==null) beans = new ArrayList<>();
        List<Model> data = new ArrayList<>();
        for (Object bean : beans) {
            Model model;
            if (Model.class.isAssignableFrom(bean.getClass())) {
                model = (Model) bean;
            } else {
                model = new Model(bean);
            }
            data.add(model);
        }
        this.data = data;
        List changeset = Collections.unmodifiableList(data);
        trigger("update", changeset);
    }

    public void reset(List<?> beans) {
        if(beans==null) beans = new ArrayList<>();
        List<Model> data = new ArrayList<>();
        for (Object bean : beans) {
            Model model;
            if (Model.class.isAssignableFrom(bean.getClass())) {
                model = (Model) bean;
            } else {
                model = new Model(bean);
            }
            data.add(model);
        }
        this.data = data;
        List changeset = Collections.unmodifiableList(data);
        trigger("update", changeset);
        trigger("reset", changeset);
    }

    public void reset() {
        reset(new ArrayList<>());
    }

    public void fetch() {
        fetch(this.pageable);
    }

    public void fetch(Sorter sorter) {
        fetch(this.predicates, sorter, this.pageable);
    }


    public void fetch(Pageable pageable) {
        fetch(this.predicates, this.sorter, pageable);
    }

    public void fetch(List<Predicate> predicates) {
        fetch(predicates, new PageRequest(0, pageable.size()));
    }

    public void fetch(List<Predicate> predicates, Pageable pageable) {
        fetch(predicates, this.sorter, pageable);
    }

    public void fetch(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        Searchable proxy = (this.proxy==null) ? new LocalStorage(this.data) : this.proxy;
        List<?> beans = proxy.search(predicates, sorter, pageable);
        this.predicates = predicates;
        this.sorter = sorter;
        this.pageable = pageable;
        reset(beans);
    }
}
