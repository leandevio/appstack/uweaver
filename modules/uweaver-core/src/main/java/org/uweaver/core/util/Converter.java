/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

/**
 * <h1>Converter</h1>
 * The Converter interface defines the interface of  The definitions provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 * </p>
 * @author Jason Lin
 * @version 1.0
 * @since 2014-06-06
 */

public interface Converter {
    <T> T convert(Object any, Class<T> type);

    <T> T convert(Object any, Class<T> type, T defaultValue);

    void setDefaultValue(Object defaultValue);

    <T> T getDefaultValue(Class<T> clazz);

    Class<?>[] getSupportedTypes();

    void setUseDefault(boolean useDefault);

    boolean isUseDefault();

    void setNullable(boolean nullable);

    boolean isNullable();

    <T> boolean isSupported(Class<T> type);

}
