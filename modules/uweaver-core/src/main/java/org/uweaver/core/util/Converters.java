/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.UnsupportedTypeException;

import java.util.*;

/**
 * <h1>Converters</h1>
 * The Converters is an utility class to ease the usage of converters.
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-05-6
 */
public class Converters extends BaseConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(Converters.class);

    private Map<Class<?>, Converter> converters = new HashMap<>();

    public Converters() {
        initialize();
    }

    public void initialize() {
        converters.put(DateTime.class, new DateTimeConverter());
        converters.put(Calendar.class, converters.get(DateTime.class));
        converters.put(Date.class, converters.get(DateTime.class));
        converters.put(Number.class, new NumberConverter());
        converters.put(Boolean.class, new BooleanConverter());

        supportedTypes = new Class[converters.size()];

        int i = 0;
        for(Class clazz : converters.keySet()) {
            supportedTypes[i] = clazz;
            i++;
        }
    }

    @Override
    public <T> T convert(Object any, Class<T> type) {
        T another;
        Converter converter = null;

        if(any==null) {
            return (T) convertNullValue();
        } else if(type.isInstance(any)) {
            return (T) any;
        }

        // find the converter match target type.
        converter = lookup(type);

        // find the alternative converter can handle the conversion of the value to be converted.
        if(converter==null) {
            for (Class key : converters.keySet()) {
                if (key.isInstance(any)) {
                    converter = converters.get(key);
                    break;
                }
            }
        }

        if(converter!=null) {
            another = converter.convert(any, type);
        } else {
            another = (T) handleUnsupportedError(any, type);
        }

        return another;
    }


    public void register( Class<?> type, Converter converter) {
        converters.put(type, converter);
    }

    public void unregister(Class<?> type) {
        converters.remove(type);
    }

    public Converter lookup(Class<?> type) {
        Converter converter = null;

        for(Class key : converters.keySet()) {
            if(key.equals(type)) {
                converter = converters.get(key);
                break;
            }
        }

        if(converter==null) {
            for(Class key : converters.keySet()) {
                if(key.isAssignableFrom(type)) {
                    converter = converters.get(key);
                    break;
                }
            }
        }

        return converter;
    }

    public void setDateTimePattern(String dateTimePattern) {
        for(Converter converter : converters.values()) {
            if(converter instanceof DateTimeConverter) {
                ((DateTimeConverter) converter).setDateTimePattern(dateTimePattern);
            }
        }
    }

}
