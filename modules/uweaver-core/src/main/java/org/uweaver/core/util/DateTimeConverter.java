/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.uweaver.core.exception.UnsupportedTypeException;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * <h1>DateTimeConverter</h1>
 * The DateTimeConverter class implements the conversion of Date compatible objects. The implementation provides
 * the conversion between the following types:
 * <ul>
 *     <li>Date</li>
 *     <li>Calendar</li>
 *     <li>DateTime</li>
 *     <li>Long</li>
 *     <li>String</l>
 * </ul>
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-05-6
 */
public class DateTimeConverter extends BaseConverter {
    // ISO 8601 data & time representation format.
    private static final String DEFAULTDATETIMEPATTERN = "yyyy-MM-ddTHH:mm:ss.SSSZ";
    private Environment environment = Environment.getDefaultInstance();

    protected Class[] supportedTypes = {Date.class, Calendar.class, DateTime.class,
            Long.class, String.class};

    protected DateTimeZone timeZone;
    protected String dateTimePattern;

    private DateTimeFormatter dateTimeFormatter;
    private Locale locale;

    public DateTimeConverter() {
        initialize();
    }

    public DateTimeConverter(Object defaultValue) {
        setDefaultValue(defaultValue);
        initialize();
    }

    private void initialize() {
        setDateTimePattern(DEFAULTDATETIMEPATTERN);
        setTimeZone(environment.timezone());
        setLocale(environment.locale());
    }

    @Override
    public <T> T convert(Object any, Class<T> type) {
        T another = null;

        if(any==null) {
            return (T) convertNullValue();
        } else if(type.isInstance(any)) {
            return (T) any;
        }

        if(java.util.Date.class.isAssignableFrom(type)) {
            another = (T) convertToDate(any);
        } else if(java.util.Calendar.class.isAssignableFrom(type)) {
            another = (T) convertToCalendar(any);
        } else if(org.joda.time.DateTime.class.isAssignableFrom(type)) {
            another = (T) convertToDateTime(any);
        } else if(String.class.isAssignableFrom(type)) {
            another = (T) convertToString(any);
        } else if(Long.class.isAssignableFrom(type)) {
            another = (T) convertToLong(any);
        } else {
            another = (T) handleUnsupportedError(any, type);
        }

        return another;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = DateTimeZone.forTimeZone(timeZone);
        dateTimeFormatter = dateTimeFormatter.withZone(this.timeZone);
    }

    public TimeZone getTimeZone() {
        return timeZone.toTimeZone();
    }


    public void setLocale(Locale locale) {
        this.locale = locale;
        dateTimeFormatter = dateTimeFormatter.withLocale(locale);
    }

    public Locale getLocale() {
        return locale;
    }

    public String getDateTimePattern() {
        return dateTimePattern;
    }

    public void setDateTimePattern(String dateTimePattern) {
        this.dateTimePattern = dateTimePattern;
        dateTimeFormatter = DateTimeFormat.forPattern(dateTimePattern.replace("T", "'T'")).withZone(this.timeZone);
    }

    private Date convertToDate(Object any){
        Date another;
        DateTime dt;

        try {
            dt = convertToDateTime(any);
        } catch (UnsupportedTypeException uex) {
            dt = (DateTime) handleUnsupportedError(any, Date.class, uex);
        } catch (Exception ex) {
            dt = (DateTime) handleConversionError(any, Date.class, ex);
        }

        another = (dt==null) ? null : dt.toDate();

        return another;
    }


    private DateTime convertToDateTime(Object any) {
        DateTime another;

        if(any instanceof DateTime) {
            another = (DateTime) any;
        } else if(any instanceof String) {
            try {
                another = DateTime.parse((String) any, dateTimeFormatter);
            } catch(IllegalArgumentException ex) {
                another = (DateTime) handleConversionError(any, DateTime.class, ex);
            }
        } else if(any instanceof Date) {
            another = new DateTime(any, timeZone);
        } else if(any instanceof Calendar) {
            another = new DateTime(any, ISOChronology.getInstance());
        } else if(any instanceof Long) {
            another = new DateTime(any);
        } else {
            another = (DateTime) handleUnsupportedError(any, DateTime.class);
        }

        return another;
    }

    private Calendar convertToCalendar(Object any){
        Calendar another;

        DateTime dt = convertToDateTime(any);
        another = (dt==null) ? null : dt.toCalendar(locale);

        return another;
    }


    private String convertToString(Object any){
        String another;
        DateTime dt = convertToDateTime(any);
        another = dateTimeFormatter.print(dt);

        return another;
    }

    private Long convertToLong(Object any){
        Long another;
        DateTime dt = convertToDateTime(any);
        another = dt.getMillis();

        return another;
    }
}
