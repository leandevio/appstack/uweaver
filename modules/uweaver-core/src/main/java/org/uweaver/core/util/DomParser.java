package org.uweaver.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by jasonlin on 2/8/14.
 */
public class DomParser {

    private static Logger logger = LoggerFactory.getLogger(DomParser.class);
    DocumentBuilderFactory factory;
    DocumentBuilder builder;

    public DomParser() throws ParserConfigurationException {
        //Get the DOM Builder Factory
        factory = DocumentBuilderFactory.newInstance();

        //Get the DOM Builder
        builder = factory.newDocumentBuilder();
    }

    public Document parse(String filename) throws IOException, SAXException {
        Document document = builder.parse(filename);

        //optional, but recommended
        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        document.getDocumentElement().normalize();

        return document;

    }


}
