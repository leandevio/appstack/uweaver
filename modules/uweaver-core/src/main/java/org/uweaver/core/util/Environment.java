package org.uweaver.core.util;

import org.uweaver.core.exception.InvalidFormatException;
import org.uweaver.core.exception.NotFoundException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

/**
 * The class represents the application runtime environment. The implementation provides:
 * - read the properties of the runtime environment
 * - modify the properties of the runtime environment
 *
 * Environment look for the system property file & the default property file in the class path and use them as
 * the defaults to setup the environment first, then the customized property file in the application base directory
 * to customize the environment. The properties in the system property file are loaded first and can't not be override by
 * the default property file or the custom property file. The properties in the default property file are defaults and
 * can be override by those specified in the customized property file.
 *
 * The name of the system property file is uweaver.properties. It is in the class path.
 *
 * The file name of the property file is the application name with the "properties" extension. Environment read the
 * value of ${app.name} from the system property file to determine the application name. The default is "app"
 * if not specified. Therefore the default application property file is app.properties.
 *
 * The application home is the location for the application binary distribution and default configurations. It can be
 * specify by the environment variable (or the system property). The name of the variable is the uppercase of the
 * application name with "_HOME" suffix. The default is "APP_HOME".
 *
 * The application base is the location for the customizations to application configurations and the data repository.
 * It can be specify by the environment variable (or the system property). The name of the variable is the uppercase of the
 * application name with "_BASE" suffix. The default is "APP_BASE".
 *
 * The application base is the same as the application home by default.
 *
 * Usage:
 *
 * ```java
 * Environment environment = Environment.getDefaultInstance();
 *
 * assertEquals(environment.getProperty("session.timeout"), "30");
 * assertEquals(environment.getPropertyAsInteger("batch.timeout"), 180);
 * assertEquals(environment.getPropertyAsInteger("helpdesk.email"), "alice@leandev.com.tw");
 * ```
 *
 * app.properties
 *
 * ```
 * # File Storage
 * data.folder=var/data
 * index.folder=var/index
 * log.folder=var/log
 * cache.folder=var/cache
 * tmp.folder=var/tmp
 *
 * # Locale
 * locale.language=en
 * locale.country=US
 *
 * #Time
 * time.timeZone=America/Los_Angeles
 * time.dateTimePattern=yyyy-MM-ddTHH:mm:ss.SSSZ
 * #Resource
 * resource.encoding=UTF-8
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Environment {
    private static final String SYSPROPERTYFILE = "uweaver.properties";
    private static final String PROPFILEEXTENSION = "properties";

    private static final String APPNAME = "app";
    private static final Path APPHOME = Paths.get(System.getProperty("user.home"));

    private static final String CONFFOLDER = "conf";

    // system property
    // keys
    private static final String APPNAMEKEY = "app.name";
    private static final String APPCONFIGKEY = "app.config";

    // application property
    // keys
    private static final String APPNOCONFLICTKEY = "app.noConflict";
    private static final String APPSTAGEKEY = "app.stage";
    private static final String APPURLKEY = "app.url";
    private static final String DATAFOLDERKEY = "data.folder";
    private static final String DOCFOLDERKEY = "doc.folder";
    private static final String INDEXFOLDERKEY = "index.folder";
    private static final String TPLFOLDERKEY = "tpl.folder";
    private static final String LOGFOLDERKEY = "log.folder";
    private static final String LOGLEVELKEY = "log.level";
    private static final String CACHEFOLDERKEY = "cache.folder";
    private static final String TMPFOLDERKEY = "tmp.folder";
    private static final String SHAREDFOLDERKEY = "shared.folder";
    private static final String BACKUPFOLDERKEY = "backup.folder";
    private static final String LOCALELANGUAGEKEY = "locale.language";
    private static final String LOCALECOUNTRYKEY = "locale.country";
    private static final String TIMEZONEKEY = "time.timezone";
    private static final String DATETIMEPATTERNKEY = "time.datetimePattern";
    private static final String DATEPATTERNKEY = "time.datePattern";
    private static final String TIMEPATTERNKEY = "time.timePattern";
    private static final String FILEENCODINGKEY = "file.encoding";
    private static final String FILEMAXUPLOADSIZEKEY = "file.maxUploadSize";
    private static final String APPVERSIONKEY = "app.version";

    // default values
    private static final Boolean APPNOCONFLICT = false;
    private static final String APPSTAGE = Stage.PRODUCTION.name();
    private static final String APPURL = "http://localhost/";
    private static final String DATAFOLDER = "var/data";
    private static final String DOCFOLDER = "var/doc";
    private static final String INDEXFOLDER = "var/index";
    private static final String TPLFOLDER = "var/tpl";
    private static final String LOGFOLDER = "var/log";
    private static final Integer LOGLEVEL = 3;
    private static final String CACHEFOLDER = "var/cache";
    private static final String TMPFOLDER = "var/tmp";
    private static final String SHAREDFOLDER = "var/shared";
    private static final String BACKUPFOLDER = "var/backup";

    private static final String DATETIMEPATTERN = "yyyy-MM-ddTHH:mm:ss.SSSZ";
    private static final String DATEPATTERN = "yyyy-MM-dd";
    private static final String TIMEPATTERN = "HH:mm:ss.SSS";

    private static final String FILEENCODING = "UTF-8";
    private static final Integer FILEMAXUPLOADSIZE = 8192;

    private static final String APPVERSION = "0.0.0";

    private static final Pattern WINABSOLUTEFILEPATHPATTERN = Pattern.compile("^[a-zA-Z]{1}:\\\\");

    private String appName = APPNAME;
    private Boolean appNoConflict = APPNOCONFLICT;
    private Stage stage = Stage.valueOf(APPSTAGE.toUpperCase());
    private String url;
    private Path appHome, appBase;
    private Version appVersion;
    private Properties sysProps, props;


    private Locale locale;
    private TimeZone timezone;
    private String datetimePattern, datePattern, timePattern;
    private String fileencoding;
    private Integer fileMaxUploadSize;
    private int logLevel = 0;
    private Path dataFolder, docFolder, indexFolder, tplFolder, logFolder, cacheFolder, tmpFolder, sharedFolder, backupFolder;

    public Path appHome() {
        return appHome;
    }

    public Path appBase() {
        return appBase;
    }

    public String appName() {
        return appName;
    }

    protected void setAppName(String appName) {
        this.appName = (appName==null) ? APPNAME : appName;
    }

    public Boolean appNoConflict() {
        return appNoConflict;
    }

    public Version appVersion() {
        return this.appVersion;
    }

    public Stage stage() {
        return stage;
    }

    public String url() {
        return url;
    }

    public String property(String key) {
        return property(key, null);
    }

    public String property(String key, String defaultValue) {
        return props.getProperty(key, defaultValue);
    }

    public Integer propertyAsInteger(String key, Integer defaultValue) {
        return props.containsKey(key) ? Integer.parseInt(props.getProperty(key)) : defaultValue;
    }

    public Long propertyAsLong(String key) {
        return propertyAsLong(key, null);
    }

    public Long propertyAsLong(String key, Long defaultValue) {
        return props.containsKey(key) ? Long.parseLong(props.getProperty(key)) : defaultValue;
    }

    public Integer propertyAsInteger(String key) {
        return propertyAsInteger(key, null);
    }


    public boolean propertyAsBoolean(String key, Boolean defaultValue) {
        return props.containsKey(key) ? Boolean.parseBoolean(props.getProperty(key)) : defaultValue;
    }

    public boolean propertyAsBoolean(String key) {
        return propertyAsBoolean(key, false);
    }

    public boolean propertyAsBoolean(String key, String defaultValue) {
        return propertyAsBoolean(key, Boolean.parseBoolean(defaultValue));
    }

    public Path propertyAsPath(String key, String defaultValue) {
        return propertyAsPath(key, defaultValue, false);
    }

    public Path propertyAsPath(String key, String defaultValue, Boolean create) {
        String value = property(key, defaultValue);
        Path path;

        if(value.startsWith("~")) {
            path = Paths.get(value.replaceFirst("~", System.getProperty("user.home")));
        } else if(value.startsWith("\\.")) {
            path = appBase().resolve(value);
        } else if(!value.startsWith("/") && !WINABSOLUTEFILEPATHPATTERN.matcher(value).find()) {
            path = appBase().resolve(value);
        } else {
            path = Paths.get(value);
        }

        if(create) createFolder(path);

        return path;
    }

    public Path propertyAsNoConflictPath(String key, String defaultValue, Boolean create) {
        Path path = propertyAsPath(key, defaultValue);
        if(appNoConflict() && !appName().equalsIgnoreCase(APPNAME)) {
            path = path.resolve(appName());
        }
        if(create) createFolder(path);
        return path;
    }

    public URL propertyAsURL(String key, URL defaultValue) {
        String value = property(key);
        if(value==null) return defaultValue;

        URL url;

        try {
            url = new URL(value);
        } catch (MalformedURLException e) {
            throw new InvalidFormatException(e);
        }

        return url;

    }

    public synchronized void setProperty(String key, String value) {
        props.put(key, value);
    }

    public Locale locale() {
        return locale;
    }

    public synchronized void setLocale(Locale locale) {
        this.locale = locale;
        setProperty(LOCALELANGUAGEKEY, locale.getLanguage());
        setProperty(LOCALECOUNTRYKEY, locale.getCountry());
    }

    public TimeZone timezone() {
        return timezone;
    }

    public synchronized void setTimezone(TimeZone timezone) {
        this.timezone = timezone;
    }

    public String datetimePattern() {
        return datetimePattern;
    }

    public synchronized void setDatetimePattern(String datetimePattern) {
        this.datetimePattern = datetimePattern;
    }

    public String datePattern() {
        return datePattern;
    }

    public synchronized void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public String timePattern() {
        return timePattern;
    }

    public synchronized void setTimePattern(String timePattern) {
        this.timePattern = timePattern;
    }

    public Path dataFolder() {
        return dataFolder;
    }

    public Path docFolder() {
        return docFolder;
    }

    public Path indexFolder() {
        return indexFolder;
    }

    public Path tplFolder() {
        return tplFolder;
    }

    public Path logFolder() {
        return logFolder;
    }

    public int logLevel() {
        return logLevel;
    }

    public Path cacheFolder() {
        return cacheFolder;
    }

    public Path tmpFolder() {
        return tmpFolder;
    }

    public Path sharedFolder() {
        return sharedFolder;
    }

    public Path backupFolder() {
        return backupFolder;
    }

    public String fileencoding() {
        return fileencoding;
    }

    public Integer fileMaxUploadSize() {
        return fileMaxUploadSize;
    }

    public Path conf(String filename) {
        return appHome().resolve(CONFFOLDER).resolve(filename);
    }

    public Class<?> appConfig() {
        Class<?> config = null;
        String configClassName = property(APPCONFIGKEY);

        if(configClassName==null) return config;

        try {
            config = Class.forName(configClassName);
        } catch (ClassNotFoundException e) {
            throw new NotFoundException(e);
        }

        return config;
    }


    private static class SingletonHolder {
        private static final Environment INSTANCE = Environment.getInstance();
    }

    public static Environment getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static Environment getInstance() {
        Environment environment = new Environment();
        environment.initialize();
        return environment;
    }

    protected static Environment getInstance(String appname) {
        Environment environment = new Environment();
        environment.setAppName(appname);
        environment.initialize(false);
        return environment;
    }

    private Environment() {}

    protected void reset(boolean readSystemProperties) {
        initialize(readSystemProperties);
    }

    public void reset() {
        reset(true);
    }

    public void reload() {
        initialize(false);
    }

    private void initialize(boolean readSystemProperties) {
        appHome = appBase = null;
        appVersion = null;
        sysProps = new Properties();
        props = new Properties();
        timezone = null; datetimePattern = datePattern = timePattern = null;
        logLevel = 0;
        dataFolder = docFolder = indexFolder = tplFolder = logFolder = cacheFolder = tmpFolder = sharedFolder = backupFolder = null;

        // read the system property file.
        if(readSystemProperties) readSystemProperties();

        // setup application home location
        setupAppHome();

        // read the default property file.
        readDefaultProperties();

        // read the custom property file.
        readCustomProperties();

        // use the properties to setup the context
        setupContext();

        Enumeration<?> e = props.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = props.getProperty(key);
            info(key + ": " + value);
        }
        try {
            Files.walk(tmpFolder)
                    .map(Path::toFile)
                    .forEach(file -> {
                        if(file.isFile()) file.delete();
                    });
        } catch (IOException e1) {
            info(String.format("Unable to clean the temp folder. %s", tmpFolder.toString()));
        }
    }
    private void initialize() {
        initialize(true);
    }

    private void setupContext() {
        this.appNoConflict = propertyAsBoolean(APPNOCONFLICTKEY, APPNOCONFLICT);
        this.stage = Stage.valueOf(property(APPSTAGEKEY, APPSTAGE).toUpperCase());
        this.url = property(APPURLKEY, APPURL);
        this.locale = (property(LOCALELANGUAGEKEY)==null||property(LOCALECOUNTRYKEY)==null) ? Locale.getDefault() : new Locale(property(LOCALELANGUAGEKEY), property(LOCALECOUNTRYKEY));
        this.timezone = property(TIMEZONEKEY)==null ? TimeZone.getDefault() : TimeZone.getTimeZone(property(TIMEZONEKEY));
        this.datetimePattern = property(DATETIMEPATTERNKEY, DATETIMEPATTERN);
        this.datePattern = property(DATEPATTERNKEY, DATETIMEPATTERN);
        this.timePattern = property(TIMEPATTERNKEY, DATETIMEPATTERN);
        this.fileencoding = property(FILEENCODINGKEY, FILEENCODING);
        this.fileMaxUploadSize = propertyAsInteger(FILEMAXUPLOADSIZEKEY, FILEMAXUPLOADSIZE);
        this.dataFolder = propertyAsNoConflictPath(DATAFOLDERKEY, DATAFOLDER, true);
        this.docFolder = propertyAsNoConflictPath(DOCFOLDERKEY, DOCFOLDER, true);
        this.indexFolder = propertyAsNoConflictPath(INDEXFOLDERKEY, INDEXFOLDER, true);
        this.tplFolder = propertyAsNoConflictPath(TPLFOLDERKEY, TPLFOLDER, true);
        this.logLevel = propertyAsInteger(LOGLEVELKEY, LOGLEVEL);
        this.logFolder = propertyAsNoConflictPath(LOGFOLDERKEY, LOGFOLDER, true);
        this.cacheFolder = propertyAsNoConflictPath(CACHEFOLDERKEY, CACHEFOLDER, true);
        this.tmpFolder = propertyAsNoConflictPath(TMPFOLDERKEY, TMPFOLDER, true);
        this.sharedFolder = propertyAsNoConflictPath(SHAREDFOLDERKEY, SHAREDFOLDER, true);
        this.backupFolder = propertyAsNoConflictPath(BACKUPFOLDERKEY, BACKUPFOLDER, true);
        this.appVersion = new Version(property(APPVERSIONKEY, APPVERSION));
    }

    private void readSystemProperties() {
        info("Looking for the system property file (" + SYSPROPERTYFILE + ") in the class path.");
        try (InputStream input = Environment.class.getClassLoader().getResourceAsStream(SYSPROPERTYFILE)) {
            if(input!=null) {
                info("Use the the system property file (" + SYSPROPERTYFILE + ") in the class path.");
                sysProps.load(input);
            } else {
                info("The system property file (" + SYSPROPERTYFILE + ") not found.");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.appName = sysProps.getProperty(APPNAMEKEY, APPNAME);
    }

    private void readDefaultProperties() {
        String propFileName = this.appName + "." + PROPFILEEXTENSION;

        info("Looking for the default property file (" + propFileName + ") in the class path.");
        try (InputStream input = Environment.class.getClassLoader().getResourceAsStream(propFileName)) {
            if(input!=null) {
                info("Use the default property file (" + propFileName + ") in the class path.");
                props.load(input);
            } else {
                info("The default property file (" + propFileName + ") not found.");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Enumeration<?> e = sysProps.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = sysProps.getProperty(key);
            props.setProperty(key, value);
        }
    }

    private void readCustomProperties() {
        String propFileName = this.appName + "." + PROPFILEEXTENSION;
        Properties customProps = new Properties();

        Path conf = appBase.resolve("conf");
        info("Looking for the custom property file (" + propFileName + ") in the " + conf + ".");
        Path path = conf.resolve(propFileName);
        if(Files.exists(path)) {
            info("Use the custom property file (" + propFileName + ") in the " + conf + ".");
            try (InputStream input = new FileInputStream(path.toFile())) {
                customProps.load(input);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            info("The custom property file (" + propFileName + ") not found");
        }

        Enumeration<?> e = customProps.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            if(sysProps.containsKey(key)) continue;
            String value = customProps.getProperty(key);
            props.setProperty(key, value);
        }
    }

    private void setupAppHome() {
        String app_home = appName.toUpperCase() + "_HOME";
        String app_base = appName.toUpperCase() + "_BASE";

        String property = System.getProperty(app_home, System.getenv(app_home));

        if(property==null) property = System.getProperty("APP_HOME", System.getenv("APP_HOME"));

        appHome = toPath(property, APPHOME);
        appBase = toPath(System.getProperty(app_base, System.getenv(app_base)), appHome);
    }

    private Path toPath(String s, Path defaultValue) {
        if(s==null) return defaultValue;

        if(s.startsWith("~")) {
            s = System.getProperty("user.home") + s.substring(1);
        }

        Path path = Paths.get(s);
        return Files.exists(path) ? path : defaultValue;
    }

    private void createFolder(Path dir) {
        try {
            Files.createDirectories(dir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void info(String text) {
        System.out.println(text);
    }

    public enum Stage {
        DEVELOPMENT("DEVELOPMENT"), TEST("TEST"), PRODUCTION("PRODUCTION");

        private final String name;

        Stage(String name) {
            this.name = name;
        }

        public String toString(){
            return name;
        }
    }
}
