package org.uweaver.core.util;

import org.apache.commons.beanutils.BeanMap;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;


/**
 * Item is a Java Bean utility.
 *
 * Created by jasonlin on 2/18/14.
 */
public class Item implements Map<Object, Object> {
    private Object bean;
    private BeanMap bm;

    private static Converters converters = new Converters();

    private JSONParser parser = new JSONParser();

    public Item() {
        setBean(new HashMap<>());
    }

    public Item(Object bean) {
        setBean(bean);
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
        bm = new BeanMap(bean);
    }

    public <T> T asBean(Class<T> type) {
        T bean;
        try {
            bean = type.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
        Item data = new Item(bean);

        for(Object key : data.keySet()) {
            if(!data.isWritable((String) key)) continue;
            data.put(key, get(key));
        }

        return bean;
    }

    public boolean isWritable(String key) {
        return (getWriteMethod(key)!=null);
    }

    @Override
    public int size() {
        return bm.size();
    }

    @Override
    public boolean isEmpty() {
        return bm.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return bm.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return bm.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return bm.get(key);
    }


    @Override
    public Object put(Object key, Object value) {
        if(!isWritable((String) key)) {
            throw new ApplicationException(bean.getClass().toString() + "'s property - " + key.toString() + " is not writable");
        }

        if(value==null) return bm.put(key, value);

        Object assigned;
        Type type = getWritableType((String) key);
        Class rawType;

        if(type instanceof Class) {
            rawType = (Class) type;
        } else if(type instanceof ParameterizedType) {
            rawType = (Class) ((ParameterizedType) type).getRawType();
        } else {
            return bm.put(key, value);
        }

        if(type instanceof Class && rawType.isAssignableFrom(value.getClass())) {
            assigned = value;
        } else if(type instanceof Class && converters.isSupported(rawType)&&converters.isSupported(value.getClass())) {
            assigned = converters.convert(value, rawType);
        } else if(java.util.Collection.class.isAssignableFrom(rawType) && java.util.Collection.class.isAssignableFrom(value.getClass())) {
            /**--
            java.util.Collection collection = (java.util.Collection) bm.get(key);
            collection.clear();
            collection.addAll((java.util.Collection) value);
            assigned = collection;
            --*/
             assigned = value;
        } else {
            assigned = value;
        }

        return bm.put(key, assigned);
    }

    @Override
    public Object remove(Object key) {
        return bm.remove(key);
    }

    @Override
    public void putAll(Map<?, ?> map) {
        for(Object key : map.keySet()) {
            put(key, map.get(key));
        }
    }

    public void putAllWritable(Map<String, ?> map) {
        for(String key : map.keySet()) {
            if(isWritable(key)) {
                put(key, map.get(key));
            }
        }
    }

    @Override
    public void clear() {
        bm.clear();
    }

    @Override
    public Set<Object> keySet() {
        return bm.keySet();
    }

    @Override
    public java.util.Collection<Object> values() {
        return bm.values();
    }

    @Override
    public Set<Entry<Object, Object>> entrySet() {
        return bm.entrySet();
    }

    public Class getRawType(String name) {
        return bm.getType(name);
    }

    public Type getType(String name) {
        Method method = bm.getWriteMethod(name);
        Type[] genericParameterTypes = method.getGenericParameterTypes();
        return genericParameterTypes[0];
    }

    public Type getWritableType(String name) {
        Method method = bm.getWriteMethod(name);
        Type[] genericParameterTypes = method.getGenericParameterTypes();
        return genericParameterTypes[0];
    }

    public Method getWriteMethod(String name) {
        return bm.getWriteMethod(name);
    }

    public Method getReadMethod(String name) {
        return bm.getReadMethod(name);
    }

    public Object clone() {
        try {
            BeanMap clone = (BeanMap) bm.clone();
            return clone.getBean();
        } catch (CloneNotSupportedException e) {
            throw new ApplicationException(e);
        }
    }
    /**
     * @todo use reflection instead of JSONParser to convert object type.
     * @param object
     * @param excludes
     */
    public void populate(Map<String, Object> object, String ... excludes) {
        if(object==null) return;
        List<String> props = Arrays.asList(excludes);
        String json = parser.writeValueAsString(object, excludes);
        Object obj = parser.readValue(json, getBean().getClass());
        Item item = new Item(obj);
        for(Object prop : keySet()) {
            if(props.contains(prop)) continue;
            if (object.containsKey(prop) && getWriteMethod(prop.toString())!=null) {
                put(prop, item.get(prop));
            }
        }
    }

    public boolean compare(Map map) {
        boolean result = true;
        for(Object key: keySet()) {
            if(!map.containsKey(key) || !get(key).equals(map.get(key))) {
                result = false;
                break;
            }
        }
        return result;
    }

    public boolean compare(Object bean) {
        return false;
    }

    private Class getParameterArgClass(String prop) {
        Class parameterArgClass = null;
        Method method = bm.getWriteMethod(prop);
        Type[] genericParameterTypes = method.getGenericParameterTypes();
        for(Type genericParameterType : genericParameterTypes){
            if(genericParameterType instanceof ParameterizedType){
                ParameterizedType aType = (ParameterizedType) genericParameterType;
                Type[] parameterArgTypes = aType.getActualTypeArguments();
                parameterArgClass = (Class) parameterArgTypes[0];
            }
        }

        return parameterArgClass;
    }

}
