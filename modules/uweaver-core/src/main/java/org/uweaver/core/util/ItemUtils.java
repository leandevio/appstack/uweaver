package org.uweaver.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.beanutils.BeanMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * Created by jasonlin on 2/8/14.
 */
public class ItemUtils {

    private static Logger logger = LoggerFactory.getLogger(ItemUtils.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static final String DEFAULTDATEPATTERN = "yyyy/MM/dd HH:mm:ss";

    private static Converters converters = new Converters();

    /**
     * 將來源物件的屬性複製到指定的標的物件.
     *
     * @param target 標的物件
     * @param source 來源物件
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static void bind(Object target, Object source, String ... excludes) {
        if(source instanceof Map) {
            if(target instanceof Map) {
                MapUtils.bind((Map) target, (Map) source, excludes);
                return;
            } else {
                MapUtils.bind(target, (Map) source, excludes);
                return;
            }
        } else if(target instanceof Map) {
            MapUtils.bind((Map) target, source, excludes);
            return;
        }

        BeanMap targetBm = new BeanMap(target);
        BeanMap sourceBm = new BeanMap(source);
        String index = Arrays.toString(excludes);
        for(Object key : targetBm.keySet()) {
            String name = (String) key;

            if(index.indexOf(name)!=-1) continue;
            // Check to see if the property is writable (with setter)
            Method method = targetBm.getWriteMethod(name);
            if(method==null) continue;

            if(sourceBm.containsKey(name)) {
                Object value = sourceBm.get(name);
                Class type = targetBm.getType(name);
                try {
                    targetBm.put(name, converters.convert(value, type));
                } catch(Exception e) {
                    logger.warn("Exception: ", e);
                    logger.warn("Could not bind " + name + " with " + value + "(" + type  + ")");
                }
            }
        }
    }

    public static String writePropsAsString(Object bean) throws JsonProcessingException {
        return objectMapper.writeValueAsString(bean);
    }


}
