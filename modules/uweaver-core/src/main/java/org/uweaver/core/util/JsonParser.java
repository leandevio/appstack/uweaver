package org.uweaver.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by jasonlin on 5/4/14.
 */
public class JsonParser {

    private static Logger logger = LoggerFactory.getLogger(JsonParser.class);
    public static final String DEFAULTDATEPATTERN = "yyyy/MM/dd HH:mm:ss";
    private static ObjectMapper objectMapper;

    static {
        init();
    }

    public static <T> T readValue(String json, Class<T> clazz) throws IOException {
        T any = (T) objectMapper.readValue(json, clazz);
        return any;
    }

    public static String writeValueAsString(Object any) throws JsonProcessingException {
        String value = objectMapper.writeValueAsString(any);
        return value;
    }

    private static void init() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

}
