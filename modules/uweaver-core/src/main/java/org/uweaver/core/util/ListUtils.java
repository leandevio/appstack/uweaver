package org.uweaver.core.util;

import java.util.List;

/**
 * Created by jasonlin on 2/8/14.
 */
public class ListUtils {
    public static boolean contains(List<String> list, String value) {
        for(String s : list) {
            if(s.equalsIgnoreCase(value)) return true;
        }
        return false;
    }

    public static void copy(List target, List source) {
        for(Object obj : source) {
            if(!target.contains(obj)) {
                target.add(obj);
            }
        }
    }

    public static void fromJson(List list, String json) {

    }

    public static String toJson(List list) {
        return null;
    }

}
