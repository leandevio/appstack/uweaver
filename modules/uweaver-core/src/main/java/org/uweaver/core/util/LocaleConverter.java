package org.uweaver.core.util;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by jasonlin on 5/6/15.
 */
public interface LocaleConverter extends Converter {
    Locale getLocale();
    void setLocale(Locale locale);
}
