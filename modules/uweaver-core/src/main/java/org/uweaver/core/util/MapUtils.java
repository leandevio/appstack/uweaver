package org.uweaver.core.util;

import org.apache.commons.beanutils.BeanMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by jasonlin on 2/8/14.
 */
public class MapUtils {
    private static Logger logger = LoggerFactory.getLogger(MapUtils.class);
    private static Converters converters = new Converters();
    public static final String DEFAULTDATEPATTERN = "yyyy/MM/dd HH:mm:ss";

    public static void bind(Object target, Map source, String ... excludes) {
        BeanMap bm = new BeanMap(target);
        String index = Arrays.toString(excludes);
        for(Object key : bm.keySet()) {
            String name = (String) key;
            if(index.indexOf(name)!=-1) continue;
            // Check to see if the property is writable (with setter)
            Method method = bm.getWriteMethod(name);
            if(method==null) continue;
            if(source.containsKey(name)) {
                Object value = source.get(name);
                Class type = bm.getType(name);
                try {
                    bm.put(name, converters.convert(value, type));
                } catch(Exception e) {
                    logger.warn("Exception: ", e);
                    logger.warn("Could not bind " + name + " with " + value + "(" + type  + ")");
                }
            }
        }
    }

    public static void bind(Map map, Object bean, String ... excludes) {
        BeanMap bm = new BeanMap(bean);
        String index = Arrays.toString(excludes);
        for(Object key : map.keySet()) {
            String name = (String) key;
            if(index.indexOf(name)!=-1) continue;
            if(bm.containsKey(name)) {
                Object value = bm.get(name);
                map.put(name, value);
            }
        }
    }

    public static void bind(Map target, Map source, String ... excludes) {
        String index = Arrays.toString(excludes);
        for(Object key : target.keySet()) {
            if(key instanceof String) {
                String name = (String) key;
                if(index.indexOf(name)!=-1) continue;
            }

            if(source.containsKey(key)) {
                target.put(key, source.get(key));
            }
        }
    }
}
