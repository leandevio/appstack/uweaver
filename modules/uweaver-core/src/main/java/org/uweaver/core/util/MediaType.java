/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ContentType obj = new ContentType();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class MediaType {
    public static MediaType IMAGE_PNG = new MediaType(PrimaryType.IMAGE, "png");
    public static MediaType IMAGE_BMP = new MediaType(PrimaryType.IMAGE, "bmp");
    public static MediaType IMAGE_GIF = new MediaType(PrimaryType.IMAGE, "gif");
    public static MediaType IMAGE_TIFF = new MediaType(PrimaryType.IMAGE, "tiff");
    public static MediaType IMAGE_JPEG = new MediaType(PrimaryType.IMAGE, "jpeg");

    private PrimaryType primaryType;
    private String subType;

    public MediaType(String type) {
        String[] tokens = type.toLowerCase().split("/");
        PrimaryType primaryType = PrimaryType.valueOf(tokens[0].toUpperCase());
        String subType = tokens[1];
        init(primaryType, subType);
    }

    public MediaType(String primaryType, String subType) {
        init(PrimaryType.valueOf(primaryType.toUpperCase()), subType);
    }

    public MediaType(PrimaryType primaryType, String subType) {
        init(primaryType, subType);
    }

    private void init(PrimaryType primaryType, String subType) {
        this.primaryType = primaryType;
        this.subType = subType.toLowerCase();
    }

    public PrimaryType primaryType() {
        return this.primaryType;
    }

    public String primaryTypeAsString() {
        return this.primaryType.toString();
    }

    public String subType() {
        return subType;
    }

    public String toString() {
       return primaryType.toString() + "/" + subType;
    }

    public enum PrimaryType {
        APPLICATION("application"), AUDIO("audio"), IMAGE("image"), MESSAGE("message"), MODEL("model"),
        MULTIPART("multipart"), TEXT("text"), VIDEO("video");

        private final String name;

        PrimaryType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        public String toString(){
            return name.toLowerCase();
        }
    }


}
