/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.uweaver.core.event.AbstractListenable;
import org.uweaver.core.event.Listenable;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.reflection.TypeUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.Collection;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Model obj = new Model();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Model extends AbstractListenable implements Map<String, Object>, Listenable {
    private Map<String, Object> data = new HashMap<>();
    private String description = null;

    private String idAttribute = "uuid";

    public Model() {
        super();
    }

    public Model(Object bean) {
        super();
        reset(bean);
    }

    public Object id() {
        return this.has("uuid") ? this.get("uuid") : this;
    }

    @Override
    public String toString() {
        return (description==null) ? super.toString() : description;
    }


    public <T> T asBean(Class<T> type, String ... excludes) {
        List<String> blacklist = Arrays.asList(excludes);
        T bean;
        try {
            bean = type.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }

        Item item = new Item(bean);
        for(Object name : item.keySet()) {
            if (!item.isWritable((String) name)) {
                continue;
            }
            if(blacklist.contains(name)) continue;

            Object value = data.get(name);
            // leave it the default value
            if (value == null) continue;

            Object assigned = value;
            Type valueType = item.getWritableType((String) name);

            if(valueType instanceof ParameterizedType) {
                Class valueRawType = item.getRawType((String) name);
                if(Collection.class.isAssignableFrom(valueRawType)) {
                    assigned = toParameterizedCollection((Collection) value, (ParameterizedType) valueType);
                }
            }
            item.put(name, assigned);

        }
        return bean;
    }

    private Collection toParameterizedCollection(Collection data, ParameterizedType type) {
        Class rawType = (Class) type.getRawType();
        Class typeArg = (Class) type.getActualTypeArguments()[0];
        if(TypeUtils.isScalarType(typeArg)) {
            return data;
        }

        Collection collection;
        if(List.class.isAssignableFrom(rawType)) {
            collection = new ArrayList();
        } else if(Set.class.isAssignableFrom(rawType)) {
            collection = new HashSet();
        } else {
            collection = new ArrayList();
        }

        for(Object datum : data) {
            collection.add(new Model(datum).asBean(typeArg));
        }
        return collection;
    }


    public void copyTo(Object bean) {
        Item item = new Item(bean);

        item.putAllWritable(this);

    }

    public void copyTo(Object bean, String ... excludes) {
        Item item = new Item(bean);
        List<String> blacklist = Arrays.asList(excludes);

        for(String name : keySet()) {
            if(!item.isWritable(name)) continue;
            if(blacklist.contains(name)) continue;
            item.put(name, get(name));
        }
    }

    public void copyFrom(Object bean, String ... excludes) {
        Model item = new Model(bean);
        List<String> blacklist = Arrays.asList(excludes);

        for(String name : item.keySet()) {
            if(blacklist.contains(name)) continue;
            set(name, item.get(name));
        }
    }

    public Model set(String key, Object value) {
        put(key, value);
        return this;
    }

    public boolean has(String key) {
        return containsKey(key);
    }

    public void reset(Object bean) {
        Map item;

        data.clear();

        if(Map.class.isAssignableFrom(bean.getClass())) {
            item = (Map) bean;
        } else {
            item = new Item(bean);
        }
        for(Object key : item.keySet()) {
            if(item instanceof Item && !((Item) item).isWritable((String) key)) continue;
            data.put((String) key, item.get(key));
        }

        trigger("change", data.keySet());
        trigger("reset", data.keySet());

        description = (bean==null) ? null : bean.toString();
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        boolean result = false;

        if(data.containsKey(key)) {
            result = true;
        } else if(data.containsKey("attributes")) {
            Map attributes = (Map) data.get("attributes");
            result = attributes.containsKey(key);
        }
        return result;
    }

    @Override
    public boolean containsValue(Object value) {
        return data.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        Object value = null;

        if(data.containsKey(key)) {
            value = data.get(key);
        } else if(data.containsKey("attributes")){
            Map attributes = (Map) data.get("attributes");
            value = attributes.get(key);
        }

        return value;
    }

    public String getAsString(String key, String defaultValue) {
        String s = (String) get(key);
        return (s==null) ? defaultValue : s;
    }

    public String getAsString(String key) {
        return (String) get(key);
    }

    public Date getAsDate(String key) {
        return (Date) get(key);
    }

    public Number getAsNumber(String key) {
        return (Number) get(key);
    }

    public Integer getAsInteger(String key) {
        return (Integer) get(key);
    }

    public Double getAsDouble(String key) {
        Number number = (Number) get(key);
        return number==null ? null : number.doubleValue();
    }

    public Float getAsFloat(String key) {
        Number number = (Number) get(key);
        return number==null ? null : number.floatValue();
    }

    public BigDecimal getAsBigDecimal(String key) {
        return (BigDecimal) get(key);
    }

    public BigInteger getAsBigInteger(String key) {
        return (BigInteger) get(key);
    }

    public Boolean getAsBoolean(String key) {
        return (Boolean) get(key);
    }

    public boolean getAsBoolean(String key, boolean defaultValue) {
        Boolean b = (Boolean) get(key);
        return (b==null) ? defaultValue : b;
    }

    public List getAsList(String key) {
        return (List) get(key);
    }

    @Override
    public Object put(String key, Object value) {
        Object previousValue;
        if(data.containsKey(key)) {
            previousValue = data.put(key, value);
        } else if(data.containsKey("attributes")) {
            Map attributes = (Map) data.get("attributes");
            previousValue = attributes.put(key, value);
        } else {
            previousValue = data.put(key, value);
        }

        Set<String> keySet = new HashSet<>();
        keySet.add(key);
        trigger("change", keySet);
        trigger("change:" + key, keySet);
        return previousValue;
    }

    @Override
    public Object remove(Object key) {
        Object previousValue = data.remove(key);
        Set<String> keySet = new HashSet<>();
        keySet.add(key.toString());
        trigger("change", keySet);
        trigger("change:" + key, keySet);
        return previousValue;
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        data.putAll(m);
        trigger("change", m.keySet());
    }

    @Override
    public void clear() {
        data.clear();
        trigger("change", data.keySet());
    }

    @Override
    public Set<String> keySet() {
        return data.keySet();
    }

    @Override
    public Collection<Object> values() {
        return data.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return data.entrySet();
    }

    public String idAttribute() {
        return this.idAttribute;
    }

    protected void setIdAttribute() {
        this.idAttribute = idAttribute;
    }

}
