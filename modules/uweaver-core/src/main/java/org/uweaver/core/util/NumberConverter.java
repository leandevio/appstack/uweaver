/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 *
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.UnsupportedTypeException;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * <h1>NumberConverter</h1>
 * The NumberConverter class implements the conversion of Number compatible objects. The implementation provides
 * the conversion between the following types:
 * <ul>
 *     <li>Integer</li>
 *     <li>Long</li>
 *     <li>BigInteger</li>
 *     <li>Integer</li>
 *     <li>Float</li>
 *     <li>Double</li>
 *     <li>BigDecimal</li>
 *     <li>String</li>
 * </ul>
 *
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-05-6
 */
public class NumberConverter extends BaseConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(NumberConverter.class);

    public NumberConverter() {
        initialize();
    }

    public NumberConverter(Object defaultValue) {
        setDefaultValue(defaultValue);
        initialize();
    }

    private void initialize() {
        Class[] supportedTypes = {Integer.class, Long.class, BigInteger.class, int.class, long.class,
                Float.class, Double.class, BigDecimal.class, float.class, double.class,
                String.class};

        this.supportedTypes = supportedTypes;
    }

    @Override
    public <T> T convert(Object any, Class<T> type) {
        T another = null;

        if(any==null) {
            return (T) convertNullValue();
        } else if(type.isInstance(any)) {
            return (T) any;
        }

        if(Integer.class.isAssignableFrom(type)||type==int.class) {
            another = (T) convertToInteger(any);
        } else if(Long.class.isAssignableFrom(type)||type==long.class) {
            another = (T) convertToLong(any);
        } else if(BigInteger.class.isAssignableFrom(type)) {
            another = (T) convertToBigInteger(any);
        } else if(Float.class.isAssignableFrom(type)||type==float.class) {
            another = (T) convertToFloat(any);
        } else if(Double.class.isAssignableFrom(type)||type==double.class) {
            another = (T) convertToDouble(any);
        } else if(BigDecimal.class.isAssignableFrom(type)) {
            another = (T) convertToBigDecimal(any);
        } else if(String.class.isAssignableFrom(type)) {
            another = (T) convertToString(any);
        } else {
            another = (T) handleUnsupportedError(any, type);
        }

        return another;
    }

    private Integer convertToInteger(Object any){
        Integer another;
        BigInteger n;

        try {
            n = convertToBigInteger(any);
        } catch (UnsupportedTypeException ex) {
            n = (BigInteger) handleUnsupportedError(any, Integer.class, ex);
        } catch (Exception ex) {
            n = (BigInteger) handleConversionError(any, Integer.class, ex);
        }

        another = n.intValue();

        return another;
    }

    private Long convertToLong(Object any){
        Long another;
        BigInteger n;

        try {
            n = convertToBigInteger(any);
        } catch (UnsupportedTypeException uex) {
            n = (BigInteger) handleUnsupportedError(any, Long.class);
        } catch (Exception ex) {
            n = (BigInteger) handleConversionError(any, Long.class);
        }

        another = n.longValue();

        return another;
    }


    private BigInteger convertToBigInteger(Object any) {
        BigInteger another;

        if(any instanceof BigInteger) {
            another = (BigInteger) any;
        } else if(any instanceof String) {
            try {
                another = new BigInteger((String) any);
            } catch(IllegalArgumentException ex) {
                another = (BigInteger) handleConversionError(any, BigInteger.class);
            }
        } else if(any instanceof Integer) {
            another = BigInteger.valueOf((Integer) any);
        } else if(any instanceof Long) {
            another = BigInteger.valueOf((Long) any);
        } else {
            another = (BigInteger) handleUnsupportedError(any, BigInteger.class);
        }

        return another;
    }

    private Float convertToFloat(Object any){
        Float another;
        BigDecimal n;

        try {
            n = convertToBigDecimal(any);
        } catch (UnsupportedTypeException uex) {
            n = (BigDecimal) handleUnsupportedError(any, Float.class);
        } catch (Exception ex) {
            n = (BigDecimal) handleConversionError(any, Float.class);
        }

        another = n.floatValue();

        return another;
    }

    private Double convertToDouble(Object any){
        Double another;
        BigDecimal n;

        try {
            n = convertToBigDecimal(any);
        } catch (UnsupportedTypeException uex) {
            n = (BigDecimal) handleUnsupportedError(any, Double.class);
        } catch (Exception ex) {
            n = (BigDecimal) handleConversionError(any, Double.class);
        }

        another = n.doubleValue();

        return another;
    }

    private BigDecimal convertToBigDecimal(Object any) {
        BigDecimal another;

        if(any instanceof BigDecimal) {
            another = (BigDecimal) any;
        } else if(any instanceof String) {
            try {
                another = new BigDecimal((String) any);
            } catch(IllegalArgumentException ex) {
                another = (BigDecimal) handleConversionError(any, BigDecimal.class);
            }
        } else if(any instanceof Float) {
            another = new BigDecimal(Float.toString((Float) any));
        } else if(any instanceof Double) {
            another = BigDecimal.valueOf((Double) any);
        } else if(any instanceof Integer) {
            another = BigDecimal.valueOf((Integer) any);
        } else if(any instanceof Long) {
            another = BigDecimal.valueOf((Long) any);
        } else if(any instanceof BigInteger) {
            another = new BigDecimal(any.toString());
        } else {
            another = (BigDecimal) handleUnsupportedError(any, BigDecimal.class);
        }

        return another;
    }


    private String convertToString(Object any){
        String another;
        another = any.toString();

        return another;
    }

}
