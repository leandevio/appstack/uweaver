/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PathPattern obj = new PathPattern();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PathPattern {
    private static final Pattern DOT = Pattern.compile("\\.");
    private static final Pattern WILDWILDCARD = Pattern.compile("\\*\\*/");
    private static final Pattern WILDCARD = Pattern.compile("\\*");
    private static final Pattern SUBPATTERNS = Pattern.compile("\\{(.*)\\}");
    private static final Pattern WILDCARDTAG = Pattern.compile("\\<wildcard\\>");

    private Syntax syntax;
    private String pattern;
    private Pattern regex;

    public PathPattern(Syntax syntax, String pattern) {
        Pattern regex;
        if(syntax.equals(Syntax.GLOB)) {
            regex = glob2Pattern(pattern);
        } else {
            regex = Pattern.compile(pattern);
        }
        this.syntax = syntax;
        this.pattern = pattern;
        this.regex = regex;
    }

    public Syntax syntax() {
        return this.syntax;
    }

    public String pattern() {
        return this.pattern;
    }

    public static PathPattern compile(Syntax syntax, String pattern) {
        return new PathPattern(syntax, pattern);
    }

    public static PathPattern compile(String pathSpec) {
        String[] tokens = pathSpec.split(":");
        Syntax syntax = Syntax.valueOf(tokens[0]);
        String pattern = tokens[1];

        return compile(syntax, pattern);
    }

    private Pattern glob2Pattern(String pattern) {
        String regex = pattern;

        regex = DOT.matcher(regex).replaceAll("\\\\.");
        regex = WILDWILDCARD.matcher(regex).replaceAll(".<wildcard>");
        regex = WILDCARD.matcher(regex).replaceAll("[^/]<wildcard>");

        Matcher matcher = SUBPATTERNS.matcher(regex);
        if(matcher.find()) {
            String found = matcher.group(1);
            found = found.replace(',', '|');
            regex = matcher.replaceFirst("(" + found + ")");
        }

        regex = WILDCARDTAG.matcher(regex).replaceAll("*");

        regex = regex.replace("//", "/");
        regex = "^" + regex + "(/|$)";


        return Pattern.compile(regex);
    }

    public Matcher matcher(String pathInfo) {
        return regex.matcher(pathInfo);
    }

    public enum Syntax {
        GLOB("glob"), REGEX("regex");

        private final String name;

        Syntax(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString(){
            return name();
        }
    }
}
