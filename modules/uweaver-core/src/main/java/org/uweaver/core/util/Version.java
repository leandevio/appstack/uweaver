package org.uweaver.core.util;

public class Version {
    private String major = "0";
    private String minor = "0";
    private String build = "0";

    public Version() {}

    public Version(String version) {
        if(version==null) return;
        String[] tokens = version.split("\\.");
        setMajor(tokens[0]);
        if(tokens.length>1) setMinor(tokens[1]);
        if(tokens.length>2) setBuild(tokens[2]);
    }

    public String major() {
        return getMajor();
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String minor() {
        return getMinor();
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String build() {
        return getBuild();
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }
}
