/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import java.util.HashMap;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * XMap obj = new XMap();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class XMap<K, V> extends HashMap<K, V> {
    private boolean ci = true;

    public XMap() {}

    public XMap(boolean ci) {
        this.ci = ci;
    }

    @Override
    public V put(K key, V value) {
        K key2 = (ci && key instanceof String) ? (K) ((String) key).toLowerCase() : key;
        return super.put(key2, value);
    }

    @Override
    public V get(Object key) {
        Object key2 = (ci && key instanceof String) ? ((String) key).toLowerCase() : key;
        return super.get(key2);
    }

    @Override
    public V remove(Object key) {
        Object key2 = (ci && key instanceof String) ? ((String) key).toLowerCase() : key;
        return super.remove(key2);
    }

    @Override
    public boolean containsKey(Object key) {
        return super.containsKey(key.toString().toLowerCase());
    }

}