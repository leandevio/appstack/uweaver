package org.uweaver.scheduling;

import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

public class CronExpression {
    private org.quartz.CronExpression cronExpression;

    public CronExpression(String cronExpression) throws ParseException {
        this.cronExpression = new org.quartz.CronExpression(cronExpression);
    }

    public String expression() {
        return cronExpression.getCronExpression();
    }

    public TimeZone timeZone() {
        return cronExpression.getTimeZone();
    }

    public void setTimeZone(TimeZone timeZone) {
        this.cronExpression.setTimeZone(timeZone);
    }

    public Date nextValidTimeAfter(Date date) {
        return cronExpression.getNextValidTimeAfter(date);
    }
}
