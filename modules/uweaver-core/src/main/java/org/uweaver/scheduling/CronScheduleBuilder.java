package org.uweaver.scheduling;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class CronScheduleBuilder {
    private Class<? extends Job> jobType;
    private String name;
    private String group;
    private String description;
    private Date startTime = new Date();
    private Date endTime;
    private String cronExpression;
    private TimeZone timeZone;
    private Map<String, Object> data = new HashMap<>();
    private int priority = 5;

    public static CronScheduleBuilder newSchedule(String cronExpression) {
        return new CronScheduleBuilder(cronExpression);
    }

    public CronScheduleBuilder(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public CronScheduleBuilder forJob(Class<? extends Job> jobType) {
        this.jobType = jobType;
        return this;
    }

    public CronScheduleBuilder withIdentity(String name, String group) {
        this.name = name;
        this.group = group;
        return this;
    }

    public CronScheduleBuilder withIdentity(String name) {
        return withIdentity(name, null);
    }

    public CronScheduleBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public CronScheduleBuilder startNow() {
        return startAt(new Date());
    }

    public CronScheduleBuilder endAt(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    public CronScheduleBuilder startAt(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    public CronScheduleBuilder inTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
        return this;
    }


    public CronScheduleBuilder withPriority(int priority) {
        this.priority = priority;
        return this;
    }

    public Schedule build() throws ParseException {
        CronSchedule schedule = new CronSchedule(cronExpression);
        schedule.setJobType(jobType);
        schedule.setName(name);
        schedule.setGroup(group);
        schedule.setDescription(description);
        schedule.setTimeZone(timeZone);
        schedule.setPriority(priority);
        schedule.setData(data);
        return schedule;
    }

    public CronScheduleBuilder usingData(String key, Object value) {
        data.put(key, value);
        return this;
    }
}
