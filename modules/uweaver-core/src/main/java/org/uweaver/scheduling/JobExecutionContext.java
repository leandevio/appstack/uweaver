package org.uweaver.scheduling;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;

public class JobExecutionContext {
    private Scheduler scheduler;
    private Schedule schedule;
    private Date fireTime;

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public Scheduler scheduler() {
        return getScheduler();
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public Schedule schedule() {
        return getSchedule();
    }

    public Object getData(String key) {
        return schedule().data().get(key);
    }

    public String getDataAsString(String key) {
        return (String) schedule().data().get(key);
    }

    public Integer getDataAsInteger(String key) {
        return (Integer) schedule().data().get(key);
    }

    public Long getDataAsLong(String key) {
        return (Long) schedule().data().get(key);
    }

    public Double getDataAsDouble(String key) {
        return (Double) schedule().data().get(key);
    }

    public BigInteger getDataAsBigInteger(String key) {
        return (BigInteger) schedule().data().get(key);
    }

    public BigDecimal getDataAsBigDecimal(String key) {
        return (BigDecimal) schedule().data().get(key);
    }

    public Date getDataAsDate(String key) {
        return (Date) schedule().data().get(key);
    }

    public LocalDate getDataAsLocalDate(String key) {
        return (LocalDate) schedule().data().get(key);
    }

    public void setFireTime(Date fireTime) {
        this.fireTime = fireTime;
    }

    public Date getFireTime() {
        return fireTime;
    }

    public Date fireTime() {
        return getFireTime();
    }
}
