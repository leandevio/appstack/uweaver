package org.uweaver.scheduling;

public class JobExecutionException extends Exception {
    public JobExecutionException(String message) {
        super(message);
    }
    public JobExecutionException(Throwable cause) {
        super(cause);
    }
    public JobExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
