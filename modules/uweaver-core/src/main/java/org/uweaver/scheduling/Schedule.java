package org.uweaver.scheduling;

import java.util.Date;
import java.util.Map;

public interface Schedule {
    Class<? extends Job> jobType();

    String name();

    String group();

    String description();

    Date startTime();

    Date endTime();

    int priority();

    Map<String, Object> data();

    int timesTriggered();

    Date previousFireTime();

    Date nextFireTime();
}
