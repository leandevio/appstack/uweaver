package org.uweaver.scheduling;

import org.quartz.*;
import org.quartz.CronScheduleBuilder;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

public class Scheduler {
    private org.quartz.Scheduler scheduler;

    protected Scheduler(org.quartz.Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public Scheduler() throws IOException, SchedulerException {
        StdSchedulerFactory factory = new StdSchedulerFactory();
        String resource = "quartz.properties";
        if(Scheduler.class.getResource(resource)!=null) {
            try(InputStream inputStream = Scheduler.class.getResourceAsStream(resource)) {
                factory.initialize(inputStream);
            } catch (org.quartz.SchedulerException e) {
                throw new SchedulerException(e);
            }
        }

        try {
            this.scheduler = factory.getScheduler();
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public void setJobFactory(JobFactory jobFactory) throws SchedulerException {
        try {
            scheduler.setJobFactory(jobFactory);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public void start() throws SchedulerException {
        try {
            scheduler.start();
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public void shutdown() throws SchedulerException {
        try {
            scheduler.shutdown();
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public Date arrange(Schedule schedule) throws SchedulerException {
        JobDetail job = createJobDetail(schedule);
        Trigger trigger = createTrigger(schedule);
        try {
            return scheduler.scheduleJob(job, trigger);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public Date rearrange(Schedule schedule) throws SchedulerException {
        String name = schedule.name();
        String group = schedule.group();
        TriggerKey triggerKey = new TriggerKey(name, group);
        try {
            scheduler.unscheduleJob(triggerKey);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
        return arrange(schedule);
    }

    public Schedule get(String name, String group) throws SchedulerException {
        Schedule schedule;
        TriggerKey triggerKey = new TriggerKey(name, group);
        try {
            Trigger trigger = scheduler.getTrigger(triggerKey);
            if(trigger==null) return null;
            schedule = createSchedule(trigger);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
        return schedule;
    }

    public Schedule get(String name) throws SchedulerException {
        return get(name, null);
    }

    public boolean cancel(String name, String group) throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(name, group);
        try {
            return scheduler.unscheduleJob(triggerKey);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public boolean cancel(String name) throws SchedulerException {
        return cancel(name, null);
    }

    public void suspend(String name, String group) throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(name, group);
        try {
            scheduler.pauseTrigger(triggerKey);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public void suspend(String name) throws SchedulerException {
        suspend(name, null);
    }

    public void resume(String name, String group) throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(name, group);
        try {
            scheduler.resumeTrigger(triggerKey);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public void resume(String name) throws SchedulerException {
        resume(name, null);
    }

    public void trigger(String name, String group) throws SchedulerException {
        JobKey jobKey = new JobKey(name, group);
        try {
            scheduler.triggerJob(jobKey);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        }
    }

    public void trigger(String name) throws SchedulerException {
        trigger(name, null);
    }

    private Trigger createTrigger(Schedule schedule) throws SchedulerException {
        if(schedule instanceof SimpleSchedule) {
            return createTrigger((SimpleSchedule) schedule);
        } else if(schedule instanceof CronSchedule) {
            return createTrigger((CronSchedule) schedule);
        } else {
            return null;
        }
    }

    private JobDetail createJobDetail(Schedule schedule) throws SchedulerException {
        Class jobType = schedule.jobType();
        String name = schedule.name();
        String description = schedule.description();

        JobDetail job;
        try {
            job = JobBuilder.newJob().ofType(JobWrapper.class)
                    .withIdentity("~" + name)
                    .withDescription(description)
                    .usingJobData("jobType", jobType.getName())
                    .build();
        } catch(Exception e) {
            throw new SchedulerException(e);
        }


        return job;
    }

    private SimpleTrigger createTrigger(SimpleSchedule schedule) throws SchedulerException {
        String name = schedule.name();
        String group = schedule.group();
        String description = schedule.description();
        Date startTime = schedule.startTime();
        Date endTime = schedule.endTime();
        int priority = schedule.priority();
        long interval = schedule.interval();
        int repeatCount = schedule.repeatCount();
        JobDataMap jobDataMap = new JobDataMap();

        for(String key : schedule.data().keySet()) {
            Object value = schedule.data().get(key);
            jobDataMap.put(key, value);
        }

        SimpleTrigger trigger;
        try {
            trigger = TriggerBuilder.newTrigger()
                    .withIdentity(name, group)
                    .withDescription(description)
                    .startAt(startTime)
                    .endAt(endTime)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInMilliseconds(interval)
                            .withRepeatCount(repeatCount))
                    .withPriority(priority)
                    .usingJobData(jobDataMap)
                    .build();
        } catch(Exception e) {
            throw new SchedulerException(e);
        }

        return trigger;
    }


    private CronTrigger createTrigger(CronSchedule schedule) throws SchedulerException {
        String name = schedule.name();
        String group = schedule.group();
        String description = schedule.description();
        Date startTime = schedule.startTime();
        Date endTime = schedule.endTime();
        int priority = schedule.priority();
        JobDataMap jobDataMap = new JobDataMap();
        CronExpression cronExpression = schedule.cronExpression();

        for(String key : schedule.data().keySet()) {
            Object value = schedule.data().get(key);
            jobDataMap.put(key, value);
        }

        CronTrigger trigger;
        try {
            trigger = TriggerBuilder.newTrigger()
                    .withIdentity(name, group)
                    .withDescription(description)
                    .startAt(startTime)
                    .endAt(endTime)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression.expression()).inTimeZone(cronExpression.timeZone()))
                    .withPriority(priority)
                    .usingJobData(jobDataMap)
                    .build();
        } catch(Exception e) {
            throw new SchedulerException(e);
        }

        return trigger;
    }

    protected Schedule createSchedule(Trigger trigger) throws SchedulerException {
        Class triggerType = trigger.getClass();
        try {
            if(SimpleTrigger.class.isAssignableFrom(triggerType)) {
                return createSimpleSchedule((SimpleTrigger) trigger);
            } else if(CronTrigger.class.isAssignableFrom(triggerType)) {
                return createCronSchedule((CronTrigger) trigger);
            } else {
                throw new SchedulerException(String.format("Unable to create a compatible schedule from the trigger(%s).", triggerType.getName()));
            }
        } catch(SchedulerException e) {
            throw e;
        } catch (ParseException e) {
            throw new SchedulerException(e);
        }

    }

    protected SimpleSchedule createSimpleSchedule(SimpleTrigger trigger) throws SchedulerException {
        String jobTypeName;
        Class<? extends Job> jobType;
        try {
            jobTypeName = scheduler.getJobDetail(trigger.getJobKey()).getJobDataMap().getString("jobType");
            jobType = (Class<? extends Job>) Class.forName(jobTypeName);
        } catch (org.quartz.SchedulerException | ClassNotFoundException e) {
            throw new SchedulerException(e);
        }
        SimpleSchedule schedule = new SimpleSchedule();
        schedule.setJobType(jobType);
        schedule.setName(trigger.getKey().getName());
        schedule.setGroup(trigger.getKey().getGroup());
        schedule.setDescription(trigger.getDescription());
        schedule.setStartTime(trigger.getStartTime());
        schedule.setEndTime(trigger.getEndTime());
        schedule.setPriority(trigger.getPriority());
        schedule.setInterval(trigger.getRepeatInterval());
        schedule.setRepeatCount(trigger.getRepeatCount());
        schedule.setData(trigger.getJobDataMap());

        schedule.setTimesTriggered(trigger.getTimesTriggered());
        schedule.setPreviousFireTime(trigger.getPreviousFireTime());
        schedule.setNextFireTime(trigger.getNextFireTime());

        return schedule;
    }

    protected CronSchedule createCronSchedule(CronTrigger trigger) throws SchedulerException, ParseException {
        String jobTypeName;
        Class<? extends Job> jobType;
        try {
            jobTypeName = scheduler.getJobDetail(trigger.getJobKey()).getJobDataMap().getString("jobType");
            jobType = (Class<? extends Job>) Class.forName(jobTypeName);
        } catch (org.quartz.SchedulerException e) {
            throw new SchedulerException(e);
        } catch (ClassNotFoundException e) {
            throw new SchedulerException(e);
        }
        CronSchedule schedule = new CronSchedule(trigger.getCronExpression());
        schedule.setJobType(jobType);
        schedule.setName(trigger.getKey().getName());
        schedule.setGroup(trigger.getKey().getGroup());
        schedule.setDescription(trigger.getDescription());
        schedule.setStartTime(trigger.getStartTime());
        schedule.setEndTime(trigger.getEndTime());
        schedule.setPriority(trigger.getPriority());
        schedule.setData(trigger.getJobDataMap());

        schedule.setTimesTriggered(-1);
        schedule.setPreviousFireTime(trigger.getPreviousFireTime());
        schedule.setNextFireTime(trigger.getNextFireTime());

        return schedule;
    }
}
