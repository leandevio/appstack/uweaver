package org.uweaver.scheduling;

public class SchedulerException extends Exception {
    public SchedulerException(String message) {
        super(message);
    }
    public SchedulerException(Throwable cause) {
        super(cause);
    }
    public SchedulerException(String message, Throwable cause) {
        super(message, cause);
    }
}
