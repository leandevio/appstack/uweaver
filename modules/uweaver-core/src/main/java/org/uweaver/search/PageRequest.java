/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.search;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PageRequest obj = new PageRequest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PageRequest implements Pageable {
    private static int MAXPAGESIZE = Integer.MAX_VALUE;
    private int page;
    private int size;

    public PageRequest(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public PageRequest(int page) {
        this(page, MAXPAGESIZE);
    }

    public PageRequest() {
        this(0, MAXPAGESIZE);
    }

    @Override
    public int page() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public long offset() {
        return page * size;
    }

    @Override
    public Pageable next() {
        return new PageRequest(this.page + 1, this.size);
    }

    @Override
    public Pageable move(int page) {
        return new PageRequest(page, this.size);
    }

    public void setSize(int size) {
        this.size = size;
    }
}
