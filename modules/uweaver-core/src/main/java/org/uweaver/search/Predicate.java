/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.search;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Predicate obj = new Predicate();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Predicate {
    String subject;
    Comparator comparator;
    Object expectation;

    public Predicate(String subject, Object expectation) {
        this(subject, expectation, Comparator.EQ);
    }

    public Predicate(String subject, Object expectation, String comparator) {
        this(subject, expectation, Comparator.valueOf(comparator.toUpperCase()));
    }

    public Predicate(String subject, Object expectation, Comparator comparator) {
        this.subject = subject;
        this.comparator = comparator;
        this.expectation = expectation;
    }

    public String subject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Comparator comparator() {
        return comparator;
    }

    public void setComparator(Comparator comparator) {
        this.comparator = comparator;
    }

    public Object expectation() {
        return expectation;
    }

    public void setExpectation(Object expectation) {
        this.expectation = expectation;
    }

    public enum Comparator {
        LIKE("LIKE"), EQ("EQ"), LT("LT"), LE("LE"), GT("GT"), GE("GE"), IN("IN");

        private final String value;

        Comparator(String value){
            this.value = value;
        }

        public String value(){
            return value;
        }

        public String toString(){
            return value;
        }
    }
}
