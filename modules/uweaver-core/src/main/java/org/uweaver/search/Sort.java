/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.search;

/**
 * *
 * The implementation provides:
 * - ...
 * - ...
 * <p>
 * Usage:
 * <p>
 * ```java
 * Sort obj = new Sort();
 * ...
 * ```
 * <p>
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Sort {
    private String property;
    private Direction direction;

    public Sort() {
        this.property = null;
        this.direction = Direction.ASC;
    }

    public Sort(String property, Direction direction) {
        this.property = property;
        this.direction = direction;
    }

    public String property() {
        return this.property;
    }

    public Direction direction() {
        return this.direction;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public enum Direction {
        ASC("ASC"), DESC("DESC");

        private final String value;

        Direction(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

        public String toString(){
            return value;
        }
    }
}
