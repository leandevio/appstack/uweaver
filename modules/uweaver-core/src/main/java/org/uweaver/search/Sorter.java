/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.search;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Sorter obj = new Sorter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Sorter {
    private List<Sort> sorts = new ArrayList<>();

    public Sorter() {}

    public Sorter(Sort... sorts) {
        by(sorts);
    }

    public Sorter by(Sort... sorts) {
        StringBuilder index = new StringBuilder("|");
        for(Sort sort : sorts) {
            if(sort.property()==null) continue;
            index.append(sort.property()).append("|");
        }
        this.sorts.removeIf(sort -> (index.indexOf("|" + sort.property() + "|") != -1));
        for(Sort sort : sorts) {
            if(sort.property()==null) continue;
            this.sorts.add(sort);
        }
        return this;
    }

    public Sorter by(String... properties) {
        return by(Sort.Direction.ASC, properties);
    }

    public Sorter by(Sort.Direction direction, String... properties) {
        List<String> props = Arrays.asList(properties);
        this.sorts.removeIf(sort -> (props.contains(sort.property())));
        for(String property : properties) {
            sorts.add(new Sort(property, direction));
        }
        return this;
    }

    public int size() {
        return this.sorts.size();
    }

    public List<? extends Sort> sorts() {
        List<Sort> sorts = new ArrayList<>();
        sorts.addAll(this.sorts);
        return sorts;
    }

}
