package org.uweaver.ui;

public enum Position {
    MIDDLE_CENTER("MIDDLE_CENTER"), MIDDLE_LEFT("MIDDLE_LEFT"), MIDDLE_RIGHT("MIDDLE_RIGHT"),
    TOP_CENTER("TOP_CENTER"), TOP_LEFT("TOP_LEFT"), TOP_RIGHT("TOP_RIGHT"),
    BOTTOM_CENTER("BOTTOM_CENTER"), BOTTOM_LEFT("BOTTOM_LEFT"), BOTTOM_RIGHT("BOTTOM_RIGHT");
    private final String value;

    Position(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }
}
