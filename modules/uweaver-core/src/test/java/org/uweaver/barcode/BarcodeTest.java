package org.uweaver.barcode;

import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.MediaType;

import java.io.*;

import static org.testng.Assert.assertEquals;

public class BarcodeTest {

    Environment environment = Environment.getDefaultInstance();

    @Test
    public void testWrite() throws Exception {
        File file = environment.tmpFolder().resolve("barcode.png").toFile();
        OutputStream out = new FileOutputStream(file);

        Barcode barcode = new Barcode("1485-383-ABC", Barcode.Format.QR_CODE, 100, 100);
        BarcodeWriter writer = new BarcodeWriter();
        writer.write(barcode, MediaType.IMAGE_PNG, out);

        out.close();
    }

    @Test
    public void testRead() throws Exception {
        File file = environment.tmpFolder().resolve("barcode.png").toFile();
        InputStream in = new FileInputStream(file);
        BarcodeReader reader = new BarcodeReader();

        String text = reader.read(in);

        assertEquals(text, "1485-383-ABC");

        in.close();

    }
}