package org.uweaver.chart;

import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class ChartsTest {
    @Test
    public void testDistribute() throws Exception {

        Extents testItems = new Extents();

        testItems.add(new TestItem("g/dL", 80.5, 20.5));
        testItems.add(new TestItem("mmo/L", 1000, 1));
        testItems.add(new TestItem("mmo/L", 180, 1));
        testItems.add(new TestItem("mmo/L", 250, 1));
        testItems.add(new TestItem("mmo/L", 1500, 1));
        testItems.add(new TestItem("U/L", 70, 1));
        testItems.add(new TestItem("μg/dL", 20.2, 5.4));
        testItems.add(new TestItem("mg/dL", 10.1, 2.3));

        List<Extents> extents = Charts.distribute(testItems, 4);
        assertEquals(extents.size(), 5);

        extents =  Charts.distribute(testItems, 5);
        assertEquals(extents.size(), 5);

        extents =  Charts.distribute(testItems, 6);
        assertEquals(extents.size(), 6);
    }
}