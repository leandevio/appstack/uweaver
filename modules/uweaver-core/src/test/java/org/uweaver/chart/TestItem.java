/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.chart;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TestItem obj = new TestItem();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TestItem implements Extent {

    private String unit;
    private double max = 100;
    private double min = 0;

    public TestItem(String unit, double max, double min) {
        this.unit = unit;
        this.max = max;
        this.min = min;
    }

    @Override
    public String unit() {
        return this.unit;
    }

    @Override
    public double max() {
        return this.max;
    }

    @Override
    public double min() {
        return this.min;
    }
}
