package org.uweaver.core.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.reflection.Reflections;

import java.util.List;

/**
 * Created by jasonlin on 8/31/16.
 */
public class AppTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppTest.class);
//    @Test
//    public void testStartReadyStop() throws Exception {
//        App app = App.getDefaultInstance();
//
//        app.subscribe(StartEvent.class, new Action1<ServerEvent>() {
//            @Override
//            public void call(ServerEvent event) {
//                LOGGER.debug(event.toString());
//            }
//        });
//
//        app.start();
//        app.ready();
//        app.stop();
//    }

    @Test
    public void testAppListener() throws Exception {
        App app = App.getDefaultInstance();

        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(AppListener.class).list();

        for(Class type : types) {
            app.subscribe(type.newInstance());
        }

        app.start();
        app.ready();
        app.stop();
    }

}