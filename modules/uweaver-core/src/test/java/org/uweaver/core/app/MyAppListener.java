/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.event.server.ReadyEvent;
import org.uweaver.core.event.server.StartEvent;
import org.uweaver.core.event.server.StopEvent;
import org.uweaver.core.observer.annotation.Subscribe;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DAppListener obj = new DAppListener();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@AppListener
public class MyAppListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyAppListener.class);
    @Subscribe
    public void onStart(StartEvent event) {
        LOGGER.debug(event.toString());

    }
    @Subscribe
    public void onReady(ReadyEvent event) {
        LOGGER.debug(event.toString());

    }
    @Subscribe
    public void onStop(StopEvent event) {
        LOGGER.debug(event.toString());

    }
}
