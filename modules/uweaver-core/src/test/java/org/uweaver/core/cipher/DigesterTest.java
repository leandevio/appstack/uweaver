package org.uweaver.core.cipher;

import org.testng.annotations.Test;

/**
 * Created by jasonlin on 2/21/14.
 */
public class DigesterTest {
    @Test
    public void testVerify() throws Exception {
        String message = "This is a secret";
        Digester digester = new Digester(Digester.Algorithm.MD5);
        String hash = digester.digest(message);
        assert(digester.verify(message, hash));

        digester = new Digester(Digester.Algorithm.SHA1);
        hash = digester.digest(message);
        assert(digester.verify(message, hash));

        digester = new Digester(Digester.Algorithm.SHA256);
        hash = digester.digest(message);
        assert(digester.verify(message, hash));
    }
}
