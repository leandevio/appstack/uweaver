package org.uweaver.core.cipher;

import org.apache.commons.codec.binary.Base64;
import org.testng.annotations.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.util.Arrays;

import static org.testng.Assert.assertEquals;

/**
 * Created by jasonlin on 2/21/14.
 */
public class EncryptorTest {


    @Test
    public void testCipyerDecipher() throws Exception {
        String message = "This is my secret";
        Encryptor encryptor = new Encryptor(Encryptor.Algorithm.AES, "mykey");
        String cypher = encryptor.cipher(message);
        assertEquals(message, encryptor.decipher(cypher));

        encryptor = new Encryptor(Encryptor.Algorithm.DES, "mykey");
        cypher = encryptor.cipher(message);
        assertEquals(message, encryptor.decipher(cypher));

    }

    @Test
    public void testDecode() throws Exception {
        String key = "Az34";
        String username = "60001";
        String credential = "T0fk2gWQ46WNWAEOM/CeCg==";

        byte[] keyInBytes = new byte[16];
        Arrays.fill(keyInBytes, (byte) 0);
        System.arraycopy(key.getBytes(), 0, keyInBytes, 0, Math.min(keyInBytes.length, key.getBytes().length));
        SecretKeySpec secretKey = new SecretKeySpec(keyInBytes, "AES");

        Cipher decoder = Cipher.getInstance("AES/ECB/PKCS5Padding");
        decoder.init(Cipher.DECRYPT_MODE, secretKey);

        byte[] value = Base64.decodeBase64(credential);
        byte[] theUsernameInBytes = decoder.doFinal(value);

        String theUsername = new String(theUsernameInBytes);

        assertEquals(username, theUsername);
    }

    @Test
    public void testEncode() throws Exception {
        String key = "Az34";
        String username = "60001";
        String credential = "T0fk2gWQ46WNWAEOM/CeCg==";

        byte[] keyInBytes = new byte[16];
        Arrays.fill(keyInBytes, (byte) 0);
        System.arraycopy(key.getBytes(), 0, keyInBytes, 0, Math.min(keyInBytes.length, key.getBytes().length));
        SecretKeySpec secretKey = new SecretKeySpec(keyInBytes, "AES");

        Cipher encoder = Cipher.getInstance("AES/ECB/PKCS5Padding");
        encoder.init(Cipher.ENCRYPT_MODE, secretKey);

        byte[] theCredentialInBytes = encoder.doFinal(username.getBytes());

        String theCredential = Base64.encodeBase64String(theCredentialInBytes);

        assertEquals(credential, theCredential);
    }
}
