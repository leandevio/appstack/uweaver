package org.uweaver.core.compress;

import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.io.InputStream;
import java.util.zip.ZipEntry;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 6/22/16.
 */
public class ZipUnzipTest {
    Environment environment = Environment.getDefaultInstance();

    @Test
    public void testZip() throws Exception {

        /**
         * @todo environment folder problem
         */
        File zipFile = new File(environment.tmpFolder() + File.separator + "files.zip");
        Zip zip = new Zip(zipFile);

        InputStream in = this.getClass().getClassLoader().getResourceAsStream("org/uweaver/core/compress/order.json");
        ZipEntry zipEntry = zip.addNewEntry("order.json");
        zip.compress(in);
        in.close();

        in = this.getClass().getClassLoader().getResourceAsStream("org/uweaver/core/compress/leandev.png");
        zipEntry = zip.addNewEntry("logo/leandev.png");
        zip.compress(in);
        in.close();
    }

    @Test
    public void testUnzip() throws Exception {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("org/uweaver/core/compress/files.zip");
        Unzip unzip = new Unzip(in);

        /**
         * @todo environment folder problem
         */
        String outputFolder = environment.tmpFolder().toString();

        ZipEntry zipEntry = unzip.nextEntry();

        while(zipEntry!=null){
            String fileName = zipEntry.getName();
            File file = new File(outputFolder + File.separator + fileName);
            new File(file.getParent()).mkdirs();
            file.delete();

            unzip.extract(file);

            zipEntry = unzip.nextEntry();
        }

        unzip.close();

    }


}