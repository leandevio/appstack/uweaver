package org.uweaver.core.concurrent;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.exception.LockException;
import org.uweaver.core.exception.TimeoutException;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 6/3/14.
 */
public class LockManagerTest {

    private LockManager lockManager;
    private final int TTL = 15;

    @BeforeTest
    public void setUp() throws Exception {
        lockManager = new LockManager(TTL);
    }

    @Test
    public void testLock() throws Exception {
        Product product = new Product("apple", 3.2);
        lockManager.lock(product.name);
        assertTrue(lockManager.isLocked(product.name));
        assertFalse(lockManager.isAvailable(product.name));

        try {
            lockManager.lock(product.name);
        } catch(Exception e) {
            assertTrue(e instanceof LockException);
        }
    }

    @Test(dependsOnMethods={"testLock"})
    public void testUnlock() throws Exception {
        Product product = new Product("apple", 3.2);
        lockManager.unlock(product.name);
        assertFalse(lockManager.isLocked(product.name));
    }


    @Test(dependsOnMethods={"testUnlock"})
    public void testExtend() throws Exception {
        Product product = new Product("apple", 3.2);

        try {
            lockManager.extend(product.name);
        } catch(Exception e) {
            assertTrue(e instanceof LockException);
        }

        lockManager.lock(product.name);
        Thread.sleep((TTL * 2 / 3) * 1000);
        assertTrue(lockManager.isLocked(product.name));

        lockManager.extend(product.name);
        Thread.sleep((TTL * 2 / 3) * 1000);
        assertTrue(lockManager.isLocked(product.name));
        Thread.sleep((TTL * 2 / 3) * 1000);
        assertFalse(lockManager.isLocked(product.name));

        try {
            lockManager.extend(product.name);
        } catch(Exception e) {
            assertTrue(e instanceof TimeoutException || e instanceof LockException);
        }

        lockManager.lock(product.name);
        assertTrue(lockManager.isLocked(product.name));
    }

    @AfterTest
    public void tearDown() {
        lockManager.shutdown();
    }

    private class Product {
        public String name;
        public double price;

        public Product(String name, double price) {
            this.name = name;
            this.price = price;
        }
    }

}