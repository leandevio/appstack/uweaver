/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.DateTimeConverter;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.MediaType;

import java.awt.*;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PPTXFileTest obj = new PPTXFileTest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PptxFileTest {
    private Environment environment = Environment.getDefaultInstance();
    private JSONParser jsonParser = new JSONParser();
    private DateTimeConverter dateTimeConverter = new DateTimeConverter();

    @BeforeTest
    public void setUp() throws Exception {
        dateTimeConverter.setDateTimePattern("yyyy/MM/dd");
    }


    @Test
    public void testCreateSlidesUsingTemplate() throws Exception {

        InputStream issueStatData = this.getClass().getClassLoader().getResourceAsStream("org/uweaver/core/doc/issueStat.json");
        List<Map<String, Object>> issueStat = jsonParser.readValue(issueStatData, List.class);

        InputStream template = this.getClass().getClassLoader().getResourceAsStream("org/uweaver/core/doc/weeklyIssueReportTpl.pptx");

        PptxFile pptx = new PptxFile(template);

        // create 1st slide.
        Slide slide = pptx.createSlide("Table & Picture");

        Placeholder titlePlaceholder = slide.getPlaceholder("TITLE");

        titlePlaceholder.setText("Submit & Close Trend");

        // add one picture.
        Placeholder chartPlaceholder = slide.getPlaceholder("PICTURE");
        InputStream chartData = this.getClass().getClassLoader().getResourceAsStream("org/uweaver/core/doc/submitCloseTrend.png");
        Image chart = slide.createImage(chartData, MediaType.IMAGE_PNG);
        chartPlaceholder.setContent(chart);

        // add one table.
        Placeholder tablePlaceholder = slide.getPlaceholder("TABLE");
        Table table = slide.createTable(7, 4);
        table.setColumnWidth(0, 200);
        table.setFontSize(12d).setTextAlign(TextAlign.CENTER)
                .setBorderWidth(Border.Edge.BOTTOM, 1.0).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
        TableRow header = table.getRow(0);
        header.getCell(0).setText("Date");
        header.getCell(1).setText("Week");
        header.getCell(2).setText("Submit");
        header.getCell(3).setText("Close");

        for(int i=1; i<table.getRows().size(); i++) {
            TableRow row = table.getRow(i);
            row.getCell(0).setText(dateTimeConverter.convert(issueStat.get(i-1).get("date"), String.class));
            row.getCell(1).setText(issueStat.get(i-1).get("week").toString());
            row.getCell(2).setText(issueStat.get(i-1).get("submit").toString());
            row.getCell(3).setText(issueStat.get(i-1).get("close").toString());
        }
        tablePlaceholder.setContent(table);

        // create 2nd slide.
        slide = pptx.createSlide("Table & Table");
        titlePlaceholder = slide.getPlaceholder("TITLE");
        titlePlaceholder.setText("Submit & Close Table");

        // get all placeholders of table.
        List<Placeholder> tablePlaceholders = slide.getPlaceholders("TABLE");

        // add one table.
        table = slide.createTable(7, 3);
        table.setColumnWidth(0, 200);
        table.setFontSize(12d).setTextAlign(TextAlign.CENTER).setFontFamily(new FontFamily("Arial")).setColor(Color.BLUE)
                .setBorderWidth(Border.Edge.BOTTOM, 1.0).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
        header = table.getRow(0);
        header.getCell(0).setText("Date");
        header.getCell(1).setText("Week");
        header.getCell(2).setText("Submit");

        for(int i=1; i<table.getRows().size(); i++) {
            TableRow row = table.getRow(i);
            row.getCell(0).setText(dateTimeConverter.convert(issueStat.get(i-1).get("date"), String.class));
            row.getCell(1).setText(issueStat.get(i-1).get("week").toString());
            row.getCell(2).setText(issueStat.get(i-1).get("submit").toString());
        }
        tablePlaceholders.get(0).setContent(table);

        // add another table
        table = slide.createTable(7, 3);
        table.setColumnWidth(0, 200);
        table.setFontSize(12d).setTextAlign(TextAlign.CENTER)
                .setBorderWidth(Border.Edge.BOTTOM, 1.0).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
        header = table.getRow(0);
        header.getCell(0).setText("Date");
        header.getCell(1).setText("Week");
        header.getCell(2).setText("Close");

        for(int i=1; i<table.getRows().size(); i++) {
            TableRow row = table.getRow(i);
            row.getCell(0).setText(dateTimeConverter.convert(issueStat.get(i-1).get("date"), String.class));
            row.getCell(1).setText(issueStat.get(i-1).get("week").toString());
            row.getCell(2).setText(issueStat.get(i-1).get("close").toString());
        }
        tablePlaceholders.get(1).setContent(table);

        /**
         * @todo environment folder problem
         */
        String filepath = environment.tmpFolder() + File.separator + "Issue Statistic Report.pptx";
        File file = new File(filepath);
        file.delete();
        pptx.write(file);
        template.close();
    }


}
