/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.DateTimeConverter;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.util.Arrays;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * XlsxWorkbookWriterTest obj = new XlsxWorkbookWriterTest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class XlsxWorkbookWriterTest {
    Environment environment = Environment.getDefaultInstance();
    JSONParser jsonParser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    java.util.List<Map> products;
    DateTimeConverter dateTimeConverter = new DateTimeConverter();

    @BeforeTest
    public void setUp() throws Exception {
        dateTimeConverter.setDateTimePattern("yyyy/MM/dd");
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + "products.json");
        products = Arrays.asList(jsonParser.readValue(in, Map[].class));
        in.close();
    }

    @Test
    public void testCreateWorkbook() throws Exception {
        Workbook workbook = new Workbook();

        Sheet sheet = workbook.createSheet("products");
        for(int rownum=1; rownum<=5; rownum++) {
            Map product = products.get(rownum-1);
            SheetRow row = sheet.getRow(rownum);
            row.getCell(1).setValue(product.get("name"));
            row.getCell(2).setValue(product.get("series"));
            row.getCell(3).setValue(product.get("price"));
            row.getCell(4).setValue(product.get("release"));
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookWriter writer = new XlsxWorkbookWriter(out);
        workbook.write(writer);
        byte[] bytes = out.toByteArray();

        // write the bytes to file
        /**
         * @todo environment folder problem
         */
        File output = new File(environment.tmpFolder() + File.separator + "testCreateWorkbook.xlsx");
        output.delete();

        FileOutputStream fos = new FileOutputStream(output);
        fos.write(bytes);
        fos.close();
    }

    @Test
    public void testCreateWorkbookFromTpl() throws Exception {
        InputStream template = clazz.getClassLoader().getResourceAsStream(path + File.separator + "workbookTpl.xlsx");
        WorkbookReader reader = new XlsxWorkbookReader(template);
        Workbook workbook = new Workbook(reader);

        Sheet sheet = workbook.getSheet("products");
        for(int rownum=1; rownum<=5; rownum++) {
            Map product = products.get(rownum-1);
            SheetRow row = sheet.getRow(rownum);
            row.getCell(1).setValue(product.get("name"));
            row.getCell(2).setValue(product.get("series"));
            row.getCell(3).setValue(product.get("price"));
            row.getCell(4).setValue(product.get("release"));
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookWriter writer = new XlsxWorkbookWriter(out);
        workbook.write(writer);
        byte[] bytes = out.toByteArray();

        // write the bytes to file
        /**
         * @todo environment folder problem
         */
        File output = new File(environment.tmpFolder() + File.separator + "testCreateWorkbookFromTpl.xlsx");
        output.delete();

        FileOutputStream fos = new FileOutputStream(output);
        fos.write(bytes);
        fos.close();
    }
}
