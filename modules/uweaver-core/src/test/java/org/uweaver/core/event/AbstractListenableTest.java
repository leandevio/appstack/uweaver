package org.uweaver.core.event;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Model;

import static org.testng.Assert.*;

public class AbstractListenableTest {
    TaskService taskService = new TaskService();
    int counter = 0;

    @BeforeTest
    public void setUp() throws Exception {
        taskService.addEventListener("complete", event -> this.afterComplete((Model) event.data()));
        taskService.addEventListener("add", event -> this.beforeAdd((Model) event.data()));
    }

    private void afterComplete(Model data) {
        assertEquals(data.get("action"), "complete");
        assertEquals(data.get("status"), "completed");
        counter++;
    }


    private void beforeAdd(Model data) {
        assertEquals(data.get("action"), "add");
        assertEquals(data.get("status"), "todo");
        counter++;
    }

    @Test
    public void testTrigger() throws Exception {
        taskService.complete();
        assertEquals(counter, 3);
    }

}