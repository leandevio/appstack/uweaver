package org.uweaver.core.event;

import org.uweaver.core.util.Model;

public class TaskService extends AbstractListenable implements Listenable {
    public void complete() {
        Model data = new Model();
        data.put("status", "completed");
        data.put("action", "complete");
        this.trigger("complete", data);
        data = new Model();
        data.put("status", "todo");
        data.put("action", "add");
        this.trigger("add", data);
        data = new Model();
        data.put("status", "todo");
        data.put("action", "add");
        this.trigger("add", data);
    }
}
