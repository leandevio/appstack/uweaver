package org.uweaver.core.exception;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.*;

public class ApplicationExceptionTest {
    private Exception lastException;

    @BeforeTest
    public void setUp() throws Exception {
    }


    @Test
    public void testWrapException() throws Exception {
        try {
            wrapException();
        } catch (ApplicationException ex) {
            assertEquals(Arrays.toString(ex.getCause().getStackTrace()), Arrays.toString(lastException.getStackTrace()));
            assertEquals(ex.getCause(), lastException);
        }
    }



    private void wrapException() {
        try {
            generateException();
        } catch (ArithmeticException ex) {
            lastException = ex;
            throw new ApplicationException(ex);
        }
    }



    private void generateException() {
        int balance = 4/0;
    }


}