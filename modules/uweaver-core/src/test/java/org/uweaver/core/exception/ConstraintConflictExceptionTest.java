package org.uweaver.core.exception;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
/**
 * Created by jasonlin on 1/27/15.
 */
public class ConstraintConflictExceptionTest {

    @BeforeMethod
    public void setUp() throws Exception {

    }

//    @Test
//    public void testGenerateException() throws Exception {
//        User user = new User("jason.lin", "Jason Lin");
//        Violation violation1 = new Violation("AlphaNumber", "id");
//        Violation violation2 = new Violation("NotNull", "email", "mobile");
//        Object[] parameters = {60};
//        Violation violation3 = new Violation("Max", parameters, "age");
//        try {
//            throw new ConstraintConflictException(user, violation1, violation2, violation3);
//        } catch (ConstraintConflictException ex) {
//            String message = ex.getMessage();
//            assertTrue(message.contains("jason.lin"));
//            assertTrue(message.contains("AlphaNumber"));
//            assertTrue(message.contains("email"));
//        }
//    }

    private class User {
        private String id;
        private String name;
        private String email;
        private String mobile;

        public User(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public String getMobile() {
            return mobile;
        }
    }
}
