package org.uweaver.core.file;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.util.*;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/3/14.
 */

@Test(dependsOnGroups={"CsvDataFileWriterTest"})
public class CsvDataFileReaderTest {

    private String FILENAME = "dataFile.csv";
    private File dataFile;

    private String[] header = {"no", "name", "price", "effectiveDate", "expireDate"};
    private Object[][] records = {{"001", "Beef Burger", new Double(3.8), null, new DateTime(2014, 4, 1, 0, 0, 0)},
            {"002", "Milk Shake", new Double(2.5), new DateTime(2014, 1, 1, 8, 0, 0).toDate(), new DateTime(2014, 3, 16, 0, 0, 0)},
            {"003", "French Fries", new Double(1.5), null, new DateTime(2014, 3, 13, 0, 0, 0)},
            {null, null, null, null, null},
            {"005", "Fish & Chips", new Double(5.4), null, new DateTime(2014, 4, 16, 0, 0, 0)}};
    private Class[] type = {String.class, String.class, Double.class, Date.class, DateTime.class, DateTime.class};
    private String dateTimePattern = "yyyy-MM-dd HH:mm:ss";

    @BeforeTest
    public void setUp() throws Exception {
        /**
         * @todo environment folder problem
         */
        String filepath = Environment.getDefaultInstance().tmpFolder() + File.separator + FILENAME;
        dataFile = new File(filepath);
        dataFile.delete();
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("header", Arrays.asList(header));
        CsvDataFileWriter writer = new CsvDataFileWriter(dataFile, cfg);
        writer.setDateTimePattern(dateTimePattern);
        for(int i=0; i<records.length; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }
        writer.close();
    }

    @Test
    public void testNextRow() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        cfg.put("dateTimePattern", dateTimePattern);
        DataFileReader reader = new CsvDataFileReader(dataFile, cfg);

        for(int i=0; i<records.length; i++) {
            List row = reader.nextRow();
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }
        reader.close();
    }

    @Test
    public void testGetRow() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        DataFileReader reader = new CsvDataFileReader(dataFile, cfg);
        reader.setDateTimePattern(dateTimePattern);

        for(int i=records.length-1; i>=0; i--) {
            List row = reader.getRow(i + 1);
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }
        reader.close();
    }

    @Test
    public void testNextItem() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        DataFileReader reader = new CsvDataFileReader(dataFile, cfg);
        reader.setDateTimePattern(dateTimePattern);

        for(int i=0; i<records.length; i++) {
            Map<String, Object> item = reader.nextItem();
            Object[] record = records[i];
            for(int j=0; j<header.length; j++) {
                String key = header[j];
                assertEquals(item.get(key), record[j]);
            }
        }
        reader.close();
    }
}