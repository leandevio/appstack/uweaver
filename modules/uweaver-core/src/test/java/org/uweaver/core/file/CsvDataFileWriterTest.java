package org.uweaver.core.file;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.*;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/3/14.
 */

@Test(groups="CsvDataFileWriterTest")
public class CsvDataFileWriterTest {
    private Environment environment = Environment.getDefaultInstance();
    private String FILENAME = "dataFile.csv";
    private File dataFile;
    private String[] header = {"no", "name", "price", "effectiveDate", "expireDate"};
    private Object[][] records = {{"001", "Beef Burger", new Double(3.8), null, new DateTime(2014, 4, 1, 0, 0, 0)},
            {"002", "Milk Shake", new Double(2.5), new DateTime(2014, 1, 1, 8, 0, 0).toDate(), new DateTime(2014, 3, 16, 0, 0, 0)},
            {"003", "French Fries", new Double(1.5), null, new DateTime(2014, 3, 13, 0, 0, 0)},
            {null, null, null, null, null},
            {"005", "Fish & Chips", new Double(5.4), null, new DateTime(2014, 4, 16, 0, 0, 0)}};
    private Class[] type = {String.class, String.class, Double.class, Date.class, DateTime.class};
    private String dateTimePattern = "yyyy-MM-dd HH:mm:ss";

    @BeforeMethod
    public void setUp() throws Exception {
        /**
         * @todo environment folder problem
         */
        String filepath = environment.tmpFolder() + File.separator + FILENAME;
        dataFile = new File(filepath);
        dataFile.delete();
    }

    @Test
    public void testWriteRow() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("header", Arrays.asList(header));
        DataFileWriter writer = new CsvDataFileWriter(dataFile, cfg);
        writer.setDateTimePattern(dateTimePattern);
        writer.writeHeader();
        for(int i=0; i<records.length; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }
        writer.close();
        validate();
    }

    @Test
    public void testWriteItem() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("header", Arrays.asList(header));
        cfg.put("dateTimePattern", dateTimePattern);
        DataFileWriter writer = new CsvDataFileWriter(dataFile, cfg);
        writer.writeHeader();
        for(int i=0; i<records.length; i++) {
            Object[] record = records[i];
            Map<String, Object> item = new HashMap<>();
            for(int j = record.length-1; j >=0; j--) {
                item.put(header[j], record[j]);
            }
            writer.writeItem(item);
        }
        writer.close();
        validate();
    }

    @Test
    public void testLargeFile() throws Exception {
        URL resource = CsvDataFileWriterTest.class.getClassLoader().getResource("workExperience.txt");
        File dataFile = new File(resource.toURI());
        /**
         * @todo environment folder problem
         */
        File output = new File(environment.tmpFolder() + File.separator + "WorkExperience.csv");
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("delimiter", '\t');
        cfg.put("quote", '`');
        DataFileReader reader = new CsvDataFileReader(dataFile, cfg);
        cfg.put("delimiter", ',');
        cfg.put("quote", '`');
        DataFileWriter writer = new CsvDataFileWriter(output, cfg);

        List<Object> row = reader.nextRow();
        while(row!=null) {
            writer.writeRow(row);
            row = reader.nextRow();
        }
        reader.close();
        writer.close();
    }

    private void validate() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        DataFileReader reader = new CsvDataFileReader(dataFile, cfg);
        reader.setDateTimePattern(dateTimePattern);

        reader.getHeader().equals(Arrays.asList(header));
        for(int i=0; i<records.length; i++) {
            List row = reader.nextRow();
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }
        reader.close();
    }
}