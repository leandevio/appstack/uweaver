package org.uweaver.core.file;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.search.*;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 3/17/17.
 */
public class FileIndexerTest {
    Environment environment = Environment.getDefaultInstance();
    Path repository = environment.tmpFolder().resolve("repository");
    Path document = repository.resolve("document");
    Path presentation = repository.resolve("presentation");
    Path spreadsheet = repository.resolve("spreadsheet");

    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');

    String[] presentationFiles = {"Call_of_Duty_Advanced_Warfare.pptx", "Hearts_of_Iron_IV.pptx", "MLB_13_The_Show.pptx"
            , "MLB_The_Show_16.pptx", "The_Elder_Scrolls_V_Skyrim.pptx"};
    String[] spreadsheetFiles = {"Call_of_Duty_Advanced_Warfare.xlsx", "Hearts_of_Iron_IV.xlsx", "MLB_13_The_Show.xlsx"
            , "MLB_The_Show_16.xlsx", "The_Elder_Scrolls_V_Skyrim.xlsx"};
    String[] documentFiles = {"Call_of_Duty_Advanced_Warfare.txt", "Hearts_of_Iron_IV.txt", "MLB_13_The_Show.txt"
            , "MLB_The_Show_16.txt", "The_Elder_Scrolls_V_Skyrim.txt"};

    @BeforeTest
    public void beforeTest() throws Exception {
        clear();
        generatePresentation();
        generateDocument();
        generateWorkbook();
        IndexRepository.getDefaultInstance().reset();
    }

    @Test
    public void testIndex() throws Exception {
        FileIndexer indexer = new FileIndexer(repository);

        indexer.index(document);
        indexer.index(presentation);
        indexer.index(spreadsheet);

        IndexRepository indexRepository = IndexRepository.getDefaultInstance();
        IndexCollection documentIndex = indexRepository.getCollection("document");
        IndexCollection presentationIndex = indexRepository.getCollection("presentation");
        IndexCollection spreadsheetIndex = indexRepository.getCollection("spreadsheet");
        assertNull(documentIndex.getUpdateTime());
        assertNull(presentationIndex.getUpdateTime());
        assertNull(spreadsheetIndex.getUpdateTime());

        indexRepository.scan();
        documentIndex = indexRepository.getCollection("document");
        presentationIndex = indexRepository.getCollection("presentation");
        spreadsheetIndex = indexRepository.getCollection("spreadsheet");
        assertNull(documentIndex.getUpdateTime());
        assertNull(presentationIndex.getUpdateTime());
        assertNull(spreadsheetIndex.getUpdateTime());
    }



    @Test(dependsOnMethods = "testIndex")
    public void testBuild() throws Exception {
        FileIndexer indexer = new FileIndexer(repository);

        IndexSearcher searcher = new IndexSearcher(indexer.getIndexRepository());

        IndexQuery query = new IndexQuery();
        String kql = "content:\"職棒\"";

        assertEquals(searcher.count(query.from("presentation").where(kql)), 0);
        assertEquals(searcher.count(query.from("document").where(kql)), 0);
        assertEquals(searcher.count(query.from("spreadsheet").where(kql)), 0);
        indexer.build();
        assertEquals(searcher.count(query.from("presentation").where(kql)), 2);
        assertEquals(searcher.count(query.from("document").where(kql)), 2);
        assertEquals(searcher.count(query.from("spreadsheet").where(kql)), 2);

//        indexer.remove("document", records.get(0));
//        assertEquals(searcher.count(query), 4);

    }

    @Test(dependsOnMethods = "testIndex")
    public void testReindex() throws Exception {
        FileIndexer indexer = new FileIndexer(repository);

        indexer.build("presentation");
        indexer.index(document);

        IndexRepository indexRepository = IndexRepository.getDefaultInstance();
        IndexCollection documentIndex = indexRepository.getCollection("presentation");
        assertNotNull(documentIndex.getUpdateTime());
    }

    private void clear() throws Exception {
        deleteDirectory(repository);
        Files.createDirectory(repository);
        Files.createDirectory(document);
        Files.createDirectory(presentation);
        Files.createDirectory(spreadsheet);
    }

    private void generatePresentation() throws Exception {
        for(String filename : presentationFiles) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();

            Path path = presentation.resolve(filename);
            OutputStream out = new FileOutputStream(path.toFile());

            out.write(bytes);
            out.close();
        }
    }

    private void generateDocument() throws Exception {
        for(String filename : documentFiles) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();

            Path path = document.resolve(filename);
            OutputStream out = new FileOutputStream(path.toFile());

            out.write(bytes);
            out.close();
        }
    }

    private void generateWorkbook() throws Exception {
        for(String filename : spreadsheetFiles) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();

            Path path = spreadsheet.resolve(filename);
            OutputStream out = new FileOutputStream(path.toFile());

            out.write(bytes);
            out.close();
        }
    }

    private void deleteDirectory(Path path) throws IOException {
        if(!path.toFile().exists()) return;
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException e)
                    throws IOException
            {
                if (e == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed
                    throw e;
                }
            }
        });
    }
}