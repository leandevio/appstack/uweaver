package org.uweaver.core.file;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.search.*;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 3/17/17.
 */
public class FileSearcherTest {
    Environment environment = Environment.getDefaultInstance();
    Path repository = environment.tmpFolder().resolve("repository");
    Path document = repository.resolve("document");
    Path presentation = repository.resolve("presentation");
    Path spreadsheet = repository.resolve("spreadsheet");

    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');

    String[] presentationFiles = {"Call_of_Duty_Advanced_Warfare.pptx", "Hearts_of_Iron_IV.pptx", "MLB_13_The_Show.pptx"
            , "MLB_The_Show_16.pptx", "The_Elder_Scrolls_V_Skyrim.pptx"};
    String[] spreadsheetFiles = {"Call_of_Duty_Advanced_Warfare.xlsx", "Hearts_of_Iron_IV.xlsx", "MLB_13_The_Show.xlsx"
            , "MLB_The_Show_16.xlsx", "The_Elder_Scrolls_V_Skyrim.xlsx"};
    String[] documentFiles = {"Call_of_Duty_Advanced_Warfare.txt", "Hearts_of_Iron_IV.txt", "MLB_13_The_Show.txt"
            , "MLB_The_Show_16.txt", "The_Elder_Scrolls_V_Skyrim.txt"};



    @BeforeTest
    public void beforeTest() throws Exception {
        clear();
        generatePresentation();
        generateDocument();
        generateWorkbook();
        IndexRepository.getDefaultInstance().reset();

        FileIndexer indexer = new FileIndexer(repository);
        indexer.index(presentation);
        indexer.index(document);
        indexer.index(spreadsheet);
        indexer.build();
    }

    @Test
    public void testSearchPresentation() throws Exception {
        FileSearcher searcher = new FileSearcher(repository);
        Map<String, Object> predicate = new HashMap<>();
        int limit = 10;
        predicate.put("content", "職棒");
        int count = searcher.count(presentation, predicate);
        Hits hits = searcher.search(presentation, predicate, limit);

        assertEquals(count, 2);
        assertEquals(hits.size(), 2);
        assertEquals(hits.getTotal(), 2);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("path") + "\t" + document.get("filename"));
        }
    }

    @Test
    public void testSearchSpreadsheet() throws Exception {
        FileSearcher searcher = new FileSearcher(repository);
        Map<String, Object> predicate = new HashMap<>();
        int limit = 10;
        predicate.put("content", "職棒");
        int count = searcher.count(spreadsheet, predicate);
        Hits hits = searcher.search(spreadsheet, predicate, limit);

        assertEquals(count, 2);
        assertEquals(hits.size(), 2);
        assertEquals(hits.getTotal(), 2);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("path") + "\t" + document.get("filename"));
        }
    }

    @Test
    public void testSearchDocument() throws Exception {
        FileSearcher searcher = new FileSearcher(repository);
        Map<String, Object> predicate = new HashMap<>();
        int limit = 10;
        predicate.put("content", "職棒");
        int count = searcher.count(document, predicate);
        Hits hits = searcher.search(document, predicate, limit);

        assertEquals(count, 2);
        assertEquals(hits.size(), 2);
        assertEquals(hits.getTotal(), 2);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("path") + "\t" + document.get("filename"));
        }
    }


    @Test
    public void testSearchByKQL() throws Exception {
        FileSearcher searcher = new FileSearcher(repository);
        String kql = "content:\"職棒\"";
        int limit = 10;
        int count = searcher.count(document, kql);
        Hits hits = searcher.search(document, kql, limit);

        assertEquals(count, 2);
        assertEquals(hits.size(), 2);
        assertEquals(hits.getTotal(), 2);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("path") + "\t" + document.get("filename"));
        }
    }

    @Test
    public void testSearchOffset() throws Exception {
        FileSearcher searcher = new FileSearcher(repository);

        int limit = 2, offset = 3;
        int count = searcher.count(presentation);
        Hits hits = searcher.search(presentation, limit, offset);

        assertEquals(count, 5);
        assertEquals(hits.size(), limit);
        assertEquals(hits.getTotal(), 5);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("path") + "\t" + document.get("filename"));
        }
    }

    private void clear() throws Exception {
        deleteDirectory(repository);
        Files.createDirectory(repository);
        Files.createDirectory(document);
        Files.createDirectory(presentation);
        Files.createDirectory(spreadsheet);
    }

    private void generatePresentation() throws Exception {
        for(String filename : presentationFiles) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();

            Path path = presentation.resolve(filename);
            OutputStream out = new FileOutputStream(path.toFile());

            out.write(bytes);
            out.close();
        }
    }

    private void generateDocument() throws Exception {
        for(String filename : documentFiles) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();

            Path path = document.resolve(filename);
            OutputStream out = new FileOutputStream(path.toFile());

            out.write(bytes);
            out.close();
        }
    }

    private void generateWorkbook() throws Exception {
        for(String filename : spreadsheetFiles) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();

            Path path = spreadsheet.resolve(filename);
            OutputStream out = new FileOutputStream(path.toFile());

            out.write(bytes);
            out.close();
        }
    }

    private void deleteDirectory(Path path) throws IOException {
        if(!path.toFile().exists()) return;
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException e)
                    throws IOException
            {
                if (e == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed
                    throw e;
                }
            }
        });
    }
}