package org.uweaver.core.file;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.util.*;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 6/8/15.
 */
public class XlsDataFileReaderTest {
    private String FILENAME = "XlsDataFileReaderTest.xls";
    private String[] anchors = {"Sheet 1", "Sheet 2"};
    private File dataFile;
    private String[] header = {"no", "name", "price", "effectiveDate", "expireDate"};
    private Object[][] records = {{"001", "Beef Burger", new Double(3.8), null, new DateTime(2014, 4, 1, 0, 0, 0)},
            {"002", "Milk Shake", new Double(2.5), new DateTime(2014, 1, 1, 8, 0, 0).toDate(), new DateTime(2014, 3, 16, 0, 0, 0)},
            {"003", "French Fries", new Double(1.5), null, new DateTime(2014, 3, 13, 0, 0, 0)},
            {null, null, null, null, null},
            {"005", "Fish & Chips", new Double(5.4), null, new DateTime(2014, 4, 16, 0, 0, 0)}};
    private Class[] type = {String.class, String.class, Double.class, Date.class, DateTime.class, DateTime.class};
    private String dateTimePattern = "yyyy/MM/dd";

    @BeforeTest
    public void setUp() throws Exception {
        /**
         * @todo environment folder problem
         */
        String filepath = Environment.getDefaultInstance().tmpFolder() + File.separator + FILENAME;
        dataFile = new File(filepath);
        dataFile.delete();

        Map<String, Object> cfg = new HashMap<>();
        cfg.put("header", Arrays.asList(header));
        cfg.put("anchor", anchors[0]);
        DataFileWriter writer = new XlsDataFileWriter(dataFile, cfg);
        writer.setDateTimePattern(dateTimePattern);
        writer.writeHeader();
        for(int i=0; i<records.length; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }

        writer.setAnchor(anchors[1]);
        writer.writeHeader();
        for(int i=0; i<records.length-2; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }

        writer.close();
    }

    @Test
    public void testNextRow() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        cfg.put("anchor", anchors[0]);
        DataFileReader reader = new XlsDataFileReader(dataFile, cfg);

        for(int i=0; i<records.length; i++) {
            List row = reader.nextRow();
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }
        reader.close();
    }

    @Test
    public void testGetRow() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        cfg.put("anchor", anchors[0]);
        DataFileReader reader = new XlsDataFileReader(dataFile, cfg);

        for(int i=records.length-1; i>=0; i--) {
            List row = reader.getRow(i + 1);
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }
        reader.close();
    }

    @Test
    public void testNextItem() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        cfg.put("anchor", anchors[0]);
        DataFileReader reader = new XlsDataFileReader(dataFile, cfg);

        for(int i=0; i<records.length; i++) {
            Map<String, Object> item = reader.nextItem();
            Object[] record = records[i];
            for(int j=0; j<header.length; j++) {
                String key = header[j];
                assertEquals(item.get(key), record[j]);
            }
        }
        reader.close();
    }

    @Test
    public void testAnchors() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        DataFileReader reader = new XlsDataFileReader(dataFile, cfg);

        List<String> list = reader.getAnchors();
        for(int i=0; i<anchors.length; i++) {
            assertEquals(list.get(i), anchors[i]);
        }

        reader.setAnchor(list.get(0));
        assertEquals(reader.size(), records.length);
        reader.setAnchor(list.get(1));
        assertEquals(reader.size(), records.length-2);

        reader.close();
    }

}