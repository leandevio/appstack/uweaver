package org.uweaver.core.file;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.util.*;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/3/14.
 */
@Test(groups="XlsxDataFileWriterTest")
public class XlsxDataFileWriterTest {
    private String FILENAME = "XlsxDataFileWriterTest.xlsx";
    private String[] anchors = {"Sheet 1", "Sheet 2"};
    private File dataFile;
    private String[] header = {"no", "name", "price", "effectiveDate", "expireDate"};
    private Object[][] records = {{"001", "Beef Burger", new Double(3.8), null, new DateTime(2014, 4, 1, 0, 0, 0)},
            {"002", "Milk Shake", new Double(2.5), new DateTime(2014, 1, 1, 8, 0, 0).toDate(), new DateTime(2014, 3, 16, 0, 0, 0)},
            {"003", "French Fries", new Double(1.5), null, new DateTime(2014, 3, 13, 0, 0, 0)},
            {null, null, null, null, null},
            {"005", "Fish & Chips", new Double(5.4), null, new DateTime(2014, 4, 16, 0, 0, 0)}};
    private Class[] type = {String.class, String.class, Double.class, Date.class, DateTime.class, DateTime.class};
    private String dateTimePattern = "yyyy-MM-dd HH:mm";

    @BeforeMethod
    public void setUp() throws Exception {
        /**
         * @todo environment folder problem
         */
        String filepath = Environment.getDefaultInstance().tmpFolder() + File.separator + FILENAME;
        dataFile = new File(filepath);
        dataFile.delete();
    }

    @Test
    public void testWriteRow() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("header", Arrays.asList(header));
        cfg.put("anchor", anchors[0]);
        DataFileWriter writer = new XlsxDataFileWriter(dataFile, cfg);
        writer.setDateTimePattern(dateTimePattern);
        writer.writeHeader();
        for(int i=0; i<records.length; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }

        writer.setAnchor(anchors[1]);
        writer.writeHeader();
        for(int i=0; i<records.length-2; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }

        writer.close();
        validate();
    }

    @Test
    public void testWriteItem() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("header", Arrays.asList(header));
        cfg.put("anchor", anchors[0]);
        cfg.put("dateTimePattern", dateTimePattern);
        DataFileWriter writer = new XlsxDataFileWriter(dataFile, cfg);
        writer.writeHeader();
        for(int i=0; i<records.length; i++) {
            Object[] record = records[i];
            Map<String, Object> item = new HashMap<>();
            for(int j = record.length-1; j >=0; j--) {
                item.put(header[j], record[j]);
            }
            writer.writeItem(item);
        }

        writer.setAnchor(anchors[1]);
        writer.writeHeader();
        for(int i=0; i<records.length-2; i++) {
            List row = Arrays.asList(records[i]);
            writer.writeRow(row);
        }

        writer.close();
        validate();
    }


    private void validate() throws Exception {
        Map<String, Object> cfg = new HashMap<>();
        cfg.put("hasHeader", true);
        cfg.put("type", Arrays.asList(type));
        cfg.put("anchor", anchors[0]);
        DataFileReader reader = new XlsxDataFileReader(dataFile, cfg);

        reader.getHeader().equals(Arrays.asList(header));
        for(int i=0; i<records.length; i++) {
            List row = reader.nextRow();
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }

        reader.setAnchor(anchors[1]);
        for(int i=0; i<records.length-2; i++) {
            List row = reader.nextRow();
            List record = Arrays.asList(records[i]);
            assertTrue(row.equals(record));
        }

        reader.close();
    }

}