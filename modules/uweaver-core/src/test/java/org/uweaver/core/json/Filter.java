package org.uweaver.core.json;

/**
 * Created by jasonlin on 5/26/16.
 */
public class Filter {

    private String property;
    private Operator operator = Operator.EQ;
    private Object value;

    public Filter(){}

    public Filter(String property, String operator, Object value) {
        this.property = property;
        if(operator!=null) this.operator = Operator.valueOf(operator);
        this.value = value;
    }

    public Filter(String property, Object value) {
        this.property = property;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public enum Operator {
        LIKE("like"), EQ("="), LT("<"), LE("<="), GT(">"), GE(">="), IN("in");

        private final String value;

        private Operator(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

        public String toString(){
            return value;
        }
    }
}
