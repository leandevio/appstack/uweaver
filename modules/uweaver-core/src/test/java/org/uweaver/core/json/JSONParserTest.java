package org.uweaver.core.json;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.exception.ConstraintConflictException;
import org.uweaver.core.exception.Violation;

import java.io.InputStream;
import java.util.*;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 4/15/16.
 */
public class JSONParserTest {

    private JSONParser jsonParser;
    private String json;
    private String jsons;

    @BeforeTest
    public void setUp() throws Exception {
        jsonParser = new JSONParser();

        InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/json/order.json");
        byte[] buffer = new byte[in.available()];
        in.read(buffer);
        in.close();
        json = new String(buffer, "UTF-8");

        in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/json/orders.json");
        buffer = new byte[in.available()];
        in.read(buffer);
        in.close();
        jsons = new String(buffer, "UTF-8");


    }


    @Test
    public void testRWValueForBean() throws Exception {
        Order expect = jsonParser.readValue(json, Order.class);
        String str = jsonParser.writeValueAsString(expect);
        Order actual = jsonParser.readValue(str, Order.class);

        assertEquals(actual, expect);
    }

    @Test(dependsOnMethods = "testRWValueForBean")
    public void testRWValueForMap() throws Exception {
        Order expect = jsonParser.readValue(json, Order.class);
        Map order = jsonParser.readValue(json, Map.class);
        assertEquals(order.get("orderDate"), expect.getOrderDate());
        assertEquals(order.get("shipDate"), expect.getShipDate());
        String str = jsonParser.writeValueAsString(order);
        Order actual = jsonParser.readValue(str, Order.class);

        assertEquals(actual, expect);
    }

    @Test
    public void testRWValueForNullOrEmptyBean() throws Exception {
        Order actual = jsonParser.readValue((String) null, Order.class);
        assertNull(actual);

        actual = null;
        assertNull(jsonParser.writeValueAsString(actual));

        actual = jsonParser.readValue("{}", Order.class);
        assertNotNull(actual);

        actual = new Order();
        assertNotNull(jsonParser.writeValueAsString(actual));

    }


    @Test
    public void testRWValueForCollection() throws Exception {
        List<Order> expect = Arrays.asList(jsonParser.readValue(jsons, Order[].class));
        String str = jsonParser.writeValueAsString(expect);
        List<Order> actual = Arrays.asList(jsonParser.readValue(str, Order[].class));

        for(int i=0; i<expect.size(); i++) {
            assertEquals(actual.get(i), expect.get(i));
        }
    }

    @Test
    public void testRWValueForNullOrEmptyCollection() throws Exception {
        Order[] actual = jsonParser.readValue((String) null, Order[].class);
        assertNull(actual);
        actual = jsonParser.readValue("[]", Order[].class);
        assertNotNull(actual);
    }


    @Test
    public void testRWValueForArray() throws Exception {
        Order[] expect = jsonParser.readValue(jsons, Order[].class);
        String str = jsonParser.writeValueAsString(expect);
        Order[] actual = jsonParser.readValue(str, Order[].class);

        for(int i=0; i<expect.length; i++) {
            assertEquals(actual[i], expect[i]);
        }
    }


    @Test
    public void testWriteAsBytes() throws Exception {

    }

    @Test
    public void testRWValueForBeanWithExcludes() throws Exception {
        Order expect = jsonParser.readValue(json, Order.class, "any", "note", "productName");
        String str = jsonParser.writeValueAsString(expect, "any", "note", "productName");
        Order actual = jsonParser.readValue(str, Order.class);

        assertEquals(actual, expect);
    }

    @Test
    public void testRWValueForBeanWithEnum() throws Exception {
        InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/json/filter.json");
        byte[] buffer = new byte[in.available()];
        in.read(buffer);
        in.close();
        String json = new String(buffer, "UTF-8");

        Filter expect = jsonParser.readValue(json, Filter.class);
        String str = jsonParser.writeValueAsString(expect);
        Filter actual = jsonParser.readValue(str, Filter.class);

        assertEquals(actual.getOperator(), expect.getOperator());
        assertEquals(actual.getProperty(), expect.getProperty());
        assertEquals(actual.getValue(), expect.getValue());

    }

    @Test
    public void testRWValueForArrayWithEnum() throws Exception {
        InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/json/filters.json");
        byte[] buffer = new byte[in.available()];
        in.read(buffer);
        in.close();
        String json = new String(buffer, "UTF-8");

        Filter[] expect = jsonParser.readValue(json, Filter[].class);
        String str = jsonParser.writeValueAsString(expect);
        Filter[] actual = jsonParser.readValue(str, Filter[].class);

        for(int i=0; i<expect.length; i++) {
            assertEquals(actual[i].getOperator(), expect[i].getOperator());
            assertEquals(actual[i].getProperty(), expect[i].getProperty());
            assertEquals(actual[i].getValue(), expect[i].getValue());
        }

    }


    @Test
    public void testRWValueForBeanWithSkipProps() throws Exception {
        Order expect = jsonParser.readValue(json, Order.class);
        InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/json/order_skip.json");

        Order actual = jsonParser.readValue(in, Order.class);

        assertEquals(actual, expect);


    }


    @Test
    public void testWriteValueForBeanWithCycling() throws Exception {
        Order order = jsonParser.readValue(json, Order.class);

        for(OrderItem orderItem : order.getOrderItems()) {
            orderItem.setOrder(order);
        }

        String json = jsonParser.writeValueAsString(order);
    }

//    @Test
//    public void testWriteAsStringForToJSON() throws Exception {
//        Order expect = jsonParser.readValue(json, Order.class);
//        Violation violation1 = new Violation("AlphaNumber", "no");
//        Violation violation2 = new Violation("NotNull", "productNo", "productName");
//        Object[] parameters = {100};
//        Violation violation3 = new Violation("Max", parameters, "price");
//        ConstraintConflictException ex = new ConstraintConflictException(expect, violation1, violation2, violation3);
//
//        jsons = jsonParser.writeValueAsString(ex);
//    }



}