/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.json;

import java.util.*;

/**
 * ## Order
 * ### The class implements ... 
 *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Order obj = new Order();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Order {
    private String uuid;
    private String no;
    private Date orderDate;
    private Date shipDate;
    private List<OrderItem> items = new ArrayList<>();
    private String note;
    private Object any;

    private Customer customer;


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public List<OrderItem> getOrderItems() {
        return items;
    }
    public void setOrderItems(List<OrderItem> items) {
        this.items = items;
    }

    public void addOrderItem(OrderItem item) {
        items.add(item);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Object getAny() {
        return any;
    }

    public void setAny(Object any) {
        this.any = any;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return this.no;
    }
/*
    public BigDecimal getAmount() {
        BigDecimal amount = new BigDecimal(0);
        for(OrderItem item : items) {
            BigDecimal quantity = new BigDecimal(item.getQuantity());
            amount = amount.add(item.getUnitPrice().multiply(quantity));
        }

        return amount;
    }
*/
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Order)) return false;
        if(obj==null) return false;

        Order other = (Order) obj;

        return (this.hashCode()==other.hashCode());

    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(getUuid(), getNo(), getOrderDate(), getShipDate(), getAny());

        if(customer!=null) {
            hash = Objects.hash(hash, customer.hashCode());
        }

        for(OrderItem item: getOrderItems()) {
            hash = Objects.hash(hash, item.hashCode());
        }

        return hash;
    }
}
