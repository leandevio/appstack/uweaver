package org.uweaver.core.mail;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jasonlin on 5/12/15.
 */
public class MailSenderTest {

    String from = "notification@talentonline.io";
    String name = "Mail Sender";
    String to = "jasonlin@leandev.io";
    List<File> attachments = new ArrayList<>();

    @BeforeTest
    public void setUp() throws Exception {
        String[] filenames = {"附件-1.txt", "附件-2.txt"};

        for(int i=0; i<filenames.length; i++) {
            /**
             * @todo environment folder problem
             */
            String filepath = Environment.getDefaultInstance().tmpFolder() + File.separator + filenames[i];
            PrintWriter writer = new PrintWriter(filepath, "UTF-8");
            writer.println("The first line");
            writer.println("The second line");
            writer.close();
            attachments.add(new File(filepath));
        }
    }

    @Test
    public void testSendText() throws Exception {
        MailSender mailSender = new MailSender();

        Mail email = new Mail();

        email.setFrom(from, name);
        email.setTo(to);
        email.setSubject("MailSender 測試郵件");
        email.setText("您好：\n這封信沒有附件．");
        email.setSentDate(new Date());
        email.setSender(mailSender);

        email.send();

    }

    @Test
    public void testSendHTML() throws Exception {
        MailSender mailSender = new MailSender();

        HtmlMail email = new HtmlMail();

        email.setFrom(from, name);
        email.setTo(to);
        email.setSubject("MailSender 測試郵件");
        email.setContent("<span'>您好：</span><p>" +
                "<span style='color:blue'>這段文字是藍色的．</span><p>" +
                "<span>這封信沒有附件．</span>");
        email.setText("這段文字是給無法支援HTML格式的郵件閱讀器顯示的．");
        email.setSentDate(new Date());
        email.setSender(mailSender);

        email.send();
    }



    @Test
    public void testSendAttachement() throws Exception {
        MailSender mailSender = new MailSender();

        HtmlMail email = new HtmlMail();

        email.setFrom(from, name);
        email.setTo(to);
        email.setSubject("MailSender 測試郵件");
        email.setContent("<span'>您好：</span><p>" +
                "<span style='color:blue'>這段文字是藍色的．</span><p>" +
                "<span>這封信有兩個附件．</span>");
        email.setText("這段文字是給無法支援HTML格式的郵件閱讀器顯示的．");
        email.setSentDate(new Date());
        email.setSender(mailSender);
        email.setAttachments(attachments);

        email.send();
    }

    @Test
    public void testSendMultiple() throws Exception {
        MailSender mailSender = new MailSender();

        Mail email = new Mail();

        email.setFrom(from, name);
        email.setTo(to);
        email.setSubject("MailSender 測試郵件");
        email.setText("您好：\n這是兩封信件的第一封．");
        email.setSentDate(new Date());
        email.setSender(mailSender);

        HtmlMail htmlEmail = new HtmlMail();

        htmlEmail.setFrom(from, name);
        htmlEmail.setTo(to);
        htmlEmail.setSubject("MailSender 測試郵件");
        htmlEmail.setContent("<span'>您好：</span><p>" +
                "<span style='color:blue'>這是兩封信件的第二封．</span>");
        htmlEmail.setText("這段文字是給無法支援HTML格式的郵件閱讀器顯示的．");
        htmlEmail.setSentDate(new Date());
        htmlEmail.setSender(mailSender);

        Mail[] mails = new Mail[2];
        mails[0] = email;
        mails[1] = htmlEmail;

        mailSender.send(mails);
    }

    @Test
    public void testTest() throws Exception {
        MailSender mailSender = new MailSender();

        mailSender.test();
    }

}