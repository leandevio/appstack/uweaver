/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.net;

import org.testng.annotations.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SSOTest obj = new SSOTest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SSOTest {
    private final String USER_AGENT = "Mozilla/5.0";

    @Test
    public void testGet() throws Exception {

        URL url = new URL("http://localhost:8080/crm-web/rest/site/");
        HttpURLConnection http = (HttpURLConnection) url.openConnection();

        http.setRequestMethod("GET");
        http.setRequestProperty("User-Agent", USER_AGENT);

        receive(http);


    }

    private void receive(HttpURLConnection http) throws Exception {
        int responseCode = http.getResponseCode();

        System.out.println("\nSending 'GET' request to URL : " + http.getURL());
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(http.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
    }

    @Test
    public void testPostForm() throws Exception {
        URL url = new URL("http://localhost:8080/crm-web/rest/site/");
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setDoOutput(true);

        Map<String,String> arguments = new HashMap<>();
        arguments.put("username", "root");
        arguments.put("password", "sjh76HSn!"); // This is a fake password obviously
        StringBuffer sb = new StringBuffer();
        for(Map.Entry<String,String> entry : arguments.entrySet())
            sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"))
                    .append("=").append(URLEncoder.encode(entry.getValue(), "UTF-8")).append("&");
        byte[] out = sb.toString().getBytes("UTF-8");
        int length = out.length;

        http.setFixedLengthStreamingMode(length);
        http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        http.connect();
        try(OutputStream os = http.getOutputStream()) {
            os.write(out);
        }

        receive(http);
    }

    @Test
    public void testPostJSON() throws Exception {
        URL url = new URL("http://localhost:8080/crm-web/rest/site/");
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setDoOutput(true);

        byte[] out = "{\"username\":\"root\",\"password\":\"password\"}" .getBytes("UTF-8");
        int length = out.length;

        http.setFixedLengthStreamingMode(length);
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        try(OutputStream os = http.getOutputStream()) {
            os.write(out);
        }

        receive(http);
    }

    @Test
    public void testPostMultiparts() throws Exception {
        URL url = new URL("http://localhost:8080/crm-web/rest/site/");
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setDoOutput(true);

        String boundary = UUID.randomUUID().toString();
        byte[] boundaryBytes = boundaryBytes =
                ("--" + boundary + "\r\n").getBytes("UTF-8");
        byte[] finishBoundaryBytes =
                ("--" + boundary + "--").getBytes("UTF-8");
        http.setRequestProperty("Content-Type",
                "multipart/form-data; charset=UTF-8; boundary=" + boundary);

        // Enable streaming mode with default settings
        http.setChunkedStreamingMode(0);

        // Send our fields:
        try(OutputStream out = http.getOutputStream()) {
            // Send our header (thx Algoman)
            out.write(boundaryBytes);

            // Send our first field
            sendField(out, "username", "root");

            // Send a seperator
            out.write(boundaryBytes);

            // Send our second field
            sendField(out, "password", "toor");

            // Send another seperator
            out.write(boundaryBytes);

            // Send our file
            try(InputStream file = new FileInputStream("test.txt")) {
                sendFile(out, "identification", file, "text.txt");
            }

            // Finish the request
            out.write(finishBoundaryBytes);
        }

        receive(http);
    }

    private void sendFile (OutputStream out, String name, InputStream in, String filename) throws Exception {
        String o = "Content-Disposition: form-data; name=\"" + URLEncoder.encode(name,"UTF-8")
                + "\"; filename=\"" + URLEncoder.encode(filename,"UTF-8") + "\"\r\n\r\n";
        out.write(o.getBytes("UTF-8"));
        byte[] buffer = new byte[2048];
        for (int n = 0; n >= 0; n = in.read(buffer))
            out.write(buffer, 0, n);
        out.write("\r\n".getBytes("UTF-8"));
    }

    private void sendField(OutputStream out, String name, String field) throws Exception {
        String o = "Content-Disposition: form-data; name=\""
                + URLEncoder.encode(name,"UTF-8") + "\"\r\n\r\n";
        out.write(o.getBytes("UTF-8"));
        out.write(URLEncoder.encode(field,"UTF-8").getBytes("UTF-8"));
        out.write("\r\n".getBytes("UTF-8"));
    }

    @Test
    public void testPost() throws Exception {
        String url = "http://localhost:8080/crm-web/rest/site";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
    }


}
