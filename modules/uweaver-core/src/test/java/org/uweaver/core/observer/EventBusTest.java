package org.uweaver.core.observer;

import org.testng.annotations.Test;
import org.uweaver.core.event.server.StartEvent;
import org.uweaver.core.event.server.StopEvent;

/**
 * Created by jasonlin on 8/30/16.
 */
public class EventBusTest {

    @Test
    public void testPost() throws Exception {
        WorkerThread worker = new WorkerThread();
        worker.start();

        for (; ; ) {
            int waitInSeconds = (int)(Math.random()*10);
            Thread.sleep(waitInSeconds*1000);

            if (!worker.isRunning()) {
                EventBus.getDefaultInstance().post(new StartEvent(null));
            } else {
                EventBus.getDefaultInstance().post(new StopEvent(null));
            }
        }
    }

}