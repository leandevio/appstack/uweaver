/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.observer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;

import java.util.Arrays;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ReactiveXTest obj = new ReactiveXTest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ReactiveXTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReactiveXTest.class);

    @Test
    public void testObservableFromIterator() throws Exception {
        List<String> list = Arrays.asList("One", "Two", "Three", "Four");

        Observable<String> observable = Observable.from(list);

        observable.subscribe(new Action1<String>() {
            @Override
            public void call(String e) {
                LOGGER.debug(e);
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                LOGGER.error("Exception", throwable);
            }
        }, new Action0() {
            @Override
            public void call() {
                LOGGER.debug("Done.");
            }
        });
    }

    @Test
    public void testObservableJust() throws Exception {
        Observable<String> observable = Observable.just("One", "Two", "Three", "Four");

        observable.map(new Func1() {
            @Override
            public Object call(Object o) {
                return ((String) o).toUpperCase();
            }
        }).subscribe(new Action1<String>() {
            @Override
            public void call(String e) {
                LOGGER.debug(e);
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                LOGGER.error("Exception", throwable);
            }
        }, new Action0() {
            @Override
            public void call() {
                LOGGER.debug("Done.");
            }
        });
    }

    @Test
    public void testObservableCreate() throws Exception {
        RxMaker maker = new RxMaker();

        maker.subscribe(event -> LOGGER.debug("A: " + event.toString()));

        maker.subscribe(event -> LOGGER.debug("B: " + event.toString()));

        maker.post(6);
    }

    @Test
    public void testSubject() throws Exception {
        PublishSubject<String> subject = PublishSubject.create();
        Subscription subscriptionA = subject.subscribe(s -> LOGGER.debug("subscriber A: " + s));

        subject.onNext("Event - 1");
        subject.onNext("Event - 2");

        Subscription subscriptionB = subject.subscribe(s -> LOGGER.debug("subscriber B: " + s));
        subject.onNext("Event - 3");
        subject.onNext("Event - 4");

        subscriptionA.unsubscribe();
        subject.onNext("Event - 5");


    }
}
