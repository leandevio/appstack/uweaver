package org.uweaver.core.reflection;

import org.testng.annotations.Test;
import org.uweaver.core.app.annotation.AppContext;

import java.util.List;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 8/29/16.
 */
public class ReflectionsTest {
    @Test
    public void testCreateTypeQuery() throws Exception {
        Reflections reflections = Reflections.getDefaultInstance();

        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(AppContext.class).list();

        assertEquals(types.size(), 1);
        assertEquals(types.get(0), MyAppContext.class);
    }

}