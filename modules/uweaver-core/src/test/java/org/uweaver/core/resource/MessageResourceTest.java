package org.uweaver.core.resource;

import org.testng.annotations.Test;

import java.util.Locale;

import static org.testng.Assert.*;

public class MessageResourceTest {
    private final String WELCOME = "Hello Jason Lin, welcome to Flora Management System.";
    private final String WELCOME_zh_TW = "林晉陞您好，歡迎使用花坊管理系統．";
    private final String SPECIALCHARACTERS = "'\\{}<div></div>\n";

    @Test
    public void testGetMessage() throws Exception {
        Locale en_US = new Locale("en", "US");
        MessageResource messageResource = new MessageResource(en_US);
        String welcome = messageResource.getMessage("WELCOME", "Jason Lin", "Flora Management System");
        assertEquals(welcome, WELCOME);
        Locale zh_TW = new Locale("zh", "TW");
        messageResource = new MessageResource(zh_TW);
        String welcome_zh_TW = messageResource.getMessage("WELCOME", "林晉陞", "花坊管理系統");
        assertEquals(welcome_zh_TW, WELCOME_zh_TW);
    }

    @Test
    public void testGetNotDefinedMessage() throws Exception{
        Locale zh_TW = new Locale("zh", "TW");
        MessageResource messageResource = new MessageResource(zh_TW);

        String not_defined_message = messageResource.getMessage("NOT_DEFINED_MESSAGE");

        assertEquals(not_defined_message, "NOT_DEFINED_MESSAGE");

    }

    @Test
    public void testGetMessageWithReservedSymbol() throws Exception{
        Locale en_US = new Locale("en", "US");
        MessageResource messageResource = new MessageResource(en_US);

        String specialCharacters = messageResource.getMessage("SPECIALCHARACTERS");

        assertEquals(specialCharacters, SPECIALCHARACTERS);

    }
}