package org.uweaver.core.resource;

import org.testng.annotations.Test;

import java.util.Locale;

import static org.testng.Assert.*;

public class TermResourceTest {
    private final String EFFECTIVEDATE="Effective Date";
    private final String EFFECTIVEDATE_zh_TW="有效日期";
    @Test
    public void testGetTerm() throws Exception {
        Locale en_US = new Locale("en", "US");
        TermResource termResource = new TermResource(en_US);
        String effectiveDate = termResource.getTerm("Effective Date");
        assertEquals(effectiveDate, EFFECTIVEDATE);
        Locale zh_TW = new Locale("zh", "TW");
        termResource = new TermResource(zh_TW);
        String effectiveDate_zh_TW = termResource.getTerm("Effective Date");
        assertEquals(effectiveDate_zh_TW, EFFECTIVEDATE_zh_TW);

    }
}