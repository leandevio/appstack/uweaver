package org.uweaver.core.scheduling;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


/**
 * Created by jasonlin on 6/22/15.
 */
public class SchedulerTest {

    @Test
    public void testScheduleWithFixedDelay() throws Exception {
        Scheduler scheduler = Scheduler.getInstance();

        Job job = new Job("One Minute Task", OneMinuteTask.class);
        scheduler.scheduleWithFixedDelay(job, 20, TimeUnit.SECONDS);

        scheduler.start();

        Thread.sleep(10 * 1000);
        scheduler.schedule(job);

        Thread.sleep(160 * 1000);

        System.out.println("Shutting down ...");

        scheduler.stop(30, TimeUnit.SECONDS);
    }

//    @Test
//    public void testThrottle() throws Exception {
//        Scheduler scheduler = Scheduler.getInstance();
//
//        Job job = new Job("One Minute Task", OneMinuteTask.class);
//        job.setThrottle(10L, TimeUnit.SECONDS);
//
//        // trigger a recurring scheduling.
//        scheduler.scheduleWithFixedDelay(job, 0, 120, TimeUnit.SECONDS);
//
//        scheduler.startup();
//        Thread.sleep(30 * 1000);
//        // all the scheduling should be throttled.
//        for(int i=0; i<8; i++) {
//            Thread.sleep(1000);
//            System.out.println(String.format("Scheduling to be throttled - %d ...", i));
//            scheduler.schedule(job);
//        }
//
//        // trigger another scheduling to run.
//        System.out.println("Waiting for the first run of the recurring scheduling to finish.");
//        Thread.sleep(120 * 1000);
//
//        System.out.println("Shutting down...");
//        scheduler.shutdown(120, TimeUnit.SECONDS);
//    }

//    @Test
//    public void testThrottleWithFixedDelayByType() throws Exception {
//        Scheduler scheduler = Scheduler.getInstance();
//
//        scheduler.throttle(OneMinuteTask.class, 10L, TimeUnit.SECONDS);
//
//        System.out.println("Scheduling the periodic task to run ...");
//        scheduler.scheduleWithFixedDelay(new OneMinuteTask("task #1"), 0, 30, TimeUnit.SECONDS);
//
//        // only one task should be scheduled to run.
//        for(int i=0; i<8; i++) {
//            Thread.sleep(1000);
//            System.out.println(String.format("Scheduling foobar - %d to be throttled ...", i));
//            scheduler.schedule(new OneMinuteTask("foobar - " + i));
//        }
//
//        Thread.sleep(60 * 1000);
//
//        System.out.println(new Date() + ": Shutting down the scheduler...");
//        scheduler.shutdownImmediately();
//        System.out.println(new Date() + ": The scheduler has benn shutdown.");
//    }

//    @Test
//    public void testQuartz() throws Exception {
//        SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
//        org.quartz.Scheduler sched = schedFact.getScheduler();
//
//        // define the job and tie it to our HelloJob class
//        JobDetail job = newJob(HelloJob.class)
//                .withIdentity("myJob", "group1")
//                .build();
//        // Trigger the job to run now, and then every 40 seconds
//        SimpleTrigger trigger = newTrigger()
//                .withIdentity("myTrigger", "group1")
//                .startNow()
//                .withSchedule(simpleSchedule()
//                        .withIntervalInSeconds(10)
//                        .repeatForever())
//                .build();
//        // Tell quartz to schedule the job using our trigger
//        sched.scheduleJob(job, trigger);
//        sched.start();
//        Thread.sleep(90L * 1000L);
//
//        sched.shutdown(true);
//
//    }
}