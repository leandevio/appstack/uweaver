package org.uweaver.core.search;

import org.testng.annotations.Test;
import org.uweaver.core.file.TextFileReader;
import org.uweaver.core.util.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.testng.Assert.assertEquals;

public class IndexSearcherTest {
    private final Class CLASS = this.getClass();
    private final ClassLoader CLASSLOADER = this.getClass().getClassLoader();
    private final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private final String INDEX_COLLECTION_NAME = "books";
    private final String INDEX_COLLECTION_KEY = "isbn";
    private final IndexRepository indexRepository = IndexRepository.getDefaultInstance();

    @Test
    public void testIndex() throws Exception {
        indexRepository.scan();
        IndexCollection indexCollection = indexRepository.getCollection(INDEX_COLLECTION_NAME);
        if(indexCollection == null) {
            indexCollection = new IndexCollection(INDEX_COLLECTION_NAME);
            List<Field> fields = new ArrayList<>();
            fields.add(new Field("title"));
            fields.add(new Field("author"));
            fields.add(new Field("year_published"));
            fields.add(new Field("genre"));
            fields.add(new Field("isbn"));
            indexCollection.setFields(fields);
            List<Field> key = new ArrayList<>();
            key.add(new Field(INDEX_COLLECTION_KEY));
            indexCollection.setKey(key);
            indexRepository.saveCollection(indexCollection);
        }
        assertEquals(indexCollection.getFields().size(), 5);
    }


    @Test
    public void testBuild() throws Exception {
        IndexCollection indexCollection = indexRepository.getCollection(INDEX_COLLECTION_NAME);

        List<Map<String, Object>> books = loadData();

        IndexWriter indexWriter = new IndexWriter(indexRepository);
        for(Map<String, Object> book : books) {
            removeDocument((String) book.get(INDEX_COLLECTION_KEY));
            indexWriter.add(indexCollection, book);
        }

        Date now = new Date();
        indexCollection.setUpdateTime(now);
        indexRepository.saveCollection(indexCollection);

        indexRepository.scan();
        indexCollection = indexRepository.getCollection(INDEX_COLLECTION_NAME);
        assertEquals(indexCollection.getUpdateTime(), now);
    }

    @Test
    public void testSearch() throws Exception {
        IndexSearcher searcher = new IndexSearcher(indexRepository);

        Map<String, Object> predicate = new HashMap<>();
        predicate.put("genre", "Fiction");
        IndexQuery query = new IndexQuery().from(INDEX_COLLECTION_NAME)
                .where(predicate);

        assertEquals(searcher.count(query), 5);

        Hits hits = searcher.search(query, 5, 0);
        for(Hit hit : hits) {
            Document document = searcher.get(hit.getDoc());
            System.out.printf("%s\n", document.get(INDEX_COLLECTION_KEY));
        }
    }

    private void removeDocument(String isbn) {
        IndexWriter indexWriter = new IndexWriter(indexRepository);
        Map<String, Object> predicate = new HashMap<>();
        predicate.put(INDEX_COLLECTION_KEY, isbn);
        IndexQuery query = new IndexQuery().from(INDEX_COLLECTION_NAME).where(predicate);
        indexWriter.remove(query);
    }

    private List<Map<String, Object>> loadData() throws IOException {
        try (InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve("books.json").toString());
             TextFileReader reader = new TextFileReader(inputStream)) {
            reader.open();
            String json = reader.getText();
            return Arrays.asList(JsonParser.readValue(json, Map[].class));
        }
    }
}
