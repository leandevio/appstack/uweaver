package org.uweaver.core.util;

import org.testng.annotations.Test;

import java.math.BigInteger;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/12/15.
 */
public class BooleanConverterTest {

    @Test
    public void testConvert() throws Exception {
        Converter converter = new BooleanConverter();

        assertTrue(converter.convert("true", Boolean.class));
        assertTrue(converter.convert("True", Boolean.class));
        assertNull(converter.convert(null, Boolean.class));
        assertFalse(converter.convert("", Boolean.class));
        assertFalse(converter.convert("yes", Boolean.class));
        assertFalse(converter.convert(0, Boolean.class));
        assertTrue(converter.convert(1, Boolean.class));

        assertEquals(converter.convert(true, String.class), "true");
        assertEquals(converter.convert(false, String.class), "false");
        assertEquals(converter.convert(true, Integer.class).intValue(), 1);
        assertEquals(converter.convert(false, Integer.class).intValue(), 0);
        assertEquals(converter.convert(true, Long.class).longValue(), 1L);
        assertEquals(converter.convert(true, BigInteger.class), new BigInteger("1"));
    }
}