package org.uweaver.core.util;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class CollectionTest {

    @Test
    public void testFind() throws Exception {
        Collection collection = new Collection();
        Model apple = new Model();
        apple.set("fruit", "apple");
        collection.add(apple);

        Model orange = new Model();
        orange.set("fruit", "orange");
        collection.add(orange);

        Model found = collection.find(model -> (model.get("fruit").equals("apple")));

        assertEquals(found, apple);
    }
}