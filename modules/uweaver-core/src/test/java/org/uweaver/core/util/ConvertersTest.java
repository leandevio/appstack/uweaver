package org.uweaver.core.util;

import org.joda.time.DateTime;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.TimeZone;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/4/15.
 */
public class ConvertersTest {

    @Test
    public void testConvert() throws Exception {
        String dateTimePattern = "MM/dd/yyyy HH:mm:ss";
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Taipei");

        Converters converter = new Converters();

        DateTimeConverter dtConverter = (DateTimeConverter) converter.lookup(Date.class);
        dtConverter.setTimeZone(timeZone);
        dtConverter.setDateTimePattern(dateTimePattern);
        dtConverter.setUseDefault(true);

        assertEquals(converter.convert("23", Integer.class), new Integer(23));
        assertEquals(converter.convert(23, Integer.class), new Integer(23));
        assertEquals(converter.convert("23", Long.class), new Long(23));
        assertEquals(converter.convert(23, Long.class), new Long(23));
        assertEquals(converter.convert("23", BigInteger.class), new BigInteger("23"));
        assertEquals(converter.convert(23, BigInteger.class), new BigInteger("23"));
        assertEquals(converter.convert("3.14159", Double.class), new Double(3.14159));
        assertEquals(converter.convert(3.14159, Double.class), new Double(3.14159));
        assertEquals(converter.convert("3.14159", Float.class), new Float(3.14159));
        assertEquals(converter.convert("4.59", BigDecimal.class), new BigDecimal("4.59"));
        assertEquals(converter.convert(new BigDecimal("4.59"), String.class), "4.59");
        assertNull(converter.convert(null, BigDecimal.class));

        assertEquals(converter.convert(new DateTime(2014, 4, 1, 18, 30, 23), String.class), "04/01/2014 18:30:23");
        assertNull(converter.convert(null, DateTime.class));
        assertNull(converter.convert("", Date.class));

        assertNull(converter.convert(null, String.class));

    }
}