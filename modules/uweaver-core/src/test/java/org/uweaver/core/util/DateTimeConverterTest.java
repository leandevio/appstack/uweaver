package org.uweaver.core.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.testng.annotations.Test;
import org.uweaver.core.exception.NotEmptyConflictException;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/8/15.
 */
public class DateTimeConverterTest {

    @Test
    public void testConvert() throws Exception {
        Locale locale = new Locale("zh", "TW");
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Taipei");
        String str = "04/01/2014 18:30:23";
        String dateTimePattern = "MM/dd/yyyy HH:mm:ss";
        DateTime dt = new DateTime(2014, 4, 1, 18, 30, 23, DateTimeZone.forTimeZone(timeZone));

        DateTimeConverter converter = new DateTimeConverter();
        converter.setTimeZone(timeZone);
        converter.setLocale(locale);
        converter.setDateTimePattern(dateTimePattern);

        assertEquals(dt, converter.convert(str, DateTime.class));
        assertEquals(dt,converter.convert(dt.toDate(), DateTime.class));
        assertEquals(dt, converter.convert(dt.toCalendar(locale), DateTime.class));

        assertEquals(converter.convert(dt, String.class), str);
        assertEquals(converter.convert(dt, Date.class), dt.toDate());
        assertEquals(converter.convert(dt, Calendar.class), dt.toCalendar(locale));

        assertNull(converter.convert(null, DateTime.class));
    }

    @Test
    public void testConvertISO8601() throws Exception {
        Locale locale = new Locale("zh", "TW");
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Taipei");
        DateTime dt = new DateTime(2014, 4, 1, 18, 30, 23, 567, DateTimeZone.forTimeZone(timeZone));
        String str0900 = "2014-04-01T19:30:23.567+0900";
        String str0800 = "2014-04-01T18:30:23.567+0800";
        String strZ = "2014-04-01T10:30:23.567Z";

        DateTimeConverter converter = new DateTimeConverter();
        converter.setTimeZone(timeZone);
        converter.setLocale(locale);

        assertEquals(converter.convert(dt, String.class), str0800);
        assertEquals(converter.convert(dt, Date.class), dt.toDate());
        assertEquals(converter.convert(dt, Calendar.class), dt.toCalendar(locale));

        assertEquals(dt, converter.convert(str0900, DateTime.class));
        assertEquals(dt, converter.convert(str0800, DateTime.class));
        assertEquals(dt, converter.convert(strZ, DateTime.class));

        assertEquals(dt.toDate(), converter.convert(str0900, Date.class));
        assertEquals(dt.toDate(), converter.convert(str0800, Date.class));
        assertEquals(dt.toDate(), converter.convert(strZ, Date.class));

        assertNull(converter.convert(null, DateTime.class));
    }


    @Test
    public void testConvertUseDefault() throws Exception {
        String invalidDate = "";
        Date now = new Date();
        DateTimeConverter converter = new DateTimeConverter();

        converter.setUseDefault(true);
        assertNull(converter.convert(invalidDate, Date.class));
        assertNull(converter.convert(null, Date.class));

        converter.setDefaultValue(now);
        assertEquals(converter.convert(invalidDate, Date.class), now);
        assertEquals(converter.convert(null, Date.class), now);
    }


    @Test
    public void testConvertWithException() throws Exception {
        String invalidDate = "";
        DateTimeConverter converter = new DateTimeConverter();
        converter.setUseDefault(true);
        converter.setNullable(false);

        try {
            converter.convert(invalidDate, Date.class);
        } catch (NotEmptyConflictException ex) {
            assertEquals(ex.parameter(0), "defaultValue");
        }

        try {
            converter.convert(null, Date.class);
        } catch (NotEmptyConflictException ex) {
            assertEquals(ex.parameter(0), "defaultValue");
        }
    }

    @Test
    public void testConvertWithDefaultValue() throws Exception {
        String invalidDate = "";
        Date now = new Date();
        DateTimeConverter converter = new DateTimeConverter();

        assertEquals(converter.convert(invalidDate, Date.class, now), now);
        assertEquals(converter.convert(null, Date.class, now), now);

    }

    @Test
    public void testConvertWithDST() throws Exception {
        // UTC-7
        TimeZone timeZoneUTC7 = TimeZone.getTimeZone("GMT-7");
        DateTimeConverter converterUTC7 = new DateTimeConverter();
        converterUTC7.setTimeZone(timeZoneUTC7);

        // UTC-6
        TimeZone timeZoneUTC6 = TimeZone.getTimeZone("GMT-6");
        DateTimeConverter converterUTC6 = new DateTimeConverter();
        converterUTC6.setTimeZone(timeZoneUTC6);

        // Mountain Standard Time - Phoenix(Arizona)
        TimeZone timeZoneMST = TimeZone.getTimeZone("US/Arizona");
        DateTimeConverter converterMST = new DateTimeConverter();
        converterMST.setTimeZone(timeZoneMST);

        // Mountain Daylight Time - Salt Lake City(Utah)
        TimeZone timeZoneMDT = TimeZone.getTimeZone("US/Mountain");
        DateTimeConverter converterMDT = new DateTimeConverter();
        converterMDT.setTimeZone(timeZoneMDT);


        DateTime dt = new DateTime(2011, 3, 12, 2, 30, DateTimeZone.forTimeZone(timeZoneMDT));

        // Both MDT(Salt Late City) and MST(Phoenix) are UTC-7 when no daylight saving(March 3, 2011 2:30).
        assertEquals(converterMST.convert(dt, String.class), converterUTC7.convert(dt, String.class));
        assertEquals(converterMDT.convert(dt, String.class), converterUTC7.convert(dt, String.class));

        int offset = timeZoneMDT.getOffset(dt.getMillis());
        assertEquals(offset/1000/60/60, -7);

        // After one day, MDT is in daylight saving.
        // MDT(Salt Late City) changes from UTC-7 to UTC-6 but MST(Phoenix) doesn't.
        dt = dt.plusDays(1);

        assertEquals(converterMST.convert(dt, String.class), converterUTC7.convert(dt, String.class));
        assertNotEquals(converterMDT.convert(dt, String.class), converterUTC7.convert(dt, String.class));
        assertEquals(converterMDT.convert(dt, String.class), converterUTC6.convert(dt, String.class));
        offset = timeZoneMDT.getOffset(dt.getMillis());
        assertEquals(offset/1000/60/60, -6);

    }

    @Test
    public void testConvertSQLDate() throws Exception {
        Locale locale = new Locale("zh", "TW");
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Taipei");
        DateTime dt = new DateTime(2014, 4, 1, 18, 30, 23, 567, DateTimeZone.forTimeZone(timeZone));
        String str0900 = "2014-04-01T19:30:23.567+0900";
        String str0800 = "2014-04-01T18:30:23.567+0800";

        DateTimeConverter converter = new DateTimeConverter();
        converter.setTimeZone(timeZone);

        assertEquals(str0800, converter.convert(new Timestamp(dt.getMillis()), String.class));

    }

    /*
    @Test
    public void testConvertToDateWithDST() throws Exception {
        TimeZone timeZoneUTC8 = TimeZone.getTimeZone("GMT+8");
        TimeZone timeZoneTPE = TimeZone.getTimeZone("Asia/Taipei");
        // Asia/Taipei has daylight saving (June 30, 1979 1:00).
        String strDate = "06/30/1979";
        String strDateTime = "06/30/1979 01:00:00";

        DateTimeConverter converter = new DateTimeConverter();
        converter.setDateTimePattern("MM/dd/yyyy");

        // Use the equivalent UTC+8 to convert the date with daylight saving.
        converter.setTimeZone(timeZoneUTC8);
        Date dt = converter.convert(strDate, Date.class);


        converter.setDateTimePattern("MM/dd/yyyy HH:mm:ss");
        converter.setTimeZone(timeZoneTPE);
        assertEquals(strDateTime, converter.convert(dt, String.class));

    }
    */
}