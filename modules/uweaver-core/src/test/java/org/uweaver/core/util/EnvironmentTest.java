/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import static org.testng.Assert.*;

/**
 * <h1>EnvironmentTest</h1>
 * The EnvironmentTest class implements ... The implementation provides ...
 * <p>
 * <b>Note:</b> This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class EnvironmentTest {
    private Environment environment = Environment.getDefaultInstance();
    private final Path TESTBASE = environment.tmpFolder().resolve(this.getClass().getSimpleName());

    private final String APPNAME_UNNAMED = "app";
    private final String APPNAME_NAMED = "merchant-office";
    private final Path APPHOME_UNNAMED = TESTBASE.resolve("app");
    private final Path APPHOME_NAMED = TESTBASE.resolve("merchant-office");

    private final Locale LOCALE_UNNAMED = new Locale("en", "US");
    private final Locale LOCALE_NAMED = new Locale("zh", "TW");
    private final TimeZone TIMEZONE_UNNAMED = TimeZone.getTimeZone("America/Los_Angeles");
    private final TimeZone TIMEZONE_NAMED = TimeZone.getTimeZone("Asia/Taipei");
    private final String DATETIMEPATTERN_UNAMED = "MM-dd-yyyy HH:mm:ss.SSS";
    private final String DATETIMEPATTERN_NAMED = "yyyy/MM/dd HH:mm:ss.SSS";
    private final String DATETIMEPATTERN_CUSTOM = "yyyy-MM-dd HH:mm:ss.SSS";
    private final String FILEENCODING_UNAMED = "UTF-8";
    private final String FILEENCODING_NAMED = "Big5";
    private final String CURRDIR = System.getProperty("user.dir");

    @BeforeTest
    public void beforeTest() throws Exception {
        clear();
    }

    @Test
    public void testDefaultNamedApp() throws Exception {
        clear();
        System.setProperty(APPNAME_NAMED.toUpperCase() + "_HOME", APPHOME_NAMED.toString());
        environment.reset();

        assertEquals(environment.appName(), APPNAME_NAMED);
        assertEquals(environment.appHome(), APPHOME_NAMED);
        assertEquals(environment.timezone(), TIMEZONE_NAMED);
        assertEquals(environment.locale(), LOCALE_NAMED);
        assertEquals(environment.property("time.timezone"), "Asia/Taipei");
        assertEquals(environment.datetimePattern(), DATETIMEPATTERN_NAMED);
        assertEquals(environment.fileencoding(), FILEENCODING_NAMED);
        assertTrue(Files.exists(environment.dataFolder()));
    }

    @Test
    public void testCustomNamedApp() throws Exception {
        clear();
        System.setProperty(APPNAME_NAMED.toUpperCase() + "_HOME", APPHOME_NAMED.toString());
        generateCustomPropFile(APPNAME_NAMED);
        environment.reset();

        assertEquals(environment.datetimePattern(), DATETIMEPATTERN_CUSTOM);
    }

    @Test
    public void testDefaultUnnamedApp() throws Exception {
        clear();
        System.setProperty(APPNAME_UNNAMED.toUpperCase() + "_HOME", APPHOME_UNNAMED.toString());
        environment.reset(false);

        assertEquals(environment.appName(), APPNAME_UNNAMED);
        assertEquals(environment.appHome(), APPHOME_UNNAMED);
        assertEquals(environment.timezone(), TIMEZONE_UNNAMED);
        assertEquals(environment.locale(), LOCALE_UNNAMED);
        assertEquals(environment.property("time.timezone"), "America/Los_Angeles");
        assertEquals(environment.datetimePattern(), DATETIMEPATTERN_UNAMED);
        assertTrue(Files.exists(environment.dataFolder()));
    }

    @Test
    public void testCustomUnnamedApp() throws Exception {
        clear();
        System.setProperty(APPNAME_UNNAMED.toUpperCase() + "_HOME", APPHOME_UNNAMED.toString());
        generateCustomPropFile(APPNAME_UNNAMED);
        environment.reset(false);

        assertEquals(environment.datetimePattern(), DATETIMEPATTERN_CUSTOM);
    }

    private void clear() throws Exception {
        deleteDirectory(TESTBASE);
        Files.createDirectory(TESTBASE);
        Files.createDirectory(APPHOME_NAMED);
        Files.createDirectory(APPHOME_UNNAMED);
    }

    private void generateCustomPropFile(String appName) throws Exception {
        String filename = appName + ".properties";
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(filename);
        Properties props = new Properties();
        props.load(in);
        in.close();

        props.setProperty("time.datetimePattern", DATETIMEPATTERN_CUSTOM);
        Path conf = TESTBASE.resolve(appName).resolve("conf");
        Files.createDirectory(conf);
        Path path = conf.resolve(filename);
        OutputStream out = new FileOutputStream(path.toFile());

        props.store(out, "Custom Application Properties");
        out.close();
    }

    private void deleteDirectory(Path path) throws IOException {
        if(!Files.exists(path)) return;
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException e)
                    throws IOException
            {
                if (e == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed
                    throw e;
                }
            }
        });
    }
}