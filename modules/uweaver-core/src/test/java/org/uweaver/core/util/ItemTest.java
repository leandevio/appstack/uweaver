package org.uweaver.core.util;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONFeature;
import org.uweaver.core.json.JSONParser;
import java.io.InputStream;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 4/19/16.
 */
public class ItemTest {
    private JSONParser jsonParser;
    private String json;
    private String jsons;


    @BeforeTest
    public void setUp() throws Exception {
        jsonParser = new JSONParser();
        jsonParser.enable(JSONFeature.PRETTY);

        InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/util/order.json");
        byte[] buffer = new byte[in.available()];
        in.read(buffer);
        in.close();
        json = new String(buffer, "UTF-8");

        in = this.getClass().getClassLoader()
                .getResourceAsStream("org/uweaver/core/util/orders.json");
        buffer = new byte[in.available()];
        in.read(buffer);
        in.close();
        jsons = new String(buffer, "UTF-8");

    }

    @Test
    public void testPopulateMap() throws Exception {

        Order expect = jsonParser.readValue(json, Order.class);
        Map<String,Object> map = jsonParser.readValue(json, Map.class);
        Order actual = new Order();

        Item item = new Item(actual);
        item.populate(map);

        assertEquals(actual, expect);
    }

    @Test
    public void testPopulateBean() throws Exception {

        Order expect = jsonParser.readValue(json, Order.class);
        Map map = jsonParser.readValue(json, Map.class);
        Order actual = new Order();

        Item item = new Item(actual);
        item.populate(map);

        assertEquals(actual, expect);


    }
}