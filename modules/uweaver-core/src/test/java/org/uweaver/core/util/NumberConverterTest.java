package org.uweaver.core.util;

import org.testng.annotations.Test;
import org.uweaver.core.exception.ConversionException;
import org.uweaver.core.exception.NotEmptyConflictException;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 5/8/15.
 */
public class NumberConverterTest {

    @Test
    public void testConvert() throws Exception {
        Converter converter = new NumberConverter();

        String strInteger = "87735773";
        int intNumber = 87735773;
        long longNumber = 87735773L;
        BigInteger bigInteger = new BigInteger(strInteger);
        BigDecimal intDecimal = new BigDecimal(strInteger);

        String strFloat = "0.5773";
        float floatNumber = 0.5773f;
        double doubleNumber = 0.5773;
        BigDecimal floatDecimal = new BigDecimal(strFloat);


        assertEquals(converter.convert(strInteger, BigInteger.class), bigInteger);
        assertEquals(converter.convert(intNumber, BigInteger.class), bigInteger);
        assertEquals(converter.convert(longNumber, BigInteger.class), bigInteger);

        assertEquals(converter.convert(bigInteger, String.class), strInteger);
        assertEquals(converter.convert(bigInteger, Integer.class).intValue(), intNumber);
        assertEquals(converter.convert(bigInteger, Long.class).longValue(), longNumber);

        assertEquals(converter.convert(strFloat, BigDecimal.class), floatDecimal);
        assertEquals(converter.convert(floatNumber, BigDecimal.class), floatDecimal);
        assertEquals(converter.convert(doubleNumber, BigDecimal.class), floatDecimal);

        assertEquals(converter.convert(floatDecimal, String.class), strFloat);
        assertEquals(converter.convert(floatDecimal, Float.class).floatValue(), floatNumber);
        assertEquals(converter.convert(floatDecimal, Double.class).doubleValue(), doubleNumber);

        assertEquals(converter.convert(strInteger, BigDecimal.class), intDecimal);
        assertEquals(converter.convert(intNumber, BigDecimal.class), intDecimal);
        assertEquals(converter.convert(longNumber, BigDecimal.class), intDecimal);
        assertEquals(converter.convert(bigInteger, BigDecimal.class), intDecimal);

    }

    @Test
    public void testConvertUseDefault() throws Exception {
        Converter converter = new NumberConverter();

        String strInvalidNumber = "";
        BigDecimal zero = new BigDecimal(0);

        converter.setUseDefault(true);
        assertNull(converter.convert(strInvalidNumber, BigDecimal.class), null);

        converter.setDefaultValue(zero);
        assertEquals(converter.convert(null, BigDecimal.class), zero);
        assertEquals(converter.convert(strInvalidNumber, BigDecimal.class), zero);

    }

    @Test
    public void testConvertWithException() throws Exception {
        String invalidNumber = "";
        NumberConverter converter = new NumberConverter();
        converter.setUseDefault(true);
        converter.setNullable(false);

        try {
            converter.convert(invalidNumber, BigDecimal.class);
        } catch (NotEmptyConflictException ex) {
            assertEquals(ex.parameter(0), "defaultValue");
        }

        try {
            converter.convert(null, BigDecimal.class);
        } catch (NotEmptyConflictException ex) {
            assertEquals(ex.parameter(0), "defaultValue");
        }
    }


    @Test
    public void testConvertWithDefaultValue() throws Exception {
        String invalidNumber = "";
        int zero = 0;
        NumberConverter converter = new NumberConverter();

        assertEquals(converter.convert(invalidNumber, Integer.class, zero).intValue(), zero);
        assertEquals(converter.convert(null, Integer.class, zero).intValue(), zero);
    }

    @Test
    public void testConvertDifferentString() throws Exception {
        NumberConverter converter = new NumberConverter();

        assertEquals(converter.convert("023", Integer.class).intValue(), 23);
        try {
            assertEquals(converter.convert(" 23", Integer.class).intValue(), 23);
        } catch (ConversionException ex) {
            assertEquals(ex.parameter(0), " 23");
        }
    }
}