/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.util;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * ## OrderItem
 * ### The class implements ... 
 *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * OrderItem obj = new OrderItem();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class OrderItem {
    private String productNo;
    private String productName;
    private BigDecimal unitPrice;
    private int quantity;

//    private Order order;

    public OrderItem() {}

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

//    public Order getOrder() {
//        return this.order;
//    }
//
//    public void setOrder(Order order) {
//        this.order = order;
//    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof OrderItem)) return false;
        if(obj==null) return false;

        OrderItem other = (OrderItem) obj;

        return (this.hashCode()==other.hashCode());

    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(this.getProductNo(), this.getProductName(), this.getUnitPrice(), this.getQuantity());
        return hash;
    }
}
