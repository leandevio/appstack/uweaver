package org.uweaver.core.util;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class VersionTest {

    @Test
    public void testMajorVersion() {
        String s = "1";

        Version version = new Version(s);

        assertEquals(version.major(), "1");
        assertEquals(version.minor(), "0");
        assertEquals(version.build(), "0");
    }

    @Test
    public void testMinorVersion() {
        String s = "1.2";

        Version version = new Version(s);

        assertEquals(version.major(), "1");
        assertEquals(version.minor(), "2");
        assertEquals(version.build(), "0");
    }

    @Test
    public void testBuildVersion() {
        String s = "1.2.33";

        Version version = new Version(s);

        assertEquals(version.major(), "1");
        assertEquals(version.minor(), "2");
        assertEquals(version.build(), "33");
    }

}