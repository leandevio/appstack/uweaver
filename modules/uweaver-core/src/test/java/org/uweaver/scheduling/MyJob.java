package org.uweaver.scheduling;

import java.time.LocalDate;

public class MyJob implements Job {

    private int counter = 0;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        counter++;
        Schedule schedule = jobExecutionContext.schedule();
        LocalDate baseline = jobExecutionContext.getDataAsLocalDate("baseline");
        System.out.println(String.format("%s: A Simple Job was executed.", schedule.name()));
        System.out.println(String.format("counter: %s", counter));
        System.out.println(String.format("baseline: %s", baseline));
        System.out.println(String.format("times of triggered: %s", jobExecutionContext.schedule().timesTriggered()));
        System.out.println(String.format("fire time: %s", jobExecutionContext.fireTime()));
    }
}
