package org.uweaver.data;

import org.uweaver.core.util.NumberConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Condition {

	private List<Filter> filters = new ArrayList<>();
	private Conjunction conjunction = Conjunction.AND;
	private NumberConverter numberConverter = new NumberConverter();
	
	public Condition() {}

	public Condition(List<Filter> filters, Conjunction conjunction) {
		if(filters!=null) this.filters = filters;
        if(conjunction!=null) this.conjunction = conjunction;
	}

	public Condition(List<Filter> filters) {
		if(filters!=null) this.filters = filters;
	}

	public Condition(Filter[] filters, Conjunction conjunction) {
		if(filters!=null) this.filters = new ArrayList<>(Arrays.asList(filters));
		if(conjunction!=null) this.conjunction = conjunction;
	}


	public Condition(Filter[] filters, String conjunction) {
		if(filters!=null) this.filters = new ArrayList<>(Arrays.asList(filters));
		if(conjunction!=null) this.conjunction = Conjunction.valueOf(conjunction);
	}

	public Condition(Filter[] filters) {
		if(filters!=null) this.filters = new ArrayList<>(Arrays.asList(filters));
	}

	public void addFilter(Filter filter) {
		filters.add(filter);
	}

	public void addFilter(String property, Object value) {
		addFilter(property, value, Filter.Operator.EQ);
	}

	public void addFilter(String property, Object value, Filter.Operator operator) {
		Filter filter = new Filter();
		filter.setProperty(property);
		filter.setValue(value);
		filter.setOperator(operator);
		filters.add(filter);
	}


	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}

    public List<Filter> getFilters() {
        return this.filters;
    }

	public void setConjunction(Conjunction conjunction) {
		this.conjunction = conjunction;
	}

	public Conjunction getConjunction() {
		return this.conjunction;
	}


    public enum Conjunction {
		AND("AND"), OR("OR");

		private final String value;

		private Conjunction(String value){
			this.value = value;
		}

		public String getValue(){
			return value;
		}

		public String toString(){
			return value;
		}
	}

}
