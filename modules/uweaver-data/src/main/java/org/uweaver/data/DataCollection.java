/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.exception.MissingAnnotationException;
import org.uweaver.core.reflection.TypeUtils;
import org.uweaver.core.util.XMap;
import org.uweaver.data.annotation.Entity;

import java.lang.reflect.Method;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataChannel obj = new DataChannel();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataCollection {

    // MYSQL: 1024; SQL Server: 900;
    private static final int MAXKEYLENGTH = 768;

    /** 名稱 */
    private String name;

    /** 欄位 */
    private XMap<String, Field> fields = new XMap<>();

    /** Primary Key */
    private List<Field> key = new ArrayList<>();

    /** Comment */
    private String comment;

//    private DataRepository dataRepository = null;

    public DataCollection(String name) {
        this.name = name;
        init();
    }

    public DataCollection(Class<?> type) {
        Entity entity = type.getAnnotation(Entity.class);
        if(entity==null) {
            throw new MissingAnnotationException("The annotation - ${0} is missing", Entity.class.getCanonicalName());
        }
        name = (entity.name().length()==0) ? type.getSimpleName() : entity.name();
        for(Method method : type.getMethods()) {
            if(TypeUtils.isSetter(method)) {
                Field field = new Field(method.getName().substring(3), method.getParameterTypes()[0]);
                addField(field);
            }
        }
        setKey(entity.key());
        int precision = MAXKEYLENGTH / entity.key().length;
        precision = precision - precision % 16;
        for(Field field : this.getFields()) {
            if(isKey(field.getName())) {
                field.setPrecision(precision);
            }
        }
    }

    private void init() {}
//
//    public void attach(DataRepository dataRepository) {
//        this.dataRepository = dataRepository;
//    }
//
//    public void detach() {
//        this.dataRepository = null;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void addField(Field field) {
        this.fields.put(field.getName(), field);
    }

    public Field getField(String name) {
        return this.fields.get(name);
    }

    public void setFields(List<Field> fields) {
        this.fields.clear();
        for(Field field : fields) {
            addField(field);
        }
    }
    public void removeField(String name) {
        this.fields.remove(name);
    }

    public List<Field> getFields() {
        List<Field> fields = new ArrayList<Field>();
        fields.addAll(this.fields.values());
        return java.util.Collections.unmodifiableList(fields);
    }

    public boolean hasField(String name) {
        return this.fields.containsKey(name);
    }

    public void setKey(String[] fields) {
        this.key.clear();
        for(String name: fields) {
            Field field = getField(name);
            if(field!=null) this.key.add(field);
        }
    }

    public void setKey(List<Field> fields) {
        this.key.clear();
        for(Field field: fields) {
            Field f = getField(field.getName());
            if(f!=null) this.key.add(f);
        }
    }

    public List<Field> getKey() {
        return java.util.Collections.unmodifiableList(this.key);
    }

    public boolean isKey(String name) {
        return this.key.contains(this.fields.get(name));
    }


}
