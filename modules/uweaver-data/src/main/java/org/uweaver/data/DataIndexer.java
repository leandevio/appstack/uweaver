/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.joda.time.DateTime;
import org.uweaver.core.doc.*;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.exception.NotFoundException;
import org.uweaver.core.file.TextFileReader;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.search.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataIndexer obj = new DataIndexer();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataIndexer {
    private static Logger LOGGER = LogManager.getLogger(DataIndexer.class);

    private JSONParser parser = new JSONParser();
    private static final String DEFAULTUPDATETIMEFIELD = "updatetime";
    private static Date smallestTime = new DateTime(1900, 1, 1, 0, 0).toDate();
    private DataRepository dataRepository;
    private IndexRepository indexRepository;
    private IndexWriter indexWriter;


    public DataIndexer(DataRepository dataRepository) {
        initialize(dataRepository, IndexRepository.getDefaultInstance());
    }

    public DataIndexer(DataRepository dataRepository, IndexRepository indexRepository) {
        initialize(dataRepository, indexRepository);
    }

    public DataRepository getDataRepository() {
        return this.dataRepository;
    }

    public IndexRepository getIndexRepository() {
        return this.indexRepository;
    }

    private void initialize(DataRepository dataRepository, IndexRepository indexRepository) {
        this.dataRepository = dataRepository;
        this.indexRepository = indexRepository;
        this.indexWriter = new IndexWriter(indexRepository);
    }

    public void index(DataCollection dataCollection) {
        index(dataCollection, dataCollection.getFields(), dataCollection.getKey());
    }

    public void index(DataCollection dataCollection, List<Field> fields) {
        index(dataCollection, fields, dataCollection.getKey());
    }

    public void index(DataCollection dataCollection, List<Field> fields, List<Field> key) {
        String updateTimeField = DEFAULTUPDATETIMEFIELD;
        if(!dataCollection.hasField(updateTimeField)) return;

        if(indexRepository.getCollection(dataCollection.getName())!=null) return;
        IndexCollection indexCollection = new IndexCollection(dataCollection.getName());
        List<Field> allFields = new ArrayList<>();
        allFields.addAll(fields); allFields.addAll(key);
        indexCollection.setFields(toSearchFields(allFields));
        indexCollection.setKey(toSearchFields(key));

        if(indexCollection.getKey().size()==0) indexCollection.setKey(indexCollection.getFields());

        indexRepository.saveCollection(indexCollection);
    }

    private List<org.uweaver.core.search.Field> toSearchFields(List<Field> fields) {
        List<org.uweaver.core.search.Field> sFields = new ArrayList<>();
        for(Field field : fields) {
            sFields.add(new org.uweaver.core.search.Field(field.getName()));
        }
        return sFields;
    }

    public void put(DataCollection dataCollection, Map<String, Object> record) {
        IndexCollection indexCollection = indexRepository.getCollection(dataCollection.getName());

        Map<String, Object> predicate = new HashMap<>();
        for(org.uweaver.core.search.Field field : indexCollection.getKey()) {
           predicate.put(field.getName(), record.get(field.getName()));
        }
        remove(dataCollection, predicate);

        // build the index of the blob filed.
        for(org.uweaver.core.search.Field field : indexCollection.getFields()) {
            Object value = record.get(field.getName());

            if(value==null) continue;

            if(dataCollection.getField(field.getName()).isLink()) {
                StringBuilder content = new StringBuilder();
                List<String> links = Arrays.asList(parser.readValue((String) value, String[].class));
                for(String link : links) {
                    try {
                        content.append(getContent(link)).append('\n');
                    }  catch(Exception e) {
                        String message = "The index of the record is not built completely." +
                                "\nRecord: \n" + parser.writeValueAsString(record) +
                                "\nField: " + field.getName() + "\nReason: ";
                        LOGGER.warn(message, e);
                    }
                }
                record.put(field.getName(), content.toString());
            }
        }
        indexWriter.add(indexCollection, record);
    }

    public void remove(DataCollection dataCollection, Map<String, Object> predicate) {
        IndexQuery query = new IndexQuery().from(dataCollection.getName());
        indexWriter.remove(query.where(predicate));
    }

    public void build() {
        for(IndexCollection collection : indexRepository.getCollections()) {
            build(collection);
        }
    }

    public void build(DataCollection dataCollection) {
        IndexCollection indexCollection = indexRepository.getCollection(dataCollection.getName());
        if(indexCollection == null) {
            String message = String.format("Index not found. %s", dataCollection.getName());
            LOGGER.debug(message);
            throw new NotFoundException(message);
        }
        build(indexCollection);
    }

    public void build(IndexCollection indexCollection) {
        String updateTimeField = DEFAULTUPDATETIMEFIELD;
        DataCollection dataCollection = dataRepository.collection(indexCollection.getName());
        if(!dataCollection.hasField(updateTimeField)) return;

        LOGGER.debug("Building the index for " + indexCollection.getName());

        Date indexUpdateTime = indexCollection.getUpdateTime()==null ? smallestTime : indexCollection.getUpdateTime();
        Date now = new Date();

        DataQuery dataQuery = new DataQuery(getDataRepository());
        Condition condition = new Condition();
        condition.addFilter(dataCollection.getField(updateTimeField).getName(), null);
        condition.addFilter(dataCollection.getField(updateTimeField).getName(), indexUpdateTime==null ? smallestTime : indexUpdateTime, Filter.Operator.GT);
        condition.setConjunction(Condition.Conjunction.OR);
        List<Record> records = dataQuery.from(dataCollection).where(condition).list();

        for(Map<String, Object> record : records) {
            put(dataCollection, record);
        }

        indexCollection.setUpdateTime(now);
        indexRepository.saveCollection(indexCollection);

        LOGGER.debug("The index for " + indexCollection.getName() + " is up-to-date.");
    }

    private String getContent(String link) {
        String content = null;

        String extension = getExtension(link);
        if(extension.equals("pptx")) {
            content = getSlideShowContent(link);
        } else if(extension.equals("xlsx")||extension.equals("xls")) {
            content = getWorkbookContent(link);
        } else if(extension.equals("docx")||extension.equals("doc")||extension.equals("pdf")) {
            content = getDocumentContent(link);
        } else if(extension.equals("txt")||extension.equals("html")||extension.equals("htm")||extension.equals("csv")) {
            content = getTextContent(link);
        }

        return content;
    }

    private String getExtension(String link) {
        int dot = link.lastIndexOf('.');
        return (dot<0) ? "" : link.substring(dot+1);
    }

    private String getTextContent(String link) {
        Path attachment = dataRepository.getBlobStorage().resolve(link);
        StringBuilder content = new StringBuilder();
        TextFileReader reader;
        InputStream in;
        try {
            in = new FileInputStream(attachment.toFile());
        } catch (FileNotFoundException e) {
            throw new ApplicationException(e);
        }
        reader = new TextFileReader(in);
        reader.open();
        for(String line=reader.readLine(); line!=null; line=reader.readLine()) {
            content.append(line).append('\n');
        }
        return content.toString();
    }

    private String getSlideShowContent(String link) {
        Path attachment = dataRepository.getBlobStorage().resolve(link);
        StringBuilder content = new StringBuilder();
        SlideShowReader reader;
        InputStream in;
        try {
            in = new FileInputStream(attachment.toFile());
        } catch (FileNotFoundException e) {
            throw new ApplicationException(e);
        }
        if(link.toString().endsWith("pptx")) {
            reader = new PptxReader(in);
        } else {
            return null;
        }

        reader.open();
        for(Slide slide : reader.readSlides()) {
            content.append(slide.getText());
        }
        reader.close();
        return content.toString();
    }

    private String getDocumentContent(String link) {
        Path attachment = dataRepository.getBlobStorage().resolve(link);
        StringBuilder content = new StringBuilder();
        DocumentReader reader;
        InputStream in = null;
        try {
            in = new FileInputStream(attachment.toFile());
        } catch (FileNotFoundException e) {
            LOGGER.warn("Exception:", e);
            return null;
        }
        if(link.toString().endsWith("docx")) {
            reader = new DocxReader();
        } else if(link.toString().endsWith("doc")) {
            reader = new DocReader();
        } else if(link.toString().endsWith("pdf")) {
            reader = new PdfReader();
        } else {
            return null;
        }
        reader.open(in);
        content.append(reader.getText());
        reader.close();
        return content.toString();
    }

    private String getWorkbookContent(String link) {
        Path attachment = dataRepository.getBlobStorage().resolve(link);
        StringBuilder content = new StringBuilder();
        WorkbookReader reader;
        InputStream in = null;
        try {
            in = new FileInputStream(attachment.toFile());
        } catch (FileNotFoundException e) {
            LOGGER.warn("Exception:", e);
            return null;
        }
        if(link.toString().endsWith("xlsx")) {
            reader = new XlsxWorkbookReader(in);
        } else if(link.toString().endsWith("xls")) {
            reader = new XlsWorkbookReader(in);
        } else {
            return null;
        }
        reader.open();
        for(int i=0; i<reader.getNumberOfSheets(); i++) {
            Sheet sheet = reader.getSheet(i);
            content.append(sheet.getText()).append('\n');
        }
        reader.close();
        return content.toString();
    }

}
