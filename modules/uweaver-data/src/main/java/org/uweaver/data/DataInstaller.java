/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.reflection.Reflections;
import org.uweaver.core.util.Environment;
import org.uweaver.data.annotation.Entity;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataInstaller obj = new DataInstaller();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataInstaller {
    private JSONParser parser = new JSONParser();
    private DataRepository dataRepository;

    public DataInstaller() {
        this(JdbcDataSource.getDefaultInstance());
    }

    public DataInstaller(DataSource dataSource) {
        this.dataRepository = new DataRepository(dataSource);
    }

    public void invalidate(Class type) {
        if (!dataRepository.hasCollection(type)) return;
        dataRepository.dropCollection(type);
        validate(type);
    }

    public void validate(Class<?> type) {
        if (dataRepository.hasCollection(type)) return;
        dataRepository.createCollection(new DataCollection(type));
    }

    public void invalidate() {
        Environment environment = Environment.getDefaultInstance();
        List<String> entities = Arrays.asList(environment.property("data.invalidate", "").split(","));
        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(Entity.class).list();
        boolean all = entities.contains("all");
        for(Class type : types) {
            String entity = type.getSimpleName();
            if(all||entities.contains(entity)) {
                invalidate(type);
            }
        }
    }

    private String decapitalize(String str) {
        char c[] = str.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }

    public <T> T read(Class<T> type) {
        Class entity;
        if(type.isArray()) {
            entity = type.getComponentType();
        } else {
            entity = type;
        }
        return readAs(entity, type);
    }

    public <T> T readAs(Class<?> entity, Class<T> type) {
        return readAs(entity.getSimpleName(), type);
    }

    public <T> T readAs(String entity, Class<T> type) {
        T data = null;
        InputStream input = open(entity);

        if (input != null) {
            data = parser.readValue(input, type);
            try {
                input.close();
            } catch (IOException e) {
                throw new org.uweaver.core.exception.IOException(e);
            }
        }
        if (data == null) {
            if (type.isArray()) {
                data = (T) Array.newInstance(type.getComponentType(), 0);
            }
        }
        return data;
    }

    private InputStream open(String entity) {
        InputStream input = null;

        String filename = entity + ".json";
        String dataResource = "/data/" + filename;
        Path dataFile = Environment.getDefaultInstance().dataFolder().resolve(filename);
        if (Files.exists(dataFile)) {
            try {
                input = new FileInputStream(dataFile.toFile());
            } catch (FileNotFoundException e) {
                throw new org.uweaver.core.exception.IOException(e);
            }
        } else if (DataInstaller.class.getClassLoader().getResource(dataResource) != null) {
            input = DataInstaller.class.getClassLoader()
                    .getResourceAsStream(dataResource);
        }

        return input;
    }
}
