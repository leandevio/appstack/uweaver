/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import oracle.sql.TIMESTAMP;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.Item;
import org.uweaver.core.reflection.TypeUtils;
import org.uweaver.data.annotation.Association;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataQuery obj = new DataQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataQuery {
    private DataRepository dataRepository = null;
    private DataCollection dataCollection = null;
    private Condition condition = new Condition();
    private List<Order> orders = new ArrayList<>();
    private Connection connection = null;
    private ResultSet resultSet = null;
    private PreparedStatement statement = null;
    private Map<Filter.Operator, String> OPERATOR = new HashMap<>();

    private static final Class[] PRIMITIVETYPE = {
            String.class, StringBuffer.class, StringBuilder.class,
            Date.class,
            Number.class,
            Boolean.class,
            byte[].class};
    private JSONParser parser = new JSONParser();
    private Converters converters = new Converters();

    public DataQuery(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.OPERATOR.put(Filter.Operator.EQ, "=");
        this.OPERATOR.put(Filter.Operator.GE, ">=");
        this.OPERATOR.put(Filter.Operator.GT, ">");
        this.OPERATOR.put(Filter.Operator.IN, "IN");
        this.OPERATOR.put(Filter.Operator.LIKE, "LIKE");
        this.OPERATOR.put(Filter.Operator.LE, "<=");
        this.OPERATOR.put(Filter.Operator.LT, "<");
    }

    public DataQuery from(DataCollection dataCollection) {
        this.dataCollection = dataCollection;
        return this;
    }

    public DataQuery where(Map<String, Object> predicate) {
        condition = new Condition();
        for(Map.Entry<String, Object> entry : predicate.entrySet()) {
            Field field = dataCollection.getField(entry.getKey());
            if(field==null) continue;
            Filter.Operator operator;
            if(entry.getValue() instanceof String && ((String) entry.getValue()).contains("%")) {
                operator = Filter.Operator.LIKE;
            } else {
                operator = Filter.Operator.EQ;
            }

            condition.addFilter(field.getName(), entry.getValue(), operator);
        }
        return this;
    }

    public DataQuery where(Condition condition) {
        this.condition = condition;
        return this;
    }

    public DataQuery sort(Order order) {
        Field field = dataCollection.getField(order.getProperty());
        if(field==null) return this;
        orders.add(order);
        return this;
    }

    public List<Record> list() {
        open();
        List<Record> records = new ArrayList<>();
        try {
            while(resultSet.next()) {
                Record record = new Record();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for(int i=1; i<=metaData.getColumnCount(); i++) {
                    String key = metaData.getColumnName(i);
                    Object value = getValue(resultSet, i);
                    record.put(key, value);
                }
                records.add(record);
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
        close();
        return records;
    }

    public <T> List<T> list(Class<T> type) {
        open();
        List<T> beans = new ArrayList<>();
        Item item = new Item();
        try {
            while(resultSet.next()) {
                item.setBean(type.newInstance());
                ResultSetMetaData metaData = resultSet.getMetaData();
                Record record = new Record();
                for(int i=1; i<=metaData.getColumnCount(); i++) {
                    String key = metaData.getColumnName(i);
                    Object value = getValue(resultSet, i);
                    record.put(key, value);
                }
                for(Map.Entry<Object, Object> entry : item.entrySet()) {
                    String key = ((String) entry.getKey());
                    if(record.containsKey(key)) {
                        String prop = (String) entry.getKey();
                        Object value;
                        if(isAssociation(item.getWriteMethod(prop))) {
                            value = fetch((String) record.get(key), item.getType(prop));
                        } else {
                            value = convert(record.get(key), item.getType(prop));
                        }
                        item.put(entry.getKey(), value);
                    }
                }
                beans.add((T) item.getBean());
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
        close();
        return beans;
    }

    private boolean isAssociation(Method method) {
        return method.getAnnotation(Association.class)!=null;
    }

    private Object fetch(String json, Type type) {
        Object value;
        Class rawType = TypeUtils.getRawType(type);
        if(java.util.Collection.class.isAssignableFrom(rawType)) {
            value = fetchCollection(json, type);
        } else {
            value = fetchBean(json, type);
        }
        return value;
    }

    private <T> java.util.Collection<T> fetchCollection(String json, Type type) {
        List<T> values = new ArrayList<>();
        Class paraType = TypeUtils.getRawType(TypeUtils.getTypeArguments(type)[0]);
        List<Map<String, Object>> predicates = (List<Map<String, Object>>) parser.readValue(json, List.class);
        DataCollection dataCollection = dataRepository.collection(paraType);
        DataQuery query = new DataQuery(dataRepository);
        query.from(dataCollection);
        for(Map<String, Object> predicate : predicates) {
            values.addAll(query.where(predicate).list(paraType));
        }
        return values;
    }

    private <T> T fetchBean(String json, Type type) {
        Class<?> rawType = TypeUtils.getRawType(type);
        Map<String, Object> predicate = (Map<String, Object>) parser.readValue(json, Map.class);
        DataCollection dataCollection = dataRepository.collection(rawType);
        DataQuery query = new DataQuery(dataRepository);
        return (T) query.from(dataCollection).where(predicate).singleResult(rawType);
    }

    public Record singleResult() {
        List<Record> records = list();
        return records.size() > 0 ? records.get(0) : null;
    }

    public <T> T singleResult(Class<T> type) {
        List<T> records = list(type);
        return records.size() > 0 ? records.get(0) : null;
    }

    private void open() {
        String name = dataCollection.getName();
        StringBuilder exprWhere = new StringBuilder();
        StringBuilder exprOrderBy = new StringBuilder();
        List<String> keys = new ArrayList();
        Map<String, Object> dataset = new HashMap<>();

        if(condition.getFilters().size()>0) {
            exprWhere.append("WHERE ");
            String conjunction = " " + condition.getConjunction().getValue() + " ";
            for(Filter filter: condition.getFilters()) {
                exprWhere.append(dataRepository.escapeIdentifier(filter.getProperty()));
                if(filter.getValue()==null) {
                    exprWhere.append(" is null");
                } else {
                    exprWhere.append(" " + toRawOperator(filter.getOperator()) + " ?");
                    dataset.put(filter.getProperty(), filter.getValue());
                    keys.add(filter.getProperty());
                }
                exprWhere.append(conjunction);
            }
            exprWhere.delete(exprWhere.length() - conjunction.length(), exprWhere.length());
        }

        if(orders.size()>0) {
             exprOrderBy.append("ORDER BY ");
            for(Order order : orders) {
                exprOrderBy.append(dataRepository.escapeIdentifier(order.getProperty()) + " " + order.getDirection().toString() + ",");
            }
            exprOrderBy.delete(exprOrderBy.length() - 1, exprOrderBy.length());
        }

        String sql = String.format("SELECT * FROM %s %s %s", dataRepository.escapeIdentifier(name), exprWhere, exprOrderBy);
        try {
            connection = dataRepository.getConnection();
            statement = connection.prepareStatement(sql);
            for(int i=0; i<keys.size(); i++) {
                Object value = dataset.get(keys.get(i));
                setValue(statement, i+1, value);
            }
            resultSet = statement.executeQuery();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private void close() {
        try {
            resultSet.close();
            statement.close();
            connection.close();
            resultSet = null;
            statement = null;
            connection = null;
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private Object convert(Object any, Type type) {
        if(any==null) return null;
        Object value;
        Class rawType = TypeUtils.getRawType(type);
        if(TypeUtils.isAssignable(any.getClass(), rawType)) {
            value = any;
        } else if(rawType.isPrimitive()) {
            value = converters.convert(any.toString(), TypeUtils.wrapper(rawType));
        } else if(TIMESTAMP.class.isAssignableFrom(any.getClass())) {
            try {
                value = ((TIMESTAMP) any).dateValue();
            } catch (SQLException e) {
                throw new ApplicationException(e);
            }
        } else {
            value = parser.readValue(any.toString(), type);
        }

        return value;
    }

    private Object getValue(ResultSet resultSet, int column) {
        Object value;
        try {
            int type = resultSet.getMetaData().getColumnType(column);
            if(type==Types.BLOB) {
                value = getAsBytes(resultSet.getBlob(column));
            } else if(type==Types.CLOB) {
                value = getAsString(resultSet.getClob(column));
            } else {
                value = resultSet.getObject(column);
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }

        return value;
    }

    private byte[] getAsBytes(Blob any) {
        if(any==null) return null;
        byte[] value;

        try (InputStream in = any.getBinaryStream()) {
            value = new byte[in.available()];
            in.read(value);
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }

        return value;
    }


    private String getAsString(Clob any) {
        if(any==null) return null;
        StringBuilder value = new StringBuilder();

        try (Reader reader = any.getCharacterStream()) {
            char[] buffer = new char[100];
            for(int bufferRead=reader.read(buffer); bufferRead>0; bufferRead=reader.read(buffer)) {
                value.append(buffer);
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }

        return value.toString();
    }

    private void setValue(PreparedStatement stmt, int index, Object value) {

        try {
            if(value==null) {
                stmt.setObject(index, value);
                return;
            }
            Class type = value.getClass();
            if(Reader.class.isAssignableFrom(type)) {
                stmt.setCharacterStream(index, (Reader) value);
            } else if(InputStream.class.isAssignableFrom(type)) {
                stmt.setBinaryStream(index, (InputStream) value);
            } else if(oracle.sql.TIMESTAMP.class.isAssignableFrom(type)) {
                stmt.setTimestamp(index, ((oracle.sql.TIMESTAMP) value).timestampValue());
            } else if(!isPrimitive(value)) {
                stmt.setString(index, parser.writeValueAsString(value));
            } else {
                if(value !=null && value instanceof Date) {
                    value = new java.sql.Date(((Date) value).getTime());
                }
                stmt.setObject(index, value);
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }


    }

    private boolean isPrimitive(Object value) {
        boolean result = false;

        if(value==null) return true;

        Class type = value.getClass();
        if(type.isPrimitive()) {
            result = true;
        } else {
            for(int i=0; i<PRIMITIVETYPE.length; i++) {
                if(PRIMITIVETYPE[i].isAssignableFrom(type)) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    private String toRawOperator(Filter.Operator operator) {
        return OPERATOR.get(operator);
    }

}
