/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import java.io.*;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataReader obj = new DataReader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataReader {
    private DataRepository dataRepository;
    private Connection connection = null;
    private DataCollection dataCollection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public DataReader(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public DataRepository getDataRepository() {
        return this.dataRepository;
    }

    public void setDataRepository(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public void open(DataCollection dataCollection) {
        this.dataCollection = dataCollection;
        this.connection = dataRepository.getConnection();
        String name = this.dataCollection.getName();
        String sql = String.format("SELECT * FROM %s", name);
        try {
            this.statement = this.connection.createStatement();
            this.resultSet = this.statement.executeQuery(sql.toString());
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public void close() {
        try {
            this.resultSet.close();
            this.statement.close();
            this.connection.close();
            this.resultSet = null;
            this.statement = null;
            this.connection = null;
            this.dataCollection = null;
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public Map<String, Object> read() {
        Map<String, Object> record = null;
        ResultSet resultSet = this.resultSet;
        try {
            if(!resultSet.next()) return record;
            record = new HashMap<>();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for(int i=1; i<=metaData.getColumnCount(); i++) {
                String key = metaData.getColumnName(i);
                Object value = getValue(resultSet, i);
                record.put(key.toLowerCase(), value);
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
        return record;
    }


    private Object getValue(ResultSet resultSet, int column) {
        Object value;
        try {
            int type = resultSet.getMetaData().getColumnType(column);
            if(type==Types.BLOB) {
                value = getAsInputStream(resultSet.getBlob(column));
            } else if(type==Types.CLOB) {
                value = getAsReader(resultSet.getClob(column));
            } else {
                value = resultSet.getObject(column);
            }
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }

        return value;
    }

    private InputStream getAsInputStream(Blob any) {
        InputStream value;

        try {
            value  = any.getBinaryStream();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }

        return value;
    }


    private Reader getAsReader(Clob any) {
        Reader value;

        try {
            value = any.getCharacterStream();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }

        return value;
    }
}
