/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.cst.DataFeature;
import org.uweaver.core.exception.MissingAnnotationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;

import javax.sql.DataSource;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataRepository obj = new DataRepository();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 *
 */
public class DataRepository {
    private static final Logger LOGGER = LogManager.getLogger(DataRepository.class);
    Environment environment = Environment.getDefaultInstance();

    public static final String MYSQL = "MySQL";
    public static final String H2 = "H2";
    public static final String ORACLE = "Oracle";
    public static final String MSSQL = "Microsoft SQL Server";
    private static final int DEFAULTSTIRNGLENGTH = 1024;
    private static final int DEFAULTPRECISION = 10;
    private static final int DEFAULTSCALE = 2;

    private DataSource dataSource;
    private Path blobStorage;
    private String schema;

    private String provider;
    private String version;
    private int majorVersion;
    private int minorVersion;

    private static final Map<String, String> ESCAPEIDENTIFIER = new HashMap<>();
    private JSONParser parser = new JSONParser();

    static {
        ESCAPEIDENTIFIER.put(MYSQL, "`%s`");
        ESCAPEIDENTIFIER.put(H2, "\"%s\"");
        ESCAPEIDENTIFIER.put(ORACLE, "\"%s\"");
        ESCAPEIDENTIFIER.put(MSSQL, "\"%s\"");
    }

    private static class SingletonHolder {
        private static final DataRepository INSTANCE = DataRepository.getInstance();
    }

    public static DataRepository getDefaultInstance() {
        return DataRepository.SingletonHolder.INSTANCE;
    }

    public static DataRepository getInstance() {
        return new DataRepository(JdbcDataSource.getDefaultInstance());
    }

    public DataRepository(DataSource dataSource, String schema) {
        this.dataSource = dataSource;
        this.schema = schema;
        init();
    }

    public DataRepository(DataSource dataSource) {
        this(dataSource, null);
    }

    private void init() {
        try {
            Connection conn = dataSource.getConnection();
            DatabaseMetaData metaData = conn.getMetaData();

            provider = metaData.getDatabaseProductName();
            version = metaData.getDatabaseProductVersion();
            majorVersion = metaData.getDatabaseMajorVersion();
            minorVersion = metaData.getDatabaseMinorVersion();

            conn.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
        this.blobStorage = environment.dataFolder();
    }

    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public Path getBlobStorage() {
        return this.blobStorage;
    }

    public void setBlobStorage(Path path) {
        this.blobStorage = path;
    }

    private String getSchema(Connection connection) {
        String schema = this.schema;
        if(this.getProvider().equals(H2)) return schema;

        try {
            schema = connection.getSchema();
        } catch (java.lang.AbstractMethodError e) {
            LOGGER.debug(e);
        } catch (SQLException e) {
            LOGGER.debug(e);
        }
        return schema;
    }

    public List<DataCollection> collections() {
        return collections(null);
    }

    public List<DataCollection> collections(String pattern) {
        List<DataCollection> collections = new ArrayList<>();

        try {
            Connection conn = dataSource.getConnection();
            DatabaseMetaData metaData = conn.getMetaData();

            String catalog = conn.getCatalog();
            String schema = getSchema(conn);
            String[] patterns = new String[]{pattern, pattern.toUpperCase()};
            for(String tableNamePattern : patterns) {
                ResultSet tables = metaData.getTables(catalog, schema, tableNamePattern, new String[]{"TABLE", "VIEW"});

                while(tables.next()) {
                    String name = tables.getString("TABLE_NAME");
                    String comment = tables.getString("REMARKS");
                    ResultSet rs = metaData.getPrimaryKeys(catalog, schema, name);
                    List<String> primaryKeys = new ArrayList<>();
                    while (rs.next()) {
                        primaryKeys.add(rs.getString("COLUMN_NAME"));
                    }
                    rs.close();

                    DataCollection dataCollection = new DataCollection(name);
                    dataCollection.setComment(comment);
                    ResultSet columns = metaData.getColumns(catalog, schema, name, null);
                    while(columns.next()) {
                        String colName = columns.getString("COLUMN_NAME");
                        int dataType = columns.getInt("DATA_TYPE");
                        int precision = columns.getInt("COLUMN_SIZE");
                        int scale = columns.getInt("DECIMAL_DIGITS");
                        String colComment = columns.getString("REMARKS");
                        Field field = new Field(colName, toType(dataType), precision, scale);
                        field.setComment(colComment);
                        dataCollection.addField(field);
                    }
                    columns.close();
                    dataCollection.setKey(primaryKeys.toArray(new String[primaryKeys.size()]));
                    collections.add(dataCollection);
                }
                tables.close();
            }
            conn.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
        return collections;
    }


    private Class toType(int dataType) {
        Class type;
        switch(dataType) {
            case Types.VARCHAR:
            case Types.NVARCHAR:
            case Types.CHAR:
            case Types.NCHAR:
                type = String.class;
                break;
            case Types.INTEGER:
            case Types.SMALLINT:
            case Types.TINYINT:
                type = Long.class;
                break;
            case Types.BIGINT:
                type = BigInteger.class;
                break;
            case Types.DOUBLE:
            case Types.FLOAT:
                type = Double.class;
                break;
            case Types.NUMERIC:
            case Types.DECIMAL:
                type = BigDecimal.class;
                break;
            case Types.DATE:
            case Types.TIMESTAMP:
            case Types.TIME:
                type = Date.class;
                break;
            case Types.BLOB:
                type = InputStream.class;
                break;
            case Types.CLOB:
            case Types.NCLOB:
                type = Reader.class;
                break;
            default:
                type = Object.class;
        }

        return type;
    }

    public String getProvider() {
        return this.provider;
    }

    public String getVersion() {
        return this.version;
    }

    public int getMajorVersion() {
        return this.majorVersion;
    }

    public int getMinorVersion() {
        return this.minorVersion;
    }

    public DataCollection createCollection(DataCollection dataCollection) {
        String sql = "";
        try {
            sql = generateDdl(dataCollection);
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            LOGGER.error(sql);
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
        return collection(dataCollection.getName());
    }


    public void dropCollection(String name) {
        try {
            DataCollection dataCollection = collection(name);
            if(dataCollection ==null) return;
            String sql = String.format("DROP TABLE %s", escapeIdentifier(dataCollection.getName()));
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public void dropCollection(Class<?> type) {
        dropCollection(new DataCollection(type).getName());
    }

    public DataCollection collection(String name) {
        List<DataCollection> collections = collections(name);
        return collections.size() > 0 ? collections.get(0) : null;
    }

    public DataCollection collection(Class<?> type) {
        return collection(new DataCollection(type).getName());
    }

    public boolean hasCollection(String name) {
        return collections(name).size()>0;
    }

    public boolean hasCollection(Class<?> type) {
        return hasCollection(new DataCollection(type).getName());
    }

    public String escapeIdentifier(String name) {
        String provider = getProvider();
        String tpl = ESCAPEIDENTIFIER.containsKey(provider) ? ESCAPEIDENTIFIER.get(provider) : "%s";

        return String.format(tpl, name);
    }

    private String generateDdl(DataCollection dataCollection) {
        String name = dataCollection.getName();
        java.util.Collection<Field> fields = dataCollection.getFields();

        StringBuilder exprFields = new StringBuilder();
        for(Field field : fields) {
            exprFields.append(generateDdl(field)).append(", ");
        }

        if(dataCollection.getKey().size()>0) {
            StringBuilder exprPks = new StringBuilder();
            for(Field pk : dataCollection.getKey()) {
                exprPks.append(escapeIdentifier(pk.getName())).append(", ");
            }
            exprPks.delete(exprPks.length() - 2, exprPks.length());
            exprFields.append(String.format("PRIMARY KEY(%s)", exprPks));
        } else {
            exprFields.delete(exprFields.length() - 2, exprFields.length());
        }

        return String.format("CREATE TABLE %s(%s)", escapeIdentifier(name), exprFields);
    }

    private String generateDdl(Field field) {
        String ddl;
        Class type = field.getType();
        String name = escapeIdentifier(field.getName());
        int precision = field.getPrecision();
        int scale = field.getScale();

        if(String.class.isAssignableFrom(type)) {
            String dbType = this.getProvider().equals(ORACLE) ? "NVARCHAR2" : "NVARCHAR";
            ddl = String.format("%s %s(%d)", name, dbType, (precision > 0 ? precision : DEFAULTSTIRNGLENGTH));
        } else if(Integer.class.isAssignableFrom(type)||int.class.isAssignableFrom(type)) {
            ddl = String.format("%s INTEGER", name);
        } else if(Long.class.isAssignableFrom(type)||long.class.isAssignableFrom(type)) {
            ddl = String.format("%s BIGINT", name);
        } else if(Float.class.isAssignableFrom(type)||float.class.isAssignableFrom(type)) {
            ddl = String.format("%s REAL", name);
        } else if(Double.class.isAssignableFrom(type)||double.class.isAssignableFrom(type)) {
            ddl = String.format("%s DOUBLE", name);
        } else if(Boolean.class.isAssignableFrom(type)||boolean.class.isAssignableFrom(type)) {
            String dbType = this.getProvider().equals(MSSQL) ? "BIT" : "BOOLEAN";
            ddl = String.format("%s %s NULL", name, dbType);
        } else if(BigDecimal.class.isAssignableFrom(type)) {
            ddl = String.format("%s DECIMAL(%d, %d)", name, (precision > 0 ? precision : DEFAULTPRECISION), (scale > 0 ? scale : DEFAULTSCALE));
        } else if(Date.class.isAssignableFrom(type)) {
            String dbType = this.getProvider().equals(MSSQL) ? "DATETIME" : "TIMESTAMP";
            ddl = String.format("%s %s NULL", name, dbType);
        } else if(InputStream.class.isAssignableFrom(type) || type.equals(byte[].class)) {
            String dbType = this.getProvider().equals(MSSQL) ? "VARBINARY(MAX)" : "BLOB";
            ddl = String.format("%s %s", name, dbType);
        } else if(Reader.class.isAssignableFrom(type)) {
            String dbType = this.getProvider().equals(MSSQL) ? "NTEXT" : "CLOB";
            ddl = String.format("%s %s", name, dbType);
        } else {
            String dbType = this.getProvider().equals(ORACLE) ? "NVARCHAR2" : "NVARCHAR";
            ddl = String.format("%s %s(%d)", name, dbType, (precision > 0 ? precision : DEFAULTSTIRNGLENGTH));
        }

        return ddl;
    }

    public void enable(DataFeature feature) {
        if(this.getProvider().equals(MYSQL)) {
            enableMySQL(feature);
        } else if(this.getProvider().equals(H2)) {
            enableH2(feature);
        }
    }

    public void disable(DataFeature feature) {
        if(this.getProvider().equals(MYSQL)) {
            disableMySQL(feature);
        } else if(this.getProvider().equals(H2)) {
            disableH2(feature);
        }
    }


    public void enableMySQL(DataFeature feature) {
        if(feature.equals(DataFeature.REFERENTIALCONSTRAINT)) {
            enableReferencialConstraintMySQL();
        }
    }

    private void enableReferencialConstraintMySQL() {
        String sql = "SET GLOBAL FOREIGN_KEY_CHECKS=1";
        try {
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private void disableMySQL(DataFeature feature) {
        if(feature.equals(DataFeature.REFERENTIALCONSTRAINT)) {
            disableReferencialConstraintMySQL();
        }
    }

    private void disableReferencialConstraintMySQL() {
        String sql = "SET GLOBAL FOREIGN_KEY_CHECKS=0";
        try {
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }


    public void enableH2(DataFeature feature) {
        if(feature.equals(DataFeature.REFERENTIALCONSTRAINT)) {
            enableReferencialConstraintH2();
        }
    }

    private void enableReferencialConstraintH2() {
        String sql = "SET REFERENTIAL_INTEGRITY TRUE";
        try {
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private void disableH2(DataFeature feature) {
        if(feature.equals(DataFeature.REFERENTIALCONSTRAINT)) {
            disableReferencialConstraintH2();
        }
    }


    private void disableReferencialConstraintH2() {
        String sql = "SET REFERENTIAL_INTEGRITY FALSE";
        try {
            Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
