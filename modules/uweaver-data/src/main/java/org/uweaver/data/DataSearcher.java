/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.search.*;

import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataSearcher obj = new DataSearcher();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataSearcher {
    private DataRepository dataRepository;
    private IndexRepository indexRepository;
    private IndexSearcher searcher;

    public DataSearcher(DataRepository dataRepository) {
        initialize(dataRepository, IndexRepository.getDefaultInstance());
    }

    public DataSearcher(DataRepository dataRepository, IndexRepository indexRepository) {
        initialize(dataRepository, indexRepository);
    }

    public DataRepository getDataRepository() {
        return this.dataRepository;
    }

    public IndexRepository getIndexRepository() {
        return this.indexRepository;
    }

    private void initialize(DataRepository dataRepository, IndexRepository indexRepository) {
        this.dataRepository = dataRepository;
        this.indexRepository = indexRepository;
        this.searcher = new IndexSearcher(indexRepository);

    }

    public Hits search(IndexQuery query, int limit, int offset) {
        return searcher.search(query, limit, offset);

    }

    public Hits search(IndexQuery query, int limit) {
        return search(query, limit, 0);
    }


    public Hits search(DataCollection dataCollection, String kql, int limit, int offset) {
        IndexQuery query = new IndexQuery().from(dataCollection.getName()).where(kql.toLowerCase());
        return search(query, limit, offset);
    }

    public Hits search(DataCollection dataCollection, String kql, int limit) {
        return search(dataCollection, kql, limit, 0);
    }


    public Hits search(DataCollection dataCollection, Map<String, Object> predicate, int limit, int offset) {
        Map<String, Object> predicateCI = new HashMap<>();
        for(Map.Entry<String, Object> entry : predicate.entrySet()) {
            predicateCI.put(entry.getKey().toLowerCase(), entry.getValue());
        }

        IndexQuery query = new IndexQuery().from(dataCollection.getName()).where(predicateCI);
        return search(query, limit, offset);
    }

    public Hits search(DataCollection dataCollection, Map<String, Object> predicate, int limit) {
        return search(dataCollection, predicate, limit, 0);
    }

    public Hits search(DataCollection dataCollection, int limit, int offset) {
        IndexQuery query = new IndexQuery().from(dataCollection.getName());
        return search(query, limit, offset);
    }

    public Hits search(DataCollection dataCollection, int limit) {
        return search(dataCollection, limit, 0);
    }

    public Document get(int docId) {
        return searcher.get(docId);
    }

    public int count(IndexQuery query) {
        return searcher.count(query);
    }

    public int count(DataCollection dataCollection, Map<String, Object> predicate) {
        Map<String, Object> predicateCI = new HashMap<>();
        for(Map.Entry<String, Object> entry : predicate.entrySet()) {
            predicateCI.put(entry.getKey().toLowerCase(), entry.getValue());
        }
        IndexQuery query = new IndexQuery().from(dataCollection.getName()).where(predicateCI);
        return count(query);
    }

    public int count(DataCollection dataCollection, String kql) {
        IndexQuery query = new IndexQuery().from(dataCollection.getName()).where(kql.toLowerCase());
        return count(query);
    }

    public int count(DataCollection dataCollection) {
        IndexQuery query = new IndexQuery().from(dataCollection.getName());
        return count(query);
    }
}
