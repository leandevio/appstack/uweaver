/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Item;
import org.uweaver.data.annotation.Association;
import org.uweaver.data.annotation.Entity;
import org.uweaver.data.annotation.Generated;

import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DataWriter obj = new DataWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DataWriter {
    private DataRepository dataRepository;
    private Connection connection = null;
    private DataCollection dataCollection = null;
    private static final Class[] PRIMITIVETYPE = {
            String.class, StringBuffer.class, StringBuilder.class,
            Date.class,
            Number.class,
            Boolean.class,
            byte[].class};
    private JSONParser parser = new JSONParser();

    public DataWriter(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public DataRepository getDataRepository() {
        return this.dataRepository;
    }

    public void setDataRepository(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }


    public void open(DataCollection dataCollection) {
        this.dataCollection = dataCollection;
        this.connection = dataRepository.getConnection();
        try {
            this.connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public void close() {
        Connection conn = this.connection;

        try {
            conn.commit();
            conn.close();
            this.connection = null;
            this.dataCollection = null;
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    public void write(Object any) {
        if(any instanceof Map) {
            writeMap((Map<String, Object>) any);
        } else if(any instanceof List) {
            writeList((List) any);
        } else {
            writeBean(any);
        }
    }

    public void remove(Object any) {
        if(any instanceof Map) {
            removeMap((Map<String, Object>) any);
        } else if(any instanceof List) {
            removeList((List) any);
        } else {
            removeEntity(any);
        }
    }

    private void removeList(List<?> data) {
        for(Object datum : data) {
            if(datum instanceof Map) {
                removeMap((Map<String, Object>) datum);
            } else {
                removeEntity(datum);
            }
        }
    }

    private void removeEntity(Object bean) {
        removeMap(getBeanPredicate(bean));
    }

    private void removeMap(Map<String, Object> predicate) {
        if(predicate==null) return;

        String name = dataRepository.escapeIdentifier(dataCollection.getName());
        StringBuilder exprWhere = new StringBuilder();
        List<String> keys = new ArrayList();

        String conjunction = "AND ";
        for(Map.Entry<String, Object> entry : predicate.entrySet()) {
            Field field = dataCollection.getField(entry.getKey());
            if(field==null) continue;
            exprWhere.append(dataRepository.escapeIdentifier(field.getName())).append(" = ? ").append(conjunction);
            keys.add(field.getName());
        }
        if(exprWhere.length()>0) exprWhere.delete(exprWhere.length() - conjunction.length(), exprWhere.length());

        String sql;
        if(exprWhere.length()>0) {
            sql = String.format("DELETE FROM %s WHERE %s", name, exprWhere);
        } else {
            sql = String.format("DELETE FROM %s", exprWhere);
        }

        Map<String, Object> dataset = new HashMap<>();
        for(Map.Entry<String, Object> datum: predicate.entrySet()) {
            dataset.put(datum.getKey().toLowerCase(), datum.getValue());
        }

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for(int i=0; i<keys.size(); i++) {
                Object value = dataset.get(keys.get(i).toLowerCase());
                setValue(statement, i+1, value);
            }
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private Map<String, Object> toMap(Object bean) {
        Item item = new Item(bean);
        Map<String, Object> data = new HashMap<>();
        for(Map.Entry<Object, Object> entry : item.entrySet()) {
            String prop = (String) entry.getKey();
            Object value;
            Method method = item.getWriteMethod(prop);
            if(method==null) continue;
            if(isGenerated(method)) {
                value = (entry.getValue()==null) ? UUID.randomUUID().toString() : entry.getValue();
            } else if(isAssociation(method)) {
                value = getPredicate(entry.getValue());
            } else {
                value = entry.getValue();
            }
            data.put((String) entry.getKey(), value);
        }
        return data;
    }


    private void writeBean(Object bean) {
        writeMap(toMap(bean));
    }

    private boolean isGenerated(Method method) {
        return method.getAnnotation(Generated.class)!=null;
    }

    private boolean isAssociation(Method method) {
        return method.getAnnotation(Association.class)!=null;
    }

    private Object getPredicate(Object any) {
        if(java.util.Collection.class.isAssignableFrom(any.getClass())) {
            return getCollectionPredicate((java.util.Collection<?>)any);
        } else {
            return getBeanPredicate(any);
        }
    }

    private List<Map<String, Object>> getCollectionPredicate(java.util.Collection<?> any) {
        List<Map<String, Object>> predicates = new ArrayList<>();
        for(Object value : any) {
            predicates.add(getBeanPredicate(value));
        }
        return predicates;
    }

    private Map<String, Object> getBeanPredicate(Object any) {
        Map<String, Object> predicate = new HashMap<>();
        Entity entity = any.getClass().getAnnotation(Entity.class);
        Item item = new Item(any);
        for(String key: entity.key()) {
            predicate.put(key, item.get(key));
        }
        return predicate;
    }


    private void writeMap(Map<String, Object> data) {
        if(data==null) return;

        String name = dataRepository.escapeIdentifier(dataCollection.getName());
        StringBuilder exprFields = new StringBuilder();
        StringBuilder exprValues = new StringBuilder();
        StringBuilder exprCondition = new StringBuilder();
        List<String> keys = new ArrayList();
        List<Field> pk = dataCollection.getKey();

        String conjunction = "AND ";
        for(Field field : pk) {
            exprCondition.append(dataRepository.escapeIdentifier(field.getName())).append(" = ? ").append(conjunction);
        }
        exprCondition.delete(exprCondition.length() - conjunction.length(), exprCondition.length());


        for(Object key: data.keySet()) {
            Field field = dataCollection.getField(key.toString());
            if(field==null) continue;
            keys.add(field.getName());
        }
        for(Object key : keys) {
            exprFields.append(dataRepository.escapeIdentifier(key.toString())).append(", ");
            exprValues.append("?, ");
        }
        exprFields.delete(exprFields.length() - 2, exprFields.length());
        exprValues.delete(exprValues.length() - 2, exprValues.length());

        String sqlCount = String.format("SELECT COUNT(*) FROM %s WHERE %s", name, exprCondition);
        String sqlDelete = String.format("DELETE FROM %s WHERE %s", name, exprCondition);
        String sql = String.format("INSERT INTO %s(%s) VALUES(%s)", name, exprFields, exprValues);

        Map<String, Object> dataset = new HashMap<>();
        for(Map.Entry<String, Object> datum: data.entrySet()) {
            dataset.put(datum.getKey().toLowerCase(), datum.getValue());
        }

        try {
            PreparedStatement statement;
            statement = connection.prepareStatement(sqlCount);
            for(int i=0; i<pk.size(); i++) {
                Object value = dataset.get(pk.get(i).getName().toLowerCase());
                setValue(statement, i+1, value);
            }
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int count = resultSet.getInt(1);
            resultSet.close();
            statement.close();
            if(count>0) {
                statement = connection.prepareStatement(sqlDelete);
                for(int i=0; i<pk.size(); i++) {
                    Object value = dataset.get(pk.get(i).getName().toLowerCase());
                    setValue(statement, i+1, value);
                }
                statement.executeUpdate();
                statement.close();
            }

            statement = connection.prepareStatement(sql);
            for(int i=0; i<keys.size(); i++) {
                Object value = dataset.get(keys.get(i).toLowerCase());
                setValue(statement, i+1, value);
            }
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private void writeList(List<?> data) {
        for(Object datum : data) {
            if(datum instanceof Map) {
                writeMap((Map<String, Object>) datum);
            } else {
                writeBean(datum);
            }
        }
    }

    public void clear() {
        String name = this.dataCollection.getName();
        Connection conn = this.connection;
        String sql = String.format("DELETE FROM %s", name);

        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }
    }

    private void setValue(PreparedStatement stmt, int index, Object value) {

        try {
            if(value==null) {
                stmt.setObject(index, value);
                return;
            }
            Class type = value.getClass();
            if(Reader.class.isAssignableFrom(type)) {
                stmt.setCharacterStream(index, (Reader) value);
            } else if(InputStream.class.isAssignableFrom(type)) {
                stmt.setBinaryStream(index, (InputStream) value);
            } else if(oracle.sql.TIMESTAMP.class.isAssignableFrom(type)) {
                stmt.setTimestamp(index, ((oracle.sql.TIMESTAMP) value).timestampValue());
            } else if(Date.class.isAssignableFrom(type)) {
                stmt.setTimestamp(index, new Timestamp(((Date) value).getTime()));
            } else if(!isPrimitive(value)) {
                stmt.setString(index, parser.writeValueAsString(value));
            } else {
                stmt.setObject(index, value);
            }

        } catch (SQLException e) {
            throw new org.uweaver.core.exception.ApplicationException(e);
        }


    }

    private boolean isPrimitive(Object value) {
        boolean result = false;

        if(value==null) return true;

        Class type = value.getClass();
        if(type.isPrimitive()) {
            result = true;
        } else {
            for(int i=0; i<PRIMITIVETYPE.length; i++) {
                if(PRIMITIVETYPE[i].isAssignableFrom(type)) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }
}
