package org.uweaver.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.search.Predicate;

import java.util.HashMap;
import java.util.Map;

public class Filter {
	final Logger logger = LoggerFactory.getLogger(Filter.class);
	private static final Map<Operator, String> SQLOPERATORS = new HashMap<>();
	
	private String property;
	private Operator operator = Operator.EQ;
	private Object value;

	static {
		SQLOPERATORS.put(Operator.EQ, "=");
		SQLOPERATORS.put(Operator.LIKE, "LIKE");
		SQLOPERATORS.put(Operator.LT, "<");
		SQLOPERATORS.put(Operator.LE, "<=");
		SQLOPERATORS.put(Operator.GT, ">");
		SQLOPERATORS.put(Operator.GE, ">=");
		SQLOPERATORS.put(Operator.IN, "IN");
	}

    public Filter(){}
    
	public Filter(String property, String operator, Object value) {
		this.property = property;
		if(operator!=null) this.operator = Operator.valueOf(operator);
		this.value = value;
	}

	public Filter(String property, Operator operator, Object value) {
		this.property = property;
		if(operator!=null) this.operator = operator;
		this.value = value;
	}
	
	public Filter(String property, Object value) {
		this.property = property;
		this.value = value;
	}

	public Filter(Predicate predicate) {
    	this.property = predicate.subject();
    	this.operator = Operator.valueOf(predicate.comparator().value().toUpperCase());
    	this.value = predicate.expectation();
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getSQLOperator() {
		return SQLOPERATORS.get(this.operator);
	}

    public enum Operator {
        LIKE("LIKE"), EQ("EQ"), LT("LT"), LE("LE"), GT("GT"), GE("GE"), IN("IN");

        private final String value;

        private Operator(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

        public String toString(){
            return value;
        }
    }
}
