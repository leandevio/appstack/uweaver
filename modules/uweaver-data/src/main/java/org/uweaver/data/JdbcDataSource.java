/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.apache.commons.dbcp2.BasicDataSource;
import org.h2.tools.Server;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.event.server.StopEvent;
import org.uweaver.core.log.LoggerManager;
import org.uweaver.core.observer.annotation.Subscribe;
import org.uweaver.core.util.Environment;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * JdbcDataSource ds = new JdbcDataSource();
 * ds.setDriver("org.h2.Driver");
 * ds.setUrl("jdbc:h2:file:~/project/flora/va/data/floraDB");
 * ds.setUsername("floramgr");
 * ds.setPassword("flora1943");
 *
 * Connection conn = ds.getConnection();
 * String sql = "select * from Product";
 * PreparedStatement stmt = conn.prepareStatement(sql);
 * ResultSet rs = stmt.executeQuery(sql);
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@AppListener
public class JdbcDataSource implements DataSource {
    private static final org.uweaver.core.log.Logger LOGGER = LoggerManager.getLogger(JdbcDataSource.class);
    private static String DEFAULTDRIVER = "org.h2.Driver";
    private static String DEFAULTFILEURL = "jdbc:h2:file:%s;MV_STORE=TRUE;MODE=Oracle;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=1";
    private static String DEFAULTTCPURL = "jdbc:h2:tcp://localhost:%s%s;MV_STORE=TRUE;MODE=Oracle;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=1";
    private static String DEFAULTUSERNAME = "admin";
    private static int DEFAULTMAXPOOLSIZE = 15;
    private static int DEFAULTINITPOOLSIZE = 5;
    private static Server server = null;
    private static Server webServer = null;
    private static String VALIDATIONQUERY = "select 1";
    private static Map<String, String> VALIDATIONQUERYS = new HashMap<>();

    private BasicDataSource dataSourceImpl = null;

    static {
        Environment environment = Environment.getDefaultInstance();
        if(environment.propertyAsBoolean("db.engine.enable")) {
            String port = environment.property("db.engine.port", "9092");
            try {
                server = Server.createTcpServer( "-tcpPort", port, "-tcpAllowOthers").start();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if(environment.propertyAsBoolean("db.web.enable")) {
            String port = environment.property("db.web.port", "8082");
            try {
                webServer = Server.createWebServer("-webAllowOthers","-webPort", port).start();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        VALIDATIONQUERYS.put("org.h2.Driver", " select 1");
        VALIDATIONQUERYS.put("oracle.jdbc.OracleDriver", "select 1 from dual");
        VALIDATIONQUERYS.put("com.microsoft.sqlserver.jdbc.SQLServerDriver", "select 1");
        VALIDATIONQUERYS.put("com.mysql.jdbc.Driver", "select 1");
    }


    private static class SingletonHolder {
        private static final JdbcDataSource INSTANCE = JdbcDataSource.getInstance();
    }

    public static JdbcDataSource getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    public static JdbcDataSource getInstance() {
        return getInstance(Environment.getDefaultInstance());

    }

    public static JdbcDataSource getInstance(Environment environment) {
        Path db = environment.dataFolder().resolve(environment.appName() + "DB");
        String url;
        if(server==null) {
            url = String.format(DEFAULTFILEURL, db.toString());
        } else {
            String relativePath = "/~/" + Paths.get(System.getProperty("user.home")).relativize(db);
            url = String.format(DEFAULTTCPURL, server.getPort(), relativePath);
        }

        JdbcDataSource jdbcDataSource = new JdbcDataSource();
        jdbcDataSource.setDriver(environment.property("db.driver", DEFAULTDRIVER));
        jdbcDataSource.setUrl(environment.property("db.url", url));
        jdbcDataSource.setUsername(environment.property("db.username", DEFAULTUSERNAME));
        jdbcDataSource.setPassword(environment.property("db.password"));
        jdbcDataSource.setMaxPoolSize(environment.propertyAsInteger("db.pool.max", DEFAULTMAXPOOLSIZE));
        jdbcDataSource.setInitPoolSize(environment.propertyAsInteger("db.pool.initial", DEFAULTINITPOOLSIZE));
        // BasicDataSource call the driver's isValid() method to validate the connection by default.
        if(environment.property("db.validationQuery")!=null) {
            jdbcDataSource.setValidationQuery(environment.property("db.validationQuery"));
        }
        return jdbcDataSource;
    }

    private static String getDefaultValidationQuery(String driverClassName) {
        return VALIDATIONQUERYS.containsKey(driverClassName) ? VALIDATIONQUERYS.get(driverClassName) : VALIDATIONQUERY;
    }

    public JdbcDataSource() {
        dataSourceImpl = new BasicDataSource();
        dataSourceImpl.setMaxTotal(DEFAULTMAXPOOLSIZE);
        dataSourceImpl.setInitialSize(DEFAULTINITPOOLSIZE);
    }

    public void setDriver(String driver) {
        dataSourceImpl.setDriverClassName(driver);
    }

    public String driverClassName() {
        return dataSourceImpl.getDriverClassName();
    }

    public void setUrl(String url) {
        dataSourceImpl.setUrl(url);
    }

    public void setUsername(String username) {
        dataSourceImpl.setUsername(username);
    }

    public void setPassword(String password) {
        dataSourceImpl.setPassword(password);
    }

    public void close() throws SQLException {
        dataSourceImpl.close();
    }

    public boolean isClosed() {
        return dataSourceImpl.isClosed();
    }

    public int getNumActive() {
        return dataSourceImpl.getNumActive();
    }

    public int getNumIdle() {
        return dataSourceImpl.getNumIdle();
    }

    public void setValidationQuery(String validationQuery) {
        dataSourceImpl.setValidationQuery(validationQuery);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getConnection(null, null);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return dataSourceImpl.getConnection();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return dataSourceImpl.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        dataSourceImpl.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        dataSourceImpl.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return dataSourceImpl.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return getParentLogger();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return isWrapperFor(iface);
    }

    public void setMaxPoolSize(int maxPoolSize) {
        dataSourceImpl.setMaxTotal(maxPoolSize);
    }

    public int getMaxPoolSize() {
        return dataSourceImpl.getMaxTotal();
    }

    public void setInitPoolSize(int initPoolSize) {
        dataSourceImpl.setInitialSize(initPoolSize);
    }

    public int getInitPoolSize() {
        return dataSourceImpl.getInitialSize();
    }

    @Subscribe
    public void onStop(StopEvent event) {
        if(server==null) return;
        LOGGER.debug("H2 Server is going to shutdown.");
        server.stop();
        server.shutdown();
        webServer.stop();
        webServer.shutdown();
    }
}
