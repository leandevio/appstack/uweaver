/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.exception.OperationFailureException;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;

import javax.sql.DataSource;
import java.sql.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * JdbcNumberSequence obj = new JdbcNumberSequence();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class NumberSequence {
    private static Logger LOGGER = LogManager.getLogger(NumberSequence.class);
    DataSource dataSource;
    private DataRepository dataRepository;

    private static class SingletonHolder {
        private static final NumberSequence INSTANCE = new NumberSequence();
    }

    public static NumberSequence getInstance() {
        return NumberSequence.SingletonHolder.INSTANCE;
    }

    private NumberSequence() {
        this.dataSource = JdbcDataSource.getDefaultInstance();;
        this.dataRepository = new DataRepository(dataSource);
        init(dataSource);
    }

    public NumberSequence(DataSource dataSource) {
        init(dataSource);
    }

    private void init(DataSource dataSource) {
        LOGGER.debug("Initialize NumberSequence - UW_SEQUENCENUMBER");
        try {
            if(!dataRepository.hasCollection("UW_SEQUENCENUMBER")) {
                dataRepository.createCollection(new DataCollection("UW_SEQUENCENUMBER"));
            }
        } catch (Exception e) {
            LOGGER.debug("Failed to initialize the NumberSequence - UW_SEQUENCENUMBER", e);
            throw new OperationFailureException("Failed to initialize the NumberSequence - UW_SEQUENCENUMBER", e);
        }
        LOGGER.debug("NumberSequence initialized");
    }

    public void reset(String name, long value) {
        Connection conn = null; PreparedStatement stmt = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(true);
            stmt = conn.prepareStatement("delete from UW_SEQUENCENUMBER where NAME = ?");
            stmt.setString(1, name);
            stmt.execute();
            stmt = conn.prepareStatement("insert into UW_SEQUENCENUMBER(NAME, VALUE) values(?, ?)");
            stmt.setString(1, name);
            stmt.setLong(2, value);
            stmt.execute();
        } catch (SQLException e) {
            throw new OperationFailureException(e);
        } finally {
            try {
                if(stmt!=null) stmt.close();
                if(conn!=null) conn.close();
            } catch (SQLException e) {
                LOGGER.warn("Exception: ", e);
            }
        }
    }

    public long nextNumber(String name) {
        return nextNumber(name, 1);
    }

    public long nextNumber(String name, int increment) {
        Long number = 1L;

        Connection conn = null; PreparedStatement stmt = null; ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(true);
            stmt = conn.prepareStatement("select VALUE from UW_SEQUENCENUMBER where NAME = ?");
            stmt.setString(1, name);
            LOGGER.debug("Execute: " + "select VALUE from UW_SEQUENCENUMBER where NAME = ?");
            rs = stmt.executeQuery();
            if(rs.next()) {
                number = rs.getLong(1) + increment;
                rs.close(); stmt.close();
                stmt = conn.prepareStatement("update UW_SEQUENCENUMBER set VALUE = ? where NAME = ?");
                stmt.setLong(1, number);
                stmt.setString(2, name);
                stmt.execute();
            } else {
                this.reset(name, number);
            }

        } catch (SQLException e) {
            throw new OperationFailureException(e);
        } finally {
            try {
                if(stmt!=null) stmt.close();
                if(rs!=null) rs.close();
                if(conn!=null) conn.close();
            } catch (SQLException e) {
                LOGGER.warn("Exception: ", e);
            }
        }

        return number;
    }

    public long currentNumber(String name) {
        long number = 1;

        Connection conn = null; PreparedStatement stmt = null; ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(true);
            stmt = conn.prepareStatement("select VALUE from UW_SEQUENCENUMBER where NAME = ?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            if(rs.next()) {
                number = rs.getLong(1);
                rs.close(); stmt.close();
            } else {
                this.reset(name, number);
            }

        } catch (SQLException e) {
            throw new OperationFailureException(e);
        } finally {
            try {
                if(stmt!=null) stmt.close();
                if(rs!=null) rs.close();
                if(conn!=null) conn.close();
            } catch (SQLException e) {
                LOGGER.warn("Exception: ", e);
            }
        }

        return number;
    }
}
