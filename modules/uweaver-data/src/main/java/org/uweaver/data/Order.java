/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Order obj = new Order();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Order {
    private String property;
    private Direction direction = Direction.ASC; // ASC or DESC

    public Order(String property, Direction direction) {
        this.property = property;
        if(direction!=null) this.direction = direction;
    }

    public Order(String property){
        this(property, Direction.ASC);
    }

    public Order(String property, String direction) {
        this(property, Direction.valueOf(direction));
    }

    public static Order asc(String property) {
        return new Order(property, Direction.ASC);
    }

    public static Order desc(String property) {
        return new Order(property, Direction.DESC);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }


    public Direction getDirection() {
        return direction;
    }


    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public enum Direction {
        ASC("ASC"), DESC("DESC");

        private final String value;

        Direction(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

        public String toString(){
            return value;
        }
    }
}
