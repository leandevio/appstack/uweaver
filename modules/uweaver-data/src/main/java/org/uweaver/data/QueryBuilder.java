/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import java.util.Arrays;

/**
 * ## SQLBuilder
 * ### The class implements ... 
 *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SQLBuilder obj = new SQLBuilder();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class QueryBuilder {
    private Class<?> entity = null;
    private String entityName = null;
    private String entityAlias = null;
    private Condition condition = null;
    private Iterable<Sorter> sorters = null;
    private String selectExpr = null;

    public QueryBuilder() {}

    public QueryBuilder select(String expr) {
        this.selectExpr = expr;
        return this;
    }

    public QueryBuilder from(Class<?> entity, String entityAlias) {
        this.entity = entity;
        this.entityName = entity.getSimpleName();
        this.entityAlias = entityAlias;
        return this;
    }

    public QueryBuilder from(Class<?> entity) {
        return from(entity, null);
    }

    public QueryBuilder from(String entityName, String entityAlias) {
        this.entityName = entityName;
        this.entityAlias = entityAlias;
        return this;
    }

    public QueryBuilder from(String entityName) {
        return from(entityName, null);
    }


    public QueryBuilder where(Condition condition) {
        this.condition = condition;
        return this;
    }

    public QueryBuilder orderBy(Iterable<Sorter> sorters) {
        this.sorters = sorters;
        return this;
    }


    public QueryBuilder orderBy(Sorter[] sorters) {
        return orderBy(Arrays.asList(sorters));
    }

    public String sql() {
        return null;
    }

    protected Class<?> getEntity() {
        return this.entity;
    }

    protected String getEntityName() {
        return this.entityName;
    }

    protected String getEntityAlias() {
        return this.entityAlias;
    }

    protected Condition getCondition() {
        return this.condition;
    }

    protected Iterable<Sorter> getSorters() {
        return this.sorters;
    }

    protected String getSelectExpr() {
        return this.selectExpr;
    }

    public void reset() {
        entity = null;
        entityName = null;
        entityAlias = null;
        condition = null;
        sorters = null;
        selectExpr = null;
    }

}
