package org.uweaver.data;

public class Sorter {
	private String property;
	private Direction direction = Direction.ASC; // ASC or DESC

    public Sorter(){}
    
	public Sorter(String property, String direction) {
		this.property = property;
		if(direction!=null) this.direction = Direction.valueOf(direction);
	}

	public Sorter(String property, Direction direction) {
		this.property = property;
		this.direction = direction;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}


	public Direction getDirection() {
		return direction;
	}


	public void setDirection(Direction direction) {
		this.direction = direction;
	}

    public enum Direction {
        ASC("ASC"), DESC("DESC");

        private final String value;

        private Direction(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

        public String toString(){
            return value;
        }
    }
}
