/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data.store;

import org.uweaver.core.exception.IOException;
import org.uweaver.core.exception.InvalidFormatException;
import org.uweaver.core.exception.NotFoundException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * JSONStore obj = new JSONStore();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class JSONStore {
    private Path dataFolder = Environment.getDefaultInstance().dataFolder();
    private JSONParser parser = new JSONParser();
    private Pattern regexp = Pattern.compile("/(.*)$");

    private InputStreamReader open(String collection) {
        InputStreamReader input;
        Path file = dataFolder.resolve(collection + ".json");

        try {
            input = new InputStreamReader(new FileInputStream(file.toFile()));
        } catch (FileNotFoundException e) {
            throw new NotFoundException("Collection({0}) not found.", collection);
        }

        return input;
    }

    private String getOne(String uri) {
        String json = null;
        Matcher matcher = regexp.matcher(uri);
        if(!matcher.find())  throw new InvalidFormatException("{0} not a valid URI.", uri);

        String collection = matcher.replaceFirst("");
        String id = matcher.group(1);
        InputStreamReader input = open(collection);
        Map<String, Object>[] beans = parser.readValue(input, HashMap[].class);
        Map<String, Object> bean = null;
        for(Map<String, Object> any : beans) {
            if(any.get("_id").equals(id)) {
                bean = any;
            }
        }
        if(bean!=null) {
            json = parser.writeValueAsString(bean);
        }
        try {
            input.close();
        } catch (java.io.IOException e) {
            throw new IOException(e);
        }

        return json;
    }

    private String getAll(String uri) {
        String collection = uri;
        InputStreamReader input = open(collection);

        BufferedReader reader = new BufferedReader(input);
        StringBuilder buffer = new StringBuilder();
        String line;
        try {
            while((line =reader.readLine())!=null) {
                buffer.append(line);
            }
            input.close();
        } catch (java.io.IOException e) {
            throw new IOException(e);
        }
        return buffer.toString();
    }

    public String get(String uri) {
        return isCollection(uri) ? getAll(uri) : getOne(uri);
    }

    private boolean isCollection(String uri) {
        Path file = dataFolder.resolve(uri + ".json");
        return Files.exists(file);
    }

    public String post(String pathInfo, String json) {

        return "";

    }

    public String post(String pathInfo, Object json) {

        return "";

    }

    public String put(String pathInfo, String json) {

        return "";

    }

    public String put(String pathInfo, Object json) {

        return "";

    }

    public String patch(String pathInfo, String json) {

        return "";

    }

    public String put(String pathInfo, Map<String, Object> json) {

        return "";

    }

    public String delete(String pathInfo) {

        return "";

    }

    public JSONStore() {}

    public JSONStore(Path dataFolder) {
        this.dataFolder = dataFolder;
    }
}
