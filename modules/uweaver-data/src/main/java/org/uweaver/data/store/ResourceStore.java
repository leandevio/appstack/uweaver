/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data.store;

import org.uweaver.core.content.ContentAnalyzer;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.MediaType;
import java.io.InputStream;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ResourceStore obj = new ResourceStore();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ResourceStore {
    private ContentAnalyzer analyzer = new ContentAnalyzer();

    public boolean exists(String uri) {
        return (ResourceStore.class.getClassLoader().getResource(uri)!=null);
    }

    public MediaType mediaType(String uri) {
        InputStream input = ResourceStore.class.getClassLoader()
                .getResourceAsStream(uri);
        return analyzer.detect(input);
    }

    public InputStream open(String uri) {
        InputStream input = ResourceStore.class.getClassLoader()
                .getResourceAsStream(uri);
        return input;
    }

}
