package org.uweaver.data;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.search.*;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 11/30/16.
 */
public class DataIndexerTest {
    Environment environment = Environment.getDefaultInstance();
    @BeforeTest
    public void beforeTest() throws Exception {
        Installer installer = Installer.getDefaultInstance();
        installer.resetData();
        IndexRepository.getDefaultInstance().reset();
    }

    @Test
    public void testIndex() throws Exception {
        DataRepository dataRepository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(dataRepository);

        DataCollection product = dataRepository.collection("product");
        product.getField("attachments").setLink(true);
        Field[] fields = {product.getField("publisher"), product.getField("name"), product.getField("attachments")};
        Field[] key = {product.getField("id")};
        indexer.index(product, Arrays.asList(fields));

        DataCollection productView = dataRepository.collection("productView");
        indexer.index(productView, Arrays.asList(fields), Arrays.asList(key));

        IndexRepository indexRepository = IndexRepository.getDefaultInstance();
        IndexCollection productIndex = indexRepository.getCollection(product.getName());
        IndexCollection productViewIndex = indexRepository.getCollection(productView.getName());
        assertNull(productIndex.getUpdateTime());
        assertNull(productViewIndex.getUpdateTime());
        for(Field field : fields) {
            assertNotNull(productIndex.getField(field.getName()));
            assertNotNull(productViewIndex.getField(field.getName()));
        }
        for(Field field : key) {
            assertNotNull(productIndex.isKey(field.getName()));
            assertNotNull(productViewIndex.getField(field.getName()));
        }

        indexRepository.scan();
        productIndex = indexRepository.getCollection(product.getName());
        productViewIndex = indexRepository.getCollection(productView.getName());
        assertNull(productIndex.getUpdateTime());
        assertNull(productViewIndex.getUpdateTime());
        for(Field field : fields) {
            assertNotNull(productIndex.getField(field.getName()));
            assertNotNull(productViewIndex.getField(field.getName()));
        }
        for(Field field : key) {
            assertNotNull(productIndex.isKey(field.getName()));
            assertNotNull(productViewIndex.getField(field.getName()));
        }

    }



    @Test(dependsOnMethods = "testIndex")
    public void testBuild() throws Exception {
        DataRepository dataRepository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(dataRepository);
        IndexSearcher searcher = new IndexSearcher(indexer.getIndexRepository());

        DataCollection product = dataRepository.collection("product");
        IndexQuery query = new IndexQuery();
        query.from(product.getName());
        String kql = "attachments:\"職棒\"";

        assertEquals(searcher.count(query.where(kql)), 0);
        indexer.build(product);
        assertEquals(searcher.count(query.where(kql)), 2);

        DataQuery dataQuery = new DataQuery(dataRepository);
        List<Record> records = dataQuery.from(product).list();

        indexer.remove(product, records.get(0));
        assertEquals(searcher.count(query.where("")), 4);

    }

    @Test(dependsOnMethods = "testIndex")
    public void testReindex() throws Exception {
        DataRepository dataRepository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(dataRepository);

        DataCollection productView = dataRepository.collection("productView");
        Field[] fields = {productView.getField("publisher"), productView.getField("name")};
        Field[] key = {productView.getField("id")};

        indexer.build(productView);
        indexer.index(productView, Arrays.asList(fields));

        IndexRepository indexRepository = IndexRepository.getDefaultInstance();
        IndexCollection productViewIndex = indexRepository.getCollection(productView.getName());
        assertNotNull(productViewIndex.getUpdateTime());
        for(Field field : fields) {
            assertNotNull(productViewIndex.getField(field.getName()));
        }
        for(Field field : key) {
            assertNotNull(productViewIndex.isKey(field.getName()));
        }
    }

}