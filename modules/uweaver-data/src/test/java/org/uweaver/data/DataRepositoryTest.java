package org.uweaver.data;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;

import java.io.*;
import java.util.Arrays;
import java.util.Map;

import static org.testng.Assert.assertEquals;

/**
 * Created by jasonlin on 7/12/16.
 */
public class DataRepositoryTest {
    JSONParser parser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    java.util.List<Map> products;

    JdbcDataSource dataSource;
    DataRepository dataRepository;

    @BeforeTest
    public void beforeTest() throws Exception {
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + "products.json");
        products = Arrays.asList(parser.readValue(in, Map[].class));
        in.close();
        for(Map product: products) {
            InputStream coverData = clazz.getClassLoader().getResourceAsStream(path + File.separator + product.get("cover"));
            byte[] cover = new byte[coverData.available()];
            coverData.read(cover);
            product.put("cover", new ByteArrayInputStream(cover));
            coverData.close();

            product.put("description", new StringReader((String) product.get("description")));
        }

        dataRepository = DataRepository.getDefaultInstance();
    }

    @Test
    public void testGetInstance() throws Exception {
        assertEquals(dataRepository.getProvider(), "H2");
        assertEquals(dataRepository.getMajorVersion(), 1);
        assertEquals(dataRepository.getMinorVersion(), 4);
    }

    @Test
    public void testCreateCollection() throws Exception {
        if(dataRepository.hasCollection("product")) {
            dataRepository.dropCollection("product");
        }

        DataCollection dataCollection = new DataCollection("product");

        Map<String, Object> product = products.get(0);

        for(Map.Entry entry: product.entrySet()) {
            String name = (String) entry.getKey();
            Object value = entry.getValue();
            Field field = new Field(name, value.getClass());
            dataCollection.addField(field);
        }

        dataRepository.createCollection(dataCollection);
    }

}