package org.uweaver.data;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.search.Document;
import org.uweaver.core.search.Hit;
import org.uweaver.core.search.Hits;
import org.uweaver.core.search.IndexRepository;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 11/21/16.
 */
public class DataSearcherTest {

    @BeforeTest
    public void beforeTest() throws Exception {
        Installer installer = Installer.getDefaultInstance();
        installer.resetData();
        IndexRepository.getDefaultInstance().reset();

        DataRepository repository = DataRepository.getDefaultInstance();

        DataCollection product = repository.collection("product");
        DataIndexer indexer = new DataIndexer(repository);
        Field[] fields = {product.getField("publisher"), product.getField("name")};
        Field[] key = {product.getField("id")};
        indexer.index(product, Arrays.asList(fields));

        DataCollection productView = repository.collection("productView");
        indexer.index(productView, Arrays.asList(fields), Arrays.asList(key));
    }

    @Test
    public void testSearchWithPK() throws Exception {
        DataRepository repository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(repository);

        DataCollection product = repository.collection("product");
        indexer.build(product);

        DataSearcher searcher = new DataSearcher(repository);
        Map<String, Object> predicate = new HashMap<>();
        int limit = 10;
        predicate.put("publisher", "activision");
        predicate.put("name", "職棒");
        int count = searcher.count(product, predicate);
        Hits hits = searcher.search(product, predicate, limit);

        assertEquals(count, 3);
        assertEquals(hits.size(), 3);
        assertEquals(hits.getTotal(), 3);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("name") + "\t" + document.get("publisher"));
        }
    }

    @Test
    public void testSearchWithKey() throws Exception {
        DataRepository repository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(repository);

        DataCollection productView = repository.collection("productView");
        indexer.build(productView);

        DataSearcher searcher = new DataSearcher(repository);
        Map<String, Object> predicate = new HashMap<>();
        int limit = 10;
        predicate.put("publisher", "activision");
        predicate.put("name", "職棒");
        int count = searcher.count(productView, predicate);
        Hits hits = searcher.search(productView, predicate, limit);

        assertEquals(count, 3);
        assertEquals(hits.size(), 3);
        assertEquals(hits.getTotal(), 3);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("name") + "\t" + document.get("publisher"));
        }
    }


    @Test
    public void testSearchByKQL() throws Exception {
        DataRepository repository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(repository);

        DataCollection product = repository.collection("product");
        indexer.build(product);

        DataCollection productView = repository.collection("productView");
        indexer.build(productView);

        DataSearcher searcher = new DataSearcher(repository);
        String kql = "publisher:\"activision\" or name:\"職棒\"";
        int limit = 10;
        int count = searcher.count(product, kql);
        Hits hits = searcher.search(product, kql, limit);

        assertEquals(count, 3);
        assertEquals(hits.size(), 3);
        assertEquals(hits.getTotal(), 3);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("name") + "\t" + document.get("publisher"));
        }
    }

    @Test
    public void testSearchOffset() throws Exception {
        DataRepository repository = DataRepository.getDefaultInstance();
        DataIndexer indexer = new DataIndexer(repository);

        DataCollection product = repository.collection("product");
        indexer.build(product);

        DataSearcher searcher = new DataSearcher(repository);
        int limit = 2, offset = 3;
        int count = searcher.count(product);
        Hits hits = searcher.search(product, limit, offset);

        assertEquals(count, 5);
        assertEquals(hits.size(), limit);
        assertEquals(hits.getTotal(), 5);
        System.out.println("Found " + hits.size() + " hits.");
        for(Hit hit : hits) {
            int docId = hit.getDoc();
            Document document = searcher.get(docId);
            System.out.println(document.get("name") + "\t" + document.get("publisher"));
        }
    }
}