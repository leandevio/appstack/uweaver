package org.uweaver.data;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.cst.DataFeature;
import org.uweaver.core.json.JSONParser;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by jasonlin on 7/13/16.
 */
public class DataWriterTest {
    JSONParser parser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    java.util.List<Map> products;

    JdbcDataSource dataSource;
    DataRepository dataRepository;

    @BeforeTest
    public void beforeTest() throws Exception {
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + "products.json");
        products = Arrays.asList(parser.readValue(in, Map[].class));
        in.close();

        for(Map product: products) {
            InputStream coverData = clazz.getClassLoader().getResourceAsStream(path + File.separator + product.get("cover"));
            byte[] cover = new byte[coverData.available()];
            coverData.read(cover);
            product.put("cover", cover);
            coverData.close();

            product.put("description", new StringReader((String) product.get("description")));
        }

        dataRepository = DataRepository.getDefaultInstance();

        if(dataRepository.hasCollection("product")) dataRepository.dropCollection("product");
        DataCollection dataCollection = new DataCollection("product");

        Map<String, Object> product = products.get(0);

        for(Map.Entry entry: product.entrySet()) {
            String name = (String) entry.getKey();
            Object value = entry.getValue();
            Field field = new Field(name, value.getClass());
            dataCollection.addField(field);
        }

        dataRepository.createCollection(dataCollection);
    }


//    @Test
//    public void testWriteList() throws Exception {
//        DataWriter writer = new DataWriter(dataRepository);
//
//        Collection collection = dataRepository.getCollection("product");
//
//        writer.open(collection);
//        writer.write(products);
//        writer.close();
//    }

//    @Test(dependsOnMethods = "testWriteList")
//    public void testWriteReader() throws Exception {
//        String name = "testWriteReader".toUpperCase();
//        DataWriter writer = new DataWriter(dataRepository);
//        DataReader reader = new DataReader(dataRepository);
//
//        Collection source = dataRepository.getCollection("product");
//
//        if(dataRepository.hasCollection(name)) dataRepository.dropCollection(name);
//        Collection target = new Collection(name);
//        for(Field field: source.getFields()) {
//            target.addField(field);
//        }
//        target.removeField("modes");
//
//        dataRepository.createCollection(target);
//
//        reader.open(source); writer.open(target);
//
//        Map<String, Object> record;
//        while((record = reader.read())!=null) {
//            writer.write(record);
//        }
//        writer.close();
//        reader.close();
//    }

//    @Test(dependsOnMethods = "testWriteReader")
//    public void testClear() throws Exception {
//        String name = "testClear".toUpperCase();
//        DataWriter writer = new DataWriter(dataRepository);
//        DataReader reader = new DataReader(dataRepository);
//
//        Collection source = dataRepository.getCollection("product");
//
//        if(dataRepository.hasCollection(name)) dataRepository.dropCollection(name);
//        Collection target = new Collection(name);
//        for(Field field: source.getFields()) {
//            target.addField(field);
//        }
//        target.removeField("modes");
//
//        dataRepository.createCollection(target);
//
//        reader.open(source); writer.open(target);
//
//        Map<String, Object> record;
//        while((record = reader.read())!=null) {
//            writer.write(record);
//        }
//        writer.close();
//        reader.close();
//
//        writer.open(target);
//        writer.clear();
//        writer.close();
//    }

    @Test
    public void migrateCrmDB() throws Exception {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setDriver("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://mysql.leandev.com.tw:3306/crmDB?useUnicode=true&characterEncoding=UTF-8");
        dataSource.setUsername("crmmgr");
        dataSource.setPassword("crm1943");

        DataRepository source = new DataRepository(dataSource, "CRMMGR");

        dataSource = new JdbcDataSource();
        dataSource.setDriver("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:tcp://localhost/~/project/crm/data/crmDB;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=2");
        dataSource.setUsername("crmmgr");
        dataSource.setPassword("crm1943");

        DataRepository target = new DataRepository(dataSource);
        target.disable(DataFeature.REFERENTIALCONSTRAINT);
        ((JdbcDataSource) target.getDataSource()).close();

        DataReader reader = new DataReader(source);
        DataWriter writer = new DataWriter(target);
        for(DataCollection dataCollection : source.collections()) {
            if(!target.hasCollection(dataCollection.getName())) {
                System.out.println(dataCollection.getName());
                continue;
            }

            DataCollection output = target.collection(dataCollection.getName());
            System.out.println(String.format("%s: start writing. ", output.getName()));
            reader.open(dataCollection); writer.open(output);
            writer.clear();
            Map<String, Object> record;
            int count = 0;
            while((record = reader.read())!=null) {
                writer.write(record);
                count++;
                if(count%5000==0) {
                    System.out.println(String.format("%s: progress = %d", output.getName(), count));
                }
            }
            System.out.println(String.format("%s: total = %d", output.getName(), count));

            reader.close(); writer.close();
        }

        target.enable(DataFeature.REFERENTIALCONSTRAINT);
    }
//
//    @Test
//    public void migrateCentralDB() throws Exception {
//        JdbcDataSource dataSource = new JdbcDataSource();
//        dataSource.setDriver("com.mysql.jdbc.Driver");
//        dataSource.setUrl("jdbc:mysql://mysql.leandev.com.tw:3306/centralDB?useUnicode=true&characterEncoding=UTF-8");
//        dataSource.setUsername("crmmgr");
//        dataSource.setPassword("crm1943");
//
//        DataRepository source = new DataRepository(dataSource, "CRMMGR");
//
//        dataSource = new JdbcDataSource();
//        dataSource.setDriver("org.h2.Driver");
//        dataSource.setUrl("jdbc:h2:tcp://localhost/~/project/crm/data/centralDB;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=2");
//        dataSource.setUsername("crmmgr");
//        dataSource.setPassword("crm1943");
//
//        DataRepository target = new DataRepository(dataSource);
//        target.disable(DataFeature.REFERENTIALCONSTRAINT);
//        ((JdbcDataSource) target.getDataSource()).close();
//
//        DataReader reader = new DataReader(source);
//        DataWriter writer = new DataWriter(target);
//        for(Collection collection: source.getCollections().values()) {
//            Collection output;
//
//            if(!target.hasCollection(collection.getName())) {
//                output = new Collection(collection.getName());
//                for(Field field : collection.getFields()) {
//                    output.addField(new Field(field.getName(), field.getType(), field.getPrecision(), field.getScale()));
//                }
//                target.createCollection(output);
//
//            } else {
//                output = target.getCollection(collection.getName());
//            }
//
//            System.out.println(String.format("%s: start writing. ", output.getName()));
//            reader.open(collection); writer.open(output);
//            writer.clear();
//            Map<String, Object> record;
//            int count = 0;
//            while((record = reader.read())!=null) {
//                writer.write(record);
//                count++;
//                if(count%5000==0) {
//                    System.out.println(String.format("%s: progress = %d", output.getName(), count));
//                }
//            }
//            System.out.println(String.format("%s: total = %d", output.getName(), count));
//
//            reader.close(); writer.close();
//        }
//
//        target.enable(DataFeature.REFERENTIALCONSTRAINT);
//    }

}