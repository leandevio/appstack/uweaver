/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.data;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Installer obj = new Installer();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Installer {
    JSONParser parser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    java.util.List<Map> products;
    Environment environment = Environment.getDefaultInstance();
    Path blobStorage = environment.tmpFolder().resolve("repository");
    JdbcDataSource dataSource;
    DataRepository dataRepository;
    String[] contentTypes = {"pptx", "txt", "xlsx", "pdf", "docx", "doc", "jpg"};

    private static class SingletonHolder {
        private static final Installer INSTANCE = Installer.getInstance();
    }

    public static Installer getDefaultInstance() throws IOException {
        return SingletonHolder.INSTANCE;
    }

    private static Installer getInstance() {
        return new Installer();
    }

    public void resetData() throws Exception {
        deleteDirectory(blobStorage);
        Files.createDirectory(blobStorage);

        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + "products.json");
        products = Arrays.asList(parser.readValue(in, Map[].class));
        in.close();

        for(Map product: products) {
            InputStream coverData = clazz.getClassLoader().getResourceAsStream(path + File.separator + product.get("cover"));
            byte[] cover = new byte[coverData.available()];
            coverData.read(cover);
            product.put("cover", cover);
            coverData.close();

            product.put("description", new StringReader((String) product.get("description")));

            String attachments = generateAttachments((String) product.get("attachments"));

            product.put("attachments", attachments);
        }

        dataRepository = DataRepository.getDefaultInstance();
        dataRepository.setBlobStorage(blobStorage);

        if(dataRepository.hasCollection("productView")) dataRepository.dropCollection("productView");
        if(dataRepository.hasCollection("product")) dataRepository.dropCollection("product");
        DataCollection dataCollection = new DataCollection("product");

        Map<String, Object> product = products.get(0);

        for(Map.Entry entry: product.entrySet()) {
            String name = (String) entry.getKey();
            Object value = entry.getValue();
            Field field = new Field(name, value.getClass());
            dataCollection.addField(field);
        }
        dataCollection.setKey(new String[] {"id"});

        dataRepository.createCollection(dataCollection);

        Connection connection = dataRepository.getConnection();
        connection.prepareStatement("create VIEW \"productView\" as select \"id\", \"name\", \"publisher\", \"attachments\", \"updateTime\" from \"product\"").execute();
        connection.close();

        DataWriter writer = new DataWriter(dataRepository);
        writer.open(dataCollection);
        writer.write(products);
        writer.close();
    }

    private String generateAttachments(String filename) throws Exception {
        List<String> attachments = new ArrayList<>();

        for(String contentType : contentTypes) {
            attachments.add(generateAttachment(filename + "." + contentType));
        }

        return parser.writeValueAsString(attachments);
    }

    private String generateAttachment(String filename) throws Exception {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(path + File.separator + filename);
        byte[] bytes = new byte[in.available()];
        in.read(bytes);
        in.close();

        Path path = blobStorage.resolve(filename);
        OutputStream out = new FileOutputStream(path.toFile());

        out.write(bytes);
        out.close();

        return blobStorage.relativize(path).toString();
    }

    private void deleteDirectory(Path path) throws IOException {
        if(!path.toFile().exists()) return;
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException e)
                    throws IOException
            {
                if (e == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed
                    throw e;
                }
            }
        });
    }
}
