package org.uweaver.data;

import org.joda.time.DateTime;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.sql.*;

/**
 * Created by jasonlin on 9/30/15.
 */
public class JdbcDataSourceTest {
    Environment environment = Environment.getDefaultInstance();

    private Object[][] records = {{"001", "Rose", new Double(3.8), null, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"002", "Tulip", new Double(2.5), new DateTime(2014, 1, 1, 8, 0, 0).toDate(), new DateTime(2014, 3, 16, 0, 0, 0).toDate()},
            {"003", "Daisy", new Double(1.5), null, new DateTime(2014, 3, 13, 0, 0, 0).toDate()},
            {"004", "Irish", new Double(5.4), null, new DateTime(2014, 4, 16, 0, 0, 0).toDate()}};
    private int[] type = {Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.DATE, Types.DATE};

    private JdbcDataSource ds;

    @BeforeTest
    public void beforeTest() throws Exception {
        ds = new JdbcDataSource();

        ds.setDriver(environment.property("db.driver"));
        ds.setUrl(environment.property("db.url"));
        ds.setUsername(environment.property("db.username"));
        ds.setPassword(environment.property("db.password"));

        Connection conn = ds.getConnection();

        ds.setMaxPoolSize(15);

        DatabaseMetaData dbms = conn.getMetaData();
        ResultSet tables = dbms.getTables(null, null, "FLOWERS", new String[]{"TABLE"});
        Statement stmt = conn.createStatement();
        if(tables.next()) {
            String sql = "drop table flowers";
            stmt.execute(sql);
        }
        String sql = "create table flowers(id int, name varchar(50), price double, effectiveDate date, expireDate date)";
        stmt.execute(sql);
        stmt.close();
        conn.close();
    }

    @Test
    public void testInsert() throws Exception {
        Connection conn = ds.getConnection();
        String sql = "insert into flowers(id, name, price, effectiveDate, expireDate) values(?, ?, ?, ?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        for(int i=0; i<records.length; i++) {
            Object[] record = records[i];
            for(int j=0; j<record.length; j++) {
                stmt.setObject(j+1, record[j], type[j]);
            }
            stmt.execute();
        }
        conn.commit();
        stmt.close();
        conn.close();
    }

    @Test(dependsOnMethods = {"testInsert"})
    public void testQuery() throws Exception {
        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();
        String sql = "select * from flowers";
        ResultSet rs = stmt.executeQuery(sql);
        stmt.close();
        conn.close();
    }

    @AfterTest
    public void afterTest() throws Exception {
        ds.close();
    }

}