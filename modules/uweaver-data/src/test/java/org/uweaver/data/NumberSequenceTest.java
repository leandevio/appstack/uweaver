package org.uweaver.data;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 6/6/16.
 */
public class NumberSequenceTest {

    @Test
    public void testReset() throws Exception {
        NumberSequence numberSequence = NumberSequence.getInstance();
        numberSequence.reset("ORDERNO", 10);

        assertEquals(numberSequence.currentNumber("ORDERNO"), 10);
    }

    @Test
    public void testNextNumber() throws Exception {
        NumberSequence numberSequence = NumberSequence.getInstance();
        long current = numberSequence.currentNumber("ORDERNO");

        assertEquals(numberSequence.nextNumber("ORDERNO"), current+1);
    }


}