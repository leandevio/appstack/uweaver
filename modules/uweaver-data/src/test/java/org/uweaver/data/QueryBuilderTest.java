package org.uweaver.data;

import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.data.QueryBuilder;
import org.uweaver.data.Sorter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jasonlin on 4/21/16.
 */
public class QueryBuilderTest {
    String FILTERSINJSON = "[{\"property\":\"price\", \"value\":0.5, \"operator\":\"GE\"}, {\"property\":\"price\", \"value\":10, \"operator\":\"LE\"}]";
    String SORTERSINJSON = "[{\"property\":\"price\", \"direction\":\"DESC\"}]";

    JSONParser parser = new JSONParser();

    @Test
    public void testJpql() throws Exception {
        String JPQL = "SELECT _Product FROM Product _Product WHERE _Product.price >= ?1 AND _Product.price <= ?2 ORDER BY _Product.price DESC";

        Condition condition = new Condition(parser.readValue(FILTERSINJSON, Filter[].class));
        List<Sorter> sorters = Arrays.asList(parser.readValue(SORTERSINJSON, Sorter[].class));
        QueryBuilder queryBuilder = new QueryBuilder();

        queryBuilder.from("Product");
        queryBuilder.where(condition);
        queryBuilder.orderBy(sorters);

        //assertEquals(queryBuilder.jpql(), JPQL);


    }
}