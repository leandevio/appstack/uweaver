/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.NumberConverter;
import org.uweaver.emulator.emulet.Emulet;
import org.uweaver.emulator.entity.Emulation;
import org.uweaver.emulator.handler.RESTHandler;
import org.uweaver.emulator.handler.WebAppHandler;
import org.uweaver.emulator.handler.WebHandler;

import java.nio.file.Path;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Server obj = new Server();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Emulator {
    private static final Logger LOGGER = LogManager.getLogger(Emulator.class);
    private static final Environment environment = Environment.getDefaultInstance();
    private static final Path EMULATION_RESOURCE = environment.conf("emulation.json");

    private Server server;
    private int port;
    private HandlerList handlers = new HandlerList();
    private List<Emulation> emulations = new ArrayList<>();
    private List<Emulet> emulets = new ArrayList<>();

     private JSONParser parser = new JSONParser();
    private NumberConverter numberConverter = new NumberConverter();

    private static class SingletonHolder {
        private static final Emulator INSTANCE = Emulator.getInstance();
    }

    public static Emulator getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static Emulator getInstance() {
        return new Emulator();
    }

    private Emulator() {
        port = environment.propertyAsInteger("server.port");
        server = new Server(port);
        emulations = readEmulation();
        try {
            for(Emulation emulation : emulations) {
                Class type = getClass().getClassLoader().loadClass(emulation.emulet());
                Emulet emulet = (Emulet) type.newInstance();
                emulet.init(emulation);
                add(emulet);
            }
        } catch (ClassNotFoundException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        }

        handlers.addHandler(new DefaultHandler());
        server.setHandler(handlers);
    }

    private void add(Emulet emulet) {
        handlers.addHandler(emulet);
        emulets.add(emulet);
    }

    public void start() {
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public void destroy() {
        server.destroy();
    }

    public int port() {
        return this.port;
    }

    public static void main(String[] args) throws Exception {
        Emulator emulator = getDefaultInstance();
        emulator.start();
    }

    private List<Emulation> readEmulation() {
        return Arrays.asList(parser.readValue(EMULATION_RESOURCE, Emulation[].class));
    }

}
