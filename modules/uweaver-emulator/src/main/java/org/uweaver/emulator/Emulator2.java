/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.NumberConverter;
import org.uweaver.emulator.handler.RESTHandler;
import org.uweaver.emulator.handler.WebAppHandler;
import org.uweaver.emulator.handler.WebHandler;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Emulator2 obj = new Emulator2();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Emulator2 {
    private static final Logger LOGGER = LogManager.getLogger(Emulator2.class);
    private static final Environment environment = Environment.getDefaultInstance();

    private Server server;
    private int port;

    private JSONParser parser = new JSONParser();
    private NumberConverter numberConverter = new NumberConverter();

    private static class SingletonHolder {
        private static final Emulator2 INSTANCE = Emulator2.getInstance();
    }

    public static Emulator2 getDefaultInstance() {
        return Emulator2.SingletonHolder.INSTANCE;
    }

    private static Emulator2 getInstance() {
        return new Emulator2();
    }

    private Emulator2() {
        port = environment.propertyAsInteger("server.port");
        server = new Server(port);

        HandlerList handlers = new HandlerList();
        for(Map<String, Object> cfg : readHandlers()) {
            handlers.addHandler(createHandler(cfg));
        }
        handlers.addHandler(new DefaultHandler());

        server.setHandler(handlers);
    }

    private Handler createHandler(Map<String, Object> cfg) {
        Handler handler;
        String type = (String) cfg.get("type");

        if(type.equals("Web")) {
            handler = createWebHandler(cfg);
        } else if(type.equals("REST")) {
            handler = createRESTHandler(cfg);
        } else if(type.equals("WebApp")) {
            handler = createWebAppHandler(cfg);
        } else {
            handler = null;
        }

        return handler;
    }

    private WebHandler createWebHandler(Map<String, Object> cfg) {
        WebHandler handler = new WebHandler();
        String contextPath = (String) cfg.get("path");
        String resourceBase = (String) cfg.get("resourceBase");
        List<String> welcomeFiles = (List<String>) cfg.get("welcomeFiles");

        handler.setContextPath(contextPath);
        handler.setWelcomeFiles(welcomeFiles);
        handler.setResourceBase(resourceBase);

        return handler;
    }

    private RESTHandler createRESTHandler(Map<String, Object> cfg) {
        RESTHandler handler = new RESTHandler();
        String contextPath = (String) cfg.get("path");
        String resourceBase = (String) cfg.get("resourceBase");
        Integer delay = numberConverter.convert(cfg.get("delay"), Integer.class, 0);

        handler.setContextPath(contextPath);
        handler.setResourceBase(resourceBase);
        handler.setDelay(delay);

        return handler;

    }

    private WebAppHandler createWebAppHandler(Map<String, Object> cfg) {
        WebAppHandler handler = new WebAppHandler();
        String contextPath = (String) cfg.get("path");
        String webContext = (String) cfg.get("webContext");
        String serviceContext = (String) cfg.get("serviceContext");
        String systemContext = (String) cfg.get("systemContext");


        handler.setContextPath(contextPath);
        handler.setWebContext(webContext);
        handler.setServiceContext(serviceContext);
        handler.setSystemContext(systemContext);

        return handler;
    }



    public void start() {
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public void destroy() {
        server.destroy();
    }

    public int port() {
        return this.port;
    }

    public static void main(String[] args) throws Exception {
        Emulator2 emulator = getDefaultInstance();
        emulator.start();
    }

    private List<Map> readHandlers() {
        Path file = environment.dataFolder().resolve("ApplicationContext.json");
        return Arrays.asList(parser.readValue(file, Map[].class));
    }
}
