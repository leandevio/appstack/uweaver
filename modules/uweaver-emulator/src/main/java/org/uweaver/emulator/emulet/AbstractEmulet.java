/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator.emulet;

import org.eclipse.jetty.server.handler.ContextHandler;
import org.uweaver.emulator.entity.Emulation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AbstractEmulet obj = new AbstractEmulet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public abstract class AbstractEmulet extends ContextHandler implements Emulet {
    protected String name = null;
    protected URL docRoot = null;
    protected URL dataRoot = null;
    protected URL scriptRoot = null;

    @Override
    public void init(Emulation emulation) {
        this.name = emulation.name();
        this.setContextPath(emulation.contextPath());
        this.docRoot = emulation.docRoot();
        this.dataRoot = emulation.dataRoot();
        this.scriptRoot = emulation.scriptRoot();
    }

    @Override
    public String name() {
        return getName();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String contextPath() {
        return getContextPath();
    }

    @Override
    public URL docRoot() {
        return getDocRoot();
    }

    @Override
    public URL getDocRoot() {
        return docRoot;
    }

    @Override
    public void setDocRoot(URL docRoot) {
        this.docRoot = docRoot;
    }

    @Override
    public URL dataRoot() {
        return getDataRoot();
    }

    @Override
    public URL getDataRoot() {
        return dataRoot;
    }

    @Override
    public void setDataRoot(URL dataRoot) {
        this.dataRoot = dataRoot;
    }

    @Override
    public URL scriptRoot() {
        return getScriptRoot();
    }

    @Override
    public URL getScriptRoot() {
        return scriptRoot;
    }

    @Override
    public void setScriptRoot(URL scriptRoot) {
        this.scriptRoot = scriptRoot;
    }
}
