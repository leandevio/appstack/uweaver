/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator.emulet;

import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.emulator.entity.Emulation;
import org.uweaver.servlet.dispatcher.RestletDispatcher;
import org.uweaver.servlet.filter.HttpTuner;
import org.uweaver.servlet.filter.SecurityFilter;
import org.uweaver.servlet.filter.ServerController;
import org.uweaver.servlet.restlet.*;
import org.uweaver.servlet.storelet.DocumentStorelet;
import org.uweaver.servlet.storelet.JSONStorelet;
import org.uweaver.servlet.storelet.ResourceStorelet;

import javax.servlet.Filter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * WebAppEmulet obj = new WebAppEmulet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class WebAppEmulet extends AbstractServletEmulet {
    private static final Logger LOGGER = LogManager.getLogger(WebAppEmulet.class);
    private static final Map<String, Class<? extends Restlet>> SYSRESTLETS = new HashMap<>();
    private static final Map<String, Class<? extends Restlet>> USERRESTLETS = new HashMap<>();
    private Environment environment;

    static {
        SYSRESTLETS.put("environment", EnvironmentRestlet.class);
        SYSRESTLETS.put("i18n", I18nRestlet.class);
        USERRESTLETS.put("site", SiteRestlet.class);
        USERRESTLETS.put("applets", AppletRestlet.class);
        USERRESTLETS.put("user", UserRestlet.class);
    }

    @Override
    public void init(Emulation emulation) {
        super.init(emulation);
        String appName = this.name().toUpperCase();
        environment = Environment.getDefaultInstance();

        SessionHandler sessionHandler = new SessionHandler();
        this.setSessionHandler(sessionHandler);

        addServerController();
        addSecurityFilter();
        addHttpTuner();
        addRestletDispatcher();
    }

    private void addServerController() {
        ServerController serverController = new ServerController();
        serverController.setEnvironment(environment);
        this.addFilter(serverController, "/*");
    }

    private void addSecurityFilter() {
        if(!environment.propertyAsBoolean("authorizer.enable", false)) return;
        SecurityFilter securityFilter = new SecurityFilter();
        this.addFilter(securityFilter, "/*");
    }

    private void addHttpTuner() {
        HttpTuner httpTuner = new HttpTuner();
        this.addFilter(httpTuner, "/*");
    }

    private void addRestletDispatcher() {
        String pathSpec = "/*";
        List<String> disabled = Arrays.asList(environment.property("restlet.disable", "").split(","));

        RestletDispatcher dispatcher = new RestletDispatcher();
        String contextPath = environment.property("restlet.contextPath");
        if(contextPath!=null) {
            pathSpec = "/" + contextPath + pathSpec;
        }
        this.addFilter(dispatcher, pathSpec);

        dispatcher.addRestlet(DocumentStorelet.getInstance(environment), "/**/*.{js,html,css,json,properties,woff,woff2,ttf}");
        dispatcher.addRestlet(new EnvironmentRestlet(environment), "/environment");
        dispatcher.addRestlet(new I18nRestlet(), "/i18n");

        for(String uri : USERRESTLETS.keySet()) {
            if(disabled.contains(uri)) continue;
            Restlet restlet = createRestlet(USERRESTLETS.get(uri));
            String restletPath = "/" + uri;
            dispatcher.addRestlet(restlet, restletPath);
        }

        dispatcher.addRestlet(JSONStorelet.getInstance(), "/*");

    }

    private Restlet createRestlet(Class<? extends Restlet> restletClass) {
        Restlet restlet = null;
        try {
            Method creator = restletClass.getMethod("getInstance", Environment.class);
            restlet = (Restlet) creator.invoke(null, environment);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (InvocationTargetException e) {
            throw new ApplicationException(e);
        } catch (NoSuchMethodException e) {
            try {
                restlet = restletClass.newInstance();
            } catch (InstantiationException ex) {
                throw new ApplicationException(ex);
            } catch (IllegalAccessException ex) {
                throw new ApplicationException(ex);
            }
        }

        return restlet;
    }

    private void addFilter(Filter filter, String pathSpec) {
        FilterHolder holder = new FilterHolder(filter);
        super.addFilter(holder, pathSpec, null);
    }

}
