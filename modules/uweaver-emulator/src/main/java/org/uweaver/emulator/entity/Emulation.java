/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator.entity;

import java.net.URL;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Emulation obj = new Emulation();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Emulation {
    private String name;
    private String contextPath;
    private String emulet;
    private URL dataRoot;
    private URL docRoot;
    private URL scriptRoot;

    public String name() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String contextPath() {
        return getContextPath();
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String emulet() {
        return getEmulet();
    }

    public String getEmulet() {
        return emulet;
    }

    public void setEmulet(String emulet) {
        this.emulet = emulet;
    }

    public URL dataRoot() {
        return getDataRoot();
    }

    public URL getDataRoot() {
        return dataRoot;
    }

    public void setDataRoot(URL dataRoot) {
        this.dataRoot = dataRoot;
    }

    public URL docRoot() {
        return getDocRoot();
    }

    public URL getDocRoot() {
        return docRoot;
    }

    public void setDocRoot(URL docRoot) {
        this.docRoot = docRoot;
    }

    public URL scriptRoot() {
        return getScriptRoot();
    }

    public URL getScriptRoot() {
        return scriptRoot;
    }

    public void setScriptRoot(URL scriptRoot) {
        this.scriptRoot = scriptRoot;
    }
}
