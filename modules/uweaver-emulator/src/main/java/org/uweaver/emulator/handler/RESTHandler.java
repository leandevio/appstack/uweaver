/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator.handler;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.NumberConverter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RESTHandler obj = new RESTHandler();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RESTHandler extends ServletContextHandler {
    private static final Logger LOGGER = LogManager.getLogger(RESTHandler.class);
    private ServletHolder handler;

    private String resourceBase;
    private int delay = 0;

    private JSONParser parser = new JSONParser();
    private NumberConverter numberConverter = new NumberConverter();

    public RESTHandler() {
        handler = new ServletHolder(new ServiceServlet());
        this.addServlet(handler, "/*");
    }

    public void setResourceBase(String resourceBase) {
        if(resourceBase.startsWith("~")) {
            resourceBase = resourceBase.replaceFirst("~",  System.getProperty("user.home"));
        }
        this.resourceBase = resourceBase;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    private class ServiceServlet extends HttpServlet {
        @Override
        protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                IOException {
            try {
                if(delay>0) Thread.sleep(delay);
            } catch (InterruptedException ex) {
                LOGGER.warn("Exception:" + ex);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(ex);
            }

            String uri = request.getRequestURI();
            String method = request.getMethod().toUpperCase();

            LOGGER.debug("Request received ...");
            LOGGER.debug("URI: " + uri);
            LOGGER.debug("Method: " + method);
            LOGGER.debug("Content Type: " + request.getContentType());
            LOGGER.debug("Encoding: " + request.getCharacterEncoding());
            LOGGER.debug("Query String: " + request.getQueryString());
            String filename = resourceBase + File.separator + uri.replace(getContextPath(), "") + File.separator + method;

            File file = new File(filename);

            response.setContentType("application/json;charset=utf-8");

            FileReader inputStream = null;
            char[] bytes = new char[(int) file.length()];
            try {
                inputStream = new FileReader(file);
                inputStream.read(bytes);
                String sb = new String(bytes);
                Map<String, Object> map = parser.readValue(sb, Map.class);

                int status = HttpServletResponse.SC_OK;
                Map<String, Object> headers = new HashMap();
                String body = null;
                if(map.containsKey("status")) {
                    status =  numberConverter.convert(map.get("status"), Integer.class);
                }
                if(map.containsKey("headers")) {
                    headers = (Map<String, Object>) map.get("headers");
                }
                if(map.containsKey("body")) {
                    body = parser.writeValueAsString(map.get("body"));
                }

                response.setStatus(status);

                for(Map.Entry<String, Object> header: headers.entrySet()) {
                    String key = header.getKey();
                    Object value = header.getValue();
                    if(value instanceof String) {
                        response.setHeader(key, (String) value);
                    } else if(value instanceof Integer) {
                        response.setIntHeader(key, (int) value);
                    } else if(value instanceof Date) {
                        response.setDateHeader(key, ((Date) value).getTime());
                    } else if(value!=null) {
                        response.setHeader(key, value.toString());
                    }
                }
                response.getWriter().print(body);


                LOGGER.debug("Response sending ...");
                LOGGER.debug("Content Type: " + response.getContentType());
                LOGGER.debug("Status Code: " + response.getStatus());
                LOGGER.debug("Response Body: \n" + body);
            } catch (Exception ex) {
                LOGGER.warn("Exception:", ex);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(ex);
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }
    }
}