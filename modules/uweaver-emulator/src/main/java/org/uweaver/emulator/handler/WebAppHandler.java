/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.emulator.handler;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.util.HostMap;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.JsonParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * WebAppHandler obj = new WebAppHandler();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class WebAppHandler extends ContextHandler {
    private static final Logger LOGGER = LogManager.getLogger(WebAppHandler.class);

    Handler handler;

    String webContext = null;
    String serviceContext = null;
    String systemContext = null;

    public WebAppHandler() {
        handler = new MyHandler();
        setHandler(handler);
    }

    public void setWebContext(String webContext) {
        this.webContext = webContext;
    }

    public void setServiceContext(String serviceContext) {
        this.serviceContext = serviceContext;
    }

    public void setSystemContext(String systemContext) {
        this.systemContext = systemContext;
    }

    private class MyHandler extends AbstractHandler {
        @Override
        public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse response) throws IOException, ServletException {
            String action = request.getHeader("Action");
            if(action==null) return;


            response.setContentType("application/json;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_OK);
            Map<String, Object> status = new HostMap<>();
            status.put("webContext", webContext);
            status.put("serviceContext", serviceContext);
            status.put("systemContext", systemContext);
            String responseText = JsonParser.writeValueAsString(status);
            response.getWriter().print(responseText);

            request.setHandled(true);
            LOGGER.debug("WebAppHandler");
        }
    }
}

