#!/usr/bin/env bash

script="$0"
basename="$(dirname $script)"
jarfile="$basename/../uweaver-simulator.jar"

if [ -z ${EMULATOR_BASE:+x} ]; then
    export EMULATOR_BASE=$EMULATOR_HOME
fi

java -Dlog4j.debug -Dlog4j.configuration=file://$EMULATOR_BASE/conf/log4j.xml -cp $jarfile org.uweaver.emulator.Emulator