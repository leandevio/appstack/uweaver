/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * developing enterprise application software.
 * 
 *      http://www.uweaver.org
 *
 * Copyright 2015 Jason Lin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * <h1>Eform</h1>
 * The Eform class implements electronic form. The implementation provides reading & writing the values to the fields.
 * <p>
 * 
 * @author   Jason Lin
 * @version  1.0
 * @since    2014-05-10
 */
public class Eform {

    private InputStream input;
    private OutputStream output;
    private File file;

    private com.itextpdf.text.pdf.PdfReader reader;
    private PdfStamper stamper;
    private AcroFields form;

    private Boolean embeddedFonts = true;

    private static final Map<Locale, Font> NOTEMBEDDEDFONTS = new HashMap<>();

    private static final Map<Locale, Font> EMBEDDEDFONTS = new HashMap<>();

    static {
        // Not every font files can be used as embedded fonts. Should test to see if the pdf displays text normally.
        EMBEDDEDFONTS.put(new Locale("en", "US"), new Font("fonts/NotoSans-Regular.ttf", BaseFont.IDENTITY_H));
//        EMBEDDEDFONTS.put(new Locale("zh", "TW"), new Font("fonts/NotoSansCJKtc-Regular.otf", BaseFont.IDENTITY_H));
//        EMBEDDEDFONTS.put(new Locale("zh", "CN"), new Font("fonts/NotoSansCJKsc-Regular.otf", BaseFont.IDENTITY_H));
//        EMBEDDEDFONTS.put(new Locale("ja", "JP"), new Font("fonts/NotoSansCJKjp-Regular.otf", BaseFont.IDENTITY_H));
//        EMBEDDEDFONTS.put(new Locale("ko", "KR"), new Font("fonts/NotoSansCJKkr-Regular.otf", BaseFont.IDENTITY_H));
        EMBEDDEDFONTS.put(new Locale("zh", "TW"), new Font("fonts/wt011.ttf", BaseFont.IDENTITY_H));
        EMBEDDEDFONTS.put(new Locale("zh", "CN"), new Font("fonts/simhei.ttf", BaseFont.IDENTITY_H));
        EMBEDDEDFONTS.put(new Locale("ja", "JP"), new Font("fonts/osaka.unicode.ttf", BaseFont.IDENTITY_H));
        EMBEDDEDFONTS.put(new Locale("ko", "KR"), new Font("fonts/UnDotum.ttf", BaseFont.IDENTITY_H));
        // ISO-32000-1: The Identity-H and Identity-V CMaps shall not be used with a non-embedded font.
        NOTEMBEDDEDFONTS.put(new Locale("zh", "TW"), new Font("MSung-Light", "UniCNS-UCS2-H"));
        NOTEMBEDDEDFONTS.put(new Locale("zh", "CN"), new Font("STSong-Light", "UniGB-UCS2-H"));
        NOTEMBEDDEDFONTS.put(new Locale("ja", "JP"), new Font("KozMinPro-Regular", "UniJIS-UCS2-H"));
        NOTEMBEDDEDFONTS.put(new Locale("ko", "KR"), new Font("HYGoThic-Medium", "UniKS-UCS2-H"));
    }

    public Eform(File file) {
        this.file = file;
        try {
            this.input = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new org.uweaver.core.exception.FileNotFoundException(e);
        }
        this.output = new ByteArrayOutputStream();
        initialize();
    }

    public Eform(InputStream input, OutputStream output) {
        this.file = null;
        this.input = input;
        this.output = output;
        initialize();
    }

    private void initialize() {
        try {
            reader = new com.itextpdf.text.pdf.PdfReader(input);
            form = reader.getAcroFields();
            stamper = null;
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void setEmbeddedFonts(Boolean value) {
        embeddedFonts = value;
    }

    public Boolean isEmbeddedFonts() {
        return embeddedFonts;
    }

    private void makeWritable() {
        if(isWritable()) return;
        try {
            stamper = new PdfStamper(reader, output);
            form = stamper.getAcroFields();

            // The default is supposed to be true according to the api doc. However, it might be false in reality.
            form.setGenerateAppearances(true);
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException((file!=null) ? file.getAbsolutePath() : null, e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException((file!=null) ? file.getAbsolutePath() : null, e);
        }
    }

    private boolean isWritable() {
        return (stamper!=null);
    }

    public void close() {
        if(!isWritable()) return;

        try {
            stamper.close();
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException((file!=null) ? file.getAbsolutePath() : null, e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException((file!=null) ? file.getAbsolutePath() : null, e);
        }

        if(this.file!=null) {
            try (FileOutputStream fileoutput = new FileOutputStream(file)){
                ((ByteArrayOutputStream) this.output).writeTo(fileoutput);
            } catch (IOException e) {
                throw new org.uweaver.core.exception.IOException((file!=null) ? file.getAbsolutePath() : null, e);
            }
        }

    }

    public void setValues(Map<String, Object> values) {
        setValues(values, Environment.getDefaultInstance().locale());
    }

    public void setValues(Map<String, Object> values, Locale locale) {
        makeWritable();

        Set<String> fields = form.getFields().keySet();
        try {
            BaseFont unicode = createFont(locale);

            for(String key : fields) {
                Object value = values.get(key);
                if (value == null) continue;

                if (form.getFieldType(key) == AcroFields.FIELD_TYPE_TEXT) {
                    // possible properties:
                    // "textfont" : BaseFont
                    // "textcolor" : BaseColor
                    // "textsize" : Float
                    // "bgcolor" : BaseColor
                    // "bordercolor" : BaseColor ==> doesn't work
                    form.setFieldProperty(key, "textfont", unicode, null);
                }
                form.setField(key, value.toString());
            }
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException((file!=null) ? file.getAbsolutePath() : null, e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException((file!=null) ? file.getAbsolutePath() : null, e);
        }
    }

    public void flatten() {
        makeWritable();

        boolean partialFlattening = false;
        Set<String> fields = form.getFields().keySet();
        for(String key : fields) {
            if (form.getFieldType(key) == AcroFields.FIELD_TYPE_TEXT) {
                form.setFieldProperty(key, "setfflags", TextField.READ_ONLY, null);
            } else if (form.getFieldType(key) == AcroFields.FIELD_TYPE_CHECKBOX) {
                stamper.partialFormFlattening(key);
                partialFlattening = true;
            }
        }

        stamper.setFormFlattening(partialFlattening);
    }

    public Map<String, Object> getValues() {
        Map<String, Object> values = new HashMap<>();
        Set<String> fields = form.getFields().keySet();
        for(String key : fields) {
            values.put(key, form.getField(key));
        }

        return values;
    }

    public void setValue(String name, Object value) {
        makeWritable();
        try {
            form.setField(name, value.toString());
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException((file!=null) ? file.getAbsolutePath() : null, e);
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException((file!=null) ? file.getAbsolutePath() : null, e);
        }
    }

    public Object getValue(String name) {
        return form.getField(name);
    }

    private BaseFont createFont(Locale locale) throws IOException, DocumentException {
        Map<Locale, Font> FONTS = embeddedFonts ? EMBEDDEDFONTS : NOTEMBEDDEDFONTS;

        Font FONT = FONTS.get(locale);

        if(FONT==null) FONT = (Font) FONTS.entrySet().toArray()[0];

        return BaseFont.createFont(FONT.name, FONT.encoding, embeddedFonts ? BaseFont.EMBEDDED : BaseFont.NOT_EMBEDDED);
    }

    private static class Font {
        String name;
        String encoding;

        public Font(String name, String encoding) {
            this.name = name;
            this.encoding = encoding;
        }
    }
}
