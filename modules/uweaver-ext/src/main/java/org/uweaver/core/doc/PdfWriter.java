/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.uweaver.core.exception.ApplicationException;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * PDFWriter obj = new PDFWriter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class PdfWriter implements SlideShowWriter {
    OutputStream out;
    Document document = new Document();
    com.itextpdf.text.pdf.PdfWriter pdfWriter;

    private static final float YLINEMIN = 36;
    private static final Map<Locale, BaseFont> BASEFONTS = new HashMap<>();
    private static final Map<Locale, BaseFont> BASEFONTSNOTEMBEDDED = new HashMap<>();
    private static final Map<TextAlign, Integer> TEXTALIGNMAP = new HashMap<>();
    private static final Map<ImageAlign, Integer> IMAGEALIGNMAP = new HashMap<>();
    private static final Map<VerticalAlign, Integer> VERTICALALIGNMAP = new HashMap<>();

    static {
        try {
            // Not every font files can be used as embedded fonts. Should test to see if the pdf displays text normally.
            BASEFONTS.put(new Locale("en", "US"), BaseFont.createFont("fonts/NotoSans-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
//        BASEFONTS.put(new Locale("zh", "TW"), new Font("fonts/NotoSansCJKtc-Regular.otf", BaseFont.IDENTITY_H));
//        BASEFONTS.put(new Locale("zh", "CN"), new Font("fonts/NotoSansCJKsc-Regular.otf", BaseFont.IDENTITY_H));
//        BASEFONTS.put(new Locale("ja", "JP"), new Font("fonts/NotoSansCJKjp-Regular.otf", BaseFont.IDENTITY_H));
//        BASEFONTS.put(new Locale("ko", "KR"), new Font("fonts/NotoSansCJKkr-Regular.otf", BaseFont.IDENTITY_H));
            BASEFONTS.put(new Locale("zh", "TW"), BaseFont.createFont("fonts/wt011.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
            BASEFONTS.put(new Locale("zh", "CN"), BaseFont.createFont("fonts/simhei.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
            BASEFONTS.put(new Locale("ja", "JP"), BaseFont.createFont("fonts/osaka.unicode.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
            BASEFONTS.put(new Locale("ko", "KR"), BaseFont.createFont("fonts/UnDotum.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
            // ISO-32000-1: The Identity-H and Identity-V CMaps shall not be used with a non-embedded font.
            BASEFONTSNOTEMBEDDED.put(new Locale("zh", "TW"), BaseFont.createFont("MSung-Light", "UniCNS-UCS2-H", BaseFont.NOT_EMBEDDED));
            BASEFONTSNOTEMBEDDED.put(new Locale("zh", "CN"), BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED));
            BASEFONTSNOTEMBEDDED.put(new Locale("ja", "JP"), BaseFont.createFont("KozMinPro-Regular", "UniJIS-UCS2-H", BaseFont.NOT_EMBEDDED));
            BASEFONTSNOTEMBEDDED.put(new Locale("ko", "KR"), BaseFont.createFont("HYGoThic-Medium", "UniKS-UCS2-H", BaseFont.NOT_EMBEDDED));
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        TEXTALIGNMAP.put(TextAlign.LEFT, com.itextpdf.text.Element.ALIGN_LEFT);
        TEXTALIGNMAP.put(TextAlign.CENTER, com.itextpdf.text.Element.ALIGN_CENTER);
        TEXTALIGNMAP.put(TextAlign.RIGHT, com.itextpdf.text.Element.ALIGN_RIGHT);
        TEXTALIGNMAP.put(TextAlign.JUSTIFY, com.itextpdf.text.Element.ALIGN_JUSTIFIED);
        TEXTALIGNMAP.put(TextAlign.DISTRIBUTE, com.itextpdf.text.Element.ALIGN_JUSTIFIED_ALL);

        IMAGEALIGNMAP.put(ImageAlign.LEFT, com.itextpdf.text.Image.ALIGN_LEFT);
        IMAGEALIGNMAP.put(ImageAlign.CENTER, com.itextpdf.text.Image.ALIGN_CENTER);
        IMAGEALIGNMAP.put(ImageAlign.RIGHT, com.itextpdf.text.Image.ALIGN_RIGHT);

        VERTICALALIGNMAP.put(VerticalAlign.TOP, com.itextpdf.text.Element.ALIGN_TOP);
        VERTICALALIGNMAP.put(VerticalAlign.MIDDLE, com.itextpdf.text.Element.ALIGN_MIDDLE);
        VERTICALALIGNMAP.put(VerticalAlign.BOTTOM, com.itextpdf.text.Element.ALIGN_BOTTOM);

    }

    private class Dictionary {

    }

    public PdfWriter(OutputStream out) {
        init(out);
    }

    private void init(OutputStream out) {
        this.out = out;
        try {
            pdfWriter = com.itextpdf.text.pdf.PdfWriter.getInstance(document, out);
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException(e);
        }
    }

    @Override
    public void open() {
        document.open();
    }

    @Override
    public void close() {
        document.close();
    }

    @Override
    public void write(Slide slide) {
        writeBackground(slide.getBackground());
        for(org.uweaver.core.doc.Element element : slide.getElements()) {
            writeElement(element, slide.getDefaultLocale());
        }
        newPage();
    }

    public void newPage() {
        document.newPage();
    }

    public void writeBackground(Image background) {
        if(background==null) return;
        Rectangle rect = background.getRect();
        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        ColumnText ct = new ColumnText(canvas);

        double left = rect.getLeft();
        double bottom = getPageSize().getHeight() - rect.getBottom();
        double right = rect.getRight();
        double top = getPageSize().getHeight() - rect.getTop();

        ct.setSimpleColumn((float) left, (float) bottom, (float) right, (float) top);
        try {
            writePdfImage(ct, toPdfImage(background));
            ct.go(false);
        } catch (DocumentException e) {
            throw new ApplicationException(e);
        }
    }

    public void writeElement(Element element, Locale defaultLocale) {
        if(element instanceof Placeholder) {
            writePlaceholder((Placeholder) element, defaultLocale);
        } else if(TextBox.class.isAssignableFrom(element.getClass())) {
            writeTextBox((TextBox) element, defaultLocale);
        } else if(Image.class.isAssignableFrom(element.getClass())) {
            writeImage((Image) element);
        } else if(Table.class.isAssignableFrom(element.getClass())) {
            writeTable((Table) element, defaultLocale);
        }
    }

    public void writePlaceholder(Placeholder placeholder, Locale defaultLocale) {
        if(placeholder.isEmpty()) return;
        if(placeholder.getContent()!=null) {
            writeElement(placeholder.getContent(), defaultLocale);
            return;
        }

        ColumnText ct = getColumnText(placeholder);
        java.util.List<com.itextpdf.text.Paragraph> pdfParagraphs = toPdfParagraphs(placeholder.getParagraphs(), defaultLocale);

        VerticalAlign verticalAlign = placeholder.getVerticalAlign();
        try {
            if (verticalAlign.equals(VerticalAlign.MIDDLE)) {
                writePdfParagraphs(ct, pdfParagraphs);
                float yLine = ct.getYLine();
                ct.go(true);
                float offset = ((float) placeholder.getRect().getHeight() - (yLine - ct.getYLine())) / 2;
                ct.setYLine(Math.max(YLINEMIN, yLine - offset));
            }
            writePdfParagraphs(ct, pdfParagraphs);
            ct.go(false);
        } catch (DocumentException e) {
            throw new ApplicationException(e);
        }
    }

    public void writeTextBox(TextBox textBox, Locale defaultLocale) {
        ColumnText ct = getColumnText(textBox);
        java.util.List<com.itextpdf.text.Paragraph> pdfParagraphs = toPdfParagraphs(textBox.getParagraphs(), defaultLocale);

        VerticalAlign verticalAlign = textBox.getVerticalAlign();
        try {
            if (verticalAlign.equals(VerticalAlign.MIDDLE)) {
                writePdfParagraphs(ct, pdfParagraphs);
                float yLine = ct.getYLine();
                ct.go(true);
                float offset = ((float) textBox.getRect().getHeight() - (yLine - ct.getYLine())) / 2;
                ct.setYLine(yLine - offset);
                ct.setYLine(Math.max(YLINEMIN, yLine - offset));
            }
            writePdfParagraphs(ct, pdfParagraphs);
            ct.go(false);
        } catch (DocumentException e) {
            throw new ApplicationException(e);
        }
    }


    public void writeImage(Image image) {
        ColumnText ct = getColumnText(image);
        try {
            writePdfImage(ct, toPdfImage(image));
            ct.go(false);
        } catch (DocumentException e) {
            throw new ApplicationException(e);
        }

    }

    private ColumnText getColumnText(Image image) {
        Rectangle rect = image.getRect();
        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        ColumnText ct = new ColumnText(canvas);

        double left = rect.getLeft();
        double bottom = getPageSize().getHeight() - rect.getBottom();
        double right = rect.getRight();
        double top = getPageSize().getHeight() - rect.getTop();

        ct.setSimpleColumn((float) left, (float) bottom, (float) right, (float) top);
        return ct;
    }

    private com.itextpdf.text.Image toPdfImage(Image image) {
        com.itextpdf.text.Image pdfImage;
        Rectangle rect = image.getRect();
        try {
            pdfImage = com.itextpdf.text.Image.getInstance(image.getBytes());
        } catch (BadElementException e) {
            throw new ApplicationException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        pdfImage.scaleToFit((float) (rect.getWidth()), (float) (rect.getHeight()));
        pdfImage.setScaleToFitHeight(true);
        pdfImage.setAlignment(toPdfImageAlign(image.getImageAlign()));

        return pdfImage;
    }

    private void writePdfImage(ColumnText ct, com.itextpdf.text.Image pdfImage) {
        ct.addElement(pdfImage);
    }


    public void writeTable(Table table, Locale defaultLocale) {
        ColumnText ct = getColumnText(table);
        try {
            writePdfTable(ct, toPdfTable(table, defaultLocale));
            ct.go(false);
        } catch (DocumentException e) {
            throw new ApplicationException(e);
        }
    }


    private void writePdfTable(ColumnText ct, PdfPTable pdfTable) {
        ct.addElement(pdfTable);
    }

    private PdfPTable toPdfTable(Table table, Locale defaultLocale) {
        PdfPTable pdfTable = new PdfPTable(table.getNumberOfColumns());
        float totalWidth = (float) table.getRect().getWidth();
        pdfTable.setTotalWidth(totalWidth);
        pdfTable.setLockedWidth(true);
        float[] widths = new float[table.getNumberOfColumns()];
        float availableWidth = totalWidth;
        int autoWidthColumns = 0;
        for(int i=0; i<widths.length; i++) {
            Double columnWidth = table.getColumnWidth(i);
            if(columnWidth==null) {
                autoWidthColumns++;
            } else {
                widths[i] = columnWidth.floatValue();
                availableWidth = availableWidth - widths[i];
            }
        }
        if(autoWidthColumns>0) {
            for(int i=0; i<widths.length; i++) {
                if(widths[i]==0) {
                    widths[i] = availableWidth / autoWidthColumns;
                }
            }
        }

        try {
            pdfTable.setWidths(widths);
        } catch (DocumentException e) {
            throw new ApplicationException(e);
        }
        for(TableRow row : table.getRows()) {
            for(TableCell cell : row.getCells()) {
                PdfPCell pdfCell = toPdfCell(cell, defaultLocale);
                pdfTable.addCell(pdfCell);
            }
        }
        return pdfTable;
    }

    private PdfPCell toPdfCell(TableCell cell, Locale defaultLocale) {
        PdfPCell pdfCell = new PdfPCell();

        Double height = cell.getParent().getHeight();
        if(height!=null) pdfCell.setMinimumHeight(height.floatValue());

        Double padding = cell.getPaddingLeft();
        if(padding!=null) pdfCell.setPaddingLeft(padding.floatValue());
        padding = cell.getPaddingTop();
        if(padding!=null) pdfCell.setPaddingTop(padding.floatValue());
        padding = cell.getPaddingRight();
        if(padding!=null) pdfCell.setPaddingRight(padding.floatValue());
        padding = cell.getPaddingBottom();
        if(padding!=null) pdfCell.setPaddingBottom(padding.floatValue());

        pdfCell.setVerticalAlignment(toPdfVerticalAlignment(cell.getVerticalAlign()));
        Double borderWidth = cell.getBorderWidth(Border.Edge.LEFT);
        pdfCell.setBorderWidthLeft((borderWidth==null) ? 0 : borderWidth.floatValue());
        borderWidth = cell.getBorderWidth(Border.Edge.TOP);
        pdfCell.setBorderWidthTop((borderWidth==null) ? 0 : borderWidth.floatValue());
        borderWidth = cell.getBorderWidth(Border.Edge.RIGHT);
        pdfCell.setBorderWidthRight((borderWidth==null) ? 0 : borderWidth.floatValue());
        borderWidth = cell.getBorderWidth(Border.Edge.BOTTOM);
        pdfCell.setBorderWidthBottom((borderWidth==null) ? 0 : borderWidth.floatValue());
        Color color = cell.getBorderColor(Border.Edge.LEFT);
        pdfCell.setBorderColorLeft((color==null) ? toPdfColor(Color.WHITE) : toPdfColor(color));
        color = cell.getBorderColor(Border.Edge.TOP);
        pdfCell.setBorderColorTop((color==null) ? toPdfColor(Color.WHITE) : toPdfColor(color));
        color = cell.getBorderColor(Border.Edge.RIGHT);
        pdfCell.setBorderColorRight((color==null) ? toPdfColor(Color.WHITE) : toPdfColor(color));
        color = cell.getBorderColor(Border.Edge.BOTTOM);
        pdfCell.setBorderColorBottom((color==null) ? toPdfColor(Color.WHITE) : toPdfColor(color));

        if(cell.getContent()!=null) {
            pdfCell.addElement(toPdfElement(cell.getContent(), defaultLocale));
        } else {
            for(Section section : cell.getSections()) {
                pdfCell.addElement(toPdfSection(section, defaultLocale));
            }
        }

        return pdfCell;
    }

    private com.itextpdf.text.Element toPdfElement(Element element, Locale defaultLocale) {
        com.itextpdf.text.Element pdfElement;

        if(Image.class.isAssignableFrom(element.getClass())) {
            pdfElement = toPdfImage((Image) element);
        } else if(Table.class.isAssignableFrom(element.getClass())) {
            pdfElement = toPdfTable((Table) element, defaultLocale);
        } else {
            pdfElement = null;
        }

        return pdfElement;
    }

    private com.itextpdf.text.Element toPdfSection(Section section, Locale defaultLocale) {
        com.itextpdf.text.Element pdfElement;
        if(List.class.isAssignableFrom(section.getClass())) {
            pdfElement = toPdfList((List) section, defaultLocale);
        } else {
            com.itextpdf.text.Paragraph pdfSection = new com.itextpdf.text.Paragraph();
            pdfSection.addAll(toPdfParagraphs(section.getParagraphs(), defaultLocale));
            pdfElement = pdfSection;
        }
        return pdfElement;
    }

    private com.itextpdf.text.List toPdfList(List list, Locale defaultLocale) {
        com.itextpdf.text.List pdfList = new com.itextpdf.text.List();
        for(Paragraph listItem : list.getItems()) {
            pdfList.add(toPdfListItem(listItem, defaultLocale));
        }
        return pdfList;
    }


    public void writeParagraphs(ColumnText ct, java.util.List<Paragraph> paragraphs, Locale defaultLocale) {
        for(Paragraph paragraph : paragraphs) {
            writeParagraph(ct, paragraph, defaultLocale);
        }
    }

    public void writeParagraph(ColumnText ct, Paragraph paragraph, Locale defaultLocale) {
        ct.addElement(toPdfParagraph(paragraph, defaultLocale));
    }

    private void writePdfParagraphs(ColumnText ct, java.util.List<com.itextpdf.text.Paragraph> pdfParagraphs) {
        for(com.itextpdf.text.Paragraph pdfParagraph : pdfParagraphs) {
            ct.addElement(pdfParagraph);
        }
    }

    private ColumnText getColumnText(TextElement textElement) {
        Rectangle rect = textElement.getRect();
        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        ColumnText ct = new ColumnText(canvas);
        Double paddingLeft = textElement.getPaddingLeft();
        Double paddingTop = textElement.getPaddingTop();
        Double paddingRight = textElement.getPaddingRight();
        Double paddingBottom = textElement.getPaddingBottom();

        if(paddingLeft==null) paddingLeft = 0d;
        if(paddingTop==null) paddingTop = 0d;
        if(paddingRight==null) paddingRight = 0d;
        if(paddingBottom==null) paddingBottom = 0d;

        double left = rect.getLeft() + paddingLeft;
        double bottom = getPageSize().getHeight() - rect.getBottom() + paddingBottom;
        double right = rect.getRight() - paddingRight;
        double top = getPageSize().getHeight() - rect.getTop() - paddingTop;

        ct.setSimpleColumn((float) left, (float) bottom, (float) right, (float) top);

        if(textElement.getTextAlign()!=null) ct.setAlignment(toPdfTextAlign(textElement.getTextAlign()));

        return ct;
    }

    public void writeParagraph(Paragraph paragraph, Locale defaultLocale) {
        try {
            document.add(toPdfParagraph(paragraph, defaultLocale));
        } catch (DocumentException e) {
            throw new org.uweaver.core.exception.DocumentException(e);
        }
    }

    public void writePhrase(TextRun textRun, Locale defaultLocale) {

    }

    private java.util.List<com.itextpdf.text.Paragraph> toPdfParagraphs(java.util.List<Paragraph> paragraphs, Locale defaultLocale) {
        java.util.List<com.itextpdf.text.Paragraph> pdfParagraphs = new ArrayList<>();

        com.itextpdf.text.List pdfList = new com.itextpdf.text.List();
        for(Paragraph paragraph : paragraphs) {
            if(paragraph.getMarker()!=null) {
                pdfList.add(toPdfListItem(paragraph, defaultLocale));
                continue;
            }

            if(pdfList.size()>0) {
                com.itextpdf.text.Paragraph pdfParagraph = new com.itextpdf.text.Paragraph();
                pdfParagraph.add(pdfList);
                pdfParagraphs.add(pdfParagraph);
                pdfList = new com.itextpdf.text.List();
            }

            pdfParagraphs.add(toPdfParagraph(paragraph, defaultLocale));
        }

        if(pdfList.size()>0) {
            com.itextpdf.text.Paragraph pdfParagraph = new com.itextpdf.text.Paragraph();
            pdfParagraph.add(pdfList);
            pdfParagraphs.add(pdfParagraph);
        }

        return pdfParagraphs;
    }

    private ListItem toPdfListItem(Paragraph paragraph, Locale defaultLocale) {
        ListItem listItem = new ListItem();
        listItem.setFont(getPdfFont(paragraph.getFontFamily(), paragraph.getFontSize(), paragraph.getColor(), defaultLocale));
        for(TextRun textRun : paragraph.getTextRuns()) {
            listItem.add(toPdfTextRun(textRun, defaultLocale));
        }
        listItem.setListSymbol(new Chunk(paragraph.getMarker()));
        return listItem;
    }

    private com.itextpdf.text.Paragraph toPdfParagraph(Paragraph paragraph, Locale defaultLocale) {

        com.itextpdf.text.Paragraph pdfParagraph = new com.itextpdf.text.Paragraph();
        pdfParagraph.setFont(getPdfFont(paragraph.getFontFamily(), paragraph.getFontSize(), paragraph.getColor(), defaultLocale));
        for(TextRun textRun : paragraph.getTextRuns()) {
            pdfParagraph.add(toPdfTextRun(textRun, defaultLocale));
        }
        pdfParagraph.setAlignment(toPdfTextAlign(paragraph.getTextAlign()));

        return pdfParagraph;
    }

    private Integer toPdfTextAlign(TextAlign textAlign) {
        return (textAlign==null) ? TEXTALIGNMAP.get(TextAlign.LEFT) : TEXTALIGNMAP.get(textAlign);
    }

    private Integer toPdfImageAlign(ImageAlign imageAlign) {
        return (imageAlign==null) ? IMAGEALIGNMAP.get(ImageAlign.CENTER) : IMAGEALIGNMAP.get(imageAlign);
    }

    private Integer toPdfVerticalAlignment(VerticalAlign verticalAlign) {
        return (verticalAlign==null) ? VERTICALALIGNMAP.get(VerticalAlign.TOP) : VERTICALALIGNMAP.get(verticalAlign);
    }

    private com.itextpdf.text.Font getPdfFont(FontFamily fontFamily, Double fontSize, Color color, Locale defaultLocale) {
        Locale locale = (fontFamily==null) ? defaultLocale : fontFamily.getLocale();
        BaseFont baseFont = BASEFONTS.get(locale==null ? defaultLocale : locale);
        com.itextpdf.text.Font pdfFont = new com.itextpdf.text.Font(baseFont);
        if(fontSize!=null) {
            pdfFont.setSize(new Float(fontSize));
        }
        pdfFont.setColor(toPdfColor(color));
        return pdfFont;
    }

    private BaseColor toPdfColor(Color color) {
        if(color==null) return BaseColor.BLACK;
        BaseColor pdfColor = new BaseColor(color.getRGB());
        return pdfColor;
    }

    private com.itextpdf.text.Chunk toPdfTextRun(TextRun textRun, Locale defaultLocale) {
        com.itextpdf.text.Chunk pdfChunk = new com.itextpdf.text.Chunk(textRun.getText());
        pdfChunk.setFont(getPdfFont(textRun.getFontFamily(), textRun.getFontSize(), textRun.getColor(), defaultLocale));
        return pdfChunk;
    }

    @Override
    public Dimension getPageSize() {
        com.itextpdf.text.Rectangle rectangle = this.document.getPageSize();
        return new Dimension(rectangle.getWidth(), rectangle.getHeight());
    }

    @Override
    public PdfWriter setPageSize(Dimension pageSize) {
        com.itextpdf.text.Rectangle rectangle = new com.itextpdf.text.Rectangle((float) pageSize.getWidth(), (float) pageSize.getHeight());
        document.setPageSize(rectangle);
        return this;
    }

    @Override
    public PdfWriter setMargins(Double left, Double top, Double right, Double bottom) {
        document.setMargins(left.floatValue(), top.floatValue(), right.floatValue(), bottom.floatValue());
        return this;
    }

    @Override
    public Double getMarginLeft() {
        return Double.valueOf(document.leftMargin());
    }

    @Override
    public PdfWriter setMarginLeft(Double marginLeft) {
        document.left(marginLeft.floatValue());
        return this;
    }

    @Override
    public Double getMarginTop() {
        return Double.valueOf(document.topMargin());
    }

    @Override
    public PdfWriter setMarginTop(Double marginTop) {
        document.top(marginTop.floatValue());
        return this;
    }

    @Override
    public Double getMarginRight() {
        return Double.valueOf(document.rightMargin());
    }

    @Override
    public PdfWriter setMarginRight(Double marginRight) {
        document.right(marginRight.floatValue());
        return this;
    }

    @Override
    public Double getMarginBottom() {
        return Double.valueOf(document.bottomMargin());
    }

    @Override
    public PdfWriter setMarginBottom(Double marginBottom) {
        document.bottom(marginBottom.floatValue());
        return this;
    }
}
