package org.uweaver.core.doc;

import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.testng.Assert.assertEquals;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.IOException;
/**
 * Created by jasonlin on 6/12/14.
 */
public class EformTest {
    private Environment environment = Environment.getDefaultInstance();

    public static final String FONT = "fonts/mingliu.ttc,1";
    public static final String TEXT = "These are the protagonists in 'Hero', a movie by Zhang Yimou:\n"
            + "\u7121\u540d (Nameless), \u6b98\u528d (Broken Sword), "
            + "\u98db\u96ea (Flying Snow), \u5982\u6708 (Moon), "
            + "\u79e6\u738b (the King), and \u9577\u7a7a (Sky).";
    public static final String CHINESE = "\u5341\u950a\u57cb\u4f0f";
    public static final String JAPANESE = "\u8ab0\u3082\u77e5\u3089\u306a\u3044";
    public static final String KOREAN = "\ube48\uc9d1";

    private static final Map<String, Object> TEXT_ZHTW = new HashMap<>();
    private static final Map<String, Object> TEXT_ZHCN = new HashMap<>();
    private static final Map<String, Object> TEXT_JAJP = new HashMap<>();
    private static final Map<String, Object> TEXT_KOKR = new HashMap<>();
    private static final Map<String, Object> VALUES = new HashMap<>();

    static {
        TEXT_ZHTW.put("Department", "研發部");
        TEXT_ZHTW.put("Degree", "碩士");
        TEXT_ZHTW.put("Seniority", "10+ 年");
        TEXT_ZHTW.put("JobTitle", "程式設計師");
        TEXT_JAJP.put("Mentor", "イチロー");
        TEXT_ZHCN.put("Location", "渖阳");
        TEXT_KOKR.put("FirstName", "추신");
        TEXT_KOKR.put("LastName", "수");
        TEXT_KOKR.put("MiddleName", "수");
        VALUES.put("JobGrade", 12);
        VALUES.put("OnboardDate", "2010/04/01");
        VALUES.put("OfferDate", "2010/03/07");
        VALUES.put("FullEnglishName", "Shin-Soo Choo");
        VALUES.put("PreferredEnglishName", "Choo");
        VALUES.put("IDNumber", "N123456789");
        VALUES.put("Male", "Yes");
        VALUES.put("Female", "No");
    }


    public void testSetValuesWhenEformCreation() throws Exception{
        URL resource = EformTest.class.getClassLoader().getResource("Eform.pdf");
        File template = new File(resource.toURI());
        File output = new File(environment.tmpFolder() + File.separator + "EformCreation.pdf");

        Eform eform = new Eform(new FileInputStream(template), new FileOutputStream(output));

        eform.setValues(TEXT_ZHTW, new Locale("zh", "TW"));
        eform.setValues(TEXT_JAJP, new Locale("ja", "JP"));
        eform.setValues(TEXT_ZHCN, new Locale("zh", "CN"));
        eform.setValues(TEXT_KOKR, new Locale("ko", "KR"));
        eform.setValues(VALUES);

        eform.flatten();

        eform.close();

        eform = new Eform(output);
        assertEquals(TEXT_ZHTW.get("Department"), eform.getValue("Department"));
        assertEquals(TEXT_JAJP.get("Mentor"), eform.getValue("Mentor"));
        assertEquals(TEXT_ZHCN.get("Location"), eform.getValue("Location"));
        assertEquals(TEXT_KOKR.get("FirstName"), eform.getValue("FirstName"));
        eform.close();

    }

    @Test
    public void testSetValuesEformModification() throws Exception {
        URL resource = EformTest.class.getClassLoader().getResource("Eform.pdf");
        File template = new File(resource.toURI());

        File file = new File(environment.tmpFolder() + File.separator + "EformModification.pdf");

        Files.copy(template.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);

        Eform eform = new Eform(file);

        eform.setValues(TEXT_ZHTW, new Locale("zh", "TW"));
        eform.setValues(TEXT_JAJP, new Locale("ja", "JP"));
        eform.setValues(TEXT_ZHCN, new Locale("zh", "CN"));
        eform.setValues(TEXT_KOKR, new Locale("ko", "KR"));
        eform.setValues(VALUES);

        eform.flatten();

        eform.close();

        eform = new Eform(file);
        assertEquals(TEXT_ZHTW.get("Department"), eform.getValue("Department"));
        assertEquals(TEXT_JAJP.get("Mentor"), eform.getValue("Mentor"));
        assertEquals(TEXT_ZHCN.get("Location"), eform.getValue("Location"));
        assertEquals(TEXT_KOKR.get("FirstName"), eform.getValue("FirstName"));
        eform.close();
    }

    @Test
    public void testSetValuesEformModificationNotEmbedded() throws Exception {
        URL resource = EformTest.class.getClassLoader().getResource("Eform.pdf");
        File template = new File(resource.toURI());

        File file = new File(environment.tmpFolder() + File.separator + "EformModificationNotEmbedded.pdf");

        Files.copy(template.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);

        Eform eform = new Eform(file);
        eform.setEmbeddedFonts(false);

        eform.setValues(TEXT_ZHTW, new Locale("zh", "TW"));
        eform.setValues(TEXT_JAJP, new Locale("ja", "JP"));
        eform.setValues(TEXT_ZHCN, new Locale("zh", "CN"));
        eform.setValues(TEXT_KOKR, new Locale("ko", "KR"));
        eform.setValues(VALUES);

        eform.flatten();

        eform.close();

        eform = new Eform(file);
        assertEquals(TEXT_ZHTW.get("Department"), eform.getValue("Department"));
        assertEquals(TEXT_JAJP.get("Mentor"), eform.getValue("Mentor"));
        assertEquals(TEXT_ZHCN.get("Location"), eform.getValue("Location"));
        assertEquals(TEXT_KOKR.get("FirstName"), eform.getValue("FirstName"));
        eform.close();
    }


    public void testUsingFont() throws Exception {
        createPdf(environment.tmpFolder() + File.separator + "Eform.pdf");
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        BaseFont bf = BaseFont.createFont(FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(bf, 12);
        //Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Paragraph p = new Paragraph("(\u6797\u6649\u965e)", font);
        document.add(p);
//        document.add(new Paragraph(CHINESE, font));
//        document.add(new Paragraph(JAPANESE, font));
//        document.add(new Paragraph(KOREAN, font));
        document.close();
    }
}