/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.core.doc;

import com.itextpdf.text.PageSize;
import com.itextpdf.text.Document;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Element;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DrawInterface;
import com.itextpdf.text.pdf.draw.LineSeparator;
import org.joda.time.DateTime;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PdfTest obj = new PdfTest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PdfTest {
    Environment environment = Environment.getDefaultInstance();
    JSONParser jsonParser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    java.util.List<Map> products;
    BaseFont BASEFONT, ZAPFDINGBATS;
    Font TITLE, NORMAL, SUBTITLE;
    DateTime NOW = new DateTime();

    @BeforeTest
    public void setUp() throws Exception {
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + "products.json");
        products = Arrays.asList(jsonParser.readValue(in, Map[].class));
        in.close();

        ZAPFDINGBATS = BaseFont.createFont(
                BaseFont.ZAPFDINGBATS, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        BASEFONT = BaseFont.createFont("fonts/wt011.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        TITLE = new Font(BASEFONT, 13, Font.BOLD, BaseColor.BLUE);
        NORMAL = new Font(BASEFONT, 11, Font.NORMAL, BaseColor.DARK_GRAY);
        SUBTITLE = new Font(BASEFONT, 12, Font.NORMAL, BaseColor.BLUE);
    }

    @Test
    public void testWrite() throws Exception {
        File file = new File(environment.tmpFolder() + File.separator + "testWrite.pdf");
        file.delete();


        Rectangle pagesize = PageSize.A4.rotate();
        Document document = new Document(pagesize, 36f, 72f, 36f, 72f);
        PdfWriter.getInstance(document, new FileOutputStream(file)).setStrictImageSequence(true);
        document.open();

        for(Map product : products) {
            Paragraph paragraph = new Paragraph();
            String template = product.get("price")==null ? "%s" : "%s - NT$%d";
            Phrase title = new Phrase(String.format(template, product.get("name"), product.get("price")), TITLE);
            Phrase description = new Phrase((String) product.get("description"), NORMAL);
            DateTime release = new DateTime(product.get("release"));

            Paragraph platforms = new Paragraph();
            platforms.setSpacingBefore(9);
            Phrase subtitle = new Phrase("支援平台：", SUBTITLE);

            InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + product.get("cover"));
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();
            Image cover = Image.getInstance(bytes);
            cover.scaleToFit(100, 100);
            cover.setScaleToFitHeight(true);

            List list = new List();
            for(String platform : (java.util.List<String>) product.get("platforms")) {
                list.add(new ListItem(platform, NORMAL));
            }
            platforms.add(subtitle);
            platforms.add(list);
            platforms.add(cover);

            LineSeparator line = new LineSeparator(1, 100, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, -3);

            paragraph.add(title);
            if(release.getYear()>=NOW.getYear()) {
                paragraph.add(new Chunk(new PositionedMark(73)));
            } else {
                paragraph.add(new Chunk(new PositionedMark(72)));
            }
            paragraph.add(line);
            paragraph.setSpacingBefore(18); // 18 * 1/72 inch.
            document.add(paragraph);
            paragraph = new Paragraph();
            paragraph.add(description);
            paragraph.add(platforms);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);

            document.add(paragraph);
        }

        document.close();
    }

    private class PositionedMark implements DrawInterface {
        private int code;

        public PositionedMark(int code) {
            this.code = code;
        }

        @Override
        public void draw(PdfContentByte canvas, float llx, float lly, float urx, float ury, float y) {
            canvas.beginText();
            canvas.setFontAndSize(ZAPFDINGBATS, 12);

            canvas.moveText(urx - 12, y);
            canvas.showText(String.valueOf((char) code));
            canvas.endText();
        }
    }

    @Test
    // use low-level api to write the content.
    public void testWriteDirect() throws Exception {
        File file = new File(environment.tmpFolder() + File.separator + "testWriteDirect.pdf");
        file.delete();
        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        // allowing to read the PDF syntax when opening the file;
        pdfWriter.setCompressionLevel(0);

        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        canvas.saveState();

        document.add(new Paragraph("你好!", NORMAL));

        Phrase hello = new Phrase("你好! (at 144, 768 & rotate 270 degree)", NORMAL);


        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, hello, 144, 768, 270);
        canvas.beginText();
        canvas.setFontAndSize(BASEFONT, 12);
        canvas.moveText(0, 0);
        canvas.showText("你好! (at 0, 0)");
        canvas.moveText(200, 200);
        canvas.showText("你好! (at 200, 200)");
        canvas.moveText(100, 100);
        canvas.showText("你好! (at 300, 300)");
        canvas.endText();

        canvas.restoreState();

        document.close();

    }
}
