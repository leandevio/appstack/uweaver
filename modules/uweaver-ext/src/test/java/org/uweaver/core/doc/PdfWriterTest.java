package org.uweaver.core.doc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.DateTimeConverter;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.MediaType;
import org.uweaver.core.util.NumberConverter;

import java.io.*;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jasonlin on 7/7/16.
 */
public class PdfWriterTest {
    Environment environment = Environment.getDefaultInstance();
    JSONParser jsonParser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    java.util.List<Map> products;
    DateTimeConverter dateTimeConverter = new DateTimeConverter();

    @BeforeTest
    public void setUp() throws Exception {
        dateTimeConverter.setDateTimePattern("yyyy/MM/dd");
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + "products.json");
        products = Arrays.asList(jsonParser.readValue(in, Map[].class));
        in.close();
    }


    @Test
    public void testWriteExistingPptxSlideShow() throws Exception {
        PptxFile pptx = createPptxFile();
        File input = new File(environment.tmpFolder() + File.separator + "products.pptx");
        InputStream in = new FileInputStream(input);
        SlideShowReader reader = new PptxReader(in);

        File output = new File(environment.tmpFolder() + File.separator + "testWriteExistingPptxSlideShow.pdf");

        output.delete();
        OutputStream out = new FileOutputStream(output);
        SlideShowWriter writer = new PdfWriter(out);

        SlideShow slideShow = new SlideShow(reader, writer);
        slideShow.setDefaultLocale(Locale.TAIWAN);
        slideShow.open();
        slideShow.close();
    }

    @Test
    public void testWriteNewPptxSlideShow() throws Exception {
        InputStream template = clazz.getClassLoader().getResourceAsStream(path + File.separator + "SlideShowTpl.pptx");
        SlideShowReader reader = new PptxReader(template);

        File output = new File(environment.tmpFolder() + File.separator + "testWriteNewPptxSlideShow.pdf");
        output.delete();
        OutputStream out = new FileOutputStream(output);
        SlideShowWriter writer = new PdfWriter(out);

        SlideShow slideShow = new SlideShow(reader, writer);
        slideShow.setDefaultLocale(Locale.TAIWAN);
        slideShow.open();
        for(Map product : products) {
            Slide slide = slideShow.createSlide("Picture");
            Placeholder titlePlaceholder = slide.getPlaceholder("TITLE");
            titlePlaceholder.setText((String) product.get("name"));

            // add one picture.
            Placeholder coverPlaceholder = slide.getPlaceholder("PICTURE");
            InputStream coverData = clazz.getClassLoader().getResourceAsStream(path + File.separator + product.get("cover"));

            Image cover = slide.createImage(coverData, detectMediaType((String) product.get("cover")));
            coverPlaceholder.setContent(cover);

            // add one table.
//            Placeholder tablePlaceholder = slide.getPlaceholder("TABLE");
//            Table table = slide.createTable(2, 3);
//            table.setRowHeight(30d).setFontSize(12d).setTextAlign(TextAlign.CENTER).setVerticalAlign(VerticalAlign.MIDDLE);
//            TableRow row = table.getRow(0);
//            row.setBorderWidth(Border.Edge.BOTTOM, 0.5).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
//            row.getCell(0).setText("建議售價");row.getCell(1).setText(String.format("NT$%d", product.get("price")));
//            row = table.getRow(1);
//            row.setBorderWidth(Border.Edge.BOTTOM, 0.5).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
//            row.getCell(0).setText("發行日期");row.getCell(1).setText(dateTimeConverter.convert(product.get("release"), String.class));
//            row = table.getRow(2);
//
//            row.setBorderWidth(Border.Edge.BOTTOM, 0.5).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
//            row.getCell(0).setText("出版商");row.getCell(1).setText((String) product.get("publisher"));
//            tablePlaceholder.setContent(table);
        }
        slideShow.close();
    }

    private PptxFile createPptxFile() throws Exception {
        InputStream template = clazz.getClassLoader().getResourceAsStream(path + File.separator + "SlideShowTpl.pptx");
        PptxFile pptx = new PptxFile(template);

        for(Map product : products) {
            Slide slide = pptx.createSlide("Table & Picture");
            Placeholder titlePlaceholder = slide.getPlaceholder("TITLE");
            titlePlaceholder.setText((String) product.get("name"));

            // add one picture.
            Placeholder coverPlaceholder = slide.getPlaceholder("PICTURE");
            InputStream coverData = clazz.getClassLoader().getResourceAsStream(path + File.separator + product.get("cover"));

            Image cover = slide.createImage(coverData, detectMediaType((String) product.get("cover")));
            coverPlaceholder.setContent(cover);

            // add one table.
            Placeholder tablePlaceholder = slide.getPlaceholder("TABLE");
            Table table = slide.createTable(2, 3);
            table.setRowHeight(30d).setFontSize(12d).setTextAlign(TextAlign.CENTER).setVerticalAlign(VerticalAlign.MIDDLE);
            TableRow row = table.getRow(0);
            row.setBorderWidth(Border.Edge.BOTTOM, 0.5).setBorderColor(Border.Edge.BOTTOM, Color.BLACK);
            row.getCell(0).setText("建議售價");row.getCell(1).setText(String.format("NT$%d", product.get("price")));
            row = table.getRow(1);
            row.getCell(0).setText("發行日期");row.getCell(1).setText(dateTimeConverter.convert(product.get("release"), String.class));
            row = table.getRow(2);
            row.getCell(0).setText("出版商");row.getCell(1).setText((String) product.get("publisher"));
            tablePlaceholder.setContent(table);
        }

        String filepath = environment.tmpFolder() + File.separator + "products.pptx";
        File file = new File(filepath);
        file.delete();
        pptx.write(file);
        template.close();

        return pptx;
    }

    private MediaType detectMediaType(String filename) {
        MediaType mediaType;

        Pattern pattern = Pattern.compile("\\.([a-zA-Z]+)$");
        Matcher matcher = pattern.matcher(filename);
        String ext = matcher.find() ? matcher.group(1) : "";


        if(ext.equals("png")) {
            mediaType = MediaType.IMAGE_PNG;
        } else if(ext.equals("jpg")) {
            mediaType = MediaType.IMAGE_JPEG;
        } else {
            mediaType = null;
        }

        return mediaType;

    }
}