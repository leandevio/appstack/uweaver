/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Code obj = new Code();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Entity
public class Code {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uuid;

    /** 分類 */
    private String type;
    /** 代碼 */
    private String code;
    /** 文字 */
    private String text;
    /** 順序 */
    private double priority = 0;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    @Size(max = 36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter: {@link #type}.
     * @return {@link #type}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter: {@link #code}.
     * @return {@link #code}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    /**
     * Getter: {@link #text}.
     * @return {@link #text}.
     */
    @Column(length = 2000)
    @Size(max = 2000)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    /**
     * Getter: {@link #priority}.
     * @return {@link #priority}.
     */
    @Column(nullable = false)
    @NotNull
    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }
}
