/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Comment obj = new Comment();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Entity
public class Comment {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uuid;
    /** 編號 */
    private String key;
    /** 主旨 */
    private String subject;
    /** 本文 */
    private String text;
    /** Media Type */
    private String mediaType;
    /** 建立者 */
    private String preparedBy;
    /** 建立日期 */
    private Date createdOn;
    /** 註解標的 */
    private String commentOn;
    /** 種類 */
    private String type;
    /** 標籤 */
    private List<String> tags = new ArrayList<>();
    /** 自訂屬性 */
    private Map<String, String> attributes;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    @Size(max = 36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter: {@link #key}.
     * @return {@link #key}.
     */
    @Column(name = "\"key\"", length = 100)
    @Size(max = 100)
    public String getKey() {
        return key;
    }

    /**
     * Setter: {@link #key}.
     * @param key {@link #key}.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Getter: {@link #subject}.
     * @return {@link #subject}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getSubject() {
        return subject;
    }

    /**
     * Setter: {@link #subject}.
     * @param subject {@link #subject}.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Getter: {@link #mediaType}.
     * @return {@link #mediaType}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getMediaType() {
        return mediaType;
    }

    /**
     * Setter: {@link #mediaType}.
     * @param mediaType {@link #mediaType}.
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * Getter: {@link #createdOn}.
     * @return {@link #createdOn}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Setter: {@link #createdOn}.
     * @param createdOn {@link #createdOn}.
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }


    /**
     * Getter: {@link #preparedBy}.
     * @return {@link #preparedBy}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getPreparedBy() {
        return preparedBy;
    }

    /**
     * Setter: {@link #preparedBy}.
     * @param preparedBy {@link #preparedBy}.
     */
    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    /**
     * Getter: {@link #commentOn}.
     * @return {@link #commentOn}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getCommentOn() {
        return commentOn;
    }

    /**
     * Setter: {@link #commentOn}.
     * @param commentOn {@link #commentOn}.
     */
    public void setCommentOn(String commentOn) {
        this.commentOn = commentOn;
    }

    /**
     * Getter: {@link #type}.
     * @return {@link #type}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getType() {
        return type;
    }

    /**
     * Setter: {@link #type}.
     * @param type {@link #type}.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter: {@link #tags}.
     * @return {@link #tags}.
     */
    @ElementCollection
    @CollectionTable(name = "Comment_Tags", joinColumns = @JoinColumn(name = "Owner"))
    @Column(length = 100)
    @Size(max = 100)
    public List<String> getTags() {
        return tags;
    }

    /**
     * Setter: {@link #tags}.
     * @param tags {@link #tags}.
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * Getter: {@link #attributes}.
     * @return {@link #attributes}.
     */
    @ElementCollection
    @CollectionTable(name="Comment_Attributes",  joinColumns = @JoinColumn(name = "Owner"))
    @Column(name="Value", length = 200)
    @Size(max = 200)
    public Map<String, String> getAttributes() {
        return this.attributes;
    }

    /**
     * Setter: {@link #attributes}.
     * @param attributes {@link #attributes}.
     */
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public String getAttribute(String name) {
        return this.attributes.get(name);
    }

    public void setAttribute(String name, String value) {
        this.attributes.put(name, value);
    }

    /**
     * Getter: {@link #text}.
     * @return {@link #text}.
     */
    @Lob
    public String getText() {
        return this.text;
    }

    /**
     * Setter: {@link #text}.
     * @param text {@link #text}.
     */
    public void setText(String text) {
        this.text = text;
    }
}
