/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SequenceNumber obj = new SequenceNumber();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Entity
public class Sequence {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uri;
    /** 種子 */
    private long seed = 0;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uri}.
     * @return {@link #uri}.
     */
    @Id
    @Column(length=100)
    @Size(max = 100)
    public String getUri() {
        return uri;
    }

    /**
     * Setter: {@link #uri}.
     * @param uri {@link #uri}.
     */
    public void setUri(String uri) {
        this.uri = uri;
    }


    /**
     * Getter: {@link #seed}.
     * @return {@link #seed}.
     */
    @Column(nullable = false)
    @NotNull
    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

}
