package org.uweaver.jpa.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.uweaver.core.util.Environment;
import org.uweaver.data.JdbcDataSource;
import org.uweaver.jpa.repository.JpaRepositoryImpl;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Configuration of jpa service beans.
 *
 * Usage:
 *      @Import({DataContext.class})
 *
 * DataSource
 * EntityManagerFactory
 * TransactionManager
 * HibernateTransactionTranslator
 *
 * Created by jasonlin on 5/3/14.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"org.uweaver.repository"},
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager",
        repositoryBaseClass = JpaRepositoryImpl.class)
public class JpaContext implements ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(JpaContext.class);
    private static Environment environment = Environment.getDefaultInstance();
    private static org.springframework.context.ApplicationContext applicationContext = null;
    private static List<String> packagesToScan = new ArrayList<>();

    public JpaContext() {
        LOGGER.debug("Establishing JPA context ....");
    }

    @Override
    public void setApplicationContext(org.springframework.context.ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> type) {
        return applicationContext.getBean(type);
    }

    public static void addPackagesToScan(List<String> packages) {
        packagesToScan.addAll(packages);
    }
/*
    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2WebServer() throws SQLException {
        String port = environment.getProperty("h2.webserver.port", "9082");
        Server server = Server.createWebServer("-web", "-webAllowOthers", "-webPort", port);
        return server;
    }
*/

    /**
     * creates a new H2 instance within the same ApplicationContext and provides a DataSource interface to it, usable by JPA.
     * @return
     * @throws SQLException
     */
    @Primary
    @Bean(destroyMethod = "close")
    public DataSource dataSource() throws SQLException {
        return JdbcDataSource.getDefaultInstance();
    }

    /**
     * The method EntityManagerFactory entityManagerFactory() creates the EntityManagerFactory. This class is responsible for creating the EntityManager, and is JPA Provider specific. In this case, this shows the creation and setup of a Hibernate JPA Provider, including the provision of the datasource dataSource().
     * The EntityManagerFactory is responsible for identifying the JPA Entities to be made available, the classes to be treated as database mapping/ persistence beans
     *
     * @return
     * @throws SQLException
     */
    @Primary
    @Bean
    public EntityManagerFactory entityManagerFactory() throws SQLException {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setPersistenceUnitName("primary");
        factory.setJpaVendorAdapter(vendorAdapter);

        Properties props = new Properties();
        if(environment.property("db.dialect")!=null) {
            props.setProperty("hibernate.dialect", environment.property("db.dialect"));
        } else if(environment.property("hibernate.dialect")!=null) {
            props.setProperty("hibernate.dialect", environment.property("hibernate.dialect"));
        }
        /*
         * 1.1.4. Hibernate configuration
         * The hbm2ddl.auto option turns on automatic generation of database schemas directly into the database. This can also be turned off by removing the configuration option, or redirected to a file with the help of the SchemaExport Ant task.
         */
        if(environment.property("db.upgrade")!=null) {
            props.setProperty("hibernate.hbm2ddl.auto", environment.property("db.upgrade"));
        } else if(environment.property("hibernate.hbm2ddl.auto")!=null) {
            props.setProperty("hibernate.hbm2ddl.auto", environment.property("hibernate.hbm2ddl.auto"));
        }

        props.setProperty("hibernate.show_sql", environment.property("db.debug", "true"));

        if(environment.property("db.schema")!=null) {
            props.setProperty("hibernate.default_schema", environment.property("db.schema"));
        } else if(environment.property("hibernate.default_schema")!=null) {
            props.setProperty("hibernate.default_schema", environment.property("hibernate.default_schema"));
        }
        props.setProperty("hibernate.physical_naming_strategy", "org.uweaver.jpa.context.HibernateNamingStrategy");
        factory.setJpaProperties(props);

        List entities = new ArrayList();
        entities.add("org.uweaver.entity");
        entities.addAll(packagesToScan);

        if(environment.property("entity.package")!=null) {
            entities.addAll(Arrays.asList(environment.property("entity.package").split(",")));
        } else if(environment.property("jpa.entity.package")!=null) {
            entities.addAll(Arrays.asList(environment.property("jpa.entity.package").split(",")));
        } else if(environment.property("jpa.domain")!=null) {
            entities.addAll(Arrays.asList(environment.property("jpa.domain").split(",")));
        }

        String[] packages = new String[entities.size()];
        entities.toArray(packages);

        factory.setPackagesToScan(packages);
        factory.setDataSource(dataSource());
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    /**
     * transactionManager() initialises the JPA transaction manager. This integrates with the declarative Transaction Management features of Spring, permitting the use of @Transactional and associated classes and configuration
     *
     * @return
     * @throws SQLException
     */
    @Primary
    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }

    /**
     * Spring provides an exception translation framework to translate exceptions from many different sources into a consistent set that your application can use. In this case, the JPA Configuration expects a bean that provides these translations, which is provided by hibernateExceptionTranslator()
     *
     * @return
     */
    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

}
