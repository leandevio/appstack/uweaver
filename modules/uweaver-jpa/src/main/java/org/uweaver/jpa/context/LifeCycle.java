/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.jpa.context;

import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.event.server.ReadyEvent;
import org.uweaver.core.event.server.StopEvent;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.observer.annotation.Subscribe;
import org.uweaver.service.SequenceService;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * LifeCycle obj = new LifeCycle();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@AppListener
public class LifeCycle {
    private static final Logger LOGGER = LogManager.getLogger(LifeCycle.class);

    public void initialize() {
        LOGGER.debug("Initializing the uweaver-jpa module ...");
        LOGGER.debug("The uweaver-jpa module was initialized.");
    }

    public void destroy() {
        LOGGER.debug("Destroying the uweaver-jpa module ...");
        LOGGER.debug("The uweaver-jpa module was destroyed.");
    }

    @Subscribe
    public void onReady(ReadyEvent event) {
        initialize();
    }

    @Subscribe
    public void onStop(StopEvent event) {
        destroy();
    }

}