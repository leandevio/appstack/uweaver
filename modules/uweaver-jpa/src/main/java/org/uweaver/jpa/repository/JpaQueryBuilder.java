/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.jpa.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.util.Converters;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.data.QueryBuilder;
import org.uweaver.data.Sorter;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * JpaQueryBuilder obj = new JpaQueryBuilder();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class JpaQueryBuilder extends QueryBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaQueryBuilder.class);

    Converters converters = new Converters();

    public TypedQuery typedQuery(EntityManager em) {
        String sql = sql();

        LOGGER.info(sql);
        TypedQuery query = em.createQuery(sql.toString(), this.getEntity());

        bind(query);

        return query;
    }

    public Query query(EntityManager em) {
        String sql = sql();

        LOGGER.info(sql);
        Query query = em.createQuery(sql.toString());

        bind(query);

        return query;
    }

    private void bind(Query query) {
        Condition condition = getCondition();

        if(condition==null||condition.getFilters()==null) return;

        for(int i=0; i<condition.getFilters().size(); i++) {
            Filter filter = condition.getFilters().get(i);
            Object value = filter.getValue();
            if(filter.getOperator().equals(Filter.Operator.LIKE) && value!=null && value instanceof String && !((String) value).contains("%")) {
                value = "%" + value + "%";
            }

            Class javaType = query.getParameter(i+1).getParameterType();

            Object parameter;

            if(value instanceof Collection) {
                List values = new ArrayList();
                for(Object datum : (Collection) value) {
                    Object p = converters.convert(datum, javaType);
                    values.add(p);

                }
                parameter = values;
            } else if(value.getClass().isArray()) {
                List values = new ArrayList();
                for(Object datum : (Object[]) value) {
                    Object p = converters.convert(datum, javaType);
                    values.add(p);

                }
                parameter = values;
            } else {
                Object p = converters.convert(value, javaType);
                parameter = p;
            }


            query.setParameter(i+1, parameter);
        }
    }

    @Override
    public String sql() {
        StringBuffer sql = new StringBuffer();
        Set<String> relatedEntities = new HashSet<>();
        String entityAlias = (getEntityAlias()==null) ? "_" + getEntityName() : getEntityAlias();

        StringBuffer join = new StringBuffer();
        StringBuffer where = new StringBuffer();
        StringBuffer orderBy = new StringBuffer();
        Condition condition = getCondition();

        if(condition!=null && condition.getFilters()!=null) {
            for(int i=0; i<condition.getFilters().size(); i++) {
                Filter filter = condition.getFilters().get(i);
                String property = filter.getProperty();
                String operator = filter.getSQLOperator();

                where.append(entityAlias).append(".").append(property).append(" ")
                        .append(operator).append(" ?" + (i+1)).append(" ").append(condition.getConjunction()).append(" ");
                if(property.contains(".")) {
                    relatedEntities.add(property.substring(0, property.indexOf(".")));
                }
            }
            if(where.length()>0) {
                where.delete(where.length() - (condition.getConjunction().toString().length()+2), where.length());
                where.insert(0, " WHERE ");
            }
        }
        if(getSorters()!=null) {
            for(Sorter sorter : getSorters()) {
                orderBy.append(entityAlias).append(".").append(sorter.getProperty()).append(" ")
                        .append(sorter.getDirection()).append(", ");
            }

            if(orderBy.length()>0) {
                orderBy.delete(orderBy.length() - 2, orderBy.length());
                orderBy.insert(0, " ORDER BY ");
            }
        }

        for(String entity : relatedEntities) {
            String alias = "__" + entity;
            String selector = entityAlias + "." + entity;
            join.append(" left join ").append(entityAlias).append(".").append(entity).append(" ").append(alias);
            where = new StringBuffer(where.toString().replaceAll(selector, alias));
        }


        String selectExpr;
        if(getSelectExpr()==null) {
//            selectExpr = (join.length()>0) ? "DISTINCT " + entityAlias : entityAlias;
            selectExpr = entityAlias;
        } else {
            selectExpr = getSelectExpr();
        }

        sql.append("SELECT ").append(selectExpr).append(" FROM ").append(getEntityName()).append(" ").append(entityAlias);
        sql.append(join);
        sql.append(where);
        sql.append(orderBy);

        return sql.toString();
    }
}
