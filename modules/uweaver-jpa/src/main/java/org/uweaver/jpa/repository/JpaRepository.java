package org.uweaver.jpa.repository;

import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.data.Condition;
import org.uweaver.search.Sorter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jasonlin on 5/5/14.
 */
public interface JpaRepository<T, ID extends Serializable>
        extends org.springframework.data.jpa.repository.JpaRepository<T, ID> {

    T clone(ID id);

    Iterable<T> search(List<Predicate> predicates, Sorter sorter);

    Iterable<T> search(List<Predicate> predicates, Sorter sorter, Pageable pageable);

    Iterable<T> search(List<Predicate> predicates, Pageable pageable);

    Iterable<T> search(List<Predicate> predicates);

    long count(List<Predicate> predicates);

    Iterable<T> search(Condition condition, Sorter sorter);

    Iterable<T> search(Condition condition, Sorter sorter, Pageable pageable);

    Iterable<T> search(Condition condition, Pageable pageable);

    Iterable<T> search(Condition condition, Sorter sorter, Integer limit, Long offset);

    Iterable<T> search(Condition condition, Integer limit, Long offset);

    Iterable<T> search(Condition condition, Iterable<org.uweaver.data.Sorter> sorters, Integer limit, Long offset);

    Iterable<T> search(Condition condition, Iterable<org.uweaver.data.Sorter> sorters, Integer limit, Integer offset);

    Iterable<T> search(Condition condition, Iterable<org.uweaver.data.Sorter> sorters);

    Iterable<T> search(Condition condition, org.uweaver.data.Sorter[] sorters, Integer limit, Long offset);

    Iterable<T> search(Condition condition, org.uweaver.data.Sorter[] sorters, Integer limit, Integer offset);

    Iterable<T> search(Condition condition, org.uweaver.data.Sorter[] sorters);

    Long count(Condition condition);

    void detach(T bean);

}