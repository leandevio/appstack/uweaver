package org.uweaver.jpa.repository;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by jasonlin on 5/5/14.
 */
public class JpaRepositoryFactoryBean<R extends org.springframework.data.jpa.repository.JpaRepository<T, I>, T, I extends Serializable>
        extends org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean<R, T, I> {

    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {

        return new JpaRepositoryFactory(entityManager);
    }

    private static class JpaRepositoryFactory<T, I extends Serializable>
            extends org.springframework.data.jpa.repository.support.JpaRepositoryFactory {

        private EntityManager entityManager;

        public JpaRepositoryFactory(EntityManager entityManager) {
            super(entityManager);

            this.entityManager = entityManager;
        }

        protected Object getTargetRepository(RepositoryInformation information) {
            JpaEntityInformation<?, Serializable> entityInformation = getEntityInformation(information.getDomainType());
            return new JpaRepositoryImpl<>(entityInformation, entityManager);
        }

        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {

            // The RepositoryMetadata can be safely ignored, it is used by the JpaRepositoryFactory
            //to check for QueryDslJpaRepository's which is out of scope.
            return JpaRepository.class;
        }
    }
}