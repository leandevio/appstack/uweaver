package org.uweaver.jpa.repository;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.core.util.Item;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.search.Sort;
import org.uweaver.search.Sorter;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.*;


/**
 * Created by jasonlin on 5/5/14.
 */
public class JpaRepositoryImpl<T, ID extends Serializable>
        extends SimpleJpaRepository<T, ID> implements JpaRepository<T, ID> {

    private EntityManager entityManager;
    private JpaEntityInformation<T, ?> entityInformation;

    public JpaRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);

        this.entityInformation = entityInformation;

        this.entityManager = entityManager;

    }

    @Override
    public T clone(ID id) {
        T bean;

        bean = findOne(id);

        entityManager.detach(bean);

        return bean;
    }


    @Override
    public Iterable<T> search(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        Condition condition = toCondition(predicates);
        int limit = pageable.size();
        long offset = pageable.offset();
        List<org.uweaver.data.Sorter> sorters = toSorters(sorter);
        return search(condition, sorters, limit, offset);
    }

    private List<org.uweaver.data.Sorter> toSorters(Sorter sorter) {
        List<org.uweaver.data.Sorter> sorters = new ArrayList<>();
        for(Sort sort : sorter.sorts()) {
            String property = sort.property();
            org.uweaver.data.Sorter.Direction direction = org.uweaver.data.Sorter.Direction.valueOf(sort.direction().name());
            sorters.add(new org.uweaver.data.Sorter(property, direction));
        }
        return sorters;
    }

    @Override
    public Iterable<T> search(List<Predicate> predicates, Pageable pageable) {
        return search(predicates, new Sorter(), pageable);
    }

    @Override
    public Iterable<T> search(List<Predicate> predicates, Sorter sorter) {
        Condition condition = toCondition(predicates);
        return search(condition, toSorters(sorter));
    }


    @Override
    public Iterable<T> search(List<Predicate> predicates) {
        return search(predicates, new org.uweaver.search.Sorter());
    }


    @Override
    public long count(List<Predicate> predicates) {
        Condition condition = toCondition(predicates);
        return count(condition);
    }

    @Override
    public Iterable<T> search(Condition condition, Sorter sorter) {
        return search(condition, toSorters(sorter));
    }

    @Override
    public Iterable<T> search(Condition condition, Sorter sorter, Pageable pageable) {
        int limit = pageable.size();
        long offset = pageable.offset();
        return search(condition, toSorters(sorter), limit, offset);
    }

    @Override
    public Iterable<T> search(Condition condition, Pageable pageable) {
        return search(condition, new Sorter(), pageable);
    }

    @Override
    public Iterable<T> search(Condition condition, Sorter sorter, Integer limit, Long offset) {
        return search(condition, toSorters(sorter), limit, offset);
    }

    @Override
    public Iterable<T> search(Condition condition, Integer limit, Long offset) {
        return search(condition, new Sorter(), limit, offset);
    }

    private Condition toCondition(List<Predicate> predicates) {
        Condition condition = new Condition();
        for(Predicate predicate : predicates) {
            if(predicate.expectation()==null) continue;
            condition.addFilter(new Filter(predicate));
        }
        return condition;
    }

    @Override
    public Iterable<T> search(Condition condition, Iterable<org.uweaver.data.Sorter> sorters, Integer limit, Long offset) {
        Class<T> entityType = entityInformation.getJavaType();

        JpaQueryBuilder builder = new JpaQueryBuilder();

        builder.from(entityType).where(condition).orderBy(sorters);

        TypedQuery<T> query = builder.typedQuery(entityManager);

        if(limit!=null) query.setMaxResults(limit);
        if(offset!=null) query.setFirstResult(offset.intValue());

        List<T> beans = query.getResultList();

        List<T> distinctBeans = new ArrayList<>();
        StringBuffer index = new StringBuffer();
        for(T bean : beans) {
            List<String> idAttributes = (List<String>) this.entityInformation.getIdAttributeNames();
            StringBuffer key = new StringBuffer("|");
            Item item = new Item(bean);
            for(String id: idAttributes) {
                key.append(item.get(id));
            }
            key.append("|");
            if(index.indexOf(key.toString())==-1) {
                distinctBeans.add(bean);
                index.append("|").append(key).append("|");
            }

        }

        return distinctBeans;
    }

    @Override
    public Iterable<T> search(Condition condition, Iterable<org.uweaver.data.Sorter> sorters, Integer limit, Integer offset) {
        return search(condition, sorters, limit, offset);
    }

    @Override
    public Iterable<T> search(Condition condition, Iterable<org.uweaver.data.Sorter> sorters) {
        return search(condition, sorters, null, (Long) null);
    }

    @Override
    public Iterable<T> search(Condition condition, org.uweaver.data.Sorter[] sorters, Integer limit, Long offset) {
        Iterable<org.uweaver.data.Sorter> sorts = (sorters==null) ? null : Arrays.asList(sorters);
        return search(condition, sorts, limit, offset);
    }

    @Override
    public Iterable<T> search(Condition condition, org.uweaver.data.Sorter[] sorters, Integer limit, Integer offset) {
        return search(condition, sorters, limit, offset);
    }

    @Override
    public Iterable<T> search(Condition condition, org.uweaver.data.Sorter[] sorters) {
        return search(condition, sorters, null, (Long) null);
    }
    
    @Override
    public Long count(Condition condition) {
        String entityName = entityInformation.getEntityName();
        String entityAlias = "e";

        JpaQueryBuilder builder = new JpaQueryBuilder();

        builder.select("count(e)").from(entityName, entityAlias).where(condition);

        Query query = builder.query(entityManager);

        return (Long) query.getSingleResult();
    }

    @Override
    public void detach(T bean) {
        entityManager.detach(bean);
    }


    @Override
    public <S extends T> S save(S bean) {
        return super.save(bean);
    }

}