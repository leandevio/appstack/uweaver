/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.data.DataInstaller;
import org.uweaver.entity.Code;
import org.uweaver.repository.CodeRepository;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.search.Searchable;
import org.uweaver.search.Sorter;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * CodeService obj = new CodeService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Service
@Transactional("transactionManager")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CodeService implements Searchable<Code> {
    private DataInstaller dataInstaller = new DataInstaller();

    @Autowired
    CodeRepository repository;

    @PostConstruct
    public void init(){
        if(repository.count()==0) {
            List<Code> beans = Arrays.asList(dataInstaller.read(Code[].class));
            repository.save(beans);
        }
    }

    public List<Code> findAllByType(String type) {
        return repository.findAllByType(type, new Sort(Sort.Direction.ASC, "code"));
    }

    public List<Code> findAllByType(String type, Sort sort) {
        return repository.findAllByType(type, sort);
    }

    public Code findOneByTypeAndCode(String type, String code) {
        return repository.findOneByTypeAndCode(type, code);
    }

    public Code save(Code code) {
        return repository.save(code);
    }

    public void delete(String type, String code) {
        Code bean = repository.findOneByTypeAndCode(type, code);
        repository.delete(bean);
    }

    public Code findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public List<Code> search(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        List<Code> codes = (List<Code>) repository.search(predicates, sorter, pageable);
        return codes;
    }

    @Override
    public long count(List<Predicate> predicates) {
        return repository.count(predicates);
    }
}
