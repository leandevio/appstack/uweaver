/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.entity.Comment;
import org.uweaver.repository.CommentRepository;

import java.util.Date;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * CommentService obj = new CommentService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Service
@Transactional("transactionManager")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    public List<Comment> findAllByCommendOn(String commentOn) {
        return commentRepository.findAllByCommentOn(commentOn);
    }

    public List<Comment> findAllByCreatedOnBetween(Date start, Date end) {
        return commentRepository.findAllByCreatedOnBetween(start, end);
    }

    public List<Comment> findAllByType(String type) {
        return commentRepository.findAllByType(type);
    }

    public List<Comment> findAllByPreparedBy(String preparedBy) {
        return commentRepository.findAllByPreparedBy(preparedBy);
    }
}
