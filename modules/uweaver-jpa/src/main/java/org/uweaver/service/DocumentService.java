/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.entity.Document;
import org.uweaver.repository.DocumentRepository;

import java.util.Date;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AttachmentService obj = new AttachmentService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Service
@Transactional("transactionManager")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DocumentService {

    @Autowired
    DocumentRepository repository;

    public List<Document> findAllByCreatedOnBetween(Date start, Date end) {
        return repository.findAllByCreatedOnBetween(start, end);
    }

    public List<Document> findAllByCreatedOnIsBetween(Date start, Date end) {
        return repository.findAllByCreatedOnIsBetween(start, end);
    }

    public List<Document> findAllByModifiedOnBetween(Date start, Date end) {
        return repository.findAllByModifiedOnBetween(start, end);
    }

    public List<Document> findAllByModifiedOnIsBetween(Date start, Date end) {
        return repository.findAllByModifiedOnIsBetween(start, end);
    }

    public List<Document> findAllByPreparedBy(String preparedBy) {
        return repository.findAllByPreparedBy(preparedBy);
    }

    public List<Document> findAllByModifiedBy(String modifiedBy) {
        return repository.findAllByModifiedBy(modifiedBy);
    }
}
