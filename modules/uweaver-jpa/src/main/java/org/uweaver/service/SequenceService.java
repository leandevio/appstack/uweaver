/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.entity.Sequence;
import org.uweaver.repository.SequenceRepository;

import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * NumberSequence obj = new NumberSequence();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Service
@Transactional(value="transactionManager", propagation=Propagation.REQUIRES_NEW)
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SequenceService {
    private Map<String, Seed> seeds = new ConcurrentHashMap<>();
    private int BUFFER = 5;

    @Autowired
    SequenceRepository repository;

    @PreDestroy
    public void destroy() {
        flush();
    }

    public long nextNumber(String uri) {
        return nextNumber(uri, 1);
    }

    public long nextNumber(String uri, int increment) {
        Seed seed = getSeed(uri);

        if(seed.current>=seed.max) {
            Sequence sequence = repository.findOne(uri);
            sequence.setSeed(sequence.getSeed() + BUFFER);
            repository.saveAndFlush(sequence);
            seed.max = sequence.getSeed();
        }

        seed.current = seed.current + increment;

        return seed.current;
    }

    public long currentNumber(String uri) {
        return getSeed(uri).current;
    }

    public synchronized void flush() {
        for(String uri : seeds.keySet()) {
            Seed seed = seeds.get(uri);
            seed.max = seed.current;
            Sequence sequence = repository.getOne(uri);
            sequence.setSeed(seed.current);
            repository.saveAndFlush(sequence);
        }
    }

    private Seed getSeed(String uri) {
        Seed seed = seeds.get(uri);

        if(seed==null) {
            synchronized(seeds) {
                seed = new Seed();
                Sequence sequence = repository.findOne(uri);
                if(sequence==null) {
                    sequence = new Sequence();
                    sequence.setUri(uri);
                } else {
                    seed.current = sequence.getSeed();
                }
                sequence.setSeed(sequence.getSeed() + BUFFER);
                repository.saveAndFlush(sequence);
                seed.max = sequence.getSeed();
                seeds.put(sequence.getUri(), seed);
            }
        }

        return seed;
    }

    private class Seed {
        public long current = 0L;
        public long max = 0L;
    }

}
