/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.leandev.flora.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.uweaver.jpa.context.JpaContext;
import org.uweaver.jpa.repository.JpaRepositoryImpl;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TestConfig obj = new TestConfig();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.leandev.flora.repository"},
        repositoryBaseClass = JpaRepositoryImpl.class)
@ComponentScan(basePackages = {"com.leandev.flora.service"})
@EnableTransactionManagement
@Import({JpaContext.class})
public class AppContext {
    private static Logger LOGGER = LoggerFactory.getLogger(AppContext.class);
    AppContext() {
        LOGGER.debug("Loading AppContext Configuration ....");
    }
}
