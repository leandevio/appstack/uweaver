package org.uweaver.jpa.repository;

import com.leandev.flora.config.AppContext;
import com.leandev.flora.domain.Order;
import com.leandev.flora.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.data.Sorter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.File;
import java.util.Arrays;
import java.util.List;


import static org.testng.Assert.*;

/**
 * Created by jasonlin on 4/28/16.
 */
@ContextConfiguration(classes=AppContext.class, loader=AnnotationConfigContextLoader.class)
public class JpaQueryBuilderTest extends AbstractTestNGSpringContextTests {
    private String DATEFILTER = "[{\"property\":\"orderDate\", \"value\":\"2016-03-01T00:00:00.000+0800\", \"operator\":\"GE\"}, {\"property\":\"orderDate\", \"value\":\"2016-03-03T23:59:59.000+0800\", \"operator\":\"LE\"}]";
    private String RELATIONALFILTER = "[{\"property\":\"items.product\", \"value\":\"Rose\"}, {\"property\":\"items.product\", \"value\":\"Sunflower\"}]";
    private String INFILTER = "[{\"property\":\"items.product\", \"value\":[\"Rose\", \"Sunflower\"], \"operator\":\"IN\"}]";
    private String CIFILTER = "[{\"property\":\"items.product\", \"value\":[\"rose\", \"sunflower\"], \"operator\":\"IN\"}]";
    private String SORTERSASC = "[{\"property\":\"orderDate\", \"direction\":\"ASC\"}]";
    private String SORTERSDESC = "[{\"property\":\"orderDate\", \"direction\":\"DESC\"}]";
    private String CONJUNCTION = "OR";

    private JSONParser parser = new JSONParser();
    private Environment environment = Environment.getDefaultInstance();
    JpaQueryBuilder queryBuilder = new JpaQueryBuilder();

    private List<Order> orders;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private OrderRepository repository;


    @BeforeClass
    public void setUp() throws Exception {
        File data = new File(environment.dataFolder() + File.separator + "order.json");
        orders = Arrays.asList(parser.readValue(data, Order[].class));
        for(Order order:orders) {
            order.patch();
        }

        repository.deleteAll();
        repository.save(orders);
    }

    @Test
    public void testTypedQuery() throws Exception {
        List<Sorter> sorters = Arrays.asList(parser.readValue(SORTERSASC, Sorter[].class));

        queryBuilder.reset();

        queryBuilder.from(Order.class).orderBy(sorters);

        TypedQuery query = queryBuilder.typedQuery(em);

        List<Order> beans = query.getResultList();

        assertEquals(beans, orders);
    }


    @Test
    public void testTypedQueryDateFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(DATEFILTER, Filter[].class));
        List<Sorter> sorters = Arrays.asList(parser.readValue(SORTERSDESC, Sorter[].class));

        queryBuilder.reset();

        queryBuilder.from(Order.class).where(condition).orderBy(sorters);

        TypedQuery query = queryBuilder.typedQuery(em);

        List<Order> beans = query.getResultList();

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testTypedQueryRelationalFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(RELATIONALFILTER, Filter[].class), CONJUNCTION);
        List<Sorter> sorters = Arrays.asList(parser.readValue(SORTERSDESC, Sorter[].class));

        queryBuilder.reset();

        queryBuilder.from(Order.class).where(condition).orderBy(sorters);

        TypedQuery query = queryBuilder.typedQuery(em);

        List<Order> beans = query.getResultList();

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testTypedQueryInFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(INFILTER, Filter[].class));

        queryBuilder.reset();

        queryBuilder.from(Order.class).where(condition).orderBy(parser.readValue(SORTERSDESC, Sorter[].class));

        TypedQuery query = queryBuilder.typedQuery(em);

        List<Order> beans = query.getResultList();

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testTypedQueryCIFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(CIFILTER, Filter[].class));

        queryBuilder.reset();

        queryBuilder.from(Order.class).where(condition).orderBy(parser.readValue(SORTERSDESC, Sorter[].class));

        TypedQuery query = queryBuilder.typedQuery(em);

        List<Order> beans = query.getResultList();

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testQuery() throws Exception {
        Condition condition = new Condition(parser.readValue(DATEFILTER, Filter[].class));

        queryBuilder.reset();

        queryBuilder.select("count(o)").from(Order.class, "o").where(condition);

        Query query = queryBuilder.query(em);

        Long count = (Long) query.getSingleResult();

        assertEquals(count.longValue(), 3);

    }

}