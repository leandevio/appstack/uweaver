/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.jpa.repository;

import com.leandev.flora.config.AppContext;
import com.leandev.flora.domain.Order;
import com.leandev.flora.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.data.Condition;
import org.uweaver.data.Filter;
import org.uweaver.data.Sorter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * JpaRepositoryTest obj = new JpaRepositoryTest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@ContextConfiguration(classes=AppContext.class, loader=AnnotationConfigContextLoader.class)
public class JpaRepositoryTest extends AbstractTestNGSpringContextTests {
    private String DATEFILTER = "[{\"property\":\"orderDate\", \"value\":\"2016-03-01T00:00:00.000+0800\", \"operator\":\"GE\"}, {\"property\":\"orderDate\", \"value\":\"2016-03-03T23:59:59.000+0800\", \"operator\":\"LE\"}]";
    private String RELATIONALFILTER = "[{\"property\":\"items.product\", \"value\":\"Rose\"}, {\"property\":\"items.product\", \"value\":\"Sunflower\"}]";
    private String INFILTER = "[{\"property\":\"items.product\", \"value\":[\"Rose\", \"Sunflower\"], \"operator\":\"IN\"}]";
    private String CIFILTER = "[{\"property\":\"items.product\", \"value\":[\"rose\", \"sunflower\"], \"operator\":\"IN\"}]";
    private String SORTERSASC = "[{\"property\":\"orderDate\", \"direction\":\"ASC\"}]";
    private String SORTERSDESC = "[{\"property\":\"orderDate\", \"direction\":\"DESC\"}]";
    private String CONJUNCTION = "OR";

    private JSONParser parser = new JSONParser();
    private Environment environment = Environment.getDefaultInstance();

    private List<Order> orders;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private OrderRepository repository;


    @BeforeClass
    public void setUp() throws Exception {
        File data = new File(environment.dataFolder() + File.separator + "order.json");
        orders = Arrays.asList(parser.readValue(data, Order[].class));
        for(Order order:orders) {
            order.patch();
        }

        repository.deleteAll();
        repository.save(orders);
    }



    @Test
    public void testSearchDateFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(DATEFILTER, Filter[].class));
        Sorter[] sorters = parser.readValue(SORTERSDESC, Sorter[].class);

        List<Order> beans = (List<Order>) repository.search(condition, sorters);

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testSearchRelationalFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(RELATIONALFILTER, Filter[].class), CONJUNCTION);
        Sorter[] sorters = parser.readValue(SORTERSDESC, Sorter[].class);

        List<Order> beans = (List<Order>) repository.search(condition, sorters);

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testSearchInFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(INFILTER, Filter[].class), CONJUNCTION);
        Sorter[] sorters = parser.readValue(SORTERSDESC, Sorter[].class);

        List<Order> beans = (List<Order>) repository.search(condition, sorters);

        assertEquals(beans.size(), 3);
    }

    @Test
    public void testSearchCIFilter() throws Exception {
        Condition condition = new Condition(parser.readValue(CIFILTER, Filter[].class), CONJUNCTION);
        Sorter[] sorters = parser.readValue(SORTERSDESC, Sorter[].class);

        List<Order> beans = (List<Order>) repository.search(condition, sorters);

        assertEquals(beans.size(), 3);
    }

}
