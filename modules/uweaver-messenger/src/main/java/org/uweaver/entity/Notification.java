package org.uweaver.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
public class Notification {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uuid;
    /** 主旨 */
    private String subject;
    /** 本文 */
    private String content;
    /** 發送者 */
    private String sender;
    /** 接收者 */
    private String recipient;
    /** 追隨者 */
    private String followers;
    /** 建立日期 */
    private Date createdOn;
    /** 發送日期 */
    private Date sentOn;
    /** 已讀取 */
    private Boolean read = false;
    /** Channel */
    private String channel;
    /** Data */
    private String data;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    @Size(max = 36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Column(length = 200, nullable = false)
    @NotNull
    @Size(max = 200)
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Column(length = 2000)
    @Size(max = 2000)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getSentOn() {
        return sentOn;
    }

    public void setSentOn(Date sentOn) {
        this.sentOn = sentOn;
    }

    @Column(nullable = false)
    @NotNull
    public Boolean isRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    @Column(length = 100)
    @Size(max = 100)
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Column(length = 2000)
    @Size(max = 2000)
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Column(length = 200, name="followers")
    @Size(max = 200)
    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public List<String> followers() {
        List<String> followers = Arrays.asList(getFollowers().split(","));
        return followers;
    }

    public void setFollowers(List<String> followers) {
        StringBuilder sb = new StringBuilder();
        for(String follower : followers) {
            sb.append(follower).append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        setFollowers(sb.toString());
    }
}
