package org.uweaver.repository;

import org.uweaver.entity.Notification;
import org.uweaver.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, String> {
}
