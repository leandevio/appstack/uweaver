package org.uweaver.service.messenger;

import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.mail.HtmlMail;
import org.uweaver.core.mail.MailSender;
import org.uweaver.core.util.Environment;
import org.uweaver.entity.Notification;
import org.uweaver.i18n.I18n;
import org.uweaver.jpa.context.JpaContext;
import org.uweaver.search.*;
import org.uweaver.security.Pass;
import org.uweaver.service.identity.IdentityManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Messenger {
    private static final Logger LOGGER = LogManager.getLogger(Messenger.class);
    private static final Environment environment = Environment.getDefaultInstance();
    private NotificationService notificationService;
    private IdentityManager identityManager = new IdentityManager();
    private static final I18n i18n = new I18n();

    public Messenger() {
        notificationService = JpaContext.getBean(NotificationService.class);
    }

    public boolean push(Notification notification) {
        Date now = new Date();
        notification.setCreatedOn(now);
        if(send(notification)) notification.setSentOn(now);
        notificationService.save(notification);
        return (notification.getSentOn()!=null);
    }

    public boolean send(Notification notification) {
        boolean sent = false;
        try {
            MailSender mailSender = new MailSender();

            if(!mailSender.test()) return sent;

            HtmlMail mail = new HtmlMail();

            Pass pass = identityManager.createPassQuery().username(notification.getRecipient()).singleResult();
            if(pass==null||pass.email()==null) return sent;

            String sender = environment.property("messenger.sender", environment.appName() + "@localhost");

            mail.setFrom(sender, i18n.translate(environment.appName()));
            mail.setTo(pass.email());
            mail.setSubject(notification.getSubject());
            mail.setContent(notification.getContent());
            mail.setSentDate(new Date());
            mail.setSender(mailSender);
            mail.send();

            sent = true;
        } catch (Exception e) {
            LOGGER.warn(e);
        }
        return sent;
    }

    public void resend() {
        List<Predicate> predicates = new ArrayList<>();
        Predicate predicate = new Predicate("sent", null);
        predicates.add(predicate);
        Sort sort = new Sort("createOn", Sort.Direction.DESC);
        Sorter sorter = new Sorter(sort);
        List<Notification> notifications = notificationService.search(predicates, sorter, new PageRequest());
        for(Notification notification : notifications) {
            if(send(notification))  {
                notification.setSentOn(new Date());
                notificationService.save(notification);
            }
        }
    }

    public NotificationQuery createNotificationQuery() {
        return new NotificationQuery(notificationService);
    }
}
