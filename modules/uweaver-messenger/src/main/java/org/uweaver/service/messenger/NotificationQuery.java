package org.uweaver.service.messenger;

import org.uweaver.entity.Notification;
import org.uweaver.search.*;

import java.util.ArrayList;
import java.util.List;

public class NotificationQuery {
    private NotificationService notificationService;
    private List<Predicate> predicates = new ArrayList<>();
    private Sorter sorter = new Sorter();

    public NotificationQuery(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    public List<Notification> list(Pageable pageable) {
        return notificationService.search(predicates, sorter, pageable);
    }

    public List<Notification> list() {
        return list(new PageRequest());
    }

    public Notification singleResult() {
        List<Notification> notifications = list();
        return notifications.size() > 0 ? notifications.get(0) : null;
    }

    public NotificationQuery recipient(String recipient) {
        Predicate predicate = new Predicate("recipient", recipient);
        predicates.add(predicate);
        return this;
    }

    public NotificationQuery isRead(boolean isRead) {
        Predicate predicate = new Predicate("read", isRead);
        predicates.add(predicate);
        return this;
    }

    public NotificationQuery orderByCreatedOn() {
        return orderByCreatedOn(Sort.Direction.ASC);
    }

    public NotificationQuery orderByCreatedOn(Sort.Direction direction) {
        this.sorter.by(direction, "createdOn");
        return this;
    }
}
