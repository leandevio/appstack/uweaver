package org.uweaver.service.messenger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uweaver.entity.Notification;
import org.uweaver.repository.NotificationRepository;
import org.uweaver.search.Pageable;
import org.uweaver.search.Predicate;
import org.uweaver.search.Searchable;
import org.uweaver.search.Sorter;

import java.util.List;

@Service
@Transactional("transactionManager")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NotificationService  implements Searchable<Notification> {
    @Autowired
    NotificationRepository repository;

    public void save(Notification notification) {
        repository.save(notification);
    }

    public void delete(String uuid) {
        repository.delete(uuid);
    }

    public Notification findOne(String uuid) {
        return repository.findOne(uuid);
    }

    @Override
    public List<Notification> search(List<Predicate> predicates, Sorter sorter, Pageable pageable) {
        List<Notification> notifications = (List<Notification>) repository.search(predicates, sorter, pageable);
        return notifications;
    }

    @Override
    public long count(List<Predicate> predicates) {
        return repository.count(predicates);
    }

}
