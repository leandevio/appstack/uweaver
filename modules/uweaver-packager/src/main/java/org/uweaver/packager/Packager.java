package org.uweaver.packager;

import org.uweaver.core.compress.Unzip;
import org.uweaver.core.net.HttpDownloader;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jasonlin on 4/29/14.
 */
public class Packager {

    private final String CDN = "http://alice.leandev.com.tw/cdn/weaver";
    private File cacheFolder;

    private final HttpDownloader downloader = new HttpDownloader();

    public Packager() {
        String userHome = System.getProperty("user.home");
        cacheFolder = new File(userHome + File.separator + ".weaver");
        if(!cacheFolder.exists()) {
            cacheFolder.mkdirs();
        }
        new File(cacheFolder + File.separator + "archetype").mkdirs();
    }

    public void create(String name) throws IOException {
        File workspace = new File("." + File.separator + "name");
        String archetype = "webapp";

        Unzip unzip = new Unzip(new File(download(archetype)));

//        unzip.extractAll(workspace.getAbsolutePath());
    }

    private String download(String archetype) throws IOException {
        String url = CDN + "/archetype/" + archetype + ".zip";
        String filepath = cacheFolder + File.separator + "archetype" + File.separator + archetype + ".zip";
        downloader.downloadFile(url, filepath);
        return filepath;
    }

    public void update(String name) {

    }

    public boolean isApp(String name) {
        File workspace = new File(name);
        return isApp(workspace);
    }

    public boolean isApp(File workspace) {
        if(!workspace.isDirectory()) return false;
        FilenameFilter fileNameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(name.equals(".weaver")) {
                    return true;
                }
                return false;
            }
        };
        if(workspace.listFiles(fileNameFilter).length!=1) {
            return false;
        }
        return true;
    }

    public List<String> searchApp(String path) {
        List apps = new ArrayList<String>();

        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        };
        File[] workspaces = new File(path).listFiles(filter);

        for(File workspace : workspaces) {
            if(isApp(workspace)) {
                apps.add(workspace.getName());
            }
        }
        return apps;
    }
}
