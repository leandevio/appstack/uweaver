/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.report;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRElementsVisitor;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRVisitorSupport;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.*;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * Report obj = new Report();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class Report {
    private static final Environment environment = Environment.getDefaultInstance();
    private static final String tplFolder = environment.tplFolder() + File.separator + "rpt";
    private static final String tmpFolder = environment.tmpFolder().toString();
    private String name;
    private Map<String, Object> parameters = new HashMap<>();
    private Connection connection;
    private JasperPrint print = null;

    public Report(String name) {
        this.name = name;
    }

    public Report setParameter(Map<String, Object> parameters) {
        this.parameters = parameters;
        return this;
    }

    public Report setConnection(Connection connection) {
        this.connection = connection;
        return this;
    }

    private InputStream openTemplate(String name) throws FileNotFoundException {
        InputStream inputStream = null;
        String file = tplFolder + File.separator + name + ".jrxml";
        String resource = "/rpt/" + name + ".jrxml";
        Path path = Paths.get(file);
        if(Files.exists(path)) {
            inputStream = new FileInputStream(path.toFile());
        } else {
            inputStream = this.getClass().getResourceAsStream(resource);
        }

        return inputStream;
    }

    private void compileTemplate(String name) throws IOException, JRException {
        File report = new File(tmpFolder + File.separator + name + ".jasper");
        if(report.exists()) return;
        InputStream template = openTemplate(name );
        OutputStream out = new FileOutputStream(report);
        JasperCompileManager.compileReportToStream(template, out);
        template.close();
        out.close();
    }

    private void compile() throws IOException, JRException {
        File report = new File(tmpFolder + File.separator + name + ".jasper");

        compileTemplate(name);

        Visitor visitor = new Visitor();
        JRElementsVisitor.visitReport((JasperReport) JRLoader.loadObject(report), visitor);

        for(JRSubreport subreport : visitor.getSubreports()) {
            String sReport = subreport.getExpression().getText().replace("\"", "");

            String sTemplate = sReport.replaceAll(".jasper", "");
            compileTemplate(sTemplate);
        }
    }

    private void fill() throws JRException {
        String report = tmpFolder + File.separator + name + ".jasper";
        print = JasperFillManager.fillReport(report, parameters, connection);
    }

    private class Visitor extends JRVisitorSupport {
        private List<JRSubreport> subreports = new ArrayList<>();

        @Override
        public void visitSubreport(JRSubreport subreport) {
            subreports.add(subreport);
        }

        public List<JRSubreport> getSubreports() {
            return this.subreports;
        }

    }

    public Report toPdf(FileOutputStream outputStream) {
        try {
            compile();
            fill();
            JasperExportManager.exportReportToPdfStream(print, outputStream);
        } catch (JRException e) {
            throw new ApplicationException(e);
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
        return this;
    }

}
