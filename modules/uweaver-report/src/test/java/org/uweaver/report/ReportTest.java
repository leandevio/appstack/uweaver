package org.uweaver.report;

import org.joda.time.DateTime;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.util.Environment;
import org.uweaver.data.JdbcDataSource;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jasonlin on 9/29/15.
 */
public class ReportTest {
    private Environment environment = Environment.getDefaultInstance();
    private JdbcDataSource ds = new JdbcDataSource();
    private String reportName = "RevenueReport";

    private Object[][] records = {{"Fresh Cut Flowers", "Sunflower", 24000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Cut Flowers", "Rose", 42000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Cut Flowers", "Irish", 12000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Cut Flowers", "Jasmine", 5600, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Bonsai", "Orchids", 30000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Bonsai", "Bamboo", 28000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Wreathes", "Spring Wedding", 28000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Wreathes", "Summer Wedding", 12000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Wreathes", "Fall Wedding", 4000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()},
            {"Fresh Wreathes", "Winter Wedding", 2000, new DateTime(2014, 4, 1, 0, 0, 0).toDate()}};
    private int[] type = {Types.VARCHAR, Types.VARCHAR, Types.DECIMAL, Types.DATE};

    @BeforeTest
    public void beforeTest() throws Exception {
        ds = new JdbcDataSource();

        ds.setDriver(environment.property("db.driver"));
        ds.setUrl(environment.property("db.url"));
        ds.setUsername(environment.property("db.username"));
        ds.setPassword(environment.property("db.password"));

        Connection conn = ds.getConnection();

        DatabaseMetaData dbms = conn.getMetaData();
        ResultSet tables = dbms.getTables(null, null, "RevenueByProduct".toUpperCase(), new String[]{"TABLE"});
        Statement stmt = conn.createStatement();
        if(tables.next()) {
            String sql = "drop table RevenueByProduct";
            stmt.execute(sql);
        }
        String sql = "create table RevenueByProduct(category varchar(100), name varchar(100), revenue decimal(10,2), dateOfSale date)";
        stmt.execute(sql);
        stmt.close();

        sql = "insert into RevenueByProduct(category, name, revenue, dateOfSale) values(?, ?, ?, ?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        for(int i=0; i<records.length; i++) {
            Object[] record = records[i];
            for(int j=0; j<record.length; j++) {
                pstmt.setObject(j+1, record[j], type[j]);
            }
            pstmt.execute();
        }
        conn.commit();
        stmt.close();
        conn.close();
    }

    @Test
    public void testToPdf() throws Exception {
        Connection conn = ds.getConnection();
        String filepath = Environment.getDefaultInstance().tmpFolder() + File.separator + reportName + ".pdf";

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("dateOfSale", new DateTime(2014, 4, 1, 0, 0, 0).toDate());

        Report report = new Report(reportName);
        report.setConnection(conn).setParameter(parameters);
        report.toPdf(new FileOutputStream(filepath));
        conn.close();
    }

    @AfterTest
    public void afterTest() throws Exception {
        ds.close();
    }

}