package org.uweaver.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.util.Environment;
import org.uweaver.server.handler.*;

import java.util.List;

/**
 * Created by jasonlin on 4/29/14.
 */
public class Server {

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);
    private Environment environment = Environment.getDefaultInstance();
    private static final int PORT = 9090;

    org.eclipse.jetty.server.Server server;
    private int port;

    public Server() {
        int port = environment.propertyAsInteger("server.port", PORT);
        initialize(port);
    }
    public Server(int port) {
        initialize(port);
    }

    private void initialize(int port) {
        // Create a basic Jetty server object that will listen on port 8080.  Note that if you set this to port 0
        // then a randomly available port will be assigned that you can either look in the logs for the port,
        // or programmatically obtain it for use in test cases.
        server = new org.eclipse.jetty.server.Server(port);

        this.port = port;
    }

    public void start() throws Exception {
        server.start();
        LOGGER.debug(server.dump());
    }

    public void stop() throws Exception {
        if(this.isStarted()) {
            server.stop();
        }
    }

    /**
     * By using the server.join() the server thread will join with the current thread.
     * @throws InterruptedException
     */
    public void join() throws InterruptedException {
        if(this.isStarted()) {
            server.join();
        }
    }

    public void destroy() throws Exception {
        if(this.isStopped()) {
            server.destroy();
        }
    }

    public boolean isStarted() {
        return server.isStarted();
    }

    public boolean isStopped() {
        return server.isStopped();
    }

    public int getPort() {
        return port;
    }

    public void setHandler(HandlerList handler) {
        server.setHandler(handler.toJettyHandlerList());
        handler.setServer(this);
    }

    public State getState() {
        if(server.isStarting()) {
            return State.STARTING;
        } else if(server.isStarted()) {
            return State.RUNNING;
        } else if(server.isStopping()) {
            return State.STOPPING;
        } else if(server.isStopped()) {
            return State.STOPPED;
        } else {
            return State.STOPPED;
        }
    }

    public enum State {
        STARTING("STARTING"), RUNNING("RUNNING"), SUSPEND("SUSPEND"), STOPPING("STOPPING"), STOPPED("STOPPED");

        private final String name;

        private State(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }
    }
}
