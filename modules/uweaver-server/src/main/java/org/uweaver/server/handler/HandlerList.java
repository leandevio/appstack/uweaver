/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.server.handler;

import org.uweaver.server.Server;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * HandlerList obj = new HandlerList();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class HandlerList {
    private Handler[] handlers;

    public void setHandlers(Handler[] handlers) {
        this.handlers = handlers;
    }

    public Handler[] getHandlers() {
        return this.handlers;
    }

    public org.eclipse.jetty.server.Handler toJettyHandlerList() {
        org.eclipse.jetty.server.handler.HandlerList jettyHandlerList = new org.eclipse.jetty.server.handler.HandlerList();
        org.eclipse.jetty.server.Handler[] jettyHandlers = new org.eclipse.jetty.server.Handler[handlers.length];

        for(int i=0; i<handlers.length; i++) {
            jettyHandlers[i] = handlers[i].getJettyHandler();
        }

        jettyHandlerList.setHandlers(jettyHandlers);

        return jettyHandlerList;

    }

    public void setServer(Server server) {
        for(int i=0; i<handlers.length; i++) {
            handlers[i].setServer(server);
        }
    }
}
