package org.uweaver.server.handler;

import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.util.Environment;
import org.uweaver.server.Server;

import java.nio.file.Path;

/**
 * Created by jasonlin on 7/1/14.
 */
public class HtmlHandler implements Handler {
    private static Logger LOGGER = LoggerFactory.getLogger(HtmlHandler.class);
    private Environment environment = Environment.getDefaultInstance();

    private Server server;
    private ContextHandler jettyHandler;

    private String contextPath = environment.property("server.html.contextPath", "/");
    private Path resourceBase = environment.propertyAsPath("server.html.resourceBase", ".");;
    private String[] welcomeFiles = environment.property("server.html.welcome", "index.html").split(",");

    public HtmlHandler(String contextPath, Path resourceBase, String[] welcomeFiles) {
        this.contextPath = contextPath;
        this.resourceBase = resourceBase;
        this.welcomeFiles = welcomeFiles;
        init();
    }

    public HtmlHandler(String contextPath, Path resourceBase) {
        this.contextPath = contextPath;
        this.resourceBase = resourceBase;
        init();
    }

    public HtmlHandler() {
        init();
    }


    private void init() {
        jettyHandler = new ContextHandler();

        jettyHandler.setContextPath(contextPath);

        // Create the ResourceHandler. It is the object that will actually handle the request for a given file. It is
        // a Jetty Handler object so it is suitable for chaining with other handlers as you will see in other examples.
        ResourceHandler resourceHandler = new ResourceHandler();
        // Configure the ResourceHandler. Setting the resource base indicates where the files should be served out of.
        // In this example it is the current directory but it can be configured to anything that the jvm has access to.
        resourceHandler.setDirectoriesListed(true);
        resourceHandler.setWelcomeFiles(welcomeFiles);
        resourceHandler.setResourceBase(resourceBase.toString());
        resourceHandler.setCacheControl("max-age=0,public");

        jettyHandler.setHandler(resourceHandler);
    }

    @Override
    public void setServer(Server server) {
        this.server = server;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public org.eclipse.jetty.server.Handler getJettyHandler() {
        return jettyHandler;
    }


    public String getContextPath() {
        return contextPath;
    }


    public Path getResourceBase() {
        return resourceBase;
    }
}

