package org.uweaver.server.handler;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.json.JSONFeature;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.Environment;
import org.uweaver.server.Server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;


/**
 * Created by jasonlin on 6/16/14.
 */
public class RestHandler implements Handler {
    private static Logger LOGGER = LoggerFactory.getLogger(RestHandler.class);
    private Environment environment = Environment.getDefaultInstance();
    ServletContextHandler jettyHandler;
    Server server;
    ServiceServlet servlet;


    private String contextPath = environment.property("server.rest.contextPath", "/rest");
    private Path resourceBase = environment.propertyAsPath("server.rest.resourceBase" ,"./rest");
    private int delay = environment.propertyAsInteger("server.delay", 0);
    private int options = ServletContextHandler.SESSIONS;
    private JSONParser parser;
    private Converters converters;

    public RestHandler() {
        init();
    }

    public RestHandler(String contextPath, Path resourceBase, int delay, int options) {
        this.contextPath = contextPath;
        this.resourceBase = resourceBase;
        this.delay = delay;
        this.options = options;
        init();
    }

    private void init() {
        jettyHandler = new ServletContextHandler(options);
        jettyHandler.setContextPath(contextPath);
        servlet = new ServiceServlet();
        jettyHandler.addServlet(new ServletHolder(servlet), "/*");
        parser = new JSONParser();
        parser.enable(JSONFeature.PRETTY);
        converters = new Converters();

    }

    @Override
    public void setServer(Server server) {
        this.server = server;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public org.eclipse.jetty.server.Handler getJettyHandler() {
        return jettyHandler;
    }


    public String getContextPath() {
        return contextPath;
    }

    public Path getResourceBase() {
        return resourceBase;
    }

    private class ServiceServlet extends HttpServlet {

        @Override
        protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                IOException {

            try {
                if(delay>0) Thread.sleep(delay);
            } catch (InterruptedException ex) {
                LOGGER.warn("Exception:" + ex);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(ex);
            }

            String uri = request.getRequestURI();
            String method = request.getMethod().toUpperCase();

            LOGGER.debug("Request received ...");
            LOGGER.debug("URI: " + uri);
            LOGGER.debug("Method: " + method);
            LOGGER.debug("Content Type: " + request.getContentType());
            LOGGER.debug("Encoding: " + request.getCharacterEncoding());
            LOGGER.debug("Query String: " + request.getQueryString());

//            String content = null;
//            if(request.getInputStream().available()>0) {
//                byte[] bytes = new byte[request.getInputStream().available()];
//                request.getInputStream().read(bytes);
//                Map payload = parser.readValue(new String(bytes), Map.class);
//                content = parser.writeValueAsString(payload);
//                LOGGER.debug("Content: \n" + content);
//            } else {
//                LOGGER.debug("Content: " + content);
//            }

            String filename = resourceBase + File.separator + uri.replace(contextPath, "") + File.separator + method;

            File file = new File(filename);

            response.setContentType("application/json;charset=utf-8");

            FileReader inputStream = null;
            char[] bytes = new char[(int) file.length()];
            try {
                inputStream = new FileReader(file);
                inputStream.read(bytes);
                String sb = new String(bytes);
                Map<String, Object> map = parser.readValue(sb, Map.class);

                int status = HttpServletResponse.SC_OK;
                Map<String, Object> headers = new HashMap();
                String body = null;
                if(map.containsKey("status")) {
                    status =  converters.convert(map.get("status"), Integer.class);
                }
                if(map.containsKey("headers")) {
                    headers = (Map<String, Object>) map.get("headers");
                }
                if(map.containsKey("body")) {
                    body = parser.writeValueAsString(map.get("body"));
                }

                response.setStatus(status);

                for(Map.Entry<String, Object> header: headers.entrySet()) {
                    String key = header.getKey();
                    Object value = header.getValue();
                    if(value instanceof String) {
                        response.setHeader(key, (String) value);
                    } else if(value instanceof Integer) {
                        response.setIntHeader(key, (int) value);
                    } else if(value instanceof Date) {
                        response.setDateHeader(key, ((Date) value).getTime());
                    } else if(value!=null) {
                        response.setHeader(key, value.toString());
                    }
                }
                response.getWriter().print(body);


                LOGGER.debug("Response sending ...");
                LOGGER.debug("Content Type: " + response.getContentType());
                LOGGER.debug("Status Code: " + response.getStatus());
                LOGGER.debug("Response Body: \n" + body);
            } catch (Exception ex) {
                LOGGER.warn("Exception:", ex);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(ex);
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }
    }

}
