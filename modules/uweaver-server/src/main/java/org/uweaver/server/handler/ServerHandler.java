/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2015 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.server.handler;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.HostMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.OperationFailureException;
import org.uweaver.core.util.JsonParser;
import org.uweaver.server.Server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * AdminHandler obj = new AdminHandler();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class ServerHandler implements Handler {
    private static Logger LOGGER = LoggerFactory.getLogger(ServerHandler.class);

    ServletContextHandler jettyHandler;
    Server server;
    ServiceServlet servlet;

    private final String CONTEXTPATH = "/server";

    public ServerHandler() {
        init();
    }

    private void init() {
        jettyHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        jettyHandler.setContextPath(CONTEXTPATH);
        servlet = new ServiceServlet();
        jettyHandler.addServlet(new ServletHolder(servlet), "/*");
    }

    @Override
    public void setServer(Server server) {
        this.server = server;
        servlet.setServer(server);
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public org.eclipse.jetty.server.Handler getJettyHandler() {
        return jettyHandler;
    }


    public String getContextPath() {
        return jettyHandler.getContextPath();
    }


    public String getResourceBase() {
        return jettyHandler.getResourceBase();
    }

    private class ServiceServlet extends HttpServlet {
        private Server server;

        @Override
        protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                IOException {

            String uri = request.getRequestURI();
            String method = request.getMethod().toUpperCase();
            String action = uri.replace(request.getContextPath() + "/", "");

            LOGGER.debug("Request received ...");
            LOGGER.debug("URI: " + uri);
            LOGGER.debug("Method: " + method);
            LOGGER.debug("Content Type: " + request.getContentType());
            LOGGER.debug("Encoding: " + request.getCharacterEncoding());
            LOGGER.debug("Query String: " + request.getQueryString());

            try {
                if(action.equals("stop")) {
                    stop(response);
                } else if(action.equals("status")) {
                    status(response);
                }
            } catch (OperationFailureException ex) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(ex);
            }
        }

        protected void setServer(Server server) {
            this.server = server;
        }

        protected void stop(HttpServletResponse response) throws IOException {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(5000);
                        server.stop();
                        server.destroy();
                    } catch(Exception ex) {
                        LOGGER.warn("Failed to stop the server.", ex);
                    }
                }
            };
            try {
                thread.join();
                thread.start();
                response.setContentType("application/json;charset=utf-8");
                response.setStatus(HttpServletResponse.SC_OK);
                String responseText = new String();
                response.getWriter().print(responseText);
            } catch (InterruptedException ex) {
                throw new OperationFailureException(ex);
            }
        }

        protected void status(HttpServletResponse response) throws IOException {
            response.setContentType("application/json;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_OK);
            Map<String, Object> status = new HostMap<>();
            status.put("state", server.getState());
            status.put("port", server.getPort());
            String responseText = JsonParser.writeValueAsString(status);
            response.getWriter().print(responseText);
        }
    }
}
