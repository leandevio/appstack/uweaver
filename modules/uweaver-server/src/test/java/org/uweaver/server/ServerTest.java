package org.uweaver.server;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

/**
 * Created by jasonlin on 12/24/15.
 */
public class ServerTest {
    @Test
    public void testStartStop() throws Exception {

        Server server = new Server();
        server.start();
        assertTrue(server.isStarted());
        server.stop();
        assertTrue(server.isStopped());
        server.destroy();
    }
}