/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.i18n;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.XMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * I18n obj = new I18n();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class I18n {
    private static final Logger LOGGER = LoggerFactory.getLogger(I18n.class);
    private static final String path = I18n.class.getPackage().getName().replace(".", File.separator);
    private Locale locale;
    private static Map<String, XMap<String, String>> termResourceBundles = new HashMap<>();
    private static Map<String, XMap<String, String>> messageResourceBundles = new HashMap<>();

    public I18n() {
        this(null);
    }

    public I18n(Locale locale) {
        this.locale = (locale == null) ? Environment.getDefaultInstance().locale() : locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale locale() {
        return this.locale;
    }

    public String getTermsAsJSON(TermType type) {
        String data = "[]";
        String filename = "terms_" + type.getName().toLowerCase() + ".json";

        try (InputStream in = I18n.class.getClassLoader().getResourceAsStream(path + File.separator + filename);
             InputStreamReader reader = new InputStreamReader(in,  StandardCharsets.UTF_8)) {
            int c;
            StringBuilder sb = new StringBuilder();
            while((c=reader.read())!=-1) {
                sb.append((char) c);
            }
            data = sb.toString();
        } catch (IOException e) {
            LOGGER.warn("The term file - '" + path + File.separator + filename + "' is missing.");
        }

        return data;
    }

    private XMap<String, String> resourceBundle(String namespace, String lang) {
        Map<String, XMap<String, String>> resourceBundles;
        if(namespace.equals("message")) {
            resourceBundles = messageResourceBundles;
        } else {
            resourceBundles = termResourceBundles;
        }
        if(resourceBundles.containsKey(lang)) return resourceBundles.get(lang);

        ClassLoader loader = I18n.class.getClassLoader();
        String resource = String.format("nls/%s/%s.json", lang, namespace);
        if(loader.getResource(resource)==null) return null;

        XMap<String, String> dictionary = null;
        try (InputStream in = loader.getResourceAsStream(resource);
             InputStreamReader reader = new InputStreamReader(in,  StandardCharsets.UTF_8)) {
            JSONParser parser = new JSONParser();
            dictionary = parser.readValue(reader, XMap.class);
            resourceBundles.put(lang, dictionary);
        } catch (IOException e) {
            LOGGER.warn("The term resource - '" + resource + "' is missing.");
        }

        return dictionary;
    }

    public String translate(String text) {
        String value = translate(text, "term");
        if(value.equals(text)) {
            value = translate(text, "message");
        }
        return value;
    }

    public String translate(String text, String namespace) {
        String value = translate(text, namespace, locale.toLanguageTag());
        if(value==null) {
            value = translate(text, namespace, locale.getLanguage());
        }
        if(value==null) {
            value = translate(text, namespace, "en");
        }

        return value==null ? text : value;
    }

    private String translate(String text, String namespace, String lang) {
        if(text==null) return null;
        XMap<String, String> resourceBundle = this.resourceBundle(namespace, lang);
        return (resourceBundle==null) ? null : resourceBundle.get(text);
    }

    public enum TermType {
        CHARACTER("CHARACTER");

        private final String name;

        TermType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }
    }
}
