package org.uweaver.schedule;

import org.apache.commons.beanutils.BeanMap;

import java.time.Duration;
import java.util.Date;
import java.util.List;

public class Activity {
    private String id;
    private Status status = null;
    private Date createdOn;
    private Date updatedOn;
    private Attendee creator;
    private String originalEventId;
    private Date originalStartDate;
    private Date startDate;
    private Date endDate;
    private Recurrence recurrence;
    private Visibility visibility = Visibility.PROTECTED;
    private List<Reminder> reminders;
    private List<Attendee> attendees;
    private String title;
    private String location;
    private String content;
    private String color;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Attendee getCreator() {
        return creator;
    }

    public void setCreator(Attendee creator) {
        this.creator = creator;
    }

    public String getOriginalEventId() {
        return originalEventId;
    }

    public void setOriginalEventId(String originalEventId) {
        this.originalEventId = originalEventId;
    }

    public Date getOriginalStartDate() {
        return originalStartDate;
    }

    public void setOriginalStartDate(Date originalStartDate) {
        this.originalStartDate = originalStartDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Recurrence getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }

    public boolean isRecurrent() {
        return getRecurrence()!=null;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Activity clone() {
        BeanMap bean = new BeanMap(this);
        BeanMap clone = new BeanMap(new Activity());

        clone.putAllWriteable(bean);

        return (Activity) clone.getBean();
    }

    public Duration getDuration() {
        return Duration.between(this.startDate.toInstant(), this.endDate.toInstant());
    }

    enum Visibility {
        PROTECTED("PROTECTED"), PUBLIC("PUBLIC"), PRIVATE("PRIVATE");

        private String value;

        Visibility(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }

    enum Status {
        ONTIME("ONTIME"), CACELLED("CANCELLED"), DELAYED("DELAYED"), POSTPONED("POSTPONED");

        private String value;

        Status(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
