package org.uweaver.schedule;

public class Attendee {
    private String id;
    private String name;
    private String email;
    private boolean isOrganizer = false;
    private Attendee.Response response;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isOrganizer() {
        return isOrganizer;
    }

    public void setOrganizer(boolean organizer) {
        isOrganizer = organizer;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    enum Response {
        ACCEPTED("ACCEPTED"), DECLINED("DECLINED"), MAYBE("MAYBE");

        private String value;

        Response(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
