package org.uweaver.schedule;

import java.util.Date;

public class Recurrence {
    private Frequency frequency = null;
    private Date until = null;
    private int interval = 1;

    public Recurrence() {}

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public enum Frequency {
        DAILY("DAILY"), WEEKLY("WEEKLY"), MONTHLY("MONTHLY"), YEARLY("YEARLY");

        private String value;

        Frequency(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
