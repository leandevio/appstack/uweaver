package org.uweaver.schedule;

public class Reminder {
    private DELIVERY delivery;
    private int minutes;

    public Reminder.DELIVERY getDelivery() {
        return delivery;
    }

    public void setDelivery(Reminder.DELIVERY delivery) {
        this.delivery = delivery;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    enum DELIVERY {
        MESSAGE("MESSAGE"), EMAIL("EMAIL");

        private String value;

        DELIVERY(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
