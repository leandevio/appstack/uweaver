package org.uweaver.schedule;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class Schedule {
    private String id;
    private String name;
    private TimeZone timeZone = TimeZone.getDefault();
    private List<Activity> activities = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public boolean add(Activity activity) {
        return this.activities.add(activity);
    }

    public boolean remove(Activity activity) {
        return this.activities.remove(activity);
    }

    public List<Activity> getActivities() {
        return Collections.unmodifiableList(this.activities);
    }

    public List<Activity> expand(Date until) {
        LocalDateTime dateTime = until.toInstant().atZone(this.getTimeZone().toZoneId()).toLocalDateTime();
        return expand(dateTime);
    }

    public List<Activity> expand(LocalDateTime until) {
        List<Activity> expandedActivities = new ArrayList<>();

        for(Activity activity : activities) {
            if(activity.isRecurrent()) {
                expandedActivities.addAll(expand(activity, until));
            } else {
                expandedActivities.add(activity);
            }
        }
        return expandedActivities;
    }

    private List<Activity> expand(Activity activity, LocalDateTime until) {
        List<Activity> expandedActivities = new ArrayList<>();

        List<Activity> alreadyExpandedActivities = getExpandedScheduleEvent(activity.getId());
        Recurrence recurrence = activity.getRecurrence();

        LocalDateTime startDateTime = activity.getStartDate().toInstant().atZone(this.getTimeZone().toZoneId()).toLocalDateTime();
        LocalDateTime occurrence = nextOccurrence(recurrence, startDateTime);

        if(recurrence.getUntil()!=null) {
            LocalDateTime dateTime = recurrence.getUntil().toInstant().atZone(this.getTimeZone().toZoneId()).toLocalDateTime();
            until = (dateTime.isBefore(until)) ? dateTime : until;
        }

        while(occurrence.isBefore(until)) {
            boolean isAlreadyExpanded = false;
            for(Activity alreadyExpandedActivity : alreadyExpandedActivities) {
                if(occurrence.equals(alreadyExpandedActivity.getOriginalStartDate())) {
                    isAlreadyExpanded = true;
                    break;
                }
            }
            if(!isAlreadyExpanded) {
                Activity expandedActivity = activity.clone();
                Duration duration = expandedActivity.getDuration();
                Date originalStartDate = Date.from(occurrence.atZone(this.getTimeZone().toZoneId()).toInstant());
                Date originalEndDate = Date.from(occurrence.plus(duration).atZone(this.getTimeZone().toZoneId()).toInstant());
                expandedActivity.setOriginalEventId(activity.getId());
                expandedActivity.setOriginalStartDate(originalStartDate);
                expandedActivity.setStartDate(originalStartDate);
                expandedActivity.setEndDate(originalEndDate);
                expandedActivities.add(expandedActivity);
            }
            occurrence = nextOccurrence(recurrence, occurrence);
        }

        return expandedActivities;
    }


    private LocalDateTime nextOccurrence(Recurrence recurrence, LocalDateTime current) {
        LocalDateTime occurence = null;
        if(recurrence.getFrequency().equals(Recurrence.Frequency.DAILY)) {
            occurence = current.plusDays(recurrence.getInterval());
        } else if(recurrence.getFrequency().equals(Recurrence.Frequency.WEEKLY)) {
            occurence = current.plusWeeks(recurrence.getInterval());
        } else if(recurrence.getFrequency().equals(Recurrence.Frequency.MONTHLY)) {
            occurence = current.plusMonths(recurrence.getInterval());
        } else if(recurrence.getFrequency().equals(Recurrence.Frequency.YEARLY)) {
            occurence = current.plusYears(recurrence.getInterval());
        }
        return occurence;
    }

    private List<Activity> getExpandedScheduleEvent(String eventId) {
        List<Activity> expandedActivities = new ArrayList<>();
        for(Activity activity : activities) {
            if(activity.getOriginalEventId()!=null && activity.getOriginalEventId().equals(eventId)) {
                expandedActivities.add(activity);
            }
        }
        return expandedActivities;
    }
}
