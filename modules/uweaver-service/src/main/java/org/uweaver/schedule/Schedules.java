package org.uweaver.schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Schedules extends ArrayList<Schedule> {
    public List<Activity> getScheduleEvents() {
        List<Activity> activities = new ArrayList<>();
        Iterator<Schedule> iterator = this.iterator();

        while(iterator.hasNext()) {
            Schedule schedule = iterator.next();
            activities.addAll(schedule.getActivities());
        }

        return activities;
    }

    public List<Activity> expand(Date until) {
        List<Activity> activities = new ArrayList<>();
        Iterator<Schedule> iterator = this.iterator();

        while(iterator.hasNext()) {
            Schedule schedule = iterator.next();
            activities.addAll(schedule.expand(until));
        }

        return activities;
    }
}
