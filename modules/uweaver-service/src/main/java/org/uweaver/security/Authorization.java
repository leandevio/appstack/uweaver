/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.security;

import org.uweaver.data.annotation.Association;
import org.uweaver.data.annotation.Entity;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Role obj = new Role();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Entity(name="UW_Authorization", key = {"key"})
public class Authorization {
    private String key;
    private String name;
    private Map<String, Permission> permissions = new HashMap<>();
    private Set<String> tags = new HashSet<>();

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Association
    public void setPermissions(List<Permission> permissions) {
        this.permissions.clear();
        addPermissions(permissions);
    }

    public List<Permission> getPermissions() {
        List<Permission> permissions = new ArrayList<>();
        permissions.addAll(this.permissions.values());
        return permissions;
    }

    public void addPermission(Permission permission) {
        if(permission==null) return;
        permissions.put(permission.getKey(), permission);
    }

    public Permission getPermission(String key) {
        return permissions.get(key);
    }

    public void addPermissions(List<Permission> permissions) {
        for(Permission permission : permissions) {
            this.permissions.put(permission.getKey(), permission);
        }
    }

    public void removePermission(Permission permission) {
        this.permissions.remove(permission.getKey());
    }

    public void removePermissions(List<Permission> permissions) {
        for(Permission permission : permissions) {
            this.permissions.remove(permission.getKey());
        }
    }

    public Set<String> getTags() {
        Set<String> tags = new HashSet<>();
        tags.addAll(tags);
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags.clear();

        if(tags==null) return;

        for(String tag : tags) {
            this.tags.add(tag);
        }
    }

    public void addTag(String tag) {
        tags.add(tag);
    }

    public void removeTag(String tag) {
        tags.remove(tag);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Authorization)) return false;
        if(obj==null) return false;

        Authorization other = (Authorization) obj;

        return (this.hashCode()==other.hashCode());

    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(getKey(), getName());

        List<String> keys = new ArrayList<>();
        keys.addAll(permissions.keySet());
        Collections.sort(keys);
        for(String key : keys) {
            hash = Objects.hash(hash, permissions.get(key).hashCode());
        }
        for(String tag: tags) {
            hash = Objects.hash(hash, tag.hashCode());
        }

        return hash;
    }

    @Override
    public String toString() {
        return getName();
    }
}
