/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.security;

import org.uweaver.core.cipher.Digester;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.data.*;
import org.uweaver.data.DataCollection;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Authorizer obj = new Authorizer();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Authorizer {
    private static final String PASS_RESOURCE = "/data/Pass.json";
    private JSONParser parser = new JSONParser();
    private Environment environment = Environment.getDefaultInstance();
    private static String KEY_ALL = "ALL", KEY_ANONYMOUS = "ANONYMOUS";
    private DataRepository dataRepository;
    private DataWriter writer;
    private AuthenticationHandler authenticationHandler;
    private Digester digester = new Digester();

    protected Authorizer() {
        this(JdbcDataSource.getDefaultInstance());
    }

    protected Authorizer(DataSource dataSource) {
        this.dataRepository = new DataRepository(dataSource);
        this.writer = new DataWriter(dataRepository);
        initialize();
    }

    public boolean verify(String plain, String hash) {
        return digester.verify(plain, hash);
    }

    private static class SingletonHolder {
        private static final Authorizer INSTANCE = new Authorizer();
    }

    public static Authorizer getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void reset() {
        clear(Privilege.class);
        clear(Authorization.class);
        clear(Pass.class);
        clear(Permission.class);
        initialize();
    }

    public void registerHandler(AuthenticationHandler authenticationHandler) {
        if(authenticationHandler==null) return;
        this.authenticationHandler = authenticationHandler;
    }

    public void registerHandler(Class<? extends AuthenticationHandler> authenticationHandler) {
        if(authenticationHandler==null) return;
        try {
            this.authenticationHandler = authenticationHandler.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
    }

    public void registerPasses(List<Pass> passes) {
        if(passes==null) return;
        DataCollection dataCollection = dataRepository.collection(Pass.class);
        writer.open(dataCollection);
        writer.write(passes);
        writer.close();
    }
    
    public void registerPass(Pass pass) {
        if(pass==null) return;
        DataCollection dataCollection = dataRepository.collection(Pass.class);
        writer.open(dataCollection);
        writer.write(pass);
        writer.close();
    }

    public void unregisterPass(Pass pass) {
        if(pass==null) return;
        revokeAll(pass);
        DataCollection dataCollection = dataRepository.collection(Pass.class);
        writer.open(dataCollection);
        writer.remove(pass);
        writer.close();
    }

    public void unregisterPass(String key) {
        unregisterPass(getPass(key));
    }


    public void registerPermissions(List<Permission> permissions) {
        if(permissions==null) return;
        DataCollection dataCollection = dataRepository.collection(Permission.class);
        writer.open(dataCollection);
        writer.write(permissions);
        writer.close();
    }


    public void registerPermission(Permission permission) {
        if(permission==null) return;
        DataCollection dataCollection = dataRepository.collection(Permission.class);
        writer.open(dataCollection);
        writer.write(permission);
        writer.close();
    }

    public void unregisterPermission(Permission permission) {
        if(permission==null) return;
        DataCollection dataCollection = dataRepository.collection(Permission.class);
        writer.open(dataCollection);
        writer.remove(permission);
        writer.close();
    }

    public void unregisterPermission(String key) {
        unregisterPermission(getPermission(key));
    }

    public void registerAuthorizations(List<Authorization> authorizations) {
        if(authorizations==null) return;
        DataCollection dataCollection = dataRepository.collection(Authorization.class);
        writer.open(dataCollection);
        writer.write(authorizations);
        writer.close();
    }

    public void registerAuthorization(Authorization authorization) {
        if(authorization==null) return;
        DataCollection dataCollection = dataRepository.collection(Authorization.class);
        writer.open(dataCollection);
        writer.write(authorization);
        writer.close();
    }

    public void unregisterAuthorization(Authorization authorization) {
        if(authorization==null) return;
        if(authorization.getKey().equals(KEY_ALL)||authorization.getKey().equals(KEY_ANONYMOUS)) return;
        revokeAll(authorization);
        DataCollection dataCollection = dataRepository.collection(Authorization.class);
        writer.open(dataCollection);
        writer.remove(authorization);
        writer.close();
    }

    public void unregisterAuthorization(String key) {
        unregisterAuthorization(getAuthorization(key));
    }

    public Pass getPass(String key) {
        return createPassQuery().key(key).singleResult();
    }

    public List<Pass> passes() {
        return createPassQuery().list();
    }

    public PassQuery createPassQuery() {
        return new PassQuery(dataRepository);
    }

    public Permission getPermission(String key) {
        return createPermissionQuery().key(key).singleResult();
    }

    public List<Permission> getPermissions(Pass pass) {
        Set<Permission> set = new HashSet<>();
        for(Authorization authorization : getAuthorizations(pass)) {
            set.addAll(authorization.getPermissions());
        }
        List<Permission> permissions = new ArrayList<>();
        permissions.addAll(set);
        return permissions;
    }

    public PermissionQuery createPermissionQuery() {
        return new PermissionQuery(dataRepository);
    }

    public Authorization getAuthorization(String key) {
        return createAuthorizationQuery().key(key).singleResult();
    }

    public List<Authorization> getAuthorizations(Pass pass) {
        List<Privilege> privileges = createPrivilegeQuery().pass(pass).list();
        List<Authorization> authorizations = new ArrayList<>();
        for(Privilege privilege : privileges) {
            authorizations.add(privilege.getAuthorization());
        }
        return authorizations;
    }

    public List<Authorization> getAuthorizationsByPass(Pass pass) {
        List<Privilege> privileges = createPrivilegeQuery().pass(pass).list();
        List<Authorization> authorizations = new ArrayList<>();
        for(Privilege privilege : privileges) {
            authorizations.add(privilege.getAuthorization());
        }
        return authorizations;
    }

    public Authorization getAnonymousAuthorization() {
        return getAuthorization(KEY_ANONYMOUS);
    }

    public Authorization getAllAuthorization() {
        return getAuthorization(KEY_ALL);
    }

    public AuthorizationQuery createAuthorizationQuery() {
        return new AuthorizationQuery(dataRepository);
    }

    public PrivilegeQuery createPrivilegeQuery() {
        return new PrivilegeQuery(dataRepository);
    }

    public Pass signin(String username, String password) {
        String key = authenticationHandler.signin(username, password);
        if(key==null) return null;
        Pass pass = getPass(key);
        if(pass==null) {
            pass = new Pass();
            pass.setKey(key);
            pass.setUsername(key);
            registerPass(pass);
        }
        return pass;
    }

    public void signout(Pass pass) {
        authenticationHandler.signout(pass);
    }

    public boolean permitAll(Pass pass) {
        return createPrivilegeQuery().pass(pass).authorization(getAuthorization(KEY_ALL)).list().size()>0 ? true : false;
    }

    public boolean permit(Pass pass, List<Permission> permissions) {
        if(pass==null) throw new NullPointerException("The pass can not be null.");
        if(permissions==null||permissions.size()==0) return true;

        if(permitAll(pass)) return true;

        List<Privilege> privileges = createPrivilegeQuery().pass(pass).list();
        Set<Permission> rights = new HashSet<>();
        for(Privilege privilege : privileges) {
            privilege.getAuthorization().getPermissions();
            rights.addAll(privilege.getAuthorization().getPermissions());
        }

        Set<String> expect = new HashSet<>();
        Set<String> actual = new HashSet<>();
        for(Permission permission : permissions) {
            expect.add(permission.getKey());
        }
        for(Permission permission : rights) {
            actual.add(permission.getKey());
        }

        return actual.containsAll(expect);
    }

    public boolean permit(Pass pass, Permission permission) {
        if(pass==null) throw new NullPointerException("The pass can not be null.");
        if(permission==null) return true;

        List<Permission> permissions = new ArrayList<>();
        permissions.add(permission);
        return permit(pass, permissions);
    }

    public boolean permit(Pass pass, Authorization authorization) {
        return permit(pass, authorization.getPermissions());
    }

    /**
     * Validate against anonymous permissions.
     * @param permissions
     * @return
     */
    public boolean permit(List<Permission> permissions) {
        if(permissions==null||permissions.size()==0) return true;

        Set<String> expect = new HashSet<>();
        Set<String> actual = new HashSet<>();
        for(Permission permission : permissions) {
            expect.add(permission.getKey());
        }
        for(Permission permission : getAnonymousAuthorization().getPermissions()) {
            actual.add(permission.getKey());
        }

        return actual.containsAll(expect);
    }

    /**
     * Validate against anonymous permissions.
     * @param permission
     * @return
     */
    public boolean permit(Permission permission) {
        if(permission==null) return true;
        List<Permission> permissions = new ArrayList<>();
        permissions.add(permission);
        return permit(permissions);
    }


    public void grant(Pass pass, Authorization authorization) {
        List<Authorization> authorizations = new ArrayList<>();
        authorizations.add(authorization);
        grant(pass, authorizations);
    }

    public void grant(Pass pass, List<Authorization> authorizations) {
        List<Authorization> rights = getAuthorizationsByPass(pass);
        List<String> index = new ArrayList<>();
        for(Authorization right : rights) {
            index.add(right.getKey());
        }

        DataCollection dataCollection = dataRepository.collection(Privilege.class);
        List<Privilege> privileges = new ArrayList<>();
        for(Authorization authorization : authorizations) {
            if(index.contains(authorization.getKey())) continue;
            Privilege privilege = new Privilege();
            privilege.setPass(pass);
            privilege.setAuthorization(authorization);
            privileges.add(privilege);
        }
        writer.open(dataCollection);
        writer.write(privileges);
        writer.close();
    }

    public void grantAll(Pass pass) {
        grant(pass, getAuthorization(KEY_ALL));
    }

    public void revoke(Pass pass, Authorization authorization) {
        List<Authorization> authorizations = new ArrayList<>();
        authorizations.add(authorization);
        revoke(pass, authorizations);
    }

    public void revoke(Pass pass, List<Authorization> authorizations) {
        List<Privilege> privileges = createPrivilegeQuery().pass(pass).list();
        List<Privilege> tobeRemoved = new ArrayList<>();
        for(Privilege privilege : privileges) {
            if(authorizations.contains(privilege.getAuthorization())) tobeRemoved.add(privilege);
        }

        DataCollection dataCollection = dataRepository.collection(Privilege.class);
        writer.open(dataCollection);
        writer.remove(tobeRemoved);
        writer.close();
    }

    public void revokeAll(Pass pass) {
        List<Privilege> privileges = createPrivilegeQuery().pass(pass).list();
        DataCollection dataCollection = dataRepository.collection(Privilege.class);
        writer.open(dataCollection);
        writer.remove(privileges);
        writer.close();
    }

    public void revokeAll(Authorization authorization) {
        List<Privilege> privileges = createPrivilegeQuery().authorization(authorization).list();
        DataCollection dataCollection = dataRepository.collection(Privilege.class);
        writer.open(dataCollection);
        writer.remove(privileges);
        writer.close();
    }

    private void clear(Class type) {
        dataRepository.dropCollection(type);
    }

    private void initialize() {
        String reinitialize = environment.property("data.initialize", "");
        if(reinitialize.contains("all")||reinitialize.contains(Privilege.class.getSimpleName().toLowerCase())) clear(Privilege.class);
        if(reinitialize.contains("all")||reinitialize.contains(Authorization.class.getSimpleName().toLowerCase())) clear(Authorization.class);
        if(reinitialize.contains("all")||reinitialize.contains(Pass.class.getSimpleName().toLowerCase())) clear(Pass.class);
        if(reinitialize.contains("all")||reinitialize.contains(Permission.class.getSimpleName().toLowerCase())) clear(Permission.class);
        authenticationHandler = new DefaultAuthenticationHandler();
        if(!dataRepository.hasCollection(Pass.class)) dataRepository.createCollection(new DataCollection(Pass.class));
        if(!dataRepository.hasCollection(Authorization.class)) dataRepository.createCollection(new DataCollection(Authorization.class));
        if(!dataRepository.hasCollection(Permission.class)) dataRepository.createCollection(new DataCollection(Permission.class));
        if(!dataRepository.hasCollection(Privilege.class)) dataRepository.createCollection(new DataCollection(Privilege.class));
        Authorization ALL = getAuthorization(KEY_ALL);
        if(ALL==null) {
            ALL = new Authorization();
            ALL.setKey(KEY_ALL);
            ALL.setName(KEY_ALL);
            registerAuthorization(ALL);
        }
        Authorization ANONYMOUS = getAuthorization(KEY_ANONYMOUS);
        if(ANONYMOUS==null) {
            ANONYMOUS = new Authorization();
            ANONYMOUS.setKey(KEY_ANONYMOUS);
            ANONYMOUS.setName(KEY_ANONYMOUS);
            registerAuthorization(ANONYMOUS);
        }

        if(passes().size()==0) registerPasses(readPass());
    }

    private List<Pass> readPass() {
        if(Authorizer.class.getClassLoader().getResource(PASS_RESOURCE)==null) {
            return new ArrayList<>();
        }
        InputStream in = Authorizer.class.getClassLoader()
                .getResourceAsStream(PASS_RESOURCE);

        List<Pass> passes = Arrays.asList(parser.readValue(in, Pass[].class));

        for(Pass pass : passes) {
            pass.setPassword(digester.digest(pass.password()));
        }
        return passes;
    }

}
