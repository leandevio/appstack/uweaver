/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.security;

import org.uweaver.data.annotation.Entity;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * User obj = new User();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Entity(name="UW_Pass", key = {"key"})
public class Pass {
    private String key;
    private String username;
    private String password;
    private String text;
    private String email;
    private String mobile;
    private String description;
    private Date enableDate;
    private Date expiryDate;
    private Map<String, Object> properties = new HashMap<>();

    public Pass() {}

    public String key() {
        return key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String username() {
        return username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String password() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String text() {
        return text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String email() {
        return email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String mobile() {
        return mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String description() {
        return description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date enableDate() {
        return enableDate;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public Date expiryDate() {
        return expiryDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setProperty(String name, Object value) {
        properties.put(name, value);
    }

    public Object getProperty(String name) {
        return properties.get(name);
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties.clear();
        if(properties==null) return;

        for(Map.Entry<String, Object> property : properties.entrySet()) {
            this.properties.put(property.getKey(), property.getValue());
        }
    }

    public Map<String, Object> getProperties() {
        return Collections.unmodifiableMap(this.properties);
    }

    protected Map<String, Object> toMap() {
        Map<String, Object> data = new HashMap<>();
        data.put("Key", getKey());
        data.put("Username", getUsername());
        data.put("Password", getPassword());
        data.put("Text", getText());
        data.put("Email", getEmail());
        data.put("Properties", getProperties());
        return data;
    }


    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Pass)) return false;
        if(obj==null) return false;

        Pass other = (Pass) obj;

        return (this.hashCode()==other.hashCode());

    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(getKey(), getUsername(), getPassword(), getEmail());

        for(Map.Entry<String, Object> property: properties.entrySet()) {
            hash = Objects.hash(hash, property.hashCode());
        }

        return hash;
    }
}
