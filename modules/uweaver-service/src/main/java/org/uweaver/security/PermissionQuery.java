/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.security;

import org.uweaver.data.DataCollection;
import org.uweaver.data.DataQuery;
import org.uweaver.data.DataRepository;
import org.uweaver.data.Field;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PermissionQuery obj = new PermissionQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PermissionQuery {
    private DataQuery dataQuery;
    private DataCollection dataCollection;
    private Map<String, Object> predicate = new HashMap<>();

    public PermissionQuery(DataRepository dataRepository) {
        dataQuery = new DataQuery(dataRepository);
        dataCollection = dataRepository.collection(Permission.class);
        dataQuery.from(dataCollection);
    }

    public PermissionQuery key(String key) {
        Field field = dataCollection.getField("Key");
        predicate.put(field.getName(), key);
        return this;
    }

    public PermissionQuery uri(String uri) {
        Field field = dataCollection.getField("Uri");
        predicate.put(field.getName(), uri);
        return this;
    }

    public PermissionQuery method(String method) {
        Field field = dataCollection.getField("Method");
        predicate.put(field.getName(), method);
        return this;
    }

    public List<Permission> list() {
        return dataQuery.where(predicate).list(Permission.class);
    }

    public Permission singleResult() {
        return dataQuery.where(predicate).singleResult(Permission.class);
    }

}
