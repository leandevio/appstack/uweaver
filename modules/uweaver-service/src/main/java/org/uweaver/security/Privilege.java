/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.security;

import org.uweaver.data.annotation.Association;
import org.uweaver.data.annotation.Entity;
import org.uweaver.data.annotation.Generated;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Privilege obj = new Privilege();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@Entity(name="UW_Privilege", key = {"id"})
public class Privilege {
    private String id;
    private Pass pass = null;
    private Authorization authorization = null;

    public String getId() {
        return id;
    }

    @Generated
    public void setId(String id) {
        this.id = id;
    }

    public Pass getPass() {
        return pass;
    }

    @Association
    public void setPass(Pass pass) {
        this.pass = pass;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    @Association
    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }
}
