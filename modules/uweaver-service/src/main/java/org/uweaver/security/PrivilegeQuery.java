/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.security;

import org.uweaver.core.json.JSONParser;
import org.uweaver.data.DataCollection;
import org.uweaver.data.DataQuery;
import org.uweaver.data.DataRepository;
import org.uweaver.data.Field;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PrivilegeQuery obj = new PrivilegeQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PrivilegeQuery {
    private DataQuery dataQuery;
    private DataCollection dataCollection;
    private Map<String, Object> predicate = new HashMap<>();
    private JSONParser parser = new JSONParser();

    public PrivilegeQuery(DataRepository dataRepository) {
        dataQuery = new DataQuery(dataRepository);
        dataCollection = dataRepository.collection(Privilege.class);
        dataQuery.from(dataCollection);
    }

    public PrivilegeQuery pass(Pass pass) {
        Field field = dataCollection.getField("Pass");
        Map<String, Object> values = new HashMap<>();
        values.put("key", pass.getKey());
        predicate.put(field.getName(), parser.writeValueAsString(values));
        return this;
    }

    public PrivilegeQuery authorization(Authorization authorization) {
        Field field = dataCollection.getField("Authorization");
        Map<String, Object> values = new HashMap<>();
        values.put("key", authorization.getKey());
        predicate.put(field.getName(), parser.writeValueAsString(values));
        return this;
    }

    public List<Privilege> list() {
        return dataQuery.where(predicate).list(Privilege.class);
    }

}
