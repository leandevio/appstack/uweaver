/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service.app;

import org.uweaver.data.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AppFolderQuery obj = new AppFolderQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class AppFolderQuery {
    private DataQuery dataQuery;
    private DataCollection dataCollection;
    private Map<String, Object> predicate = new HashMap<>();

    public AppFolderQuery(DataRepository dataRepository) {
        dataQuery = new DataQuery(dataRepository);
        dataCollection = dataRepository.collection(AppFolder.class);
        dataQuery.from(dataCollection);
    }

    public AppFolderQuery uri(String uri) {
        Field field = dataCollection.getField("Uri");
        predicate.put(field.getName(), uri);
        return this;
    }

    public List<AppFolder> list() {
        return dataQuery.where(predicate).sort(Order.desc("Priority")).list(AppFolder.class);
    }

    public AppFolder singleResult() {
        return dataQuery.where(predicate).singleResult(AppFolder.class);
    }
}
