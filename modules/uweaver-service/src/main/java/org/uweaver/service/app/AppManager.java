/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service.app;

import org.uweaver.data.DataInstaller;
import org.uweaver.data.DataCollection;
import org.uweaver.data.DataRepository;
import org.uweaver.data.DataWriter;
import org.uweaver.data.JdbcDataSource;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ApplicationManager obj = new ApplicationManager();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class AppManager {
    private DataInstaller dataInstaller;
    private DataRepository dataRepository;
    private DataWriter writer;

    public List<Applet> applets() {
        return createAppletQuery().list();
    }

    public List<AppFolder> appFolders() {
        return createAppFolderQuery().list();
    }

    public AppletQuery createAppletQuery() {
        return new AppletQuery(dataRepository);
    }

    public AppFolderQuery createAppFolderQuery() {
        return new AppFolderQuery(dataRepository);
    }

    public Applet applet(String uri) {
        return createAppletQuery().uri(uri).singleResult();
    }

    public AppFolder appFolder(String uri) {
        return createAppFolderQuery().uri(uri).singleResult();
    }

    public AppManager() {
        this(JdbcDataSource.getDefaultInstance());
    }

    public AppManager(DataSource dataSource) {
        this.dataRepository = new DataRepository(dataSource);
        this.writer = new DataWriter(dataRepository);
        this.dataInstaller = new DataInstaller(dataSource);
        validate();
    }

    public void reset() {
        clear();
        validate();
    }

    public void registerApplets(Collection<Applet> applets) {
        if(applets==null) return;
        DataCollection dataCollection = dataRepository.collection(Applet.class);
        writer.open(dataCollection);
        writer.write(applets);
        writer.close();
    }

    public void registerApplet(Applet applet) {
        if(applet==null) return;
        DataCollection dataCollection = dataRepository.collection(Applet.class);
        writer.open(dataCollection);
        writer.write(applet);
        writer.close();
    }

    public void unregisterApplet(Applet applet) {
        if(applet==null) return;
        DataCollection dataCollection = dataRepository.collection(Applet.class);
        writer.open(dataCollection);
        writer.remove(applet);
        writer.close();
    }

    public void unregisterApplet(String uri) {
        unregisterApplet(applet(uri));
    }

    public void registerAppFolders(Collection<AppFolder> appFolders) {
        if(appFolders==null) return;
        DataCollection dataCollection = dataRepository.collection(AppFolder.class);
        writer.open(dataCollection);
        writer.write(appFolders);
        writer.close();
    }

    public void registerAppFolder(AppFolder appFolder) {
        if(appFolder==null) return;
        DataCollection dataCollection = dataRepository.collection(AppFolder.class);
        writer.open(dataCollection);
        writer.write(appFolder);
        writer.close();
    }

    public void unregisterAppFolder(AppFolder appFolder) {
        if(appFolder==null) return;
        DataCollection dataCollection = dataRepository.collection(AppFolder.class);
        writer.open(dataCollection);
        writer.remove(appFolder);
        writer.close();
    }

    public void unregisterAppFolder(String uri) {
        unregisterAppFolder(appFolder(uri));
    }

    private void clear() {
        dataRepository.dropCollection(Applet.class);
        dataRepository.dropCollection(AppFolder.class);
    }

    private void validate() {
        dataInstaller.validate(Applet.class);
        if(applets().size()==0) {
            registerApplets(Arrays.asList(dataInstaller.read(Applet[].class)));
        }
        dataInstaller.validate(AppFolder.class);
        if(appFolders().size()==0) {
            registerAppFolders(Arrays.asList(dataInstaller.read(AppFolder[].class)));
        }
    }
}
