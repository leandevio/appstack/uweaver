/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.service.app;

import org.uweaver.data.DataInstaller;
import org.uweaver.core.util.Environment;
import org.uweaver.data.DataCollection;
import org.uweaver.data.DataRepository;
import org.uweaver.data.DataWriter;
import org.uweaver.data.JdbcDataSource;

import javax.sql.DataSource;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SiteManager obj = new SiteManager();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SiteManager {
    private DataInstaller dataInstaller;
    private DataRepository dataRepository;
    private DataWriter writer;
    private String appName = Environment.getDefaultInstance().appName();

    public SiteManager(DataSource dataSource) {
        this.dataRepository = new DataRepository(dataSource);
        this.writer = new DataWriter(dataRepository);
        this.dataInstaller = new DataInstaller(dataSource);
        validate();
    }


    public SiteManager() {
        this(JdbcDataSource.getDefaultInstance());
    }

    public Site site() {
        return new SiteQuery(dataRepository).singleResult();
    }

    public void reset() {
        clear();
        validate();
    }

    public void save(Site site) {
        if(site==null) return;
        // to patch SQL Server exception: 不允許從資料類型 varchar 隱含轉換到 varbinary(max)。請使用 CONVERT 函數來執行查詢。
        if(site.logo()==null && dataRepository.getProvider().equals(DataRepository.MSSQL)) {
            site.setLogo(new byte[0]);
        }
        DataCollection dataCollection = dataRepository.collection(Site.class);
        writer.open(dataCollection);
        writer.write(site);
        writer.close();
    }

    private void clear() {
        dataRepository.dropCollection(Site.class);
    }

    private void validate() {
        dataInstaller.validate(Site.class);
        if(site()==null) {
            Site site = dataInstaller.read(Site.class);
            if(site==null) {
                site = new Site();
                site.setUri(this.appName);
            }
            save(site);
        }
    }
}
