/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.user;

import org.uweaver.data.annotation.Entity;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Preference obj = new Preference();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

@Entity(name="UW_Preference", key = {"key"})
public class Preference {
    private String key;
    private Map<String, Object> properties = new HashMap<>();

    public Preference() {}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setProperty(String name, Object value) {
        properties.put(name, value);
    }

    public Object getProperty(String name) {
        return properties.get(name);
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties.clear();
        if(properties==null) return;

        for(Map.Entry<String, Object> property : properties.entrySet()) {
            this.properties.put(property.getKey(), property.getValue());
        }
    }

    public Map<String, Object> getProperties() {
        return Collections.unmodifiableMap(this.properties);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Preference)) return false;
        if(obj==null) return false;

        Preference other = (Preference) obj;

        return (this.hashCode()==other.hashCode());

    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(getKey());

        for(Map.Entry<String, Object> property: properties.entrySet()) {
            hash = Objects.hash(hash, property.hashCode());
        }

        return hash;
    }
}
