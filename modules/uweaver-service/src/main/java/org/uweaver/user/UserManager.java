/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.user;

import org.uweaver.data.DataInstaller;
import org.uweaver.core.util.Environment;
import org.uweaver.data.DataCollection;
import org.uweaver.data.DataRepository;
import org.uweaver.data.DataWriter;
import org.uweaver.data.JdbcDataSource;
import org.uweaver.security.Authorizer;

import javax.sql.DataSource;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * UserManager obj = new UserManager();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class UserManager {
    private static String KEY_DEFAULT = "DEFAULT";
    private DataInstaller dataInstaller;
    private DataRepository dataRepository;
    private DataWriter writer;

    private static class SingletonHolder {
        private static final UserManager INSTANCE = new UserManager();
    }

    public static UserManager getDefaultInstance() {
        return UserManager.SingletonHolder.INSTANCE;
    }

    public UserManager() {
        this(JdbcDataSource.getDefaultInstance());
    }

    public UserManager(DataSource dataSource) {
        this.dataRepository = new DataRepository(dataSource);
        this.writer = new DataWriter(dataRepository);
        this.dataInstaller = new DataInstaller(dataSource);
        this.validate();
    }


    public void reset() {
        clear();
        validate();
    }

    public void savePreference(List<Preference> preferences) {
        if(preferences==null) return;
        DataCollection dataCollection = dataRepository.collection(Preference.class);
        writer.open(dataCollection);
        writer.write(preferences);
        writer.close();
    }

    public void savePreference(Preference preference) {
        if(preference==null) return;
        DataCollection dataCollection = dataRepository.collection(Preference.class);
        writer.open(dataCollection);
        writer.write(preference);
        writer.close();
    }

    public void removePreference(Preference preference) {
        if(preference==null) return;
        DataCollection dataCollection = dataRepository.collection(Preference.class);
        writer.open(dataCollection);
        writer.remove(preference);
        writer.close();
    }

    public void removePreference(String key) {
        removePreference(getPreference(key));
    }

    public Preference preference(String key) {
        return createPreferenceQuery().key(key).singleResult();
    }

    public Preference getPreference(String key) {
        return createPreferenceQuery().key(key).singleResult();
    }

    public PreferenceQuery createPreferenceQuery() {
        return new PreferenceQuery(dataRepository);
    }

    private void clear() {
        dataRepository.dropCollection(Preference.class);
    }

    private void validate() {
        dataInstaller.validate(Preference.class);
        if(createPreferenceQuery().list().size()==0) {
            savePreference(Arrays.asList(dataInstaller.read(Preference[].class)));
        }

    }
}
