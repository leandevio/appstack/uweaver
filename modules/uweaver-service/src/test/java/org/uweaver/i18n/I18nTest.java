package org.uweaver.i18n;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.doc.Sheet;
import org.uweaver.core.doc.SheetCell;
import org.uweaver.core.doc.SheetRow;
import org.uweaver.core.doc.XlsWorkbookReader;
import org.uweaver.core.json.JSONParser;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 10/28/16.
 */
public class I18nTest {
    private I18n i18n;
    private static final String path = I18n.class.getPackage().getName().replace(".", File.separator);

    @BeforeTest
    public void beforeTest() throws Exception {
        i18n = new I18n();
    }

    @Test
    public void testGetTermsAsJSON() throws Exception {
        String terms = i18n.getTermsAsJSON(I18n.TermType.CHARACTER);
        List<String> index = new ArrayList<>();
        List<Map<String, String>> dict = new ArrayList<>();
        InputStream in = I18n.class.getClassLoader().getResourceAsStream(path + File.separator + "terms_TWCN.xls");
        XlsWorkbookReader reader = new XlsWorkbookReader(in);
        for(int i=0; i<3; i++) {
            Sheet sheet = reader.getSheet(i);
            for(SheetRow row : sheet.getRows()) {
                int size = row.getCells().size();
                for(int j=0; j<size/2; j++) {
                    Map<String, String> mapping = new HashMap<>();
                    SheetCell tw = row.getCell(j*2);
                    SheetCell cn = row.getCell(j*2+1);
                    if(tw.getValue()==null) continue;
                    if(tw.getValue().equals(cn.getValue())) continue;
                    if(index.contains(tw.getValue()))
                        continue;
                    mapping.put("tw", (String) tw.getValue());
                    mapping.put("cn", (String) cn.getValue());
                    dict.add(mapping);
                    index.add((String) tw.getValue());
                }
            }
        }

        in.close();
        JSONParser parser = new JSONParser();
        String data = parser.writeValueAsString(dict);
        FileWriter out = new FileWriter("/Users/jasonlin/Downloads/terms_character.json");
        out.write(data);
        out.close();
    }

    @Test
    public void testEncoding() throws Exception {
        String unicode = "</return_message>";

        System.out.println("UTF-16: " + unicode);
        char y[] = unicode.toCharArray();
        for (int i = 0; i < y.length; i++) {
            System.out.print(String.format("%x ", (int) y[i]));
        }
        System.out.println();

        // unicode 轉成 Big5 編碼
        byte[] big5 = unicode.getBytes("Big5");
        // Big5 編碼 轉回 unicode
        unicode = new String(big5, "Big5");
        System.out.println("Big5: " + unicode);
        for (int i = 0; i < big5.length; i++) {
            System.out.print(String.format("%x ", big5[i]));
        }
        System.out.println();

        byte[] utf8 = null;
        // unicode 轉成 UTF-8 編碼
        // utf8 = unicode.getBytes("UTF-8");

        // Big5 編碼 轉回 unicode 再轉成 UTF-8 編碼
        utf8 = new String(big5, "Big5").getBytes("UTF-8");

        System.out.println("UTF-8: " +unicode);
        for (int i = 0; i < utf8.length; i++) {
            System.out.print(String.format("%x ", utf8[i]));
        }
        System.out.println();

        // unicode 轉成 MS950 編碼
        byte[] ms950 = unicode.getBytes("MS950");
        // MS950 編碼 轉回 unicode
        unicode = new String(ms950, "MS950");
        System.out.println("MS950: " + unicode);
        for (int i = 0; i < big5.length; i++) {
            System.out.print(String.format("%x ", ms950[i]));
        }
        System.out.println();
    }
}