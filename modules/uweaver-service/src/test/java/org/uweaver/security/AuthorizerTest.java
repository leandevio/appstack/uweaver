package org.uweaver.security;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;

import java.io.File;
import java.io.InputStream;
import java.util.*;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 9/7/16.
 */
public class AuthorizerTest {
    Authorizer authorizer;
    JSONParser jsonParser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace(".", File.separator);
    List<Map> passData, authorizationData, permissionData;

    @BeforeTest
    public void beforeTest() throws Exception {
        authorizer = Authorizer.getDefaultInstance();
        authorizer.reset();
        passData = readData("passes");
        authorizationData = readData("authorizations");
        permissionData = readData("permissions");
    }

    private List<Map> readData(String name) throws Exception {
        List<Map> data;
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + name + ".json");
        data = Arrays.asList(jsonParser.readValue(in, Map[].class));
        in.close();

        return data;
    }

    @Test
    public void testRegisterPermission() throws Exception {
        List<Permission> expects = new ArrayList<>();
        for(Map data : permissionData) {
            Permission expect = new Permission();
            expect.setKey((String) data.get("key"));
            expect.setName((String) data.get("name"));
            expect.setUri((String) data.get("uri"));
            expect.setMethod((String) data.get("method"));
            expects.add(expect);
        }

        authorizer.registerPermissions(expects);

        for(Permission expect : expects) {
            Permission actual = authorizer.getPermission(expect.getKey());
            assertEquals(actual, expect);
        }

        List<Permission> actuals = authorizer.createPermissionQuery().list();
        for(Permission actual : actuals) {
            Permission expect = authorizer.getPermission(actual.getKey());
            assertEquals(actual, expect);
        }
    }

    @Test(dependsOnMethods = "testRegisterPermission")
    public void testRegisterAuthorization() throws Exception {
        List<Authorization> expects = new ArrayList<>();
        for(Map data : authorizationData) {
            Authorization expect = new Authorization();
            expect.setKey((String) data.get("key"));
            Set<String> tags = new HashSet<>((List<String>) data.get("tags"));
            expect.setTags(tags);
            if(data.get("permissions")!=null) {
                for(Map<String, Object> permissionData: (List<Map<String, Object>>) data.get("permissions")) {
                    Permission permission = authorizer.getPermission((String) permissionData.get("key"));
                    expect.addPermission(permission);
                }
            }
            expects.add(expect);
        }

        authorizer.registerAuthorizations(expects);

        for(Authorization expect : expects) {
            Authorization actual = authorizer.getAuthorization(expect.getKey());
            assertEquals(actual, expect);
        }

    }


    @Test(dependsOnMethods = "testRegisterAuthorization")
    public void testRegisterPass() throws Exception {
        List<Pass> expects = new ArrayList<>();
        for(Map data : passData) {
            Pass expect = new Pass();
            expect.setKey((String) data.get("key"));
            expect.setUsername((String) data.get("username"));
            expect.setPassword((String) data.get("password"));
            expect.setEmail((String) data.get("email"));
            expect.setProperties((Map<String, Object>) data.get("properties"));
            expects.add(expect);
        }

        authorizer.registerPasses(expects);

        for(Pass expect : expects) {
            Pass actual = authorizer.getPass(expect.getKey());
            assertEquals(actual, expect);
        }
    }


    @Test(dependsOnMethods = {"testRegisterPass"})
    public void testGrant() throws Exception {
        assertTrue(authorizer.permit(authorizer.getPermission("/rest/products/ GET")));
        assertFalse(authorizer.permit(authorizer.getPermission("/rest/products/{id}/ GET")));

        List<Authorization> authorizations = authorizer.createAuthorizationQuery().tag("role").list();
        Pass pass = authorizer.createPassQuery().list().get(0);

        for(Authorization authorization: authorizations) {
            if(authorization.getPermissions().size()==0) continue;
            assertFalse(authorizer.permit(pass, authorization));
            assertFalse(authorizer.permit(pass, authorization.getPermissions()));
            authorizer.grant(pass, authorization);
            assertTrue(authorizer.permit(pass, authorization));
            assertTrue(authorizer.permit(pass, authorization.getPermissions()));
        }

        List<Authorization> rights = authorizer.getAuthorizationsByPass(pass);
        authorizer.grant(pass, rights);
        assertEquals(rights.size(), authorizer.getAuthorizationsByPass(pass).size());
    }

    @Test(dependsOnMethods = {"testGrant"})
    public void testRevoke() throws Exception {
        List<Authorization> authorizations = authorizer.createAuthorizationQuery().tag("role").list();
        Pass pass = authorizer.createPassQuery().list().get(0);

        for(Authorization authorization: authorizations) {
            if(authorization.getPermissions().size()==0) continue;
            assertTrue(authorizer.permit(pass, authorization.getPermissions()));
            authorizer.revoke(pass, authorization);
            assertFalse(authorizer.permit(pass, authorization.getPermissions()));
        }
    }


    @Test(dependsOnMethods = "testRegisterPass")
    public void testSigninSignout() throws Exception {
        for(Map data : passData) {
            String username = (String) data.get("username");
            Map properties = (Map) data.get("properties");
            String password = (String) properties.get("password");
            Pass pass = authorizer.signin(username, password);
            assertEquals(pass.getKey(), data.get("key"));
            assertEquals(pass.getUsername(), data.get("username"));
            authorizer.signout(pass);
        }

        authorizer.registerHandler(MyAuthenticationHandler.class);
        for(Map data : passData) {
            String username = (String) data.get("username");
            Map properties = (Map) data.get("properties");
            String password = (String) properties.get("password");
            Pass pass = authorizer.signin(username, password);
            assertEquals(pass.getKey(), data.get("key"));
            assertEquals(pass.getUsername(), data.get("username"));
            authorizer.signout(pass);
        }
    }

    @Test(dependsOnMethods = "testSigninSignout")
    public void testUnregisterPermission() throws Exception {
        Permission permission = null;
        Authorization authorization = null;
        List<Authorization> authorizations = authorizer.createAuthorizationQuery().list();

        for(Authorization any : authorizations) {
            if(any.getPermissions().size()>0) {
                authorization = any;
                permission = any.getPermissions().get(0);
                break;
            }
        }
        assertNotNull(permission);
        authorizer.unregisterPermission(permission);
        assertNull(authorizer.getPermission(permission.getKey()));
        authorization = authorizer.getAuthorization(authorization.getKey());
        assertFalse(authorization.getPermissions().contains(permission));
    }

    @Test(dependsOnMethods = "testSigninSignout")
    public void testUnregisterAuthorization() throws Exception {
        Authorization authorization = authorizer.createAuthorizationQuery().list().get(2);
        Pass pass = authorizer.createPassQuery().list().get(0);

        authorizer.grant(pass, authorization);

        assertNotNull(authorization);
        authorizer.unregisterAuthorization(authorization);
        assertNull(authorizer.getAuthorization(authorization.getKey()));
        assertEquals(authorizer.createPrivilegeQuery().authorization(authorization).list().size(), 0);
    }
}