package org.uweaver.user;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.uweaver.core.json.JSONParser;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 10/12/16.
 */
public class UserManagerTest {

    UserManager userManager;
    JSONParser jsonParser = new JSONParser();
    Class clazz = this.getClass();
    String path = clazz.getPackage().getName().replace('.', '/');
    List<Map> preferenceData;


    @BeforeTest
    public void beforeTest() throws Exception {
        userManager = new UserManager();
        userManager.reset();
        preferenceData = readData("preferences");
    }

    private List<Map> readData(String name) throws Exception {
        List<Map> data;
        InputStream in = clazz.getClassLoader().getResourceAsStream(path + File.separator + name + ".json");
        data = Arrays.asList(jsonParser.readValue(in, Map[].class));
        in.close();

        return data;
    }

    @Test
    public void testSavePreference() throws Exception {
        List<Preference> expects = new ArrayList<>();
        for(Map data : preferenceData) {
            Preference expect = new Preference();
            expect.setKey((String) data.get("key"));
            expect.setProperties((Map<String, Object>) data.get("properties"));
            expects.add(expect);
        }

        userManager.savePreference(expects);

        for(Preference expect : expects) {
            Preference actual = userManager.getPreference(expect.getKey());
            assertEquals(actual, expect);
        }
    }

}