package org.uweaver.security.domain;

import org.uweaver.security.Pass;

public class SecurityHolder {
    private static ThreadLocal<Pass> holder = new ThreadLocal<>();

    public static Pass pass() {
        return holder.get();
    }

    public static void setPass(Pass pass) {
        holder.set(pass);
    }

    public static void clear() {
        holder.set(null);
    }
}
