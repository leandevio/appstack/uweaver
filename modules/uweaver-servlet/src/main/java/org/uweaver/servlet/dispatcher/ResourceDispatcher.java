/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.dispatcher;

import org.uweaver.core.content.ContentAnalyzer;
import org.uweaver.core.util.MediaType;
import org.uweaver.core.util.PathPattern;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ResourceDispatcher obj = new ResourceDispatcher();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ResourceDispatcher implements Filter {
    private ContentAnalyzer contentAnalyzer = new ContentAnalyzer();
    private Map<PathPattern, String> handlers = new HashMap<>();
    private ClassLoader loader = ResourceDispatcher.class.getClassLoader();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String pathInfo = request.getServletPath();
        String location = match(pathInfo);

        if(location==null) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        pathInfo = location + removeLeadingPathSeparator(pathInfo);
        if(loader.getResource(pathInfo)==null) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        MediaType mediaType;
        if(pathInfo.endsWith(".css")) {
            mediaType = new MediaType("text", "css");
        } else {
            InputStream input = loader.getResourceAsStream(pathInfo);
            mediaType = contentAnalyzer.detect(input);
            input.close();
        }
        response.setContentType(mediaType.toString());

        InputStream input = loader.getResourceAsStream(pathInfo);
        OutputStream output = response.getOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while((length = input.read(buffer)) != -1) {
            output.write(buffer, 0, length);
        }
        input.close();

        response.setStatus(HttpServletResponse.SC_OK);
    }

    private String removeLeadingPathSeparator(String pathInfo) {
        return pathInfo.startsWith("/") ? pathInfo.substring(1) : pathInfo;
    }

    private String match(String pathInfo) {
        String location = null;

        for(PathPattern pathPattern : handlers.keySet()) {
            if(pathPattern.matcher(pathInfo).find()) {
                location = handlers.get(pathPattern);
            }
        }
        return location;
    }

    @Override
    public void destroy() {
    }

    public void addResourceHandler(String pathSpec, String location) {
        handlers.put(PathPattern.compile(PathPattern.Syntax.GLOB, pathSpec), location);
    }
}