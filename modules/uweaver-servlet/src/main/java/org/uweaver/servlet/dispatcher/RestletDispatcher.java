/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.dispatcher;

import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.util.PathPattern;
import org.uweaver.servlet.restlet.*;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RestletDispatcher obj = new RestletDispatcher();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RestletDispatcher implements Filter {
    private RestletContext restletContext;
    List<RestletRegistration> queue = new ArrayList<>();

    private RestletRequest createRestletRequest(HttpServletRequest request, String restletPath) {
        RestletRequest restletRequest = new RestletRequest(request);
        restletRequest.setRestletContext(this.restletContext);
        restletRequest.setRestletPath(restletPath);
        return restletRequest;
    }

    private RestletResponse createRestletResponse(HttpServletResponse response) {
        RestletResponse restletResponse = new RestletResponse(response);
        return restletResponse;
    }

    public Restlet addRestlet(Restlet restlet, String pathSpec) {
        RestletRegistration registration = new RestletRegistration(restlet, pathSpec);
        queue.add(registration);
        return restlet;
    }

    public Restlet addRestlet(Class<? extends Restlet> restletClass, String pathSpec) {
        Restlet restlet;
        try {
            restlet = restletClass.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
        return addRestlet(restlet, pathSpec);
    }

    public void removeRestlet(Restlet restlet) {
        restletContext.removeRestlet(restlet);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String restletContextPath = filterConfig.getServletContext().getContextPath();
        restletContext = new RestletContext(restletContextPath);
        for(RestletRegistration registration : queue) {
            restletContext.addRestlet(registration.restlet(), registration.pathSpec());
        }
        queue.clear();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String pathInfo = request.getServletPath();

        RestletResponse restletResponse = createRestletResponse(response);
        RestletRequest restletRequest = null;
        for(RestletRegistration registration : restletContext.registrations()) {
            PathPattern pathPattern = registration.pathPattern();
            Matcher matcher = pathPattern.matcher(pathInfo);
            if(!matcher.find()) continue;

            Restlet restlet = registration.restlet();
            String restletPath = registration.pathSpec();
            restletRequest = createRestletRequest(request, restletPath);
            restlet.service(restletRequest, restletResponse);
            if(restletRequest.isHandled()) break;
        }

        if(restletRequest!=null && restletRequest.isHandled()) return;

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        restletContext.destroy();
    }

}