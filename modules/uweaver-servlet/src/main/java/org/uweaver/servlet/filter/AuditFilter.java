package org.uweaver.servlet.filter;

import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuditFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(AuditFilter.class);
    private final Environment environment = Environment.getDefaultInstance();
    private SimpleDateFormat filenameFormatter = new SimpleDateFormat("'audit'-yyyy-MM-dd.'log'");
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private static Pattern RESTREGEXP = Pattern.compile("/[0-9a-z]{8}-([0-9a-z]{4}-){3}[0-9a-z]{12}/");
    private static Pattern STATICCONTENTREGEXP = Pattern.compile("/.*\\.(html|js|css|json|properties|woff|woff2|tiff|ttf)$");

    private String auditLog = null;
    private FileWriter writer = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    private FileWriter getWriter(Date date) {
        String filename = filenameFormatter.format(date);
        if(filename.equals(auditLog)) {
            return writer;
        }

        closeWriter();

        auditLog = filename;
        Path path = environment.logFolder().resolve(auditLog);
        try {
            writer = new FileWriter(path.toFile(), true);
        } catch (IOException e) {
            LOGGER.warn(e);
        }

        return writer;
    }

    private void closeWriter() {
        if(writer==null) return;

        try {
            writer.close();
        } catch (IOException e) {
            LOGGER.warn(e);
        }
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String uri = parseUri(request.getServletPath());
        String method = request.getMethod();

        if(isStaticContent(uri)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpSession session = request.getSession();
        String sessionId = session.getId();

        long begin = System.currentTimeMillis();

        filterChain.doFilter(servletRequest, servletResponse);

        long end = System.currentTimeMillis();

        Date now = new Date(begin);
        StringBuilder line = new StringBuilder(timeFormatter.format(now)).append("|");
        line.append(uri).append("|");
        line.append(method).append("|");
        line.append(end-begin).append("|");
        line.append(sessionId).append("|");
        FileWriter writer = getWriter(now);
        if(writer==null) return;
        writer.write(line.toString());
        writer.write('\n');
        writer.flush();
    }

    @Override
    public void destroy() {
        closeWriter();
    }

    private String parseUri(String servletPath) {
        Matcher matcher = RESTREGEXP.matcher(servletPath);
        return matcher.replaceAll("/\\${uuid}/");
    }

    private boolean isStaticContent(String uri) {
        Matcher matcher = STATICCONTENTREGEXP.matcher(uri);
        return matcher.find();
    }

}
