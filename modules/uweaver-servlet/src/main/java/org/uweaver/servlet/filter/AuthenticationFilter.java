/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.cipher.Encryptor;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.security.Authorizer;
import org.uweaver.security.Pass;
import org.uweaver.security.domain.SecurityHolder;
import org.uweaver.security.exception.AuthenticationException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AuthenticationFilter obj = new AuthenticationFilter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class AuthenticationFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);
    private static final Environment environment = Environment.getDefaultInstance();
    private static final Authorizer authorizer = Authorizer.getDefaultInstance();
    private static final JSONParser parser = new JSONParser();
    private static final String MSG_LOGINFAILED = "Login failed: Username / password mismatch.";
    private long EXPIRATIONTIME = 864000;
    private String SIGNINGKEY = environment.property("encryptor.key", "0000");
    private static final String BEARER = "Bearer", BASIC = "Basic";
    private static final Encryptor encryptor = new Encryptor(Encryptor.Algorithm.DES, environment.property("encryptor.key", "0000"));

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;


        SecurityHolder.clear();

        try {
            Pass pass = authenticate(request);

            if(pass==null) {
                pass = login(request, response);
                if(pass!=null) {
                    successfulLogin(request, response, pass);
                }
            }

            if(pass!=null) {
                SecurityHolder.setPass(pass);
            }
        } catch (AuthenticationException ex) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().write(createMessage(ex.getMessage()));
            return;
        }

        chain.doFilter(servletRequest, servletResponse);
    }

    protected Pass authenticate(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        if(authorization==null) {
            authorization = request.getParameter("Authorization");
            if(authorization==null) {
                return null;
            }
        }

        Pass pass = null;

        String[] tokens = authorization.split(" ");
        String scheme = tokens[0];
        if(scheme.equals(BEARER)) {
            String subject = encryptor.decipher(tokens[1]);
            pass = authorizer.getPass(subject);
        }
        return pass;
    }

    protected Pass login(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String authorization = request.getHeader("Authorization");
        if(authorization==null) return null;
        Pass pass = null;

        String[] tokens = authorization.split(" ");
        String scheme = tokens[0];
        if(scheme.equals(BASIC)) {
            pass = signin(tokens[1]);
            if(pass==null) {
                throw new AuthenticationException(MSG_LOGINFAILED);
            }
        }

        return pass;
    }

    protected void successfulLogin(HttpServletRequest request, HttpServletResponse response, Pass pass) {
        String subject = pass.getKey();
        String JwtToken = encryptor.cipher(subject);
        response.addHeader("Authorization", BEARER + " " + JwtToken);
        response.addHeader("Access-Control-Expose-Headers", "Authorization");
    }

    private String createMessage(String message) {
        Map<String, String> payload = new HashMap<>();
        payload.put("message", message);
        return parser.writeValueAsString(payload);
    }

    private Pass signin(String credentials) {
        String[] tokens = decode(credentials).split(":");
        String username = tokens[0];
        String password = tokens.length>1 ? tokens[1] : null;
        return authorizer.signin(username, password);
    }

    private void signout(Pass pass) {
        authorizer.signout(pass);
    }

    private String decode(String hash) {
        String value;
        try {
            value = new String(DatatypeConverter.parseBase64Binary(hash), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ApplicationException(e);
        }
        return value;
    }

    @Override
    public void destroy() {}
}
