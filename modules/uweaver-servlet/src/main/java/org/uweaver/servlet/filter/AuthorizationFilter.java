/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.json.JSONParser;
import org.uweaver.security.Authorizer;
import org.uweaver.security.Pass;
import org.uweaver.security.Permission;
import org.uweaver.security.domain.SecurityHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AuthorizationFilter obj = new AuthorizationFilter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class AuthorizationFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityFilter.class);
    private static final Authorizer authorizer = Authorizer.getDefaultInstance();
    private static final JSONParser parser = new JSONParser();
    private static final String MSG_MULTISESSIONS = "Multiple sessions not support.";
    private static final String MSG_UNAUTHORIZED = "Authentication is required.";
    private static final String MSG_FORBIDDEN = "Access denied.";
    private static final String MSG_LOGINTIMEOUT = "Login timeout.";

    private static Pattern RESTREGEXP = Pattern.compile("/[0-9a-z]{8}-([0-9a-z]{4}-){3}[0-9a-z]{12}/");

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String uri = parseUri(request.getServletPath());
        String method = request.getMethod();

        HttpSession session = request.getSession();
        String sessionId = session.getId();
        long thread = Thread.currentThread().getId();
        Pass pass = SecurityHolder.pass();
        Integer status = null;
        String message = null;

        boolean isPermitted = true;
        if(pass==null) {
            if(!permit(uri, method)) {
                isPermitted = false;
                status = HttpServletResponse.SC_UNAUTHORIZED;
                message = MSG_UNAUTHORIZED;
            }
        } else {
            if(!permit(pass, uri, method)) {
                isPermitted = false;
                status = HttpServletResponse.SC_FORBIDDEN;
                message = MSG_FORBIDDEN;
            }
        }

        LOGGER.debug("{} {} {} {} {}", uri, (pass==null) ? "n/a" : pass, isPermitted ? "permitted" : "not permitted",
                (sessionId==null) ? "n/a" : sessionId, thread);

        if(!isPermitted) {
            response.setStatus(status);
            response.getWriter().write(createMessage(message));
            return;
        }

        chain.doFilter(servletRequest, servletResponse);
    }

    private String createMessage(String message) {
        Map<String, String> payload = new HashMap<>();
        payload.put("message", message);
        return parser.writeValueAsString(payload);
    }


    protected String parseUri(String servletPath) {
        Matcher matcher = RESTREGEXP.matcher(servletPath);
        return matcher.replaceAll("/\\${uuid}/");
    }

    private boolean permit(String uri, String method) {
        Permission permission = authorizer.createPermissionQuery().uri(uri).method(method).singleResult();
        return authorizer.permit(permission);
    }

    private boolean permit(Pass pass, String uri, String method) {
        Permission permission = authorizer.createPermissionQuery().uri(uri).method(method).singleResult();
        return authorizer.permit(pass, permission);
    }


    @Override
    public void destroy() {}
}
