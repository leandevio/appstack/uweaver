/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.filter;

import org.uweaver.core.util.Environment;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AppController obj = new AppController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ServerConnector implements Filter {
    private static Pattern ACTIONREGEXP = Pattern.compile("/_([a-z]*)");
    private static Environment environment = Environment.getDefaultInstance();
    private static class SingletonHolder {
        private static final ServerConnector INSTANCE = ServerConnector.getInstance();
    }

    public static ServerConnector getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static ServerConnector getInstance() {
        return new ServerConnector();
    }

    private ServerConnector() {}

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(request.getHeader("Authorization")!=null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else if(request.getHeader("Action")!=null) {
            dispatchAction(request, response);
        } else if(request.getServletPath().equals("/")) {
            welcome(request, response);
        } else {
            setResponseHeader(response);
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }

    private void setResponseHeader(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
    }

    private void welcome(HttpServletRequest request, HttpServletResponse response) throws IOException {
        setResponseHeader(response);
        response.sendRedirect(environment.property("web.homepage", "index.html"));
    }

    private void dispatchAction(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = request.getHeader("Action");
        if(action.equalsIgnoreCase("helo")) {
            helo(request, response);
        }
    }

    private void helo(HttpServletRequest request, HttpServletResponse response) throws IOException {

//        Restlet restlet = new EnvironmentRestlet(request.getServletContext());
//
//        String payload = restlet.service(request, response);
//
//        response.setContentType("application/json");
//        response.setCharacterEncoding("UTF-8");
//        response.getWriter().write(payload);
//        String contextPath = request.getServletContext().getContextPath();
//
//        Map<String, Object> payload = new HashMap<>();
//
//        payload.put("webContext", contextPath + "/app");
//        payload.put("restletContext", contextPath + "/rest");
//        payload.put("uwContext", contextPath + "/uw");
//        payload.put("systemContext", contextPath + "/_");
//
//        response.setStatus(HttpServletResponse.SC_OK);
//        response.getWriter().write(parser.writeValueAsString(payload));
    }

    private String parseAction(String servletPath) {
        Matcher matcher = ACTIONREGEXP.matcher(servletPath);
        return matcher.find() ? matcher.group(1) : null;
    }
}
