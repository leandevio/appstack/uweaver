/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.filter;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AppController obj = new AppController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ServerController implements Filter {
    private static final JSONParser parser = new JSONParser();
    private Environment environment = Environment.getDefaultInstance();
    private Map<String, Object> properties = new HashMap<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext context = filterConfig.getServletContext();
        Map<String, Object> servlet = new HashMap<>();
        servlet.put("contextPath", context.getContextPath());
        Map<String, Object> restlet = new HashMap<>();
        restlet.put("contextPath", environment.property("restlet.contextPath"));
        properties.put("servlet", servlet);
        properties.put("restlet", restlet);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        initLocale(request);

        if(request.getHeader("Action")==null) {
            if(request.getServletPath().equals("/")) {
                welcome(request, response);
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
            return;
        }
        dispatchAction(request, response);
    }

    private void initLocale(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if(session.getAttribute("locale")!=null) return;

        String languages = request.getHeader("accept-language");
        Locale locale = environment.locale();
        if(languages!=null) {
            String language = languages.split(",")[0];
            locale = Locale.forLanguageTag(language);
        }
        session.setAttribute("locale", locale);
    }

    @Override
    public void destroy() {
    }

    private void dispatchAction(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = request.getHeader("Action");
        if(action.equalsIgnoreCase("helo")) {
            helo(request, response);
        }
    }

    private void helo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String payload = parser.writeValueAsString(properties);

        // Set response content type
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.write(payload);

    }

    private void welcome(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String queryString = request.getQueryString();
        String home = environment.property("web.homepage", "index.html");
        String url = String.format("%s?%s", home, queryString);
        response.sendRedirect(url);
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
