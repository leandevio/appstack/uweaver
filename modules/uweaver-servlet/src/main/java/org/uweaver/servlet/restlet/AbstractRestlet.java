/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AbstractRestlet obj = new AbstractRestlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public abstract class AbstractRestlet implements Restlet {
    @Override
    public void init(RestletContext config) {}

    @Override
    public void service(RestletRequest request, RestletResponse response) {
        String method = request.method();

        if(method.equals("POST")) {
            doPost(request, response);
        } else if(method.equals("PUT")) {
            doPut(request, response);
        } else if(method.equals("PATCH")) {
            doPatch(request, response);
        } else if(method.equals("DELETE")) {
            doDelete(request, response);
        } else {
            doGet(request, response);
        }
    }

    @Override
    public void doGet(RestletRequest request, RestletResponse response) {}

    @Override
    public void doPost(RestletRequest request, RestletResponse response) {}

    @Override
    public void doPut(RestletRequest request, RestletResponse response) {}

    @Override
    public void doPatch(RestletRequest request, RestletResponse response) {}

    @Override
    public void doDelete(RestletRequest request, RestletResponse response) {}

    @Override
    public void destroy() {}
}
