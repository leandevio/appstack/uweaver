/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import javax.servlet.ServletContext;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * EnvironmentRestlet obj = new EnvironmentRestlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class EnvironmentRestlet extends AbstractRestlet implements Restlet {
    private Environment  environment;
    private static final JSONParser parser = new JSONParser();
    private Map<String, Object> properties = new HashMap<>();


    public EnvironmentRestlet() {
        environment = Environment.getDefaultInstance();
    }

    public EnvironmentRestlet(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void init(RestletContext context) {
        Map<String, Object> entity = new HashMap<>();
        entity.put("idAttribute", environment.property("entity.idAttribute", "id"));
        Map<String, Object> theme = new HashMap<>();
        theme.put("css", environment.property("theme.css"));
        Map<String, Object> wm = new HashMap<>();
        wm.put("uri", environment.property("wm.uri", "uweaver/wm/Workbench"));
        Map<String, Object> logon = new HashMap<>();
        logon.put("uri", environment.property("logon.uri", "uweaver/applet/Logon"));
        Map<String, Object> home = new HashMap<>();
        home.put("uri", environment.property("home.uri", "uweaver/applet/Home"));

        properties.put("entity", entity);
        properties.put("theme", theme);
        properties.put("wm", wm);
        properties.put("home", home);
        properties.put("logon", logon);
    }

    @Override
    public void service(RestletRequest request, RestletResponse response) {

        String payload = parser.writeValueAsString(properties);

        // Set response content type
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.writer();
        out.write(payload);
        request.setHandled(true);
    }
}
