/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.util.Environment;
import org.uweaver.i18n.I18n;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * I18nRestlet obj = new I18nRestlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class I18nRestlet extends AbstractRestlet implements Restlet  {
    private I18n i18n = new I18n();

    @Override
    public void service(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();
        Pattern pattern = Pattern.compile("/terms/(.*)");
        Matcher matcher = pattern.matcher(pathInfo);

        String payload = null;
        if(matcher.matches()) {
            String type = matcher.group(1);
            payload = i18n.getTermsAsJSON(I18n.TermType.valueOf(type.toUpperCase()));
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.writer();

        out.println(payload);
        request.setHandled(true);
    }
}
