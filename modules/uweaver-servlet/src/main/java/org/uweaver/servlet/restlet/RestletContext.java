/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.exception.ApplicationException;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RestletConfig obj = new RestletConfig();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RestletContext {
    private String contextPath;
    private List<RestletRegistration> registrations = new ArrayList<>();
    public RestletContext(String contextPath) {
        this.contextPath = contextPath;
    }

    public String contextPath() {
        return getContextPath();
    }

    public String getContextPath() {
        return contextPath;
    }

    public Restlet addRestlet(Restlet restlet, String pathSpec) {
        RestletRegistration registration = new RestletRegistration(restlet, pathSpec);
        registrations.add(registration);
        restlet.init(this);
        return restlet;
    }

    public void removeRestlet(Restlet restlet) {
        List<RestletRegistration> found = new ArrayList<>();
        for(RestletRegistration registration : registrations) {
            if(registration.restlet().equals(restlet)) {
                found.add(registration);
                registration.restlet().destroy();
            }
        }
        registrations.remove(found);
    }

    public List<RestletRegistration> registrations() {
        return Collections.unmodifiableList(this.registrations);
    }

    public void destroy() {
        for(RestletRegistration registration : registrations) {
            registration.restlet().destroy();
        }
        registrations().clear();
    }
}
