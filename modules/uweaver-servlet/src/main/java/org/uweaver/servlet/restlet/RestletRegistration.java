/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.util.PathPattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RestletRegistration obj = new RestletRegistration();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RestletRegistration {
    private String pathSpec;
    private Restlet restlet;
    private PathPattern pathPattern;

    /**
     *
     * @param restlet
     * @param pathSpec  A glob pattern
     */
    public RestletRegistration(Restlet restlet, String pathSpec) {
        this.restlet = restlet;
        setPathSpec(pathSpec);
    }

    public String pathSpec() {
        return getPathSpec();
    }

    public String getPathSpec() {
        return pathSpec;
    }

    public void setPathSpec(String pathSpec) {
        this.pathSpec = pathSpec;
        this.pathPattern = PathPattern.compile(PathPattern.Syntax.GLOB, pathSpec);
    }

    public PathPattern pathPattern() {
        return this.pathPattern;
    }

    public Restlet restlet() {
        return getRestlet();
    }

    public Restlet getRestlet() {
        return restlet;
    }

    public void setRestlet(Restlet restlet) {
        this.restlet = restlet;
    }


}
