/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.json.JSONParser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RestletRequest obj = new RestletRequest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RestletRequest {
    HttpServletRequest nativeRequest;
    RestletSession session = null;
    RestletContext restletContext = null;
    String restletPath = "";
    boolean handled = false;
    private Map<String, Object> data = new HashMap<>();
    JSONParser parser = new JSONParser();

    public RestletRequest(HttpServletRequest httpServletRequest) {
        this.nativeRequest = httpServletRequest;
        try {
            this.session = new RestletSession(httpServletRequest.getSession());
        } catch(IllegalStateException e) {}

        parse();
    }

    private void parse() {
        try {
            if(nativeRequest.getInputStream().isFinished()||nativeRequest.getInputStream().available()==0) return;
            data = parser.readValue(nativeRequest.getInputStream(), Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String pathInfo() {
        String uri = nativeRequest.getRequestURI();
        String regex = "^" + restletContext.contextPath();

        if(!hasWildcard(restletPath)) {
            regex = regex + restletPath;
        }
        return uri.replaceFirst(regex + "/", "");
    }

    public String method() {
        return nativeRequest.getMethod();
    }

    private boolean hasWildcard(String s) {
        return s.contains("*");
    }

    public RestletSession session() {
        return this.session;
    }

    public void setRestletContext(RestletContext restletContext) {
        this.restletContext = restletContext;
    }

    public void setRestletPath(String restletPath) {
        this.restletPath = restletPath;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }

    public Object data(String name) {
        return this.data.containsKey(name) ? this.data.get(name) : this.nativeRequest.getParameter(name);
    }

    public String dataAsString(String name) {
        return this.data.containsKey(name) ? (String) this.data.get(name) : this.nativeRequest.getParameter(name);
    }

    public Map<String, String> data() {
        Map<String, String> parameters = new HashMap<>();
        Enumeration<String> names = this.nativeRequest.getParameterNames();
        while(names.hasMoreElements()) {
            String name = names.nextElement();
            parameters.put(name, this.nativeRequest.getParameter(name));
        }
        return parameters;
    }
}
