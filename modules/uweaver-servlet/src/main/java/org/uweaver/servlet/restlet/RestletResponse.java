/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RestletResponse obj = new RestletResponse();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RestletResponse {
    HttpServletResponse nativeResponse;
    RestletContext restletContext = null;

    public RestletResponse(HttpServletResponse httpServletResponse) {
        this.nativeResponse = httpServletResponse;
    }

    public void setHeader(String name, String value) {
        this.nativeResponse.setHeader(name, value);
    }

    public String header(String name) {
        return this.nativeResponse.getHeader(name);
    }

    public void setContentType(String contentType) {
        this.nativeResponse.setContentType(contentType);
    }

    public void setIntHeader(String name, int value) {
        this.nativeResponse.setIntHeader(name, value);
    }

    public void setCharacterEncoding(String characterEncoding) {
        this.nativeResponse.setCharacterEncoding(characterEncoding);
    }

    public PrintWriter writer() {
        try {
            return this.nativeResponse.getWriter();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }

    public void setStatus(int status) {
        nativeResponse.setStatus(status);
    }

    public OutputStream outputStream() {
        try {
            return this.nativeResponse.getOutputStream();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }
    }
}
