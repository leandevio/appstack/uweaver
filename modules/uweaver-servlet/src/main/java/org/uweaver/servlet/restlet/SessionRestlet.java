/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.security.Pass;
import org.uweaver.security.domain.SecurityHolder;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SessionRestlet obj = new SessionRestlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SessionRestlet extends AbstractRestlet implements Restlet {
    private static final JSONParser parser = new JSONParser();

    @Override
    public void doGet(RestletRequest request, RestletResponse response) {
        RestletSession session = request.session();
        Map<String, Object> payload = toPayload(session);

        // Set response content type
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.writer();
        out.println(parser.writeValueAsString(payload));
        request.setHandled(true);
    }

    private Map<String, Object> toPayload(RestletSession session) {
        Pass pass = SecurityHolder.pass();
        Locale locale = (Locale) session.getAttribute("locale");
        if(locale==null) {
            locale = Environment.getDefaultInstance().locale();
        }

        Map<String, Object> payload = new HashMap<>();
        String username = null;
        if(pass!=null) {
            username = (pass.username()==null) ? pass.key() : pass.username();
        }

        payload.put("locale", locale.toLanguageTag());
        payload.put("username", username);
        payload.put("id", session.id());

        return payload;
    }

    @Override
    public void doPut(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();
        if(pathInfo.equals("locale")) {
            setLocale(request, response);
        }
    }

    public void setLocale(RestletRequest request, RestletResponse response) {
        RestletSession session = request.session();
        String lang = request.dataAsString("locale");
        Locale locale = (lang==null) ? Environment.getDefaultInstance().locale() : Locale.forLanguageTag(lang);
        session.setAttribute("locale", locale);

        Map<String, Object> payload = toPayload(session);
        // Set response content type
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.writer();
        out.println(parser.writeValueAsString(payload));
        request.setHandled(true);
    }

}