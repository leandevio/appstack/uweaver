/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.restlet;

import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;
import org.uweaver.service.app.Site;
import org.uweaver.service.app.SiteManager;

import java.io.PrintWriter;


/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SiteRestlet obj = new SiteRestlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SiteRestlet extends AbstractRestlet implements Restlet {
    private static SiteManager manager;
    private static final JSONParser parser = new JSONParser();


    public SiteRestlet() {
        this.manager = new SiteManager();
    }

    @Override
    public void service(RestletRequest request, RestletResponse response) {

        String payload = parser.writeValueAsString(manager.site());

        // Set response content type
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.writer();
        out.println(payload);
        request.setHandled(true);
    }

}
