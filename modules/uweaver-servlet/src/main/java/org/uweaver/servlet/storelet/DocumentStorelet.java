/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.storelet;

import org.uweaver.core.util.Environment;
import org.uweaver.data.store.DocumentStore;
import org.uweaver.servlet.restlet.RestletRequest;
import org.uweaver.servlet.restlet.RestletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * FileServlet obj = new FileServlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class DocumentStorelet extends AbstractStorelet {
    private DocumentStore store;

    private static class SingletonHolder {
        private static final DocumentStorelet INSTANCE = DocumentStorelet.getInstance();
    }

    public static DocumentStorelet getDefaultInstance() {
        return DocumentStorelet.SingletonHolder.INSTANCE;
    }

    public static DocumentStorelet getInstance() {
        return new DocumentStorelet();
    }

    public static DocumentStorelet getInstance(Environment environment) {
        DocumentStorelet restlet = new DocumentStorelet(environment.docFolder());
        return restlet;
    }

    public DocumentStorelet() {
        store = new DocumentStore();
    }

    public DocumentStorelet(Path dataFolder) {
        store = new DocumentStore(dataFolder);
    }

    @Override
    public void service(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();

        if(!store.exists(pathInfo)) return;

        response.setContentType(store.mediaType(pathInfo).toString());
        InputStream input = store.open(pathInfo);
        OutputStream output = response.outputStream();
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = input.read(buffer)) != -1) {
                output.write(buffer, 0, len);
            }
            input.close();
        } catch (IOException e) {
            throw new org.uweaver.core.exception.IOException(e);
        }

        response.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);
    }
}
