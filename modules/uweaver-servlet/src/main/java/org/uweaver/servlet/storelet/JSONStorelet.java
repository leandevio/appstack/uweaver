/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.servlet.storelet;

import org.uweaver.data.store.JSONStore;
import org.uweaver.servlet.restlet.RestletRequest;
import org.uweaver.servlet.restlet.RestletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * JSONStoreServlet obj = new JSONStoreServlet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class JSONStorelet extends AbstractStorelet {
    private JSONStore store;

    private static class SingletonHolder {
        private static final JSONStorelet INSTANCE = JSONStorelet.getInstance();
    }

    public static JSONStorelet getDefaultInstance() {
        return JSONStorelet.SingletonHolder.INSTANCE;
    }

    public static JSONStorelet getInstance() {
        return new JSONStorelet();
    }

    public JSONStorelet() {
        store = new JSONStore();
    }

    @Override
    public void doGet(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter output = response.writer();
        output.write(store.get(pathInfo));
        response.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);
    }

    @Override
    public void doPost(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = request.dataAsString("payload");
        if(json!=null) {
            store.put(pathInfo, json);
        } else {
            json = store.put(pathInfo, request.data());
        }
        PrintWriter output = response.writer();
        output.print(json);

        response.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);
    }

    @Override
    public void doPut(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = request.dataAsString("payload");
        if(json!=null) {
            store.put(pathInfo, json);
        } else {
            json = store.put(pathInfo, request.data());
        }
        PrintWriter output = response.writer();
        output.print(json);

        response.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);
    }

    @Override
    public void doPatch(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = request.dataAsString("payload");
        if(json!=null) {
            store.put(pathInfo, json);
        } else {
            json = store.post(pathInfo, request.data());
        }
        PrintWriter output = response.writer();
        output.print(json);

        response.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);
    }

    @Override
    public void doDelete(RestletRequest request, RestletResponse response) {
        String pathInfo = request.pathInfo();

        store.delete(pathInfo);

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        request.setHandled(true);
    }
}
