package org.uweaver.servlet.filter;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created by jasonlin on 8/9/17.
 */
public class SecurityFilterTest {
    @Test
    public void testParseUri() throws Exception {
        SecurityFilter securityFilter = new SecurityFilter();

        String path = "/rest/orders/";
        String expect = "/rest/orders/";
        assertEquals(securityFilter.parseUri(path), expect);

        path = "/rest/orders/259c500b-3d76-4706-b11f-7cb84328b8c5/";
        expect = "/rest/orders/${uuid}/";
        assertEquals(securityFilter.parseUri(path), expect);

        path = "/rest/orders/259c500b-3d76-4706-b11f-7cb84328b8c5/cancel/";
        expect = "/rest/orders/${uuid}/cancel/";
        assertEquals(securityFilter.parseUri(path), expect);

        path = "/rest/orders/259c500b-3d76-4706-b11f-7cb84328b8c5/items/0ecf952e-b1a6-4502-b2f2-5c9680775687/cancel";
        expect = "/rest/orders/${uuid}/items/${uuid}/cancel";
        assertEquals(securityFilter.parseUri(path), expect);
    }

}