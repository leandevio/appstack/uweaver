<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.uweaver</groupId>
    <artifactId>uweaver-shell</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>uWeaver Shell</name>
    <url>http://www.uweaver.org</url>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.8</java.version>
        <jar.mainclass>org.uweaver.shell.App</jar.mainclass>
        <tesgng.version>6.9.10</tesgng.version>
        <spring.version>4.2.5.RELEASE</spring.version>
        <spring.shell.version>1.1.0.RELEASE</spring.shell.version>
        <uweaver.version>1.0-SNAPSHOT</uweaver.version>
    </properties>

    <repositories>
        <!-- libs-release for JLine fork, artifacts may not be in maven central -->
        <repository>
            <id>libs-release</id>
            <name>Spring Maven libs-release Repository</name>
            <url>http://repo.springsource.org/libs-release/</url>
        </repository>
        <repository>
            <id>libs-milestone</id>
            <url>http://repo.springsource.org/libs-milestone/</url>
        </repository>
        <repository>
            <id>maven-release</id>
            <name>maven-release</name>
            <url>http://maven.leandev.io/repository/maven-releases</url>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>fail</checksumPolicy>
            </releases>
        </repository>
        <repository>
            <id>maven-snapshots</id>
            <name>maven-snapshots</name>
            <url>http://maven.leandev.io/repository/maven-snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

    <distributionManagement>
        <snapshotRepository>
            <id>maven-snapshots</id>
            <name>maven-snapshots</name>
            <url>http://maven.leandev.io/repository/maven-snapshots/</url>
        </snapshotRepository>
    </distributionManagement>
    <dependencies>
        <!-- ** uWeaver ** -->
        <dependency>
            <groupId>org.uweaver</groupId>
            <artifactId>uweaver-core</artifactId>
            <version>${uweaver.version}</version>
        </dependency>

        <!-- ** Unit Test ** -->
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>${tesgng.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- ** Spring Framework ** -->
        <!-- Application Context -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <!-- Spring Shell -->
        <dependency>
            <groupId>org.springframework.shell</groupId>
            <artifactId>spring-shell</artifactId>
            <version>${spring.shell.version}</version>
        </dependency>
        <dependency>
            <groupId>org.uweaver</groupId>
            <artifactId>uweaver-server</artifactId>
            <version>${uweaver.version}</version>
        </dependency>
        <dependency>
            <groupId>org.uweaver</groupId>
            <artifactId>uweaver-packager</artifactId>
            <version>${uweaver.version}</version>
        </dependency>
    </dependencies>
    <build>
        <finalName>uweaver-shell</finalName>
        <plugins>
            <!-- ** Compiler ** -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.6.1</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <!-- Enable Lint Check -->
                    <compilerArgument>-Xlint:all</compilerArgument>
                    <showWarnings>true</showWarnings>
                    <showDeprecation>true</showDeprecation>
                </configuration>
            </plugin>

            <!-- ** Unit Test ** -->
            <!--
                run tests and generate reports
            -->

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <includes>
                        <include>**/*Tests.java</include>
                    </includes>
                </configuration>
            </plugin>

            <!-- ** Assembly ** -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.2.2</version>
                <!-- The configuration of the plugin -->
                <configuration>
                    <!-- Specifies the configuration file of the assembly plugin -->
                    <descriptors>
                        <descriptor>src/main/resources/assembly.xml</descriptor>
                    </descriptors>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- copy all dependencies into a lib/ directory -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/lib</outputDirectory>
                            <overWriteReleases>true</overWriteReleases>
                            <overWriteSnapshots>true</overWriteSnapshots>
                            <overWriteIfNewer>true</overWriteIfNewer>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- make the jar executable by adding a Main-Class and Class-Path to the manifest -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.3.1</version>
                <configuration>
                    <archive>
                        <!-- Manifest specific configuration -->
                        <manifest>
                            <!-- Classpath is added to the manifest of the created jar file. -->
                            <addClasspath>true</addClasspath>
                            <!--
                                Configures the classpath prefix. This configuration option is
                                used to specify that all needed libraries are found under lib/
                                directory.
                            -->
                            <classpathPrefix>lib/</classpathPrefix>
                            <!-- Specifies the main class of the application -->
                            <mainClass>${jar.mainclass}</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
