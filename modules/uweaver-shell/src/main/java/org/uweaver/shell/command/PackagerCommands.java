package org.uweaver.shell.command;

import org.uweaver.packager.Packager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by jasonlin on 4/29/14.
 */
@Component
public class PackagerCommands implements CommandMarker {
    protected final Logger logger = LoggerFactory.getLogger(getClass().getName());

    private Packager packager;

    public PackagerCommands() {
        packager = new Packager();
    }

    @CliCommand(value = "packager create", help = "建立一個新的應用程式")
    public String create(
            @CliOption(key = { "name" }, mandatory = true, help = "應用程式的名稱") final String name)
            throws IOException {

        packager.create(name);
        return "應用程式 - " + name + " 已經產生.";
    }

    @CliCommand(value = "packager update", help = "更新應用程式")
    public String update(@CliOption(key = { "name" }, mandatory = true, help = "應用程式的名稱") final String name) {

        if(!packager.isApp(name)) {

        }
        packager.update(name);

        return "應用程式 - " + name + " 已經更新.";
    }
}