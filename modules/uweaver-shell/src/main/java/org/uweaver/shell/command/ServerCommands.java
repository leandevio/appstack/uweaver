package org.uweaver.shell.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import org.uweaver.core.util.Environment;
import org.uweaver.shell.util.ServerUtil;

/**
 * Created by jasonlin on 4/25/14.
 */
@Component
public class ServerCommands implements CommandMarker {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerCommands.class);
    private Environment environment = Environment.getDefaultInstance();

    private static final String STARTSERVERSCRIPT = "server.sh";

    @CliAvailabilityIndicator({"server start"})
    public boolean isStartAvailable() {
        return ServerUtil.isStopped();
    }

    @CliCommand(value = "server start", help = "啟動伺服器")
    public String start(
            @CliOption(key = { "mode" }, mandatory = false, help = "daemon or interactive", unspecifiedDefaultValue = "interactive") final String mode)
            throws Exception {

        if(mode.equals("daemon")) {
            String script = environment.appHome() + "/bin/" + STARTSERVERSCRIPT;
            if(script.startsWith("~")) {
                script = script.replaceFirst("~", System.getProperty("user.home"));
            }

            ProcessBuilder builder = new ProcessBuilder(script);
            builder.redirectErrorStream(true);
            try {
                Process process = builder.start();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        } else {
            ServerUtil.start();
        }

        return "伺服器已經啟動. \n連接埠: " + environment.property("server.port");
    }

    @CliAvailabilityIndicator({"server stop"})
    public boolean isStopAvailable() {
        return ServerUtil.isRunning();
    }

    @CliCommand(value = "server stop", help = "關閉伺服器")
    public String stop() throws Exception {

        ServerUtil.stop();
        return "伺服器已經關閉.";
    }

    @CliCommand(value = "server restart", help = "重啟伺服器")
    public String restart() throws Exception {

        ServerUtil.restart();
        return "伺服器已經重啟.";
    }
}
