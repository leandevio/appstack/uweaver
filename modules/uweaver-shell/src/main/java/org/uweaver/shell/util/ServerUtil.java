/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p/>
 * http://www.uweaver.org
 * <p/>
 * Copyright 2016 Jason Lin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.shell.util;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uweaver.core.exception.OperationFailureException;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.JsonParser;
import org.uweaver.server.Server;
import org.uweaver.server.handler.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * The class implements ... The implementation provides ...
 *
 * Usage:
 *
 * ```java
 * Server obj = new Server();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @version  %I%, %G%
 * @since 1.0
 */
public class ServerUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerUtil.class);
    private static final Environment env = Environment.getDefaultInstance();
    private static final String USER_AGENT = "Mozilla/5.0";

    public static Server start() throws Exception {
        Server server = new Server();

        ServerHandler serverHandler = new ServerHandler();

        HtmlHandler htmlHandler = new HtmlHandler();

        RestHandler restHandler = new RestHandler();

        // Add the ResourceHandler to the server.
        HandlerList handlers = new HandlerList();

        // The resource handler is passed the request first and looks for a matching file in the local directory to serve.
        // If it does not find a file, the request passes to the default handler which generates a 404 (or favicon.ico).
        handlers.setHandlers(new Handler[] {serverHandler, restHandler, htmlHandler, new DefaultHandler()});

        server.setHandler(handlers);

        LOGGER.debug("RestHandler: contextPath => [" + restHandler.getContextPath() + "]; resourceBase => [" + restHandler.getResourceBase() + "]");
        LOGGER.debug("HtmlHandler: contextPath => [" + htmlHandler.getContextPath() + "]; resourceBase => [" + htmlHandler.getResourceBase() + "]");
        server.start();

        return server;
    }

    public static void stop(){
        String port = env.property("server.port");
        try {
            URL url = new URL("http://localhost:" + port + "/server/stop");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // optional default is GET
            conn.setRequestMethod("GET");
            //add request header
            conn.setRequestProperty("User-Agent", USER_AGENT);
            Integer responseCode = conn.getResponseCode();
            if(responseCode!= HttpStatus.OK_200) {
                throw new OperationFailureException(responseCode.toString(), conn.getResponseMessage());
            }
        } catch (IOException e) {
            throw new OperationFailureException(e.getCause());
        }
    }

    public static boolean isRunning() {
        String state = (String) status().get("state");

        return state.equals(Server.State.RUNNING.toString());
    }

    public static boolean isStopped() {
        try {
            status();
        } catch(OperationFailureException ex) {
            return true;
        }

        return false;
    }

    public static void restart() {
        String port = env.property("server.prot");
        try {
            URL url = new URL("http://localhost:" + port + "/server/restart");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // optional default is GET
            conn.setRequestMethod("GET");
            //add request header
            conn.setRequestProperty("User-Agent", USER_AGENT);
            Integer responseCode = conn.getResponseCode();
            if(responseCode!= HttpStatus.OK_200) {
                throw new OperationFailureException(responseCode.toString(), conn.getResponseMessage());
            }
        } catch (IOException e) {
            throw new OperationFailureException(e.getCause());
        }
    }

    public static Map<String, Object> status() {
        Map<String, Object> status;

        String port = env.property("server.port");


        try {
            URL url = new URL("http://localhost:" + port + "/server/status");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // optional default is GET
            conn.setRequestMethod("GET");
            //add request header
            conn.setRequestProperty("User-Agent", USER_AGENT);
            Integer responseCode = conn.getResponseCode();
            if(responseCode!= HttpStatus.OK_200) {
                throw new OperationFailureException(responseCode.toString(), conn.getResponseMessage());
            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            status = JsonParser.readValue(response.toString(), Map.class);

        } catch (IOException e) {
            throw new OperationFailureException(e);
        }
        return status;
    }

    public static void main(String[] args) throws Exception {
        start();
    }
}
