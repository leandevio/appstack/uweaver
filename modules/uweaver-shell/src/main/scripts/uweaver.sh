#!/bin/bash

script="$0"
basename="$(dirname $script)"
jarfile="$basename/../uweaver-shell.jar"

if [ -z ${UWEAVER_BASE:+x} ]; then
    export UWEAVER_BASE=$UWEAVER_HOME
fi

java -Dlog4j.debug -Dlog4j.configuration=file://$UWEAVER_BASE/conf/log4j.xml -jar $jarfile