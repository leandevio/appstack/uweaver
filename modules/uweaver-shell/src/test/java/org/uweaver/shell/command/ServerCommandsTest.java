package org.uweaver.shell.command;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

import org.springframework.shell.Bootstrap;
import org.springframework.shell.core.CommandResult;
import org.springframework.shell.core.JLineShellComponent;

public class ServerCommandsTest {

    Bootstrap bootstrap;
    JLineShellComponent shell;
    private ServerCommands commands;

    @BeforeMethod
    public void setUp() throws Exception {
        commands = new ServerCommands();
        bootstrap = new Bootstrap();
        shell = bootstrap.getJLineShellComponent();
    }

    @Test
    public void testStart() {
        CommandResult cr = shell.executeCommand("server start");
        assertEquals(true, cr.isSuccess());

        cr = shell.executeCommand("server stop");
        assertEquals(true, cr.isSuccess());

        cr = shell.executeCommand("server start --port 9090");
        assertEquals(true, cr.isSuccess());

        cr = shell.executeCommand("server stop");
        assertEquals(true, cr.isSuccess());
    }


}