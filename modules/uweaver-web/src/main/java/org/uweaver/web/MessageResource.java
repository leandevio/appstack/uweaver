package org.uweaver.web;

import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

/**
 * Created by jasonlin on 5/1/14.
 */

public class MessageResource {

    ResourceBundleMessageSource messageSource;

    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessage(String code, Object... args) {
        return messageSource.getMessage(code, args, Locale.getDefault());
    }

    public String getMessage(String code, Locale locale, Object... args) {
        return messageSource.getMessage(code, args, locale);
    }

    public String getMessage(String code) {
        return getMessage(code, null, Locale.getDefault());
    }

    public String getMessage(String code, Locale locale) {
        return messageSource.getMessage(code, null, locale);
    }

}