package org.uweaver.web;

import org.uweaver.core.util.Converters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jasonlin on 5/2/14.
 */
public class RequestPayload {
    private Map<String, String> options = new HashMap<String, String>();
    private List items = new ArrayList();
    private Converters converters = new Converters();

    public RequestPayload(Map<String, Object> payload) {
        if(payload==null) return;

        if(payload.containsKey("options")) {
            options.putAll((Map) payload.get("options"));
        }
        if(payload.containsKey("items")) {
            items.addAll((List) payload.get("items"));
        }
    }

    public boolean isRequestPayload(Map<String, Object> payload) {
        if(!payload.containsKey("options")||!payload.containsKey("items")) {
            return false;
        }
        return true;
    }

    public String getOption(String key)
    {
        return options.get(key);
    }

    public RequestPayload putOption(String key, String value) {
        options.put(key, value);
        return this;
    }

    public RequestPayload addItem(Object item) {
        items.add(item);
        return this;
    }

    public Object firstItem() {
        if(items.size()==0) return null;

        return items.get(0);
    }

    public <T> T firstItem(Class<T> clazz) {
        if(items.size()==0) return null;
        return converters.convert(items.get(0), clazz);
    }

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

}
