package org.uweaver.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.uweaver.core.exception.*;
import org.uweaver.core.util.Environment;

import java.util.*;

/**
 * Created by jasonlin on 5/1/14.
 */
public class ResponsePayload {
    private String status;
    private Long total;
    private List items = new ArrayList();

    public ResponsePayload() {
        setStatus(WebServiceStatus.SUCCESS);
    }

    public ResponsePayload(WebServiceStatus status, List items, Long total) {
        setStatus(status);
        setItems(items);
        setTotal(total);
    }

    public ResponsePayload(WebServiceStatus status, List items) {
        setStatus(status);
        setItems(items);
        setTotal(new Long(items.size()));
    }

    public ResponsePayload(String status, List items, Long total) {
        setStatus(status);
        setItems(items);
        setTotal(total);
    }

    public ResponsePayload(String status, List items) {
        setStatus(status);
        setItems(items);
        setTotal(new Long(items.size()));
    }

    public ResponsePayload(WebServiceStatus status, Object item) {
        setStatus(status);
        addItem(item);
        setTotal(1L);
    }

    public ResponsePayload(String status, Object item) {
        setStatus(status);
        addItem(item);
        setTotal(1L);
    }

    public ResponsePayload(WebServiceStatus status) {
        setStatus(status);
    }

    public ResponsePayload(String status) {
        setStatus(status);
    }

    public ResponsePayload(Exception exception) {
        if (exception instanceof UnauthorizedException) {
            setStatus(WebServiceStatus.UNAUTHORIZED);
        } else if (exception instanceof ForbiddenException) {
            setStatus(WebServiceStatus.FORBIDDEN);
        } else if (exception instanceof NotFoundException) {
            setStatus(WebServiceStatus.NOTFOUND);
        } else if(exception instanceof VersionConflictException) {
            setStatus(WebServiceStatus.VERSIONCONFLICT);
        } else if(exception instanceof DuplicateConflictException) {
            setStatus(WebServiceStatus.DUPLICATECONFLICT);
        } else if(exception instanceof ReferentialConflictException) {
            setStatus(WebServiceStatus.REFERENTIALCONFLICT);
        } else if(exception instanceof ConflictException) {
            setStatus(WebServiceStatus.CONFLICT);
        } else if (exception instanceof SizeExceededException) {
            setStatus(WebServiceStatus.REQUESTTOOLARGE);
        } else {
            setStatus(WebServiceStatus.EXCEPTION);
        }


        Map map;
        if (exception instanceof ApplicationException) {
            //map = ((ApplicationException) exception).profile();
            map = new HashMap();
        } else {
            map = new HashMap();
        }

        if(new Boolean(Environment.getDefaultInstance().property("webservice.trace", "false"))) {
            map.put("exception", exception.getClass().getSimpleName());
            map.put("message", exception.getMessage());
            map.put("stackTrace", Arrays.toString(exception.getStackTrace()));
            Throwable cause = exception.getCause();
            if(cause!=null) {
                map.put("cause", cause.getClass().getSimpleName());
                map.put("reason", cause.getMessage());
            }        }

        addItem(map);
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatus(WebServiceStatus status) {
        setStatus(status.getValue());
    }


    public ResponsePayload addItem(Object item) {
        items.add(item);
        return this;
    }

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

    public Long getTotal() {
        return this.total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public ResponseEntity<Map<String, Object>> toResponseEntity() {
        String major = getStatus().split("\\.")[0];
        int status = Integer.parseInt(major);
        return new ResponseEntity<>(profile(), HttpStatus.valueOf(status));
    }

    private Map<String, Object> profile() {
        Map map = new HashMap();
        map.put("items", getItems());
        map.put("total", getTotal());
        map.put("status", getStatus());
        return map;
    }

}
