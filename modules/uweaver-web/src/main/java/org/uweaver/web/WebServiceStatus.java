package org.uweaver.web;

import org.springframework.http.HttpStatus;

/**
 * Web Service Status Code
 *
 * 採用與 HTTP status code 相容的設計作為 web service 回傳的狀態碼(status code).
 * 讓參與 web service 執行過程中的所有軟體元件, 包括伺服器, 瀏覽器與用戶端的應用軟體都能夠知道如何正確解讀與處理傳遞的內容.
 *
 * 格式:
 * <HTTP status code>[.<extension>]
 *
 * 2xx: 代表服務請求正常執行的狀態碼.
 * - SUCCESS: 200("OK"). 代表該服務請求執行成功, .
 * - CREATED: 201("Created"). 代表新增資料的服務請求已經執行成功.
 * - ACCEPTED: 202("Accepted"). 代表該服務請求已經被接受, 但是不一定會立刻執行, 所以還不確定未來執行的結果. 通常使用在非同步的模式之下.
 * - NOCONTENT: 204("No Content"). 代表服服務請求已經執行成功, 沒有任何資料回傳. 用戶端也不需要更新相關資料內容.
 * - UPDATED: 204.1. 代表已經依據服務請求更新資料. 因為沒有任何資料回傳, 所以用戶端不需要更新相關資料內容.
 * - RESETCONTENT: 205("Reset Content"). 代表該服務請求已經執行成功. 沒有任何資料回傳. 因為有相關資料已經不存在, 所以用戶端應該清除相關的資料內容.
 * - DELETED: 205.1. 代表已經依據服務請求刪除資料, 所以用戶端應該清除相關的資料內容.
 *
 * 4xx: 應用系統因為邏輯設計拒絕或無法正常執行該服務請求所產生的狀態碼.
 * - FAILURE: 400("Bad Request"). 代表該服務請求無法正常執行.
 * - UNAUTHORIZED: 401("Unauthorized"). 代表該服務請求因為無法辨識使用者身份, 所以無法執行.
 * - FORBIDDEN: 403("Forbidden"). 代表該服務請求因為權限不足, 所以無法執行.
 * - NOTFOUND: 404("Not Found"). 代表該服務請求因為相關資料不存在, 所以無法正常執行.
 * - CONFLICT: 409("Conflict"). 代表該服務請求會破壞資料的完整性, 所以無法正常執行.
 * - VERSIONCONFLICT: 409.1. 代表該服務請求所指定的資料在作業期間已經發生異動，不是最新版本, 所以無法正常執行.
 * - DUPLICATECONFLICT: 409.2. 代表該服務請求會造成資料重複, 所以無法正常執行.
 * - CONSTRAINTCONFLICT: 409.3. 代表該服務請求違反資料檢核的規則, 所以無法正常執行.
 * - REFERENTIALCONFLICT: 409.4. 代表該服務請求會造成誤刪資料(例如刪除有訂單資料參考的客戶資料), 所以無法正常執行.
 * - NOTEMPTYCONFLICT: 409.5. 代表該服務請求會造成誤刪資料(例如刪除有成員資料的群組資料), 所以無法正常執行.
 * - GONE: 410("Gone"). 代表該服務請求因為相關資料曾經存在, 但是現在已經不存在或停用, 所以無法正常執行.
 * - OBSOLETE: 410.1. 代表該服務請求因為相關資料曾經存在, 但是現在已經停用, 所以無法正常執行.
 * - REQUESTTOOLARGE: 413("Request Entity Too Large"). 代表該服務請求因為要求處理的資料大小超過限制, 所以無法執行.

 * 5xx: 服務請求因為伺服器的狀態或是不預期的錯誤而造成無法正常執行所產生的狀態碼.
 * - EXCEPTION: 500("Internal Server Error"). 代表該服務請求因為系統發生不預期錯誤, 所以無法正常執行.
 * - UNAVAILABLE: 503("Service Unavailable"). 代表該服務請求因為處於負載過高或維護中, 所以暫時停止對外服務.
 * - TIMEOUT: 504("Gateway Timeout"). 代表該服務請求因為執行時間超過預期, 所以中斷執行.
 *
 * Created by jasonlin on 5/28/14.
 *
 */
public enum WebServiceStatus {
    SUCCESS("200"), CREATED("201"), ACCEPTED("202"), NOCONTENT("204"), RESETCONTENT("205"),

    FAILURE("400"), UNAUTHORIZED("401"), FORBIDDEN("403"), NOTFOUND("404"), CONFLICT("409"),
    GONE("410"), REQUESTTOOLARGE("413"),

    EXCEPTION("500"), UNAVAILABLE("503"), TIMEOUT("504"),

    UPDATED("204.1"), DELETED("204.2"), // use 204.2 instead of 205 for DELETED because jQuery ajax will treat 205 as error.
    VERSIONCONFLICT("409.1"), DUPLICATECONFLICT("409.2"), CONSTRAINTCONFLICT("409.3"), REFERENTIALCONFLICT("409.4"),
    NOTEMPTYCONFLICT("409.5"),
    OBSOLETE("410.1");

    private final String value;

    private WebServiceStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString(){
        return value;
    }

}
