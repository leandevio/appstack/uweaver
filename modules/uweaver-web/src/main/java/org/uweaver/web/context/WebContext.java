package org.uweaver.web.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.uweaver.core.app.annotation.AppContext;
import org.uweaver.core.util.Environment;
import org.uweaver.web.MessageResource;

import java.io.IOException;


/**
 * Configuration of web application beans.
 *
 * Usage:
 *      @Import({WebContext.class})
 *
 * Created by jasonlin on 5/1/14.
 */
@AppContext
@Configuration
@ComponentScan(basePackages = {"org.uweaver.web.rest"})
public class WebContext implements ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebContext.class);

    private static ApplicationContext applicationContext = null;
    private static final Environment environment = Environment.getDefaultInstance();

    public WebContext() {
        LOGGER.debug("Establishing Web context ....");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }


    /**
     * According to MultipartResolver's javadoc, the bean name has to be "multipartResolver" to make the MultipartResolver to work.
     * @return
     */
    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver bean = new CommonsMultipartResolver();

        bean.setDefaultEncoding("UTF-8");

        int maxUploadSize = new Integer(environment.property("online.maxUploadSize", "1024"));

        bean.setMaxUploadSize(maxUploadSize*1024);

        return bean;
    }

    /**
     * Configure RequestMapping.
     *
     * 當 Spring MVC 被配置使用 mvc:annotation-driven 時, RequestMappingHandlerMapping 會被登錄到 MVC 的 Context.
     * 這個元件會被用來處理 @RequestMapping 配置的 url routing.
     *
     * @return
     */

    @Bean
    public RequestMappingHandlerMapping handlerMapping() {
        RequestMappingHandlerMapping bean = new RequestMappingHandlerMapping();
        Object[] interceptors = new Object[1];
        interceptors[0] = localeChangeInterceptor();
        bean.setInterceptors(interceptors);
        return bean;
    }

    /**
     * Register a “LocaleChangeInterceptor” interceptor and reference it to any handler mapping that need to supports the multiple languages. The “paramName” is the parameter value that’s used to set the locale.
     * For examples:
     * - welcome.htm?language=en – Get the message from English properties file.
     * - welcome.htm?language=zh_TW – Get the message from Chinese properties file.
     *
     * @return
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor bean = new LocaleChangeInterceptor();
        bean.setParamName("lang");
        return bean;
    }

    @Bean
    public CookieLocaleResolver localeResolver() {
        CookieLocaleResolver bean = new CookieLocaleResolver();
        //bean.setDefaultLocale(new Locale("zh", "TW"));
        return bean;
    }

    @Bean
    public MessageResource messageResource() {
        MessageResource bean = new MessageResource();
        bean.setMessageSource(messageSource());
        return bean;
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource bean = new ResourceBundleMessageSource();
        bean.setFallbackToSystemLocale(false);
        bean.setDefaultEncoding("UTF-8");
        bean.setBasenames("status", "keywords", "messages");
        return bean;
    }

}
