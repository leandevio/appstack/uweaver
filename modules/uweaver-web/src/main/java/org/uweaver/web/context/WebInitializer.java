package org.uweaver.web.context;

/**
 *
 * 取代 web.xml, 配置與登錄 Servlet.
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.uweaver.core.app.App;
import org.uweaver.core.app.annotation.AppContext;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.log.Logger;
import org.uweaver.core.log.LoggerManager;
import org.uweaver.core.reflection.Reflections;
import org.uweaver.core.util.Environment;
import org.uweaver.data.DataInstaller;
import org.uweaver.data.annotation.Entity;
import org.uweaver.servlet.dispatcher.ResourceDispatcher;
import org.uweaver.servlet.dispatcher.RestletDispatcher;
import org.uweaver.servlet.dispatcher.WebResourceDispatcher;
import org.uweaver.servlet.filter.HttpTuner;
import org.uweaver.servlet.filter.SecurityFilter;
import org.uweaver.servlet.filter.ServerController;
import org.uweaver.servlet.restlet.*;

import javax.servlet.*;
import java.util.*;

public class WebInitializer implements WebApplicationInitializer {
    private static final Logger LOGGER = LoggerManager.getLogger(WebInitializer.class);
    Environment environment = Environment.getDefaultInstance();
    private static final Map<String, Class<? extends Restlet>> SYSRESTLETS = new HashMap<>();
    private static final Map<String, Class<? extends Restlet>> USERRESTLETS = new HashMap<>();

    static {
        SYSRESTLETS.put("environment", EnvironmentRestlet.class);
        SYSRESTLETS.put("i18n", I18nRestlet.class);
        USERRESTLETS.put("site", SiteRestlet.class);
        USERRESTLETS.put("applets", AppletRestlet.class);
        USERRESTLETS.put("user", UserRestlet.class);
    }

    public WebInitializer() {
        LOGGER.debug("Initializing Web application ....");
        DataInstaller dataInstaller = new DataInstaller();
        dataInstaller.invalidate();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        WebApplicationContext rootContext = createRootContext(servletContext);

        configureWeb(servletContext, rootContext);

        App app = App.getDefaultInstance();
        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(AppListener.class).list();

        try {
            for(Class type : types) {
                app.subscribe(type.newInstance());
            }
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }

        LOGGER.debug("uWeaver web application is starting up!");

        app.start();


    }

    /**
     * 配置 Application Context.
     * It's WebApplicationContext implementation that looks for Spring configuration in classes annotated with @Configuration annotation.
     *
     * 把 ContextLoaderListener 登錄到 ServletContext.
     *
     * @param servletContext
     * @throws ServletException
     */
    private WebApplicationContext createRootContext(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        List<String> basePackages = new ArrayList<>();
        basePackages.add("org.uweaver.web.controller");

        if(environment.property("service.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("service.package").split(",")));
        }
        if(environment.property("repository.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("repository.package").split(",")));
        }
        if(environment.property("controller.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("controller.package").split(",")));
        }

        if(basePackages.size()>0) {
            rootContext.scan(basePackages.toArray(new String[basePackages.size()]));
        }
        Reflections reflections = Reflections.getDefaultInstance();

        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(AppContext.class).list();

        rootContext.register(types.toArray(new Class[types.size()]));

        rootContext.refresh();

        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.setInitParameter("defaultHtmlEscape", "true");

        return rootContext;
    }

    /**
     * 配置 Servlet Context 與登錄
     *
     * @param servletContext
     * @param rootContext
     */
    private void configureWeb(ServletContext servletContext, WebApplicationContext rootContext) {
        AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();

        webContext.register(WebConfig.class);
        webContext.setParent(rootContext);

        ServletRegistration.Dynamic appServlet = servletContext.addServlet(
                "webapp", new DispatcherServlet(webContext));
        appServlet.setLoadOnStartup(2);
        Set<String> mappingConflicts = appServlet.addMapping("/");

        if (!mappingConflicts.isEmpty()) {
            for (String s : mappingConflicts) {
                LOGGER.error("Mapping conflict: " + s);
            }
            throw new IllegalStateException(
                    "'webapp' cannot be mapped to '/'");
        }

        addServerController(servletContext);
        addSecurityFilter(servletContext);
        addHttpTuner(servletContext);
        addWebResourceDispatcher(servletContext);
        addRestletDispatcher(servletContext);
    }

    private void addServerController(ServletContext servletContext) {
        ServerController serverController = new ServerController();
        serverController.setEnvironment(environment);
        this.addFilter(servletContext, "serverController", serverController, "/*");
    }

    private void addSecurityFilter(ServletContext servletContext) {
        if(!environment.propertyAsBoolean("authorizer.enable", false)) return;
        SecurityFilter securityFilter = new SecurityFilter();
        this.addFilter(servletContext, "securityFilter", securityFilter, "/*");
    }

    private void addHttpTuner(ServletContext servletContext) {
        HttpTuner httpTuner = new HttpTuner();
        this.addFilter(servletContext, "httpTuner", httpTuner, "/*");
    }

    private void addWebResourceDispatcher(ServletContext servletContext) {
        String pathSpec = "/*";

        WebResourceDispatcher dispatcher = new WebResourceDispatcher();
        dispatcher.addResourceHandler("/**/*.{js,html,css,json,properties,woff,woff2,ttf,png}", "/WEB-INF/app");
        this.addFilter(servletContext, "WebResourceDispatcher", dispatcher, pathSpec);
    }

    private void addRestletDispatcher(ServletContext servletContext) {
        String pathSpec = "/*";
        List<String> disabled = Arrays.asList(environment.property("restlet.disable", "").split(","));

        RestletDispatcher dispatcher = new RestletDispatcher();
        String contextPath = environment.property("restlet.contextPath");
        if(contextPath!=null) {
            pathSpec = "/" + contextPath + pathSpec;
        }
        this.addFilter(servletContext, "restletDispatcher", dispatcher, pathSpec);

        dispatcher.addRestlet(new EnvironmentRestlet(environment), "/environment");
        dispatcher.addRestlet(new I18nRestlet(), "/i18n");
        dispatcher.addRestlet(new SessionRestlet(), "/session");

        for(String uri : USERRESTLETS.keySet()) {
            if(disabled.contains(uri)) continue;
            Restlet restlet = createRestlet(USERRESTLETS.get(uri));
            String restletPath = "/" + uri;
            dispatcher.addRestlet(restlet, restletPath);
        }
    }

    private Restlet createRestlet(Class<? extends Restlet> restletClass) {
        Restlet restlet;
        try {
            restlet = restletClass.newInstance();
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }

        return restlet;
    }

    private void addFilter(ServletContext servletContext, String name, Filter filter, String pathSpec) {
        FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(name, filter);
        filterRegistration.addMappingForUrlPatterns(null, true, pathSpec);
    }

}

