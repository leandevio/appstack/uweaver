package org.uweaver.web.handler;

import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.uweaver.core.exception.*;
import org.uweaver.core.util.Converters;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.Item;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by jasonlin on 6/9/14.
 */
public class HandlerExceptionResolverComposite
        extends org.springframework.web.servlet.handler.HandlerExceptionResolverComposite {

    private Converters converters = new Converters();

    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response,
                                         Object handler,
                                         Exception ex) {

        ModelAndView mv = null;
        List<HandlerExceptionResolver> exceptionResolvers = getExceptionResolvers();
        Exception betterException = translateException(ex);
        for(int i=0; i<exceptionResolvers.size()-1; i++) {
            HandlerExceptionResolver resolver = exceptionResolvers.get(i);
            mv = resolver.resolveException(request, response, handler, betterException);
            if(mv!=null) {
                break;
            }
        }

        return mv;
    }

    private Exception translateException(Exception ex) {
        Exception betterException = ex;
        String exceptionName = ex.getClass().getName();

        if(ex instanceof MaxUploadSizeExceededException) {
            long limit = converters.convert(Environment.getDefaultInstance().property("online.maxUploadSize"), Long.class);
            betterException = new UploadSizeExceededException("The uploaded file exceeds the size limit - ${0}.", ex, limit);
        } else if(exceptionName.equals("javax.persistence.OptimisticLockException")) {
            betterException = new VersionConflictException(ex);
        } else if (exceptionName.equals("PersistenceException")) {
            Throwable cause = ex.getCause();
            if(cause.getClass().getSimpleName().equals("ConstraintViolationException")) {
                throw new ReferentialConflictException(ex);
            } else {
                throw new ConflictException(ex);
            }
        }

        return betterException;
    }


    private Exception translatePersistenceException(Exception ex) {
        Exception betterException = ex;
        String exceptionName = ex.getClass().getName();


        return betterException;
    }
/*
    private Throwable findPersistenceException(Throwable ex) {
        Throwable result = null;
        if(ex instanceof PersistenceException) {
            result = ex;
        } else {
            result = findPersistenceException(ex.getCause());
        }
        return result;
    }
*/
}
