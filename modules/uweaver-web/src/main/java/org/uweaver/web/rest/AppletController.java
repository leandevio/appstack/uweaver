/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.web.rest;

import org.springframework.web.bind.annotation.*;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AppletController obj = new AppletController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController(value="__appletController")
@RequestMapping("uw/applets")
public class AppletController {
    private static Environment environment = Environment.getDefaultInstance();
    private JSONParser parser = new JSONParser();

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    String fetch(HttpServletRequest request, HttpServletResponse response) {


        List<Map> applets = readApplets();

        response.setIntHeader("Item-Count", applets.size());

        return parser.writeValueAsString(applets);
    }

    private List<Map> readApplets() {
        Path file = environment.dataFolder().resolve(environment.appName()).resolve("applet.json");
        return Arrays.asList(parser.readValue(file.toFile(), Map[].class));
    }
}
