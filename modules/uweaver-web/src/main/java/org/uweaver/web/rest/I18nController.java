/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2016 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.web.rest;

import org.springframework.web.bind.annotation.*;
import org.uweaver.core.json.JSONParser;
import org.uweaver.i18n.I18n;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * I18nController obj = new I18nController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController(value="__i18nController")
@RequestMapping("uw/i18n")
public class I18nController {
    private JSONParser parser = new JSONParser();
    private I18n i18n = new I18n();

    @RequestMapping(value = "/terms/{type}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String getTerms(@PathVariable String type) {
        return i18n.getTermsAsJSON(I18n.TermType.valueOf(type.toUpperCase()));
    }
}
