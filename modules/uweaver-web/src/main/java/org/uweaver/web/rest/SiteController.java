/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.web.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uweaver.core.json.JSONParser;
import org.uweaver.core.util.Environment;

import java.io.*;
import java.nio.file.Path;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SiteController obj = new SiteController();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
@RestController(value="__siteController")
@RequestMapping("uw/site")
public class SiteController {
    private static JSONParser parser = new JSONParser();
    private static Environment environment = Environment.getDefaultInstance();

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    String fetch() {
        return parser.writeValueAsString(readSite());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody String get(@PathVariable String id) {
        return parser.writeValueAsString(readSite());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody String save(@PathVariable String id, @RequestParam("payload") String payload, @RequestParam(value="attachments", required=false) MultipartFile attachments) throws Exception {
        Map<String, Object> site = parser.readValue(payload, Map.class);
        byte[] bytes = (attachments==null) ? null : attachments.getBytes();
        writeSite(site, bytes);
        return parser.writeValueAsString(readSite());
    }

    @RequestMapping(value = "/logo", method = RequestMethod.GET)
    public HttpEntity<byte[]> logo() throws IOException {
        HttpHeaders header = new HttpHeaders();
        Map<String, Object> site = readSite();
        Map<String, Object> logo = (Map<String, Object>) site.get("logo");
        File image = new File(environment.dataFolder() + File.separator + logo.get("name"));
        FileInputStream in = new FileInputStream(image);
        byte[] bytes = new byte[in.available()];
        in.read(bytes);
        in.close();

        String[] tokens = ((String) logo.get("type")).split("/");
        header.setContentType(new MediaType(tokens[0], tokens[1]));

        header.setContentLength(bytes.length);

        return new HttpEntity<>(bytes, header);
    }


    private Map<String, Object> readSite() {
        Path file = environment.dataFolder().resolve(environment.appName()).resolve("site.json");
        Map<String, Object> site = parser.readValue(file.toFile(), Map.class);
        return site;
    }

    private void writeSite(Map<String, Object> site, byte[] bytes) throws Exception {
        Map<String, Object> logo = (Map<String, Object>) site.get("logo");
        logo.put("url", "site/logo/");
        String json = parser.writeValueAsString(site);
        Path file = environment.dataFolder().resolve(environment.appName()).resolve("site.json");
        FileWriter writer = new FileWriter(file.toFile());
        writer.write(json);
        writer.close();

        if(bytes==null) return;
        Path image = environment.dataFolder().resolve(environment.appName()).resolve((String) logo.get("name"));
        FileOutputStream out = new FileOutputStream(image.toFile());
        out.write(bytes);
        out.close();
    }
}
