/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.applet;
import org.uweaver.zk.widget.AbstractHtmlWrapper;
import org.uweaver.zk.widget.View;

import java.util.HashMap;
import java.util.Map;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Applet obj = new Applet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public abstract class AbstractApplet extends AbstractHtmlWrapper<View> implements Applet {
    protected String title = "";
    protected String icon = "gear";

    public AbstractApplet() {
        this(new View());
    }

    public AbstractApplet(View view) {
        super(view);
    }

    @Override
    public void focus() {
        this.trigger("onFocus", null);
    }

    @Override
    public String title() {
        return this.title;
    }

    @Override
    public String setTitle(String title) {
        return this.title = title;
    }

    @Override
    public String icon() {
        return this.icon;
    }

    @Override
    public String setIcon(String icon) {
        return this.icon = icon;
    }

    @Override
    public void activate(Map<String, Object> args) {

    }

    @Override
    public void activate() {
        activate(new HashMap<>());
    }

    @Override
    public Applet navigate(String uri) {
        return router().navigate(uri);
    }
}
