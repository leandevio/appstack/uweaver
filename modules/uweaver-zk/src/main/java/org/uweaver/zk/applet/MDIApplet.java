package org.uweaver.zk.applet;

import org.uweaver.zk.widget.Pane;
import org.uweaver.zk.widget.Panel;

import java.util.*;

public class MDIApplet extends AbstractApplet {
    Pane pane;
    Map<Gadget, Panel> panels = new HashMap<>();

    public MDIApplet() {
        super();
        pane = new Pane();
        append(pane);
    }

    public void add(Gadget gadget) {
        add(gadget, false);
    }

    public void add(Gadget gadget, boolean closable) {
        Panel panel = new Panel();
        panel.setFloatable(true);
        panel.setCollapsible(true);
        panel.setClosable(closable);
        panel.setMovable(true);
        panel.setSizable(true);
        panel.setTitle(gadget.title());
        panel.append(gadget);
        pane.append(panel);
        panels.put(gadget, panel);
    }

    public void remove(Gadget gadget) {
        pane.removeChild(panels.get(gadget));
        panels.remove(gadget);
    }

    public List<Gadget> gadgets() {
        List<Gadget> list = new ArrayList<>();
        for(Gadget gadget : panels.keySet()) {
            list.add(gadget);
        }
        return list;
    }

    public Panel panel(Gadget gadget) {
        return panels.get(gadget);
    }
}
