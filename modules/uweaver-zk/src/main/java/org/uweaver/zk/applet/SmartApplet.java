/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.applet;

import org.uweaver.zk.widget.TabPanel;
import org.zkoss.zk.ui.Component;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * DynaApplet obj = new DynaApplet();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class SmartApplet extends AbstractApplet {
    TabPanel tabPanel;

    public SmartApplet() {
        super();
        tabPanel = new TabPanel();
        tabPanel.addEventListener("onSelect", event -> this.trigger("onSelect", event.getData()));
        tabPanel.addEventListener("onClose", event -> this.remove((Gadget) event.getData()));
        append(tabPanel);
    }

    public void add(Gadget gadget, boolean closable) {
        tabPanel.add(gadget, gadget.title(), closable);
        gadget.addEventListener("onClose", event -> remove((Gadget) event.getTarget()));
    }

    public void select(int index) {
        this.tabPanel.select(index);
    }

    public void select(Gadget component) {
        this.tabPanel.select(component);
    }

    public Gadget get(int index) {
        return (Gadget) this.tabPanel.get(index);
    }

    public void remove(Gadget gadget) {
        tabPanel.remove(gadget);
        this.trigger("onClose", gadget);
    }

    public void add(Gadget gadget) {
        add(gadget, false);
    }
}
