package org.uweaver.zk.calendar;

import org.uweaver.schedule.Activity;
import org.uweaver.schedule.Schedules;
import org.uweaver.zk.widget.AbstractWrapper;
import org.uweaver.zk.widget.Wrapper;
import org.zkoss.calendar.event.CalendarsEvent;
import org.zkoss.calendar.impl.SimpleCalendarModel;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

public class Calendar extends AbstractWrapper<org.zkoss.calendar.Calendars> implements Wrapper {
    private Schedules schedules = new Schedules();

    public Calendar() {
        this(new org.zkoss.calendar.Calendars());
    }

    public Calendar(org.zkoss.calendar.Calendars calendars) {
        super(calendars);
        this.component.addEventListener("onEventCreate", this::onEventCreate);
        this.component.addEventListener("onEventEdit", this::onEventEdit);
        this.component.addEventListener("onEventUpdate", this::onEventUpdate);
    }

    private void onEventCreate(Event event) {
        CalendarsEvent calendarsEvent = (CalendarsEvent) event;
        Date startTime = calendarsEvent.getBeginDate();
        Date endTime = calendarsEvent.getEndDate();
        CalendarEvent calendarEvent = new CalendarEvent("onEventCreate", this, null, null, startTime, endTime);
        Events.postEvent(calendarEvent);
    }

    private void onEventEdit(Event event) {
        CalendarsEvent calendarsEvent = (CalendarsEvent) event;
        Date startTime = calendarsEvent.getBeginDate();
        Date endTime = calendarsEvent.getEndDate();
        Activity activity = ((ZKCalendarEvent) calendarsEvent.getCalendarEvent()).getActivity();
        CalendarEvent calendarEvent = new CalendarEvent("onEventEdit", this, null, activity, startTime, endTime);
        Events.postEvent(calendarEvent);

    }

    private void onEventUpdate(Event event) {
        CalendarsEvent calendarsEvent = (CalendarsEvent) event;
        Date startTime = calendarsEvent.getBeginDate();
        Date endTime = calendarsEvent.getEndDate();
        Activity activity = ((ZKCalendarEvent) calendarsEvent.getCalendarEvent()).getActivity();
        CalendarEvent calendarEvent = new CalendarEvent("onEventUpdate", this, null, activity, startTime, endTime);
        Events.postEvent(calendarEvent);
    }

    @Override
    public void setMold(String mold) {
        if(mold.equalsIgnoreCase("month")) {
            this.component.setMold("month");
        } else {
            this.component.setMold("default");
        }
        this.render();
    }

    public String getMold() {
        return this.component.getMold();
    }

    public void setDays(int days) {
        this.component.setDays(days);
        this.render();
    }

    public int getDays() {
        return this.component.getDays();
    }

    public void setCurrentDate(Date currentDate) {
        this.component.setCurrentDate(currentDate);
        this.render();
    }

    public Date getCurrentDate() {
        return this.component.getCurrentDate();
    }

    public Date getStartDate() {
        return this.component.getBeginDate();
    }

    public Date getEndDate() {
        return this.component.getEndDate();
    }

    public TimeZone getTimeZone() {
        return this.component.getDefaultTimeZone();
    }

    public void setTimeZone(TimeZone timeZone) {
        this.component.setTimeZone(timeZone.toZoneId().toString());
    }

    public void previous() {
        ZoneId zone = this.component.getDefaultTimeZone().toZoneId();
        Date date = this.component.getCurrentDate();
        LocalDateTime dateTime = date.toInstant().atZone(zone).toLocalDateTime();
        LocalDateTime current;
        if(this.getMold().equalsIgnoreCase("month")) {
            current = dateTime.minusMonths(1);
        } else {
            current = dateTime.minusDays(this.getDays());
        }
        Date currentDate = Date.from(current.atZone(zone).toInstant());
        this.component.setCurrentDate(currentDate);
    }

    public void next() {
        ZoneId zone = this.component.getDefaultTimeZone().toZoneId();
        Date date = this.component.getCurrentDate();
        LocalDateTime dateTime = date.toInstant().atZone(zone).toLocalDateTime();
        LocalDateTime current;
        if(this.getMold().equalsIgnoreCase("month")) {
            current = dateTime.plusMonths(1);
        } else {
            current = dateTime.plusDays(this.getDays());
        }
        Date currentDate = Date.from(current.atZone(zone).toInstant());
        this.component.setCurrentDate(currentDate);
    }

    @Override
    public Calendar render() {
        org.zkoss.calendar.Calendars calendars = this.component();
        SimpleCalendarModel scm = new SimpleCalendarModel();
        for(Activity activity : schedules.expand(this.getEndDate())) {
            scm.add(toZKCalendarEvent(activity));
        }
        calendars.setModel(scm);
        return this;
    }
    @Override
    public void focus() {}

    @Override
    public Object getWidth() {
        return this.component.getWidth();
    }

    @Override
    public Object getHeight() {
        return this.component.getHeight();
    }

    public void setData(Schedules schedules) {
        this.schedules.clear();
        this.schedules.addAll(schedules);
        this.render();
    }

    private ZKCalendarEvent toZKCalendarEvent(Activity activity) {
        ZKCalendarEvent zkCalendarEvent = new ZKCalendarEvent();
        zkCalendarEvent.setBeginDate(activity.getStartDate());
        zkCalendarEvent.setEndDate(activity.getEndDate());
        zkCalendarEvent.setContent(activity.getTitle());
        zkCalendarEvent.setHeaderColor(activity.getColor());
        zkCalendarEvent.setContentColor(activity.getColor());
        zkCalendarEvent.setLocked(false);
        zkCalendarEvent.setActivity(activity);
        return zkCalendarEvent;
    }
}
