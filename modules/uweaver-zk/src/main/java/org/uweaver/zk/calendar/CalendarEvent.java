package org.uweaver.zk.calendar;

import org.uweaver.schedule.Activity;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

import java.util.Date;

public class CalendarEvent extends Event {
    private Date startTime = null;
    private Date endTime = null;
    private Activity activity = null;

    public CalendarEvent(String name) {
        super(name);
    }

    public CalendarEvent(String name, Component target) {
        super(name, target);
    }

    public CalendarEvent(String name, Component target, Object data) {
        super(name, target, data);
    }

    public CalendarEvent(String name, Component target, Object data, Activity activity, Date startTime, Date endTime) {
        super(name, target, data);
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Activity getActivity() {
        return this.activity;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }
}
