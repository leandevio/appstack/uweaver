package org.uweaver.zk.chart;

import org.uweaver.core.util.Environment;
import org.uweaver.i18n.I18n;
import org.uweaver.zk.router.Router;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;

import java.util.Locale;

public abstract class AbstractAxis<T extends org.zkoss.chart.Axis> implements Axis {
    protected T component;
    private Scale scale;
    protected I18n i18n = new I18n(locale());

    public AbstractAxis(T component) {
        this.component = component;
        setScale(new LinearScale());
    }

    public T component() {
        return this.component;
    }

    @Override
    public void setScale(Scale scale) {
        this.scale = scale;
        this.component.setType(scale.component());
    }

    @Override
    public Scale scale() {
        return scale;
    }

    @Override
    public String title() {
        return this.component.getTitle().getText();
    }

    @Override
    public void setTitle(String title) {
        this.component.setTitle(translate(title));
    }

    @Override
    public Locale locale() {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        return router.locale();
    }

    @Override
    public String translate(String text) {
        return i18n.translate(text);
    }


}
