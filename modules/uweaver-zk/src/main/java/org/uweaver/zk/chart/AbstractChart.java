package org.uweaver.zk.chart;

import org.uweaver.zk.widget.AbstractWrapper;
import org.uweaver.zk.widget.Wrapper;
import org.zkoss.chart.Charts;
public class AbstractChart<T extends Charts> extends AbstractWrapper<T> implements Chart, Wrapper {

    public AbstractChart(T charts) {
        super(charts);
    }

    public AbstractChart render() {
        return this;
    }

    @Override
    public void focus() {

    }

    public void addSeries(Series series) {
        this.component.addSeries(toNativeSeries(series));
    }

    private org.zkoss.chart.Series toNativeSeries(Series series) {
        org.zkoss.chart.Series nativeSeries = new org.zkoss.chart.Series();

        nativeSeries.setName(series.name());
        return nativeSeries;
    }

    public T component() {
        return this.component;
    }

    @Override
    public void setWidth(Number width) {
        this.component.setWidth(width);
    }

    @Override
    public void setHeight(Number height) {
        this.component.setHeight(height);
    }

    @Override
    public Number getWidth() {
        return this.component.getWidth();
    }

    @Override
    public Number getHeight() {
        return this.component.getHeight();
    }

    @Override
    public void setTitle(String title) {
        this.component.setTitle(title);
    }
}
