package org.uweaver.zk.chart;

import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Environment;
import org.uweaver.i18n.I18n;
import org.uweaver.search.Predicate;
import org.uweaver.zk.router.Router;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class AbstractSeries<T extends org.zkoss.chart.Series> implements Series {
    protected Collection data = new Collection();
    protected List<Predicate> predicates = new ArrayList<>();
    protected T component;
    protected I18n i18n = new I18n(locale());


    public AbstractSeries(String name) {
        this.component = (T) new org.zkoss.chart.Series();
        setName(name);
    }

    public T component() {
        return this.component;
    }

    @Override
    public String name() {
        return this.component.getName();
    }

    @Override
    public void setName(String name) {
        this.component.setName(translate(name));
    }

    @Override
    public void setData(Collection data, Predicate predicate) {
        this.data = data;
        setPredicate(predicate);
    }

    @Override
    public void setData(Collection data) {
        this.data = data;
        predicates.clear();
        render();
    }

    @Override
    public void setPredicate(Predicate predicate) {
        predicates.clear();
        predicates.add(predicate);
        render();
    }

    @Override
    public Collection projection() {
        return data.filter(predicates);
    }


    @Override
    public Locale locale() {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        return router.locale();
    }

    @Override
    public String translate(String text) {
        return i18n.translate(text);
    }
}
