package org.uweaver.zk.chart;

public class AngularAxis extends AbstractAxis<org.zkoss.chart.XAxis> {

    public AngularAxis() {
        super(new org.zkoss.chart.XAxis());
    }
}
