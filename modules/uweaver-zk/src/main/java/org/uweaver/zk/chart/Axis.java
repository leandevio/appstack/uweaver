package org.uweaver.zk.chart;

import java.util.Locale;

public interface Axis {

    void setScale(Scale scale);

    Scale scale();

    String title();

    void setTitle(String title);

    Locale locale();

    String translate(String text);
}
