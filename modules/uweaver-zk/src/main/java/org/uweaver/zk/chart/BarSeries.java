package org.uweaver.zk.chart;

import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Model;

import java.util.Date;

public class BarSeries extends AbstractSeries<org.zkoss.chart.Series> {
    private String xField, yField;

    public BarSeries(String name, String xField, String yField) {
        super(name);
        org.zkoss.chart.Series series = this.component;
        series.setType("bar");
        this.xField = xField;
        this.yField = yField;
    }

    @Override
    public BarSeries render() {
        org.zkoss.chart.Series series = this.component;
        series.remove();
        Collection projection = this.projection();
        for(Model datum : projection) {
            Object xValue = datum.get(xField);
            Number yValue = datum.getAsNumber(yField);
            if(xValue instanceof Number) {
                series.addPoint((Number) xValue, yValue);
            } else if(xValue instanceof Date) {
                Number value = (xValue==null) ? null : ((Date) xValue).getTime();
                series.addPoint(value, yValue);
            } else if(xValue instanceof String) {
                series.addPoint((String) xValue, yValue);
            } else {
                series.addPoint(xValue==null ? null : xValue.toString(), yValue);
            }
        }
        return this;
    }
}
