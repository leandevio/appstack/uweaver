package org.uweaver.zk.chart;


import org.uweaver.core.util.Collection;
import org.uweaver.search.Predicate;
import org.zkoss.chart.Charts;

import java.util.ArrayList;
import java.util.List;

public class CartesianChart extends AbstractChart<org.zkoss.chart.Charts> {
    private Collection data = new Collection();
    private List<Series> series = new ArrayList<>();
    private XAxis xAxis = new XAxis();
    private YAxis yAxis = new YAxis();

    public CartesianChart() {
        this(new Charts());
    }

    public CartesianChart(Charts charts) {
        super(charts);
    }

    @Override
    public CartesianChart render() {
        Charts chart = this.component();
        chart.setXAxis(xAxis.component());
        chart.setYAxis(yAxis.component());

        return this;
    }

    public void setData(Collection data) {
        this.data = data;
        render();
    }

    public LineSeries addLineSeries(String name, String xField, String yField) {
        return addLineSeries(name, xField, yField, null);
    }

    public LineSeries addLineSeries(String name, String xField, String yField, Predicate predicate) {
        LineSeries series = new LineSeries(name, xField, yField);
        series.setData(this.data, predicate);
        this.series.add(series);
        this.component.addSeries(series.component());
        return series;
    }

    public BarSeries addBarSeries(String name, String xField, String yField) {
        return addBarSeries(name, xField, yField, null);
    }

    public BarSeries addBarSeries(String name, String xField, String yField, Predicate predicate) {
        BarSeries series = new BarSeries(name, xField, yField);
        series.setData(this.data, predicate);
        this.series.add(series);
        this.component.addSeries(series.component());
        return series;
    }

    public ColumnSeries addColumnSeries(String name, String xField, String yField) {
        return addColumnSeries(name, xField, yField, null);
    }

    public ColumnSeries addColumnSeries(String name, String xField, String yField, Predicate predicate) {
        ColumnSeries series = new ColumnSeries(name, xField, yField);
        series.setData(this.data, predicate);
        this.series.add(series);
        this.component.addSeries(series.component());
        return series;
    }

    public XAxis xAxis() {
        return this.xAxis;
    }

    public YAxis yAxis() {
        return this.yAxis;
    }

    public void setStacking(Stacking stacking) {
        String option = null;

        if(stacking.equals(Stacking.NORMAL)) {
            option = "normal";
        } else if(stacking.equals(Stacking.PERCENT)) {
            option = "percent";
        }

        this.component.getPlotOptions().getColumn().setStacking(option);
    }
}
