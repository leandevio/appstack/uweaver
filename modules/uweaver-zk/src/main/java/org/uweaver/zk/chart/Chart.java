package org.uweaver.zk.chart;

import org.uweaver.zk.widget.Widget;

public interface Chart extends Widget {
    void setWidth(Number width);

    void setHeight(Number height);

    Number getWidth();

    Number getHeight();

    void setTitle(String title);

    enum Stacking {
        NORMAL, PERCENT
    }

}
