package org.uweaver.zk.chart;

public class LinearScale implements Scale {
    @Override
    public String component() {
        return "linear";
    }
}
