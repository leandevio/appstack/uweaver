package org.uweaver.zk.chart;

import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Model;

public class PieSeries extends AbstractSeries<org.zkoss.chart.Series> {
    private String angularField, labelField;

    public PieSeries(String name, String angularField, String labelField) {
        super(name);
        org.zkoss.chart.Series series = this.component;
        series.setType("pie");
        this.angularField = angularField;
        this.labelField = labelField;
    }

    @Override
    public PieSeries render() {
        org.zkoss.chart.Series series = this.component;
        series.remove();
        Collection projection = this.projection();
        for(Model datum : projection) {
            Object label = datum.get(labelField);
            Number angularValue = datum.getAsNumber(angularField);
            if(label instanceof String) {
                series.addPoint((String) label, angularValue);
            } else {
                series.addPoint(label==null ? null : label.toString(), angularValue);
            }
        }
        return this;
    }
}
