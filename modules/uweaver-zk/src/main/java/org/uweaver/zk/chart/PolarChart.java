package org.uweaver.zk.chart;

import org.uweaver.core.util.Collection;
import org.uweaver.search.Predicate;
import org.zkoss.chart.Charts;

import java.util.ArrayList;
import java.util.List;

public class PolarChart extends AbstractChart<org.zkoss.chart.Charts> {
    private Collection data = new Collection();
    private List<Series> series = new ArrayList<>();
    private AngularAxis angularAxis = new AngularAxis();
    private RadicalAxis radicalAxis = new RadicalAxis();

    public PolarChart() {
        this(new Charts());
    }

    public PolarChart(Charts charts) {
        super(charts);
    }

    @Override
    public PolarChart render() {
        Charts chart = this.component();
        chart.setXAxis(angularAxis.component());
        chart.setYAxis(radicalAxis.component());

        return this;
    }

    public void setData(Collection data) {
        this.data = data;
        render();
    }

    public PieSeries addPieSeries(String name, String angularField, String labelField) {
        return addPieSeries(name, angularField, labelField, null);
    }

    public PieSeries addPieSeries(String name, String angularField, String labelField, Predicate predicate) {
        PieSeries series = new PieSeries(name, angularField, labelField);
        series.setData(this.data, predicate);
        this.series.add(series);
        this.component.addSeries(series.component());
        return series;
    }

    public void setDonut(double percent) {
        String size = percent*100 + "%";
        this.component.getPlotOptions().getPie().setInnerSize(size);
    }

    public AngularAxis angularAxis() {
        return this.angularAxis;
    }

    public RadicalAxis radicalAxis() {
        return this.radicalAxis;
    }
}
