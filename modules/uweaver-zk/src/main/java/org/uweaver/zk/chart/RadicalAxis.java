package org.uweaver.zk.chart;

public class RadicalAxis extends AbstractAxis<org.zkoss.chart.YAxis> {

    public RadicalAxis() {
        super(new org.zkoss.chart.YAxis());
    }
}
