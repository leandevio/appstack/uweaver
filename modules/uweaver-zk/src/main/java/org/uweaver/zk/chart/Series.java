package org.uweaver.zk.chart;

import org.uweaver.core.util.Collection;
import org.uweaver.search.Predicate;

import java.util.Locale;

public interface Series {
    String name();

    void setName(String name);

    void setData(Collection data, Predicate predicate);

    void setData(Collection data);

    void setPredicate(Predicate predicate);

    Collection projection();

    Series render();

    Locale locale();

    String translate(String text);
}
