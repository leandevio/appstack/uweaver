package org.uweaver.zk.chart;

public class XAxis extends AbstractAxis<org.zkoss.chart.XAxis> {

    public XAxis() {
        super(new org.zkoss.chart.XAxis());
    }
}
