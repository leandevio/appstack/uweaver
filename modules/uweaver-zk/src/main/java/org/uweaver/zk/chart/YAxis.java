package org.uweaver.zk.chart;

public class YAxis extends AbstractAxis<org.zkoss.chart.YAxis> {

    public YAxis() {
        super(new org.zkoss.chart.YAxis());
    }
}
