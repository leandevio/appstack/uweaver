package org.uweaver.zk.context;

/**
 *
 * 取代 web.xml, 配置與登錄 Servlet.
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */

import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.uweaver.core.app.App;
import org.uweaver.core.app.annotation.AppContext;
import org.uweaver.core.app.annotation.AppListener;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.log.Logger;
import org.uweaver.core.log.LoggerManager;
import org.uweaver.core.reflection.Reflections;
import org.uweaver.core.util.Environment;
import org.uweaver.data.DataInstaller;
import org.uweaver.jpa.context.JpaContext;
import org.uweaver.zk.filter.ServerController;
import org.zkoss.zk.au.http.DHtmlUpdateServlet;
import org.zkoss.zk.ui.http.DHtmlLayoutServlet;
import org.zkoss.zk.ui.http.HttpSessionListener;

import javax.servlet.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WebInitializer implements WebApplicationInitializer {
    private static final Logger LOGGER = LoggerManager.getLogger(WebInitializer.class);
    Environment environment = Environment.getDefaultInstance();


    public WebInitializer() {
        LOGGER.debug("Initializing the uweaver-zk application ....");
        DataInstaller dataInstaller = new DataInstaller();
        dataInstaller.invalidate();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        WebApplicationContext rootContext = createRootContext(servletContext);

        configureWeb(servletContext, rootContext);

        App app = App.getDefaultInstance();
        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> types = reflections.createTypeQuery().annotatedWith(AppListener.class).list();

        try {
            for(Class type : types) {
                app.subscribe(type.newInstance());
            }
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }

        LOGGER.debug("The uweaver-zk application is starting up!");

        app.start();
    }

    /**
     * 配置 Application Context.
     * It's WebApplicationContext implementation that looks for Spring configuration in classes annotated with @Configuration annotation.
     *
     * 把 ContextLoaderListener 登錄到 ServletContext.
     *
     * @param servletContext
     * @throws ServletException
     */
    private WebApplicationContext createRootContext(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        List<String> basePackages = new ArrayList<>();
        Reflections reflections = Reflections.getDefaultInstance();
        List<Class<?>> contexts = reflections.createTypeQuery().annotatedWith(AppContext.class).list();
        Class appConfig = environment.appConfig();

        contexts.add(JpaContext.class);
        if(appConfig!=null) {
            basePackages.addAll(parseRepositoryPackages(appConfig));
            basePackages.addAll(parseServicePackages(appConfig));
            JpaContext.addPackagesToScan(parseEntityPackages(appConfig));
            contexts.add(appConfig);
        }

        basePackages.add("org.uweaver.repository");
        if(environment.property("repository.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("repository.package").split(",")));
        }

        basePackages.add("org.uweaver.service");
        if(environment.property("service.package")!=null) {
            basePackages.addAll(Arrays.asList(environment.property("service.package").split(",")));
        }


        if(basePackages.size()>0) {
            rootContext.scan(basePackages.toArray(new String[basePackages.size()]));
        }

        rootContext.register(contexts.toArray(new Class[contexts.size()]));

        rootContext.refresh();

        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.setInitParameter("defaultHtmlEscape", "true");

        return rootContext;
    }

    private List<String> parseEntityPackages(Class<?> config) {
        List<String> packages = new ArrayList<>();
        try {
            Method method = config.getMethod("entityPackages");
            packages.addAll((List<String>) method.invoke(null));
        } catch (NoSuchMethodException e) {
            ;
        } catch (IllegalAccessException e) {
            ;
        } catch (InvocationTargetException e) {
            ;
        }

        Import annotation = config.getAnnotation(Import.class);
        if(annotation!=null) {
            Class<?>[] dependencies = annotation.value();
            for(Class<?> dependency : dependencies) {
                packages.addAll(parseEntityPackages(dependency));
            }
        }

        return packages;
    }

    private List<String> parseRepositoryPackages(Class<?> config) {
        List<String> packages = new ArrayList<>();
        try {
            Method method = config.getMethod("repositoryPackages");
            packages.addAll((List<String>) method.invoke(null));
        } catch (NoSuchMethodException e) {
            ;
        } catch (IllegalAccessException e) {
            ;
        } catch (InvocationTargetException e) {
            ;
        }

        Import annotation = config.getAnnotation(Import.class);
        if(annotation!=null) {
            Class<?>[] dependencies = annotation.value();
            for(Class<?> dependency : dependencies) {
                packages.addAll(parseRepositoryPackages(dependency));
            }
        }

        return packages;
    }

    private List<String> parseServicePackages(Class<?> config) {
        List<String> packages = new ArrayList<>();
        try {
            Method method = config.getMethod("servicePackages");
            packages.addAll((List<String>) method.invoke(null));
        } catch (NoSuchMethodException e) {
            ;
        } catch (IllegalAccessException e) {
            ;
        } catch (InvocationTargetException e) {
            ;
        }

        Import annotation = config.getAnnotation(Import.class);
        if(annotation!=null) {
            Class<?>[] dependencies = annotation.value();
            for(Class<?> dependency : dependencies) {
                packages.addAll(parseServicePackages(dependency));
            }
        }

        return packages;
    }


    /**
     * 配置 Servlet Context 與登錄
     *
     * @param servletContext
     * @param rootContext
     */
    private void configureWeb(ServletContext servletContext, WebApplicationContext rootContext) {
        AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();

        webContext.setParent(rootContext);

        // ZK listener for session cleanup
        servletContext.addListener(new HttpSessionListener());

        // ZK loader for ZUML pages
        ServletRegistration.Dynamic zkLoader = servletContext.addServlet("zkLoader", new DHtmlLayoutServlet());
        zkLoader.setInitParameter("update-uri", "/zkau");
        zkLoader.setLoadOnStartup(1);
        zkLoader.addMapping("*.zul");

        // The asynchronous update engine for ZK
        ServletRegistration.Dynamic auEngine = servletContext.addServlet("auEngine", new DHtmlUpdateServlet());
        zkLoader.addMapping("/zkau/*");

        addServerController(servletContext);
    }

    private void addFilter(ServletContext servletContext, String name, Filter filter, String pathSpec) {
        FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(name, filter);
        filterRegistration.addMappingForUrlPatterns(null, true, pathSpec);
    }

    private void addServerController(ServletContext servletContext) {
        ServerController serverController = new ServerController();
        serverController.setEnvironment(environment);
        this.addFilter(servletContext, "serverController", serverController, "/*");
    }
}

