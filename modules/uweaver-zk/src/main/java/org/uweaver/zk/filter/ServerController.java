/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.filter;

import org.uweaver.core.cipher.Encryptor;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.security.Authorizer;
import org.uweaver.security.Pass;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.zkoss.web.Attributes;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * SecurityFilter obj = new SecurityFilter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ServerController implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(ServerController.class);
    private static Environment environment = Environment.getDefaultInstance();
    private Encryptor encryptor;
    private static Pattern STATICCONTENTREGEXP = Pattern.compile("/.*\\.(png|gif|jpg|html|js|css|json|properties|woff|woff2|tiff|ttf)$");

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encryptor = new Encryptor(Encryptor.Algorithm.DES, environment.property("encryptor.key", "0000"));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();

        initLocale(request);

        String servletPath = request.getServletPath();
        if(isStaticContent(servletPath)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String credential = request.getParameter("credential");
        if(credential!=null) {
            Pass newPass = resolveCredential(credential);
            Pass pass = (Pass) session.getAttribute("pass");
            if(newPass==null||pass==null||!newPass.username().equals(pass.username())) {
                session.invalidate();
                session = request.getSession(true);
                session.setAttribute("pass", newPass);
            }
        }

        Pass pass = (Pass) session.getAttribute("pass");
        if(pass==null) {
            if(servletPath.endsWith(".zul")) {
                if(!servletPath.equals("/index.zul")) {
                    bootstrap(request, response);
                    return;
                }
            } else if(servletPath.equals("/zkau")) {
                String pathInfo = request.getPathInfo();
                if(pathInfo!=null&&pathInfo.endsWith(".zul")){
                    bootstrap(request, response);
                    return;
                }
            } else {
                bootstrap(request, response);
                return;
            }
        }

        setResponseHeader(response);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void initLocale(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if(session.getAttribute("locale")!=null) return;

        String languages = request.getHeader("accept-language");
        Locale locale = environment.locale();
        if(languages!=null) {
            String language = languages.split(",")[0];
            locale = Locale.forLanguageTag(language);
        }
        session.setAttribute("locale", locale);
        session.setAttribute(Attributes.PREFERRED_LOCALE, locale);
    }

    private Pass resolveCredential(String credential) {
        Pass pass = null;
        try {
            String username = encryptor.decipher(credential);
            pass = Authorizer.getDefaultInstance().getPass(username);
        } catch(Exception e) {
            LOGGER.warn(e);
        }

        return pass;
    }

    @Override
    public void destroy() {

    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    private void setResponseHeader(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Expires", "0"); // Proxies.
    }

    private void bootstrap(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String contextPath = request.getServletContext().getContextPath();
        setResponseHeader(response);
        response.sendRedirect(contextPath + "/");
    }

    private boolean isStaticContent(String uri) {
        Matcher matcher = STATICCONTENTREGEXP.matcher(uri);
        return matcher.find();
    }

}
