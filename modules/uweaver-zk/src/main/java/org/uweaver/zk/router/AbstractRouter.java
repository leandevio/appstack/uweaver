/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.router;

import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.core.util.Environment;
import org.uweaver.security.Pass;
import org.uweaver.zk.widget.Pane;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.theme.Themes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AbstractRouter obj = new AbstractRouter();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public abstract class AbstractRouter extends Pane implements Router {
    private static final Logger LOGGER = LogManager.getLogger(AbstractRouter.class);
    protected Pass pass;
    protected Locale locale;

    public AbstractRouter() {
        Path.getComponent("/").setAttribute("router", this);
        Locale locale = (Locale) Executions.getCurrent().getSession().getAttribute("locale");
        this.locale = (locale==null) ? Environment.getDefaultInstance().locale() : locale;
    }

    @Override
    public boolean isAuthenticated() {
        return this.pass()!=null;
    }

    @Override
    public Pass pass() {
        return this.pass;
    }

    @Override
    public void setPass(Pass pass) {
        this.pass = pass;

    }

    @Override
    public void setTheme(String theme) {
        Themes.setTheme(Executions.getCurrent(), theme);
        Executions.sendRedirect("");
    }

    @Override
    public String getTheme() {
        return Themes.getCurrentTheme();
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
        Executions.sendRedirect("");
    }

    @Override
    public Locale locale() {
        return this.locale;
    }

    @Override
    public org.uweaver.zk.applet.Applet navigate(String uri) {
        return this.navigate(uri, new HashMap<>());
    }

    @Override
    public Pane dock(Runnable action, Map<String, Object> options) {
        return null;
    }

    @Override
    public Pane dock(Supplier<Map<String, Object>> data, boolean repeat, Map<String, Object> options) {
        return null;
    }

    @Override
    public void undock(Pane pane) {

    }

    protected void calibrate() {
        try(InputStream inputStream = Workbench.class.getResourceAsStream("/org/uweaver/zk/router/calibrate.js");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String script = reader.lines().collect(Collectors.joining(System.lineSeparator()));
            reader.close();
            inputStream.close();
            Clients.evalJavaScript(script);
        } catch (IOException e) {
            LOGGER.debug(e);
        }
    }
}
