/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.router;

import org.uweaver.core.util.Model;
import org.uweaver.security.Authorizer;
import org.uweaver.security.Pass;
import org.uweaver.zk.util.ComponentBuilder;
import org.uweaver.zk.util.UIMLResourceBundle;
import org.uweaver.zk.widget.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Logon obj = new Logon();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Logon extends Pane {
    private UIMLResourceBundle resources = new UIMLResourceBundle(Logon.class);
    private ComponentBuilder builder = new ComponentBuilder();
    private Form form;
    private Authorizer authorizer = Authorizer.getDefaultInstance();

    public Logon() {
        super();
        String zuml = resources.get("main");
        Component component = builder.build(zuml);
        this.append(component);

        Trigger criteriaTrigger = query(".trigger", Trigger.class);
        criteriaTrigger.addEventListener("onLogin", this::login);
        this.form = query("grid", Form.class);
        this.form.addEventListener("onOK", this::login);
    }

    private void login(Event event) {
        if(!this.form.validate()) {
            Popup.warn(form.errorMessage());
            return;
        }

        Model data = this.form.data();
        String username = data.getAsString("username");
        String password = data.getAsString("password");

        Pass pass = authorizer.signin(username, password);
        if(pass==null){
            Popup.warn("username or password is not correct.", "Warning");
            return;
        }
        this.router().setPass(pass);
        this.trigger("onLogin", null);
    }
}
