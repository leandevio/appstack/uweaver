/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.router;

import org.uweaver.security.Pass;
import org.uweaver.zk.applet.Applet;
import org.uweaver.zk.widget.Pane;
import org.uweaver.zk.widget.Wrapper;

import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The interface defines ... The definitions provides ...
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public interface Router extends Wrapper {
    Locale locale();
    Pass pass();
    void setPass(Pass pass);
    boolean isAuthenticated();
    void setTheme(String theme);

    String getTheme();

    void setLocale(Locale locale);

    Applet navigate(String uri, Map<String, Object> args);

    Applet navigate(String uri);

    Pane dock(Runnable action, Map<String, Object> options);

    Pane dock(Supplier<Map<String, Object>> data, boolean repeat, Map<String, Object> options);

    void undock(Pane pane);

}
