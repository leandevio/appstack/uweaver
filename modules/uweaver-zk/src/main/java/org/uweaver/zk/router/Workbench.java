/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.router;

import org.uweaver.core.cipher.Encryptor;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.exception.NotFoundException;
import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Environment;
import org.uweaver.core.util.Model;
import org.uweaver.security.Authorizer;
import org.uweaver.security.Pass;
import org.uweaver.security.Permission;
import org.uweaver.service.app.AppFolder;
import org.uweaver.service.app.AppManager;
import org.uweaver.service.app.Applet;
import org.uweaver.service.app.SiteManager;
import org.uweaver.service.messenger.Messenger;
import org.uweaver.ui.Position;
import org.uweaver.zk.util.ComponentBuilder;
import org.uweaver.zk.util.UIMLResourceBundle;
import org.uweaver.zk.widget.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Span;
import org.zkoss.zul.theme.Themes;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;
import java.util.function.Supplier;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Workbench obj = new Workbench();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Workbench extends AbstractRouter {
    private UIMLResourceBundle resources = new UIMLResourceBundle(Workbench.class);
    private ComponentBuilder builder = new ComponentBuilder();
    private Environment environment = Environment.getDefaultInstance();
    private Logon logon;
    private Navbar launchpad;
    private Window logonPanel;
    private Component workspace;
    private Pane desktop;
    private Pane banner;
    private Hbox dock;
    private Popup accountPopup, taskListPopup, themePopup, languagePopup, dataPopup;
    private org.zkoss.zul.Timer timer;
    private SiteManager siteManager = new SiteManager();
    private AppManager appManager = new AppManager();
    private Authorizer authorizer = Authorizer.getDefaultInstance();
    private Encryptor encryptor = new Encryptor(Encryptor.Algorithm.DES, environment.property("encryptor.key", "0000"));
    private List<org.uweaver.zk.applet.Applet> tasks = new ArrayList<>();
    private Messenger messenger = new Messenger();
    private Map<Pane, Supplier<Map<String, Object>>> liveDockItems = new HashMap<>();

    public Workbench() {
        super();
        String zuml = resources.get("main");
        workspace = builder.build(zuml);
        this.setVflex("1");
        append(workspace);
        workspace.setVisible(false);
        buildBanner();
        buildDock();
        buildAccountPopup();
        buildTaskListPopup();
        buildThemePopup();
        buildLanguagePopup();
        buildDataPopup();
        buildLogon();
        buildLaunchpad();
        buildDesktop();
        buildTimer();
        this.render();
    }

    private void buildBanner() {
        this.banner = query("north div", Pane.class);
    }

    private void buildDock() {
        this.dock = (Hbox) this.banner.query("hbox[name=dock]");
        Pane account = new Pane((Div)this.dock.query("div[name=account]"));
        account.addEventListener("onClick", this::showAccountPopup);
        Pane home = new Pane((Div)this.dock.query("div[name=home]"));
        home.addEventListener("onClick", event -> this.home());
        Pane taskList = new Pane((Div)this.dock.query("div[name=taskList]"));
        taskList.addEventListener("onClick", this::showTaskListPopup);
        Pane theme = new Pane((Div)this.dock.query("div[name=theme]"));
        theme.addEventListener("onClick", this::showThemePopup);
        Pane language = new Pane((Div)this.dock.query("div[name=language]"));
        language.addEventListener("onClick", this::showLanguagePopup);
    }

    private void buildAccountPopup() {
        String zuml = resources.get("accountPopup");
        this.accountPopup = builder.build(zuml, Popup.class);
        this.append(this.accountPopup);
        Trigger trigger = new Trigger(this.accountPopup.component());
        trigger.addEventListener("onLogout", event -> logout());
    }

    private void buildTaskListPopup() {
        String zuml = resources.get("taskListPopup");
        this.taskListPopup = builder.build(zuml, Popup.class);
        this.append(this.taskListPopup);
    }

    private void buildThemePopup() {
        String zuml = resources.get("themePopup");
        this.themePopup = builder.build(zuml, Popup.class);
        this.append(this.themePopup);
        Label header = (Label) this.themePopup.query("div label");
        header.setValue(translate(header.getValue()));
        for(Component el : this.themePopup.queryAll("div.uw-clickable")) {
            Div div = (Div) el;
            Label label = (Label) div.query("label");
            label.addEventListener("onClick", event -> {
                Label target = (Label) event.getTarget();
                String name = (String) target.getAttribute("name");
                this.setTheme(name);
            });
        }
    }


    private void buildLanguagePopup() {
        String zuml = resources.get("languagePopup");
        this.languagePopup = builder.build(zuml, Popup.class);
        this.append(this.languagePopup);
        Label header = (Label) this.languagePopup.query("div label");
        header.setValue(translate(header.getValue()));
        for(Component el : this.languagePopup.queryAll("div.uw-clickable")) {
            Div div = (Div) el;
            Label label = (Label) div.query("label");
            label.setValue(translate(label.getValue()));
            label.addEventListener("onClick", event -> {
                Label target = (Label) event.getTarget();
                String lang = (String) target.getAttribute("lang");
                this.setLocale(Locale.forLanguageTag(lang));
            });
        }
    }

    private void buildDataPopup() {
        String zuml = resources.get("dataPopup");
        this.dataPopup = builder.build(zuml, Popup.class);
        this.append(this.dataPopup);
    }

    private void showAccountPopup(Event event) {
        Popup popup = this.accountPopup;
        Label label = (Label) popup.query("label");
        String displayName = pass().text();
        if(displayName==null) displayName = pass().username();
        label.setValue("Hi, " + displayName);
        popup.open((Widget) event.getTarget(), Position.BOTTOM_LEFT);
    }

    private void showTaskListPopup(Event event) {
        String tplHeader = "<div><label value=\"%s\" style=\"font-weight: bolder\" /><separator bar=\"true\" spacing=\"16px\"/></div>";
        String tplTask = "<div sclass=\"uw-clickable\"><custom-attributes uri=\"%s\"/><span sclass=\"z-icon-%s\" style=\"margin-right:10px\" /><span>%s</span></div>";
        StringBuffer zuml = new StringBuffer();
        Popup popup = this.taskListPopup;
        popup.empty();
        zuml.append(String.format(tplHeader, translate("Task List"))).append("<vbox>");
        for(org.uweaver.zk.applet.Applet task : tasks) {
            String uri = task.getClass().getCanonicalName();
            String icon = (task.icon()==null ? "gear" : task.icon());
            String title = translate(task.title());
            zuml.append(String.format(tplTask, uri, icon, title));
        }
        if(tasks.size()==0) {
            zuml.append(String.format("<label value=\"%s\"/>", "Empty"));
        }
        zuml.append("</vbox>");
        popup.append(builder.build(zuml.toString()));
        for(Component div : popup.queryAll("div.uw-clickable")) {
            div.addEventListener("onClick", evt -> {
                Component c = evt.getTarget();
                String uri = (String) c.getAttribute("uri");
                startup(uri, null, null);
            });
        }
        popup.open((Widget) event.getTarget(), Position.BOTTOM_LEFT);
    }

    private void showThemePopup(Event event) {
        Popup popup = this.themePopup;
        String theme = this.getTheme();
        for(Component el : popup.queryAll("div.uw-clickable")) {
            Div div = (Div) el;
            Label label = (Label) div.query("label");
            Span icon = (Span) div.query("span");
            icon.setVisible(label.getAttribute("name").equals(theme));
        }
        popup.open((Widget) event.getTarget(), Position.BOTTOM_LEFT);
    }


    private void showLanguagePopup(Event event) {
        Popup popup = this.languagePopup;
        Locale locale = this.locale();
        for(Component el : popup.queryAll("div.uw-clickable")) {
            Div div = (Div) el;
            Label label = (Label) div.query("label");
            Span icon = (Span) div.query("span");
            icon.setVisible(label.getAttribute("lang").equals(locale.toLanguageTag()));
        }
        popup.open((Widget) event.getTarget(), Position.BOTTOM_LEFT);
    }

    private void showDataPopup(Widget dockItem, Map<String, Object> data) {
        String tpl = "<label value=\"%s: %s\" />";
        Popup popup = this.dataPopup;
        StringBuffer uiml = new StringBuffer();
        uiml.append("<vbox>");
        for(String name : data.keySet()) {
            if("flash".equals(name)) continue;
            uiml.append(String.format(tpl, translate(name), data.get(name)));
        }
        uiml.append("</vbox>");
        Component el = builder.build(uiml.toString());
        popup.empty();
        popup.append(el);
        popup.open(dockItem, Position.BOTTOM_LEFT);
    }


    public void logout() {
//        Executions.getCurrent().getSession().invalidate();
        Executions.sendRedirect(Executions.getCurrent().getContextPath());
    }

    private void buildDesktop() {
        this.desktop = query("center div", Pane.class);
    }

    private void buildTimer() {
        this.timer = new org.zkoss.zul.Timer();
        this.timer.setDelay(1000);
        this.timer.setRepeats(true);
        this.timer.stop();
        timer.addEventListener("onTimer", event -> {
            for(Pane pane : liveDockItems.keySet()) {
                if(System.currentTimeMillis()%10==0) {
                    Supplier<Map<String, Object>> supplier = liveDockItems.get(pane);
                    Map<String, Object> data = supplier.get();
                    if(data.containsKey("flash")) {
                        pane.setAttribute("flash", data.get("flash"));
                    }
                }
                String sclass;
                if(pane.getAttributeAsBoolean("flash")) {
                    sclass = pane.getAttributeAsString("original_sclass") + " uw-color-yellow";
                } else {
                    sclass = pane.getAttributeAsString("original_sclass");
                }

                if(sclass.equals(pane.getSclass())) {
                    pane.setSclass(pane.getAttributeAsString("original_sclass"));
                } else {
                    pane.setSclass(sclass);
                }
            }
        });
        this.banner.append(this.timer);
    }

    private void buildLogon() {
        Logon logon = new Logon();
        logon.addEventListener("onLogin", this::onLogin);
        Window window = new Window();
        window.setBorder(false);
        window.setShadow(false);
        window.setStyle("padding:201px 365px 165px 365px;background-image: url('image/logon-background.png');background-repeat: no-repeat;background-color: rgb(6,38,72);");
        window.append(logon);
        append(window);
        window.hide();
        this.logon = logon;
        this.logonPanel = window;
    }

    private void buildLaunchpad() {
        Navbar launchpad = new Navbar();
        launchpad.setStyle("border: none");
        launchpad.addEventListener("onSelect", this::onSelectApplet);
        Pane pane = query("west > div", Pane.class);
        pane.append(launchpad);
        this.launchpad = launchpad;
    }

    private void onSelectApplet(Event event) {
        Model data =  (Model) event.getData();
        String uri = data.getAsString("uri");
        String title = data.getAsString("title");
        String icon = data.getAsString("icon");
        org.uweaver.zk.applet.Applet applet = launch(uri, title, icon);
        if(applet!=null) applet.activate();
    }

    private void home() {
        home(new HashMap<>());
    }

    private void home(Map<String, Object> args) {
        String home = environment.property("home.uri");
        if(home!=null) {
            org.uweaver.zk.applet.Applet applet = launch(home, "Home", "home");
            applet.activate(args);
        } else {
            desktop.empty();
        }
    }

    private org.uweaver.zk.applet.Applet launch(String uri, String title, String icon) {
        if(uri.startsWith("http://") || uri.startsWith("https://")) {
            forward(uri);
            return null;
        } else {
            return startup(uri, title, icon);
        }
    }

    private void forward(String uri) {
        StringBuilder url = new StringBuilder(uri);
        url.append("?credential=").append(encode(credential()));
        Executions.getCurrent().sendRedirect(url.toString(), "_blank");
    }

    private org.uweaver.zk.applet.Applet startup(String uri, String title, String icon) {
        org.uweaver.zk.applet.Applet applet = null;
        for(org.uweaver.zk.applet.Applet task : tasks) {
            if(task.getClass().getCanonicalName().equals(uri)) {
                applet = task;
                break;
            }
        }

        try {
            if(applet==null) {
                applet = (org.uweaver.zk.applet.Applet) this.getClass().getClassLoader().loadClass(uri).newInstance();
                if(applet.title()==null||applet.title().length()==0) {
                    applet.setTitle((title==null) ? appManager.applet(uri).title() : title);
                }
                applet.setIcon(icon);
                applet.render();
            } else {
                this.tasks.remove(applet);
            }
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (ClassNotFoundException e) {
            throw new NotFoundException(e);
        }
        desktop.empty();
        desktop.append(applet);
        this.calibrate();
        this.tasks.add(applet);
        return applet;
    }

    private void renderLaunchpad() {
        Pass pass = pass();
        Collection data = new Collection();
        for(Applet applet : appManager.applets()) {
            Model datum = new Model(applet);
            Permission permission = authorizer.getPermission(applet.uri());
            datum.set("lock", !authorizer.permit(pass, permission));
            datum.set("id", applet.uri());
            datum.set("folder", applet.category());
            datum.set("isFolder", false);
            data.add(datum);
        }
        for(AppFolder appFolder : appManager.appFolders()) {
            Model datum = new Model(appFolder);
            datum.set("id", appFolder.uri());
            datum.set("folder", appFolder.parent());
            datum.set("isFolder", true);
            data.add(datum);
        }
        launchpad.setData(data);
    }

    private String credential() {
        return isAuthenticated() ? encryptor.cipher(pass().username()) : "";
    }

    private void onLogin(Event event) {
        this.logonPanel.hide();
        this.workspace.setVisible(true);
        render();
    }

    @Override
    public Workbench render() {
        super.render();
        this.calibrate();
        if(!isAuthenticated()) {
            this.logonPanel.overlap();
            return this;
        }
        this.workspace.setVisible(true);
        renderLaunchpad();
        setDisplayName(pass());
        setTitle(siteManager.site().title());

        HttpServletRequest request = ((HttpServletRequest) Executions.getCurrent().getNativeRequest());
        String uri = request.getParameter("applet");

        Map args = new HashMap<String, Object>();
        Enumeration<String> names = request.getParameterNames();
        while(names.hasMoreElements()) {
            String name = names.nextElement();
            args.put(name, request.getParameter(name));
        }
        if(uri==null) {
            this.home(args);
        } else {
            org.uweaver.zk.applet.Applet applet = launch(uri, null, null);

            applet.activate(args);
        }

        this.timer.start();

        return this;
    }

    public void setDisplayName(Pass pass) {
        Label label = (Label) this.banner.query("label[name=displayName]");
        String text = pass.text();
        String displayName = (text==null) ? pass.username() : String.format("%s (%s)", text, pass.username());
        label.setValue(displayName);
    }

    public void setTitle(String title) {
        Label label = (Label) this.banner.query("label[name=title]");
        label.setValue(title);
    }

    public void setTheme(String theme) {
        Themes.setTheme(Executions.getCurrent(), theme);
        Executions.sendRedirect("");
    }

    @Override
    public org.uweaver.zk.applet.Applet navigate(String uri, Map<String, Object> args) {
        org.uweaver.zk.applet.Applet applet = this.launch(uri, null, null);
        applet.activate(args);
        return applet;
    }

    @Override
    public Pane dock(Runnable action, Map<String, Object> options) {
        String tpl = "<div sclass=\"z-icon-%s uw-clickable\" style=\"%s\"></div>";
        String icon = (options.containsKey("icon") ? (String) options.get("icon") : "info");
        String uiml = String.format(tpl, icon, options.get("style"));
        Pane pane = builder.build(uiml, Pane.class);
        pane.setAttribute("original_sclass", pane.getSclass());
        pane.addEventListener("onClick", event -> {
            action.run();
        });
        this.dock.appendChild(pane.component());
        return pane;
    }

    @Override
    public Pane dock(Supplier<Map<String, Object>> supplier, boolean repeat, Map<String, Object> options) {

        String tpl = "<div sclass=\"z-icon-%s uw-clickable\" style=\"%s\"></div>";
        String icon = (options.containsKey("icon") ? (String) options.get("icon") : "info");
        String uiml = String.format(tpl, icon, options.get("style"));
        Pane pane = builder.build(uiml, Pane.class);
        pane.setAttribute("original_sclass", pane.getSclass());
        pane.setAttribute("flash", false);
        pane.addEventListener("onClick", event -> {
            Map<String, Object> data = supplier.get();
            showDataPopup((Widget) event.getTarget(), data);
        });
        if(repeat) {
            this.liveDockItems.put(pane, supplier);
        }

        if(options.containsKey("index")) {
            int index = (int) options.get("index");
            Component ref = this.dock.getChildren().get(index);
            this.dock.insertBefore(pane.component(), ref);
        } else {
            this.dock.appendChild(pane.component());
        }

        return pane;
    }

    @Override
    public void undock(Pane pane) {
        pane.stopListening();
        this.dock.removeChild(pane.component());
    }

    private String encode(String s) {
        String code;
        try {
            code = URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ApplicationException(e);
        }

        return code;
    }
}
