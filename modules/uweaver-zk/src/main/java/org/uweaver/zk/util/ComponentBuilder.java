/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.util;

import org.uweaver.core.exception.ApplicationException;
import org.uweaver.i18n.I18n;
import org.uweaver.zk.router.Router;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Label;
import org.zkoss.zul.impl.XulElement;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

/**
 *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ComponentBuilder obj = new ComponentBuilder();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ComponentBuilder {
    public Component build(String zuml) {
        Component component;
        try {
            component =  Executions.createComponentsDirectly(zuml, "zul", null, null);
        } catch(UiException e) {
            component = Executions.createComponentsDirectly("<div>" + zuml + "</div>", "zul", null, null);
        }

        translate(component);
        return component;
    }

    public <T> T build(String zuml, Class<T> type) {
        Component component = build(zuml);
        T widget;
        try {
            Constructor<T> constructor = getConstructor(type, component.getClass());
            widget = constructor.newInstance(component);
        } catch (NoSuchMethodException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (InvocationTargetException e) {
            throw new ApplicationException(e);
        }

        translate(component);
        return widget;

    }

    private void translate(Component component) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        Locale locale = router.locale();
        I18n i18n = (locale==null) ? new I18n() : new I18n(locale);
        component.queryAll("label").forEach(element -> {
            Label label = (Label) element;
            label.setValue(i18n.translate(label.getValue()));
        });
    }


    private <T> Constructor<T> getConstructor(Class type, Class parameter) throws NoSuchMethodException{
        Constructor<T> constructor;
        try {
            constructor = type.getConstructor(parameter);
        } catch (NoSuchMethodException e) {
            constructor = type.getConstructor(XulElement.class);
        }
        return constructor;
    }
}
