/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.util;

import org.uweaver.core.util.MediaType;
import org.zkoss.zul.Filedownload;

import java.io.InputStream;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Downloader obj = new Downloader();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Downloader {
    public static void save(InputStream in, MediaType mediaType, String filename) {
        Filedownload.save(in, mediaType.toString(), filename);
    }

    public static void save(byte[] bytes, MediaType mediaType, String filename) {
        Filedownload.save(bytes, mediaType.toString(), filename);
    }

    public static void save(InputStream in, String mediaType, String filename) {
        Filedownload.save(in, mediaType, filename);
    }

    public static void save(byte[] bytes, String mediaType, String filename) {
        Filedownload.save(bytes, mediaType, filename);
    }
}
