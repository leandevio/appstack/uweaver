/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.util.Environment;
import org.uweaver.i18n.I18n;
import org.uweaver.security.Pass;
import org.uweaver.zk.router.Router;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.Path;
import org.zkoss.zul.Label;
import org.zkoss.zul.impl.LabelElement;
import java.util.Locale;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * AbstractWrapper obj = new AbstractWrapper();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class AbstractHtmlWrapper<T extends HtmlBasedComponent> extends AbstractWrapper<T> implements HtmlWidget, Wrapper {
    public AbstractHtmlWrapper(T component) {
        super(component);
    }

    @Override
    public void focus() {
        this.component().setFocus(true);
        this.trigger("onFocus", null);
    }

    @Override
    public void setWidth(String width) {
        this.component.setWidth(width);
    }

    @Override
    public void setHeight(String height) {
        this.component.setHeight(height);
    }

    @Override
    public String getWidth() {
        return this.component.getWidth();
    }

    @Override
    public String getHeight() {
        return this.component.getHeight();
    }

    @Override
    public void setSclass(String sclass) {
        this.component.setSclass(sclass);
    }

    @Override
    public String getSclass() {
        return this.component.getSclass();
    }

    @Override
    public void setStyle(String style) {
        this.component.setStyle(style);
    }

    public void setVflex(String flex) {
        this.component.setVflex(flex);
    }

    public void setHflex(String flex) {
        this.component.setHflex(flex);
    }

}
