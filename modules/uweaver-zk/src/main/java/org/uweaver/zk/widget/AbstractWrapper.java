package org.uweaver.zk.widget;

import org.uweaver.core.event.Event;
import org.uweaver.core.event.Listenable;
import org.uweaver.core.exception.ApplicationException;
import org.uweaver.core.util.Environment;
import org.uweaver.i18n.I18n;
import org.uweaver.security.Pass;
import org.uweaver.zk.router.Router;
import org.zkoss.xel.VariableResolver;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Label;
import org.zkoss.zul.impl.LabelElement;
import org.zkoss.zul.impl.XulElement;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

public abstract class AbstractWrapper<T extends Component> extends AbstractComponent implements Wrapper {
    protected T component;
    protected I18n i18n = new I18n(locale());

    public AbstractWrapper(T component) {
        this.component = component;
    }

    @Override
    public AbstractWrapper render() {
        return this;
    }

    @Override
    public T component() {
        return this.component;
    }

    @Override
    public void trigger(String name, Object data) {
        Events.sendEvent(name, this, data);
    }

    @Override
    public void wire() {
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);

        List<VariableResolver> resolvers = new ArrayList<>();
        resolvers.add(new DelegatingVariableResolver());
        Selectors.wireVariables(this, this, resolvers);
    }

    @Override
    public Component query(String selector) {
        return this.component.query(selector);
    }

    @Override
    public <E> E query(String selector, Class<E> type) {
        E widget;
        Component component = this.component.query(selector);
        try {
            Constructor<E> constructor = getConstructor(type, component.getClass());
            widget = constructor.newInstance(component);
        } catch (NoSuchMethodException e) {
            throw new ApplicationException(e);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        } catch (InstantiationException e) {
            throw new ApplicationException(e);
        } catch (InvocationTargetException e) {
            throw new ApplicationException(e);
        }
        return widget;
    }

    private <T> Constructor<T> getConstructor(Class type, Class parameter) throws NoSuchMethodException{
        Constructor<T> constructor;
        try {
            constructor = type.getConstructor(parameter);
        } catch (NoSuchMethodException e) {
            constructor = type.getConstructor(XulElement.class);
        }
        return constructor;
    }


    @Override
    public List<Component> queryAll(String selector) {
        List<Component> components = new ArrayList<>();
        this.component.queryAll(selector).forEach(element -> {
            components.add(element);
        });

        return components;
    }

    @Override
    public void show() {
        this.component.setVisible(true);
    }

    @Override
    public void hide() {
        this.component.setVisible(false);
    }

    public void toggle() {
        this.component.setVisible(!this.isVisible());
    }

    @Override
    public boolean isVisible() {
        return this.component.isVisible();
    }

    @Override
    public boolean setVisible(boolean visible) {
        return this.component.setVisible(visible);
    }

    @Override
    public String id() {
        return this.component.getId();
    }

    @Override
    public boolean append(Component component) {
        return this.appendChild(component);
    }

    @Override
    public boolean appendChild(Component component) {
        Component element = component;
        if(component instanceof Wrapper) {
            element = ((Wrapper) component).component();
        }

        return this.component.appendChild(element);
    }

    @Override
    public boolean removeChild(Component component) {
        Component element = component;
        if(component instanceof Wrapper) {
            element = ((Wrapper) component).component();
        }
        return this.component.removeChild(element);
    }

    @Override
    public void empty() {
        for(Component component : this.component.getChildren()) {
            this.component.removeChild(component);
        }
    }

    @Override
    public List<Component> getChildren() {
        return this.component.getChildren();
    }

    @Override
    public boolean hasAttribute(String name) {
        return this.component.hasAttribute(name);
    }

    @Override
    public Object getAttribute(String name) {
        return this.component.getAttribute(name);
    }

    @Override
    public Object setAttribute(String name, Object value) {
        this.component.setAttribute(name, value);
        return value;
    }

    @Override
    public Object getAttribute(String name, Object defaultValue) {
        return hasAttribute(name) ? getAttribute(name) : defaultValue;
    }

    @Override
    public String getAttributeAsString(String name) {
        return (String) this.getAttribute(name);
    }

    @Override
    public String getAttributeAsString(String name, String defaultValue) {
        return (String) this.getAttribute(name, defaultValue);
    }

    @Override
    public Integer getAttributeAsInteger(String name) {
        return getAttributeAsInteger(name, null);
    }

    @Override
    public Integer getAttributeAsInteger(String name, Integer defaultValue) {
        return this.hasAttribute(name) ? Integer.parseInt(this.getAttributeAsString(name)) : defaultValue;
    }

    @Override
    public Boolean getAttributeAsBoolean(String name) {
        return getAttributeAsBoolean(name, null);
    }

    @Override
    public Boolean getAttributeAsBoolean(String name, Boolean defaultValue) {
        Object value = this.hasAttribute(name) ? this.getAttribute(name) : defaultValue;

        if(!(value instanceof Boolean)) {
            value = Boolean.valueOf(this.getAttributeAsString(name));
        }
        return (Boolean) value;
    }

    @Override
    public void listenTo(Listenable listenable, String event, Consumer<Event> fn) {
        listenable.addEventListener(event, fn);
    }

    @Override
    public void stopListening() {

    }

    @Override
    public void stopListening(Listenable listenable) {

    }


    /**
     * super.appendChild is a shortcut to super.insertBefore(child, null).
     *
     * @param component
     * @param beforeComponent
     * @return
     */
    @Override
    public boolean insertBefore(Component component, Component beforeComponent) {
        Component element = component;
        if(component instanceof Wrapper) {
            element = ((Wrapper) component).component();
        }
        return this.component.insertBefore(element, beforeComponent);
    }


    @Override
    public Locale locale() {
        return router().locale();
    }

    @Override
    public void enhance(String selector) {
        for(Component component : queryAll(selector)) {
            if(Label.class.isAssignableFrom(component.getClass())) {
                Label label = (Label) component;
                label.setValue(this.translate(label.getValue()));
            } else if(LabelElement.class.isAssignableFrom(component.getClass())){
                LabelElement label = (LabelElement) component;
                label.setLabel(this.translate(label.getLabel()));
            }
        }
    }

    @Override
    public String translate(String text) {
        return i18n.translate(text);
    }

    @Override
    public Router router() {
        return (Router) Path.getComponent("/").getAttribute("router");
    }

    @Override
    public Pass pass() {
        return router().pass();
    }

    @Override
    public boolean isAuthenticated() {
        return router().isAuthenticated();
    }
}
