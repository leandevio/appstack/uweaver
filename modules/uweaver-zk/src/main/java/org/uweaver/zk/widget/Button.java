/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Button obj = new Button();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Button extends AbstractHtmlWrapper<org.zkoss.zul.Button> implements Control<String> {
    private String name;
    private String action;
    private String value;
    private String state = null;
    private String disableWhen = null;

    public Button() {
        this(new org.zkoss.zul.Button());
    }

    public Button(org.zkoss.zul.Button button) {
        super(button);
        this.name = getAttributeAsString("name");
        this.action =  getAttributeAsString("action");
        this.disableWhen = getAttributeAsString("disableWhen");
        this.value = getAttributeAsString("value");
        component.addEventListener("onClick", event -> click());
        setLabel(label());
    }

    public void click() {
        if(this.hasAttribute("warning")) {
            warn(() -> this.trigger("onAction", value()));
        } else if(this.hasAttribute("confirmation")) {
            confirm(() -> this.trigger("onAction", value()));
        } else {
            this.trigger("onAction", value());
        }
    }

    private void warn(Runnable ok) {
        String warning = this.getAttributeAsString("warning");
        if(warning==null) {
            ok.run();
            return;
        }

        String[] tokens = warning.split(":");
        String message = tokens[tokens.length-1];
        String title = (tokens.length==2) ? tokens[0] :  null;
        Popup.warn(message, title, () -> ok.run());
    }

    private void confirm(Runnable ok) {
        String confirmation = this.getAttributeAsString("confirmation");
        if(confirmation==null) {
            ok.run();
            return;
        }

        String[] tokens = confirmation.split(":");
        String message = tokens[tokens.length-1];
        String title = (tokens.length==2) ? tokens[0] :  null;
        Popup.confirm(message, title, () -> ok.run());
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String action() {
        return this.action;
    }

    @Override
    public String label() {
        return component.getLabel();
    }

    @Override
    public void setLabel(String text) {
        component.setLabel(translate(text));
    }

    @Override
    public void setState(String state) {
        String disableWhen = disableWhen();
        if(disableWhen!=null) {
            setDisabled(disableWhen.equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    @Override
    public void enable() {
        this.component.setDisabled(false);
    }

    @Override
    public void disable() {
        this.component.setDisabled(true);
    }

    @Override
    public void setDisabled(boolean disable) {
        this.component.setDisabled(disable);
    }

    @Override
    public String disableWhen() {
        return this.disableWhen;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", (name()==null ? label() : name()), action());
    }
}
