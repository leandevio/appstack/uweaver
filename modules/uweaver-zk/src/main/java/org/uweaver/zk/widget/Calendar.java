package org.uweaver.zk.widget;

import java.util.Date;

public class Calendar extends AbstractWrapper<org.zkoss.zul.Calendar> implements Wrapper {
    public Calendar() {
        this(new org.zkoss.zul.Calendar());
    }

    public Calendar(org.zkoss.zul.Calendar calendar) {
        super(calendar);
        calendar.addEventListener("onChange", event -> this.change());
        calendar.addEventListener("onClick", event -> this.click());
        render();
    }

    private void change() {
        this.trigger("onChange", this.component.getValue());
    }

    private void click() {
        this.trigger("onClick", null);
    }

    public void setValue(Date value) {
        this.component.setValue(value);
    }

    public Date getValue() {
        return this.component.getValue();
    }

    @Override
    public void focus() {
        this.component.focus();
    }

    @Override
    public String getWidth() {
        return this.component.getWidth();
    }

    @Override
    public String getHeight() {
        return this.component.getHeight();
    }
}
