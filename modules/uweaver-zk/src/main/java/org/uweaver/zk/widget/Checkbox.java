/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Checkbox obj = new Checkbox();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Checkbox extends AbstractHtmlWrapper<org.zkoss.zul.Checkbox> implements Input<Object> {
    private String state = null;

    public Checkbox() {
        this(new org.zkoss.zul.Checkbox());
    }

    public Checkbox(org.zkoss.zul.Checkbox checkbox) {
        super(checkbox);
        component.addEventListener("onCheck", event -> change());
        render();
    }


    public void change() {
        this.trigger("onChange", value());
    }

    @Override
    public String name() {
        return this.component.getName();
    }

    @Override
    public Object value() {
        Object value = null;
        Object option = this.component.getValue();
        if(option==null) {
            value = isChecked();
        } else if(isChecked()) {
            value = option;
        }
        return value;
    }

    @Override
    public Input setValue(Object value) {
        if(value==null) {
            setChecked(false);
            return this;
        }

        Object v = this.component.getValue();

        if(v==null) {
            if(value instanceof Boolean) {
                setValue2((Boolean) value);
            } else {
                setChecked(false);
            }
        } else if(Collection.class.isAssignableFrom(value.getClass())) {
            setValue2((Collection) value);
        } else if(value.getClass().isArray()) {
            setValue2((Object[]) value);
        } else {
            setValue2(value);
        }

        return this;
    }

    private void setValue2(Boolean value) {
        setChecked(value);
    }

    private void setValue2(Collection values) {
        setChecked(values.contains(this.component.getValue()));
    }

    private void setValue2(Object[] values) {
        List selection = Arrays.asList(values);
        setValue2(selection);
    }

    private void setValue2(Object value) {
        setChecked(this.component.getValue().equals(value));
    }


    public void setChecked(boolean checked) {
        this.component.setChecked(checked);
    }

    public boolean isChecked() {
        return this.component.isChecked();
    }

    @Override
    public Input setInplace(boolean inplace) {
        return null;
    }


    @Override
    public void setWidth(String width) {
        component.setWidth(width);
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    public String errorMessage() {
        return null;
    }

    @Override
    public String predicate() {
        return this.getAttributeAsString("predicate");
    }

    @Override
    public void reset() {
        return;
    }

    @Override
    public String subject() {
        return this.getAttributeAsString("subject");
    }

    @Override
    public void setState(String state) {
        String readonlyWhen = readonlyWhen();
        if(readonlyWhen!=null) {
            setReadonly(readonlyWhen().equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    @Override
    public void setReadonly(boolean readonly) {
        this.component.setDisabled(readonly);
    }

    @Override
    public boolean isReadonly() {
        return this.component.isDisabled();
    }

    @Override
    public String readonlyWhen() {
        return this.getAttributeAsString("readonlyWhen");
    }

    @Override
    public boolean isRequired() {
        return getAttributeAsBoolean("required", false);
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name(), value());
    }
}
