/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Model;
import org.uweaver.service.CodeService;
import org.uweaver.ui.Selectable;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.ListModelList;

import java.util.*;

/**
 * *
 * The implementation provides:
 * - ...
 * - ...
 * <p>
 * Usage:
 * <p>
 * ```java
 * Combobox obj = new Combobox();
 * ...
 * ```
 * <p>
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Combobox<T> extends AbstractHtmlWrapper<Chosenbox> implements Input, Selectable<T> {
    private static String REQUIRED = "This field is required.";
    private static String HINTS = "Click to show the options";
    private List data = null;
    private String state = null;
    private String valueField, textField;
    private List<T> selection = new ArrayList<>();
    private List<Option> options = new ArrayList<>();
    private ListModelList<Option> listModel = new ListModelList(options);

    @WireVariable
    private CodeService codeService;

    public Combobox(Chosenbox chosenbox) {
        super(chosenbox);
        this.component.setModel(this.listModel);
        // 使 Chosenbox 在 iOS 上能夠偵測到點擊.
        if(this.component.getEmptyMessage()==null||this.component.getEmptyMessage().length()==0) {
            this.component.setEmptyMessage(this.translate(HINTS));
        }
        this.component.addEventListener("onSelect", this::onComponentSelect);
        valueField = getAttributeAsString("valueField");
        textField = getAttributeAsString("textField");
        setData(new Collection());

        wire();
        String type = code();
        if (type != null) {
            valueField = "code";
            textField = "text";
            setData(codeService.findAllByType(type));
        } else {
            setData(new Collection());
        }
    }

    public String code() {
        return getAttributeAsString("code");
    }

    private void onComponentSelect(Event event) {
        setValue(parse());
    }

    private List parse() {
        List selection = new ArrayList<>();

        Set<Option> selectedObjects = this.component.getSelectedObjects();

        for (Option option : selectedObjects) {
            selection.add(option.value);
        }

        return selection.size()>0 ? selection : null;
    }

    public Combobox render() {
        options.clear();

        for (Object datum : data) {
            Model model = new Model(datum);
            Object value = (valueField == null) ? datum : model.get(valueField);
            String text = (textField == null) ? datum.toString() : model.getAsString(textField);
            double priority = model.has("priority") ? model.getAsDouble("priority") : 0;
            Option option = new Option(value, text, priority);
            options.add(option);
        }
        options.sort(Comparator.comparingDouble(option -> option.priority()));
        this.listModel.clear();
        this.listModel.addAll(options);

        renderSelection();

        return this;
    }

    @Override
    public String name() {
        return this.component.getName();
    }

    @Override
    public Object value() {
        Object value;

        if(this.selection.size()==0) {
            value = null;
        } else if(mode().equals(Mode.SINGLE)) {
            value = this.selection.get(0);
        } else {
            value = new ArrayList();
            ((List) value).addAll(this.selection);
        }

        return value;
    }

    @Override
    public Combobox setValue(Object value) {
        Object current = value();
        if(current==null&&value==null) {
            return this;
        } else if(current!=null&&current.equals(value)) {
            return this;
        }

        Object previousValue = current;

        this.selection.clear();
        if(value==null) {
            ;
        } else if(mode().equals(Mode.SINGLE)) {
            this.selection.add((T) value);
        } else {
            this.selection.addAll((List<T>) value);
        }

        renderSelection();

        this.trigger("onChange", previousValue);

        return this;
    }

    private void renderSelection() {
        this.listModel.clearSelection();

        if(this.selection.size()==0) return;

        for(Option option: this.options) {
            if(selection.contains(option.value())) {
                this.listModel.addToSelection(option);
            }
        }
    }

    @Override
    public Combobox setInplace(boolean inplace) {
        return this;
    }

    @Override
    public void setWidth(String width) {
        component.setWidth(width);
    }

    public void setData(List data) {
        if (this.data!=null && Collection.class.isAssignableFrom(this.data.getClass())) {
            stopListening((Collection) this.data);
        }
        this.data = (data==null) ? new Collection() : data;
        if (Collection.class.isAssignableFrom(this.data.getClass())) {
            listenTo((Collection) this.data, "change", event -> this.render());
        }
        this.render();
    }

    @Override
    public boolean validate() {
        boolean isValid = true;
        if(this.isRequired()&& this.value()==null) {
            isValid = false;
        }
        return isValid;
    }

    @Override
    public String errorMessage() {
        return validate() ? null : REQUIRED;
    }


    @Override
    public String predicate() {
        return this.getAttributeAsString("predicate");
    }

    @Override
    public void reset() {
        this.component.clearSelection();
    }

    @Override
    public String subject() {
        return this.getAttributeAsString("subject");
    }

    @Override
    public void setState(String state) {
        String readonlyWhen = readonlyWhen();
        if (readonlyWhen != null) {
            setReadonly(readonlyWhen().equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    @Override
    public Mode mode() {
        return this.hasAttribute("mode") ? Mode.valueOf(this.getAttributeAsString("mode").toUpperCase()) : Mode.MULTI;
    }

    @Override
    public java.util.Collection<T> selection() {
        return this.selection;
    }

    @Override
    public void setReadonly(boolean readonly) {
        this.component.setDisabled(readonly);
    }

    @Override
    public boolean isReadonly() {
        return this.component.isDisabled();
    }

    @Override
    public String readonlyWhen() {
        return this.getAttributeAsString("readonlyWhen");
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name(), value());
    }

    @Override
    public boolean isRequired() {
        return getAttributeAsBoolean("required", false);
    }

    private class Option {
        private Object value = null;
        private String text = null;
        private double priority = 0;

        public Option(Object value, String text, double priority) {
            this.value = value;
            this.text = text;
            this.priority = priority;
        }

        public Object value() {
            return this.value;
        }

        public String text() {
            return this.text;
        }

        public double priority() {
            return this.priority;
        }

        @Override
        public String toString() {
            return (text == null) ? this.value.toString() : this.text;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Combobox.Option)) return false;
            if(obj==null) return false;
            Option other = (Option) obj;
            return (this.hashCode()==other.hashCode());
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }
}
