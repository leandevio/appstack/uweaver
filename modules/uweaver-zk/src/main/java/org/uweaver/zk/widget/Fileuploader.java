/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.entity.Document;
import org.uweaver.security.Pass;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Button;

import java.util.Date;

/**
 * *
 * The implementation provides:
 * - ...
 * - ...
 * <p>
 * Usage:
 * <p>
 * ```java
 * Filebox obj = new Filebox();
 * ...
 * ```
 * <p>
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Fileuploader extends AbstractHtmlWrapper<Button> implements Input<Document>, Control<Document> {
    private String name;
    private String action;
    private Document document = null;
    private String state = null;
    private String disableWhen = null;

    public Fileuploader() {
        this(new org.zkoss.zul.Fileupload());
    }

    public Fileuploader(org.zkoss.zul.Button fileupload) {
        super(fileupload);
        this.name = getAttributeAsString("name");
        this.action =  getAttributeAsString("action");
        this.disableWhen = getAttributeAsString("disableWhen");
        String upload = this.component().getUpload();
        if(!upload.contains("native")) {
            this.component().setUpload(upload + ",native");
        }
        this.component.addEventListener("onUpload", this::onComponentUpload);
        render();
    }

    private void onComponentUpload(Event event) {
        Media media = ((UploadEvent) event).getMedia();
        setValue(toDocument(media));
        this.trigger("onAction", value());
        this.trigger("onChange", value());
    }

    private Document toDocument(Media media) {
        Pass pass = this.pass();
        Document document = new Document();
        document.setMediaType(media.getContentType());
        document.setName(media.getName());
        document.setFormat(media.getFormat());

        byte[] bytes;
        if(media.isBinary()) {
            bytes = media.getByteData();
        } else {
            bytes = media.getStringData().getBytes();
        }

        document.setBytes(bytes);
        document.setSize(bytes.length);
        document.setCreatedOn(new Date());
        document.setPreparedBy(pass.getUsername());
        return document;
    }

    @Override
    public String name() {
        return (String) this.component.getAttribute("name");
    }

    @Override
    public String action() {
        return this.action;
    }

    @Override
    public String label() {
        return component().getLabel();
    }

    @Override
    public void setLabel(String text) {
        component().setLabel(text);
    }

    @Override
    public Document value() {
        return this.document;
    }

    @Override
    public Input setValue(Document document) {
        this.document = document;
        return this;
    }

    @Override
    public Input setInplace(boolean inplace) {
        return this;
    }

    @Override
    public String predicate() {
        return this.getAttributeAsString("predicate");
    }

    @Override
    public void reset() {

    }

    @Override
    public String subject() {
        return this.getAttributeAsString("subject");
    }

    @Override
    public void setState(String state) {
        String readonlyWhen = readonlyWhen();
        if (readonlyWhen != null) {
            setReadonly(readonlyWhen().equalsIgnoreCase(state));
        }
        String disableWhen = disableWhen();
        if(disableWhen!=null) {
            setDisabled(disableWhen.equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    @Override
    public void enable() {
        this.component().setDisabled(false);
    }

    @Override
    public void disable() {
        this.component().setDisabled(true);
    }

    @Override
    public void setDisabled(boolean disable) {
        this.component().setDisabled(disable);
    }

    @Override
    public String disableWhen() {
        return this.disableWhen;
    }


    @Override
    public void setReadonly(boolean readonly) {

    }

    @Override
    public boolean isReadonly() {
        return false;
    }

    @Override
    public String readonlyWhen() {
        return null;
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    public String errorMessage() {
        return null;
    }


    @Override
    public String toString() {
        return String.format("%s: %s", (name()==null ? label() : name()), action());
    }

    @Override
    public boolean isRequired() {
        return getAttributeAsBoolean("required", false);
    }
}
