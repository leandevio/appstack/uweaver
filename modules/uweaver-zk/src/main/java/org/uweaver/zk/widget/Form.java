/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.exception.ConstraintConflictException;
import org.uweaver.core.exception.Violation;
import org.uweaver.search.Predicate;
import org.uweaver.ui.DataBinding;
import org.uweaver.core.util.Model;
import org.uweaver.ui.Validatable;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.impl.InputElement;
import org.zkoss.zul.impl.NumberInputElement;
import org.zkoss.zul.impl.XulElement;

import java.util.*;
import java.util.function.Consumer;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Form obj = new Form();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Form extends AbstractHtmlWrapper<XulElement> implements DataBinding<Model>, Validatable {
    private final static String SELECTOR = "textbox, intbox, decimalbox, datebox, combobox, chosenbox, checkbox, grid[input=true], button";
    private List<Input> inputs = new ArrayList<>();
    private List<Input> indicators = new ArrayList<>();
    private Map<String, Label> labels = new HashMap<>();
    private Model data;
    private String state = null;

    public Form() {
        this(new Div());
    }

    public Form(XulElement div) {
        super(div);
        setData(new Model());
        String formName = (String) div.getAttribute("name");
        boolean isNamedForm = (div.hasAttribute("name"));
        for(Component element: this.queryAll(SELECTOR)) {
            boolean isNamedInput = element.hasAttribute("form");
            if(isNamedForm!=isNamedInput) {
                continue;
            } else if(isNamedForm && !formName.equals(element.getAttribute("form"))){
                continue;
            }

            Input input;
            if(InputElement.class.isAssignableFrom((element.getClass()))) {
                input = buildInput((InputElement) element);
            } else if(Chosenbox.class.isAssignableFrom(element.getClass())) {
                input = buildInput((Chosenbox) element);
            } else if(org.zkoss.zul.Checkbox.class.isAssignableFrom(element.getClass())) {
                input = buildInput((org.zkoss.zul.Checkbox) element);
            } else if(org.zkoss.zul.Grid.class.isAssignableFrom(element.getClass())) {
                input = buildInput((org.zkoss.zul.Grid) element);
            } else if(org.zkoss.zul.Button.class.isAssignableFrom(element.getClass())) {
                input = buildInput((org.zkoss.zul.Button) element);
            } else {
                continue;
            }

            if(input!=null) add(input);
        }

        for(Component component : this.queryAll("label")) {
            Label label = (Label) component;
            String name = (String) label.getAttribute("name");
            if(name!=null) {
                this.labels.put(name, label);
                Input input = this.input(name);
                if(input!=null && input.isRequired()) {
                    addSclass(label, "uw-label-required");
                }
            }
            if(label.getSclass()!=null && label.getSclass().contains("uw-label-input")) {
                label.setValue(this.translate(label.getValue()));
                Component parent = label.getParent();
                Cell cell;
                if(parent instanceof Cell) {
                    cell = (Cell) parent;
                } else {
                    cell = new Cell();
                    parent.insertBefore(cell, label);
                    label.detach();
                    cell.appendChild(label);
                }
                addSclass(cell, "uw-label-input");
            }
        }
        this.enhance("groupbox > caption");
        render();
    }

    private void addSclass(HtmlBasedComponent component, String sclass) {
        StringBuilder sb = new StringBuilder();
        String existingSclass = component.getSclass();
        if(existingSclass!=null) sb.append(existingSclass).append(" ");
        sb.append(sclass);
        component.setSclass(sb.toString());
    }

    private Input buildInput(InputElement element) {
        if(element.getName()==null) return null;

        Input input = null;
        if(NumberInputElement.class.isAssignableFrom(element.getClass())) {
            input = new Numberbox((NumberInputElement) element);
        } else if(org.zkoss.zul.Datebox.class.isAssignableFrom(element.getClass())) {
            input = new Datebox((org.zkoss.zul.Datebox) element);
        } else if(org.zkoss.zul.Combobox.class.isAssignableFrom(element.getClass())) {
            input = new Selectbox((org.zkoss.zul.Combobox) element);
        } else if(org.zkoss.zul.Textbox.class.isAssignableFrom(element.getClass())) {
            input = new Textbox((org.zkoss.zul.Textbox) element);
        }

        return input;
    }

    private Input buildInput(Chosenbox element) {
        if(element.getName()==null) return null;

        Input input = new Combobox(element);

        return input;
    }

    private Input buildInput(org.zkoss.zul.Grid element) {
        if(element.getAttribute("name")==null) return null;

        Input input = new Grid(element);

        return input;
    }

    private Input buildInput(org.zkoss.zul.Checkbox element) {
        if(element.getName()==null) return null;

        Input input = new Checkbox(element);

        return input;
    }

    private Input buildInput(org.zkoss.zul.Button element) {
        if(element.getAttribute("name")==null) return null;

        Input input = new Fileuploader(element);

        return input;
    }


    @Override
    public Form render() {
        for(Input input : inputs) {
            String name = input.name();
            input.setValue(data.get(name));
        }
        for(Input input : indicators) {
            String name = input.name();
            Object value = data.get(name);
            input.setValue(value==null ? null : value.toString());
        }
        return this;
    }

    public void add(Input input) {
        if(input==null||input.name()==null) return;
        if(input.hasAttribute("indicator")) {
            indicators.add(input);
        } else {
            inputs.add(input);
            input.addEventListener("onChange", event -> onInputChange(event));
            input.addEventListener("onOK", event -> onInputOK(event));
        }
    }

    public void add(List<Input> inputs) {
        for(Input input : inputs) {
            add(input);
        }
    }

    private void onInputOK(Event event) {
        this.trigger("onOK", null);
    }

    private void onInputChange(Event event) {
        Input target = (Input) event.getTarget();
        String name = target.name();
        Object value = target.value();

        if(target instanceof Checkbox) {
            List<Input> inputs = inputs(name);
            if(inputs.size()>1) {
                List data = new ArrayList();
                for (Input input : inputs) {
                    Object datum = input.value();
                    if (datum == null) continue;
                    data.add(datum);
                }
                value = data;
            }
        }

        data.set(name, value);
        this.trigger("onChange", data);
    }

    public void clear() {
        this.data.clear();
        reset();
    }

    public void setData(Model data) {
        this.data = data;
        listenTo(data, "change", this::onDataChange);
        render();
    }

    private void onDataChange(org.uweaver.core.event.Event event) {
        render();
    }

    @Override
    public Model data() {
        return this.data;
    }

    @Override
    public String errorMessage() {
        StringBuilder message = new StringBuilder();
        for(Input input : inputs) {
            if(input.validate()) continue;
            Label label = this.label(input.name());
            String caption = (label==null) ? input.name() : label.getValue();
            message.append(caption + ": " + translate(input.errorMessage()) + "\n");
        }
        return message.toString();
    }

    public boolean validate(Runnable done, Consumer<String> fail) {
        boolean valid = true;
        for(Input input : inputs) {
            if(!input.validate()) {
                valid = false;
            }
        }
        if(valid) {
            if(done!=null) done.run();
        } else {
            if(fail!=null) fail.accept(errorMessage());
        }

        return valid;
    }

    @Override
    public boolean validate() {
        return validate(null, null);
    }

    public void reset() {
        for(Input element : inputs) {
            element.reset();
        }
        this.render();
    }

    public Input input(String name) {
        Input input = null;
        for(Input element : inputs) {
            if(element.name().equalsIgnoreCase(name)) {
                input = element;
                break;
            }
        }
        return input;
    }

    public List<Input> inputs(String name) {
        List<Input> inputs = new ArrayList<>();
        for(Input element : this.inputs) {
            if(element.name().equalsIgnoreCase(name)) {
                inputs.add(element);
            }
        }
        return inputs;
    }

    public List<Input> inputs() {
        List<Input> inputs = new ArrayList<>();
        inputs.addAll(this.inputs);
        return inputs;
    }

    public Label label(String name) {
        return this.labels.get(name);
    }

    public List<Label> lables() {
        List<Label> labels = new ArrayList<>();
        labels.addAll(this.labels.values());
        return labels;
    }

    public List<Predicate> predicate() {
        List<Predicate> predicates = new ArrayList<>();
        for(Input element : this.inputs) {
            String subject = element.hasAttribute("subject") ? element.subject() : element.name();
            Object expection = element.value();
            if(expection==null) continue;
            if(expection instanceof String && ((String) expection).length()==0) continue;
            String comparator = element.predicate();
            Predicate predicate = (comparator==null) ? new Predicate(subject, expection) : new Predicate(subject, expection, comparator);
            predicates.add(predicate);
        }
        return predicates;
    }

    public void setState(String state) {
        this.state = state;
        for(Input input : inputs) {
            input.setState(state);
        }
    }

    public String state() {
        return this.state;
    }

    public void alert(Exception ex) {
        Popup.alert(toErrorText(ex));
    }

    public void warn(Exception ex) {
        Popup.warn(toErrorText(ex));
    }

    private String toErrorText(Exception ex) {
        String CAPTIONSEPARATOR = ", ";
        String TEXTSEPARATOR = ": ";
        StringBuilder sb = new StringBuilder();
        if(ex.getMessage()!=null) {
            sb.append(ex.getLocalizedMessage()).append("\n");
        }
        if(ex instanceof ConstraintConflictException) {
            List<Violation> violations = Arrays.asList(((ConstraintConflictException) ex).violations());
            for(Violation violation : violations) {
                StringBuffer caption = new StringBuffer();
                for(String name : violation.properties()) {
                    Label label = label(name);
                    String text = (label==null) ? name : label.getValue();
                    caption.append(this.translate(text)).append(CAPTIONSEPARATOR);
                }
                if(caption.length()>0) {
                    sb.append(caption.substring(0, caption.length()-CAPTIONSEPARATOR.length())).append(TEXTSEPARATOR);
                }
                sb.append(this.translate(violation.message())).append("\n");
            }
        }
        return sb.toString();
    }

    public void setError(List<Violation> error) {
        ;
    }
}
