/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.exception.ConstraintConflictException;
import org.uweaver.core.exception.Violation;
import org.uweaver.core.util.Model;
import org.uweaver.i18n.I18n;
import org.zkoss.zul.Label;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Forms obj = new Forms();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Forms {
    private I18n i18n = new I18n();
    private List<Form> forms = new ArrayList<>();

    public void add(Form form) {
        this.forms.add(form);
    }

    public void remove(Form form) {
        this.forms.remove(form);
    }

    public void render() {
        for(Form form : this.forms) {
            form.render();
        }
    }

    public void setData(Model data) {
        for(Form form : this.forms) {
            form.setData(data);
        }
    }

    public void setState(String state) {
        for(Form form : this.forms) {
            form.setState(state);
        }
    }

    public String errorMessage() {
        StringBuilder message = new StringBuilder();
        for(Form form : this.forms) {
            if(form.validate()) continue;
            message.append(form.errorMessage() + "\n");
        }
        return message.toString();
    }

    public boolean validate() {
        return validate(null, null);
    }

    public boolean validate(Runnable done, Consumer<String> fail) {
        boolean valid = true;
        for(Form form : this.forms) {
            valid = form.validate();
            if(!valid) break;
        }
        if(valid) {
            if(done!=null) done.run();
        } else {
            if(fail!=null) fail.accept(errorMessage());
        }

        return valid;
    }

    public Input input(String name) {
        Input input = null;
        for(Form form : this.forms) {
            input = form.input(name);
            if(input!=null) break;
        }
        return input;
    }

    public Label label(String name) {
        Label label = null;
        for(Form form : this.forms) {
            label = form.label(name);
            if(label!=null) break;
        }
        return label;
    }

    public void alert(Exception ex) {
        Popup.alert(toErrorText(ex));
    }

    public void warn(Exception ex) {
        Popup.warn(toErrorText(ex));
    }

    private String toErrorText(Exception ex) {
        String CAPTIONSEPARATOR = ", ";
        String TEXTSEPARATOR = ": ";
        StringBuilder sb = new StringBuilder();
        if(ex.getMessage()!=null) {
            sb.append(ex.getLocalizedMessage()).append("\n");
        }
        if(ex instanceof ConstraintConflictException) {
            List<Violation> violations = Arrays.asList(((ConstraintConflictException) ex).violations());
            for(Violation violation : violations) {
                StringBuffer caption = new StringBuffer();
                for(String name : violation.properties()) {
                    Label label = label(name);
                    String text = (label==null) ? name : label.getValue();
                    caption.append(i18n.translate(text)).append(CAPTIONSEPARATOR);
                }
                if(caption.length()>0) {
                    sb.append(caption.substring(0, caption.length()-CAPTIONSEPARATOR.length())).append(TEXTSEPARATOR);
                }
                sb.append(i18n.translate(violation.message())).append("\n");
            }
        }
        return sb.toString();
    }


    private void setError(List<Violation> violations) {
        for(Form form : this.forms) {
            form.setError(violations);
        }
    }
}
