/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;
import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.entity.Code;
import org.uweaver.search.*;
import org.uweaver.core.util.*;
import org.uweaver.service.CodeService;
import org.uweaver.ui.DataBinding;
import org.uweaver.ui.Selectable;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SortEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Grid obj = new Grid();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Grid extends AbstractHtmlWrapper<org.zkoss.zul.Grid> implements Input<Collection>, DataBinding<Collection>, Selectable<Model> {
    private static final Logger LOGGER = LogManager.getLogger(Grid.class);
    private Collection data = new Collection();
    private Converters converters = new Converters();
    private Form editor = null;
    private Collection selection = new Collection();
    private boolean allowNullSelection = true;
    private Paging paging;
    private String state = null;
    private boolean readonly = false;
    private NumberFormat numberFormat = NumberFormat.getInstance();

    @WireVariable
    private CodeService codeService;

    public Grid() {
        this(new org.zkoss.zul.Grid());
    }

    public Grid(org.zkoss.zul.Grid grid) {
        super(grid);
        Foot foot = this.component.getFoot();
        if(foot==null) {
            foot = new Foot();
            this.appendChild(foot);
        }
        Footer footer = new Footer();
        foot.appendChild(footer);
        this.paging = new Paging();
        this.paging.setStyle("border-style: none");
        footer.setSpan(columnCount());
        footer.appendChild(paging);
        paging.addEventListener("onPaging", event -> {
            int page = ((PagingEvent) event).getActivePage();
            int size = this.data.pageable().size();
            Pageable pageRequest = new PageRequest(page, size);
            this.data.fetch(pageRequest);
        });

        List<Column> columns = this.columns();
        for(Column column : columns) {
            column.component().addEventListener("onSort", this::onSort);
        }

        converters.setDateTimePattern("yyyy/MM/dd");
        this.enhance("auxheader, columns > column");

        wire();

        render();
    }

    public List<Column> columns() {
        List<Column> columns = new ArrayList<>();
        List<org.zkoss.zul.Column> rawColumns = this.component.getColumns().getChildren();
        for(org.zkoss.zul.Column rawColumn : rawColumns) {
            columns.add(new Column(rawColumn));
        }
        return columns;
    }

    public Column column(String dataIndex) {
        Column target = null;
        List<Column> columns = this.columns();
        for(Column column : columns) {
            if(dataIndex.equals(column.getAttribute("dataIndex"))) {
                target = column;
                break;
            }
        }
        return target;
    }

    private void onSort(Event event) {
        SortEvent sortEvent = (SortEvent) event;
        Column column = new Column((org.zkoss.zul.Column) sortEvent.getTarget());
        String dataIndex = (String) column.getAttribute("dataIndex");
        Sort.Direction direction = sortEvent.isAscending() ? Sort.Direction.ASC : Sort.Direction.DESC;
        clearSortDirection();
        column.setSortDirection(direction);
        Sorter sorter = new Sorter().by(direction, dataIndex);
        this.data.fetch(sorter);
        event.stopPropagation();
    }

    private void clearSortDirection() {
        List<Column> columns = this.columns();
        for(Column column : columns) {
            column.clearSortDirection();
        }
    }

    private int columnCount() {
        return this.columns().size();
    }

    public void touch(int index, String dataIndex) {
        this.touch(this.data.at(index), dataIndex);
    }

    public void touch(Model model, String dataIndex) {
        setEditorData(model);
        this.toggle(model, dataIndex);
    }

    private void setEditorData(Model model) {
        if(this.editor!=null) {
            this.editor.setData(model);
        }
    }

    private void clearEditorData() {
        if(this.editor!=null) {
            this.editor.setData(new Model());
        }
    }

    public void toggle(int index, String dataIndex) {
        toggle(data.at(index), dataIndex);
    }

    public void toggle(Model model, String dataIndex) {
        if(this.selection.contains(model)) {
            this.deselect(model, dataIndex);
        } else {
            this.select(model, dataIndex);
        }

        Model event = new Model();
        event.put("data", model);
        event.put("dataIndex", dataIndex);
        this.trigger("onTouch", event);
    }

    public void select(Model model, String dataIndex) {
        if(this.mode().equals(Mode.NONE)) return;

        if(this.mode().equals(Mode.SINGLE)&&this.selection.size()>0) {
            int index = this.data.indexOf(this.selection.at(0));
            if(index!=-1) {
                Row row = (Row) this.component.getRows().getChildren().get(index);
                row.setSclass(null);
            }
            this.selection.remove(0);
        }

        this.selection.add(model);
        int index = this.data.indexOf(model);
        Row row = (Row) this.component.getRows().getChildren().get(index);
        mark(row);
        this.trigger("onSelect", model);

        Model event = new Model();
        event.put("data", model);
        event.put("dataIndex", dataIndex);
        this.trigger("onMark", event);
    }

    public void deselect(Model model, String dataIndex) {
        if(this.allowNullSelection || this.selection.size()>1) {
            this.selection.remove(model);
            int index = this.data.indexOf(model);
            Row row = (Row) this.component.getRows().getChildren().get(index);
            unmark(row);
            this.trigger("onDeselect", model);
            Model event = new Model();
            event.put("data", model);
            event.put("dataIndex", dataIndex);
            this.trigger("onUnmark", event);
        }
    }

    private void mark(Row row) {
        row.setSclass("uw-mark");
    }

    private void unmark(Row row) {
        row.setSclass(null);
    }

    public void select(int index, String dataIndex) {
        select(data.at(index), dataIndex);
    }

    public void deselect(int index, String dataIndex) {
        deselect(data.at(index), dataIndex);
    }

    @Override
    public Grid render() {
        List<Column> columns = this.columns();
        Rows rows = component.getRows();
        if(rows!=null) {
            component.removeChild(rows);
        }
        rows = new Rows();
        component.appendChild(rows);

        if(data!=null && data.size()>0 && data.isPaginal()) {
            Long totalSize = data.count();
            int page = this.data.pageable().page();

            this.paging.setTotalSize(totalSize>Integer.MAX_VALUE ? Integer.MAX_VALUE : totalSize.intValue());
            this.paging.setPageSize(data.pageable().size());
            this.paging.setActivePage(page);
            this.component.getFoot().setVisible(true);
        } else {
            this.component.getFoot().setVisible(false);
        }

        if(data==null || data.size()==0) {
            Row row = new Row();
            Cell cell = new Cell();
            cell.setStyle("padding-left: 10px;padding-right:10px;");
            cell.setColspan(columns.size());
            Label label = new Label("No data.");
            row.appendChild(cell);
            cell.appendChild(label);
            rows.appendChild(row);
            return this;
        }

        int index = 0;
        for(Model model : data) {
            Row row = new Row();
            Model found;
            if(model.id()==null) {
                found = selection.contains(model) ? model : null;
            } else {
                found = selection.find(new Predicate(model.idAttribute(), model.id()));
            }
            if(found!=null) {
                selection.remove(found);
                selection.add(model);
                mark(row);
            }
            for(Column column : columns) {
                String dataIndex = (String) column.getAttribute("dataIndex");
                boolean editable = converters.convert(column.getAttribute("editable"), Boolean.class, false);
                String type = converters.convert(column.getAttribute("type"), String.class, "string");
                String subtype = converters.convert(column.getAttribute("subtype"), String.class);
                String format = converters.convert(column.getAttribute("format"), String.class);
                Object value = (dataIndex==null) ? null : model.get(dataIndex);
                String code = (String) column.getAttribute("code");
                StringBuilder style = new StringBuilder("padding-left: 10px;padding-right:10px;");
                Cell cell = new Cell();
                cell.setAttribute("dataIndex", dataIndex);

                row.appendChild(cell);
                if(!editable) {
                    cell.appendChild(renderLabel(value, subtype, format, code));
                } else {
                    Input input = (code==null) ? renderEditor(value, type, subtype) : renderSelectable(value, code);
                    input.setAttribute("dataIndex", dataIndex);
                    input.setAttribute("model", model);
                    Component component = (input instanceof Wrapper) ? ((Wrapper) input).component() : input;
                    cell.appendChild(component);
                }
                if(resolveType(value, type).equals("number")) {
                    style.append("text-align:right;");
                }
                cell.setStyle(style.toString());
                cell.addEventListener("onClick", event -> onClick(event));
            }
            row.setAttribute("index", index++);
            rows.appendChild(row);
        }

        return this;
    }

    private void onClick(Event event) {
        Cell cell = (Cell) event.getTarget();
        Row row = (Row) cell.getParent();
        int index = (int) row.getAttribute("index");
        String dataIndex = (String) cell.getAttribute("dataIndex");
        this.touch(index, dataIndex);
    }

    private Input renderSelectable(Object value, String code) {
        Selectbox input = new Selectbox();

        input.setCode(code);

        input.setValue(value);

        input.setWidth("100%");
        input.setInplace(true);
        return input;
    }

    private Input renderEditor(Object value, String defaultType, String defaultSubtype) {
        Input input;
        String type = resolveType(value, defaultType);
        String subtype = resolveSubtype(value, defaultSubtype);
        if(type.equalsIgnoreCase("string")) {
            input = new Textbox();
        } else if(type.equalsIgnoreCase("number")) {
            input = new Numberbox();
        } else if(type.equalsIgnoreCase("date")) {
            input = new Datebox();
        } else {
            input = new Textbox();
        }

        input.setValue(value);

        input.addEventListener("onChange", this::onEditorChange);

        input.setWidth("100%");
        input.setInplace(true);

        return input;
    }

    private void onEditorChange(Event event) {
        Input input = (Input) event.getTarget();
        Object value = event.getData();
        Model model = (Model) input.getAttribute("model");
        String dataIndex = input.getAttributeAsString("dataIndex");
        model.set(dataIndex, value);
    }


    private String resolveType(Object value, String defaultType) {
        String type = defaultType;
        Class clazz = (value==null) ? String.class : value.getClass();

        if(String.class.isAssignableFrom(clazz)) {
            type = "string";
        } else if(Number.class.isAssignableFrom(clazz)) {
            type = "number";
        } else if(Date.class.isAssignableFrom(clazz)) {
            type = "date";
        } else if(Boolean.class.isAssignableFrom(clazz)) {
            type = "boolean";
        }

        return type;
    }

    private String resolveSubtype(Object value, String defaultSubtype) {
        String subtype = defaultSubtype;
        Class clazz = (value==null) ? String.class : value.getClass();

        return subtype;
    }

    private Label renderLabel(Object value, String subtype, String format, String codeType) {
        Label label = new Label(format(value, subtype, format, codeType));
        label.setWidth("100%");
        return label;
    }

    private String format(Object value, String subtype, String format, String codeType) {
        if(value==null) return "";

        String text;

        if(String.class.isAssignableFrom(value.getClass())) {
            text = (codeType==null) ? (String) value : toCodeText(codeType, (String) value);

        } else if(Date.class.isAssignableFrom(value.getClass())) {
            text = formatDate((Date) value, subtype, format);

        } else if(java.util.Collection.class.isAssignableFrom(value.getClass())) {
            StringBuilder sb = new StringBuilder();
            java.util.Collection data = (java.util.Collection) value;
            for(Object datum : data) {
                String s = converters.convert(datum, String.class);
                if(codeType!=null) {
                    s = toCodeText(codeType, s);
                }
                sb.append(s).append(", ");
            }
            text = (data.size()>0) ? sb.substring(0, sb.length()-2) : "";
        } else if(Number.class.isAssignableFrom(value.getClass())) {
            text = formatNumber((Number) value, subtype, format);
        } else if(converters.isSupported(value.getClass())) {
            text = converters.convert(value, String.class);
        } else {
            text = value.toString();
        }
        return text;
    }

    private String formatDate(Date value, String subtype, String format) {
        String text;
        String dateTimePattern;
        if(format!=null) {
            dateTimePattern = format;
        } else if(subtype==null) {
            dateTimePattern = "yyyy/MM/dd";
        } else if(subtype.equalsIgnoreCase("datetime")) {
            dateTimePattern = "yyyy/MM/dd HH:mm:ss";
        } else if(subtype.equalsIgnoreCase("date")) {
            dateTimePattern = "yyyy/MM/dd";
        } else if(subtype.equalsIgnoreCase("time")) {
            dateTimePattern = "HH:mm:ss";
        } else {
            dateTimePattern = "yyyy/MM/dd";
        }

        converters.setDateTimePattern(dateTimePattern);
        text = converters.convert(value, String.class);

        return text;
    }

    private String formatNumber(Number value, String subtype, String format) {
        String text;
        String pattern;
        if(format!=null) {
            pattern = format;
	} else if(subtype==null) {
	    pattern = null;
        } else if(subtype.equalsIgnoreCase("money")) {
            pattern = "$##,##0.####";
        } else {
            pattern = null;
	}

        if(pattern!=null) {
            DecimalFormat formatter = new DecimalFormat(pattern);
            text = formatter.format(value);
        } else {
            text = value.toString();
        }

        return text;
    }

    private String toCodeText(String type, String code) {
        String text = code;
        Code bean = codeService.findOneByTypeAndCode(type, code);
        if(bean!=null && bean.getText()!=null) {
            text = bean.getText();
        }
        return text;
    }

    public void setData(Collection data) {
        stopListening(this.data);
        this.data = (data==null) ? new Collection() : data;
        listenTo(this.data, "reset", event -> reset());
        listenTo(this.data, "update", event -> render());
        render();
    }

    @Override
    public Collection data() {
        return this.data;
    }

    public void setData(List<?> data) {
        setData(data, true);
    }

    public void setData(List<?> data, boolean reset) {
        this.data.reset(data);
        if(reset) {
            this.reset();
        } else {
            this.render();
        }
    }

    public void reset() {
        this.selection.clear();
        this.render();
    }

    @Override
    public void setWidth(String width) {
        component.setWidth(width);
    }

    @Override
    public String name() {
        return (String) component.getAttribute("name");
    }

    @Override
    public Collection value() {
        return data();
    }

    @Override
    public Input setValue(Collection value) {
        setData(value);
        return this;
    }

    @Override
    public Input setInplace(boolean inplace) {
        return this;
    }

    @Override
    public String predicate() {
        return this.getAttributeAsString("predicate");
    }

    public void setEditor(Form editor) {
        this.editor = editor;
        editor.addEventListener("onChange", event -> render());
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    public String errorMessage() {
        return "";
    }

    public void add() {
        add(new Model());
    }

    public void add(Object bean) {
        Model model = (bean instanceof Model) ? (Model) bean : new Model(bean);
        this.data.add(model);
        this.render();
        this.select(model, null);
    }

    public void remove() {
        this.data.removeAll(this.selection);
        this.selection.clear();
        this.render();
        this.clearEditorData();
    }

    @Override
    public Mode mode() {
        return this.hasAttribute("mode") ? Mode.valueOf(this.getAttributeAsString("mode").toUpperCase()) : Mode.SINGLE;
    }

    public Grid setMode(Mode mode) {
        this.setAttribute("mode", mode.name());
        return this;
    }

    public Grid setAllowNullSelection(boolean allowNullSelection) {
        this.allowNullSelection = allowNullSelection;
        return this;
    }

    public boolean allowNullSelection() {
        return this.allowNullSelection;
    }

    public Model singleSelection() {
        return this.selection.size() > 0 ? this.selection.at(0) : null;
    }

    @Override
    public Collection selection() {
        return new Collection(this.selection);
    }

    public boolean hasSelection() {
        return this.selection.size()>0;
    }

    @Override
    public String subject() {
        return this.getAttributeAsString("subject");
    }

    public void clear() {
        this.data.clear();
        this.reset();
    }

    @Override
    public void setState(String state) {
        String readonlyWhen = readonlyWhen();
        if(readonlyWhen!=null) {
            setReadonly(readonlyWhen().equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    // TODO: 02/12/2017 complete the implementation of readonly feature.
    @Override
    public void setReadonly(boolean readonly) {
        this.readonly = true;
    }

    @Override
    public boolean isReadonly() {
        return this.readonly;
    }

    @Override
    public String readonlyWhen() {
        return this.getAttributeAsString("readonlyWhen");
    }

    @Override
    public boolean isRequired() {
        return getAttributeAsBoolean("required", false);
    }


    public class Column extends AbstractHtmlWrapper<org.zkoss.zul.Column> {
        public Column(org.zkoss.zul.Column component) {
            super(component);
        }

        public void setSortDirection(Sort.Direction direction) {
            String sortDir = "natural";
            if(direction.equals(Sort.Direction.ASC)) {
                sortDir = "ascending";
            } else if(direction.equals(Sort.Direction.DESC)) {
                sortDir = "descending";
            }
            this.component.setSortDirection(sortDir);
        }

        public void clearSortDirection() {
            this.component.setSortDirection("natural");
        }
    }
}
