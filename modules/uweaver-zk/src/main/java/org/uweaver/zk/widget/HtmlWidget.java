package org.uweaver.zk.widget;

public interface HtmlWidget extends Widget {
    void setWidth(String width);

    void setHeight(String height);

    @Override
    String getWidth();

    @Override
    String getHeight();

    void setSclass(String sclass);

    String getSclass();

    void setStyle(String style);
}
