package org.uweaver.zk.widget;

public class Image extends AbstractHtmlWrapper<org.zkoss.zul.Image> {

    public Image() {
        this(new org.zkoss.zul.Image());
    }

    public Image(org.zkoss.zul.Image image) {
        super(image);
    }

    public void setSrc(String src) {
        this.component.setSrc(src);
    }

    public String src() {
        return this.component.getSrc();
    }
}
