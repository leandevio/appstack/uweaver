package org.uweaver.zk.widget;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

import java.util.ArrayList;
import java.util.List;

public class Menubar extends AbstractHtmlWrapper<org.zkoss.zul.Menubar> {
    private final static String SELECTOR = "menu,menuitem";
    private List<Control> controls = new ArrayList<>();
    private String state = null;

    public Menubar() {
        this(new org.zkoss.zul.Menubar());
    }

    public Menubar(org.zkoss.zul.Menubar component) {
        super(component);

        render();
    }

    private void add(Control control) {
        if(control==null||control.action()==null) return;
        this.controls.add(control);
        control.addEventListener("onAction", event -> onAction(event));
    }

    private Control buildControl(org.zkoss.zul.Menuitem component) {
//        if(component.getAttribute("action")==null) return null;

        Control control = new Menuitem(component);

        return control;
    }


    private Control buildControl(org.zkoss.zul.Menu component) {
//        if(component.getAttribute("action")==null) return null;

        Control control = new Menu(component);

        return control;
    }

    private void onAction(Event event) {
        Control control = (Control) event.getTarget();
        this.trigger("onAction", event.getData());
        this.trigger("on" + capitalize(control.action()), event.getData());
    }

    private String capitalize(String str) {
        char c[] = str.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        return new String(c);
    }

    @Override
    public Menubar render() {
        super.render();
        for (Component element : this.queryAll(SELECTOR)) {
            Control control;
            if (org.zkoss.zul.Menuitem.class.isAssignableFrom(element.getClass())) {
                control = buildControl((org.zkoss.zul.Menuitem) element);
            } else if (org.zkoss.zul.Menu.class.isAssignableFrom(element.getClass())) {
                control = buildControl((org.zkoss.zul.Menu) element);
            } else {
                continue;
            }
            if(control!=null) add(control);
        }
        return this;
    }

    public Control control(String action) {
        Control control = null;
        for (Control element : controls) {
            if (element.action().equalsIgnoreCase(action)) {
                control = element;
                break;
            }
        }
        return control;
    }

    public void setState(String state) {
        this.state = state;
        for(Control control : controls) {
            control.setState(state);
        }
    }

    public String state() {
        return this.state;
    }
}
