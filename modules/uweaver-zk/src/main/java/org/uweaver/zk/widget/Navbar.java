/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Model;
import org.uweaver.search.Predicate;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Nav;
import org.zkoss.zkmax.zul.Navitem;


/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Navbar obj = new Navbar();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Navbar extends AbstractHtmlWrapper<org.zkoss.zkmax.zul.Navbar> {
    private Collection data;

    public Navbar() {
        this(new org.zkoss.zkmax.zul.Navbar());
    }

    public Navbar(org.zkoss.zkmax.zul.Navbar window) {
        super(window);
        this.component.setOrient("vertical");
        setData(new Collection());
    }

    public void setData(Collection data) {
        stopListening(this.data);
        this.data = (data==null) ? new Collection() : data;
        listenTo(this.data, "reset", event -> render());
        listenTo(this.data, "update", event -> render());
        render();
    }

    public Collection data() {
        return this.data;
    }

    @Override
    public Navbar render() {
        super.render();

        Collection items = data.filter(new Predicate("folder", null));
        items.sort((left, right) -> Double.compare(right.getAsDouble("priority"), left.getAsDouble("priority")));
        for(Model any : items) {
            append(renderAny(any));
        }
        return this;
    }

    private HtmlBasedComponent renderAny(Model any) {
        HtmlBasedComponent node;
        if (any.has("isFolder") && any.getAsBoolean("isFolder")) {
            node = renderFolder(any);
        } else {
            node = renderItem(any);
        }
        return node;
    }

    private Nav renderFolder(Model folder) {
        Nav nav = new Nav(translate(folder.getAsString("title")));
        String icon = folder.getAsString("icon", "gear");
        nav.setIconSclass("z-icon-" + icon);
        nav.setAttribute("data", folder);

        if(folder.get("id")==null) return nav;

        Predicate predicate = new Predicate("folder", folder.get("id"));
        Collection items = data.filter(predicate);
        for(Model any : items) {
            HtmlBasedComponent node = renderAny(any);
            node.setStyle("padding-left: 24px;");
            nav.appendChild(node);
        }
        return nav;
    }

    private Navitem renderItem(Model item) {
        Navitem navitem = new Navitem();
        String icon = item.getAsString("icon", "gear");
        navitem.setIconSclass("z-icon-" + icon);
        navitem.setAttribute("data", item);

        navitem.setLabel(translate(item.getAsString("title")));
        navitem.setStyle("color: blue;");
        if(item.getAsBoolean("disabled", false)) navitem.setDisabled(true);
        if(!navitem.isDisabled() && item.getAsBoolean("lock", false)) navitem.setDisabled(true);
        navitem.addEventListener(Events.ON_CLICK, this::onAction);
        return navitem;
    }

    private void onAction(Event event) {
        Object data = event.getTarget().getAttribute("data");
        this.trigger("onSelect", data);
    }
}
