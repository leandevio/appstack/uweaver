package org.uweaver.zk.widget;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Panelchildren;

public class Panel extends AbstractHtmlWrapper<org.zkoss.zul.Panel> {
    public Panel() {
        this(new org.zkoss.zul.Panel());
    }

    public Panel(org.zkoss.zul.Panel panel) {
        super(panel);
        if(panel.getPanelchildren()==null) {
            Panelchildren panelchildren = new Panelchildren();
            panelchildren.setParent(panel);
        }
        panel.addEventListener("onClick", event -> {
            this.click();
        });
    }

    public void setTitle(String title) {
        this.component.setTitle(title);
    }

    public void setFloatable(boolean floatable) {
        this.component.setFloatable(floatable);
    }

    public void setClosable(boolean closable) {
        this.component.setClosable(closable);
    }

    public void setCollapsible(boolean collapsible) {
        this.component.setCollapsible(collapsible);
    }

    public void setMovable(boolean movable) {
        this.component.setMovable(movable);
    }

    public void setSizable(boolean sizable) {
        this.component.setSizable(sizable);
    }

    public void click() {
        this.trigger("onClick", null);
    }

    @Override
    public boolean appendChild(Component component) {
        Panelchildren panelchildren = this.component.getPanelchildren();
        Component element = component;
        if(component instanceof Wrapper) {
            element = ((Wrapper) component).component();
        }
        return panelchildren.appendChild(element);
    }

    public void setLeft(int left) {
        this.setLeft(left + "px");
    }

    public void setLeft(double percent) {
        this.setLeft(percent + "%");
    }

    public void setLeft(String left) {
        this.component.setLeft(left);
    }

    public String getLeft() {
        return this.component.getLeft();
    }

    public void setRight(int right) {
        this.setRight(right + "px");
    }

    public void setRight(double percent) {
        this.setRight(percent + "%");
    }

    public void setRight(String right) {
        this.component.setStyle("right: " + right);
    }

    public String getRight() {
        return this.component.getLeft();
    }

    public void setTop(String top) {
        this.component.setTop(top);
    }

    public void setTop(int top) {
        this.setTop(top + "px");
    }

    public void setTop(double percent) {
        this.setTop(percent + "%");
    }

    public String getTop() {
        return this.component.getTop();
    }
}