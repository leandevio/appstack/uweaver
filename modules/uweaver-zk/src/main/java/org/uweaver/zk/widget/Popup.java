/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.i18n.I18n;
import org.uweaver.ui.DataBinding;
import org.uweaver.ui.Position;
import org.uweaver.ui.Selectable;
import org.uweaver.ui.Validatable;
import org.uweaver.zk.router.Router;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import org.zkoss.zul.Window;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *  *
 * The implementation provides:
 * - ...t
 * - ...
 *
 * Usage:
 *
 * ```java
 * Popup obj = new Popup();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Popup extends AbstractHtmlWrapper<org.zkoss.zul.Popup> {
    private static final Logger LOGGER = LogManager.getLogger(Popup.class);
    private static Map<Position, String> positionMapping = new HashMap<>();

    static {
        positionMapping.put(Position.BOTTOM_LEFT, "after_start");
        positionMapping.put(Position.BOTTOM_CENTER, "after_center");
        positionMapping.put(Position.BOTTOM_RIGHT, "after_right");
    }

    public Popup() {
        this(new org.zkoss.zul.Popup());
    }

    public Popup(org.zkoss.zul.Popup popup) {
        super(popup);
    }

    public void open(Widget widget, Position position) {
        Component component = (widget instanceof Wrapper) ? ((Wrapper) widget).component() : widget;
        this.component().open(component, positionMapping.get(position));
    }

    public static void alert(Object message, String title) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        String caption = (title==null) ? i18n.translate("Alert") : i18n.translate(title);
        Messagebox.show(toText(message), caption, Messagebox.OK, Messagebox.ERROR);
    }

    public static void alert(Object message) {
        alert(message, null);
    }

    public static void warn(Object message, String title) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        String caption = (title==null) ? i18n.translate("Warning") : i18n.translate(title);
        Messagebox.show(toText(message), caption, Messagebox.OK, Messagebox.EXCLAMATION);
    }

    public static void warn(Object message, String title, Runnable ok) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        String caption = (title==null) ? i18n.translate("Warning") : i18n.translate(title);
        Messagebox.show(toText(message), caption, Messagebox.OK | Messagebox.CANCEL, Messagebox.EXCLAMATION, event->{
            if (event.getName().equals("onOK")) {
                if(ok!=null) ok.run();
            }
        });
    }

    public static void warn(Object message) {
        warn(message, null);
    }

    public static void info(Object message, String title) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        String caption = (title==null) ? i18n.translate("Information") : i18n.translate(title);
        Messagebox.show(toText(message), caption, Messagebox.OK, Messagebox.INFORMATION);
    }

    public static void info(Object message) {
        info(message, null);
    }

    public static void notify(Object message, String title) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        String caption = (title==null) ? i18n.translate("Notification") : i18n.translate(title);
        Messagebox.show(toText(message), caption, Messagebox.OK, Messagebox.INFORMATION);
    }

    public static void notify(Object message) {
        notify(message, null);
    }

    public static void confirm(Object message, String title, Runnable ok) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        String caption = (title==null) ? i18n.translate("Confirmation") : i18n.translate(title);
        Messagebox.show(toText(message), caption, Messagebox.OK | Messagebox.CANCEL, Messagebox.INFORMATION,event->{
            if (event.getName().equals("onOK")) {
                if(ok!=null) ok.run();
            }
        });
    }


    public static void prompt(Widget widget, String title) {
        prompt(widget, null, null, null, title);
    }

    public static void prompt(Widget widget, Consumer<Object> resolve) {
        prompt(widget, resolve, null, null, null);
    }

    public static void prompt(Widget widget, Consumer<Object> resolve, String title) {
        prompt(widget, resolve, null, null, title);
    }

    public static void prompt(Widget widget, Consumer<Object> resolve, Predicate<Object> validate) {
        prompt(widget, resolve, validate, null, null);
    }

    public static void prompt(Widget widget, Consumer<Object> resolve, Predicate<Object> validate, String title) {
        prompt(widget, resolve, validate, null, title);
    }

    public static void prompt(Widget widget, Consumer<Object> resolve, Predicate<Object> validate, Runnable reject, String title) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        Window window = new Window();
        window.setPosition("center,center");
        window.setClosable(true);
        window.setAction("show: slideDown;hide: slideUp");
        window.setBorder("normal");
        if(widget.getWidth()==null) {
            window.setWidth("60%");
        }
        window.setTitle(title==null ? i18n.translate("Prompt") : i18n.translate(title));
        Component component = (widget instanceof Wrapper) ? ((Wrapper) widget).component() : widget;
        window.appendChild(component);
        Trigger trigger = buildTrigger();
        window.appendChild(trigger.component());
        window.setPage(Executions.getCurrent().getDesktop().getFirstPage());
        trigger.addEventListener("onResolve", event -> {
            if(Validatable.class.isAssignableFrom(widget.getClass()) && !((Validatable) widget).validate()) {
                Popup.warn(((Validatable) widget).errorMessage());
                return;
            }

            Object data = null;
            if(Selectable.class.isAssignableFrom(widget.getClass())) {
                Selectable selectable = (Selectable) widget;
                if(selectable.selection().size()==0) return;
                data = selectable.selection();
            } else if(DataBinding.class.isAssignableFrom(widget.getClass())) {
                data = ((DataBinding) widget).data();
            }

            if(validate!=null && !validate.test(data)) return;

            window.detach();
            resolve.accept(data);
        });
        trigger.addEventListener("onReject", event -> {
            window.detach();
            if(reject!=null) reject.run();
        });
        window.doModal();
    }

    private static Trigger buildTrigger() {
        Div div = new Div();
        Hbox hbox = new Hbox();
        hbox.setWidth("100%"); hbox.setPack("end");
        org.zkoss.zul.Button resolve = new org.zkoss.zul.Button();
        resolve.setLabel("OK"); resolve.setAttribute("action", "resolve"); resolve.setSclass("uw-button-default");
        org.zkoss.zul.Button reject = new org.zkoss.zul.Button();
        reject.setLabel("Cancel"); reject.setAttribute("action", "reject"); reject.setSclass("uw-button-caution");
        hbox.appendChild(resolve); hbox.appendChild(reject);
        div.appendChild(new Separator());
        div.appendChild(hbox);
        Trigger trigger = new Trigger(div);
        return trigger;
    }

    private static String toText(Object data) {
        Router router = (Router) Path.getComponent("/").getAttribute("router");
        I18n i18n = new I18n(router.locale());
        if(data==null) return "";

        String message;
        if(Collection.class.isAssignableFrom(data.getClass())) {
            StringBuilder messages = new StringBuilder();
            for(Object datum : (Collection) data) {
                messages.append(i18n.translate(datum.toString())).append("\n");
            }
            message = messages.toString();

        } else {
            message = i18n.translate(data.toString());
        }

        return message.toString();

    }

}