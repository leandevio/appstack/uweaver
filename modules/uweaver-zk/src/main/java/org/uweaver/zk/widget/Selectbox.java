/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.util.Collection;
import org.uweaver.core.util.Model;
import org.uweaver.service.CodeService;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Selectbox obj = new Selectbox();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Selectbox<T> extends AbstractHtmlWrapper<Combobox> implements Input<T> {
    private static String REQUIRED = "This field is required.";
    private List data = null;
    private String state = null;
    private String valueField, textField;
    private T value = null;
    private List<Option> options = new ArrayList<>();
    private ListModelList<Option> listModel = new ListModelList(options);

    @WireVariable
    private CodeService codeService;

    public Selectbox() {
        this(new Combobox());
    }

    public Selectbox(Combobox combobox) {
        super(combobox);
        setupConstraint();
        if(this.component.isReadonly()) this.component.setDisabled(true);
        this.component.setReadonly(true);
        this.component.setModel(this.listModel);
        this.component.addEventListener("onSelect", this::onComponentSelect);
        valueField = getAttributeAsString("valueField");
        textField = getAttributeAsString("textField");
        wire();
        String code = code();
        if(code!=null) {
            fetchCode(code);
        } else {
            setData(new Collection());
        }
    }

    private void setupConstraint() {
        StringBuilder constraint = new StringBuilder();
        if(isRequired()) {
            constraint.append("no empty: " + translate(REQUIRED));
        }
        this.component.setConstraint(constraint.toString());
    }

    public String code() {
        return getAttributeAsString("code");
    }

    public void setCode(String code) {
        setAttribute("code", code);
        fetchCode(code);

    }

    private void fetchCode(String type) {
        valueField = "code";
        textField = "text";
        setData(codeService.findAllByType(type));
    }

    private void onComponentSelect(Event event) {
        setValue((T) parse());
    }

    private Object parse() {
        int cursor = this.component.getSelectedIndex();
        Object value;

        if(cursor<0) {
            value = null;
        } else {
            Option option = (Option) this.component.getModel().getElementAt(cursor);
            value = option.value();
        }

        return value;
    }

    public Selectbox render() {
        options.clear();
        double maxPriority = 0;

        for (Object datum : data) {
            Model model = new Model(datum);
            Object value = (valueField == null) ? datum : model.get(valueField);
            String text = (textField == null) ? datum.toString() : model.getAsString(textField);
            double priority = model.has("priority") ? model.getAsDouble("priority") : 0;
            maxPriority = Math.max(maxPriority, priority);
            Option option = new Option(value, text, priority);
            options.add(option);
        }
        options.add(new Option(null, "", maxPriority+1));

        options.sort((option1, option2) -> {
            if(option1.priority()==option2.priority()) return 0;
            return option1.priority()<option2.priority() ? 1 : -1 ;
        });


        this.listModel.clear();
        this.listModel.addAll(options);

        renderSelection();

        return this;
    }

    @Override
    public String name() {
        return component.getName();
    }

    @Override
    public T value() {
        return this.value;
    }

    @Override
    public Selectbox setValue(T value) {
        if(this.value==null&&value==null) {
            return this;
        } else if(this.value!=null&&this.value.equals(value)) {
            return this;
        }

        Object previousValue = this.value;

        this.value = value;

        renderSelection();

        this.trigger("onChange", previousValue);

        return this;
    }

    private void renderSelection() {
        this.listModel.clearSelection();

        if(this.value==null) return;

        for(Option option: this.options) {
            if(this.value.equals(option.value())) {
                this.listModel.addToSelection(option);
                break;
            }
        }
    }


    @Override
    public Selectbox setInplace(boolean inplace) {
        this.component.setInplace(inplace);
        return this;
    }

    @Override
    public void setWidth(String width) {
        component.setWidth(width);
    }

    public void setData(List data) {
        if (this.data!=null && Collection.class.isAssignableFrom(this.data.getClass())) {
            stopListening((Collection) this.data);
        }
        this.data = (data==null) ? new Collection() : data;
        if (Collection.class.isAssignableFrom(this.data.getClass())) {
            listenTo((Collection) this.data, "change", event -> this.render());
        }
        this.render();
    }

    @Override
    public boolean validate() {
        return this.component.isValid();
    }

    @Override
    public String errorMessage() {
        String errorMessage = this.component.getErrorMessage();
        return (errorMessage==null) ? REQUIRED : errorMessage;
    }

    @Override
    public String predicate() {
        return this.getAttributeAsString("predicate");
    }

    @Override
    public void reset() {
        setValue(null);
        this.component.clearErrorMessage();
    }

    @Override
    public String subject() {
        return this.getAttributeAsString("subject");
    }

    @Override
    public void setState(String state) {
        String readonlyWhen = readonlyWhen();
        if(readonlyWhen!=null) {
            setReadonly(readonlyWhen().equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    @Override
    public void setReadonly(boolean readonly) {
        this.component.setDisabled(readonly);
    }

    @Override
    public boolean isReadonly() {
        return this.component.isDisabled();
    }

    @Override
    public String readonlyWhen() {
        return this.getAttributeAsString("readonlyWhen");
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name(), value());
    }

    @Override
    public boolean isRequired() {
        return getAttributeAsBoolean("required", false);
    }

    private class Option {
        private Object value = null;
        private String text = null;
        private double priority = 0;

        public Option(Object value, String text, double priority) {
            this.value = value;
            this.text = text;
            this.priority = priority;
        }

        public Object value() {
            return this.value;
        }

        public String text() {
            return this.text;
        }

        public double priority() {
            return this.priority;
        }

        @Override
        public String toString() {
            return (text == null) ? this.value.toString() : this.text;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Selectbox.Option)) return false;
            if(obj==null) return false;
            Option other = (Option) obj;
            return (this.hashCode()==other.hashCode());
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }
}

