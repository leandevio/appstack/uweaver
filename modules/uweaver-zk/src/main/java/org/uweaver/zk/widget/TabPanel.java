/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TabPanel obj = new TabPanel();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TabPanel extends AbstractHtmlWrapper<Tabbox> {
    private List<Wrapper> widgets = new ArrayList<>();

    public TabPanel() {
        this(new Tabbox());
    }

    public TabPanel(Tabbox tabbox) {
        super(tabbox);
        if(tabbox.getTabs()==null) {
            Tabs tabs = new Tabs();
            tabbox.appendChild(tabs);
        }
        if(tabbox.getTabpanels()==null) {
            Tabpanels tabpanels = new Tabpanels();
            tabbox.appendChild(tabpanels);
        }
        this.component.addEventListener("onSelect", event -> {
            Widget widget = this.current();
            this.trigger("onSelect", component);
            widget.focus();
        });
    }

    public int cursor() {
        return this.component.getSelectedIndex();
    }

    public Widget current() {
        return get(cursor());
    }

    public TabPanel add(Wrapper widget, String title, boolean closable, boolean selected) {
        Tabbox tabbox = this.component();
        Tab tab = new Tab(translate(title));
        Tabpanel tabpanel = new Tabpanel();
        tabbox.getTabs().appendChild(tab);
        tabbox.getTabpanels().appendChild(tabpanel);
        if(selected) tabbox.setSelectedTab(tab);

        if(closable) {
            tab.setClosable(closable);
            tab.addEventListener("onClose", event -> {
                this.remove(widget);
                this.trigger("onClose", widget);
            });
        }
        tabpanel.appendChild(widget.component());
        this.widgets.add(widget);
        return this;
    }

    public TabPanel add(Wrapper widget, String title, boolean closable) {
        return add(widget, title, closable, true);
    }

    public TabPanel add(Wrapper widget, String title) {
        return add(widget, title, false);
    }

    public void remove(Wrapper widget) {
        int index = -1;
        for(int i=0; i<widgets.size(); i++) {
            if(widgets.get(i).equals(widget)) {
                index = i;
                break;
            }
        }
        if(index!=-1) {
            if(this.component.getSelectedIndex()==index) {
                select(index-1);
            }
            List<Tabpanel> tabPanels = this.component.getTabpanels().getChildren();
            Tabpanel tabPanel = tabPanels.get(index);
            Tab tab = (Tab) this.component.getTabs().getChildren().get(index);
            this.component.getTabpanels().removeChild(tabPanel);
            this.component.getTabs().removeChild(tab);
            this.widgets.remove(widget);
        }
    }

    public void select(int index) {
        if(this.size()==0) return;
        int cursor = (index<0) ? 0 : index;
        this.component.setSelectedIndex(cursor);
        Widget widget = get(index);
        this.trigger("onSelect", widget);
        widget.focus();
    }

    public void select(Wrapper widget) {
        int index = -1;
        for(int i=0; i<widgets.size(); i++) {
            if(widgets.get(i).equals(widget)) {
                index = i;
            }
        }
        select(index);
        widget.focus();
    }

    public Widget get(int index) {
        return this.widgets.get(index);
    }

    public int size() {
        return this.widgets.size();
    }
}
