/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.zkoss.zk.ui.event.InputEvent;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Textbox obj = new Textbox();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Textbox extends AbstractHtmlWrapper<org.zkoss.zul.Textbox> implements Input<String> {
    private String state = null;
    private static String REQUIRED = "This field is required.";

    public Textbox() {
        this(new org.zkoss.zul.Textbox());
    }

    public Textbox(org.zkoss.zul.Textbox textbox) {
        super(textbox);

        setupConstraint();

        this.component.addEventListener("onChange", event -> change(((InputEvent)event).getValue()));
        this.component.addEventListener("onOK", event -> ok());

        render();
    }

    private void setupConstraint() {
        StringBuilder constraint = new StringBuilder();
        if(isRequired()) {
            constraint.append("no empty: " + translate(REQUIRED));
        }
        this.component.setConstraint(constraint.toString());
    }

    public void change(Object value) {
        this.trigger("onChange", value);
    }

    public void ok() {
        this.trigger("onOK", null);
    }
    @Override
    public String name() {
        return component.getName();
    }

    @Override
    public String value() {
        return (String) component.getRawValue();
    }

    @Override
    public Textbox setValue(String value) {
        component.setRawValue(value);
        return this;
    }

    @Override
    public Textbox setInplace(boolean inplace) {
        component.setInplace(inplace);
        return this;
    }

    @Override
    public boolean validate() {
        return this.component.isValid();
    }

    @Override
    public String errorMessage() {
        String errorMessage = this.component.getErrorMessage();
        return (errorMessage==null) ? REQUIRED : errorMessage;
    }

    @Override
    public void setWidth(String width) {
        component.setWidth(width);
    }

    @Override
    public String predicate() {
        return this.getAttributeAsString("predicate");
    }

    @Override
    public void reset() {
        this.component.clearErrorMessage();
    }

    @Override
    public String subject() {
        return this.getAttributeAsString("subject");
    }

    @Override
    public void setState(String state) {
        String readonlyWhen = readonlyWhen();
        if(readonlyWhen!=null) {
            setReadonly(readonlyWhen().equalsIgnoreCase(state));
        }
        this.state = state;
    }

    @Override
    public String state() {
        return this.state;
    }

    @Override
    public void setReadonly(boolean readonly) {
        this.component.setReadonly(readonly);
    }

    @Override
    public boolean isReadonly() {
        return this.component.isReadonly();
    }

    @Override
    public String readonlyWhen() {
        return getAttributeAsString("readonlyWhen");
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name(), value());
    }

    @Override
    public boolean isRequired() {
        return getAttributeAsBoolean("required", false);
    }
}
