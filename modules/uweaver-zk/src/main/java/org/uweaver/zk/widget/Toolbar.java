package org.uweaver.zk.widget;

public class Toolbar extends AbstractHtmlWrapper<org.zkoss.zul.Toolbar> {
    public Toolbar() {
        this(new org.zkoss.zul.Toolbar());
    }

    public Toolbar(org.zkoss.zul.Toolbar component) {
        super(component);
    }
}
