/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.impl.XulElement;

import java.util.ArrayList;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Trigger obj = new Trigger();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Trigger extends AbstractHtmlWrapper<XulElement> {
    private final static String SELECTOR = "button";
    private List<Control> controls = new ArrayList<>();
    private String state = null;

    public Trigger() {
        this(new Div());
    }

    public Trigger(XulElement xulElement) {
        super(xulElement);
        for (Component element : this.queryAll(SELECTOR)) {
            Control control;
            if (org.zkoss.zul.Button.class.isAssignableFrom(element.getClass())) {
                control = buildControl((org.zkoss.zul.Button) element);
            } else {
                continue;
            }
            if(control!=null) add(control);
        }
        render();
    }

    public void add(Control control) {
        if(control==null||control.action()==null) return;
        this.controls.add(control);
        control.addEventListener("onAction", event -> onAction(event));
    }

    private Control buildControl(org.zkoss.zul.Button component) {
        if(component.getAttribute("action")==null) return null;

        Control control;
        if(component.getUpload()!=null) {
            control = new Fileuploader(component);
        } else {
            control = new Button(component);
        }

        return control;
    }

    private void onAction(Event event) {
        Control control = (Control) event.getTarget();
        this.trigger("onAction", event.getData());
        this.trigger("on" + capitalize(control.action()), event.getData());
    }

    private String capitalize(String str) {
        char c[] = str.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        return new String(c);
    }

    @Override
    public Trigger render() {
        super.render();
        return this;
    }

    public Control control(String action) {
        Control control = null;
        for (Control element : controls) {
            if (element.action().equalsIgnoreCase(action)) {
                control = element;
                break;
            }
        }
        return control;
    }

    public void setState(String state) {
        this.state = state;
        for(Control control : controls) {
            control.setState(state);
        }
    }

    public String state() {
        return this.state;
    }
}