package org.uweaver.zk.widget;

import org.uweaver.core.event.Listenable;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

public interface Widget extends Component {
    Widget render();

    void focus();

    Object getWidth();

    Object getHeight();

    void trigger(String name, Object data);

    boolean append(Component component);

    void wire();

    <T> T query(String selector, Class<T> type);

    List<Component> queryAll(String selector);

    void show();

    void hide();

    boolean isVisible();

    String id();

    Object setAttribute(String name, Object value);

    void empty();

    boolean hasAttribute(String name);
    Object getAttribute(String name);
    Object getAttribute(String name, Object defaultValue);
    String getAttributeAsString(String name);
    String getAttributeAsString(String name, String defaultValue);
    Integer getAttributeAsInteger(String name);
    Integer getAttributeAsInteger(String name, Integer defaultValue);


    boolean addEventListener(java.lang.String evtnm,
                             EventListener<? extends Event> listener);

    Boolean getAttributeAsBoolean(String name);

    Boolean getAttributeAsBoolean(String name, Boolean defaultValue);

    void listenTo(Listenable listenable, String event, Consumer<org.uweaver.core.event.Event> fn);

    void stopListening();

    void stopListening(Listenable listenable);

    Locale locale();
    String translate(String text);
    void enhance(String selector);

}
