/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uweaver.zk.widget;

import org.uweaver.core.log.LogManager;
import org.uweaver.core.log.Logger;
import org.uweaver.ui.Position;
import org.zkoss.zk.ui.util.Clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Panel obj = new Panel();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Window extends AbstractHtmlWrapper<org.zkoss.zul.Window> {
    private static final Logger LOGGER = LogManager.getLogger(Window.class);
    public Window() {
        this(new org.zkoss.zul.Window());
    }

    public Window(org.zkoss.zul.Window window) {
        super(window);
        window.setBorder(false);
    }

    public void popup(boolean dismissable, Position position) {
        if(dismissable) {
            this.component.doPopup();
        } else {
            this.component.doOverlapped();
        }
        move(position);
    }

    public void popup() {
        popup(true, Position.MIDDLE_CENTER);
    }

    public void popup(Position position) {
        popup(true, position);
    }

    public void popup(boolean dismissable) {
        popup(dismissable, Position.MIDDLE_CENTER);
    }

    public void highlight(Position position) {
        this.component.doHighlighted();
        move(position);
    }

    public void highlight() {
        highlight(Position.MIDDLE_CENTER);
    }

    public void overlap(Position position) {
        this.component.doOverlapped();
        move(position);
    }

    public void overlap() {
        overlap(Position.MIDDLE_CENTER);
    }

    public void move(Position position) {
        String pos = "parent,center";
        if(position==Position.MIDDLE_CENTER) {
            pos = "parent,center";
        } else if(position==Position.TOP_CENTER) {
            pos = "parent,top,center";
        } else if(position==Position.BOTTOM_CENTER) {
            pos = "parent,bottom,center";
        }

        this.component.setPosition(pos);
    }

    public void move(String left, String top) {
        this.component.setLeft(left);
        this.component.setTop(top);
    }

    public void setBorder(boolean border) {
        this.component.setBorder(border);
    }

    public void setShadow(boolean shadow) {
        this.component.setShadow(shadow);
    }
}