var scrolled = false;
function calibrate()  {
    if(isIPhone()) {
        (scrolled) || window.scrollTo(0, 0);
        scrolled = true;
        var elements = document.querySelectorAll('.z-window-modal, .z-messagebox-window');
        elements.forEach(function(element) {
            if(!element.hasAttribute('calibrated')||isOutOfBound(element)) {
                calibrateWindow(element);
                element.setAttribute('calibrated', 'true');
            }
        });
    }

    var grids = document.querySelectorAll('.z-grid');
    grids.forEach(function(grid, index) {
        if(!grid.hasAttribute('calibrated')) {
            calibrateGrid(grid);
            grid.setAttribute('calibrated', 'true');
        }
    });

    setTimeout(calibrate, 1000);
}

function isIPhone() {
    return navigator.userAgent.indexOf('iPhone') !== -1;
}

function calibrateWindow(element) {
    var left = window.scrollX;
    if((window.visualViewport.width > element.offsetWidth)) {
        left += (window.visualViewport.width - element.offsetWidth) / 2;
    }
    element.style.left = left + 'px';
    var top = window.scrollY;
    if(window.visualViewport.height > element.offsetHeight) {
        top += (window.visualViewport.height - element.offsetHeight) / 2;
    }
    element.style.top = top + 'px';
}

function isOutOfBound(element) {
    var top = window.scrollY;
    var bottom = top + window.visualViewport.height;
    var left = window.scrollX;
    var right = left + window.visualViewport.width;
    var test = false;

    if(element.offsetTop>=bottom||element.offsetLeft>=right) {
        test = true;
    } else if(element.offsetTop+element.offsetHeight<=top||element.offsetLeft+element.offsetWidth<=left) {
        test = true;
    }

    return test;

}

function calibrateGrid(grid) {
    if(grid.style.height!=='') {
        grid.style.maxHeight = grid.style.height;
        grid.style.height = '';
        var body = grid.querySelector('.z-grid-body');
        if(body.style.height!=='') {
            body.style.maxHeight = body.style.height;
            body.style.height = '';
        }
    }
}

setTimeout(calibrate, 1000);
